﻿namespace centralUnidas
{
    partial class frmIdioma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmIdioma));
            this.pictFrame = new System.Windows.Forms.PictureBox();
            this.pnlBrasil = new System.Windows.Forms.Panel();
            this.pictBrasil = new System.Windows.Forms.PictureBox();
            this.pnlUK = new System.Windows.Forms.Panel();
            this.pictUK = new System.Windows.Forms.PictureBox();
            this.pnlEspanha = new System.Windows.Forms.Panel();
            this.pictEspanha = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tmrIdioma = new System.Windows.Forms.Timer(this.components);
            this.tmrReturn = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictFrame)).BeginInit();
            this.pnlBrasil.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictBrasil)).BeginInit();
            this.pnlUK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictUK)).BeginInit();
            this.pnlEspanha.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictEspanha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictFrame
            // 
            this.pictFrame.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictFrame.BackColor = System.Drawing.Color.White;
            this.pictFrame.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictFrame.Image = global::centralUnidas.Properties.Resources.Totem_flag_frame;
            this.pictFrame.Location = new System.Drawing.Point(313, 230);
            this.pictFrame.Name = "pictFrame";
            this.pictFrame.Size = new System.Drawing.Size(825, 276);
            this.pictFrame.TabIndex = 0;
            this.pictFrame.TabStop = false;
            // 
            // pnlBrasil
            // 
            this.pnlBrasil.Controls.Add(this.pictBrasil);
            this.pnlBrasil.Location = new System.Drawing.Point(416, 346);
            this.pnlBrasil.Name = "pnlBrasil";
            this.pnlBrasil.Size = new System.Drawing.Size(160, 97);
            this.pnlBrasil.TabIndex = 1;
            this.pnlBrasil.Click += new System.EventHandler(this.pnlBrasil_Click);
            this.pnlBrasil.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlBrasil_Paint);
            // 
            // pictBrasil
            // 
            this.pictBrasil.BackgroundImage = global::centralUnidas.Properties.Resources.Totem_Flag_Brasil;
            this.pictBrasil.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictBrasil.Location = new System.Drawing.Point(6, 6);
            this.pictBrasil.Name = "pictBrasil";
            this.pictBrasil.Size = new System.Drawing.Size(149, 85);
            this.pictBrasil.TabIndex = 0;
            this.pictBrasil.TabStop = false;
            this.pictBrasil.Click += new System.EventHandler(this.pictBrasil_Click);
            // 
            // pnlUK
            // 
            this.pnlUK.Controls.Add(this.pictUK);
            this.pnlUK.Location = new System.Drawing.Point(871, 346);
            this.pnlUK.Name = "pnlUK";
            this.pnlUK.Size = new System.Drawing.Size(160, 97);
            this.pnlUK.TabIndex = 2;
            this.pnlUK.Click += new System.EventHandler(this.pnlUK_Click);
            // 
            // pictUK
            // 
            this.pictUK.BackgroundImage = global::centralUnidas.Properties.Resources.Totem_Flag_UK;
            this.pictUK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictUK.Location = new System.Drawing.Point(6, 6);
            this.pictUK.Name = "pictUK";
            this.pictUK.Size = new System.Drawing.Size(149, 85);
            this.pictUK.TabIndex = 0;
            this.pictUK.TabStop = false;
            this.pictUK.Click += new System.EventHandler(this.pictUK_Click);
            // 
            // pnlEspanha
            // 
            this.pnlEspanha.Controls.Add(this.pictEspanha);
            this.pnlEspanha.Location = new System.Drawing.Point(642, 346);
            this.pnlEspanha.Name = "pnlEspanha";
            this.pnlEspanha.Size = new System.Drawing.Size(160, 97);
            this.pnlEspanha.TabIndex = 3;
            this.pnlEspanha.Click += new System.EventHandler(this.pnlEspanha_Click);
            // 
            // pictEspanha
            // 
            this.pictEspanha.BackgroundImage = global::centralUnidas.Properties.Resources.Totem_Flag_Espanha;
            this.pictEspanha.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictEspanha.Location = new System.Drawing.Point(6, 6);
            this.pictEspanha.Name = "pictEspanha";
            this.pictEspanha.Size = new System.Drawing.Size(149, 85);
            this.pictEspanha.TabIndex = 0;
            this.pictEspanha.TabStop = false;
            this.pictEspanha.Click += new System.EventHandler(this.pictEspanha_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::centralUnidas.Properties.Resources.Totem_flag_selecione;
            this.pictureBox1.Location = new System.Drawing.Point(406, 278);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(641, 31);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // tmrIdioma
            // 
            this.tmrIdioma.Interval = 1000;
            this.tmrIdioma.Tick += new System.EventHandler(this.tmrIdioma_Tick);
            // 
            // tmrReturn
            // 
            this.tmrReturn.Interval = 60000;
            this.tmrReturn.Tick += new System.EventHandler(this.tmrReturn_Tick);
            // 
            // frmIdioma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1366, 768);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pnlEspanha);
            this.Controls.Add(this.pnlUK);
            this.Controls.Add(this.pnlBrasil);
            this.Controls.Add(this.pictFrame);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmIdioma";
            this.Opacity = 0.91D;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.TopMost = false;
            this.TransparencyKey = System.Drawing.Color.Black;
            this.WindowState = System.Windows.Forms.FormWindowState.Normal;
            this.Load += new System.EventHandler(this.frmIdioma_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictFrame)).EndInit();
            this.pnlBrasil.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictBrasil)).EndInit();
            this.pnlUK.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictUK)).EndInit();
            this.pnlEspanha.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictEspanha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictFrame;
        private System.Windows.Forms.Panel pnlBrasil;
        private System.Windows.Forms.PictureBox pictBrasil;
        private System.Windows.Forms.Panel pnlUK;
        private System.Windows.Forms.PictureBox pictUK;
        private System.Windows.Forms.Panel pnlEspanha;
        private System.Windows.Forms.PictureBox pictEspanha;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer tmrIdioma;
        private System.Windows.Forms.Timer tmrReturn;

    }
}