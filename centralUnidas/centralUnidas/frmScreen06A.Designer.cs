﻿namespace centralUnidas
{
    partial class frmScreen06A
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtboxAvisoRodizio = new System.Windows.Forms.RichTextBox();
            this.butContinuar = new System.Windows.Forms.Button();
            this.pictUnidasLogo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // rtboxAvisoRodizio
            // 
            this.rtboxAvisoRodizio.AutoWordSelection = true;
            this.rtboxAvisoRodizio.BackColor = System.Drawing.Color.White;
            this.rtboxAvisoRodizio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtboxAvisoRodizio.Cursor = System.Windows.Forms.Cursors.No;
            this.rtboxAvisoRodizio.Location = new System.Drawing.Point(419, 159);
            this.rtboxAvisoRodizio.Name = "rtboxAvisoRodizio";
            this.rtboxAvisoRodizio.ReadOnly = true;
            this.rtboxAvisoRodizio.ShowSelectionMargin = true;
            this.rtboxAvisoRodizio.Size = new System.Drawing.Size(631, 463);
            this.rtboxAvisoRodizio.TabIndex = 1;
            this.rtboxAvisoRodizio.Text = "";
            // 
            // butContinuar
            // 
            this.butContinuar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.butContinuar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.butContinuar.Font = new System.Drawing.Font("Arial", 22F, System.Drawing.FontStyle.Bold);
            this.butContinuar.ForeColor = System.Drawing.Color.White;
            this.butContinuar.Location = new System.Drawing.Point(610, 641);
            this.butContinuar.Name = "butContinuar";
            this.butContinuar.Size = new System.Drawing.Size(271, 79);
            this.butContinuar.TabIndex = 170;
            this.butContinuar.Text = "Continuar";
            this.butContinuar.UseVisualStyleBackColor = false;
            this.butContinuar.Click += new System.EventHandler(this.butContinuar_Click);
            // 
            // pictUnidasLogo
            // 
            this.pictUnidasLogo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictUnidasLogo.BackColor = System.Drawing.Color.Transparent;
            this.pictUnidasLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictUnidasLogo.Image = global::centralUnidas.Properties.Resources.totem_header;
            this.pictUnidasLogo.Location = new System.Drawing.Point(0, 0);
            this.pictUnidasLogo.Name = "pictUnidasLogo";
            this.pictUnidasLogo.Size = new System.Drawing.Size(1366, 140);
            this.pictUnidasLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictUnidasLogo.TabIndex = 171;
            this.pictUnidasLogo.TabStop = false;
            // 
            // frmScreen06A
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 32F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1366, 741);
            this.Controls.Add(this.pictUnidasLogo);
            this.Controls.Add(this.butContinuar);
            this.Controls.Add(this.rtboxAvisoRodizio);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Arial", 20.25F);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.Name = "frmScreen06A";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = false;
            this.TransparencyKey = System.Drawing.Color.Black;
            this.WindowState = System.Windows.Forms.FormWindowState.Normal;
            this.Load += new System.EventHandler(this.frmScreen06A_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtboxAvisoRodizio;
        private System.Windows.Forms.Button butContinuar;
        private System.Windows.Forms.PictureBox pictUnidasLogo;
    }
}