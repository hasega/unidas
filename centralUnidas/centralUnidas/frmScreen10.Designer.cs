﻿namespace centralUnidas
{
    partial class frmScreen10
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmScreen10));
            this.pictShutdown = new System.Windows.Forms.PictureBox();
            this.pictUnidasLogo = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.txtCodigoSeguranca = new System.Windows.Forms.TextBox();
            this.pictTec_BACK2 = new System.Windows.Forms.PictureBox();
            this.pictTec_TECLADO = new System.Windows.Forms.PictureBox();
            this.pictTec_0 = new System.Windows.Forms.PictureBox();
            this.pictTec_9 = new System.Windows.Forms.PictureBox();
            this.pictTec_8 = new System.Windows.Forms.PictureBox();
            this.pictTec_7 = new System.Windows.Forms.PictureBox();
            this.pictTec_6 = new System.Windows.Forms.PictureBox();
            this.pictTec_5 = new System.Windows.Forms.PictureBox();
            this.pictTec_4 = new System.Windows.Forms.PictureBox();
            this.pictTec_3 = new System.Windows.Forms.PictureBox();
            this.pictTec_2 = new System.Windows.Forms.PictureBox();
            this.pictTec_1 = new System.Windows.Forms.PictureBox();
            this.pictTecladoFundo = new System.Windows.Forms.PictureBox();
            this.tmrTeclado = new System.Windows.Forms.Timer(this.components);
            this.tmrReturn = new System.Windows.Forms.Timer(this.components);
            this.lblCodigo = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblCodigoSegurancaMaior = new System.Windows.Forms.Label();
            this.butIdioma = new System.Windows.Forms.Button();
            this.lblCodigoSegurancaoMenor = new System.Windows.Forms.Label();
            this.butVoltar = new System.Windows.Forms.Button();
            this.butContinuar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictShutdown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_BACK2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_TECLADO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTecladoFundo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // pictShutdown
            // 
            this.pictShutdown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictShutdown.BackColor = System.Drawing.Color.Transparent;
            this.pictShutdown.Image = global::centralUnidas.Properties.Resources.shutdown_button;
            this.pictShutdown.Location = new System.Drawing.Point(12, 185);
            this.pictShutdown.Name = "pictShutdown";
            this.pictShutdown.Size = new System.Drawing.Size(50, 55);
            this.pictShutdown.TabIndex = 151;
            this.pictShutdown.TabStop = false;
            this.pictShutdown.Visible = false;
            this.pictShutdown.Click += new System.EventHandler(this.pictShutdown_Click);
            // 
            // pictUnidasLogo
            // 
            this.pictUnidasLogo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictUnidasLogo.BackColor = System.Drawing.Color.Transparent;
            this.pictUnidasLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictUnidasLogo.Image = global::centralUnidas.Properties.Resources.totem_header;
            this.pictUnidasLogo.Location = new System.Drawing.Point(0, 0);
            this.pictUnidasLogo.Name = "pictUnidasLogo";
            this.pictUnidasLogo.Size = new System.Drawing.Size(1382, 140);
            this.pictUnidasLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictUnidasLogo.TabIndex = 152;
            this.pictUnidasLogo.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox4.Image = global::centralUnidas.Properties.Resources.cvv_cartao_v2;
            this.pictureBox4.Location = new System.Drawing.Point(602, 261);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(194, 130);
            this.pictureBox4.TabIndex = 157;
            this.pictureBox4.TabStop = false;
            // 
            // txtCodigoSeguranca
            // 
            this.txtCodigoSeguranca.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCodigoSeguranca.BackColor = System.Drawing.Color.White;
            this.txtCodigoSeguranca.Font = new System.Drawing.Font("Arial", 27.75F);
            this.txtCodigoSeguranca.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(109)))), ((int)(((byte)(175)))));
            this.txtCodigoSeguranca.Location = new System.Drawing.Point(913, 276);
            this.txtCodigoSeguranca.MaxLength = 1;
            this.txtCodigoSeguranca.Name = "txtCodigoSeguranca";
            this.txtCodigoSeguranca.PasswordChar = '*';
            this.txtCodigoSeguranca.Size = new System.Drawing.Size(107, 71);
            this.txtCodigoSeguranca.TabIndex = 158;
            this.txtCodigoSeguranca.WordWrap = false;
            this.txtCodigoSeguranca.TextChanged += new System.EventHandler(this.txtCodigoSeguranca_TextChanged);
            // 
            // pictTec_BACK2
            // 
            this.pictTec_BACK2.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_BACK2.Image = global::centralUnidas.Properties.Resources.totem_Backspace;
            this.pictTec_BACK2.Location = new System.Drawing.Point(766, 685);
            this.pictTec_BACK2.Name = "pictTec_BACK2";
            this.pictTec_BACK2.Size = new System.Drawing.Size(76, 73);
            this.pictTec_BACK2.TabIndex = 171;
            this.pictTec_BACK2.TabStop = false;
            this.pictTec_BACK2.Click += new System.EventHandler(this.pictTec_BACK2_Click);
            // 
            // pictTec_TECLADO
            // 
            this.pictTec_TECLADO.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_TECLADO.Image = global::centralUnidas.Properties.Resources.totem_teclado;
            this.pictTec_TECLADO.Location = new System.Drawing.Point(602, 685);
            this.pictTec_TECLADO.Name = "pictTec_TECLADO";
            this.pictTec_TECLADO.Size = new System.Drawing.Size(76, 73);
            this.pictTec_TECLADO.TabIndex = 170;
            this.pictTec_TECLADO.TabStop = false;
            this.pictTec_TECLADO.Click += new System.EventHandler(this.pictTec_TECLADO_Click);
            // 
            // pictTec_0
            // 
            this.pictTec_0.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_0.Image = global::centralUnidas.Properties.Resources.totem_0;
            this.pictTec_0.Location = new System.Drawing.Point(684, 685);
            this.pictTec_0.Name = "pictTec_0";
            this.pictTec_0.Size = new System.Drawing.Size(76, 73);
            this.pictTec_0.TabIndex = 169;
            this.pictTec_0.TabStop = false;
            this.pictTec_0.Click += new System.EventHandler(this.pictTec_0_Click);
            // 
            // pictTec_9
            // 
            this.pictTec_9.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_9.Image = global::centralUnidas.Properties.Resources.totem_9;
            this.pictTec_9.Location = new System.Drawing.Point(766, 606);
            this.pictTec_9.Name = "pictTec_9";
            this.pictTec_9.Size = new System.Drawing.Size(76, 73);
            this.pictTec_9.TabIndex = 168;
            this.pictTec_9.TabStop = false;
            this.pictTec_9.Click += new System.EventHandler(this.pictTec_9_Click);
            // 
            // pictTec_8
            // 
            this.pictTec_8.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_8.Image = global::centralUnidas.Properties.Resources.totem_8;
            this.pictTec_8.Location = new System.Drawing.Point(684, 606);
            this.pictTec_8.Name = "pictTec_8";
            this.pictTec_8.Size = new System.Drawing.Size(76, 73);
            this.pictTec_8.TabIndex = 167;
            this.pictTec_8.TabStop = false;
            this.pictTec_8.Click += new System.EventHandler(this.pictTec_8_Click);
            // 
            // pictTec_7
            // 
            this.pictTec_7.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_7.Image = global::centralUnidas.Properties.Resources.totem_7;
            this.pictTec_7.Location = new System.Drawing.Point(602, 606);
            this.pictTec_7.Name = "pictTec_7";
            this.pictTec_7.Size = new System.Drawing.Size(76, 73);
            this.pictTec_7.TabIndex = 166;
            this.pictTec_7.TabStop = false;
            this.pictTec_7.Click += new System.EventHandler(this.pictTec_7_Click);
            // 
            // pictTec_6
            // 
            this.pictTec_6.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_6.Image = global::centralUnidas.Properties.Resources.totem_6;
            this.pictTec_6.Location = new System.Drawing.Point(766, 527);
            this.pictTec_6.Name = "pictTec_6";
            this.pictTec_6.Size = new System.Drawing.Size(76, 73);
            this.pictTec_6.TabIndex = 165;
            this.pictTec_6.TabStop = false;
            this.pictTec_6.Click += new System.EventHandler(this.pictTec_6_Click);
            // 
            // pictTec_5
            // 
            this.pictTec_5.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_5.Image = global::centralUnidas.Properties.Resources.totem_5;
            this.pictTec_5.Location = new System.Drawing.Point(684, 527);
            this.pictTec_5.Name = "pictTec_5";
            this.pictTec_5.Size = new System.Drawing.Size(76, 73);
            this.pictTec_5.TabIndex = 164;
            this.pictTec_5.TabStop = false;
            this.pictTec_5.Click += new System.EventHandler(this.pictTec_5_Click);
            // 
            // pictTec_4
            // 
            this.pictTec_4.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_4.Image = global::centralUnidas.Properties.Resources.totem_4;
            this.pictTec_4.Location = new System.Drawing.Point(602, 527);
            this.pictTec_4.Name = "pictTec_4";
            this.pictTec_4.Size = new System.Drawing.Size(76, 73);
            this.pictTec_4.TabIndex = 163;
            this.pictTec_4.TabStop = false;
            this.pictTec_4.Click += new System.EventHandler(this.pictTec_4_Click);
            // 
            // pictTec_3
            // 
            this.pictTec_3.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_3.Image = global::centralUnidas.Properties.Resources.totem_3;
            this.pictTec_3.Location = new System.Drawing.Point(766, 448);
            this.pictTec_3.Name = "pictTec_3";
            this.pictTec_3.Size = new System.Drawing.Size(76, 73);
            this.pictTec_3.TabIndex = 162;
            this.pictTec_3.TabStop = false;
            this.pictTec_3.Click += new System.EventHandler(this.pictTec_3_Click);
            // 
            // pictTec_2
            // 
            this.pictTec_2.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_2.Image = global::centralUnidas.Properties.Resources.totem_2;
            this.pictTec_2.Location = new System.Drawing.Point(684, 448);
            this.pictTec_2.Name = "pictTec_2";
            this.pictTec_2.Size = new System.Drawing.Size(76, 73);
            this.pictTec_2.TabIndex = 161;
            this.pictTec_2.TabStop = false;
            this.pictTec_2.Click += new System.EventHandler(this.pictTec_2_Click);
            // 
            // pictTec_1
            // 
            this.pictTec_1.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_1.Image = global::centralUnidas.Properties.Resources.totem_1;
            this.pictTec_1.Location = new System.Drawing.Point(602, 448);
            this.pictTec_1.Name = "pictTec_1";
            this.pictTec_1.Size = new System.Drawing.Size(76, 73);
            this.pictTec_1.TabIndex = 160;
            this.pictTec_1.TabStop = false;
            this.pictTec_1.Click += new System.EventHandler(this.pictTec_1_Click);
            // 
            // pictTecladoFundo
            // 
            this.pictTecladoFundo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictTecladoFundo.BackColor = System.Drawing.Color.Transparent;
            this.pictTecladoFundo.BackgroundImage = global::centralUnidas.Properties.Resources.totem_teclado_fundo;
            this.pictTecladoFundo.Location = new System.Drawing.Point(0, 433);
            this.pictTecladoFundo.Name = "pictTecladoFundo";
            this.pictTecladoFundo.Size = new System.Drawing.Size(1382, 335);
            this.pictTecladoFundo.TabIndex = 159;
            this.pictTecladoFundo.TabStop = false;
            // 
            // tmrTeclado
            // 
            this.tmrTeclado.Tick += new System.EventHandler(this.tmrTeclado_Tick);
            // 
            // tmrReturn
            // 
            this.tmrReturn.Enabled = true;
            this.tmrReturn.Interval = 90000;
            this.tmrReturn.Tick += new System.EventHandler(this.tmrReturn_Tick);
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.BackColor = System.Drawing.Color.White;
            this.lblCodigo.Font = new System.Drawing.Font("Arial", 17F);
            this.lblCodigo.ForeColor = System.Drawing.Color.Gray;
            this.lblCodigo.Location = new System.Drawing.Point(356, 222);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(1011, 39);
            this.lblCodigo.TabIndex = 173;
            this.lblCodigo.Text = "Digite o código de segurança para finalizarmos sua pré-autorização.";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox2.Image = global::centralUnidas.Properties.Resources.Totem_Traco;
            this.pictureBox2.Location = new System.Drawing.Point(364, 212);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(683, 1);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 174;
            this.pictureBox2.TabStop = false;
            // 
            // lblCodigoSegurancaMaior
            // 
            this.lblCodigoSegurancaMaior.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCodigoSegurancaMaior.BackColor = System.Drawing.Color.White;
            this.lblCodigoSegurancaMaior.Font = new System.Drawing.Font("Arial", 26F, System.Drawing.FontStyle.Bold);
            this.lblCodigoSegurancaMaior.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblCodigoSegurancaMaior.Location = new System.Drawing.Point(0, 155);
            this.lblCodigoSegurancaMaior.Name = "lblCodigoSegurancaMaior";
            this.lblCodigoSegurancaMaior.Size = new System.Drawing.Size(1382, 54);
            this.lblCodigoSegurancaMaior.TabIndex = 175;
            this.lblCodigoSegurancaMaior.Text = "Código de Segurança_EN";
            this.lblCodigoSegurancaMaior.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // butIdioma
            // 
            this.butIdioma.FlatAppearance.BorderSize = 0;
            this.butIdioma.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.butIdioma.ForeColor = System.Drawing.Color.Gray;
            this.butIdioma.Image = global::centralUnidas.Properties.Resources.Totem_Flag_Brasil_botao;
            this.butIdioma.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butIdioma.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.butIdioma.Location = new System.Drawing.Point(1148, 0);
            this.butIdioma.Name = "butIdioma";
            this.butIdioma.Size = new System.Drawing.Size(148, 61);
            this.butIdioma.TabIndex = 266;
            this.butIdioma.Text = "PORTUGUÊS";
            this.butIdioma.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butIdioma.UseCompatibleTextRendering = true;
            this.butIdioma.UseMnemonic = false;
            this.butIdioma.UseVisualStyleBackColor = true;
            // 
            // lblCodigoSegurancaoMenor
            // 
            this.lblCodigoSegurancaoMenor.AutoSize = true;
            this.lblCodigoSegurancaoMenor.BackColor = System.Drawing.Color.White;
            this.lblCodigoSegurancaoMenor.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.lblCodigoSegurancaoMenor.ForeColor = System.Drawing.Color.Gray;
            this.lblCodigoSegurancaoMenor.Location = new System.Drawing.Point(788, 273);
            this.lblCodigoSegurancaoMenor.Name = "lblCodigoSegurancaoMenor";
            this.lblCodigoSegurancaoMenor.Size = new System.Drawing.Size(169, 70);
            this.lblCodigoSegurancaoMenor.TabIndex = 267;
            this.lblCodigoSegurancaoMenor.Text = "Código de\r\nSegurança";
            // 
            // butVoltar
            // 
            this.butVoltar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.butVoltar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.butVoltar.Font = new System.Drawing.Font("Arial", 13F, System.Drawing.FontStyle.Bold);
            this.butVoltar.ForeColor = System.Drawing.Color.White;
            this.butVoltar.Location = new System.Drawing.Point(1186, 341);
            this.butVoltar.Name = "butVoltar";
            this.butVoltar.Size = new System.Drawing.Size(155, 40);
            this.butVoltar.TabIndex = 268;
            this.butVoltar.Text = "Voltar";
            this.butVoltar.UseVisualStyleBackColor = false;
            this.butVoltar.Click += new System.EventHandler(this.butVoltar_Click);
            // 
            // butContinuar
            // 
            this.butContinuar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.butContinuar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.butContinuar.Font = new System.Drawing.Font("Arial", 13F, System.Drawing.FontStyle.Bold);
            this.butContinuar.ForeColor = System.Drawing.Color.White;
            this.butContinuar.Location = new System.Drawing.Point(913, 341);
            this.butContinuar.Name = "butContinuar";
            this.butContinuar.Size = new System.Drawing.Size(155, 40);
            this.butContinuar.TabIndex = 269;
            this.butContinuar.Text = "Continuar";
            this.butContinuar.UseVisualStyleBackColor = false;
            this.butContinuar.Click += new System.EventHandler(this.butContinuar_Click);
            // 
            // frmScreen10
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(20F, 37F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1366, 768);
            this.ControlBox = false;
            this.Controls.Add(this.butContinuar);
            this.Controls.Add(this.butVoltar);
            this.Controls.Add(this.lblCodigoSegurancaoMenor);
            this.Controls.Add(this.butIdioma);
            this.Controls.Add(this.pictShutdown);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.lblCodigo);
            this.Controls.Add(this.pictTec_BACK2);
            this.Controls.Add(this.pictTec_TECLADO);
            this.Controls.Add(this.pictTec_0);
            this.Controls.Add(this.pictTec_9);
            this.Controls.Add(this.pictTec_8);
            this.Controls.Add(this.pictTec_7);
            this.Controls.Add(this.pictTec_6);
            this.Controls.Add(this.pictTec_5);
            this.Controls.Add(this.pictTec_4);
            this.Controls.Add(this.pictTec_3);
            this.Controls.Add(this.pictTec_2);
            this.Controls.Add(this.pictTec_1);
            this.Controls.Add(this.pictTecladoFundo);
            this.Controls.Add(this.txtCodigoSeguranca);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictUnidasLogo);
            this.Controls.Add(this.lblCodigoSegurancaMaior);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmScreen10";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CAU - Unidas";
            this.Load += new System.EventHandler(this.frmScreen10_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictShutdown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_BACK2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_TECLADO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTecladoFundo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictShutdown;
        private System.Windows.Forms.PictureBox pictUnidasLogo;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.TextBox txtCodigoSeguranca;
        private System.Windows.Forms.PictureBox pictTec_BACK2;
        private System.Windows.Forms.PictureBox pictTec_TECLADO;
        private System.Windows.Forms.PictureBox pictTec_0;
        private System.Windows.Forms.PictureBox pictTec_9;
        private System.Windows.Forms.PictureBox pictTec_8;
        private System.Windows.Forms.PictureBox pictTec_7;
        private System.Windows.Forms.PictureBox pictTec_6;
        private System.Windows.Forms.PictureBox pictTec_5;
        private System.Windows.Forms.PictureBox pictTec_4;
        private System.Windows.Forms.PictureBox pictTec_3;
        private System.Windows.Forms.PictureBox pictTec_2;
        private System.Windows.Forms.PictureBox pictTec_1;
        private System.Windows.Forms.PictureBox pictTecladoFundo;
        private System.Windows.Forms.Timer tmrTeclado;
        private System.Windows.Forms.Timer tmrReturn;
        private System.Windows.Forms.Label lblCodigo;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblCodigoSegurancaMaior;
        private System.Windows.Forms.Button butIdioma;
        private System.Windows.Forms.Label lblCodigoSegurancaoMenor;
        private System.Windows.Forms.Button butVoltar;
        private System.Windows.Forms.Button butContinuar;
    }
}