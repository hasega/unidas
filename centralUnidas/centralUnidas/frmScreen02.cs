﻿// Aplicação: Unidas Totem
// Data     : 01/10/2015
// Versão   : 1.0
// Empresa  : Unidas Rent a Car
// Equipe   : Unidas Desenvolvimento
// Objetivo : Obtem reserva via web-service
// Histórico: 
//      01/10/2015: Inicio de desenvolvimento para acesso geral
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Globalization;
using System.Configuration;
using Newtonsoft.Json;
using System.Web;
using System.Runtime.Serialization.Json;
using fastJSON;
using Newtonsoft.Json.Linq;
using System.Drawing.Imaging;
using System.Threading;
using centralUnidas.centralUnidasWCF;
using centralUnidas.wbsUnidasSrv;
using System.Runtime.InteropServices;
using System.Management;
using System.Diagnostics;
using System.Resources;

namespace centralUnidas
{

    public partial class frmScreen02 : Form
    {

        public ResXResourceSet ResxSetForm; 

        public frmScreen02()
        {
            InitializeComponent();
            Teclado_Hide();
        }

        private void frmScreen02_Load(object sender, EventArgs e)
        {
            Globals.glbScreen = 2;
            Globals.glbReservaNumero = "";

            int nScreenWidth = this.Width;
            int nScreenWidthSplit = nScreenWidth / 2;
            this.Refresh();

            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;
            lblVersao.Text = String.Format("Versao: {0}", version);
            lblLoja.Text = "Loja: "+ Globals.glbLojaTotem.Trim();

            Globals.glbDebugMode = Convert.ToInt16(ConfigurationManager.AppSettings.Get("appDebugMode"));
            Shutdown_Enable();

            btnContinuar.Focus();
            tmrTecladoShow.Enabled = true;
            tmrReturn.Enabled = true;

            AtualizarInterfaceParaIdiomaCorrente();

           

        }

        private void Shutdown_Enable()
        {
            if (Globals.glbDebugMode == 0)
                pictShutdown.Visible = false;
            else
                pictShutdown.Visible = true;
        }

        private void txtReserva1_TextChanged(object sender, EventArgs e)
        {
            if (txtReserva1.TextLength > 0)
            {
                tmrReturn.Stop();
                tmrReturn.Start();
            }
        }

        private void txtReserva1_Enter(object sender, EventArgs e)
        {
            tmrTecladoShow.Enabled = true;
        }

        private string ConverterData(string strDataEntrada)
        {
            CultureInfo cultureBR = new CultureInfo("pt-BR");
            string strDataSaida = "";
            DateTime dateTimeValueOut;
            IFormatProvider en = new CultureInfo("en-US");
            DateTime.TryParse(strDataEntrada, en, System.Globalization.DateTimeStyles.None, out dateTimeValueOut);
            strDataSaida = dateTimeValueOut.ToString("dd/MM/yyyy HH:mm", cultureBR);
            return strDataSaida;
        }

        private int obtemReservaXMLviaWCF(string xmlEncodedList)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xmlEncodedList);

                string srvGivenName = "";
                string srvEmail = "";
                string srvVehReservation = "";
                string srvVoucherIdentifier = "";
                string srvVoucherValueType = "";
                string srvConfID = "";
                string srvCompanyShortName = "";
                string srvResGruCod = "";
                string srvResSigOri = "";
                string srvPickUpDateTime = "";
                string srvReturnDateTime = "";
                string srvDistUnitName = "";
                string srvPickUpLocation = "";
                string srvReturnLocation = "";
                string srvPassengerQuantity = "";
                string srvBaggageQuantity = "";
                string srvVehicleCode = "";
                string srvVehicleCodeContext = "";
                string srvVehicleMakeModel = "";
                string srvPictureURL = "";
                string srvVehicleChargeCurrencyCode = "";
                string srvVehicleChargeAmount = "";
                string srvVehicleChargeTaxInclusive = "";
                string srvVehicleChargeGuaranteedInd = "";
                string srvVehicleChargePurpose = "";
                string srvGender = "";
                string srvMOTDES = "";
                string srvLOJNOM = "";
                string srvLOJDES = "";
                string srvLOJNOMDEV = "";
                string srvLOJDESDEV = "";
                string srvRTBDIARIA = "";
                string srvRTBDIAEXT = "";
                string srvRTBHOREXT = "";
                string srvRTBTOTDIA = "";
                string srvRTBTOTDIE = "";
                string srvRTBTOTHRE = "";
                string srvRTBTOTDSC = "";
                string srvRTBSUBTOT = "";
                string srvRTBTOTRES = "";
                string srvRTBVALDIA = "";
                string srvRTBVALFRQ = "";
                string srvRTBPERTXS = "";
                string srvRTBVALTXS = "";
                string srvRTBPERTAG = "";
                string srvRTBVALTAG = "";
                string srvRESOPE = "";
                string srvLOJTEL = "";
                string srvRESDOCCLI = "";
                string srvCPFCARHAB = "";
                string srvCPFEXPHAB = "";
                string srvLOJCODSITE = "";
                string srvSTIP = "";
                string srvSTPORTA = "";
                string srvRESPESCPF = "";
                string srvDATA_CAP = "";
                string srvCLIDOC = "";

                //Apenas reservas confirmadas serão processadas: ReservationStatus = "CFA"

                XmlNodeList xmlVehReservation = xmlDoc.GetElementsByTagName("STASIG");
                if (xmlVehReservation.Count > 0)
                {
                    for (int x = 0; x < xmlVehReservation.Count; x++)
                    {
                        if (!string.IsNullOrEmpty(xmlVehReservation.Item(x).InnerXml))
                        {
                            srvVehReservation = xmlVehReservation.Item(x).InnerXml;
                            Globals.glbVehReservation = srvVehReservation.Trim();
                        }
                    }
                }

                if (!string.IsNullOrEmpty(Globals.glbVehReservation))
                {
                    if (Globals.glbVehReservation.ToUpper() == "CFA")
                    {
                        XmlNodeList xmlMOTDES = xmlDoc.GetElementsByTagName("MOTDES");
                        if (xmlMOTDES.Count > 0)
                        {
                            for (int x = 0; x < xmlMOTDES.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlMOTDES.Item(x).InnerXml))
                                {
                                    srvMOTDES = xmlMOTDES.Item(x).InnerXml;
                                    Globals.glbMOTDES = srvMOTDES.Trim();
                                }
                            }
                        }

                        #region Regra de preenchimento do PAX
                        XmlNodeList xmlGivenName = xmlDoc.GetElementsByTagName("RESPAX");
                        if (xmlGivenName.Count > 0)
                        {
                            for (int x = 0; x < xmlGivenName.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlGivenName.Item(x).InnerXml))
                                {
                                    srvGivenName = xmlGivenName.Item(x).InnerXml;
                                    Globals.glbGivenName = srvGivenName.Trim();
                                    Globals.glbRA_respax = srvGivenName.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlEmail = xmlDoc.GetElementsByTagName("RESPAXEML");
                        if (xmlEmail.Count > 0)
                        {
                            for (int x = 0; x < xmlEmail.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlEmail.Item(x).InnerXml))
                                {
                                    srvEmail = xmlEmail.Item(0).InnerXml;
                                    Globals.glbEmail = srvEmail.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlRESPESCPF = xmlDoc.GetElementsByTagName("RESPAXCPF");
                        if (xmlRESPESCPF.Count > 0)
                        {
                            for (int x = 0; x < xmlRESPESCPF.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlRESPESCPF.Item(x).InnerXml))
                                {
                                    srvRESPESCPF = xmlRESPESCPF.Item(0).InnerXml;
                                    Globals.glbRESPESCPF = srvRESPESCPF.Trim();
                                    Globals.glbRA_respaxcpf = srvRESPESCPF.Trim();
                                }
                            }
                        }
                        #endregion 

                        if (string.IsNullOrEmpty(Globals.glbGivenName))
                        {
                            XmlNodeList xmlGivenName1 = xmlDoc.GetElementsByTagName("RESCON");
                            if (xmlGivenName1.Count > 0)
                            {
                                for (int x = 0; x < xmlGivenName1.Count; x++)
                                {
                                    if (!string.IsNullOrEmpty(xmlGivenName1.Item(x).InnerXml))
                                    {
                                        srvGivenName = xmlGivenName1.Item(x).InnerXml;
                                        Globals.glbGivenName = srvGivenName.Trim();
                                        //Globals.glbRA_clinom = srvGivenName.Trim();
                                    }
                                }
                            }
                        }

                        if (string.IsNullOrEmpty(Globals.glbCLIDOC_EXIST))
                        {
                            XmlNodeList xmlCLIDOC_EXIST = xmlDoc.GetElementsByTagName("CLIDOC");
                            if (xmlCLIDOC_EXIST.Count > 0)
                            {
                                for (int x = 0; x < xmlCLIDOC_EXIST.Count; x++)
                                {
                                    if (!string.IsNullOrEmpty(xmlCLIDOC_EXIST.Item(x).InnerXml))
                                    {
                                        srvCLIDOC = xmlCLIDOC_EXIST.Item(x).InnerXml;
                                        Globals.glbCLIDOC_EXIST = srvCLIDOC.Trim();
                                    }
                                }
                            }
                        }

                        XmlNodeList tabRESLOJOPE = xmlDoc.GetElementsByTagName("RESLOJOPE");
                        if (tabRESLOJOPE.Count > 0)
                        {
                            for (int x = 0; x < tabRESLOJOPE.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabRESLOJOPE.Item(x).InnerXml))
                                {
                                    Globals.glbRA_r_aresloj = tabRESLOJOPE.Item(x).InnerXml.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlRESOPE = xmlDoc.GetElementsByTagName("RESOPE");
                        if (xmlRESOPE.Count > 0)
                        {
                            for (int x = 0; x < xmlRESOPE.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlRESOPE.Item(x).InnerXml))
                                {
                                    srvRESOPE = xmlRESOPE.Item(0).InnerXml;
                                    Globals.glbRESOPE = srvRESOPE.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlLOJTEL = xmlDoc.GetElementsByTagName("LOJTEL");
                        if (xmlLOJTEL.Count > 0)
                        {
                            for (int x = 0; x < xmlLOJTEL.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlLOJTEL.Item(x).InnerXml))
                                {
                                    srvLOJTEL = xmlLOJTEL.Item(0).InnerXml;
                                    Globals.glbLOJTEL = srvLOJTEL.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlLOJCODSITE = xmlDoc.GetElementsByTagName("LOJCODSITE");
                        if (xmlLOJCODSITE.Count > 0)
                        {
                            for (int x = 0; x < xmlLOJCODSITE.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlLOJCODSITE.Item(x).InnerXml))
                                {
                                    srvLOJCODSITE = xmlLOJCODSITE.Item(0).InnerXml;
                                    Globals.glbLOJCODSITE = srvLOJCODSITE.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlSTIP = xmlDoc.GetElementsByTagName("STIP");
                        if (xmlSTIP.Count > 0)
                        {
                            for (int x = 0; x < xmlSTIP.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlSTIP.Item(x).InnerXml))
                                {
                                    srvSTIP = xmlSTIP.Item(0).InnerXml;
                                    Globals.glbSTIP = srvSTIP.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlDATA_CAP = xmlDoc.GetElementsByTagName("DATA_CAP");
                        if (xmlDATA_CAP.Count > 0)
                        {
                            for (int x = 0; x < xmlDATA_CAP.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlDATA_CAP.Item(x).InnerXml))
                                {
                                    srvDATA_CAP = xmlDATA_CAP.Item(0).InnerXml;

                                    if (srvDATA_CAP.Trim().Contains("1900"))
                                    {
                                        Globals.glbDATA_CAP = "";
                                    }
                                    else
                                    {
                                        Globals.glbDATA_CAP = srvDATA_CAP.Trim();
                                    }
                                }
                            }
                        }

                        XmlNodeList xmlSTPORTA = xmlDoc.GetElementsByTagName("STPORTA");
                        if (xmlSTPORTA.Count > 0)
                        {
                            for (int x = 0; x < xmlSTPORTA.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlSTPORTA.Item(x).InnerXml))
                                {
                                    srvSTPORTA = xmlSTPORTA.Item(0).InnerXml;
                                    Globals.glbSTPORTA = srvSTPORTA.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlRESDOCCLI = xmlDoc.GetElementsByTagName("RESDOCCLI");
                        if (xmlRESDOCCLI.Count > 0)
                        {
                            for (int x = 0; x < xmlRESDOCCLI.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlRESDOCCLI.Item(x).InnerXml))
                                {
                                    srvRESDOCCLI = xmlRESDOCCLI.Item(0).InnerXml;
                                    Globals.glbRA_clifat = srvRESDOCCLI.Trim();
                                    Globals.glbRESDOCCLI = srvRESDOCCLI.Trim();
                                }
                            }
                        }

                        #region Regra de Cliente
                        // Regra ok - valida
                        XmlNodeList tabRESLOC = xmlDoc.GetElementsByTagName("RESLOC");
                        if (tabRESLOC.Count > 0)
                        {
                            for (int x = 0; x < tabRESLOC.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabRESLOC.Item(x).InnerXml))
                                {
                                    Globals.glbRA_clidoc = tabRESLOC.Item(x).InnerXml.Trim();
                                }
                            }
                        }

                        XmlNodeList tabRESLOCNOM = xmlDoc.GetElementsByTagName("RESLOCNOM");
                        if (tabRESLOCNOM.Count > 0)
                        {
                            for (int x = 0; x < tabRESLOCNOM.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabRESLOCNOM.Item(x).InnerXml))
                                {
                                    Globals.glbRA_clinom = tabRESLOCNOM.Item(x).InnerXml.Trim();
                                }
                            }
                        }
                        #endregion

                        XmlNodeList tabANUMRES = xmlDoc.GetElementsByTagName("R_ANUMRES");
                        if (tabANUMRES.Count > 0)
                        {
                            for (int x = 0; x < tabANUMRES.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabANUMRES.Item(x).InnerXml))
                                {
                                    Globals.glbR_ANUMRES = tabANUMRES.Item(x).InnerXml.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlCPFCARHAB = xmlDoc.GetElementsByTagName("CPFCARHAB");
                        if (xmlCPFCARHAB.Count > 0)
                        {
                            for (int x = 0; x < xmlCPFCARHAB.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlCPFCARHAB.Item(x).InnerXml))
                                {
                                    srvCPFCARHAB = xmlCPFCARHAB.Item(0).InnerXml;
                                    Globals.glbCPFCARHAB = srvCPFCARHAB.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlCPFEXPHAB = xmlDoc.GetElementsByTagName("CPFEXPHAB");
                        if (xmlCPFEXPHAB.Count > 0)
                        {
                            for (int x = 0; x < xmlCPFEXPHAB.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlCPFEXPHAB.Item(x).InnerXml))
                                {
                                    srvCPFEXPHAB = xmlCPFEXPHAB.Item(0).InnerXml;
                                    Globals.glbCPFEXPHAB = srvCPFEXPHAB.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlVoucher = xmlDoc.GetElementsByTagName("RESNUMVCH");
                        if (xmlVoucher.Count > 0)
                        {
                            for (int x = 0; x < xmlVoucher.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlVoucher.Item(x).InnerXml))
                                {
                                    srvVoucherIdentifier = xmlVoucher.Item(0).InnerXml;
                                    Globals.glbVoucherIdentifier = srvVoucherIdentifier.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlVoucherValueType = xmlDoc.GetElementsByTagName("RESTPVCH");
                        if (xmlVoucherValueType.Count > 0)
                        {
                            for (int x = 0; x < xmlVoucherValueType.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlVoucherValueType.Item(x).InnerXml))
                                {
                                    srvVoucherValueType = xmlVoucherValueType.Item(0).InnerXml;
                                    Globals.glbVoucherValueType = srvVoucherValueType.Trim();
                                    Globals.glbRA_ResTpvCh = srvVoucherValueType.Trim();
                                }
                            }
                        }

                        XmlNodeList tabRESPNR = xmlDoc.GetElementsByTagName("RESPNR");
                        if (tabRESPNR.Count > 0)
                        {
                            for (int x = 0; x < tabRESPNR.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabRESPNR.Item(x).InnerXml))
                                {
                                    Globals.glbRA_respnr = tabRESPNR.Item(x).InnerXml.Trim();
                                }
                            }
                        }

                        XmlNodeList tabCATSEGDSC = xmlDoc.GetElementsByTagName("CATSEGDSC");
                        if (tabCATSEGDSC.Count > 0)
                        {
                            for (int x = 0; x < tabCATSEGDSC.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabCATSEGDSC.Item(x).InnerXml))
                                {
                                    Globals.glbRA_r_atipo = tabCATSEGDSC.Item(x).InnerXml.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlResnum = xmlDoc.GetElementsByTagName("RESNUM");
                        if (xmlResnum.Count > 0)
                        {
                            for (int x = 0; x < xmlResnum.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlResnum.Item(x).InnerXml))
                                {
                                    srvConfID = xmlResnum.Item(0).InnerXml;
                                    Globals.glbConfID = srvConfID.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlCompanyShortName = xmlDoc.GetElementsByTagName("RESNOM");
                        if (xmlCompanyShortName.Count > 0)
                        {
                            for (int x = 0; x < xmlCompanyShortName.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlCompanyShortName.Item(x).InnerXml))
                                {
                                    srvCompanyShortName = xmlCompanyShortName.Item(0).InnerXml;
                                    Globals.glbCompanyShortName = srvCompanyShortName.Trim();
                                    Globals.glbRESNOM = srvCompanyShortName.Trim();
                                }
                            }
                        }

                        Globals.glbPickUpDateTime = "";
                        XmlNodeList xmlRESDATRET = xmlDoc.GetElementsByTagName("RESDATRET");
                        if (xmlRESDATRET.Count > 0)
                        {

                            for (int x = 0; x < xmlRESDATRET.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlRESDATRET.Item(x).InnerXml))
                                {
                                    srvPickUpDateTime = xmlRESDATRET.Item(0).InnerXml;
                                    Globals.glbPickUpDateTime = srvPickUpDateTime.Substring(0, 10);
                                    Globals.glbRA_r_adatret = srvPickUpDateTime.Substring(0, 10);
                                }
                            }
                        }

                        srvPickUpDateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                        Globals.glbPickUpDateTime = srvPickUpDateTime.Substring(0, 10);
                        Globals.glbRA_r_adatret = srvPickUpDateTime.Substring(0, 10);

                        XmlNodeList xmlRESHORRET = xmlDoc.GetElementsByTagName("RESHORRET");
                        if (xmlRESHORRET.Count > 0)
                        {

                            for (int x = 0; x < xmlRESHORRET.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlRESHORRET.Item(x).InnerXml))
                                {
                                    srvPickUpDateTime = xmlRESHORRET.Item(0).InnerXml;
                                    Globals.glbPickUpDateTime = Globals.glbPickUpDateTime + "T" + srvPickUpDateTime.Trim().Substring(0, 2) + ":" + srvPickUpDateTime.Trim().Substring(2, 2);
                                    Globals.glbRA_r_ahorret = Globals.glbPickUpDateTime.Trim();
                                }
                            }
                        }

                        Globals.glbReturnDateTime = "";
                        XmlNodeList xmlRESDATDEV = xmlDoc.GetElementsByTagName("RESDATDEV");
                        if (xmlRESDATDEV.Count > 0)
                        {
                            for (int x = 0; x < xmlRESDATDEV.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlRESDATDEV.Item(x).InnerXml))
                                {
                                    srvReturnDateTime = xmlRESDATDEV.Item(0).InnerXml;
                                    Globals.glbReturnDateTime = srvReturnDateTime.Substring(0, 10);
                                    Globals.glbRA_r_adatdev = srvReturnDateTime.Substring(0, 10);
                                }
                            }
                        }

                        XmlNodeList xmlRESHORDEV = xmlDoc.GetElementsByTagName("RESHORDEV");
                        if (xmlRESHORDEV.Count > 0)
                        {
                            for (int x = 0; x < xmlRESHORDEV.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlRESHORDEV.Item(x).InnerXml))
                                {
                                    srvReturnDateTime = xmlRESHORDEV.Item(0).InnerXml;
                                    Globals.glbReturnDateTime = Globals.glbReturnDateTime + "T" + srvReturnDateTime.Trim().Substring(0, 2) + ":" + srvReturnDateTime.Trim().Substring(2, 2);
                                    Globals.glbRA_r_ahordev = Globals.glbReturnDateTime.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlResGruCod = xmlDoc.GetElementsByTagName("RESGRUCOD");
                        if (xmlResGruCod.Count > 0)
                        {
                            for (int x = 0; x < xmlResGruCod.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlResGruCod.Item(x).InnerXml))
                                {
                                    srvResGruCod = xmlResGruCod.Item(0).InnerXml;
                                    Globals.glbResGruCod = srvResGruCod.Trim();
                                    Globals.glbRA_grucod = srvResGruCod.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlRESSIGRET = xmlDoc.GetElementsByTagName("RESSIGRET");
                        if (xmlRESSIGRET.Count > 0)
                        {
                            for (int x = 0; x < xmlRESSIGRET.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlRESSIGRET.Item(x).InnerXml))
                                {
                                    srvPickUpLocation = xmlRESSIGRET.Item(0).InnerXml;
                                    Globals.glbPickUpLocation = srvPickUpLocation.Trim();
                                    Globals.glbRA_r_alojret = srvPickUpLocation.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlRESSIGDEV = xmlDoc.GetElementsByTagName("RESSIGDEV");
                        if (xmlRESSIGDEV.Count > 0)
                        {
                            for (int x = 0; x < xmlRESSIGDEV.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlRESSIGDEV.Item(x).InnerXml))
                                {
                                    srvReturnLocation = xmlRESSIGDEV.Item(0).InnerXml;
                                    Globals.glbReturnLocation = srvReturnLocation.Trim();
                                    Globals.glbRA_r_alojdev = Globals.glbReturnLocation.Trim();
                                }
                            }
                        }

                        XmlNodeList tabTPTCONCOD = xmlDoc.GetElementsByTagName("TPTCONCOD");
                        if (tabTPTCONCOD.Count > 0)
                        {
                            for (int x = 0; x < tabTPTCONCOD.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabTPTCONCOD.Item(x).InnerXml))
                                {
                                    Globals.glbRA_tptconcod = tabTPTCONCOD.Item(x).InnerXml.Trim();
                                }
                            }
                        }

                        XmlNodeList tabTARDES = xmlDoc.GetElementsByTagName("TARDES");
                        if (tabTARDES.Count > 0)
                        {
                            for (int x = 0; x < tabTARDES.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabTARDES.Item(x).InnerXml))
                                {
                                    Globals.glbRA_tardes = tabTARDES.Item(x).InnerXml.Trim();
                                }
                            }
                        }

                        XmlNodeList tabTRFCOD = xmlDoc.GetElementsByTagName("TRFCOD");
                        if (tabTRFCOD.Count > 0)
                        {
                            for (int x = 0; x < tabTRFCOD.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabTRFCOD.Item(x).InnerXml))
                                {
                                    Globals.glbRA_trfcod = tabTRFCOD.Item(x).InnerXml.Trim();
                                }
                            }
                        }

                        XmlNodeList tabACOCOD = xmlDoc.GetElementsByTagName("ACOCOD");
                        if (tabACOCOD.Count > 0)
                        {
                            for (int x = 0; x < tabACOCOD.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabACOCOD.Item(x).InnerXml))
                                {
                                    Globals.glbRA_acocod = tabACOCOD.Item(x).InnerXml.Trim();
                                }
                            }
                        }

                        XmlNodeList tabACODIG = xmlDoc.GetElementsByTagName("ACODIG");
                        if (tabACODIG.Count > 0)
                        {
                            for (int x = 0; x < tabACODIG.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabACODIG.Item(x).InnerXml))
                                {
                                    Globals.glbRA_acodig = tabACODIG.Item(x).InnerXml.Trim();
                                }
                            }
                        }

                        XmlNodeList tabRESCLISEG = xmlDoc.GetElementsByTagName("RESCLISEG");
                        if (tabRESCLISEG.Count > 0)
                        {
                            for (int x = 0; x < tabRESCLISEG.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabRESCLISEG.Item(x).InnerXml))
                                {
                                    Globals.glbRA_R_aCliSeg = tabRESCLISEG.Item(x).InnerXml.Trim();
                                }
                            }
                        }

                        XmlNodeList tabRESCANVEN = xmlDoc.GetElementsByTagName("RESCANVEN");
                        if (tabRESCANVEN.Count > 0)
                        {
                            for (int x = 0; x < tabRESCANVEN.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabRESCANVEN.Item(x).InnerXml))
                                {
                                    Globals.glbRA_R_aCanVen = tabRESCANVEN.Item(x).InnerXml.Trim();
                                }
                            }
                        }

                        XmlNodeList tabRESTRFNEW = xmlDoc.GetElementsByTagName("RESTRFNEW");
                        if (tabRESTRFNEW.Count > 0)
                        {
                            for (int x = 0; x < tabRESTRFNEW.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabRESTRFNEW.Item(x).InnerXml))
                                {
                                    Globals.glbRA_restrfnew = tabRESTRFNEW.Item(x).InnerXml.Trim();
                                }
                            }
                        }

                        XmlNodeList tabRESTIPPRT = xmlDoc.GetElementsByTagName("RESTIPPRT");
                        if (tabRESTIPPRT.Count > 0)
                        {
                            for (int x = 0; x < tabRESTIPPRT.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabRESTIPPRT.Item(x).InnerXml))
                                {
                                    Globals.glbRA_r_aprot = tabRESTIPPRT.Item(x).InnerXml.Trim();
                                }
                            }
                        }

                        XmlNodeList tabRESPROT = xmlDoc.GetElementsByTagName("RESPROT");
                        if (tabRESPROT.Count > 0)
                        {
                            for (int x = 0; x < tabRESPROT.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabRESPROT.Item(x).InnerXml))
                                {
                                    Globals.glbRA_r_aprotpot = tabRESPROT.Item(x).InnerXml.Trim();
                                }
                            }
                        }

                        XmlNodeList tabRESPRO = xmlDoc.GetElementsByTagName("RESPRO");
                        if (tabRESPRO.Count > 0)
                        {
                            for (int x = 0; x < tabRESPRO.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabRESPRO.Item(x).InnerXml))
                                {
                                    Globals.glbRA_r_aprtpa = tabRESPRO.Item(x).InnerXml.Trim();
                                }
                            }
                        }

                        XmlNodeList tabRESDSC = xmlDoc.GetElementsByTagName("RESDSC");
                        if (tabRESDSC.Count > 0)
                        {
                            for (int x = 0; x < tabRESDSC.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabRESDSC.Item(x).InnerXml))
                                {
                                    Globals.glbRA_r_adsc = tabRESDSC.Item(x).InnerXml.Trim();
                                }
                            }
                        }

                        XmlNodeList tabCLITARCOD = xmlDoc.GetElementsByTagName("CLITARCOD");
                        if (tabCLITARCOD.Count > 0)
                        {
                            for (int x = 0; x < tabCLITARCOD.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabCLITARCOD.Item(x).InnerXml))
                                {
                                    Globals.glbRA_clitarcod = tabCLITARCOD.Item(x).InnerXml.Trim();
                                }
                            }
                        }

                        XmlNodeList tabCLITARSEQ = xmlDoc.GetElementsByTagName("CLITARSEQ");
                        if (tabCLITARSEQ.Count > 0)
                        {
                            for (int x = 0; x < tabCLITARSEQ.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabCLITARSEQ.Item(x).InnerXml))
                                {
                                    Globals.glbRA_clitarseq = tabCLITARSEQ.Item(x).InnerXml.Trim();
                                }
                            }
                        }

                        XmlNodeList tabRESCODSML = xmlDoc.GetElementsByTagName("RESCODSML");
                        if (tabRESCODSML.Count > 0)
                        {
                            for (int x = 0; x < tabRESCODSML.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabRESCODSML.Item(x).InnerXml))
                                {
                                    Globals.glbRA_rescodsml = tabRESCODSML.Item(x).InnerXml.Trim();
                                }
                            }
                        }

                        XmlNodeList tabRESTIPSML = xmlDoc.GetElementsByTagName("RESTIPSML");
                        if (tabRESTIPSML.Count > 0)
                        {
                            for (int x = 0; x < tabRESTIPSML.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabRESTIPSML.Item(x).InnerXml))
                                {
                                    Globals.glbRA_restipsml = tabRESTIPSML.Item(x).InnerXml.Trim();
                                }
                            }
                        }

                        XmlNodeList tabRESTVCH = xmlDoc.GetElementsByTagName("RESTVCH");
                        if (tabRESTVCH.Count > 0)
                        {
                            for (int x = 0; x < tabRESTVCH.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabRESTVCH.Item(x).InnerXml))
                                {
                                    Globals.glbRA_ResTVch = tabRESTVCH.Item(x).InnerXml.Trim();
                                }
                            }
                        }

                        XmlNodeList tabRESEXPCLI = xmlDoc.GetElementsByTagName("RESEXPCLI");
                        if (tabRESEXPCLI.Count > 0)
                        {
                            for (int x = 0; x < tabRESEXPCLI.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabRESEXPCLI.Item(x).InnerXml))
                                {
                                    Globals.glbRA_ResExpCli = tabRESEXPCLI.Item(x).InnerXml.Trim();
                                }
                            }
                        }

                        XmlNodeList tabRESGRUPRO = xmlDoc.GetElementsByTagName("RESGRUPRO");
                        if (tabRESGRUPRO.Count > 0)
                        {
                            for (int x = 0; x < tabRESGRUPRO.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabRESGRUPRO.Item(x).InnerXml))
                                {
                                    Globals.glbRA_resgrupro = tabRESGRUPRO.Item(x).InnerXml.Trim();
                                }
                            }
                        }

                        XmlNodeList tabRESMOTADI = xmlDoc.GetElementsByTagName("RESMOTADI");
                        if (tabRESMOTADI.Count > 0)
                        {
                            for (int x = 0; x < tabRESMOTADI.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabRESMOTADI.Item(x).InnerXml))
                                {
                                    Globals.glbRA_resmotadi = tabRESMOTADI.Item(x).InnerXml.Trim();
                                }
                            }
                        }

                        XmlNodeList tabRESCODTAM = xmlDoc.GetElementsByTagName("RESCODTAM");
                        if (tabRESCODTAM.Count > 0)
                        {
                            for (int x = 0; x < tabRESCODTAM.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabRESCODTAM.Item(x).InnerXml))
                                {
                                    Globals.glbRA_rescodtam = tabRESCODTAM.Item(x).InnerXml.Trim();
                                }
                            }
                        }

                        XmlNodeList tabRESCUPCOD = xmlDoc.GetElementsByTagName("RESCUPCOD");
                        if (tabRESCUPCOD.Count > 0)
                        {
                            for (int x = 0; x < tabRESCUPCOD.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabRESCUPCOD.Item(x).InnerXml))
                                {
                                    Globals.glbRA_rescupcod = tabRESCUPCOD.Item(x).InnerXml.Trim();
                                }
                            }
                        }

                        XmlNodeList tabRESDIAAUT = xmlDoc.GetElementsByTagName("RESDIAAUT");
                        if (tabRESDIAAUT.Count > 0)
                        {
                            for (int x = 0; x < tabRESDIAAUT.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(tabRESDIAAUT.Item(x).InnerXml))
                                {
                                    Globals.glbRA_resdiaaut = tabRESDIAAUT.Item(x).InnerXml.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlGRUPAS = xmlDoc.GetElementsByTagName("GRUPAS");
                        if (xmlGRUPAS.Count > 0)
                        {
                            for (int x = 0; x < xmlGRUPAS.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlGRUPAS.Item(x).InnerXml))
                                {
                                    srvPassengerQuantity = xmlGRUPAS.Item(0).InnerXml;
                                    Globals.glbPassengerQuantity = srvPassengerQuantity.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlGRUBAG = xmlDoc.GetElementsByTagName("GRUBAG");
                        if (xmlGRUBAG.Count > 0)
                        {
                            for (int x = 0; x < xmlGRUBAG.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlGRUBAG.Item(x).InnerXml))
                                {
                                    srvBaggageQuantity = xmlGRUBAG.Item(0).InnerXml;
                                    Globals.glbBaggageQuantity = srvBaggageQuantity.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlGRUSIPP = xmlDoc.GetElementsByTagName("GRUSIPP");
                        if (xmlGRUSIPP.Count > 0)
                        {
                            for (int x = 0; x < xmlGRUSIPP.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlGRUSIPP.Item(x).InnerXml))
                                {
                                    srvVehicleCode = xmlGRUSIPP.Item(0).InnerXml;
                                    Globals.glbVehicleCode = srvVehicleCode.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlGRUDES = xmlDoc.GetElementsByTagName("GRUDES");
                        if (xmlGRUDES.Count > 0)
                        {
                            for (int x = 0; x < xmlGRUDES.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlGRUDES.Item(x).InnerXml))
                                {
                                    srvVehicleMakeModel = xmlGRUDES.Item(0).InnerXml;
                                    Globals.glbVehicleMakeModel = srvVehicleMakeModel.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlPictureURL = xmlDoc.GetElementsByTagName("GRUURL");
                        if (xmlPictureURL.Count > 0)
                        {
                            for (int x = 0; x < xmlPictureURL.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlPictureURL.Item(x).InnerXml))
                                {
                                    srvPictureURL = xmlPictureURL.Item(x).InnerXml;
                                    Globals.glbPictureURL = srvPictureURL.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlMOEISO = xmlDoc.GetElementsByTagName("MOEISO");
                        if (xmlMOEISO.Count > 0)
                        {
                            for (int x = 0; x < xmlMOEISO.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlMOEISO.Item(x).InnerXml))
                                {
                                    srvVehicleChargeCurrencyCode = xmlMOEISO.Item(x).InnerXml;
                                    Globals.glbVehicleChargeCurrencyCode = srvVehicleChargeCurrencyCode.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlCPFSEX = xmlDoc.GetElementsByTagName("CPFSEX");
                        if (xmlCPFSEX.Count > 0)
                        {
                            for (int x = 0; x < xmlCPFSEX.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlCPFSEX.Item(x).InnerXml))
                                {
                                    srvGender = xmlCPFSEX.Item(x).InnerXml;
                                    if (srvGender.Trim().ToUpper() == "F")
                                        srvGender = "female";
                                    else
                                        srvGender = "male";
                                    Globals.glbGender = srvGender.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlLOJNOM = xmlDoc.GetElementsByTagName("LOJNOM");
                        if (xmlLOJNOM.Count > 0)
                        {
                            for (int x = 0; x < xmlLOJNOM.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlLOJNOM.Item(x).InnerXml))
                                {
                                    srvLOJNOM = xmlLOJNOM.Item(x).InnerXml;
                                    Globals.glbLOJNOM = srvLOJNOM.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlLOJDES = xmlDoc.GetElementsByTagName("LOJDES");
                        if (xmlLOJDES.Count > 0)
                        {
                            for (int x = 0; x < xmlLOJDES.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlLOJDES.Item(x).InnerXml))
                                {
                                    srvLOJDES = xmlLOJDES.Item(x).InnerXml;
                                    Globals.glbLOJDES = srvLOJDES.Trim();
                                }
                            }
                        }


                        XmlNodeList xmlLOJNOMDEV = xmlDoc.GetElementsByTagName("LOJNOMDEV");
                        if (xmlLOJNOMDEV.Count > 0)
                        {
                            for (int x = 0; x < xmlLOJNOMDEV.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlLOJNOMDEV.Item(x).InnerXml))
                                {
                                    srvLOJNOMDEV = xmlLOJNOMDEV.Item(x).InnerXml;
                                    Globals.glbLOJNOMDEV = srvLOJNOMDEV.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlLOJDESDEV = xmlDoc.GetElementsByTagName("LOJDESDEV");
                        if (xmlLOJDESDEV.Count > 0)
                        {
                            for (int x = 0; x < xmlLOJDESDEV.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlLOJDESDEV.Item(x).InnerXml))
                                {
                                    srvLOJDESDEV = xmlLOJDESDEV.Item(x).InnerXml;
                                    Globals.glbLOJDESDEV = srvLOJDESDEV.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlRTBDIARIA = xmlDoc.GetElementsByTagName("RTBDIARIA");
                        if (xmlRTBDIARIA.Count > 0)
                        {
                            for (int x = 0; x < xmlRTBDIARIA.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlRTBDIARIA.Item(x).InnerXml))
                                {
                                    srvRTBDIARIA = xmlRTBDIARIA.Item(x).InnerXml;
                                    Globals.glbRTBDIARIA = srvRTBDIARIA.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlRTBDIAEXT = xmlDoc.GetElementsByTagName("RTBDIAEXT");
                        if (xmlRTBDIAEXT.Count > 0)
                        {
                            for (int x = 0; x < xmlRTBDIAEXT.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlRTBDIAEXT.Item(x).InnerXml))
                                {
                                    srvRTBDIAEXT = xmlRTBDIAEXT.Item(x).InnerXml;
                                    Globals.glbRTBDIAEXT = srvRTBDIAEXT.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlRTBHOREXT = xmlDoc.GetElementsByTagName("RTBHOREXT");
                        if (xmlRTBHOREXT.Count > 0)
                        {
                            for (int x = 0; x < xmlRTBHOREXT.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlRTBHOREXT.Item(x).InnerXml))
                                {
                                    srvRTBHOREXT = xmlRTBHOREXT.Item(x).InnerXml;
                                    Globals.glbRTBHOREXT = srvRTBHOREXT.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlRTBTOTDIA = xmlDoc.GetElementsByTagName("RTBTOTDIA");
                        if (xmlRTBTOTDIA.Count > 0)
                        {
                            for (int x = 0; x < xmlRTBTOTDIA.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlRTBTOTDIA.Item(x).InnerXml))
                                {
                                    srvRTBTOTDIA = xmlRTBTOTDIA.Item(x).InnerXml;
                                    Globals.glbRTBTOTDIA = srvRTBTOTDIA.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlRTBTOTDIE = xmlDoc.GetElementsByTagName("RTBTOTDIE");
                        if (xmlRTBTOTDIE.Count > 0)
                        {
                            for (int x = 0; x < xmlRTBTOTDIE.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlRTBTOTDIE.Item(x).InnerXml))
                                {
                                    srvRTBTOTDIE = xmlRTBTOTDIE.Item(x).InnerXml;
                                    Globals.glbRTBTOTDIE = srvRTBTOTDIE.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlRTBTOTHRE = xmlDoc.GetElementsByTagName("RTBTOTHRE");
                        if (xmlRTBTOTHRE.Count > 0)
                        {
                            for (int x = 0; x < xmlRTBTOTHRE.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlRTBTOTHRE.Item(x).InnerXml))
                                {
                                    srvRTBTOTHRE = xmlRTBTOTHRE.Item(x).InnerXml;
                                    Globals.glbRTBTOTHRE = srvRTBTOTHRE.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlRTBTOTDSC = xmlDoc.GetElementsByTagName("RTBTOTDSC");
                        if (xmlRTBTOTDSC.Count > 0)
                        {
                            for (int x = 0; x < xmlRTBTOTDSC.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlRTBTOTDSC.Item(x).InnerXml))
                                {
                                    srvRTBTOTDSC = xmlRTBTOTDSC.Item(x).InnerXml;
                                    Globals.glbRTBTOTDSC = srvRTBTOTDSC.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlRTBSUBTOT = xmlDoc.GetElementsByTagName("RTBSUBTOT");
                        if (xmlRTBSUBTOT.Count > 0)
                        {
                            for (int x = 0; x < xmlRTBSUBTOT.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlRTBSUBTOT.Item(x).InnerXml))
                                {
                                    srvRTBSUBTOT = xmlRTBSUBTOT.Item(x).InnerXml;
                                    Globals.glbRTBSUBTOT = srvRTBSUBTOT.Trim();
                                }
                            }
                        }

                        XmlNodeList xmlRTBTOTRES = xmlDoc.GetElementsByTagName("RTBTOTRES");
                        if (xmlRTBTOTRES.Count > 0)
                        {
                            for (int x = 0; x < xmlRTBTOTRES.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmlRTBTOTRES.Item(x).InnerXml))
                                {
                                    srvRTBTOTRES = xmlRTBTOTRES.Item(x).InnerXml;
                                    Globals.glbRTBTOTRES = srvRTBTOTRES.Trim().Replace(".", ",");
                                    Globals.glbRTBTOTRES_ANT = srvRTBTOTRES.Trim().Replace(".", ",");
                                }
                            }
                        }

                        XmlNodeList xmRTBVALDIA = xmlDoc.GetElementsByTagName("RTBVALDIA");
                        if (xmRTBVALDIA.Count > 0)
                        {
                            for (int x = 0; x < xmRTBVALDIA.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmRTBVALDIA.Item(x).InnerXml))
                                {
                                    srvRTBVALDIA = xmRTBVALDIA.Item(x).InnerXml;
                                    Globals.glbRTBVALDIA = srvRTBVALDIA.Trim();
                                }
                            }
                        }

                        XmlNodeList xmRTBVALFRQ = xmlDoc.GetElementsByTagName("RTBVALFRQ");
                        if (xmRTBVALFRQ.Count > 0)
                        {
                            for (int x = 0; x < xmRTBVALFRQ.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmRTBVALFRQ.Item(x).InnerXml))
                                {
                                    srvRTBVALFRQ = xmRTBVALFRQ.Item(x).InnerXml;
                                    Globals.glbRTBVALFRQ = srvRTBVALFRQ.Trim();
                                }
                            }
                        }

                        XmlNodeList xmRTBPERTXS = xmlDoc.GetElementsByTagName("RTBPERTXS");
                        if (xmRTBPERTXS.Count > 0)
                        {
                            for (int x = 0; x < xmRTBPERTXS.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmRTBPERTXS.Item(x).InnerXml))
                                {
                                    srvRTBPERTXS = xmRTBPERTXS.Item(x).InnerXml;
                                    Globals.glbRTBPERTXS = srvRTBPERTXS.Trim();
                                }
                            }
                        }

                        XmlNodeList xmRTBVALTXS = xmlDoc.GetElementsByTagName("RTBVALTXS");
                        if (xmRTBVALTXS.Count > 0)
                        {
                            for (int x = 0; x < xmRTBVALTXS.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmRTBVALTXS.Item(x).InnerXml))
                                {
                                    srvRTBVALTXS = xmRTBVALTXS.Item(x).InnerXml;
                                    Globals.glbRTBVALTXS = srvRTBVALTXS.Trim();
                                }
                            }
                        }

                        XmlNodeList xmRTBPERTAG = xmlDoc.GetElementsByTagName("RTBPERTAG");
                        if (xmRTBPERTAG.Count > 0)
                        {
                            for (int x = 0; x < xmRTBPERTAG.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmRTBPERTAG.Item(x).InnerXml))
                                {
                                    srvRTBPERTAG = xmRTBPERTAG.Item(x).InnerXml;
                                    Globals.glbRTBPERTAG = srvRTBPERTAG.Trim();
                                }
                            }
                        }

                        XmlNodeList xmRTBVALTAG = xmlDoc.GetElementsByTagName("RTBVALTAG");
                        if (xmRTBVALTAG.Count > 0)
                        {
                            for (int x = 0; x < xmRTBVALTAG.Count; x++)
                            {
                                if (!string.IsNullOrEmpty(xmRTBVALTAG.Item(x).InnerXml))
                                {
                                    srvRTBVALTAG = xmRTBVALTAG.Item(x).InnerXml;
                                    Globals.glbRTBVALTAG = srvRTBVALTAG.Trim();
                                }
                            }
                        }

                        if (Globals.glbRA_r_atipo.Trim().ToUpper().Contains("NCIA"))
                        {
                            Globals.glbRA_r_atipo = "A";
                        }
                        if (Globals.glbRA_r_atipo.Trim().ToUpper().Contains("FISICA"))
                        {
                            Globals.glbRA_r_atipo = "F";
                        }
                        if ((Globals.glbRA_r_atipo != "F") && (Globals.glbRA_r_atipo != "A"))
                        {
                            Globals.glbRA_r_atipo = "J";
                        }

                        /* 
                         
                         Sistema aceita pessoa fisica ou juridica
                         
                         */

                        return 0;

                    }
                    else
                    {
                        return 3;
                    }
                }
                else
                {
                    return 3;
                }
            }
            catch (Exception ex)
            {
                return 2;
            }
        }


        private int obtemReservaXML(string xmlEncodedList)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlEncodedList = xmlEncodedList.Replace("xmlns=\"http://www.opentravel.org/OTA/2003/05\"", "");
                xmlDoc.LoadXml(xmlEncodedList);

                string srvGivenName = "";
                string srvEmail = "";
                string srvVehReservation = "";
                string srvVoucherIdentifier = "";
                string srvVoucherValueType = "";
                string srvConfID = "";
                string srvCompanyShortName = "";
                string srvPickUpDateTime = "";
                string srvReturnDateTime = "";
                string srvDistUnitName = "";
                string srvPickUpLocation = "";
                string srvReturnLocation = "";
                string srvPassengerQuantity = "";
                string srvBaggageQuantity = "";
                string srvVehicleCode = "";
                string srvVehicleCodeContext = "";
                string srvVehicleMakeModel = "";
                string srvPictureURL = "";
                string srvVehicleChargeCurrencyCode = "";
                string srvVehicleChargeAmount = "";
                string srvVehicleChargeTaxInclusive = "";
                string srvVehicleChargeGuaranteedInd = "";
                string srvVehicleChargePurpose = "";
                string srvTelephone = "";
                string srvDocument = "";
                string srvNomeLojaCode = "";
                string srvNomeLojaNome = "";
                string srvNomeLojaCodeDev = "";
                string srvNomeLojaNomeDev = "";
                string srvDiaria = "";
                string srvDesconto = "";
                string srvDescontoValor = "";
                string srvRateTotalAmount = "";
                string srvDiariaUnitaria = "";

                Globals globalMetodo = new Globals();

                //Apenas reservas confirmadas serão processadas: ReservationStatus = "CFA"

                List<string> ReservationStatus = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation", "ReservationStatus");
                if (ReservationStatus.Count > 0) Globals.glbVehReservation = ReservationStatus[0].Trim();

                if (!string.IsNullOrEmpty(Globals.glbVehReservation))
                {
                    if (Globals.glbVehReservation.ToUpper() == "CFA")
                    {

                        Globals.glbMOTDES = "CONFIRMADA";
                        Globals.glbRESOPE = "WBS_WEB";

                        Globals.glbGivenName = globalMetodo.returnAttributesOneXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/ConfID/CompanyName", "CompanyShortName");

                        Globals.glbEmail = globalMetodo.returnInnerXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/Customer/Primary/Email");

                        List<string> VoucherIdentifier = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/Customer/Primary/PaymentForm/Voucher", "Identifier");
                        if (VoucherIdentifier.Count > 0) Globals.glbVoucherIdentifier = VoucherIdentifier[0].Trim();

                        List<string> VoucherValueType = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/Customer/Primary/PaymentForm/Voucher", "ValueType");
                        if (VoucherValueType.Count > 0) Globals.glbVoucherValueType = VoucherValueType[0].Trim();

                        List<string> ConfID = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/ConfID", "ID");
                        if (ConfID.Count > 0) Globals.glbConfID = ConfID[0].Trim();

                        List<string> CompanyShortName = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/ConfID/CompanyName", "CompanyShortName");
                        if (CompanyShortName.Count > 0) Globals.glbCompanyShortName = CompanyShortName[0].Trim();

                        List<string> CompanyNameCode = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/ConfID/CompanyName", "Code");
                        if (CompanyNameCode.Count > 0) Globals.glbCompanyNameCode = CompanyNameCode[0].Trim();

                        List<string> PickUpDateTime = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/VehRentalCore", "PickUpDateTime");
                        if (PickUpDateTime.Count > 0) Globals.glbPickUpDateTime = PickUpDateTime[0].Trim();

                        List<string> ReturnDateTime = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/VehRentalCore", "ReturnDateTime");
                        if (ReturnDateTime.Count > 0) Globals.glbReturnDateTime = ReturnDateTime[0].Trim();

                        List<string> DistUnitName = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/VehRentalCore", "DistUnitName");
                        if (DistUnitName.Count > 0) Globals.glbDistUnitName = DistUnitName[0].Trim();

                        List<string> PickUpLocation = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/VehRentalCore/PickUpLocation", "LocationCode");
                        if (PickUpLocation.Count > 0) Globals.glbPickUpLocation = PickUpLocation[0].Trim();

                        List<string> ReturnLocation = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/VehRentalCore/ReturnLocation", "LocationCode");
                        if (ReturnLocation.Count > 0) Globals.glbReturnLocation = ReturnLocation[0].Trim();

                        List<string> PassengerQuantity = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/Vehicle", "PassengerQuantity");
                        if (PassengerQuantity.Count > 0) Globals.glbPassengerQuantity = PassengerQuantity[0].Trim();

                        List<string> BaggageQuantity = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/Vehicle", "BaggageQuantity");
                        if (BaggageQuantity.Count > 0) Globals.glbBaggageQuantity = BaggageQuantity[0].Trim();

                        List<string> VehicleCode = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/Vehicle", "Code");
                        if (VehicleCode.Count > 0) Globals.glbVehicleCode = VehicleCode[0].Trim();

                        List<string> CodeContext = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/Vehicle", "CodeContext");
                        if (CodeContext.Count > 0) Globals.glbVehicleCodeContext = CodeContext[0].Trim();

                        List<string> VehMakeModelName = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/Vehicle/VehMakeModel", "Name");
                        if (VehMakeModelName.Count > 0) Globals.glbVehicleMakeModel = VehMakeModelName[0].Trim();

                        Globals.glbPictureURL = globalMetodo.returnInnerXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/Vehicle/PictureURL");

                        List<string> VehicleChargeCurrencyCode = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/RentalRate/VehicleCharges/VehicleCharge", "CurrencyCode");
                        if (VehicleChargeCurrencyCode.Count > 0) Globals.glbVehicleChargeCurrencyCode = VehicleChargeCurrencyCode[0].Trim();

                        List<string> VehicleChargeAmount = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/RentalRate/VehicleCharges/VehicleCharge", "Amount");
                        if (VehicleChargeAmount.Count > 0)
                        {
                            Globals.glbVehicleChargeAmount = VehicleChargeAmount[0].Trim();
                            Globals.glbResumoDiariasVar = string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDouble(VehicleChargeAmount[0].Trim().Replace(".", ",")));
                        }

                        List<string> VehicleChargeQuantity = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/RentalRate/VehicleCharges/VehicleCharge/Calculation", "Quantity");
                        if (VehicleChargeQuantity.Count > 0)
                        {
                            Globals.glbResumoDiariasQtd = VehicleChargeQuantity[0].Trim();
                            Globals.glbRTBDIARIA = VehicleChargeQuantity[0].Trim();
                        }

                        List<string> VehicleChargeUnitCharge = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/RentalRate/VehicleCharges/VehicleCharge/Calculation", "UnitCharge");
                        if (VehicleChargeUnitCharge.Count > 0)
                        {
                            Globals.glbRTBVALDIA = VehicleChargeUnitCharge[0].Trim();
                            Globals.glbRTBSUBTOT = VehicleChargeUnitCharge[0].Trim();
                            Globals.glbResumoDiariasUnit = string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDouble(VehicleChargeUnitCharge[0].Trim().Replace(".", ",")));
                        }

                        int nPricedEquip = 0;

                        List<string> PricedEquipDescription = globalMetodo.returnInnerListXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/PricedEquips/PricedEquip/Equipment/Description");
                        if (PricedEquipDescription.Count > 0)
                        {
                            nPricedEquip = PricedEquipDescription.Count;
                            Globals.glbPricedEquipDescription = new string[nPricedEquip];
                            for (int x = 0; x < nPricedEquip; x++)
                            {
                                Globals.glbPricedEquipDescription[x] = PricedEquipDescription[x].Trim();
                            }
                        }

                        List<string> PricedEquipChargeAmount = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/PricedEquips/PricedEquip/Charge", "Amount");
                        if (PricedEquipChargeAmount.Count > 0)
                        {
                            nPricedEquip = PricedEquipChargeAmount.Count;
                            Globals.glbPricedEquipChargeAmount = new string[nPricedEquip];
                            for (int x = 0; x < nPricedEquip; x++)
                            {
                                Globals.glbPricedEquipChargeAmount[x] = string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDouble(PricedEquipChargeAmount[x].Trim().Replace(".", ",")));
                            }
                        }

                        List<string> PricedEquipChargeUnitCharge = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/PricedEquips/PricedEquip/Charge/Calculation", "UnitCharge");
                        if (PricedEquipChargeUnitCharge.Count > 0)
                        {
                            nPricedEquip = PricedEquipChargeUnitCharge.Count;
                            Globals.glbPricedEquipChargeUnitCharge = new string[nPricedEquip];
                            for (int x = 0; x < nPricedEquip; x++)
                            {
                                Globals.glbPricedEquipChargeUnitCharge[x] = string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDouble(PricedEquipChargeUnitCharge[x].Trim().Replace(".", ",")));
                            }
                        }

                        List<string> PricedEquipChargeQuantity = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/PricedEquips/PricedEquip/Charge/Calculation", "Quantity");
                        if (PricedEquipChargeQuantity.Count > 0)
                        {
                            nPricedEquip = PricedEquipChargeQuantity.Count;
                            Globals.glbPricedEquipChargeQuantity = new string[nPricedEquip];
                            for (int x = 0; x < nPricedEquip; x++)
                            {
                                Globals.glbPricedEquipChargeQuantity[x] = PricedEquipChargeQuantity[x].Trim();
                            }
                        }

                        List<string> PricedOffLocServiceCharge = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentInfo/PricedOffLocService/Charge", "Amount");
                        if (PricedOffLocServiceCharge.Count > 0)
                        {
                            Globals.glbResumoDescontoVar = string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDouble(PricedOffLocServiceCharge[0].Trim().Replace(".", ",")));
                            Globals.glbResumoDescontoTotal = PricedOffLocServiceCharge[0].Trim();
                            Globals.glbRTBTOTDSC = PricedOffLocServiceCharge[0].Trim();
                        }

                        List<string> PricedOffLocServiceChargeCalculationQuantity = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentInfo/PricedOffLocService/Charge/Calculation", "Quantity");
                        if (PricedOffLocServiceChargeCalculationQuantity.Count > 0)
                        {
                            Globals.glbResumoDescontoQtd = PricedOffLocServiceChargeCalculationQuantity[0].Trim();
                        }

                        List<string> PricedOffLocServiceChargeCalculationPercentage = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentInfo/PricedOffLocService/Charge/Calculation", "Percentage");
                        if (PricedOffLocServiceChargeCalculationPercentage.Count > 0)
                        {
                            Globals.glbResumoDescontoPercentage = PricedOffLocServiceChargeCalculationPercentage[0].Trim();
                        }

                        decimal nValorDiaria = 0;
                        decimal nValorDesconto = 0;
                        decimal nValorUnit = 0;

                        List<string> PricedOffLocServiceChargeCalculationTotal = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentInfo/PricedOffLocService/Charge", "Amount");
                        if (PricedOffLocServiceChargeCalculationTotal.Count > 0)
                        {
                            int nResumoDiariasQtd = Convert.ToInt32(Globals.glbResumoDiariasQtd);

                            nValorDesconto = Convert.ToDecimal(PricedOffLocServiceChargeCalculationTotal[0].Trim().Replace(".", ","));
                            nValorDiaria = Convert.ToDecimal(Globals.glbVehicleChargeAmount.Trim().Replace(".", ","));
                            nValorDiaria = nValorDesconto + nValorDiaria;
                            nValorUnit = nValorDiaria / nResumoDiariasQtd;

                            Globals.glbResumoDiariasVar = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorDiaria);
                            Globals.glbResumoDiariasUnit = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorUnit);
                        }

                        List<string> LocationDetailsTelephone = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentInfo/LocationDetails/Telephone", "PhoneNumber");
                        if (LocationDetailsTelephone.Count > 0)
                        {
                            Globals.glbLOJTEL = LocationDetailsTelephone[0].Trim();
                        }

                        if (String.IsNullOrEmpty(Globals.glbRESDOCCLI))
                        {
                            List<string> DocumentDocID = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/Customer/Primary/Document", "DocID");
                            if (DocumentDocID.Count > 0)
                            {
                                Globals.glbRESDOCCLI = DocumentDocID[0].Trim();
                            }
                        }

                        List<string> FeeAmount = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/Fees/Fee", "Amount");
                        if (FeeAmount.Count > 0)
                        {
                            int nFeeAmount = FeeAmount.Count;
                            Globals.glbFeeAmount = new string[nFeeAmount];
                            for (int x = 0; x < nFeeAmount; x++)
                            {
                                Globals.glbFeeAmount[x] = string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDecimal(FeeAmount[x].Trim().Replace(".", ",")));
                            }
                        }

                        List<string> FeeAmountUnit = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/Fees/Fee/Calculation", "Quantity");
                        if (FeeAmountUnit.Count > 0)
                        {
                            int nFeeAmountUnit = FeeAmountUnit.Count;
                            Globals.glbFeeAmountUnit = new string[nFeeAmountUnit];
                            for (int x = 0; x < nFeeAmountUnit; x++)
                            {
                                Globals.glbFeeAmountUnit[x] = string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDecimal(FeeAmountUnit[x].Trim().Replace(".", ",")));
                            }
                        }

                        List<string> FeeUnitCharge = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/Fees/Fee/Calculation", "UnitCharge");
                        if (FeeUnitCharge.Count > 0)
                        {
                            int nFeeUnitCharge = FeeUnitCharge.Count;
                            Globals.glbFeeUnitCharge = new string[nFeeUnitCharge];
                            for (int x = 0; x < nFeeUnitCharge; x++)
                            {
                                Globals.glbFeeUnitCharge[x] = string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDecimal(FeeUnitCharge[x].Trim().Replace(".", ",")));
                            }
                        }

                        List<string> FeeAmountPercentage = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/Fees/Fee/Calculation", "Percentage");
                        if (FeeAmountPercentage.Count > 0)
                        {
                            int nFeeAmountPercentage = FeeAmountPercentage.Count;
                            Globals.glbFeeAmountPercentage = new string[nFeeAmountPercentage];
                            for (int x = 0; x < nFeeAmountPercentage; x++)
                            {
                                Globals.glbFeeAmountPercentage[x] = string.Format(new CultureInfo("pt-BR"), "{0:0%}", Convert.ToDecimal(FeeAmountPercentage[0].Trim().Replace(".", ",")) / 100);
                            }
                        }

                        List<string> LocationDetailsCode = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentInfo/LocationDetails", "Code");
                        if (LocationDetailsCode.Count > 0)
                        {
                            for (int x = 0; x < LocationDetailsCode.Count; x++)
                            {
                                if (x == 0) Globals.glbLOJNOM = LocationDetailsCode[x].Trim();
                                if (x == 1) Globals.glbLOJNOMDEV = LocationDetailsCode[x].Trim();
                            }
                        }

                        List<string> LocationDetailsName = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentInfo/LocationDetails", "Name");
                        if (LocationDetailsName.Count > 0)
                        {
                            for (int x = 0; x < LocationDetailsName.Count; x++)
                            {
                                if (x == 0) Globals.glbLOJDES = LocationDetailsName[x].Trim();
                                if (x == 1) Globals.glbLOJDESDEV = LocationDetailsName[x].Trim();
                            }
                        }

                        List<string> EstimatedTotalAmount = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/TotalCharge", "EstimatedTotalAmount");
                        if (EstimatedTotalAmount.Count > 0)
                        {
                        }

                        return 0;
                    }
                    else
                    {
                        if (Globals.glbVehReservation.ToUpper() == "NSH")
                        {
                            return 4;
                        }
                        else
                            return 3;
                    }
                }
                else
                {
                    return 1;
                }
            }
            catch (Exception ex)
            {
                return 2;
            }
        }

        private void ResetGeral()
        {
            txtReserva1.Text = "";
            pictLoad.Visible = false;
            tmrReserva.Enabled = false;
        }

        private void pictShutdown_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(1);
        }

        private void tmrReserva_Tick(object sender, EventArgs e)
        {

            LogTotem.InsertLog(new LogOperacao()
            {
                Application_Name = Globals.glbApplicationName,
                Application_Version = Globals.glbApplicationVersion,
                Local = Globals.glbLojaTotem,
                Operation_Ticks = Globals.glbOperationTicks,
                Operation_Name = "Informe Numero Reserva - Obtem Dados Complementares Reserva",
                Form = "FrmScreen02",
                RESNUM = String.IsNullOrEmpty(Globals.glbReservaNumeroPesquisa) ? 0 : Convert.ToInt32(Globals.glbReservaNumeroPesquisa),
                RANUM = String.IsNullOrEmpty(Globals.glbRA_Contrato) ? 0 : Convert.ToInt32(Globals.glbRA_Contrato)
            });

            tmrReserva.Enabled = false;
            Application.DoEvents();

            int iRetServico = 0;
            string strReserva = Globals.glbReservaNumeroPesquisa;

            try
            {
                // Obtem dados complementares da reserva
                CentralUnidasClassClient clientWcf = new CentralUnidasClassClient();

                string xmlRetornoWCF = clientWcf.GetReservaData(Convert.ToInt32(strReserva));
                if (string.IsNullOrEmpty(xmlRetornoWCF) || (xmlRetornoWCF.Trim() == "<UNIDASWCF />"))
                {
                    txtReserva1.Focus();
                    timerDisabled();

                    Globals.glbMensagem = 0203;

                    frmMensagem frmMsg = new frmMensagem();
                    frmMsg.Show();

                    ResetGeral();

                    this.Dispose();
                    return;
                }
                iRetServico = obtemReservaXMLviaWCF(xmlRetornoWCF);

                Globals.glbRA_r_adatret = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
                Globals.glbRA_r_ahorret = Globals.glbRA_r_adatret.Substring(11, 5);
                Globals.glbPickUpDateTime = Globals.glbRA_r_adatret;

                string strAcordo = clientWcf.GetAcordo(Convert.ToInt32(strReserva));
                strAcordo = strAcordo.Replace("[", "");
                strAcordo = strAcordo.Replace("]", "");

                if (!string.IsNullOrEmpty(strAcordo))
                {
                    dynamic data = JObject.Parse(strAcordo);
                    Globals.glbACOCOD = data.mACOCOD;
                    Globals.glbACOSEN = data.mACOSEN;
                    Globals.glbACOWBS = data.mACOWBS;
                    Globals.glbACWTAG = data.mACWTAG;
                }

                Application.DoEvents();

                /*
                 * 
                 Obtem informações do webservice Unidas - Reserva
                     
                 */

                WBS2Z_OTA_GDSSoapClient wsUnidas = new WBS2Z_OTA_GDSSoapClient();

                Application.DoEvents();

                OTA_VehRetResRQ paramOtaVehRetRes = new OTA_VehRetResRQ();
                OTA_VehRetResRQVehRetResRQCore paramVehRetResRQCore = new OTA_VehRetResRQVehRetResRQCore();
                UniqueID_Type[] paramUniqueID = new UniqueID_Type[1];
                paramUniqueID[0] = new UniqueID_Type();
                paramUniqueID[0].ID = strReserva.Trim();
                paramUniqueID[0].ID_Context = "";
                paramVehRetResRQCore.UniqueID = paramUniqueID;
                paramOtaVehRetRes.VehRetResRQCore = paramVehRetResRQCore;
                paramOtaVehRetRes.PrimaryLangID = "pt-BR";

                SourceType[] sourceType = new SourceType[1];
                sourceType[0] = new SourceType();

                SourceTypeRequestorID requestor = new SourceTypeRequestorID();
                CompanyNameType companyType = new CompanyNameType();
                companyType.CodeContext = Globals.glbACWTAG.Trim();

                // AQUI 10.02.2016 - DEBUG: Muitas tags pesquisadas na tabela do CodeContext podem estar em branco!

                if (!string.IsNullOrEmpty(Globals.glbACWTAG.Trim()))
                {
                    companyType.CodeContext = Globals.glbACWTAG.Trim();
                }
                //else
                //{
                //    companyType.CodeContext = "CR";
                //}
                // if (string.IsNullOrEmpty(companyType.CodeContext)) companyType.CodeContext = "PU";

                requestor.CompanyName = companyType;
                sourceType[0].RequestorID = requestor;

                paramOtaVehRetRes.POS = sourceType;

                Application.DoEvents();

                OtaVehRetResResult resOtaVehRetResResult = wsUnidas.OtaVehRetRes(paramOtaVehRetRes);
                if (resOtaVehRetResResult == null)
                {
                    txtReserva1.Focus();
                    timerDisabled();
                    Globals.glbMensagem = 0203;
                    frmMensagem frmMsg = new frmMensagem();
                    frmMsg.Show();
                    this.Close();
                    this.Dispose();
                    return;
                }

                Application.DoEvents();

                int nRegistros = resOtaVehRetResResult.Items.Count();
                if (nRegistros < 2)
                {
                    iRetServico = 3;
                }
                else
                {
                    if (!string.IsNullOrEmpty(strReserva))
                    {

                        Application.DoEvents();

                        Globals.glbReservaNumero = strReserva;

                        var xmlEncodedList = "";
                        var subReq = new OtaVehRetResResult();
                        subReq = resOtaVehRetResResult;

                        Application.DoEvents();

                        // Busca informações de reserva via XML
                        using (var stream = new MemoryStream())
                        {
                            using (var writerXml = XmlWriter.Create(stream))
                            {
                                new XmlSerializer(subReq.GetType()).Serialize(writerXml, subReq);
                                xmlEncodedList = Encoding.UTF8.GetString(stream.ToArray());
                            }
                        }
                        string _byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
                        if (xmlEncodedList.StartsWith(_byteOrderMarkUtf8))
                        {
                            xmlEncodedList = xmlEncodedList.Remove(0, _byteOrderMarkUtf8.Length);
                        }

                        // AQUI 10.02.2016 - DEBUG 'xmlEncodedList' contem o retorno do Webservice original da UNIDAS

                        Application.DoEvents();
                        iRetServico = obtemReservaXML(xmlEncodedList);
                    }
                    else
                    {
                        iRetServico = 1;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                txtReserva1.Focus();
                timerDisabled();
                Globals.glbMensagem = 1001;
                frmMensagem frmMsg = new frmMensagem();
                frmMsg.Show();
                this.Close();
                this.Dispose();
                return;
            }

            Application.DoEvents();

            if (!string.IsNullOrEmpty(Globals.glbR_ANUMRES.Trim()))
            {
                // AQUI VOLTAR 23/02/2016
                if (Convert.ToInt32(Globals.glbR_ANUMRES.Trim()) > 0)
                {
                    iRetServico = 5;
                }
            }

            // Volta foco para o textfield da Reserva
            txtReserva1.Focus();

            int nProtec = 0;

            switch (iRetServico)
            {
                // sucesso na obtenção das informações segue o fluxo
                case 0:

                    if (string.IsNullOrEmpty(Globals.glbCLIDOC_EXIST))
                    {
                        timerDisabled();
                        Globals.glbScreen = 1;
                        Globals.glbMensagem = 0205;
                        frmMensagem frmMsg = new frmMensagem();
                        frmMsg.Show();
                        this.Close();
                        this.Dispose();
                        return;
                    }

                    // obtem protecoes e acessorios
                    CentralUnidasClassClient clientWcf = new CentralUnidasClassClient();
                    string strNumeroReserva = Globals.glbConfID.Trim();
                    string xmlRetornoWCF = "";
                    xmlRetornoWCF = clientWcf.GetProtecoesReservaData(Convert.ToInt32(strNumeroReserva));

                    if (string.IsNullOrEmpty(xmlRetornoWCF) || (xmlRetornoWCF.Trim() == "<UNIDASWCF />"))
                    {
                        iRetServico = 99;
                    }
                    else
                    {
                        xmlRetornoWCF = xmlRetornoWCF.Replace("\r\n", "");
                        DataSet dtSet = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(xmlRetornoWCF);
                        dtSet.ReadXml(xmlSR);


                        int nAcess = 0;
                        int nRegistros = dtSet.Tables["Table"].Rows.Count;

                        Globals.glbSERV_RESLOJOPE = new string[nRegistros];
                        Globals.glbSERV_CATSRVCOD = new string[nRegistros];
                        Globals.glbSERV_CATSRVDSC = new string[nRegistros];
                        Globals.glbSERV_CATSRVTIP = new string[nRegistros];
                        Globals.glbSERV_CATSRVOTA = new string[nRegistros];
                        Globals.glbSERV_SRVCOD = new string[nRegistros];
                        Globals.glbSERV_CATSRVQFR = new string[nRegistros];
                        Globals.glbSERV_RESSRVCPF = new string[nRegistros];
                        Globals.glbSERV_RESSRVDAT = new string[nRegistros];
                        Globals.glbSERV_RESSRVVAL = new string[nRegistros];
                        Globals.glbSERV_RESSRVGRU = new string[nRegistros];
                        Globals.glbSERV_RESFRQVAL = new string[nRegistros];
                        Globals.glbSERV_RESQTDSRV = new string[nRegistros];
                        Globals.glbSERV_RESQTDFRQ = new string[nRegistros];
                        Globals.glbSERV_RESSRVTOT = new string[nRegistros];
                        Globals.glbSERV_RESFRQVALD = new string[nRegistros];
                        Globals.glbSERV_CATVALPAR = new string[nRegistros];

                        Globals.glbProtecoesValorPre1 = "0";

                        int nRow = 0;

                        foreach (DataRow dbRow in dtSet.Tables["Table"].Rows)
                        {

                            Globals.glbSERV_RESLOJOPE[nRow] = dbRow["RESLOJOPE"].ToString().Trim();
                            Globals.glbSERV_CATSRVCOD[nRow] = dbRow["CATSRVCOD"].ToString().Trim();
                            Globals.glbSERV_CATSRVDSC[nRow] = dbRow["CATSRVDSC"].ToString().Trim();
                            Globals.glbSERV_CATSRVTIP[nRow] = dbRow["CATSRVTIP"].ToString().Trim();
                            // Globals.glbSERV_CATSRVOTA[nRow] = dbRow["CATSRVOTA"].ToString().Trim();
                            Globals.glbSERV_SRVCOD[nRow] = dbRow["SRVCOD"].ToString().Trim();
                            Globals.glbSERV_CATSRVQFR[nRow] = dbRow["CATSRVQFR"].ToString().Trim();
                            Globals.glbSERV_RESSRVCPF[nRow] = dbRow["RESSRVCPF"].ToString().Trim();
                            Globals.glbSERV_RESSRVDAT[nRow] = dbRow["RESSRVDAT"].ToString().Trim();
                            Globals.glbSERV_RESSRVVAL[nRow] = dbRow["RESSRVVAL"].ToString().Trim();
                            Globals.glbSERV_RESSRVGRU[nRow] = dbRow["RESSRVGRU"].ToString().Trim();
                            Globals.glbSERV_RESFRQVAL[nRow] = dbRow["RESFRQVAL"].ToString().Trim();
                            Globals.glbSERV_RESQTDSRV[nRow] = dbRow["RESQTDSRV"].ToString().Trim();
                            Globals.glbSERV_RESQTDFRQ[nRow] = dbRow["RESQTDFRQ"].ToString().Trim();
                            Globals.glbSERV_RESSRVTOT[nRow] = dbRow["RESSRVTOT"].ToString().Trim();
                            Globals.glbSERV_RESFRQVALD[nRow] = dbRow["RESFRQVALD"].ToString().Trim();
                            Globals.glbSERV_CATVALPAR[nRow] = dbRow["CATVALPAR"].ToString().Trim();

                            string strVariavel = dbRow["CATSRVDSC"].ToString().Trim();

                            decimal nValorPre = Convert.ToDecimal(dbRow["CATVALPRE"].ToString().Trim().Replace(".", ","));
                            if (nValorPre > 0)
                            {
                                Globals.glbProtecoesValorPre1 = dbRow["CATVALPRE"].ToString().Trim().Replace(".", ",");
                            }

                            nRow++;

                            string strCATSRVDSC = dbRow["CATSRVTIP"].ToString().Trim();
                            if (strCATSRVDSC.ToUpper().Contains("PROTE"))
                            {
                                nProtec++;
                                if (nProtec < 6)
                                {
                                    if (nProtec == 1) Globals.glbProtecoesVar1 = dbRow["CATSRVDSC"].ToString().Trim();
                                    if (nProtec == 1) Globals.glbProtecoesCodVar1 = dbRow["CATSRVCOD"].ToString().Trim();
                                    if (nProtec == 2) Globals.glbProtecoesVar2 = dbRow["CATSRVDSC"].ToString().Trim();
                                    if (nProtec == 2) Globals.glbProtecoesCodVar2 = dbRow["CATSRVCOD"].ToString().Trim();
                                    if (nProtec == 3) Globals.glbProtecoesVar3 = dbRow["CATSRVDSC"].ToString().Trim();
                                    if (nProtec == 3) Globals.glbProtecoesCodVar3 = dbRow["CATSRVCOD"].ToString().Trim();
                                    if (nProtec == 4) Globals.glbProtecoesVar4 = dbRow["CATSRVDSC"].ToString().Trim();
                                    if (nProtec == 4) Globals.glbProtecoesCodVar4 = dbRow["CATSRVCOD"].ToString().Trim();
                                    if (nProtec == 5) Globals.glbProtecoesVar5 = dbRow["CATSRVDSC"].ToString().Trim();
                                    if (nProtec == 5) Globals.glbProtecoesCodVar5 = dbRow["CATSRVCOD"].ToString().Trim();

                                }
                            }
                            else
                            {
                                string strCSCOTAPRT = dbRow["CATSRVOTA"].ToString().Trim();
                                if (string.IsNullOrEmpty(strCSCOTAPRT)) strCSCOTAPRT = "0";
                                int iCSCOTAPRT = Convert.ToInt32(strCSCOTAPRT);
                                if (iCSCOTAPRT > 0)
                                {
                                    decimal numCATSRVDSC = Convert.ToDecimal(dbRow["RESSRVTOT"].ToString().Trim().Replace(".", ","));
                                    if (numCATSRVDSC > 0)
                                    {
                                        nAcess++;
                                        if (nAcess < 10)
                                        {
                                            if (nAcess == 1) Globals.glbAcessoriosVar1 = dbRow["CATSRVDSC"].ToString().Trim();
                                            if (nAcess == 2) Globals.glbAcessoriosVar2 = dbRow["CATSRVDSC"].ToString().Trim();
                                            if (nAcess == 3) Globals.glbAcessoriosVar3 = dbRow["CATSRVDSC"].ToString().Trim();
                                            if (nAcess == 4) Globals.glbAcessoriosVar4 = dbRow["CATSRVDSC"].ToString().Trim();
                                            if (nAcess == 5) Globals.glbAcessoriosVar5 = dbRow["CATSRVDSC"].ToString().Trim();
                                            if (nAcess == 6) Globals.glbAcessoriosVar6 = dbRow["CATSRVDSC"].ToString().Trim();
                                            if (nAcess == 7) Globals.glbAcessoriosVar7 = dbRow["CATSRVDSC"].ToString().Trim();
                                            if (nAcess == 8) Globals.glbAcessoriosVar8 = dbRow["CATSRVDSC"].ToString().Trim();
                                            if (nAcess == 9) Globals.glbAcessoriosVar9 = dbRow["CATSRVDSC"].ToString().Trim();
                                        }
                                    }
                                }
                                if (strCATSRVDSC.ToUpper().Contains("SERVI"))
                                {
                                    Globals.glbServicosCodVar1 = dbRow["CATSRVCOD"].ToString().Trim();
                                    Globals.glbServicosVar1 = dbRow["CATSRVDSC"].ToString().Trim();
                                }

                                if (strCATSRVDSC.ToUpper().Contains("RETORNO"))
                                {
                                    Globals.glbRetornoCodVar1 = dbRow["CATSRVCOD"].ToString().Trim();
                                    Globals.glbRetornoVar1 = dbRow["CATSRVDSC"].ToString().Trim();
                                }
                            }
                        }
                    }

                    // Permite sem proteção

                    //if (nProtec == 0)
                    //{
                    //    Globals.glbMensagem = 1018;
                    //    Globals.glbScreen = 1;
                    //    frmMensagem frmMsg1018 = new frmMensagem();
                    //    frmMsg1018.Show();
                    //    this.Close();
                    //    this.Dispose();
                    //    return;
                    //}

                    // Voucher valido aberto
                    bool bVoucherVerifica = false;

                    if (!string.IsNullOrEmpty(Globals.glbVoucherIdentifier))
                    {
                        if (Convert.ToInt32(Globals.glbVoucherIdentifier) > 0)
                        {
                            bVoucherVerifica = true;
                            string strVoucherValido = clientWcf.GetVoucherValido(Convert.ToInt32(strReserva));
                            strVoucherValido = strVoucherValido.Replace("[", "");
                            strVoucherValido = strVoucherValido.Replace("]", "");

                            if (!string.IsNullOrEmpty(strVoucherValido))
                            {
                                dynamic data = JObject.Parse(strVoucherValido);
                                string mRESGRUCOD = data.mRESGRUCOD;
                                string mRESTPVCH = data.mRESTPVCH;
                                string mVOUPRE = data.mVOUPRE;

                                if (!string.IsNullOrEmpty(mVOUPRE))
                                    Globals.glbVOUPRE = mVOUPRE.Trim();

                                if (string.IsNullOrEmpty(Globals.glbRA_ResTpvCh)) Globals.glbRA_ResTpvCh = mRESTPVCH.Trim();
                                if (string.IsNullOrEmpty(Globals.glbResGruCod)) Globals.glbResGruCod = mRESGRUCOD.Trim();
                                if (string.IsNullOrEmpty(mRESGRUCOD))
                                {
                                    timerDisabled();
                                    Globals.glbMensagem = 0204;
                                    frmMensagem frmMsg = new frmMensagem();
                                    frmMsg.Show();
                                    this.Close();
                                    this.Dispose();
                                    return;
                                }
                            }
                            else
                            {
                                Globals.glbVoucherIdentifier = "";
                            }
                        }
                        else
                        {
                            Globals.glbVoucherIdentifier = "";
                        }
                    }

                    if (string.IsNullOrEmpty(Globals.glbRESDOCCLI))
                    {
                        timerDisabled();
                        Globals.glbMensagem = 0205;
                        frmMensagem frmMsg = new frmMensagem();
                        frmMsg.Show();
                        this.Close();
                        this.Dispose();
                        return;
                    }

                    if (string.IsNullOrEmpty(Globals.glbDATA_CAP))
                    {
                        timerDisabled();
                        Globals.glbMensagem = 0205;
                        frmMensagem frmMsg = new frmMensagem();
                        frmMsg.Show();
                        this.Close();
                        this.Dispose();
                        return;
                    }

                    if (bVoucherVerifica)
                    {
                        if (string.IsNullOrEmpty(Globals.glbVoucherIdentifier))
                        {
                            timerDisabled();
                            Globals.glbScreen = 1;
                            Globals.glbMensagem = 1011;
                            frmMensagem frmMsg = new frmMensagem();
                            frmMsg.Show();
                            this.Close();
                            this.Dispose();
                            return;
                        }

                        if (string.IsNullOrEmpty(Globals.glbRA_ResTpvCh))
                        {
                            timerDisabled();
                            Globals.glbScreen = 1;
                            Globals.glbMensagem = 1011;
                            frmMensagem frmMsg = new frmMensagem();
                            frmMsg.Show();
                            this.Close();
                            this.Dispose();
                            return;
                        }

                        // Rotina retirada conforme email de Rodrigo Lodi em 26/02/2016 12:29
                        //if (Globals.glbRA_ResTpvCh.Trim().ToUpper() == "FPA")
                        //{
                        //    Globals.glbScreen = 1;
                        //    Globals.glbMensagem = 1011;
                        //    frmMensagem frmMsg = new frmMensagem();
                        //    frmMsg.Show();
                        //    this.Close();
                        //    this.Dispose();
                        //    return;
                        //}
                        //if (Globals.glbRA_ResTpvCh.Trim().ToUpper() == "INV03")
                        //{
                        //    Globals.glbScreen = 1;
                        //    Globals.glbMensagem = 1011;
                        //    frmMensagem frmMsg = new frmMensagem();
                        //    frmMsg.Show();
                        //    this.Close();
                        //    this.Dispose();
                        //    return;
                        //}

                    }

                    int nHorasAntes = Convert.ToInt32(ConfigurationManager.AppSettings.Get("horasReservaAntes"));
                    int nHorasDepois = Convert.ToInt32(ConfigurationManager.AppSettings.Get("horasReservaDepois"));

                    DateTime dataAtual = DateTime.Now;
                    DateTime dataReserva = DateTime.Now;

                    // Verifica retirada antecipada

                    if (!string.IsNullOrEmpty(Globals.glbPickUpDateTime))
                    {
                        dataReserva = DateTime.Parse(Globals.glbPickUpDateTime);
                        dataReserva = dataReserva.AddHours(nHorasAntes);
                    }

                    DateTime dtReservaOriginal = DateTime.Parse(Globals.glbPickUpDateTime);
                    TimeSpan span = dataAtual - dtReservaOriginal;
                    double nHours = span.TotalHours;

                    /*
                     
                     Debug: Sistema Totem Debug - AQUI 22.01.2016
                     * Evita bloqueio por data/hora
                     
                     */

                    // AQUI 10.02.2016 - DEBUG PARA PERMITIR CONSULTAR RESERVAS
                    // Verifica condição de retirada com uma hora antes
                    if (nHours < 0)
                    {
                        if (Math.Abs(nHours) > Math.Abs(nHorasAntes))
                        {
                            timerDisabled();
                            Globals.glbScreen = 1;
                            Globals.glbMensagem = 0206;
                            frmMensagem frmMsg = new frmMensagem();
                            frmMsg.Show();
                            this.Dispose();
                            return;
                        }
                    }
                    // Verifica retirada posterior a prevista
                    
                    /*
                     
                     Debug: Sistema Totem Debug - AQUI 22.01.2016
                     * Evita bloqueio por data/hora
                     
                     */

                    // AQUI 26.02.2016 - DEBUG PARA PERMITIR CONSULTAR RESERVAS
                    // Verifica condição de retirada com duas horas depois
                    if (nHours > 0)
                    {
                        if (Math.Abs(nHours) >= Math.Abs(nHorasDepois))
                        {
                            timerDisabled();
                            Globals.glbScreen = 1;
                            Globals.glbMensagem = 0206;
                            frmMensagem frmMsg = new frmMensagem();
                            frmMsg.Show();
                            this.Dispose();
                            return;
                        }
                    }

                    /*
                     
                     Comments: Inicio para obter loja
                     
                     */

                    string strHostName = System.Environment.MachineName.Trim();
                    string lojaUnidas = "";

                    xmlRetornoWCF = clientWcf.GetHostnameLojas(strHostName);

                    if (clientWcf != null) clientWcf.Close();
                    if (string.IsNullOrEmpty(xmlRetornoWCF) || (xmlRetornoWCF.Trim() == "<UNIDASWCF />"))
                    {
                        timerDisabled();
                        Globals.glbScreen = 1;
                        Globals.glbMensagem = 1012;
                        frmMensagem frmMsg = new frmMensagem();
                        frmMsg.Show();
                        this.Dispose();
                        return;
                    }
                    else
                    {
                        xmlRetornoWCF = xmlRetornoWCF.Replace("\r\n", "");
                        DataSet dtSet = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(xmlRetornoWCF);
                        dtSet.ReadXml(xmlSR);

                        int nRegistros = dtSet.Tables["Table"].Rows.Count;
                        foreach (DataRow dbRow in dtSet.Tables["Table"].Rows)
                        {
                            lojaUnidas = dbRow["PRASIG"].ToString().Trim();
                            Globals.glbPortaPinPad = dbRow["PORTPINPAD"].ToString().Trim();
                            Globals.glbLojMunCod = dbRow["MUNCOD"].ToString().Trim();
                        }
                    }

                    /*
                     
                     Comments: Fim para para obter loja
                     
                     */


                    /*
                     
                     Debug: Sistema Totem Debug - AQUI 22.01.2016
                     * Evita bloqueio por loja de retirada
                     
                     */

                    // AQUI 10.02.2016 - DEBUG PARA PERMITIR CONSULTAR RESERVA DE LOJA DIFERENTE

                    if (lojaUnidas.Trim() != Globals.glbPickUpLocation)
                    {
                        timerDisabled();
                        Globals.glbScreen = 1;
                        Globals.glbMensagem = 1012;
                        frmMensagem frmMsg = new frmMensagem();
                        frmMsg.Show();
                        this.Dispose();
                        return;
                    }

                    timerDisabled();
                    Globals.glbVezes = 0;
                    frmScreen03 frm03 = new frmScreen03();
                    frm03.Show();
                    this.Close();
                    this.Dispose();
                    break;
                // Erro tipo 0201
                case 1:
                    timerDisabled();
                    Globals.glbMensagem = 0201;
                    frmMensagem frmMsg0201 = new frmMensagem();
                    frmMsg0201.Show();
                    this.Close();
                    this.Dispose();
                    break;
                // Erro tipo 0202
                case 2:
                    timerDisabled();
                    Globals.glbMensagem = 0202;
                    frmMensagem frmMsg0202 = new frmMensagem();
                    frmMsg0202.Show();
                    this.Close();
                    this.Dispose();
                    break;
                // Erro tipo 0203
                case 3:
                    timerDisabled();
                    Globals.glbMensagem = 0203;
                    frmMensagem frmMsg0203 = new frmMensagem();
                    frmMsg0203.Show();
                    this.Close();
                    this.Dispose();
                    break;
                case 4:
                    timerDisabled();
                    Globals.glbMensagem = 0207;
                    frmMensagem frmMsg0207 = new frmMensagem();
                    frmMsg0207.Show();
                    this.Close();
                    this.Dispose();
                    break;
                case 5:
                    timerDisabled();
                    Globals.glbScreen = 1;
                    Globals.glbMensagem = 1009;
                    frmMensagem frmMsg1009 = new frmMensagem();
                    frmMsg1009.Show();
                    this.Close();
                    this.Dispose();
                    break;
                // Erro tipo 0202
                default:
                    timerDisabled();
                    Globals.glbMensagem = 0202;
                    frmMensagem frmMsg0202a = new frmMensagem();
                    frmMsg0202a.Show();
                    this.Close();
                    this.Dispose();
                    break;
            }
        }

        #region TecladoTotem

        private void pictTec_Q_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = txtReserva1.Text + "Q";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_W_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = txtReserva1.Text + "W";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_E_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = txtReserva1.Text + "E";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_R_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = txtReserva1.Text + "R";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_T_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = txtReserva1.Text + "T";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_Y_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = txtReserva1.Text + "Y";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_U_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = txtReserva1.Text + "U";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_I_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = txtReserva1.Text + "I";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_O_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = txtReserva1.Text + "O";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_P_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = txtReserva1.Text + "P";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_1_Click(object sender, EventArgs e)
        {
            if (txtReserva1.Text.Length < 8)
            {
                txtReserva1.Text = txtReserva1.Text + "1";
                txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
            }
        }

        private void pictTec_2_Click(object sender, EventArgs e)
        {
            if (txtReserva1.Text.Length < 8)
            {
                txtReserva1.Text = txtReserva1.Text + "2";
                txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
            }
        }

        private void pictTec_3_Click(object sender, EventArgs e)
        {
            if (txtReserva1.Text.Length < 8)
            {
                txtReserva1.Text = txtReserva1.Text + "3";
                txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
            }
        }

        private void pictTec_BACK1_Click(object sender, EventArgs e)
        {
            int nTamanho = txtReserva1.Text.Length;
            if (nTamanho > 0)
            {
                txtReserva1.Text = txtReserva1.Text.Substring(0, nTamanho - 1);
            }
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_A_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = txtReserva1.Text + "A";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_S_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = txtReserva1.Text + "S";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_D_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = txtReserva1.Text + "D";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_F_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = txtReserva1.Text + "F";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_G_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = txtReserva1.Text + "G";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_H_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = txtReserva1.Text + "H";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_J_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = txtReserva1.Text + "J";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_K_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = txtReserva1.Text + "K";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_L_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = txtReserva1.Text + "L";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_4_Click(object sender, EventArgs e)
        {
            if (txtReserva1.Text.Length < 8)
            {
                txtReserva1.Text = txtReserva1.Text + "4";
                txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
            }
        }

        private void pictTec_5_Click(object sender, EventArgs e)
        {
            if (txtReserva1.Text.Length < 8)
            {
                txtReserva1.Text = txtReserva1.Text + "5";
                txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
            }
        }

        private void pictTec_6_Click(object sender, EventArgs e)
        {
            if (txtReserva1.Text.Length < 8)
            {
                txtReserva1.Text = txtReserva1.Text + "6";
                txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
            }
        }

        private void pictTec_Z_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = txtReserva1.Text + "Z";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_X_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = txtReserva1.Text + "X";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_C_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = txtReserva1.Text + "C";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_V_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = txtReserva1.Text + "V";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_B_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = txtReserva1.Text + "B";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_N_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = txtReserva1.Text + "N";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_M_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = txtReserva1.Text + "M";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_VIRGULA_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = txtReserva1.Text + ",";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_PONTO_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = txtReserva1.Text + ".";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_7_Click(object sender, EventArgs e)
        {
            if (txtReserva1.Text.Length < 8)
            {
                txtReserva1.Text = txtReserva1.Text + "7";
                txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
            }
        }

        private void pictTec_8_Click(object sender, EventArgs e)
        {
            if (txtReserva1.Text.Length < 8)
            {
                txtReserva1.Text = txtReserva1.Text + "8";
                txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
            }
        }

        private void pictTec_9_Click(object sender, EventArgs e)
        {
            if (txtReserva1.Text.Length < 8)
            {
                txtReserva1.Text = txtReserva1.Text + "9";
                txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
            }
        }

        private void pictTec_ESPACO_Click(object sender, EventArgs e)
        {
            if (txtReserva1.Text.Length < 8)
            {
                txtReserva1.Text = txtReserva1.Text + " ";
                txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
            }
        }

        private void pictTec_LIMPA_Click(object sender, EventArgs e)
        {
            txtReserva1.Text = "";
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        private void pictTec_0_Click(object sender, EventArgs e)
        {
            if (txtReserva1.Text.Length < 8)
            {
                txtReserva1.Text = txtReserva1.Text + "0";
                txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
            }
        }

        private void pictTec_BACK2_Click(object sender, EventArgs e)
        {
            int nTamanho = txtReserva1.Text.Length;
            if (nTamanho > 0)
            {
                txtReserva1.Text = txtReserva1.Text.Substring(0, nTamanho - 1);
            }
            txtReserva1.SelectionStart = txtReserva1.Text.Length + 1;
        }

        #endregion TecladoTotem

        private void btnContinuar_Click(object sender, EventArgs e)
        {
            Globals.glbOperationTicks = DateTime.Now.Ticks;
            Verifica_Reserva();
            tmrReturn.Enabled = false;
            Application.DoEvents();

        }

        private void Verifica_Reserva()
        {
            string strReservaFinal = "";
            strReservaFinal = txtReserva1.Text.Trim();

            // Saida do sistema via numero de reserva
            if (strReservaFinal == "91919199")
            {
                System.Environment.Exit(1);
            }

            if (string.IsNullOrEmpty(strReservaFinal))
            {
                txtReserva1.Focus();
                timerDisabled();
                Globals.glbMensagem = 0201;
                frmMensagem frmMsg = new frmMensagem();
                frmMsg.Show();
                this.Close();
                this.Dispose();
                //ResetGeral();
                return;
            }

            string strReserva = String.Format("{0:0}", Convert.ToDouble(strReservaFinal));
            Globals.glbReservaNumeroPesquisa = strReserva;
            pictLoad.Visible = true;
            tmrReserva.Enabled = true;

            Application.DoEvents();
        }

        private void pictTec_ENTER1_Click(object sender, EventArgs e)
        {
            tmrReturn.Enabled = false;
            Verifica_Reserva();
            Application.DoEvents();
        }

        private void pictTec_ENTER2_Click(object sender, EventArgs e)
        {
            tmrReturn.Enabled = false;
            Verifica_Reserva();
            Application.DoEvents();
        }

        private void pictTec_TECLADO_Click(object sender, EventArgs e)
        {
            // tmrTeclado.Enabled = true;
        }

        private void tmrTeclado_Tick(object sender, EventArgs e)
        {
            tmrTeclado.Enabled = false;
            Teclado_Hide();
        }

        private void tmrTecladoShow_Tick(object sender, EventArgs e)
        {
            tmrTecladoShow.Enabled = false;
            Teclado_Show();
        }

        private void Teclado_Hide()
        {
            int nTarget = pictTecladoFundo.Top + pictTecladoFundo.Height + 100;
            int x = pictTecladoFundo.Top;
            // Primeira fileira
            int tec1 = pictTec_1.Top - x;
            int tec2 = pictTec_2.Top - x;
            int tec3 = pictTec_3.Top - x;

            // Segunda fileira
            int tec4 = pictTec_4.Top - x;
            int tec5 = pictTec_5.Top - x;
            int tec6 = pictTec_6.Top - x;

            // Terceira fileira
            int tec7 = pictTec_7.Top - x;
            int tec8 = pictTec_8.Top - x;
            int tec9 = pictTec_9.Top - x;

            // Quarta fileira
            int tecTECLADO = pictTec_TECLADO.Top - x;
            int tec0 = pictTec_0.Top - x;
            int tecBACK2 = pictTec_BACK2.Top - x;

            while (x < nTarget)
            {

                pictTecladoFundo.Top = x;

                // PRIMEIRA FILEIRA
                pictTec_1.Top = tec1 + x;
                pictTec_2.Top = tec2 + x;
                pictTec_3.Top = tec3 + x;

                // SEGUNDA FILEIRA
                pictTec_4.Top = tec4 + x;
                pictTec_5.Top = tec5 + x;
                pictTec_6.Top = tec6 + x;

                // TERCEIRA FILEIRA
                pictTec_7.Top = tec7 + x;
                pictTec_8.Top = tec8 + x;
                pictTec_9.Top = tec9 + x;

                // QUARTA FILEIRA
                pictTec_TECLADO.Top = tecTECLADO + x;
                pictTec_0.Top = tec0 + x;
                pictTec_BACK2.Top = tecBACK2 + x;

                this.Refresh();
                x = x + 100;
            }
        }

        private void Teclado_Show()
        {
            int nTarget = this.Height - pictTecladoFundo.Height - 20;
            int x = pictTecladoFundo.Top;
            // Primeira fileira
            int tec1 = pictTec_1.Top - x;
            int tec2 = pictTec_2.Top - x;
            int tec3 = pictTec_3.Top - x;

            // Segunda fileira
            int tec4 = pictTec_4.Top - x;
            int tec5 = pictTec_5.Top - x;
            int tec6 = pictTec_6.Top - x;

            // Terceira fileira
            int tec7 = pictTec_7.Top - x;
            int tec8 = pictTec_8.Top - x;
            int tec9 = pictTec_9.Top - x;

            // Quarta fileira
            int tecTECLADO = pictTec_TECLADO.Top - x;
            int tec0 = pictTec_0.Top - x;
            int tecBACK2 = pictTec_BACK2.Top - x;

            while (x > nTarget)
            {

                pictTecladoFundo.Top = x;

                // PRIMEIRA FILEIRA
                pictTec_1.Top = tec1 + x;
                pictTec_2.Top = tec2 + x;
                pictTec_3.Top = tec3 + x;

                // SEGUNDA FILEIRA
                pictTec_4.Top = tec4 + x;
                pictTec_5.Top = tec5 + x;
                pictTec_6.Top = tec6 + x;

                // TERCEIRA FILEIRA
                pictTec_7.Top = tec7 + x;
                pictTec_8.Top = tec8 + x;
                pictTec_9.Top = tec9 + x;

                // QUARTA FILEIRA
                pictTec_TECLADO.Top = tecTECLADO + x;
                pictTec_0.Top = tec0 + x;
                pictTec_BACK2.Top = tecBACK2 + x;

                this.Refresh();
                x = x - 100;
            }
        }

        private void pictTecladoShow_Click(object sender, EventArgs e)
        {
            tmrTecladoShow.Enabled = true;
        }

        private void tmrReturn_Tick(object sender, EventArgs e)
        {
            tmrReturn.Enabled = false;
            timerDisabled();
            frmScreen01 frm1 = new frmScreen01();
            frm1.Show();
            this.Close();
            this.Dispose();
        }

        private void AtualizarInterfaceParaIdiomaCorrente()
        {

            MontarResourceSetParaIdiomaCorrente();

            btnContinuar.Text = ResxSetForm.GetString("btnContinuar.Text");
            butIdioma.Text = ResxSetForm.GetString("butIdioma.Text");
            lblInformeDadasAbaixo.Text = ResxSetForm.GetString("lblInformeDadasAbaixo.Text");
            lblNumeroReserva.Text = ResxSetForm.GetString("lblNumeroReserva.Text");
            this.Icon = ((System.Drawing.Icon)(ResxSetForm.GetObject("$this.Icon")));

            if (Globals.glbLanguage == 1) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_Brasil_botao;
            if (Globals.glbLanguage == 2) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_Espanha_botao;
            if (Globals.glbLanguage == 3) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_UK_botao;

            butIdioma.Refresh();
        }

        private void MontarResourceSetParaIdiomaCorrente()
        {
            string srtCaminhoExeTotem = System.IO.Path.GetDirectoryName(Application.ExecutablePath.ToString());
            // Português
            if (Globals.glbLanguage == 1)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen02.pt.resx");
            }
            //Espanhol
            else if (Globals.glbLanguage == 2)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen02.es.resx");
            }
            
            // Inglês
            else if (Globals.glbLanguage == 3)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen02.en.resx");              
            }
        }

        private void butIdioma_Click(object sender, EventArgs e)
        {
            frmIdioma frmIdi = new frmIdioma();
            frmIdi.ShowDialog();
            AtualizarInterfaceParaIdiomaCorrente();
        }

        private void timerDisabled()
        {
            tmrReturn.Enabled = false;
            tmrReserva.Enabled = false;
            tmrTeclado.Enabled = false;
            tmrTecladoShow.Enabled = false;
        }
    }
}
