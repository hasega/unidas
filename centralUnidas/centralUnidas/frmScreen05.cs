﻿// Aplicação: Unidas Totem
// Data     : 01/10/2015
// Versão   : 1.0
// Empresa  : Unidas Rent a Car
// Equipe   : Unidas Desenvolvimento
// Objetivo : Seleção de grupo de veiculo
// Histórico: 
//      01/10/2015: Inicio de desenvolvimento para acesso geral
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using centralUnidas.centralUnidasWCF;
using System.Xml;
using System.Resources;

namespace centralUnidas
{
    public partial class frmScreen05 : Form
    {
        private bool bEscolheu = false;

        public ResXResourceSet ResxSetForm; 

        public frmScreen05()
        {
            InitializeComponent();
        }

        private void frmScreen05_Load(object sender, EventArgs e)
        {
            try
            {


                LogTotem.InsertLog(new LogOperacao()
                {
                    Application_Name = Globals.glbApplicationName,
                    Application_Version = Globals.glbApplicationVersion,
                    Local = Globals.glbLojaTotem,
                    Operation_Ticks = Globals.glbOperationTicks,
                    Operation_Name = "Seleção de Carro do Grupo",
                    Form = "FrmScreen05",
                    RESNUM = String.IsNullOrEmpty(Globals.glbReservaNumeroPesquisa) ? 0 : Convert.ToInt32(Globals.glbReservaNumeroPesquisa),
                    RANUM = String.IsNullOrEmpty(Globals.glbRA_Contrato) ? 0 : Convert.ToInt32(Globals.glbRA_Contrato)
                });
                Globals.glbScreen = 5;
                int nScreenWidth = this.Width;
                int nScreenWidthSplit = nScreenWidth / 2;

                pnlVeiculo1.Visible = false;
                pnlVeiculo2.Visible = false;
                pnlVeiculo3.Visible = false;
                pnlVeiculo4.Visible = false;

                pictVeiculo1.Image = centralUnidas.Properties.Resources.GrupoIndisponivel;
                pictVeiculo1.Refresh();

                pictVeiculo2.Image = centralUnidas.Properties.Resources.GrupoIndisponivel;
                pictVeiculo2.Refresh();

                pictVeiculo3.Image = centralUnidas.Properties.Resources.GrupoIndisponivel;
                pictVeiculo3.Refresh();

                pictVeiculo4.Image = centralUnidas.Properties.Resources.GrupoIndisponivel;
                pictVeiculo4.Refresh();

                this.Refresh();

                AtualizarInterfaceParaIdiomaCorrente();

                this.Refresh();

                Globals.glbDebugMode = Convert.ToInt16(ConfigurationManager.AppSettings.Get("appDebugMode"));
                Shutdown_Enable();

                lblSelecioneGrupo2.Text = "para o Grupo: " + Globals.glbResGruCod.Trim() + " - " + Globals.glbVehicleMakeModel.Trim() + ":";

                

                int iRetServico = ConsultaGrupo();

                

                Application.DoEvents();
                switch (iRetServico)
                {
                    // sucesso na obtenção das informações segue o fluxo
                    case 0:
                        break;
                    // Erro tipo 0201
                    case 1:
                        timerDisabled();
                        Globals.glbMensagem = 0201;
                        frmMensagem frmMsg0201 = new frmMensagem();
                        frmMsg0201.Show();
                        this.Close();
                        this.Dispose();
                        break;
                    // Erro tipo 0202
                    case 2:
                        timerDisabled();
                        Globals.glbMensagem = 0202;
                        frmMensagem frmMsg0202 = new frmMensagem();
                        frmMsg0202.Show();
                        this.Close();
                        this.Dispose();
                        break;
                    // Erro tipo 0203
                    case 3:
                        timerDisabled();
                        Globals.glbMensagem = 0203;
                        frmMensagem frmMsg0203 = new frmMensagem();
                        frmMsg0203.Show();
                        this.Close();
                        this.Dispose();
                        break;
                    case 4:
                        timerDisabled();
                        Globals.glbScreen = 1;
                        Globals.glbMensagem = 1010;
                        frmMensagem frmMsg1010 = new frmMensagem();
                        frmMsg1010.Show();
                        this.Close();
                        this.Dispose();
                        break;
                    // Erro tipo 0202
                    default:
                        timerDisabled();
                        Globals.glbMensagem = 0202;
                        frmMensagem frmMsg0202a = new frmMensagem();
                        frmMsg0202a.Show();
                        this.Close();
                        this.Dispose();
                        break;
                }
            }
            catch (Exception ex)
            {
                timerDisabled();
                Globals.glbMensagem = 1001;
                frmMensagem frmMsg = new frmMensagem();
                frmMsg.Show();
                this.Close();
                this.Dispose();
                return;
            }
        }

        private void Shutdown_Enable()
        {
            if (Globals.glbDebugMode == 0)
                pictShutdown.Visible = false;

            else
                pictShutdown.Visible = true;

            pictShutdown.Refresh();
        }

        private int ConsultaGrupo()
        {
            int iRetServico = 0;
            string xmlRetornoWCF = "";
            try
            {
                CentralUnidasClassClient clientWcf = new CentralUnidasClassClient();

                string strResGruCod = Globals.glbResGruCod;
                string strGrusipp = "";
                string strPickUpLocation = Globals.glbPickUpLocation;
                int iCanVenCod = 0;
                string strPickUpDateTime = Globals.glbPickUpDateTime;
                string stReturnDateTime = Globals.glbReturnDateTime;
                int iAcoCod = 0;
                int iAcoDig = 0;

                strPickUpDateTime = strPickUpDateTime.Replace("T", " ");
                stReturnDateTime = stReturnDateTime.Replace("T", " ");

                xmlRetornoWCF = clientWcf.GetGrupoData(strResGruCod, strGrusipp, strPickUpLocation, iCanVenCod, strPickUpDateTime, stReturnDateTime, iAcoCod, iAcoDig);
                if (string.IsNullOrEmpty(xmlRetornoWCF) || (xmlRetornoWCF.Trim() == "<UNIDASWCF />"))
                {
                    iRetServico = 4;
                }
                else
                {
                    iRetServico = obtemGrupoXMLviaWCF(xmlRetornoWCF);
                    if (string.IsNullOrEmpty(Globals.glbResGruCod))
                    {
                        if (!string.IsNullOrEmpty(Globals.glbGRUCOD))
                        {
                            Globals.glbResGruCod = Globals.glbGRUCOD.Trim();
                        }
                    }


                    if (iRetServico == 0)
                    {
                        int intLanguage = Globals.glbLanguage;
                        switch (intLanguage)
                        {
                            case 1:
                                // Portugues
                                lblSelecioneGrupo1.Text = "Selecione um dos veículos disponíveis";
                                lblSelecioneGrupo2.Text = "para o Grupo " + Globals.glbGRUCOD.Trim() + " - " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Globals.glbGRUDES.ToLower()) + ":";
                                lblSelecioneGrupo2.Refresh();
                                lblSelecioneGrupo1.Refresh();
                                break;
                            case 2:
                                // Espanhol
                                lblSelecioneGrupo1.Text = "Seleccione uno de los modelos";
                                lblSelecioneGrupo2.Text = "para el Grupo " + Globals.glbGRUCOD.Trim() + " - " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Globals.glbGRUDES.ToLower()) + ":";
                                lblSelecioneGrupo2.Visible = true;
                                lblSelecioneGrupo2.Refresh();
                                lblSelecioneGrupo1.Refresh();
                                break;
                            case 3:
                                // Ingles
                                lblSelecioneGrupo1.Text = "Choose one of the vehicles ";
                                lblSelecioneGrupo2.Text = "for the Group " + Globals.glbGRUCOD.Trim() + " - " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Globals.glbGRUDES.ToLower()) + ":";
                                lblSelecioneGrupo2.Visible = true;
                                lblSelecioneGrupo2.Refresh();
                                lblSelecioneGrupo1.Refresh();
                                break;
                            default:
                                // Portugues
                                lblSelecioneGrupo1.Text = "Selecione um dos veículos disponíveis";
                                lblSelecioneGrupo2.Text = "para o Grupo " + Globals.glbGRUCOD.Trim() + " - " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Globals.glbGRUDES.ToLower()) + ":";
                                lblSelecioneGrupo2.Visible = true;
                                lblSelecioneGrupo2.Refresh();
                                lblSelecioneGrupo1.Refresh();
                                break;
                        }
                        xmlRetornoWCF = clientWcf.GetModeloGrupoData(strResGruCod, strPickUpLocation);
                        if (string.IsNullOrEmpty(xmlRetornoWCF) || (xmlRetornoWCF.Trim() == "<UNIDASWCF />"))
                        {
                            iRetServico = 4;
                        }
                        else
                        {

                            xmlRetornoWCF = xmlRetornoWCF.Replace("\r\n", "");

                            DataSet dtSet = new DataSet();
                            System.IO.StringReader xmlSR = new System.IO.StringReader(xmlRetornoWCF);

                            dtSet.ReadXml(xmlSR);

                            int nIndice = 0;
                            string strTipo = "";
                            foreach (DataRow dbRow in dtSet.Tables["Table"].Rows)
                            {
                                switch (nIndice)
                                {
                                    case 0:
                                        lblModeloPNL1.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(dbRow["ModDes"].ToString().Trim().ToLower());
                                        lblDescricaoPNL1.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(dbRow["TpmDes"].ToString().Trim().ToLower());

                                        strTipo = lblModeloPNL1.Text.Trim().ToUpper();

                                        pictVeiculo1.Image = centralUnidas.Properties.Resources.GrupoIndisponivel;

                                        if (strTipo.Contains("A4")) pictVeiculo1.Image = centralUnidas.Properties.Resources.AudiA4;
                                        if (strTipo.Contains("C3")) pictVeiculo1.Image = centralUnidas.Properties.Resources.C3Picasso;
                                        if (strTipo.Contains("CLASSIC")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Classic34;
                                        if (strTipo.Contains("UNO")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Unobranco4portas;
                                        if (strTipo.Contains("PALIO")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Palio2portasmerged;
                                        if (strTipo.Contains("ATTRACTIVE")) pictVeiculo1.Image = centralUnidas.Properties.Resources.FiatPalioAttractive;
                                        if (strTipo.Contains("ADVENTURE")) pictVeiculo1.Image = centralUnidas.Properties.Resources.FiatStradaAdventure2014;
                                        if (strTipo.Contains("STRADA")) pictVeiculo1.Image = centralUnidas.Properties.Resources.FiatStradaAdventure2014;
                                        if (strTipo.Contains("PUNTO")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Puntomerged;
                                        if (strTipo.Contains("WORKING")) pictVeiculo1.Image = centralUnidas.Properties.Resources.FiatStradaWorking2014DIR;
                                        if (strTipo.Contains("FIESTA")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Fiesta3_4SedanFly;
                                        if (strTipo.Contains("FOCUS")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Ford_Focus_Sedan2014;
                                        if (strTipo.Contains("SEDAN 2.0 16V Glx")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Focus3_4sedanGlx;
                                        if (strTipo.Contains("FUSION")) pictVeiculo1.Image = centralUnidas.Properties.Resources.FordFusion2014;
                                        if (strTipo.Contains("KA")) pictVeiculo1.Image = centralUnidas.Properties.Resources.FORDKA3_42014;
                                        if (strTipo.Contains("RANGER")) pictVeiculo1.Image = centralUnidas.Properties.Resources.FORDRANGER;
                                        if (strTipo.Contains("ONIX")) pictVeiculo1.Image = centralUnidas.Properties.Resources.GMBRAOnix_3_4;
                                        if (strTipo.Contains("PRISMA")) pictVeiculo1.Image = centralUnidas.Properties.Resources.GMBRAPrisma3_4;
                                        if (strTipo.Contains("CRUZE")) pictVeiculo1.Image = centralUnidas.Properties.Resources.GMCruzeSedan2014;
                                        if (strTipo.Contains("GOL")) pictVeiculo1.Image = centralUnidas.Properties.Resources.GolG6VW;
                                        if (strTipo.Contains("G6")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Gol_G6_VW;
                                        if (strTipo.Contains("G5")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Gol_G5_VW;
                                        if (strTipo.Contains("LINEA")) pictVeiculo1.Image = centralUnidas.Properties.Resources.LineaLX1_8FlexMerged;
                                        if (strTipo.Contains("MARCH")) pictVeiculo1.Image = centralUnidas.Properties.Resources.March_SL_Prata;
                                        if (strTipo.Contains("TRITON")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Mitsubishil200_triton;
                                        if (strTipo.Contains("FRONTIER")) pictVeiculo1.Image = centralUnidas.Properties.Resources.NissanFrontier;
                                        if (strTipo.Contains("LIVINA")) pictVeiculo1.Image = centralUnidas.Properties.Resources.NissanLivina1;
                                        if (strTipo.Contains("GRAND LIVINA")) pictVeiculo1.Image = centralUnidas.Properties.Resources.NissanGrandLivina;
                                        if (strTipo.Contains("SENTRA")) pictVeiculo1.Image = centralUnidas.Properties.Resources.NissanSentra;
                                        if (strTipo.Contains("408")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Peugeot408;
                                        if (strTipo.Contains("POLO")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Polo3_4hatch;
                                        if (strTipo.Contains("KANGOO")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Renault_KangooVU_ComPorta;
                                        if (strTipo.Contains("LOGAN")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Renault_Logan_2014;
                                        if (strTipo.Contains("SANDERO")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Renault_Sandero_2015;
                                        if (strTipo.Contains("FLUENCE")) pictVeiculo1.Image = centralUnidas.Properties.Resources.RenaultFluence;
                                        if (strTipo.Contains("S10")) pictVeiculo1.Image = centralUnidas.Properties.Resources.S103_4;
                                        if (strTipo.Contains("SPIN")) pictVeiculo1.Image = centralUnidas.Properties.Resources.SpinGM;
                                        if (strTipo.Contains("CAMRY")) pictVeiculo1.Image = centralUnidas.Properties.Resources.ToyotaCAMRY2015;
                                        if (strTipo.Contains("VECTRA")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Vectamerged;
                                        if (strTipo.Contains("VERSA")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Versa_Prata_SL;
                                        if (strTipo.Contains("AMAROK")) pictVeiculo1.Image = centralUnidas.Properties.Resources.VWAmarokTrendline;
                                        if (strTipo.Contains("FOX")) pictVeiculo1.Image = centralUnidas.Properties.Resources.VWFoxPrime_4P;
                                        if (strTipo.Contains("JETTA")) pictVeiculo1.Image = centralUnidas.Properties.Resources.VWJetta;
                                        if (strTipo.Contains("KOMBI")) pictVeiculo1.Image = centralUnidas.Properties.Resources.VWKombi;
                                        if (strTipo.Contains("SAVEIRO")) pictVeiculo1.Image = centralUnidas.Properties.Resources.VW_Saveiro;
                                        if (strTipo.Contains("UP")) pictVeiculo1.Image = centralUnidas.Properties.Resources.VWUP;
                                        if (strTipo.Contains("VOYAGE")) pictVeiculo1.Image = centralUnidas.Properties.Resources.VWVOYAGE_PRATA;
                                        if (strTipo.Contains("COBALT")) pictVeiculo1.Image = centralUnidas.Properties.Resources.GM_Cobalt_LTZ;

                                        pnlVeiculo1.Visible = true;

                                        if (dtSet.Tables["Table"].Rows.Count == 1)
                                        {
                                            bEscolheu = true;
                                            pnlVeiculo1.BackColor = Color.Yellow;
                                            pnlVeiculo2.BackColor = Color.Transparent;
                                            pnlVeiculo3.BackColor = Color.Transparent;
                                            pnlVeiculo4.BackColor = Color.Transparent;

                                            Globals.glbMODDES = lblModeloPNL1.Text.Trim().ToUpper();
                                            Globals.glbTPMDES = lblDescricaoPNL1.Text.Trim().ToUpper();
                                        }

                                        pictVeiculo1.Refresh();

                                        Application.DoEvents();
                                        break;
                                    case 1:
                                        lblModeloPNL2.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(dbRow["ModDes"].ToString().Trim().ToLower());
                                        lblDescricaoPNL2.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(dbRow["TpmDes"].ToString().Trim().ToLower());
                                        strTipo = lblModeloPNL2.Text.Trim().ToUpper();

                                        pictVeiculo2.Image = centralUnidas.Properties.Resources.GrupoIndisponivel;

                                        if (strTipo.Contains("A4")) pictVeiculo2.Image = centralUnidas.Properties.Resources.AudiA4;
                                        if (strTipo.Contains("C3")) pictVeiculo2.Image = centralUnidas.Properties.Resources.C3Picasso;
                                        if (strTipo.Contains("CLASSIC")) pictVeiculo2.Image = centralUnidas.Properties.Resources.Classic34;
                                        if (strTipo.Contains("UNO")) pictVeiculo2.Image = centralUnidas.Properties.Resources.Unobranco4portas;
                                        if (strTipo.Contains("PALIO")) pictVeiculo2.Image = centralUnidas.Properties.Resources.Palio2portasmerged;
                                        if (strTipo.Contains("ATTRACTIVE")) pictVeiculo2.Image = centralUnidas.Properties.Resources.FiatPalioAttractive;
                                        if (strTipo.Contains("ADVENTURE")) pictVeiculo2.Image = centralUnidas.Properties.Resources.FiatStradaAdventure2014;
                                        if (strTipo.Contains("STRADA")) pictVeiculo2.Image = centralUnidas.Properties.Resources.FiatStradaAdventure2014;
                                        if (strTipo.Contains("PUNTO")) pictVeiculo2.Image = centralUnidas.Properties.Resources.Puntomerged;
                                        if (strTipo.Contains("WORKING")) pictVeiculo2.Image = centralUnidas.Properties.Resources.FiatStradaWorking2014DIR;
                                        if (strTipo.Contains("FIESTA")) pictVeiculo2.Image = centralUnidas.Properties.Resources.Fiesta3_4SedanFly;
                                        if (strTipo.Contains("FOCUS")) pictVeiculo2.Image = centralUnidas.Properties.Resources.Focus3_4sedanGlx;
                                        if (strTipo.Contains("FUSION")) pictVeiculo2.Image = centralUnidas.Properties.Resources.FordFusion2014;
                                        if (strTipo.Contains("KA")) pictVeiculo2.Image = centralUnidas.Properties.Resources.FORDKA3_42014;
                                        if (strTipo.Contains("RANGER")) pictVeiculo2.Image = centralUnidas.Properties.Resources.FORDRANGER;
                                        if (strTipo.Contains("ONIX")) pictVeiculo2.Image = centralUnidas.Properties.Resources.GMBRAOnix_3_4;
                                        if (strTipo.Contains("PRISMA")) pictVeiculo2.Image = centralUnidas.Properties.Resources.GMBRAPrisma3_4;
                                        if (strTipo.Contains("CRUZE")) pictVeiculo2.Image = centralUnidas.Properties.Resources.GMCruzeSedan2014;
                                        if (strTipo.Contains("GOL")) pictVeiculo2.Image = centralUnidas.Properties.Resources.GolG6VW;
                                        if (strTipo.Contains("G6")) pictVeiculo2.Image = centralUnidas.Properties.Resources.Gol_G6_VW;
                                        if (strTipo.Contains("G5")) pictVeiculo2.Image = centralUnidas.Properties.Resources.Gol_G5_VW;
                                        if (strTipo.Contains("LINEA")) pictVeiculo2.Image = centralUnidas.Properties.Resources.LineaLX1_8FlexMerged;
                                        if (strTipo.Contains("MARCH")) pictVeiculo2.Image = centralUnidas.Properties.Resources.March_SL_Prata;
                                        if (strTipo.Contains("TRITON")) pictVeiculo2.Image = centralUnidas.Properties.Resources.Mitsubishil200_triton;
                                        if (strTipo.Contains("FRONTIER")) pictVeiculo2.Image = centralUnidas.Properties.Resources.NissanFrontier;
                                        if (strTipo.Contains("LIVINA")) pictVeiculo2.Image = centralUnidas.Properties.Resources.NissanLivina1;
                                        if (strTipo.Contains("GRAND LIVINA")) pictVeiculo2.Image = centralUnidas.Properties.Resources.NissanGrandLivina;
                                        if (strTipo.Contains("SENTRA")) pictVeiculo2.Image = centralUnidas.Properties.Resources.NissanSentra;
                                        if (strTipo.Contains("408")) pictVeiculo2.Image = centralUnidas.Properties.Resources.Peugeot408;
                                        if (strTipo.Contains("POLO")) pictVeiculo2.Image = centralUnidas.Properties.Resources.Polo3_4hatch;
                                        if (strTipo.Contains("KANGOO")) pictVeiculo2.Image = centralUnidas.Properties.Resources.Renault_KangooVU_ComPorta;
                                        if (strTipo.Contains("LOGAN")) pictVeiculo2.Image = centralUnidas.Properties.Resources.Renault_Logan_2014;
                                        if (strTipo.Contains("SANDERO")) pictVeiculo2.Image = centralUnidas.Properties.Resources.Renault_Sandero_2015;
                                        if (strTipo.Contains("FLUENCE")) pictVeiculo2.Image = centralUnidas.Properties.Resources.RenaultFluence;
                                        if (strTipo.Contains("S10")) pictVeiculo2.Image = centralUnidas.Properties.Resources.S103_4;
                                        if (strTipo.Contains("SPIN")) pictVeiculo2.Image = centralUnidas.Properties.Resources.SpinGM;
                                        if (strTipo.Contains("CAMRY")) pictVeiculo2.Image = centralUnidas.Properties.Resources.ToyotaCAMRY2015;
                                        if (strTipo.Contains("VECTRA")) pictVeiculo2.Image = centralUnidas.Properties.Resources.Vectamerged;
                                        if (strTipo.Contains("VERSA")) pictVeiculo2.Image = centralUnidas.Properties.Resources.Versa_Prata_SL;
                                        if (strTipo.Contains("AMAROK")) pictVeiculo2.Image = centralUnidas.Properties.Resources.VWAmarokTrendline;
                                        if (strTipo.Contains("FOX")) pictVeiculo2.Image = centralUnidas.Properties.Resources.VWFoxPrime_4P;
                                        if (strTipo.Contains("JETTA")) pictVeiculo2.Image = centralUnidas.Properties.Resources.VWJetta;
                                        if (strTipo.Contains("KOMBI")) pictVeiculo2.Image = centralUnidas.Properties.Resources.VWKombi;
                                        if (strTipo.Contains("SAVEIRO")) pictVeiculo2.Image = centralUnidas.Properties.Resources.VW_Saveiro;
                                        if (strTipo.Contains("UP")) pictVeiculo2.Image = centralUnidas.Properties.Resources.VWUP;
                                        if (strTipo.Contains("VOYAGE")) pictVeiculo2.Image = centralUnidas.Properties.Resources.VWVOYAGE_PRATA;

                                        pictVeiculo2.Refresh();
                                        pnlVeiculo2.Visible = true;
                                        Application.DoEvents();
                                        break;
                                    case 2:
                                        lblModeloPNL3.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(dbRow["ModDes"].ToString().Trim().ToLower());
                                        lblDescricaoPNL3.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(dbRow["TpmDes"].ToString().Trim().ToLower());
                                        strTipo = lblModeloPNL3.Text.Trim().ToUpper();

                                        pictVeiculo3.Image = centralUnidas.Properties.Resources.GrupoIndisponivel;

                                        if (strTipo.Contains("A4")) pictVeiculo3.Image = centralUnidas.Properties.Resources.AudiA4;
                                        if (strTipo.Contains("C3")) pictVeiculo3.Image = centralUnidas.Properties.Resources.C3Picasso;
                                        if (strTipo.Contains("CLASSIC")) pictVeiculo3.Image = centralUnidas.Properties.Resources.Classic34;
                                        if (strTipo.Contains("UNO")) pictVeiculo3.Image = centralUnidas.Properties.Resources.Unobranco4portas;
                                        if (strTipo.Contains("PALIO")) pictVeiculo3.Image = centralUnidas.Properties.Resources.Palio2portasmerged;
                                        if (strTipo.Contains("ATTRACTIVE")) pictVeiculo3.Image = centralUnidas.Properties.Resources.FiatPalioAttractive;
                                        if (strTipo.Contains("ADVENTURE")) pictVeiculo3.Image = centralUnidas.Properties.Resources.FiatStradaAdventure2014;
                                        if (strTipo.Contains("STRADA")) pictVeiculo3.Image = centralUnidas.Properties.Resources.FiatStradaAdventure2014;
                                        if (strTipo.Contains("PUNTO")) pictVeiculo3.Image = centralUnidas.Properties.Resources.Puntomerged;
                                        if (strTipo.Contains("WORKING")) pictVeiculo3.Image = centralUnidas.Properties.Resources.FiatStradaWorking2014DIR;
                                        if (strTipo.Contains("FIESTA")) pictVeiculo3.Image = centralUnidas.Properties.Resources.Fiesta3_4SedanFly;
                                        if (strTipo.Contains("FOCUS")) pictVeiculo3.Image = centralUnidas.Properties.Resources.Focus3_4sedanGlx;
                                        if (strTipo.Contains("FUSION")) pictVeiculo3.Image = centralUnidas.Properties.Resources.FordFusion2014;
                                        if (strTipo.Contains("KA")) pictVeiculo3.Image = centralUnidas.Properties.Resources.FORDKA3_42014;
                                        if (strTipo.Contains("RANGER")) pictVeiculo3.Image = centralUnidas.Properties.Resources.FORDRANGER;
                                        if (strTipo.Contains("ONIX")) pictVeiculo3.Image = centralUnidas.Properties.Resources.GMBRAOnix_3_4;
                                        if (strTipo.Contains("PRISMA")) pictVeiculo3.Image = centralUnidas.Properties.Resources.GMBRAPrisma3_4;
                                        if (strTipo.Contains("CRUZE")) pictVeiculo3.Image = centralUnidas.Properties.Resources.GMCruzeSedan2014;
                                        if (strTipo.Contains("GOL")) pictVeiculo3.Image = centralUnidas.Properties.Resources.GolG6VW;
                                        if (strTipo.Contains("G6")) pictVeiculo3.Image = centralUnidas.Properties.Resources.Gol_G6_VW;
                                        if (strTipo.Contains("G5")) pictVeiculo3.Image = centralUnidas.Properties.Resources.Gol_G5_VW;
                                        if (strTipo.Contains("LINEA")) pictVeiculo3.Image = centralUnidas.Properties.Resources.LineaLX1_8FlexMerged;
                                        if (strTipo.Contains("MARCH")) pictVeiculo3.Image = centralUnidas.Properties.Resources.March_SL_Prata;
                                        if (strTipo.Contains("TRITON")) pictVeiculo3.Image = centralUnidas.Properties.Resources.Mitsubishil200_triton;
                                        if (strTipo.Contains("FRONTIER")) pictVeiculo3.Image = centralUnidas.Properties.Resources.NissanFrontier;
                                        if (strTipo.Contains("LIVINA")) pictVeiculo3.Image = centralUnidas.Properties.Resources.NissanLivina1;
                                        if (strTipo.Contains("GRAND LIVINA")) pictVeiculo3.Image = centralUnidas.Properties.Resources.NissanGrandLivina;
                                        if (strTipo.Contains("SENTRA")) pictVeiculo3.Image = centralUnidas.Properties.Resources.NissanSentra;
                                        if (strTipo.Contains("408")) pictVeiculo3.Image = centralUnidas.Properties.Resources.Peugeot408;
                                        if (strTipo.Contains("POLO")) pictVeiculo3.Image = centralUnidas.Properties.Resources.Polo3_4hatch;
                                        if (strTipo.Contains("KANGOO")) pictVeiculo3.Image = centralUnidas.Properties.Resources.Renault_KangooVU_ComPorta;
                                        if (strTipo.Contains("LOGAN")) pictVeiculo3.Image = centralUnidas.Properties.Resources.Renault_Logan_2014;
                                        if (strTipo.Contains("SANDERO")) pictVeiculo3.Image = centralUnidas.Properties.Resources.Renault_Sandero_2015;
                                        if (strTipo.Contains("FLUENCE")) pictVeiculo3.Image = centralUnidas.Properties.Resources.RenaultFluence;
                                        if (strTipo.Contains("S10")) pictVeiculo3.Image = centralUnidas.Properties.Resources.S103_4;
                                        if (strTipo.Contains("SPIN")) pictVeiculo3.Image = centralUnidas.Properties.Resources.SpinGM;
                                        if (strTipo.Contains("CAMRY")) pictVeiculo3.Image = centralUnidas.Properties.Resources.ToyotaCAMRY2015;
                                        if (strTipo.Contains("VECTRA")) pictVeiculo3.Image = centralUnidas.Properties.Resources.Vectamerged;
                                        if (strTipo.Contains("VERSA")) pictVeiculo3.Image = centralUnidas.Properties.Resources.Versa_Prata_SL;
                                        if (strTipo.Contains("AMAROK")) pictVeiculo3.Image = centralUnidas.Properties.Resources.VWAmarokTrendline;
                                        if (strTipo.Contains("FOX")) pictVeiculo3.Image = centralUnidas.Properties.Resources.VWFoxPrime_4P;
                                        if (strTipo.Contains("JETTA")) pictVeiculo3.Image = centralUnidas.Properties.Resources.VWJetta;
                                        if (strTipo.Contains("KOMBI")) pictVeiculo3.Image = centralUnidas.Properties.Resources.VWKombi;
                                        if (strTipo.Contains("SAVEIRO")) pictVeiculo3.Image = centralUnidas.Properties.Resources.VW_Saveiro;
                                        if (strTipo.Contains("UP")) pictVeiculo3.Image = centralUnidas.Properties.Resources.VWUP;
                                        if (strTipo.Contains("VOYAGE")) pictVeiculo3.Image = centralUnidas.Properties.Resources.VWVOYAGE_PRATA;

                                        pictVeiculo3.Refresh();
                                        pnlVeiculo3.Visible = true;
                                        Application.DoEvents();
                                        break;
                                    case 3:
                                        lblModeloPNL4.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(dbRow["ModDes"].ToString().Trim().ToLower());
                                        lblDescricaoPNL4.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(dbRow["TpmDes"].ToString().Trim().ToLower());
                                        strTipo = lblModeloPNL4.Text.Trim().ToUpper();

                                        pictVeiculo4.Image = centralUnidas.Properties.Resources.GrupoIndisponivel;

                                        if (strTipo.Contains("A4")) pictVeiculo4.Image = centralUnidas.Properties.Resources.AudiA4;
                                        if (strTipo.Contains("C3")) pictVeiculo4.Image = centralUnidas.Properties.Resources.C3Picasso;
                                        if (strTipo.Contains("CLASSIC")) pictVeiculo4.Image = centralUnidas.Properties.Resources.Classic34;
                                        if (strTipo.Contains("UNO")) pictVeiculo4.Image = centralUnidas.Properties.Resources.Unobranco4portas;
                                        if (strTipo.Contains("PALIO")) pictVeiculo4.Image = centralUnidas.Properties.Resources.Palio2portasmerged;
                                        if (strTipo.Contains("ATTRACTIVE")) pictVeiculo4.Image = centralUnidas.Properties.Resources.FiatPalioAttractive;
                                        if (strTipo.Contains("ADVENTURE")) pictVeiculo4.Image = centralUnidas.Properties.Resources.FiatStradaAdventure2014;
                                        if (strTipo.Contains("STRADA")) pictVeiculo4.Image = centralUnidas.Properties.Resources.FiatStradaAdventure2014;
                                        if (strTipo.Contains("PUNTO")) pictVeiculo4.Image = centralUnidas.Properties.Resources.Puntomerged;
                                        if (strTipo.Contains("WORKING")) pictVeiculo4.Image = centralUnidas.Properties.Resources.FiatStradaWorking2014DIR;
                                        if (strTipo.Contains("FIESTA")) pictVeiculo4.Image = centralUnidas.Properties.Resources.Fiesta3_4SedanFly;
                                        if (strTipo.Contains("FOCUS")) pictVeiculo4.Image = centralUnidas.Properties.Resources.Focus3_4sedanGlx;
                                        if (strTipo.Contains("FUSION")) pictVeiculo4.Image = centralUnidas.Properties.Resources.FordFusion2014;
                                        if (strTipo.Contains("KA")) pictVeiculo4.Image = centralUnidas.Properties.Resources.FORDKA3_42014;
                                        if (strTipo.Contains("RANGER")) pictVeiculo4.Image = centralUnidas.Properties.Resources.FORDRANGER;
                                        if (strTipo.Contains("ONIX")) pictVeiculo4.Image = centralUnidas.Properties.Resources.GMBRAOnix_3_4;
                                        if (strTipo.Contains("PRISMA")) pictVeiculo4.Image = centralUnidas.Properties.Resources.GMBRAPrisma3_4;
                                        if (strTipo.Contains("CRUZE")) pictVeiculo4.Image = centralUnidas.Properties.Resources.GMCruzeSedan2014;
                                        if (strTipo.Contains("GOL")) pictVeiculo4.Image = centralUnidas.Properties.Resources.GolG6VW;
                                        if (strTipo.Contains("G6")) pictVeiculo4.Image = centralUnidas.Properties.Resources.Gol_G6_VW;
                                        if (strTipo.Contains("G5")) pictVeiculo4.Image = centralUnidas.Properties.Resources.Gol_G5_VW;
                                        if (strTipo.Contains("LINEA")) pictVeiculo4.Image = centralUnidas.Properties.Resources.LineaLX1_8FlexMerged;
                                        if (strTipo.Contains("MARCH")) pictVeiculo4.Image = centralUnidas.Properties.Resources.March_SL_Prata;
                                        if (strTipo.Contains("TRITON")) pictVeiculo4.Image = centralUnidas.Properties.Resources.Mitsubishil200_triton;
                                        if (strTipo.Contains("FRONTIER")) pictVeiculo4.Image = centralUnidas.Properties.Resources.NissanFrontier;
                                        if (strTipo.Contains("LIVINA")) pictVeiculo4.Image = centralUnidas.Properties.Resources.NissanLivina1;
                                        if (strTipo.Contains("GRAND LIVINA")) pictVeiculo4.Image = centralUnidas.Properties.Resources.NissanGrandLivina;
                                        if (strTipo.Contains("SENTRA")) pictVeiculo4.Image = centralUnidas.Properties.Resources.NissanSentra;
                                        if (strTipo.Contains("408")) pictVeiculo4.Image = centralUnidas.Properties.Resources.Peugeot408;
                                        if (strTipo.Contains("POLO")) pictVeiculo4.Image = centralUnidas.Properties.Resources.Polo3_4hatch;
                                        if (strTipo.Contains("KANGOO")) pictVeiculo4.Image = centralUnidas.Properties.Resources.Renault_KangooVU_ComPorta;
                                        if (strTipo.Contains("LOGAN")) pictVeiculo4.Image = centralUnidas.Properties.Resources.Renault_Logan_2014;
                                        if (strTipo.Contains("SANDERO")) pictVeiculo4.Image = centralUnidas.Properties.Resources.Renault_Sandero_2015;
                                        if (strTipo.Contains("FLUENCE")) pictVeiculo4.Image = centralUnidas.Properties.Resources.RenaultFluence;
                                        if (strTipo.Contains("S10")) pictVeiculo4.Image = centralUnidas.Properties.Resources.S103_4;
                                        if (strTipo.Contains("SPIN")) pictVeiculo4.Image = centralUnidas.Properties.Resources.SpinGM;
                                        if (strTipo.Contains("CAMRY")) pictVeiculo4.Image = centralUnidas.Properties.Resources.ToyotaCAMRY2015;
                                        if (strTipo.Contains("VECTRA")) pictVeiculo4.Image = centralUnidas.Properties.Resources.Vectamerged;
                                        if (strTipo.Contains("VERSA")) pictVeiculo4.Image = centralUnidas.Properties.Resources.Versa_Prata_SL;
                                        if (strTipo.Contains("AMAROK")) pictVeiculo4.Image = centralUnidas.Properties.Resources.VWAmarokTrendline;
                                        if (strTipo.Contains("FOX")) pictVeiculo4.Image = centralUnidas.Properties.Resources.VWFoxPrime_4P;
                                        if (strTipo.Contains("JETTA")) pictVeiculo4.Image = centralUnidas.Properties.Resources.VWJetta;
                                        if (strTipo.Contains("KOMBI")) pictVeiculo4.Image = centralUnidas.Properties.Resources.VWKombi;
                                        if (strTipo.Contains("SAVEIRO")) pictVeiculo4.Image = centralUnidas.Properties.Resources.VW_Saveiro;
                                        if (strTipo.Contains("UP")) pictVeiculo4.Image = centralUnidas.Properties.Resources.VWUP;
                                        if (strTipo.Contains("VOYAGE")) pictVeiculo4.Image = centralUnidas.Properties.Resources.VWVOYAGE_PRATA;

                                        pnlVeiculo4.Visible = true;
                                        Application.DoEvents();
                                        break;

                                    default:
                                        break;
                                }
                                nIndice++;
                                if (nIndice > 4) break;
                            }
                        }
                    }
                    else
                    {
                        iRetServico = 2;
                    }
                }
                clientWcf.Close();
            }
            catch (Exception ex)
            {
                iRetServico = 2;
            }

            return iRetServico;
        }

        private int obtemGrupoXMLviaWCF(string xmlEncodedList)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xmlEncodedList);

                string srvPRASIG = "";
                string srvLOJNOM = "";
                string srvLOJDES = "";
                string srvLOJNOMDEV = "";
                string srvLOJDESDEV = "";
                string srvLOJMAPBLQ = "";
                string srvLOJMAPPER = "";
                string srvGRUCOD = "";
                string srvGRUSIPP = "";
                string srvGRUDES = "";
                string srvGRUURL = "";
                string srvFRTOPE = "";
                string srvTOTMAPA = "";
                string srvTOTOCUP = "";
                string srvDISPPERC = "";
                string srvDISPPERC_DSC = "";
                string srvDISPPERC_AGV = "";
                string srvDISPONIB1 = "";
                string srvDISPONIB2 = "";
                string srvSTATUS = "";
                string srvAGRAVO = "";

                XmlNodeList xmlPRASIG = xmlDoc.GetElementsByTagName("PRASIG");
                if (xmlPRASIG.Count > 0)
                {
                    for (int x = 0; x < xmlPRASIG.Count; x++)
                    {
                        if (!string.IsNullOrEmpty(xmlPRASIG.Item(x).InnerXml))
                        {
                            srvPRASIG = xmlPRASIG.Item(x).InnerXml;
                            Globals.glbPRASIG = srvPRASIG.Trim();
                        }
                    }
                }

                if (!string.IsNullOrEmpty(Globals.glbPRASIG))
                {
                    XmlNodeList xmlLOJNOM = xmlDoc.GetElementsByTagName("LOJNOM");
                    if (xmlLOJNOM.Count > 0)
                    {
                        for (int x = 0; x < xmlLOJNOM.Count; x++)
                        {
                            if (!string.IsNullOrEmpty(xmlLOJNOM.Item(x).InnerXml))
                            {
                                srvLOJNOM = xmlLOJNOM.Item(x).InnerXml;
                                Globals.glbLOJNOM = srvLOJNOM.Trim();
                            }
                        }
                    }

                    XmlNodeList xmlLOJDES = xmlDoc.GetElementsByTagName("LOJDES");
                    if (xmlLOJDES.Count > 0)
                    {
                        for (int x = 0; x < xmlLOJDES.Count; x++)
                        {
                            if (!string.IsNullOrEmpty(xmlLOJDES.Item(x).InnerXml))
                            {
                                srvLOJDES = xmlLOJDES.Item(x).InnerXml;
                                Globals.glbLOJDES = srvLOJDES.Trim();
                            }
                        }
                    }

                    XmlNodeList xmlLOJNOMDEV = xmlDoc.GetElementsByTagName("LOJNOMDEV");
                    if (xmlLOJNOM.Count > 0)
                    {
                        for (int x = 0; x < xmlLOJNOMDEV.Count; x++)
                        {
                            if (!string.IsNullOrEmpty(xmlLOJNOMDEV.Item(x).InnerXml))
                            {
                                srvLOJNOMDEV = xmlLOJNOMDEV.Item(x).InnerXml;
                                Globals.glbLOJNOMDEV = srvLOJNOMDEV.Trim();
                            }
                        }
                    }

                    XmlNodeList xmlLOJDESDEV = xmlDoc.GetElementsByTagName("LOJDESDEV");
                    if (xmlLOJDESDEV.Count > 0)
                    {
                        for (int x = 0; x < xmlLOJDESDEV.Count; x++)
                        {
                            if (!string.IsNullOrEmpty(xmlLOJDESDEV.Item(x).InnerXml))
                            {
                                srvLOJDESDEV = xmlLOJDESDEV.Item(x).InnerXml;
                                Globals.glbLOJDESDEV = srvLOJDESDEV.Trim();
                            }
                        }
                    }

                    XmlNodeList xmlLOJMAPBLQ = xmlDoc.GetElementsByTagName("LOJMAPBLQ");
                    if (xmlLOJMAPBLQ.Count > 0)
                    {
                        for (int x = 0; x < xmlLOJMAPBLQ.Count; x++)
                        {
                            if (!string.IsNullOrEmpty(xmlLOJMAPBLQ.Item(x).InnerXml))
                            {
                                srvLOJMAPBLQ = xmlLOJMAPBLQ.Item(x).InnerXml;
                                Globals.glbLOJMAPBLQ = srvLOJMAPBLQ.Trim();
                            }
                        }
                    }

                    XmlNodeList xmlLOJMAPPER = xmlDoc.GetElementsByTagName("LOJMAPPER");
                    if (xmlLOJMAPPER.Count > 0)
                    {
                        for (int x = 0; x < xmlLOJMAPPER.Count; x++)
                        {
                            if (!string.IsNullOrEmpty(xmlLOJMAPPER.Item(x).InnerXml))
                            {
                                srvLOJMAPPER = xmlLOJMAPPER.Item(x).InnerXml;
                                Globals.glbLOJMAPPER = srvLOJMAPPER.Trim();
                            }
                        }
                    }

                    XmlNodeList xmlGRUCOD = xmlDoc.GetElementsByTagName("GRUCOD");
                    if (xmlGRUCOD.Count > 0)
                    {
                        for (int x = 0; x < xmlGRUCOD.Count; x++)
                        {
                            if (!string.IsNullOrEmpty(xmlGRUCOD.Item(x).InnerXml))
                            {
                                srvGRUCOD = xmlGRUCOD.Item(x).InnerXml;
                                Globals.glbGRUCOD = srvGRUCOD.Trim();
                            }
                        }
                    }

                    XmlNodeList xmlGRUSIPP = xmlDoc.GetElementsByTagName("GRUSIPP");
                    if (xmlGRUSIPP.Count > 0)
                    {
                        for (int x = 0; x < xmlGRUSIPP.Count; x++)
                        {
                            if (!string.IsNullOrEmpty(xmlGRUSIPP.Item(x).InnerXml))
                            {
                                srvGRUSIPP = xmlGRUSIPP.Item(x).InnerXml;
                                Globals.glbGRUSIPP = srvGRUSIPP.Trim();
                            }
                        }
                    }

                    XmlNodeList xmlGRUDES = xmlDoc.GetElementsByTagName("GRUDES");
                    if (xmlGRUDES.Count > 0)
                    {
                        for (int x = 0; x < xmlGRUDES.Count; x++)
                        {
                            if (!string.IsNullOrEmpty(xmlGRUDES.Item(x).InnerXml))
                            {
                                srvGRUDES = xmlGRUDES.Item(x).InnerXml;
                                Globals.glbGRUDES = srvGRUDES.Trim();
                            }
                        }
                    }

                    XmlNodeList xmlGRUURL = xmlDoc.GetElementsByTagName("GRUURL");
                    if (xmlGRUURL.Count > 0)
                    {
                        for (int x = 0; x < xmlGRUURL.Count; x++)
                        {
                            if (!string.IsNullOrEmpty(xmlGRUURL.Item(x).InnerXml))
                            {
                                srvGRUURL = xmlGRUURL.Item(x).InnerXml;
                                Globals.glbGRUURL = srvGRUURL.Trim();
                            }
                        }
                    }

                    XmlNodeList xmlFRTOPE = xmlDoc.GetElementsByTagName("FRTOPE");
                    if (xmlFRTOPE.Count > 0)
                    {
                        for (int x = 0; x < xmlFRTOPE.Count; x++)
                        {
                            if (!string.IsNullOrEmpty(xmlFRTOPE.Item(x).InnerXml))
                            {
                                srvFRTOPE = xmlFRTOPE.Item(x).InnerXml;
                                Globals.glbFRTOPE = srvFRTOPE.Trim();
                            }
                        }
                    }

                    XmlNodeList xmlTOTMAPA = xmlDoc.GetElementsByTagName("TOTMAPA");
                    if (xmlTOTMAPA.Count > 0)
                    {
                        for (int x = 0; x < xmlTOTMAPA.Count; x++)
                        {
                            if (!string.IsNullOrEmpty(xmlTOTMAPA.Item(x).InnerXml))
                            {
                                srvTOTMAPA = xmlTOTMAPA.Item(x).InnerXml;
                                Globals.glbTOTMAPA = srvTOTMAPA.Trim();
                            }
                        }
                    }

                    XmlNodeList xmlTOTOCUP = xmlDoc.GetElementsByTagName("TOTOCUP");
                    if (xmlTOTOCUP.Count > 0)
                    {
                        for (int x = 0; x < xmlTOTOCUP.Count; x++)
                        {
                            if (!string.IsNullOrEmpty(xmlTOTOCUP.Item(x).InnerXml))
                            {
                                srvTOTOCUP = xmlTOTOCUP.Item(x).InnerXml;
                                Globals.glbTOTOCUP = srvTOTOCUP.Trim();
                            }
                        }
                    }

                    XmlNodeList xmlDISPPERC = xmlDoc.GetElementsByTagName("DISPPERC");
                    if (xmlDISPPERC.Count > 0)
                    {
                        for (int x = 0; x < xmlDISPPERC.Count; x++)
                        {
                            if (!string.IsNullOrEmpty(xmlDISPPERC.Item(x).InnerXml))
                            {
                                srvDISPPERC = xmlDISPPERC.Item(x).InnerXml;
                                Globals.glbDISPPERC = srvDISPPERC.Trim();
                            }
                        }
                    }

                    XmlNodeList xmlDISPPERC_DSC = xmlDoc.GetElementsByTagName("DISPPERC_DSC");
                    if (xmlDISPPERC_DSC.Count > 0)
                    {
                        for (int x = 0; x < xmlDISPPERC_DSC.Count; x++)
                        {
                            if (!string.IsNullOrEmpty(xmlDISPPERC_DSC.Item(x).InnerXml))
                            {
                                srvDISPPERC_DSC = xmlDISPPERC_DSC.Item(x).InnerXml;
                                Globals.glbDISPPERC_DSC = srvDISPPERC_DSC.Trim();
                            }
                        }
                    }

                    XmlNodeList xmlDISPPERC_AGV = xmlDoc.GetElementsByTagName("DISPPERC_AGV");
                    if (xmlDISPPERC_AGV.Count > 0)
                    {
                        for (int x = 0; x < xmlDISPPERC_AGV.Count; x++)
                        {
                            if (!string.IsNullOrEmpty(xmlDISPPERC_AGV.Item(x).InnerXml))
                            {
                                srvDISPPERC_AGV = xmlDISPPERC_AGV.Item(x).InnerXml;
                                Globals.glbDISPPERC_AGV = srvDISPPERC_AGV.Trim();
                            }
                        }
                    }

                    XmlNodeList xmlDISPONIB1 = xmlDoc.GetElementsByTagName("DISPONIB1");
                    if (xmlDISPONIB1.Count > 0)
                    {
                        for (int x = 0; x < xmlDISPONIB1.Count; x++)
                        {
                            if (!string.IsNullOrEmpty(xmlDISPONIB1.Item(x).InnerXml))
                            {
                                srvDISPONIB1 = xmlDISPONIB1.Item(x).InnerXml;
                                Globals.glbDISPONIB1 = srvDISPONIB1.Trim();
                            }
                        }
                    }

                    XmlNodeList xmlDISPONIB2 = xmlDoc.GetElementsByTagName("DISPONIB2");
                    if (xmlDISPONIB2.Count > 0)
                    {
                        for (int x = 0; x < xmlDISPONIB2.Count; x++)
                        {
                            if (!string.IsNullOrEmpty(xmlDISPONIB2.Item(x).InnerXml))
                            {
                                srvDISPONIB2 = xmlDISPONIB2.Item(x).InnerXml;
                                Globals.glbDISPONIB2 = srvDISPONIB2.Trim();
                            }
                        }
                    }

                    XmlNodeList xmlSTATUS = xmlDoc.GetElementsByTagName("STATUS");
                    if (xmlSTATUS.Count > 0)
                    {
                        for (int x = 0; x < xmlSTATUS.Count; x++)
                        {
                            if (!string.IsNullOrEmpty(xmlSTATUS.Item(x).InnerXml))
                            {
                                srvSTATUS = xmlSTATUS.Item(x).InnerXml;
                                Globals.glbSTATUS = srvSTATUS.Trim();
                            }
                        }
                    }

                    XmlNodeList xmlAGRAVO = xmlDoc.GetElementsByTagName("AGRAVO");
                    if (xmlAGRAVO.Count > 0)
                    {
                        for (int x = 0; x < xmlAGRAVO.Count; x++)
                        {
                            if (!string.IsNullOrEmpty(xmlAGRAVO.Item(x).InnerXml))
                            {
                                srvAGRAVO = xmlAGRAVO.Item(x).InnerXml;
                                Globals.glbAGRAVO = srvAGRAVO.Trim();
                            }
                        }
                    }
                    return 0;
                }
                else
                {
                    return 3;
                }
            }
            catch (Exception ex)
            {
                return 2;
            }
        }

        private void pictShutdown_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(1);
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            if (!bEscolheu)
            {
                timerDisabled();
                Globals.glbScreen = 16;
                Globals.glbMensagem = 1016;
                frmMensagem frmMsg1016 = new frmMensagem();
                frmMsg1016.Show();
                this.Close();
                this.Dispose();
                return;
            }
            timerDisabled();
            frmScreen06 frm06 = new frmScreen06();
            frm06.Show();
            this.Close();
            this.Dispose();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            tmrReturn.Enabled = false;
            timerDisabled();
            frmScreen01 frm02 = new frmScreen01();
            frm02.Show();
            this.Close();
            this.Dispose();
        }

        private void pnlVeiculo1_Click(object sender, EventArgs e)
        {
            bEscolheu = true;
            pnlVeiculo1.BackColor = Color.Yellow;
            pnlVeiculo2.BackColor = Color.Transparent;
            pnlVeiculo3.BackColor = Color.Transparent;
            pnlVeiculo4.BackColor = Color.Transparent;

            Globals.glbMODDES = lblModeloPNL1.Text.Trim().ToUpper();
            Globals.glbTPMDES = lblDescricaoPNL1.Text.Trim().ToUpper();

            tmrReturn.Stop();
            tmrReturn.Start();
        }

        private void pnlVeiculo2_Click(object sender, EventArgs e)
        {
            bEscolheu = true;
            pnlVeiculo1.BackColor = Color.Transparent;
            pnlVeiculo2.BackColor = Color.Yellow;
            pnlVeiculo3.BackColor = Color.Transparent;
            pnlVeiculo4.BackColor = Color.Transparent;

            Globals.glbMODDES = lblModeloPNL2.Text.Trim().ToUpper();
            Globals.glbTPMDES = lblDescricaoPNL2.Text.Trim().ToUpper();

            tmrReturn.Stop();
            tmrReturn.Start();
        }

        private void pnlVeiculo3_Click(object sender, EventArgs e)
        {
            bEscolheu = true;
            pnlVeiculo1.BackColor = Color.Transparent;
            pnlVeiculo2.BackColor = Color.Transparent;
            pnlVeiculo3.BackColor = Color.Yellow;
            pnlVeiculo4.BackColor = Color.Transparent;

            Globals.glbMODDES = lblModeloPNL3.Text.Trim().ToUpper();
            Globals.glbTPMDES = lblDescricaoPNL3.Text.Trim().ToUpper();

            tmrReturn.Stop();
            tmrReturn.Start();
        }

        private void pnlVeiculo4_Click(object sender, EventArgs e)
        {
            bEscolheu = true;
            pnlVeiculo1.BackColor = Color.Transparent;
            pnlVeiculo2.BackColor = Color.Transparent;
            pnlVeiculo3.BackColor = Color.Transparent;
            pnlVeiculo4.BackColor = Color.Yellow;

            Globals.glbMODDES = lblModeloPNL4.Text.Trim().ToUpper();
            Globals.glbTPMDES = lblDescricaoPNL4.Text.Trim().ToUpper();

            tmrReturn.Stop();
            tmrReturn.Start();
        }

        private void pictVeiculo1_Click(object sender, EventArgs e)
        {
            bEscolheu = true;
            pnlVeiculo1.BackColor = Color.Yellow;
            pnlVeiculo2.BackColor = Color.Transparent;
            pnlVeiculo3.BackColor = Color.Transparent;
            pnlVeiculo4.BackColor = Color.Transparent;

            Globals.glbMODDES = lblModeloPNL1.Text.Trim().ToUpper();
            Globals.glbTPMDES = lblDescricaoPNL1.Text.Trim().ToUpper();

            tmrReturn.Stop();
            tmrReturn.Start();
        }

        private void pictVeiculo2_Click(object sender, EventArgs e)
        {
            bEscolheu = true; 
            pnlVeiculo1.BackColor = Color.Transparent;
            pnlVeiculo2.BackColor = Color.Yellow;
            pnlVeiculo3.BackColor = Color.Transparent;
            pnlVeiculo4.BackColor = Color.Transparent;

            Globals.glbMODDES = lblModeloPNL2.Text.Trim().ToUpper();
            Globals.glbTPMDES = lblDescricaoPNL2.Text.Trim().ToUpper();

            tmrReturn.Stop();
            tmrReturn.Start();
        }

        private void pictVeiculo3_Click(object sender, EventArgs e)
        {
            bEscolheu = true; 
            pnlVeiculo1.BackColor = Color.Transparent;
            pnlVeiculo2.BackColor = Color.Transparent;
            pnlVeiculo3.BackColor = Color.Yellow;
            pnlVeiculo4.BackColor = Color.Transparent;

            Globals.glbMODDES = lblModeloPNL3.Text.Trim().ToUpper();
            Globals.glbTPMDES = lblDescricaoPNL3.Text.Trim().ToUpper();

            tmrReturn.Stop();
            tmrReturn.Start();
        }

        private void pictVeiculo4_Click(object sender, EventArgs e)
        {
            bEscolheu = true; 
            pnlVeiculo1.BackColor = Color.Transparent;
            pnlVeiculo2.BackColor = Color.Transparent;
            pnlVeiculo3.BackColor = Color.Transparent;
            pnlVeiculo4.BackColor = Color.Yellow;

            Globals.glbMODDES = lblModeloPNL4.Text.Trim().ToUpper();
            Globals.glbTPMDES = lblDescricaoPNL4.Text.Trim().ToUpper();

            tmrReturn.Stop();
            tmrReturn.Start();
        }

        private void tmrReturn_Tick(object sender, EventArgs e)
        {
            tmrReturn.Enabled = false;
            timerDisabled();
            frmScreen01 frm1 = new frmScreen01();
            frm1.Show();
            this.Close();
            this.Dispose();
        }

        private void pictLang_Click(object sender, EventArgs e)
        {
            frmIdioma frmIdi = new frmIdioma();
            frmIdi.ShowDialog();

            if (Globals.glbLanguage == 1) pictLang.Image = centralUnidas.Properties.Resources.totem_langPT;
            if (Globals.glbLanguage == 2) pictLang.Image = centralUnidas.Properties.Resources.totem_langPT;
            if (Globals.glbLanguage == 3) pictLang.Image = centralUnidas.Properties.Resources.totem_langPT;
        }

        private void AtualizarInterfaceParaIdiomaCorrente()
        {

            MontarResourceSetParaIdiomaCorrente();

            butIdioma.Text = ResxSetForm.GetString("butIdioma.Text");
            butVoltar.Text = ResxSetForm.GetString("butVoltar.Text");
            butConfirmar.Text = ResxSetForm.GetString("butConfirmar.Text");
            //lblDescricaoPNL1.Text = ResxSetForm.GetString("lblDescricaoPNL.Text");
            //lblDescricaoPNL2.Text = ResxSetForm.GetString("lblDescricaoPNL.Text");
            //lblDescricaoPNL3.Text = ResxSetForm.GetString("lblDescricaoPNL.Text");
            //lblModeloPNL1.Text = ResxSetForm.GetString("lblModeloPNL.Text");
            //lblModeloPNL2.Text = ResxSetForm.GetString("lblModeloPNL.Text");
            //lblModeloPNL3.Text = ResxSetForm.GetString("lblModeloPNL.Text");

            lblSelecioneGrupo1.Text = ResxSetForm.GetString("lblSelecioneGrupo1.Text") + Globals.glbGRUCOD.Trim() + " - " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Globals.glbGRUDES.ToLower()) + ":"; ;
            lblSelecioneGrupo2.Text = ResxSetForm.GetString("lblSelecioneGrupo2.Text");	

            //this.Icon = ((System.Drawing.Icon)(ResxSetForm.GetObject("$this.Icon")));

            if (Globals.glbLanguage == 1) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_Brasil_botao;
            if (Globals.glbLanguage == 2) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_Espanha_botao;
            if (Globals.glbLanguage == 3) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_UK_botao;

            butIdioma.Refresh();
        }

        private void MontarResourceSetParaIdiomaCorrente()
        {
            string srtCaminhoExeTotem = System.IO.Path.GetDirectoryName(Application.ExecutablePath.ToString());
            // Português
            if (Globals.glbLanguage == 1)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen05.pt.resx");
            }
            //Espanhol
            else if (Globals.glbLanguage == 2)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen05.es.resx");
            }
            // Inglês
            else if (Globals.glbLanguage == 3)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen05.en.resx");
            }
        }

        private void butVoltar_Click(object sender, EventArgs e)
        {
            tmrReturn.Enabled = false;
            timerDisabled();
            frmScreen01 frm02 = new frmScreen01();
            frm02.Show();
            this.Close();
            this.Dispose();
        }

        private void butConfirmar_Click(object sender, EventArgs e)
        {
            if (!bEscolheu)
            {
                timerDisabled();
                Globals.glbScreen = 16;
                Globals.glbMensagem = 1016;
                frmMensagem frmMsg1016 = new frmMensagem();
                frmMsg1016.Show();
                this.Close();
                this.Dispose();
                return;
            }
            timerDisabled();
            frmScreen06 frm06 = new frmScreen06();
            frm06.Show();
            this.Close();
            this.Dispose();
        }

        private void butIdioma_Click(object sender, EventArgs e)
        {
            frmIdioma frmIdi = new frmIdioma();
            frmIdi.ShowDialog();
            AtualizarInterfaceParaIdiomaCorrente();
        }

        private void timerDisabled()
        {
            tmrReturn.Enabled = false;
        }

    }
}
