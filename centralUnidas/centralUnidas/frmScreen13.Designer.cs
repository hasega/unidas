﻿namespace centralUnidas
{
    partial class frmScreen13
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmScreen13));
            this.pictUnidasLogo = new System.Windows.Forms.PictureBox();
            this.pictLang = new System.Windows.Forms.PictureBox();
            this.pictShutdown = new System.Windows.Forms.PictureBox();
            this.lblNumeroCartao = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblDigiteNumeroCartao = new System.Windows.Forms.Label();
            this.txtCartao1 = new System.Windows.Forms.TextBox();
            this.txtCartao2 = new System.Windows.Forms.TextBox();
            this.txtCartao3 = new System.Windows.Forms.TextBox();
            this.txtCartao4 = new System.Windows.Forms.TextBox();
            this.pictTec_BACK2 = new System.Windows.Forms.PictureBox();
            this.pictTec_TECLADO = new System.Windows.Forms.PictureBox();
            this.pictTec_0 = new System.Windows.Forms.PictureBox();
            this.pictTec_9 = new System.Windows.Forms.PictureBox();
            this.pictTec_8 = new System.Windows.Forms.PictureBox();
            this.pictTec_7 = new System.Windows.Forms.PictureBox();
            this.pictTec_6 = new System.Windows.Forms.PictureBox();
            this.pictTec_5 = new System.Windows.Forms.PictureBox();
            this.pictTec_4 = new System.Windows.Forms.PictureBox();
            this.pictTec_3 = new System.Windows.Forms.PictureBox();
            this.pictTec_2 = new System.Windows.Forms.PictureBox();
            this.pictTec_1 = new System.Windows.Forms.PictureBox();
            this.pictTecladoFundo = new System.Windows.Forms.PictureBox();
            this.tmrTeclado = new System.Windows.Forms.Timer(this.components);
            this.tmrReturn = new System.Windows.Forms.Timer(this.components);
            this.butContinuar = new System.Windows.Forms.Button();
            this.butVoltar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictLang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictShutdown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_BACK2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_TECLADO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTecladoFundo)).BeginInit();
            this.SuspendLayout();
            // 
            // pictUnidasLogo
            // 
            this.pictUnidasLogo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictUnidasLogo.BackColor = System.Drawing.Color.Transparent;
            this.pictUnidasLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictUnidasLogo.Image = global::centralUnidas.Properties.Resources.totem_header;
            this.pictUnidasLogo.Location = new System.Drawing.Point(0, 0);
            this.pictUnidasLogo.Name = "pictUnidasLogo";
            this.pictUnidasLogo.Size = new System.Drawing.Size(1370, 140);
            this.pictUnidasLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictUnidasLogo.TabIndex = 153;
            this.pictUnidasLogo.TabStop = false;
            // 
            // pictLang
            // 
            this.pictLang.Image = ((System.Drawing.Image)(resources.GetObject("pictLang.Image")));
            this.pictLang.Location = new System.Drawing.Point(1148, 0);
            this.pictLang.Name = "pictLang";
            this.pictLang.Size = new System.Drawing.Size(148, 61);
            this.pictLang.TabIndex = 154;
            this.pictLang.TabStop = false;
            this.pictLang.Visible = false;
            this.pictLang.Click += new System.EventHandler(this.pictLang_Click);
            // 
            // pictShutdown
            // 
            this.pictShutdown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictShutdown.BackColor = System.Drawing.Color.Transparent;
            this.pictShutdown.Image = global::centralUnidas.Properties.Resources.shutdown_button;
            this.pictShutdown.Location = new System.Drawing.Point(12, 173);
            this.pictShutdown.Name = "pictShutdown";
            this.pictShutdown.Size = new System.Drawing.Size(50, 55);
            this.pictShutdown.TabIndex = 155;
            this.pictShutdown.TabStop = false;
            this.pictShutdown.Visible = false;
            this.pictShutdown.Click += new System.EventHandler(this.pictShutdown_Click);
            // 
            // lblNumeroCartao
            // 
            this.lblNumeroCartao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNumeroCartao.BackColor = System.Drawing.Color.White;
            this.lblNumeroCartao.Font = new System.Drawing.Font("Arial", 26F, System.Drawing.FontStyle.Bold);
            this.lblNumeroCartao.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblNumeroCartao.Location = new System.Drawing.Point(0, 147);
            this.lblNumeroCartao.Name = "lblNumeroCartao";
            this.lblNumeroCartao.Size = new System.Drawing.Size(1370, 54);
            this.lblNumeroCartao.TabIndex = 176;
            this.lblNumeroCartao.Text = "Número do Cartão";
            this.lblNumeroCartao.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox2.Image = global::centralUnidas.Properties.Resources.Totem_Traco;
            this.pictureBox2.Location = new System.Drawing.Point(364, 212);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(687, 1);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 177;
            this.pictureBox2.TabStop = false;
            // 
            // lblDigiteNumeroCartao
            // 
            this.lblDigiteNumeroCartao.AutoSize = true;
            this.lblDigiteNumeroCartao.BackColor = System.Drawing.Color.White;
            this.lblDigiteNumeroCartao.Font = new System.Drawing.Font("Arial", 17F);
            this.lblDigiteNumeroCartao.ForeColor = System.Drawing.Color.Gray;
            this.lblDigiteNumeroCartao.Location = new System.Drawing.Point(363, 225);
            this.lblDigiteNumeroCartao.Name = "lblDigiteNumeroCartao";
            this.lblDigiteNumeroCartao.Size = new System.Drawing.Size(680, 26);
            this.lblDigiteNumeroCartao.TabIndex = 178;
            this.lblDigiteNumeroCartao.Text = "Digite o número do cartão de crédito para conferência do sistema:";
            // 
            // txtCartao1
            // 
            this.txtCartao1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCartao1.BackColor = System.Drawing.Color.White;
            this.txtCartao1.Font = new System.Drawing.Font("Arial", 27.75F);
            this.txtCartao1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(109)))), ((int)(((byte)(175)))));
            this.txtCartao1.Location = new System.Drawing.Point(497, 279);
            this.txtCartao1.MaxLength = 1;
            this.txtCartao1.Name = "txtCartao1";
            this.txtCartao1.Size = new System.Drawing.Size(96, 50);
            this.txtCartao1.TabIndex = 179;
            this.txtCartao1.WordWrap = false;
            this.txtCartao1.TextChanged += new System.EventHandler(this.txtCartao1_TextChanged);
            // 
            // txtCartao2
            // 
            this.txtCartao2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCartao2.BackColor = System.Drawing.Color.White;
            this.txtCartao2.Font = new System.Drawing.Font("Arial", 27.75F);
            this.txtCartao2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(109)))), ((int)(((byte)(175)))));
            this.txtCartao2.Location = new System.Drawing.Point(595, 279);
            this.txtCartao2.MaxLength = 1;
            this.txtCartao2.Name = "txtCartao2";
            this.txtCartao2.Size = new System.Drawing.Size(96, 50);
            this.txtCartao2.TabIndex = 180;
            this.txtCartao2.WordWrap = false;
            this.txtCartao2.TextChanged += new System.EventHandler(this.txtCartao2_TextChanged);
            // 
            // txtCartao3
            // 
            this.txtCartao3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCartao3.BackColor = System.Drawing.Color.White;
            this.txtCartao3.Font = new System.Drawing.Font("Arial", 27.75F);
            this.txtCartao3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(109)))), ((int)(((byte)(175)))));
            this.txtCartao3.Location = new System.Drawing.Point(693, 279);
            this.txtCartao3.MaxLength = 1;
            this.txtCartao3.Name = "txtCartao3";
            this.txtCartao3.Size = new System.Drawing.Size(96, 50);
            this.txtCartao3.TabIndex = 181;
            this.txtCartao3.WordWrap = false;
            this.txtCartao3.TextChanged += new System.EventHandler(this.txtCartao3_TextChanged);
            // 
            // txtCartao4
            // 
            this.txtCartao4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCartao4.BackColor = System.Drawing.Color.White;
            this.txtCartao4.Font = new System.Drawing.Font("Arial", 27.75F);
            this.txtCartao4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(109)))), ((int)(((byte)(175)))));
            this.txtCartao4.Location = new System.Drawing.Point(791, 279);
            this.txtCartao4.MaxLength = 1;
            this.txtCartao4.Name = "txtCartao4";
            this.txtCartao4.Size = new System.Drawing.Size(96, 50);
            this.txtCartao4.TabIndex = 182;
            this.txtCartao4.WordWrap = false;
            this.txtCartao4.TextChanged += new System.EventHandler(this.txtCartao4_TextChanged);
            // 
            // pictTec_BACK2
            // 
            this.pictTec_BACK2.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_BACK2.Image = global::centralUnidas.Properties.Resources.totem_Backspace;
            this.pictTec_BACK2.Location = new System.Drawing.Point(766, 685);
            this.pictTec_BACK2.Name = "pictTec_BACK2";
            this.pictTec_BACK2.Size = new System.Drawing.Size(76, 73);
            this.pictTec_BACK2.TabIndex = 195;
            this.pictTec_BACK2.TabStop = false;
            this.pictTec_BACK2.Click += new System.EventHandler(this.pictTec_BACK2_Click);
            // 
            // pictTec_TECLADO
            // 
            this.pictTec_TECLADO.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_TECLADO.Image = global::centralUnidas.Properties.Resources.totem_teclado;
            this.pictTec_TECLADO.Location = new System.Drawing.Point(602, 685);
            this.pictTec_TECLADO.Name = "pictTec_TECLADO";
            this.pictTec_TECLADO.Size = new System.Drawing.Size(76, 73);
            this.pictTec_TECLADO.TabIndex = 194;
            this.pictTec_TECLADO.TabStop = false;
            this.pictTec_TECLADO.Click += new System.EventHandler(this.pictTec_TECLADO_Click);
            // 
            // pictTec_0
            // 
            this.pictTec_0.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_0.Image = global::centralUnidas.Properties.Resources.totem_0;
            this.pictTec_0.Location = new System.Drawing.Point(684, 685);
            this.pictTec_0.Name = "pictTec_0";
            this.pictTec_0.Size = new System.Drawing.Size(76, 73);
            this.pictTec_0.TabIndex = 193;
            this.pictTec_0.TabStop = false;
            this.pictTec_0.Click += new System.EventHandler(this.pictTec_0_Click);
            // 
            // pictTec_9
            // 
            this.pictTec_9.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_9.Image = global::centralUnidas.Properties.Resources.totem_9;
            this.pictTec_9.Location = new System.Drawing.Point(766, 606);
            this.pictTec_9.Name = "pictTec_9";
            this.pictTec_9.Size = new System.Drawing.Size(76, 73);
            this.pictTec_9.TabIndex = 192;
            this.pictTec_9.TabStop = false;
            this.pictTec_9.Click += new System.EventHandler(this.pictTec_9_Click);
            // 
            // pictTec_8
            // 
            this.pictTec_8.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_8.Image = global::centralUnidas.Properties.Resources.totem_8;
            this.pictTec_8.Location = new System.Drawing.Point(684, 606);
            this.pictTec_8.Name = "pictTec_8";
            this.pictTec_8.Size = new System.Drawing.Size(76, 73);
            this.pictTec_8.TabIndex = 191;
            this.pictTec_8.TabStop = false;
            this.pictTec_8.Click += new System.EventHandler(this.pictTec_8_Click);
            // 
            // pictTec_7
            // 
            this.pictTec_7.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_7.Image = global::centralUnidas.Properties.Resources.totem_7;
            this.pictTec_7.Location = new System.Drawing.Point(602, 606);
            this.pictTec_7.Name = "pictTec_7";
            this.pictTec_7.Size = new System.Drawing.Size(76, 73);
            this.pictTec_7.TabIndex = 190;
            this.pictTec_7.TabStop = false;
            this.pictTec_7.Click += new System.EventHandler(this.pictTec_7_Click);
            // 
            // pictTec_6
            // 
            this.pictTec_6.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_6.Image = global::centralUnidas.Properties.Resources.totem_6;
            this.pictTec_6.Location = new System.Drawing.Point(766, 527);
            this.pictTec_6.Name = "pictTec_6";
            this.pictTec_6.Size = new System.Drawing.Size(76, 73);
            this.pictTec_6.TabIndex = 189;
            this.pictTec_6.TabStop = false;
            this.pictTec_6.Click += new System.EventHandler(this.pictTec_6_Click);
            // 
            // pictTec_5
            // 
            this.pictTec_5.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_5.Image = global::centralUnidas.Properties.Resources.totem_5;
            this.pictTec_5.Location = new System.Drawing.Point(684, 527);
            this.pictTec_5.Name = "pictTec_5";
            this.pictTec_5.Size = new System.Drawing.Size(76, 73);
            this.pictTec_5.TabIndex = 188;
            this.pictTec_5.TabStop = false;
            this.pictTec_5.Click += new System.EventHandler(this.pictTec_5_Click);
            // 
            // pictTec_4
            // 
            this.pictTec_4.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_4.Image = global::centralUnidas.Properties.Resources.totem_4;
            this.pictTec_4.Location = new System.Drawing.Point(602, 527);
            this.pictTec_4.Name = "pictTec_4";
            this.pictTec_4.Size = new System.Drawing.Size(76, 73);
            this.pictTec_4.TabIndex = 187;
            this.pictTec_4.TabStop = false;
            this.pictTec_4.Click += new System.EventHandler(this.pictTec_4_Click);
            // 
            // pictTec_3
            // 
            this.pictTec_3.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_3.Image = global::centralUnidas.Properties.Resources.totem_3;
            this.pictTec_3.Location = new System.Drawing.Point(766, 448);
            this.pictTec_3.Name = "pictTec_3";
            this.pictTec_3.Size = new System.Drawing.Size(76, 73);
            this.pictTec_3.TabIndex = 186;
            this.pictTec_3.TabStop = false;
            this.pictTec_3.Click += new System.EventHandler(this.pictTec_3_Click);
            // 
            // pictTec_2
            // 
            this.pictTec_2.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_2.Image = global::centralUnidas.Properties.Resources.totem_2;
            this.pictTec_2.Location = new System.Drawing.Point(684, 448);
            this.pictTec_2.Name = "pictTec_2";
            this.pictTec_2.Size = new System.Drawing.Size(76, 73);
            this.pictTec_2.TabIndex = 185;
            this.pictTec_2.TabStop = false;
            this.pictTec_2.Click += new System.EventHandler(this.pictTec_2_Click);
            // 
            // pictTec_1
            // 
            this.pictTec_1.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_1.Image = global::centralUnidas.Properties.Resources.totem_1;
            this.pictTec_1.Location = new System.Drawing.Point(602, 448);
            this.pictTec_1.Name = "pictTec_1";
            this.pictTec_1.Size = new System.Drawing.Size(76, 73);
            this.pictTec_1.TabIndex = 184;
            this.pictTec_1.TabStop = false;
            this.pictTec_1.Click += new System.EventHandler(this.pictTec_1_Click);
            // 
            // pictTecladoFundo
            // 
            this.pictTecladoFundo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictTecladoFundo.BackColor = System.Drawing.Color.Transparent;
            this.pictTecladoFundo.BackgroundImage = global::centralUnidas.Properties.Resources.totem_teclado_fundo;
            this.pictTecladoFundo.Location = new System.Drawing.Point(0, 432);
            this.pictTecladoFundo.Name = "pictTecladoFundo";
            this.pictTecladoFundo.Size = new System.Drawing.Size(1370, 335);
            this.pictTecladoFundo.TabIndex = 183;
            this.pictTecladoFundo.TabStop = false;
            // 
            // tmrTeclado
            // 
            this.tmrTeclado.Tick += new System.EventHandler(this.tmrTeclado_Tick);
            // 
            // tmrReturn
            // 
            this.tmrReturn.Enabled = true;
            this.tmrReturn.Interval = 90000;
            this.tmrReturn.Tick += new System.EventHandler(this.tmrReturn_Tick);
            // 
            // butContinuar
            // 
            this.butContinuar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.butContinuar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.butContinuar.Font = new System.Drawing.Font("Arial", 13F, System.Drawing.FontStyle.Bold);
            this.butContinuar.ForeColor = System.Drawing.Color.White;
            this.butContinuar.Location = new System.Drawing.Point(908, 289);
            this.butContinuar.Name = "butContinuar";
            this.butContinuar.Size = new System.Drawing.Size(147, 45);
            this.butContinuar.TabIndex = 275;
            this.butContinuar.Text = "Continuar";
            this.butContinuar.UseVisualStyleBackColor = false;
            this.butContinuar.Click += new System.EventHandler(this.butContinuar_Click);
            // 
            // butVoltar
            // 
            this.butVoltar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.butVoltar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.butVoltar.Font = new System.Drawing.Font("Arial", 13F, System.Drawing.FontStyle.Bold);
            this.butVoltar.ForeColor = System.Drawing.Color.White;
            this.butVoltar.Location = new System.Drawing.Point(646, 379);
            this.butVoltar.Name = "butVoltar";
            this.butVoltar.Size = new System.Drawing.Size(151, 45);
            this.butVoltar.TabIndex = 274;
            this.butVoltar.Text = "Voltar";
            this.butVoltar.UseVisualStyleBackColor = false;
            this.butVoltar.Click += new System.EventHandler(this.butVoltar_Click);
            // 
            // frmScreen13
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1366, 768);
            this.ControlBox = false;
            this.Controls.Add(this.butContinuar);
            this.Controls.Add(this.butVoltar);
            this.Controls.Add(this.pictTec_BACK2);
            this.Controls.Add(this.pictTec_TECLADO);
            this.Controls.Add(this.pictTec_0);
            this.Controls.Add(this.pictTec_9);
            this.Controls.Add(this.pictTec_8);
            this.Controls.Add(this.pictTec_7);
            this.Controls.Add(this.pictTec_6);
            this.Controls.Add(this.pictTec_5);
            this.Controls.Add(this.pictTec_4);
            this.Controls.Add(this.pictTec_3);
            this.Controls.Add(this.pictTec_2);
            this.Controls.Add(this.pictTec_1);
            this.Controls.Add(this.pictTecladoFundo);
            this.Controls.Add(this.txtCartao4);
            this.Controls.Add(this.txtCartao3);
            this.Controls.Add(this.txtCartao2);
            this.Controls.Add(this.txtCartao1);
            this.Controls.Add(this.lblDigiteNumeroCartao);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictShutdown);
            this.Controls.Add(this.lblNumeroCartao);
            this.Controls.Add(this.pictLang);
            this.Controls.Add(this.pictUnidasLogo);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmScreen13";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CAU - Unidas";
            this.TopMost = false;
            this.WindowState = System.Windows.Forms.FormWindowState.Normal;
            this.Load += new System.EventHandler(this.frmScreen13_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictLang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictShutdown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_BACK2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_TECLADO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTecladoFundo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictUnidasLogo;
        private System.Windows.Forms.PictureBox pictLang;
        private System.Windows.Forms.PictureBox pictShutdown;
        private System.Windows.Forms.Label lblNumeroCartao;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblDigiteNumeroCartao;
        private System.Windows.Forms.TextBox txtCartao1;
        private System.Windows.Forms.TextBox txtCartao2;
        private System.Windows.Forms.TextBox txtCartao3;
        private System.Windows.Forms.TextBox txtCartao4;
        private System.Windows.Forms.PictureBox pictTec_BACK2;
        private System.Windows.Forms.PictureBox pictTec_TECLADO;
        private System.Windows.Forms.PictureBox pictTec_0;
        private System.Windows.Forms.PictureBox pictTec_9;
        private System.Windows.Forms.PictureBox pictTec_8;
        private System.Windows.Forms.PictureBox pictTec_7;
        private System.Windows.Forms.PictureBox pictTec_6;
        private System.Windows.Forms.PictureBox pictTec_5;
        private System.Windows.Forms.PictureBox pictTec_4;
        private System.Windows.Forms.PictureBox pictTec_3;
        private System.Windows.Forms.PictureBox pictTec_2;
        private System.Windows.Forms.PictureBox pictTec_1;
        private System.Windows.Forms.PictureBox pictTecladoFundo;
        private System.Windows.Forms.Timer tmrTeclado;
        private System.Windows.Forms.Timer tmrReturn;
        private System.Windows.Forms.Button butContinuar;
        private System.Windows.Forms.Button butVoltar;
    }
}