﻿// Aplicação: Unidas Totem
// Data     : 01/10/2015
// Versão   : 1.0
// Empresa  : Unidas Rent a Car
// Equipe   : Unidas Desenvolvimento
// Objetivo : Informar validade do cartao
// Histórico: 
//      01/10/2015: Inicio de desenvolvimento para acesso geral
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;
using centralUnidas.centralUnidasWCF;
using System.Resources;

namespace centralUnidas
{
    public partial class frmScreen14 : Form
    {
        public ResXResourceSet ResxSetForm;

        int gConfirma = 0;
        int gPrint = 0;

        PrintDocument pdoc = null;
        public frmScreen14()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void tmrReturn_Tick(object sender, EventArgs e)
        {

            tmrReturn.Enabled = false;
            frmScreen01 frm1 = new frmScreen01();
            frm1.Show();
            this.Close();
            this.Dispose();
        }

        private void pictCartaoConfirmar_Click(object sender, EventArgs e)
        {
            if (gConfirma == 0)
            {
                tmrReturn.Stop();
                tmrReturn.Enabled = false;
                bool bValida = true;

                if (comboAno.Text.Trim().Length == 0) bValida = false;
                if (comboMes.Text.Trim().Length == 0) bValida = false;

                if (bValida)
                {
                    timerDisabled();

                    Globals.glbCC_SegurancaInformado = comboAno.Text.Trim() + comboMes.Text.Trim();
                    frmScreen10 frm10 = new frmScreen10();
                    frm10.Show();
                    this.Close();
                    this.Dispose();
                }
                else
                {
                    timerDisabled();

                    Globals.glbScreen = 14;
                    Globals.glbMensagem = 1008;
                    frmMensagem frmMsg = new frmMensagem();
                    frmMsg.Show();
                    this.Close();
                    this.Dispose();
                }
            }
            gConfirma = 1;

        }

        private void pictVoltar_Click(object sender, EventArgs e)
        {
            tmrReturn.Stop();
            tmrReturn.Enabled = false;

            CentralUnidasClassClient clientWcf = new CentralUnidasClassClient();
            string xmlRetornoWCF = clientWcf.getCupomFiscal(Convert.ToInt32(Globals.glbReservaNumeroPesquisa));
            if (string.IsNullOrEmpty(xmlRetornoWCF) || (xmlRetornoWCF.Trim() == "<UNIDASWCF />"))
            {
                Globals.glbCupomFiscal1 = "";
                Globals.glbCupomFiscal2 = "";
                Globals.glbCC_CodigoPreAutorizacao = "";
                Globals.glbCC_CodigoAutorizacaoEmissor = "";
                Globals.glbBandeiraCartao = "";
            }
            else
            {
                xmlRetornoWCF = xmlRetornoWCF.Replace("\r\n", "");
                DataSet dtSet = new DataSet();
                System.IO.StringReader xmlSR = new System.IO.StringReader(xmlRetornoWCF);
                dtSet.ReadXml(xmlSR);
                foreach (DataRow dbRow in dtSet.Tables["Table"].Rows)
                {

                    Globals.glbCupomFiscal1 = dbRow["CUPOM_CLIENTE"].ToString().Trim();
                    Globals.glbCupomFiscal2 = dbRow["CUPOM_LOJA"].ToString().Trim();
                    Globals.glbCC_CodigoPreAutorizacao = dbRow["CODIGO_PREAUTORIZACAO"].ToString().Trim();
                    Globals.glbCC_CodigoAutorizacaoEmissor = dbRow["CODIGO_EMISSOR"].ToString().Trim();

                    string strCupomString = "";
                    strCupomString = Globals.glbCupomFiscal1.Trim();
                    if (strCupomString.ToUpper().Contains("REDE")) Globals.glbBandeiraCartao = "RED";
                    if (strCupomString.ToUpper().Contains("AMERICAN")) Globals.glbBandeiraCartao = "AMX";
                    if (strCupomString.ToUpper().Contains("CIELO")) Globals.glbBandeiraCartao = "CIE";
                    if (strCupomString.ToUpper().Contains("DINERS")) Globals.glbBandeiraCartao = "DNR";
                    if (strCupomString.ToUpper().Contains("ELO")) Globals.glbBandeiraCartao = "ELO";
                    if (strCupomString.ToUpper().Contains("MASTERCARD")) Globals.glbBandeiraCartao = "MAS";
                    if (strCupomString.ToUpper().Contains("VISA")) Globals.glbBandeiraCartao = "VIS";

                    break;
                }
            }

            int nRetorno = 0;
            nRetorno = printRA();

            if (gPrint > 0)
            {
                timerDisabled();

                Globals.glbScreen = 15;
                Globals.glbMensagem = 1015;
                frmMensagem frmMsg = new frmMensagem();
                frmMsg.Show();
                this.Close();
                this.Dispose();
            }
            else
            {
                timerDisabled();

                frmScreen01 frm02 = new frmScreen01();
                frm02.Show();
                this.Close();
                this.Dispose();
            }
        }

        private int printRA()
        {
            try
            {
                pdoc = new PrintDocument();
                PrinterSettings ps = new PrinterSettings();

                pdoc.PrintPage += new PrintPageEventHandler(pdoc_PrintPage);
                PaperSize pkSize = new PaperSize("RA", 300, 1100);
                Margins pkMargin = new Margins(10, 10, 10, 10);

                pdoc.OriginAtMargins = true;
                pdoc.DefaultPageSettings.Margins.Left = 20;
                pdoc.DefaultPageSettings.Margins.Right = 20;
                pdoc.DefaultPageSettings.Margins.Top = 20;
                pdoc.DefaultPageSettings.Margins.Bottom = 20;
                pdoc.DefaultPageSettings.PaperSize = pkSize;
                pdoc.PrinterSettings.DefaultPageSettings.PaperSize = new PaperSize("RA", 300, 1100);
                pdoc.PrinterSettings.DefaultPageSettings.Margins.Left = 20;
                pdoc.PrinterSettings.DefaultPageSettings.Margins.Right = 20;
                pdoc.PrinterSettings.DefaultPageSettings.Margins.Top = 20;
                pdoc.PrinterSettings.DefaultPageSettings.Margins.Bottom = 20;
                pdoc.PrinterSettings.DefaultPageSettings.PrinterSettings.DefaultPageSettings.PaperSize = new PaperSize("RA", 300, 1100);
                pdoc.DocumentName = "RA";
                pdoc.Print();
            }
            catch (Exception ex)
            {

            }
            return gPrint;
        }

        void pdoc_PrintPage(object sender, PrintPageEventArgs e)
        {
            try
            {
                Graphics graphics = e.Graphics;
                Font font = new Font("Courier New", 9, FontStyle.Bold);
                float fontHeight = font.GetHeight();

                int startX = 3;
                int startY = 5;
                int Offset = 40;

                if (!string.IsNullOrEmpty(Globals.glbCupomFiscal1))
                {
                    gPrint = 1;

                    graphics.DrawString("--------------------------------------------", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    Offset = Offset + 17;
                    graphics.DrawString("               CUPOM FISCAL                  ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    Offset = Offset + 17;
                    Offset = Offset + 17;
                    graphics.DrawString(Globals.glbCupomFiscal1, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    for (int x = 0; x < 17; x++)
                    {
                        Offset = Offset + 17;
                    }
                }

                if (!string.IsNullOrEmpty(Globals.glbCupomFiscal2))
                {

                    gPrint = 1;

                    graphics.DrawString("--------------------------------------------", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    Offset = Offset + 17;
                    graphics.DrawString("               CUPOM FISCAL                  ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    Offset = Offset + 17;
                    Offset = Offset + 17;
                    graphics.DrawString(Globals.glbCupomFiscal2, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    for (int x = 0; x < 17; x++)
                    {
                        Offset = Offset + 17;
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void frmScreen14_Load(object sender, EventArgs e)
        {
            LogTotem.InsertLog(new LogOperacao()
            {
                Application_Name = Globals.glbApplicationName,
                Application_Version = Globals.glbApplicationVersion,
                Local = Globals.glbLojaTotem,
                Operation_Ticks = Globals.glbOperationTicks,
                Operation_Name = "Informa Mes/Ano Validade Cartão",
                Form = "FrmScreen14",
                RESNUM = String.IsNullOrEmpty(Globals.glbReservaNumeroPesquisa) ? 0 : Convert.ToInt32(Globals.glbReservaNumeroPesquisa),
                RANUM = String.IsNullOrEmpty(Globals.glbRA_Contrato) ? 0 : Convert.ToInt32(Globals.glbRA_Contrato)

            });

            Globals.glbScreen = 8;
            int nScreenWidth = this.Width;
            int nScreenWidthSplit = nScreenWidth / 2;

            AtualizarInterfaceParaIdiomaCorrente();

            this.Refresh();

            Globals.glbDebugMode = Convert.ToInt16(ConfigurationManager.AppSettings.Get("appDebugMode"));
            Shutdown_Enable();
        }

        private void Shutdown_Enable()
        {
            if (Globals.glbDebugMode == 0)
                pictShutdown.Visible = false;
            else
                pictShutdown.Visible = true;
        }

        private void pictShutdown_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(1);
        }

        private void comboMes_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboMes_Click(object sender, EventArgs e)
        {
            comboMes.DroppedDown = true;
        }

        private void comboAno_Click(object sender, EventArgs e)
        {
            comboAno.DroppedDown = true;
        }

        private void butContinuar_Click(object sender, EventArgs e)
        {
            if (gConfirma == 0)
            {
                tmrReturn.Stop();
                tmrReturn.Enabled = false;
                bool bValida = true;

                if (comboAno.Text.Trim().Length == 0) bValida = false;
                if (comboMes.Text.Trim().Length == 0) bValida = false;

                if (bValida)
                {
                    timerDisabled();

                    Globals.glbCC_SegurancaInformado = comboAno.Text.Trim() + comboMes.Text.Trim();
                    frmScreen10 frm10 = new frmScreen10();
                    frm10.Show();
                    this.Close();
                    this.Dispose();
                }
                else
                {
                    timerDisabled();

                    Globals.glbScreen = 14;
                    Globals.glbMensagem = 1008;
                    frmMensagem frmMsg = new frmMensagem();
                    frmMsg.Show();
                    this.Close();
                    this.Dispose();
                }
            }
            gConfirma = 1;
        }

        private void butVoltar_Click(object sender, EventArgs e)
        {
            tmrReturn.Stop();
            tmrReturn.Enabled = false;

            CentralUnidasClassClient clientWcf = new CentralUnidasClassClient();
            string xmlRetornoWCF = clientWcf.getCupomFiscal(Convert.ToInt32(Globals.glbReservaNumeroPesquisa));
            if (string.IsNullOrEmpty(xmlRetornoWCF) || (xmlRetornoWCF.Trim() == "<UNIDASWCF />"))
            {
                Globals.glbCupomFiscal1 = "";
                Globals.glbCupomFiscal2 = "";
                Globals.glbCC_CodigoPreAutorizacao = "";
                Globals.glbCC_CodigoAutorizacaoEmissor = "";
                Globals.glbBandeiraCartao = "";
            }
            else
            {
                xmlRetornoWCF = xmlRetornoWCF.Replace("\r\n", "");
                DataSet dtSet = new DataSet();
                System.IO.StringReader xmlSR = new System.IO.StringReader(xmlRetornoWCF);
                dtSet.ReadXml(xmlSR);
                foreach (DataRow dbRow in dtSet.Tables["Table"].Rows)
                {

                    Globals.glbCupomFiscal1 = dbRow["CUPOM_CLIENTE"].ToString().Trim();
                    Globals.glbCupomFiscal2 = dbRow["CUPOM_LOJA"].ToString().Trim();
                    Globals.glbCC_CodigoPreAutorizacao = dbRow["CODIGO_PREAUTORIZACAO"].ToString().Trim();
                    Globals.glbCC_CodigoAutorizacaoEmissor = dbRow["CODIGO_EMISSOR"].ToString().Trim();

                    string strCupomString = "";
                    strCupomString = Globals.glbCupomFiscal1.Trim();
                    if (strCupomString.ToUpper().Contains("REDE")) Globals.glbBandeiraCartao = "RED";
                    if (strCupomString.ToUpper().Contains("AMERICAN")) Globals.glbBandeiraCartao = "AMX";
                    if (strCupomString.ToUpper().Contains("CIELO")) Globals.glbBandeiraCartao = "CIE";
                    if (strCupomString.ToUpper().Contains("DINERS")) Globals.glbBandeiraCartao = "DNR";
                    if (strCupomString.ToUpper().Contains("ELO")) Globals.glbBandeiraCartao = "ELO";
                    if (strCupomString.ToUpper().Contains("MASTERCARD")) Globals.glbBandeiraCartao = "MAS";
                    if (strCupomString.ToUpper().Contains("VISA")) Globals.glbBandeiraCartao = "VIS";

                    break;
                }
            }

            int nRetorno = 0;
            nRetorno = printRA();

            if (gPrint > 0)
            {
                timerDisabled();

                Globals.glbScreen = 15;
                Globals.glbMensagem = 1015;
                frmMensagem frmMsg = new frmMensagem();
                frmMsg.Show();
                this.Close();
                this.Dispose();
            }
            else
            {
                timerDisabled();

                frmScreen01 frm02 = new frmScreen01();
                frm02.Show();
                this.Close();
                this.Dispose();
            }
        }

        private void AtualizarInterfaceParaIdiomaCorrente()
        {
            MontarResourceSetParaIdiomaCorrente();

            butContinuar.Text = ResxSetForm.GetString("butContinuar.Text");
            butVoltar.Text = ResxSetForm.GetString("butVoltar.Text");
            lblAno.Text = ResxSetForm.GetString("lblAno.Text");
            lblInformeValidade.Text = ResxSetForm.GetString("lblInformeValidade.Text");
            lblMes.Text = ResxSetForm.GetString("lblMes.Text");
            lblValidadeCartao.Text = ResxSetForm.GetString("lblValidadeCartao.Text");            
        }

        private void MontarResourceSetParaIdiomaCorrente()
        {
            string srtCaminhoExeTotem = System.IO.Path.GetDirectoryName(Application.ExecutablePath.ToString());

            // Português
            if (Globals.glbLanguage == 1)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen14.pt.resx");
            }
            //Espanhol
            else if (Globals.glbLanguage == 2)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen14.es.resx");
            }
            // Inglês
            else if (Globals.glbLanguage == 3)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen14.en.resx");
            }
        }

        private void timerDisabled()
        {
            tmrReturn.Enabled = false;
        }
    }
}
