﻿// Aplicação: Unidas Totem
// Data     : 01/10/2015
// Versão   : 1.0
// Empresa  : Unidas Rent a Car
// Equipe   : Unidas Desenvolvimento
// Objetivo : Resumo da Reserva + Abre RA 
// Histórico: 
//      01/10/2015: Inicio de desenvolvimento para acesso geral
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using centralUnidas.centralUnidasWCF;
using Newtonsoft.Json.Linq;
using System.Xml;
using centralUnidas.wbsUnidasSrv;
using System.IO;
using System.Xml.Serialization;
using System.Resources;
using System.Diagnostics;

namespace centralUnidas
{
    public partial class frmScreen08 : Form
    {
        OtaVehRetResResult resOtaVehRetResResult = null;

        public ResXResourceSet ResxSetForm;

        string sTiksExecucaoFormulário;

        public frmScreen08()
        {
            InitializeComponent();
        }

        private void frmScreen08_Load(object sender, EventArgs e)
        {

            LogTotem.InsertLog(new LogOperacao()
            {
                Application_Name = Globals.glbApplicationName,
                Application_Version = Globals.glbApplicationVersion,
                Local = Globals.glbLojaTotem,
                Operation_Ticks = Globals.glbOperationTicks,
                Operation_Name = "Resumo da Reserva",
                Form = "FrmScreen08",
                RESNUM = String.IsNullOrEmpty(Globals.glbReservaNumeroPesquisa) ? 0 : Convert.ToInt32(Globals.glbReservaNumeroPesquisa),
                RANUM = String.IsNullOrEmpty(Globals.glbRA_Contrato) ? 0 : Convert.ToInt32(Globals.glbRA_Contrato)
            });

            sTiksExecucaoFormulário = DateTime.Now.Ticks.ToString();

            butConfirmar.Enabled = true;

            Globals.glbScreen = 6;
            int nScreenWidth = this.Width;
            int nScreenWidthSplit = nScreenWidth / 2;

            CultureInfo culture = new CultureInfo("pt-BR");

            this.Refresh();

            Globals.glbDebugMode = Convert.ToInt16(ConfigurationManager.AppSettings.Get("appDebugMode"));
            Shutdown_Enable();

            pictLoad.Visible = true;
            tmrAguardar.Enabled = true;

            // EventLog.WriteEntry("Totem", "Início Load Form08 " + sTiksExecucaoFormulário);

            // EventLog.WriteEntry("Totem", "Início AtualizarInterfaceParaIdiomaCorrente " + sTiksExecucaoFormulário);

            AtualizarInterfaceParaIdiomaCorrente();

            // EventLog.WriteEntry("Totem", " Fim AtualizarInterfaceParaIdiomaCorrente " + sTiksExecucaoFormulário);

            // EventLog.WriteEntry("Totem", "Início AtualizaTexto " + sTiksExecucaoFormulário);

            AtualizaTexto();

            // EventLog.WriteEntry("Totem", "Fim AtualizaTexto " + sTiksExecucaoFormulário);


            pictLoad.Visible = false;

        }

        private int obtemReservaXML(string xmlEncodedList)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlEncodedList = xmlEncodedList.Replace("xmlns=\"http://www.opentravel.org/OTA/2003/05\"", "");
                xmlDoc.LoadXml(xmlEncodedList);

                string srvGivenName = "";
                string srvEmail = "";
                string srvVehReservation = "";
                string srvVoucherIdentifier = "";
                string srvVoucherValueType = "";
                string srvConfID = "";
                string srvCompanyShortName = "";
                string srvPickUpDateTime = "";
                string srvReturnDateTime = "";
                string srvDistUnitName = "";
                string srvPickUpLocation = "";
                string srvReturnLocation = "";
                string srvPassengerQuantity = "";
                string srvBaggageQuantity = "";
                string srvVehicleCode = "";
                string srvVehicleCodeContext = "";
                string srvVehicleMakeModel = "";
                string srvPictureURL = "";
                string srvVehicleChargeCurrencyCode = "";
                string srvVehicleChargeAmount = "";
                string srvVehicleChargeTaxInclusive = "";
                string srvVehicleChargeGuaranteedInd = "";
                string srvVehicleChargePurpose = "";
                string srvTelephone = "";
                string srvDocument = "";
                string srvNomeLojaCode = "";
                string srvNomeLojaNome = "";
                string srvNomeLojaCodeDev = "";
                string srvNomeLojaNomeDev = "";
                string srvDiaria = "";
                string srvDesconto = "";
                string srvDescontoValor = "";
                string srvRateTotalAmount = "";
                string srvDiariaUnitaria = "";

                Globals globalMetodo = new Globals();

                //Apenas reservas confirmadas serão processadas: ReservationStatus = "CFA"
                List<string> ReservationStatus = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation", "ReservationStatus");
                if (ReservationStatus.Count > 0) Globals.glbVehReservation = ReservationStatus[0].Trim();

                if (!string.IsNullOrEmpty(Globals.glbVehReservation))
                {
                    if (Globals.glbVehReservation.ToUpper() == "CFA")
                    {

                        Globals.glbMOTDES = "CONFIRMADA";
                        Globals.glbRESOPE = "WBS_WEB";

                        Globals.glbGivenName = globalMetodo.returnInnerXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/Customer/Primary/PersonName/GivenName");

                        Globals.glbEmail = globalMetodo.returnInnerXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/Customer/Primary/Email");

                        List<string> VoucherIdentifier = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/Customer/Primary/PaymentForm/Voucher", "Identifier");
                        if (VoucherIdentifier.Count > 0) Globals.glbVoucherIdentifier = VoucherIdentifier[0].Trim();

                        List<string> VoucherValueType = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/Customer/Primary/PaymentForm/Voucher", "ValueType");
                        if (VoucherValueType.Count > 0) Globals.glbVoucherValueType = VoucherValueType[0].Trim();

                        List<string> ConfID = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/ConfID", "ID");
                        if (ConfID.Count > 0) Globals.glbConfID = ConfID[0].Trim();

                        List<string> CompanyShortName = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/ConfID/CompanyName", "CompanyShortName");
                        if (CompanyShortName.Count > 0) Globals.glbCompanyShortName = CompanyShortName[0].Trim();

                        List<string> CompanyNameCode = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/ConfID/CompanyName", "Code");
                        if (CompanyNameCode.Count > 0) Globals.glbCompanyNameCode = CompanyNameCode[0].Trim();

                        List<string> PickUpDateTime = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/VehRentalCore", "PickUpDateTime");
                        if (PickUpDateTime.Count > 0) Globals.glbPickUpDateTime = PickUpDateTime[0].Trim();

                        List<string> ReturnDateTime = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/VehRentalCore", "ReturnDateTime");
                        if (ReturnDateTime.Count > 0) Globals.glbReturnDateTime = ReturnDateTime[0].Trim();

                        List<string> DistUnitName = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/VehRentalCore", "DistUnitName");
                        if (DistUnitName.Count > 0) Globals.glbDistUnitName = DistUnitName[0].Trim();

                        List<string> PickUpLocation = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/VehRentalCore/PickUpLocation", "LocationCode");
                        if (PickUpLocation.Count > 0) Globals.glbPickUpLocation = PickUpLocation[0].Trim();

                        List<string> ReturnLocation = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/VehRentalCore/ReturnLocation", "LocationCode");
                        if (ReturnLocation.Count > 0) Globals.glbReturnLocation = ReturnLocation[0].Trim();

                        List<string> PassengerQuantity = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/Vehicle", "PassengerQuantity");
                        if (PassengerQuantity.Count > 0) Globals.glbPassengerQuantity = PassengerQuantity[0].Trim();

                        List<string> BaggageQuantity = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/Vehicle", "BaggageQuantity");
                        if (BaggageQuantity.Count > 0) Globals.glbBaggageQuantity = BaggageQuantity[0].Trim();

                        List<string> VehicleCode = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/Vehicle", "Code");
                        if (VehicleCode.Count > 0) Globals.glbVehicleCode = VehicleCode[0].Trim();

                        List<string> CodeContext = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/Vehicle", "CodeContext");
                        if (CodeContext.Count > 0) Globals.glbVehicleCodeContext = CodeContext[0].Trim();

                        List<string> VehMakeModelName = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/Vehicle/VehMakeModel", "Name");
                        if (VehMakeModelName.Count > 0) Globals.glbVehicleMakeModel = VehMakeModelName[0].Trim();

                        Globals.glbPictureURL = globalMetodo.returnInnerXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/Vehicle/PictureURL");

                        List<string> VehicleChargeCurrencyCode = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/RentalRate/VehicleCharges/VehicleCharge", "CurrencyCode");
                        if (VehicleChargeCurrencyCode.Count > 0) Globals.glbVehicleChargeCurrencyCode = VehicleChargeCurrencyCode[0].Trim();

                        List<string> VehicleChargeAmount = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/RentalRate/VehicleCharges/VehicleCharge", "Amount");
                        if (VehicleChargeAmount.Count > 0)
                        {
                            Globals.glbVehicleChargeAmount = VehicleChargeAmount[0].Trim();
                            Globals.glbResumoDiariasVar = string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDouble(VehicleChargeAmount[0].Trim().Replace(".", ",")));
                        }

                        decimal vlPricedTaxAmount = 0;
                        List<string> VehicleChargeTaxAmount = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/RentalRate/VehicleCharges/VehicleCharge/TaxAmounts/TaxAmount", "Total");
                        if (VehicleChargeTaxAmount.Count > 0)
                        {
                            int nTaxAmount = VehicleChargeTaxAmount.Count;

                            for (int x = 0; x < nTaxAmount; x++)
                            {
                                vlPricedTaxAmount = vlPricedTaxAmount + Convert.ToDecimal(VehicleChargeTaxAmount[0].Trim().Replace(".", ","));
                            }
                        }

                        // Se valor maior que zero, existe taxa regional
                        if (vlPricedTaxAmount > 0)
                        {
                            decimal vlVehicleChargeAmount = Convert.ToDecimal(Globals.glbVehicleChargeAmount.Trim().Replace(".", ","));
                            vlVehicleChargeAmount = vlVehicleChargeAmount + vlPricedTaxAmount;
                            Globals.glbVehicleChargeAmount = vlVehicleChargeAmount.ToString().Replace(",", ".");
                        }

                        List<string> VehicleChargeQuantity = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/RentalRate/VehicleCharges/VehicleCharge/Calculation", "Quantity");
                        if (VehicleChargeQuantity.Count > 0)
                        {
                            Globals.glbResumoDiariasQtd = VehicleChargeQuantity[0].Trim();
                            Globals.glbRTBDIARIA = VehicleChargeQuantity[0].Trim();
                        }

                        List<string> VehicleChargeUnitCharge = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/RentalRate/VehicleCharges/VehicleCharge/Calculation", "UnitCharge");
                        if (VehicleChargeUnitCharge.Count > 0)
                        {
                            Globals.glbRTBVALDIA = VehicleChargeUnitCharge[0].Trim();
                            Globals.glbRTBSUBTOT = VehicleChargeUnitCharge[0].Trim();
                            Globals.glbResumoDiariasUnit = string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDouble(VehicleChargeUnitCharge[0].Trim().Replace(".", ",")));
                        }


                        // Obtem informações de Equipamentos
                        int nPricedEquip = 0;
                        List<string> PricedEquipDescription = globalMetodo.returnInnerListXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/PricedEquips/PricedEquip/Equipment/Description");
                        if (PricedEquipDescription.Count > 0)
                        {
                            nPricedEquip = PricedEquipDescription.Count;
                            Globals.glbPricedEquipDescription = new string[nPricedEquip];
                            for (int x = 0; x < nPricedEquip; x++)
                            {
                                Globals.glbPricedEquipDescription[x] = PricedEquipDescription[x].Trim();
                            }
                        }
                        List<string> PricedEquipChargeAmount = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/PricedEquips/PricedEquip/Charge", "Amount");
                        if (PricedEquipChargeAmount.Count > 0)
                        {
                            nPricedEquip = PricedEquipChargeAmount.Count;
                            Globals.glbPricedEquipChargeAmount = new string[nPricedEquip];
                            for (int x = 0; x < nPricedEquip; x++)
                            {
                                Globals.glbPricedEquipChargeAmount[x] = string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDouble(PricedEquipChargeAmount[x].Trim().Replace(".", ",")));
                            }
                        }
                        List<string> PricedEquipChargeUnitCharge = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/PricedEquips/PricedEquip/Charge/Calculation", "UnitCharge");
                        if (PricedEquipChargeUnitCharge.Count > 0)
                        {
                            nPricedEquip = PricedEquipChargeUnitCharge.Count;
                            Globals.glbPricedEquipChargeUnitCharge = new string[nPricedEquip];
                            for (int x = 0; x < nPricedEquip; x++)
                            {
                                Globals.glbPricedEquipChargeUnitCharge[x] = string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDouble(PricedEquipChargeUnitCharge[x].Trim().Replace(".", ",")));
                            }
                        }
                        List<string> PricedEquipChargeQuantity = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/PricedEquips/PricedEquip/Charge/Calculation", "Quantity");
                        if (PricedEquipChargeQuantity.Count > 0)
                        {
                            nPricedEquip = PricedEquipChargeQuantity.Count;
                            Globals.glbPricedEquipChargeQuantity = new string[nPricedEquip];
                            for (int x = 0; x < nPricedEquip; x++)
                            {
                                Globals.glbPricedEquipChargeQuantity[x] = PricedEquipChargeQuantity[x].Trim();
                            }
                        }

                        // Obtem informações de Proteções
                        int nPricedCoverage = 0;
                        List<string> PricedCoverageDetails = globalMetodo.returnInnerListXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentInfo/PricedCoverages/PricedCoverage/Coverage/Details");
                        if (PricedCoverageDetails.Count > 0)
                        {
                            nPricedCoverage = PricedCoverageDetails.Count;
                            Globals.glbPricedCoverageDetails = new string[nPricedCoverage];
                            for (int x = 0; x < nPricedCoverage; x++)
                            {
                                Globals.glbPricedCoverageDetails[x] = PricedCoverageDetails[x].Trim();
                            }
                        }

                        List<string> PricedCoverageChargeAmount = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentInfo/PricedCoverages/PricedCoverage/Charge", "Amount");
                        if (PricedCoverageChargeAmount.Count > 0)
                        {
                            nPricedCoverage = PricedCoverageChargeAmount.Count;
                            Globals.glbPricedCoverageChargeAmount = new string[nPricedCoverage];
                            for (int x = 0; x < nPricedCoverage; x++)
                            {
                                Globals.glbPricedCoverageChargeAmount[x] = string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDouble(PricedCoverageChargeAmount[x].Trim().Replace(".", ",")));
                            }
                        }

                        List<string> PricedCoverageChargeCalculationUnitCharge = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentInfo/PricedCoverages/PricedCoverage/Charge/Calculation", "UnitCharge");
                        if (PricedCoverageChargeCalculationUnitCharge.Count > 0)
                        {
                            nPricedCoverage = PricedCoverageChargeCalculationUnitCharge.Count;
                            Globals.glbPricedCoverageChargeCalculationUnitCharge = new string[nPricedCoverage];
                            for (int x = 0; x < nPricedCoverage; x++)
                            {
                                Globals.glbPricedCoverageChargeCalculationUnitCharge[x] = string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDouble(PricedCoverageChargeCalculationUnitCharge[x].Trim().Replace(".", ",")));
                            }
                        }

                        List<string> PricedCoverageChargeCalculationQuantity = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentInfo/PricedCoverages/PricedCoverage/Charge/Calculation", "Quantity");
                        if (PricedCoverageChargeCalculationQuantity.Count > 0)
                        {
                            nPricedCoverage = PricedCoverageChargeCalculationQuantity.Count;
                            Globals.glbPricedCoverageChargeCalculationQuantity = new string[nPricedCoverage];
                            for (int x = 0; x < nPricedCoverage; x++)
                            {
                                Globals.glbPricedCoverageChargeCalculationQuantity[x] = PricedCoverageChargeCalculationQuantity[x].Trim();
                            }
                        }

                        List<string> PricedOffLocServiceCharge = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentInfo/PricedOffLocService/Charge", "Amount");
                        if (PricedOffLocServiceCharge.Count > 0)
                        {
                            Globals.glbResumoDescontoVar = string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDouble(PricedOffLocServiceCharge[0].Trim().Replace(".", ",")));
                            Globals.glbResumoDescontoTotal = PricedOffLocServiceCharge[0].Trim();
                            Globals.glbRTBTOTDSC = PricedOffLocServiceCharge[0].Trim();
                        }

                        List<string> PricedOffLocServiceChargeCalculationQuantity = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentInfo/PricedOffLocService/Charge/Calculation", "Quantity");
                        if (PricedOffLocServiceChargeCalculationQuantity.Count > 0)
                        {
                            Globals.glbResumoDescontoQtd = PricedOffLocServiceChargeCalculationQuantity[0].Trim();
                        }

                        List<string> PricedOffLocServiceChargeCalculationPercentage = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentInfo/PricedOffLocService/Charge/Calculation", "Percentage");
                        if (PricedOffLocServiceChargeCalculationPercentage.Count > 0)
                        {
                            Globals.glbResumoDescontoPercentage = PricedOffLocServiceChargeCalculationPercentage[0].Trim();
                        }

                        decimal nValorDiaria = 0;
                        decimal nValorDesconto = 0;
                        decimal nValorUnit = 0;

                        List<string> PricedOffLocServiceChargeCalculationTotal = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentInfo/PricedOffLocService/Charge", "Amount");
                        if (PricedOffLocServiceChargeCalculationTotal.Count > 0)
                        {
                            int nResumoDiariasQtd = Convert.ToInt32(Globals.glbResumoDiariasQtd);

                            nValorDesconto = Convert.ToDecimal(PricedOffLocServiceChargeCalculationTotal[0].Trim().Replace(".", ","));
                            nValorDiaria = Convert.ToDecimal(Globals.glbVehicleChargeAmount.Trim().Replace(".", ","));
                            nValorDiaria = nValorDesconto + nValorDiaria;
                            nValorUnit = nValorDiaria / nResumoDiariasQtd;

                            Globals.glbResumoDiariasVar = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorDiaria);
                            Globals.glbResumoDiariasUnit = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorUnit);
                        }

                        List<string> LocationDetailsTelephone = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentInfo/LocationDetails/Telephone", "PhoneNumber");
                        if (LocationDetailsTelephone.Count > 0)
                        {
                            Globals.glbLOJTEL = LocationDetailsTelephone[0].Trim();
                        }

                        List<string> DocumentDocID = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/Customer/Primary/Document", "DocID");
                        if (DocumentDocID.Count > 0)
                        {
                            Globals.glbRESDOCCLI = DocumentDocID[0].Trim();
                        }

                        List<string> FeeDescription = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/Fees/Fee", "Description");
                        if (FeeDescription.Count > 0)
                        {
                            int nFeeDescription = FeeDescription.Count;
                            Globals.glbFeeDescription = new string[nFeeDescription];
                            for (int x = 0; x < nFeeDescription; x++)
                            {
                                Globals.glbFeeDescription[x] = FeeDescription[x].Trim();
                            }
                        }

                        List<string> FeeAmount = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/Fees/Fee", "Amount");
                        if (FeeAmount.Count > 0)
                        {
                            int nFeeAmount = FeeAmount.Count;
                            Globals.glbFeeAmount = new string[nFeeAmount];
                            for (int x = 0; x < nFeeAmount; x++)
                            {
                                Globals.glbFeeAmount[x] = string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDecimal(FeeAmount[x].Trim().Replace(".", ",")));
                            }
                        }

                        List<string> FeeAmountUnit = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/Fees/Fee/Calculation", "Quantity");
                        if (FeeAmountUnit.Count > 0)
                        {
                            int nFeeAmountUnit = FeeAmountUnit.Count;
                            Globals.glbFeeAmountUnit = new string[nFeeAmountUnit];
                            for (int x = 0; x < nFeeAmountUnit; x++)
                            {
                                Globals.glbFeeAmountUnit[x] = FeeAmountUnit[x].Trim().Replace(".", ",");
                            }
                        }

                        List<string> FeeUnitCharge = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/Fees/Fee/Calculation", "UnitCharge");
                        if (FeeUnitCharge.Count > 0)
                        {
                            int nFeeUnitCharge = FeeUnitCharge.Count;
                            Globals.glbFeeUnitCharge = new string[nFeeUnitCharge];
                            for (int x = 0; x < nFeeUnitCharge; x++)
                            {
                                Globals.glbFeeUnitCharge[x] = string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDecimal(FeeUnitCharge[x].Trim().Replace(".", ",")));
                            }
                        }

                        List<string> FeeAmountPercentage = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/Fees/Fee/Calculation", "Percentage");
                        if (FeeAmountPercentage.Count > 0)
                        {
                            int nFeeAmountPercentage = FeeAmountPercentage.Count;
                            Globals.glbFeeAmountPercentage = new string[nFeeAmountPercentage];
                            for (int x = 0; x < nFeeAmountPercentage; x++)
                            {
                                Globals.glbFeeAmountPercentage[x] = string.Format(new CultureInfo("pt-BR"), "{0:0%}", Convert.ToDecimal(FeeAmountPercentage[0].Trim().Replace(".", ",")) / 100);
                            }
                        }

                        List<string> LocationDetailsCode = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentInfo/LocationDetails", "Code");
                        if (LocationDetailsCode.Count > 0)
                        {
                            for (int x = 0; x < LocationDetailsCode.Count; x++)
                            {
                                if (x == 0) Globals.glbLOJNOM = LocationDetailsCode[x].Trim();
                                if (x == 1) Globals.glbLOJNOMDEV = LocationDetailsCode[x].Trim();
                            }
                        }

                        List<string> LocationDetailsName = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentInfo/LocationDetails", "Name");
                        if (LocationDetailsName.Count > 0)
                        {
                            for (int x = 0; x < LocationDetailsName.Count; x++)
                            {
                                if (x == 0) Globals.glbLOJDES = LocationDetailsName[x].Trim();
                                if (x == 1) Globals.glbLOJDESDEV = LocationDetailsName[x].Trim();
                            }
                        }

                        List<string> EstimatedTotalAmount = globalMetodo.returnAttributesXml(xmlDoc, @"/OtaVehRetResResult/VehRetResRSCore/VehReservation/VehSegmentCore/TotalCharge", "EstimatedTotalAmount");
                        if (EstimatedTotalAmount.Count > 0)
                        {
                        }

                        return 0;
                    }
                    else
                    {
                        if (Globals.glbVehReservation.ToUpper() == "NSH")
                        {
                            return 4;
                        }
                        else
                            return 3;
                    }
                }
                else
                {
                    return 1;
                }
            }
            catch (Exception ex)
            {
                return 2;
            }
        }


        private void AtualizaTexto()
        {

            // EventLog.WriteEntry("Totem", "AtualizaTexto - Prepara Chamada OTA_VehRetResRQ - linha 486" + sTiksExecucaoFormulário);
            WBS2Z_OTA_GDSSoapClient wsUnidas = new WBS2Z_OTA_GDSSoapClient();
            OTA_VehRetResRQ paramOtaVehRetRes = new OTA_VehRetResRQ();
            OTA_VehRetResRQVehRetResRQCore paramVehRetResRQCore = new OTA_VehRetResRQVehRetResRQCore();
            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai Setar UniqueID_Type[] paramUniqueID = new UniqueID_Type[1] " + sTiksExecucaoFormulário);
            UniqueID_Type[] paramUniqueID = new UniqueID_Type[1];
            paramUniqueID[0] = new UniqueID_Type();
            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai Setar Globals.glbReservaNumeroPesquisa " + sTiksExecucaoFormulário);
            paramUniqueID[0].ID = Globals.glbReservaNumeroPesquisa.Trim();
            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai Setar paramUniqueID[0].ID_Context " + sTiksExecucaoFormulário);
            paramUniqueID[0].ID_Context = "";
            paramVehRetResRQCore.UniqueID = paramUniqueID;
            paramOtaVehRetRes.VehRetResRQCore = paramVehRetResRQCore;

            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai Setar paramOtaVehRetRes.PrimaryLangID " + sTiksExecucaoFormulário);
            paramOtaVehRetRes.PrimaryLangID = "pt-BR";

            SourceType[] sourceType = new SourceType[1];
            sourceType[0] = new SourceType();

            SourceTypeRequestorID requestor = new SourceTypeRequestorID();
            CompanyNameType companyType = new CompanyNameType();

            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai Setar Globals.glbACWTAG " + sTiksExecucaoFormulário);
            companyType.CodeContext = Globals.glbACWTAG.Trim();

            // AQUI 09.03.2016 - DEBUG: Muitas tags pesquisadas na tabela do CodeContext podem estar em branco!
            if (!string.IsNullOrEmpty(Globals.glbACWTAG.Trim()))
            {
                // EventLog.WriteEntry("Totem", "AtualizaTexto - Setou dentro do IF Globals.glbACWTAG " + sTiksExecucaoFormulário);
                companyType.CodeContext = Globals.glbACWTAG.Trim();
            }

            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar requestor.CompanyName " + sTiksExecucaoFormulário);
            requestor.CompanyName = companyType;
            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar requestor " + sTiksExecucaoFormulário);

            sourceType[0].RequestorID = requestor;
            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar POS " + sTiksExecucaoFormulário);

            paramOtaVehRetRes.POS = sourceType;

            Application.DoEvents();

            resOtaVehRetResResult = wsUnidas.OtaVehRetRes(paramOtaVehRetRes);
            if (resOtaVehRetResResult != null)
            {
                Application.DoEvents();
                int nRegistros = resOtaVehRetResResult.Items.Count();
                if (nRegistros > 1)
                {

                    Application.DoEvents();

                    var xmlEncodedList = "";
                    var subReq = new OtaVehRetResResult();
                    subReq = resOtaVehRetResResult;

                    Application.DoEvents();
                    // Busca informações de reserva via XML
                    using (var stream = new MemoryStream())
                    {
                        using (var writerXml = XmlWriter.Create(stream))
                        {
                            new XmlSerializer(subReq.GetType()).Serialize(writerXml, subReq);
                            xmlEncodedList = Encoding.UTF8.GetString(stream.ToArray());
                        }
                    }
                    string _byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
                    if (xmlEncodedList.StartsWith(_byteOrderMarkUtf8))
                    {
                        xmlEncodedList = xmlEncodedList.Remove(0, _byteOrderMarkUtf8.Length);
                    }
                    Application.DoEvents();

                    // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai chamar obtemReservaXML " + sTiksExecucaoFormulário);

                    int iRetServico = obtemReservaXML(xmlEncodedList);
                }
            }

            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai chamar  MostrarOcultarVoucher " + sTiksExecucaoFormulário);

            MostrarOcultarVoucher();


            //lblResumoReserva.Text = "Resumo da Reserva: " + Globals.glbReservaNumeroPesquisa;
            //lblResumoReserva.Refresh();
            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar intLanguage " + sTiksExecucaoFormulário);

            int intLanguage = Globals.glbLanguage;

            //switch (intLanguage)
            //{
            //    case 1:
            //        // Portugues
            //        //lblSaudacoes.Text = "Olá " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Globals.glbGivenName.ToLower()) + ", é um prazer tê-lo(a) como Cliente!";
            //        //lblConfira.Text = "Confira abaixo os detalhes da sua reserva:";
            //        //lblReservaNumero.Text = "Reserva Número:";
            //        //lblLocalDevolucao.Text = "Local da Devolução:";
            //        //lblLocalRetirada.Text = "Local da Retirada:";
            //        //lblDataRetirada.Text = "Data da Retirada:";
            //        //lblDataDevolucao.Text = "Data da Devolução:";
            //        //lblGrupoVeiculo.Text = "Grupo de Veículo:";

            //        MostrarOcultarVoucher();

            //        break;
            //    case 2:
            //        // Espanhol
            //        lblSaudacoes.Text = "Hola " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Globals.glbGivenName.ToLower()) + ", es un placer tenerte como Cliente!";
            //        lblConfira.Text = "Echa los detalles de su reserva:";
            //        lblReservaNumero.Text = "Número de Reserva:";
            //        lblLocalDevolucao.Text = "Local da devolução:";
            //        lblLocalRetirada.Text = "Local da Retirada:";
            //        lblDataRetirada.Text = "Fecha de retiro:";
            //        lblDataDevolucao.Text = "Fecha de regreso:";
            //        lblGrupoVeiculo.Text = "Grupo de vehículo:";

            //        MostrarOcultarVoucher();

            //        break;
            //    case 3:
            //        // Ingles
            //        lblSaudacoes.Text = "Hi " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Globals.glbGivenName.ToLower()) + ", it's a great pleasure to have you as our Customer!";
            //        lblConfira.Text = "Please, check your booking details:";
            //        lblReservaNumero.Text = "Booking number:";
            //        lblLocalRetirada.Text = "Pick-up location:";
            //        lblDataRetirada.Text = "Pick-up date:";
            //        lblLocalDevolucao.Text = "Return location:";
            //        lblDataDevolucao.Text = "Return date:";
            //        lblGrupoVeiculo.Text = "Vehicle group:";

            //        if (!string.IsNullOrEmpty(Globals.glbVoucherIdentifier.Trim()))
            //        {
            //            lblVoucher.Text = "Voucher:";
            //            lblVoucherVar.Text = Globals.glbVoucherIdentifier.Trim();
            //            lblVoucher.Visible = true;
            //            lblVoucherVar.Visible = true;
            //            lblVoucherVar.Refresh();
            //            lblVoucher.Refresh();
            //        }
            //        else
            //        {
            //            lblVoucher.Text = "";
            //            lblVoucherVar.Text = "";
            //            lblVoucher.Visible = false;
            //            lblVoucherVar.Visible = false;
            //            lblVoucherVar.Refresh();
            //            lblVoucher.Refresh();
            //        }
            //        break;
            //    default:
            //        // Portugues
            //        lblSaudacoes.Text = "Olá " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Globals.glbGivenName.ToLower()) + ", é um prazer tê-lo(a) como Cliente!";
            //        lblConfira.Text = "Confira abaixo os detalhes da sua reserva:";
            //        lblReservaNumero.Text = "Reserva Número:";
            //        lblLocalDevolucao.Text = "Local da Devolução:";
            //        lblLocalRetirada.Text = "Local da Retirada:";
            //        lblDataRetirada.Text = "Data da Retirada:";
            //        lblDataDevolucao.Text = "Data da Devolução:";
            //        lblGrupoVeiculo.Text = "Grupo de Veículo:";

            //        if (!string.IsNullOrEmpty(Globals.glbVoucherIdentifier.Trim()))
            //        {
            //            lblVoucher.Text = "Voucher:";
            //            lblVoucherVar.Text = Globals.glbVoucherIdentifier.Trim();
            //            lblVoucher.Visible = true;
            //            lblVoucherVar.Visible = true;
            //            lblVoucherVar.Refresh();
            //            lblVoucher.Refresh();
            //        }
            //        else
            //        {
            //            lblVoucher.Text = "";
            //            lblVoucherVar.Text = "";
            //            lblVoucher.Visible = false;
            //            lblVoucherVar.Visible = false;
            //            lblVoucherVar.Refresh();
            //            lblVoucher.Refresh();
            //        }
            //        break;
            //}
            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar dtData1 " + sTiksExecucaoFormulário);

            DateTime dtData1 = Convert.ToDateTime(Globals.glbPickUpDateTime);

            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar dtData2 " + sTiksExecucaoFormulário);

            DateTime dtData2 = Convert.ToDateTime(Globals.glbReturnDateTime);

            string strData1 = "";
            string strData2 = "";

            CultureInfo culture = new CultureInfo("pt-BR");
            switch (intLanguage)
            {
                case 1:
                    // Portugues
                    culture = new CultureInfo("pt-BR");

                    // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar strData1  Portuques " + sTiksExecucaoFormulário);
                    strData1 = dtData1.ToString("dd/MM/yyyy HH:mm", culture);
                    // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar strData2 Portuques " + sTiksExecucaoFormulário);
                    strData2 = dtData2.ToString("dd/MM/yyyy HH:mm", culture);
                    break;
                case 2:
                    // Espanhol
                    culture = new CultureInfo("Es-es");
                    // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar strData1  Espanhol " + sTiksExecucaoFormulário);
                    strData1 = dtData1.ToString("dd/MM/yyyy HH:mm", culture);
                    // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar strData2 Espanhol " + sTiksExecucaoFormulário);
                    strData2 = dtData2.ToString("dd/MM/yyyy HH:mm", culture);
                    break;
                case 3:
                    // Ingles
                    culture = new CultureInfo("en-US");
                    // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar strData1  Inglês " + sTiksExecucaoFormulário);
                    strData1 = dtData1.ToString("MM/dd/yyyy hh:mm", culture);
                    // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar strData2  Inglês " + sTiksExecucaoFormulário);
                    strData2 = dtData2.ToString("MM/dd/yyyy hh:mm", culture);
                    break;
                default:
                    // Portugues
                    culture = new CultureInfo("pt-BR");
                    strData1 = dtData1.ToString("dd/MM/yyyy HH:mm", culture);
                    strData2 = dtData2.ToString("dd/MM/yyyy HH:mm", culture);
                    break;
            }

            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar lblReservaNumeroVar " + sTiksExecucaoFormulário);
            lblReservaNumeroVar.Text = Globals.glbConfID;
            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar lblDataRetiradaVar " + sTiksExecucaoFormulário);
            lblDataRetiradaVar.Text = strData1;
            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar lblDataDevolucaoVar " + sTiksExecucaoFormulário);
            lblDataDevolucaoVar.Text = strData2;

            lblCategoria.Location = new Point(300, lblCategoria.Location.Y);
            lblCategoria.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Globals.glbVehicleMakeModel.ToLower());
            lblCategoria.Refresh();

            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar pictGrupoVeiculo" + sTiksExecucaoFormulário);
            pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.GrupoIndisponivel;
            pictGrupoVeiculo.Refresh();

            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar lblLocalRetiradaVar " + sTiksExecucaoFormulário);
            lblLocalRetiradaVar.Text = Globals.glbLOJDES.Trim();
            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar lblLocalDevolucaoVar " + sTiksExecucaoFormulário);
            lblLocalDevolucaoVar.Text = Globals.glbLOJDESDEV.Trim();

            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar lblGrupoVeiculoVar " + sTiksExecucaoFormulário);
            lblGrupoVeiculoVar.Text = Globals.glbResGruCod + " - " + Globals.glbVehicleMakeModel;


            switch (Globals.glbResGruCod)
            {
                case "A":
                    pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.GrupoA_NovoUno;
                    pictGrupoVeiculo.Refresh();
                    break;
                case "AT":
                    pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.GrupoAT_Focus;
                    pictGrupoVeiculo.Refresh();
                    break;
                case "B-C3":
                    pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.GrupoB_C3;
                    pictGrupoVeiculo.Refresh();
                    break;
                case "BL":
                    pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.GrupoBL_Fusion;
                    pictGrupoVeiculo.Refresh();
                    break;
                case "B":
                    pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.GrupoB_NewMarch;
                    pictGrupoVeiculo.Refresh();
                    break;
                case "CA":
                    pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.GrupoCA_Logan;
                    pictGrupoVeiculo.Refresh();
                    break;
                case "CG":
                    pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.GrupoCG_Kangoo;
                    pictGrupoVeiculo.Refresh();
                    break;
                case "C":
                    pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.GrupoC_Versa;
                    pictGrupoVeiculo.Refresh();
                    break;
                case "D":
                    pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.GrupoD_C3PICASSO;
                    pictGrupoVeiculo.Refresh();
                    break;
                case "E":
                    pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.GrupoE_KA;
                    pictGrupoVeiculo.Refresh();
                    break;
                case "F":
                    pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.GrupoF_Cruze;
                    pictGrupoVeiculo.Refresh();
                    break;
                case "G1":
                    pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.GrupoG1_FiatStrada;
                    pictGrupoVeiculo.Refresh();
                    break;
                case "G":
                    pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.GrupoG_Saveiro;
                    pictGrupoVeiculo.Refresh();
                    break;
                case "I":
                    pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.GrupoI;
                    pictGrupoVeiculo.Refresh();
                    break;
                case "J":
                    pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.GrupoJ_Ranger;
                    pictGrupoVeiculo.Refresh();
                    break;
                case "SL":
                    pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.GrupoSL_AudiA4;
                    pictGrupoVeiculo.Refresh();
                    break;
                case "SV":
                    pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.GrupoSV_Duster;
                    pictGrupoVeiculo.Refresh();
                    break;
                case "S":
                    pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.GrupoS_Voyage;
                    pictGrupoVeiculo.Refresh();
                    break;
                case "UP":
                    pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.GrupoUP_UP;
                    pictGrupoVeiculo.Refresh();
                    break;
                default:
                    pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.GrupoIndisponivel;
                    pictGrupoVeiculo.Refresh();
                    break;
            }

            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar strTipo " + sTiksExecucaoFormulário);
            string strTipo = Globals.glbMODDES.Trim().ToUpper();
            pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.GrupoIndisponivel;

            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar pictGrupoVeiculo " + sTiksExecucaoFormulário);
            if (strTipo.Contains("A4")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.AudiA4;
            if (strTipo.Contains("C3")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.C3Picasso;
            if (strTipo.Contains("CLASSIC")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.Classic34;
            if (strTipo.Contains("UNO")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.Unobranco4portas;
            if (strTipo.Contains("PALIO")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.Palio2portasmerged;
            if (strTipo.Contains("ATTRACTIVE")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.FiatPalioAttractive;
            if (strTipo.Contains("ADVENTURE")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.FiatStradaAdventure2014;
            if (strTipo.Contains("STRADA")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.FiatStradaAdventure2014;
            if (strTipo.Contains("PUNTO")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.Puntomerged;
            if (strTipo.Contains("WORKING")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.FiatStradaWorking2014DIR;
            if (strTipo.Contains("FIESTA")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.Fiesta3_4SedanFly;
            if (strTipo.Contains("FOCUS")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.Focus3_4sedanGlx;
            if (strTipo.Contains("FUSION")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.FordFusion2014;
            if (strTipo.Contains("KA")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.FORDKA3_42014;
            if (strTipo.Contains("RANGER")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.FORDRANGER;
            if (strTipo.Contains("ONIX")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.GMBRAOnix_3_4;
            if (strTipo.Contains("PRISMA")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.GMBRAPrisma3_4;
            if (strTipo.Contains("CRUZE")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.GMCruzeSedan2014;
            if (strTipo.Contains("GOL")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.Gol_G6_VW;
            if (strTipo.Contains("G6")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.Gol_G6_VW;
            if (strTipo.Contains("G5")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.Gol_G5_VW;
            if (strTipo.Contains("LINEA")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.LineaLX1_8FlexMerged;
            if (strTipo.Contains("MARCH")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.March_SL_Prata;
            if (strTipo.Contains("TRITON")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.Mitsubishil200_triton;
            if (strTipo.Contains("FRONTIER")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.NissanFrontier;
            if (strTipo.Contains("LIVINA")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.NissanLivina1;
            if (strTipo.Contains("GRAND LIVINA")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.NissanGrandLivina;
            if (strTipo.Contains("SENTRA")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.NissanSentra;
            if (strTipo.Contains("408")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.Peugeot408;
            if (strTipo.Contains("POLO")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.Polo3_4hatch;
            if (strTipo.Contains("KANGOO")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.Renault_KangooVU_ComPorta;
            if (strTipo.Contains("LOGAN")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.Renault_Logan_2014;
            if (strTipo.Contains("SANDERO")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.Renault_Sandero_2015;
            if (strTipo.Contains("FLUENCE")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.RenaultFluence;
            if (strTipo.Contains("S10")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.S103_4;
            if (strTipo.Contains("SPIN")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.SpinGM;
            if (strTipo.Contains("CAMRY")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.ToyotaCAMRY2015;
            if (strTipo.Contains("VECTRA")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.Vectamerged;
            if (strTipo.Contains("VERSA")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.Versa_Prata_SL;
            if (strTipo.Contains("AMAROK")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.VWAmarokTrendline;
            if (strTipo.Contains("FOX")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.VWFoxPrime_4P;
            if (strTipo.Contains("JETTA")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.VWJetta;
            if (strTipo.Contains("KOMBI")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.VWKombi;
            if (strTipo.Contains("SAVEIRO")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.VW_Saveiro;
            if (strTipo.Contains("UP")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.VWUP;
            if (strTipo.Contains("VOYAGE")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.VWVOYAGE_PRATA;
            if (strTipo.Contains("COBALT")) pictGrupoVeiculo.Image = centralUnidas.Properties.Resources.GM_Cobalt_LTZ;

            pictGrupoVeiculo.Refresh();

            // Totais
            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai zerar totais " + sTiksExecucaoFormulário);
            decimal totProtecoes = 0;
            decimal totServicos = 0;
            decimal totDiarias = 0;
            decimal totDesconto = 0;
            decimal totAdministrativo = 0;
            decimal totHoraExtra = 0;

            string strServicos = "";
            string strDiarias = "";
            string strDesconto = "";
            string strAdministrativo = "";
            string strHoraExtra = "";

            totServicos = 0;
            int nAcessorio = 0;
            // EventLog.WriteEntry("Totem", "AtualizaTexto - Globals.glbPricedEquipDescription != null) nAcessorio = Globals.glbPricedEquipDescription.Count()  " + sTiksExecucaoFormulário);
            if (Globals.glbPricedEquipDescription != null) nAcessorio = Globals.glbPricedEquipDescription.Count();
            if (nAcessorio > 0)
            {
                // EventLog.WriteEntry("Totem", "AtualizaTexto - string.IsNullOrEmpty(Globals.glbPricedEquipDescription[0]) " + sTiksExecucaoFormulário);
                if (string.IsNullOrEmpty(Globals.glbPricedEquipDescription[0]))
                    lblAcessoriosVar1.Text = "Não Contratado";
                else
                {
                    // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar lblAcessoriosVar1 " + sTiksExecucaoFormulário);
                    lblAcessoriosVar1.Text = Globals.glbPricedEquipDescription[0].Trim();

                    // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar lblAcessoriosQtd1 " + sTiksExecucaoFormulário);
                    lblAcessoriosQtd1.Text = Globals.glbPricedEquipChargeQuantity[0].Trim();

                    // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar lblAcessoriosUnit1 " + sTiksExecucaoFormulário);
                    lblAcessoriosUnit1.Text = Globals.glbPricedEquipChargeUnitCharge[0].Trim();

                    // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar lblAcessoriosTot1 " + sTiksExecucaoFormulário);
                    lblAcessoriosTot1.Text = Globals.glbPricedEquipChargeAmount[0].Trim();

                    // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar strServicos " + sTiksExecucaoFormulário);
                    strServicos = Globals.glbPricedEquipChargeAmount[0].Trim().Replace("R$", "");
                    strServicos = strServicos.Replace(" ", "");
                    totServicos = totServicos + Convert.ToDecimal(strServicos);
                }
            }
            else
                lblAcessoriosVar1.Text = "Não Contratado";

            if (nAcessorio > 1)
            {
                // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar lblAcessoriosVar2 " + sTiksExecucaoFormulário);

                lblAcessoriosVar2.Text = Globals.glbPricedEquipDescription[1].Trim();

                // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar lblAcessoriosQtd2 " + sTiksExecucaoFormulário);

                lblAcessoriosQtd2.Text = Globals.glbPricedEquipChargeQuantity[1].Trim();

                // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar lblAcessoriosUnit2 " + sTiksExecucaoFormulário);

                lblAcessoriosUnit2.Text = Globals.glbPricedEquipChargeUnitCharge[1].Trim();

                // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar lblAcessoriosTot2 " + sTiksExecucaoFormulário);

                lblAcessoriosTot2.Text = Globals.glbPricedEquipChargeAmount[1].Trim();

                // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar strServicos " + sTiksExecucaoFormulário);

                strServicos = Globals.glbPricedEquipChargeAmount[1].Trim().Replace("R$", "");
                strServicos = strServicos.Replace(" ", "");
                totServicos = totServicos + Convert.ToDecimal(strServicos);
            }

            if (nAcessorio > 2)
            {
                // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar lblAcessoriosVar3 " + sTiksExecucaoFormulário);

                lblAcessoriosVar3.Text = Globals.glbPricedEquipDescription[2].Trim();

                // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar lblAcessoriosQtd3 " + sTiksExecucaoFormulário);

                lblAcessoriosQtd3.Text = Globals.glbPricedEquipChargeQuantity[2].Trim();

                // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar lblAcessoriosUnit3 " + sTiksExecucaoFormulário);

                lblAcessoriosUnit3.Text = Globals.glbPricedEquipChargeUnitCharge[2].Trim();

                // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar lblAcessoriosTot3 " + sTiksExecucaoFormulário);

                lblAcessoriosTot3.Text = Globals.glbPricedEquipChargeAmount[2].Trim();

                // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar strServicos " + sTiksExecucaoFormulário);

                strServicos = Globals.glbPricedEquipChargeAmount[2].Trim().Replace("R$", "");
                strServicos = strServicos.Replace(" ", "");
                totServicos = totServicos + Convert.ToDecimal(strServicos);
            }

            if (nAcessorio > 3)
            {

                // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar lblAcessoriosVar4 " + sTiksExecucaoFormulário);

                lblAcessoriosVar4.Text = Globals.glbPricedEquipDescription[3].Trim();

                // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar lblAcessoriosQtd4 " + sTiksExecucaoFormulário);

                lblAcessoriosQtd4.Text = Globals.glbPricedEquipChargeQuantity[3].Trim();

                // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar lblAcessoriosUnit4 " + sTiksExecucaoFormulário);

                lblAcessoriosUnit4.Text = Globals.glbPricedEquipChargeUnitCharge[3].Trim();

                // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar lblAcessoriosTot4 " + sTiksExecucaoFormulário);

                lblAcessoriosTot4.Text = Globals.glbPricedEquipChargeAmount[3].Trim();

                // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar strServicos " + sTiksExecucaoFormulário);
                strServicos = Globals.glbPricedEquipChargeAmount[3].Trim().Replace("R$", "");
                strServicos = strServicos.Replace(" ", "");
                totServicos = totServicos + Convert.ToDecimal(strServicos);
            }

            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar strDiarias " + sTiksExecucaoFormulário);
            strDiarias = Globals.glbResumoDiariasVar.Trim().Replace("R$", "");
            strDiarias = strDiarias.Replace(" ", "");
            totDiarias = Convert.ToDecimal(strDiarias);

            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar lblDiariaVar " + sTiksExecucaoFormulário);
            lblDiariaVar.Text = Globals.glbResumoDiariasVar.Trim();
            lblDiariaVar.Refresh();

            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar lblDiariaUnit " + sTiksExecucaoFormulário);
            lblDiariaUnit.Text = Globals.glbResumoDiariasUnit.Trim();
            lblDiariaUnit.Refresh();

            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar lblDiariaQtd " + sTiksExecucaoFormulário);
            lblDiariaQtd.Text = Globals.glbResumoDiariasQtd.Trim();
            lblDiariaQtd.Refresh();

            // EventLog.WriteEntry("Totem", "AtualizaTexto - If (!string.IsNullOrEmpty(Globals.glbResumoDescontoVar)" + sTiksExecucaoFormulário);

            if (!string.IsNullOrEmpty(Globals.glbResumoDescontoVar))
            {
                // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar lblDescontoVar )" + sTiksExecucaoFormulário);
                lblDescontoVar.Text = "- " + Globals.glbResumoDescontoVar.Trim();

                // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar strDesconto )" + sTiksExecucaoFormulário);
                strDesconto = Globals.glbResumoDescontoVar.Trim().Replace("R$", "");
                strDesconto = strDesconto.Replace(" ", "");
                totDesconto = Convert.ToDecimal(strDesconto);

                // EventLog.WriteEntry("Totem", "AtualizaTexto - if (totDesconto == 0) " + sTiksExecucaoFormulário);
                if (totDesconto == 0)
                {
                    lblDescontoVar.Visible = false;
                    lblDescontoVar.Refresh();
                }
            }
            else
                lblDescontoVar.Text = "";

            // inicio - Recarrega proteções

            int nProtecoes = 0;

            // EventLog.WriteEntry("Totem", "AtualizaTexto - if (Globals.glbPricedCoverageDetails != null) " + sTiksExecucaoFormulário);
            if (Globals.glbPricedCoverageDetails != null)
            {
                // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar nProtecoes " + sTiksExecucaoFormulário);

                nProtecoes = Globals.glbPricedCoverageDetails.Length;
                Globals.glbPricedCoverageCode = new string[nProtecoes];

                for (int x = 0; x < nProtecoes; x++)
                {
                    // EventLog.WriteEntry("Totem", "AtualizaTexto - if (!string.IsNullOrEmpty(Globals.glbProtecoesVar1.Trim())) " + sTiksExecucaoFormulário);

                    if (!string.IsNullOrEmpty(Globals.glbProtecoesVar1.Trim()))
                    {
                        // EventLog.WriteEntry("Totem", "AtualizaTexto - RemoverAcentos " + sTiksExecucaoFormulário);

                        if (this.RemoverAcentos(Globals.glbPricedCoverageDetails[x].Trim()).ToUpper().Contains(this.RemoverAcentos(Globals.glbProtecoesVar1.Trim().ToUpper())))
                        {
                            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar glbPricedCoverageCode[x] " + sTiksExecucaoFormulário);

                            Globals.glbPricedCoverageCode[x] = Globals.glbProtecoesCodVar1.Trim();
                        }
                    }
                    // EventLog.WriteEntry("Totem", "AtualizaTexto - if (!string.IsNullOrEmpty(Globals.glbProtecoesVar2.Trim())) " + sTiksExecucaoFormulário);
                    if (!string.IsNullOrEmpty(Globals.glbProtecoesVar2.Trim()))
                    {
                        if (this.RemoverAcentos(Globals.glbPricedCoverageDetails[x].Trim()).ToUpper().Contains(this.RemoverAcentos(Globals.glbProtecoesVar2.Trim().ToUpper())))
                        {
                            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar glbPricedCoverageCode[x] " + sTiksExecucaoFormulário);

                            Globals.glbPricedCoverageCode[x] = Globals.glbProtecoesCodVar2.Trim();
                        }
                    }
                    // EventLog.WriteEntry("Totem", "AtualizaTexto - if (!string.IsNullOrEmpty(Globals.glbProtecoesVar3.Trim())) " + sTiksExecucaoFormulário);
                    if (!string.IsNullOrEmpty(Globals.glbProtecoesVar3.Trim()))
                    {
                        if (this.RemoverAcentos(Globals.glbPricedCoverageDetails[x].Trim()).ToUpper().Contains(this.RemoverAcentos(Globals.glbProtecoesVar3.Trim().ToUpper())))
                        {
                            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar glbPricedCoverageCode[x] " + sTiksExecucaoFormulário);
                            Globals.glbPricedCoverageCode[x] = Globals.glbProtecoesCodVar3.Trim();
                        }
                    }

                    // EventLog.WriteEntry("Totem", "AtualizaTexto - if (!string.IsNullOrEmpty(Globals.glbProtecoesVar4.Trim())) " + sTiksExecucaoFormulário);
                    if (!string.IsNullOrEmpty(Globals.glbProtecoesVar4.Trim()))
                    {
                        if (this.RemoverAcentos(Globals.glbPricedCoverageDetails[x].Trim()).ToUpper().Contains(this.RemoverAcentos(Globals.glbProtecoesVar4.Trim().ToUpper())))
                        {
                            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar glbPricedCoverageCode[x] " + sTiksExecucaoFormulário);

                            Globals.glbPricedCoverageCode[x] = Globals.glbProtecoesCodVar4.Trim();
                        }
                    }
                    // EventLog.WriteEntry("Totem", "AtualizaTexto - if (!string.IsNullOrEmpty(Globals.glbProtecoesVar5.Trim())) " + sTiksExecucaoFormulário);
                    if (!string.IsNullOrEmpty(Globals.glbProtecoesVar5.Trim()))
                    {
                        if (this.RemoverAcentos(Globals.glbPricedCoverageDetails[x].Trim()).ToUpper().Contains(this.RemoverAcentos(Globals.glbProtecoesVar5.Trim().ToUpper())))
                        {
                            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar glbPricedCoverageCode[x] " + sTiksExecucaoFormulário);

                            Globals.glbPricedCoverageCode[x] = Globals.glbProtecoesCodVar5.Trim();
                        }
                    }
                    // EventLog.WriteEntry("Totem", "AtualizaTexto - if (!string.IsNullOrEmpty(Globals.glbProtecoesVar6.Trim())) " + sTiksExecucaoFormulário);
                    if (!string.IsNullOrEmpty(Globals.glbProtecoesVar6.Trim()))
                    {
                        if (this.RemoverAcentos(Globals.glbPricedCoverageDetails[x].Trim()).ToUpper().Contains(this.RemoverAcentos(Globals.glbProtecoesVar6.Trim().ToUpper())))
                        {
                            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar glbPricedCoverageCode[x] " + sTiksExecucaoFormulário);
                            Globals.glbPricedCoverageCode[x] = Globals.glbProtecoesCodVar6.Trim();
                        }
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "AtualizaTexto - Vai setar Zerar Variáveis de Proteção " + sTiksExecucaoFormulário);

            Globals.glbProtecoesValorVar1 = "";
            Globals.glbProtecoesVar1 = "";
            Globals.glbProtecoesCodVar1 = "";
            Globals.glbProtecoesQtdUnit1 = "";

            Globals.glbProtecoesValorVar2 = "";
            Globals.glbProtecoesVar2 = "";
            Globals.glbProtecoesCodVar2 = "";
            Globals.glbProtecoesQtdUnit2 = "";

            Globals.glbProtecoesValorVar3 = "";
            Globals.glbProtecoesVar3 = "";
            Globals.glbProtecoesCodVar3 = "";
            Globals.glbProtecoesQtdUnit3 = "";

            Globals.glbProtecoesValorVar4 = "";
            Globals.glbProtecoesVar4 = "";
            Globals.glbProtecoesCodVar4 = "";
            Globals.glbProtecoesQtdUnit4 = "";

            Globals.glbProtecoesValorVar5 = "";
            Globals.glbProtecoesVar5 = "";
            Globals.glbProtecoesCodVar5 = "";
            Globals.glbProtecoesQtdUnit5 = "";

            Globals.glbProtecoesValorVar6 = "";
            Globals.glbProtecoesVar6 = "";
            Globals.glbProtecoesCodVar6 = "";
            Globals.glbProtecoesQtdUnit6 = "";

            // 

            int nCarrega = 0;

            // EventLog.WriteEntry("Totem", "AtualizaTexto -  for (int y = 0; y < nProtecoes; y++)  " + sTiksExecucaoFormulário);
            for (int y = 0; y < nProtecoes; y++)
            {
                // EventLog.WriteEntry("Totem", "AtualizaTexto -  if (!string.IsNullOrEmpty(Globals.glbPricedCoverageCode[y].Trim())) " + sTiksExecucaoFormulário);

                if (!string.IsNullOrEmpty(Globals.glbPricedCoverageCode[y].Trim()))
                {
                    if (nCarrega == 0)
                    {
                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  glbProtecoesValorVar1 " + sTiksExecucaoFormulário);
                        Globals.glbProtecoesValorVar1 = Globals.glbPricedCoverageChargeAmount[y].Trim();
                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  glbProtecoesVar1 " + sTiksExecucaoFormulário);
                        Globals.glbProtecoesVar1 = Globals.glbPricedCoverageDetails[y].Trim();
                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  glbProtecoesCodVar1 " + sTiksExecucaoFormulário);
                        Globals.glbProtecoesCodVar1 = Globals.glbPricedCoverageCode[y].Trim();
                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  glbProtecoesQtdUnit1 " + sTiksExecucaoFormulário);
                        Globals.glbProtecoesQtdUnit1 = Globals.glbPricedCoverageChargeCalculationQuantity[y].Trim();
                    }
                    if (nCarrega == 1)
                    {
                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  glbProtecoesValorVar2 " + sTiksExecucaoFormulário);

                        Globals.glbProtecoesValorVar2 = Globals.glbPricedCoverageChargeAmount[y].Trim();
                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  glbProtecoesVar2 " + sTiksExecucaoFormulário);

                        Globals.glbProtecoesVar2 = Globals.glbPricedCoverageDetails[y].Trim();

                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  glbProtecoesCodVar2 " + sTiksExecucaoFormulário);

                        Globals.glbProtecoesCodVar2 = Globals.glbPricedCoverageCode[y].Trim();
                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  glbProtecoesQtdUnit2 " + sTiksExecucaoFormulário);

                        Globals.glbProtecoesQtdUnit2 = Globals.glbPricedCoverageChargeCalculationQuantity[y].Trim();
                    }
                    if (nCarrega == 2)
                    {
                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  glbProtecoesValorVar3 " + sTiksExecucaoFormulário);

                        Globals.glbProtecoesValorVar3 = Globals.glbPricedCoverageChargeAmount[y].Trim();

                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  glbProtecoesVar3 " + sTiksExecucaoFormulário);

                        Globals.glbProtecoesVar3 = Globals.glbPricedCoverageDetails[y].Trim();

                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  glbProtecoesCodVar3 " + sTiksExecucaoFormulário);

                        Globals.glbProtecoesCodVar3 = Globals.glbPricedCoverageCode[y].Trim();

                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  glbProtecoesQtdUnit3 " + sTiksExecucaoFormulário);

                        Globals.glbProtecoesQtdUnit3 = Globals.glbPricedCoverageChargeCalculationQuantity[y].Trim();
                    }
                    if (nCarrega == 3)
                    {
                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  glbProtecoesValorVar4 " + sTiksExecucaoFormulário);

                        Globals.glbProtecoesValorVar4 = Globals.glbPricedCoverageChargeAmount[y].Trim();

                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  glbProtecoesVar4 " + sTiksExecucaoFormulário);

                        Globals.glbProtecoesVar4 = Globals.glbPricedCoverageDetails[y].Trim();

                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  glbProtecoesCodVar4" + sTiksExecucaoFormulário);

                        Globals.glbProtecoesCodVar4 = Globals.glbPricedCoverageCode[y].Trim();

                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  glbProtecoesQtdUnit4 " + sTiksExecucaoFormulário);

                        Globals.glbProtecoesQtdUnit4 = Globals.glbPricedCoverageChargeCalculationQuantity[y].Trim();
                    }
                    if (nCarrega == 4)
                    {
                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  glbProtecoesValorVar5 " + sTiksExecucaoFormulário);
                        Globals.glbProtecoesValorVar5 = Globals.glbPricedCoverageChargeAmount[y].Trim();

                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  glbProtecoesVar5 " + sTiksExecucaoFormulário);

                        Globals.glbProtecoesVar5 = Globals.glbPricedCoverageDetails[y].Trim();

                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  glbProtecoesCodVar5" + sTiksExecucaoFormulário);

                        Globals.glbProtecoesCodVar5 = Globals.glbPricedCoverageCode[y].Trim();

                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  glbProtecoesQtdUnit5 " + sTiksExecucaoFormulário);

                        Globals.glbProtecoesQtdUnit5 = Globals.glbPricedCoverageChargeCalculationQuantity[y].Trim();
                    }
                    if (nCarrega == 5)
                    {
                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  glbProtecoesValorVar6 " + sTiksExecucaoFormulário);
                        Globals.glbProtecoesValorVar6 = Globals.glbPricedCoverageChargeAmount[y].Trim();
                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  glbProtecoesVar6 " + sTiksExecucaoFormulário);

                        Globals.glbProtecoesVar6 = Globals.glbPricedCoverageDetails[y].Trim();

                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  glbProtecoesCodVar6 " + sTiksExecucaoFormulário);

                        Globals.glbProtecoesCodVar6 = Globals.glbPricedCoverageCode[y].Trim();

                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  glbProtecoesQtdUnit6 " + sTiksExecucaoFormulário);

                        Globals.glbProtecoesQtdUnit6 = Globals.glbPricedCoverageChargeCalculationQuantity[y].Trim();
                    }
                    nCarrega = nCarrega + 1;
                }
            }
            // fim - Recarrega proteções

            // EventLog.WriteEntry("Totem", "AtualizaTexto -  if (!string.IsNullOrEmpty(Globals.glbProtecoesValorVar1))  " + sTiksExecucaoFormulário);

            if (!string.IsNullOrEmpty(Globals.glbProtecoesValorVar1))
            {
                // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  strValorProt1 " + sTiksExecucaoFormulário);

                string strValorProt1 = Globals.glbProtecoesValorVar1.Trim().Replace("R$", "");
                strValorProt1 = strValorProt1.Replace(" ", "");

                // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  totProtecoes " + sTiksExecucaoFormulário);

                totProtecoes = totProtecoes + Convert.ToDecimal(strValorProt1);

                // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  lblProtecoesTot1 " + sTiksExecucaoFormulário);

                lblProtecoesTot1.Text = Globals.glbProtecoesValorVar1.Trim();

                // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  lblProtecoesQtd1 " + sTiksExecucaoFormulário);

                lblProtecoesQtd1.Text = Globals.glbProtecoesQtdUnit1.Trim();

                int nQtd1 = 1;

                // EventLog.WriteEntry("Totem", "AtualizaTexto - if (!string.IsNullOrEmpty(Globals.glbProtecoesQtdUnit1.Trim()))  " + sTiksExecucaoFormulário);

                if (!string.IsNullOrEmpty(Globals.glbProtecoesQtdUnit1.Trim()))
                {
                    // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  nQtd1  " + sTiksExecucaoFormulário);

                    nQtd1 = Convert.ToInt32(Globals.glbProtecoesQtdUnit1.Trim());
                }
                else
                {
                    // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  nQtd1  no Else " + sTiksExecucaoFormulário);

                    nQtd1 = Convert.ToInt32(Globals.glbResumoDiariasQtd.Trim());
                }

                // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  valorUnit1 " + sTiksExecucaoFormulário);

                decimal valorUnit1 = Convert.ToDecimal(strValorProt1) / nQtd1;

                // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  lblProtecoesUnit1 " + sTiksExecucaoFormulário);

                lblProtecoesUnit1.Text = string.Format(new CultureInfo("pt-BR"), "{0:C}", valorUnit1);

                lblProtecoesTot1.Refresh();
                lblProtecoesQtd1.Refresh();
                lblProtecoesUnit1.Refresh();

            }
            else
            {
                lblProtecoesTot1.Text = "";
                lblProtecoesQtd1.Text = "";
                lblProtecoesUnit1.Text = "";
            }

            // EventLog.WriteEntry("Totem", "AtualizaTexto -  if (!string.IsNullOrEmpty(Globals.glbProtecoesValorVar2)) " + sTiksExecucaoFormulário);

            if (!string.IsNullOrEmpty(Globals.glbProtecoesValorVar2))
            {
                // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  strValorProt2 " + sTiksExecucaoFormulário);

                string strValorProt2 = Globals.glbProtecoesValorVar2.Trim().Replace("R$", "");
                strValorProt2 = strValorProt2.Replace(" ", "");

                // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  totProtecoes " + sTiksExecucaoFormulário);

                totProtecoes = totProtecoes + Convert.ToDecimal(strValorProt2);

                // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  lblProtecoesTot2 " + sTiksExecucaoFormulário);

                lblProtecoesTot2.Text = Globals.glbProtecoesValorVar2.Trim();

                // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  lblProtecoesQtd2 " + sTiksExecucaoFormulário);

                lblProtecoesQtd2.Text = Globals.glbProtecoesQtdUnit2.Trim();

                // EventLog.WriteEntry("Totem", "AtualizaTexto - if (!string.IsNullOrEmpty(Globals.glbProtecoesQtdUnit2.Trim())) " + sTiksExecucaoFormulário);

                int nQtd2 = 1;
                if (!string.IsNullOrEmpty(Globals.glbProtecoesQtdUnit2.Trim()))
                {
                    // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  nQtd2 " + sTiksExecucaoFormulário);

                    nQtd2 = Convert.ToInt32(Globals.glbProtecoesQtdUnit2.Trim());
                }
                else
                {
                    // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  nQtd2 Else" + sTiksExecucaoFormulário);

                    nQtd2 = Convert.ToInt32(Globals.glbResumoDiariasQtd.Trim());
                }

                // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  valorUnit2 " + sTiksExecucaoFormulário);

                decimal valorUnit2 = Convert.ToDecimal(strValorProt2) / nQtd2;

                // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  lblProtecoesUnit2 " + sTiksExecucaoFormulário);

                lblProtecoesUnit2.Text = string.Format(new CultureInfo("pt-BR"), "{0:C}", valorUnit2);
            }
            else
            {
                lblProtecoesTot2.Text = "";
                lblProtecoesQtd2.Text = "";
                lblProtecoesUnit2.Text = "";
            }
            lblProtecoesTot2.Refresh();
            lblProtecoesQtd2.Refresh();
            lblProtecoesUnit2.Refresh();

            // EventLog.WriteEntry("Totem", "AtualizaTexto -  if (!string.IsNullOrEmpty(Globals.glbProtecoesValorVar3)) " + sTiksExecucaoFormulário);

            if (!string.IsNullOrEmpty(Globals.glbProtecoesValorVar3))
            {

                // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  strValorProt3 " + sTiksExecucaoFormulário);

                string strValorProt3 = Globals.glbProtecoesValorVar3.Trim().Replace("R$", "");
                strValorProt3 = strValorProt3.Replace(" ", "");

                // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  totProtecoes " + sTiksExecucaoFormulário);

                totProtecoes = totProtecoes + Convert.ToDecimal(strValorProt3);

                // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  lblProtecoesTot3 " + sTiksExecucaoFormulário);

                lblProtecoesTot3.Text = Globals.glbProtecoesValorVar3.Trim();

                // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  lblProtecoesQtd3 " + sTiksExecucaoFormulário);

                lblProtecoesQtd3.Text = Globals.glbProtecoesQtdUnit3.Trim();

                int nQtd3 = 1;

                // EventLog.WriteEntry("Totem", "AtualizaTexto -  if (!string.IsNullOrEmpty(Globals.glbProtecoesQtdUnit3.Trim())) " + sTiksExecucaoFormulário);

                if (!string.IsNullOrEmpty(Globals.glbProtecoesQtdUnit3.Trim()))
                {
                    // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  nQtd3 " + sTiksExecucaoFormulário);

                    nQtd3 = Convert.ToInt32(Globals.glbProtecoesQtdUnit3.Trim());
                }
                else
                {
                    // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  nQtd3 Else " + sTiksExecucaoFormulário);

                    nQtd3 = Convert.ToInt32(Globals.glbResumoDiariasQtd.Trim());
                }

                // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  valorUnit3 " + sTiksExecucaoFormulário);

                decimal valorUnit3 = Convert.ToDecimal(strValorProt3) / nQtd3;

                // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  lblProtecoesUnit3 " + sTiksExecucaoFormulário);

                lblProtecoesUnit3.Text = string.Format(new CultureInfo("pt-BR"), "{0:C}", valorUnit3);
            }
            else
            {
                lblProtecoesTot3.Text = "";
                lblProtecoesQtd3.Text = "";
                lblProtecoesUnit3.Text = "";
            }

            // EventLog.WriteEntry("Totem", "AtualizaTexto -  if (string.IsNullOrEmpty(Globals.glbProtecoesVar1)) " + sTiksExecucaoFormulário);

            if (string.IsNullOrEmpty(Globals.glbProtecoesVar1))
                lblProtecoesVar1.Text = "Não Contratado";
            else
            {
                // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  lblProtecoesVar1 " + sTiksExecucaoFormulário);

                lblProtecoesVar1.Text = Globals.glbProtecoesVar1.Trim();
            }

            // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  lblProtecoesVar2 " + sTiksExecucaoFormulário);

            if (!string.IsNullOrEmpty(Globals.glbProtecoesVar2)) lblProtecoesVar2.Text = Globals.glbProtecoesVar2.Trim();

            // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  lblProtecoesVar3 " + sTiksExecucaoFormulário);

            if (!string.IsNullOrEmpty(Globals.glbProtecoesVar3)) lblProtecoesVar3.Text = Globals.glbProtecoesVar3.Trim();

            lblProtecoesVar1.Refresh();
            lblProtecoesVar2.Refresh();
            lblProtecoesVar3.Refresh();

            decimal totAdministrativoAuxiliar = 0;
            decimal taxaAdministrativa = 0;
            decimal totRetorno = 0;
            string strTaxaAdministrativa = "0";
            string strRetorno = "0";

            // Percentual para total administrativo
            //for (int x = 0; x < nTaxas; x++)
            //{
            //    strTaxaAdministrativa = Globals.glbFeeAmountPercentage[x].Replace(".", ",");
            //    strTaxaAdministrativa = strTaxaAdministrativa.Replace("%", "");
            //    strTaxaAdministrativa = strTaxaAdministrativa.Replace(" ", "");
            //    taxaAdministrativa = Convert.ToDecimal(strTaxaAdministrativa) / 100;
            //    totAdministrativoAuxiliar = ((totDiarias - totDesconto) + totProtecoes + totServicos) * taxaAdministrativa;
            //}
            int nTaxas = 0;
            // EventLog.WriteEntry("Totem", "AtualizaTexto -  if (Globals.glbFeeAmount != null) " + sTiksExecucaoFormulário);

            if (Globals.glbFeeAmount != null)
            {

                // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  nTaxas " + sTiksExecucaoFormulário);

                nTaxas = Globals.glbFeeAmount.Count();
                lblTaxaRetorno.Visible = false;
                lblTaxaRetorno.Refresh();

                // EventLog.WriteEntry("Totem", "AtualizaTexto -  for (int x = 0; x < nTaxas; x++) " + sTiksExecucaoFormulário);

                for (int x = 0; x < nTaxas; x++)
                {
                    // EventLog.WriteEntry("Totem", "AtualizaTexto -   if (Globals.glbFeeDescription[x].Trim().ToUpper().Contains('RETORNO')) " + sTiksExecucaoFormulário);

                    if (Globals.glbFeeDescription[x].Trim().ToUpper().Contains("RETORNO"))
                    {
                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  lblRetornoQtd " + sTiksExecucaoFormulário);
                        lblRetornoQtd.Text = Globals.glbFeeAmountUnit[x].Trim();

                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  lblRetornoUnit " + sTiksExecucaoFormulário);

                        lblRetornoUnit.Text = Globals.glbFeeUnitCharge[x].Trim();

                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  lblRetornoTot " + sTiksExecucaoFormulário);

                        lblRetornoTot.Text = Globals.glbFeeAmount[x].Trim();

                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  strRetorno " + sTiksExecucaoFormulário);

                        strRetorno = Globals.glbFeeAmount[x].Trim().Replace("R$", "");
                        strRetorno = strRetorno.Replace(" ", "");

                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  totRetorno " + sTiksExecucaoFormulário);

                        totRetorno = Convert.ToDecimal(strRetorno);

                        lblTaxaRetorno.Visible = true;
                        lblTaxaRetorno.Refresh();
                    }
                }

                // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  nTaxas " + sTiksExecucaoFormulário);
                nTaxas = Globals.glbFeeAmount.Count();

                // EventLog.WriteEntry("Totem", "AtualizaTexto -  for (int x = 0; x < nTaxas; x++) " + sTiksExecucaoFormulário);

                for (int x = 0; x < nTaxas; x++)
                {
                    // EventLog.WriteEntry("Totem", "AtualizaTexto -  if (Globals.glbFeeDescription[x].Trim().ToUpper().Contains('SERVI')) " + sTiksExecucaoFormulário);

                    if (Globals.glbFeeDescription[x].Trim().ToUpper().Contains("SERVI"))
                    {
                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  lblAdministrativaUnit " + sTiksExecucaoFormulário);

                        lblAdministrativaUnit.Text = Globals.glbFeeAmountPercentage[0].Trim();

                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  lblAdministrativaTot " + sTiksExecucaoFormulário);

                        lblAdministrativaTot.Text = Globals.glbFeeAmount[x].Trim();

                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  strAdministrativo " + sTiksExecucaoFormulário);

                        strAdministrativo = Globals.glbFeeAmount[x].Trim().Replace("R$", "");
                        strAdministrativo = strAdministrativo.Replace(" ", "");

                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  totAdministrativo " + sTiksExecucaoFormulário);

                        totAdministrativo = Convert.ToDecimal(strAdministrativo);

                    }

                    // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  if (Globals.glbFeeDescription[x].Trim().ToUpper().Contains('ADMIN')) " + sTiksExecucaoFormulário);

                    if (Globals.glbFeeDescription[x].Trim().ToUpper().Contains("ADMIN"))
                    {
                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  lblAdministrativaUnit " + sTiksExecucaoFormulário);

                        lblAdministrativaUnit.Text = Globals.glbFeeAmountPercentage[x].Trim();

                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  lblAdministrativaTot " + sTiksExecucaoFormulário);

                        lblAdministrativaTot.Text = Globals.glbFeeAmount[x].Trim();

                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  strAdministrativo " + sTiksExecucaoFormulário);

                        strAdministrativo = Globals.glbFeeAmount[x].Trim().Replace("R$", "");
                        strAdministrativo = strAdministrativo.Replace(" ", "");

                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  totAdministrativo " + sTiksExecucaoFormulário);

                        totAdministrativo = Convert.ToDecimal(strAdministrativo);
                    }

                }

                // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  nTaxas " + sTiksExecucaoFormulário);

                nTaxas = Globals.glbFeeAmount.Count();

                // EventLog.WriteEntry("Totem", "AtualizaTexto -  for (int x = 0; x < nTaxas; x++) " + sTiksExecucaoFormulário);
                for (int x = 0; x < nTaxas; x++)
                {
                    // EventLog.WriteEntry("Totem", "AtualizaTexto -  if (Globals.glbFeeDescription[x].Trim().ToUpper().Contains('HORA EXTRA')) " + sTiksExecucaoFormulário);

                    if (Globals.glbFeeDescription[x].Trim().ToUpper().Contains("HORA EXTRA"))
                    {
                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  lblHoraExtraUnit " + sTiksExecucaoFormulário);
                        lblHoraExtraUnit.Text = Globals.glbFeeUnitCharge[x].Trim();

                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  lblHoraExtraQtd " + sTiksExecucaoFormulário);

                        lblHoraExtraQtd.Text = Globals.glbFeeAmountUnit[x].Trim();

                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  lblHoraExtraTot " + sTiksExecucaoFormulário);

                        lblHoraExtraTot.Text = Globals.glbFeeAmount[x].Trim();


                        // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  strHoraExtra " + sTiksExecucaoFormulário);

                        strHoraExtra = Globals.glbFeeAmount[x].Trim().Replace("R$", "");
                        strHoraExtra = strHoraExtra.Replace(" ", "");
                        totHoraExtra = Convert.ToDecimal(strHoraExtra);
                    }
                }
            }

            lblProtecoesTot1.Refresh();
            lblProtecoesTot2.Refresh();
            lblProtecoesTot3.Refresh();

            lblProtecoesQtd1.Refresh();
            lblProtecoesQtd2.Refresh();
            lblProtecoesQtd3.Refresh();

            lblProtecoesUnit1.Refresh();
            lblProtecoesUnit2.Refresh();
            lblProtecoesUnit3.Refresh();

            lblAcessoriosVar1.Refresh();
            lblAcessoriosVar2.Refresh();
            lblAcessoriosVar3.Refresh();
            lblAcessoriosVar4.Refresh();

            lblReservaNumero.Refresh();
            lblDataRetirada.Refresh();
            lblDataDevolucao.Refresh();
            lblReservaNumeroVar.Refresh();
            lblLocalRetirada.Refresh();
            lblDataRetiradaVar.Refresh();
            lblDataDevolucaoVar.Refresh();
            lblLocalDevolucao.Refresh();
            lblGrupoVeiculoVar.Refresh();

            decimal totGeral = 0;

            // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  totGeral " + sTiksExecucaoFormulário);

            totGeral = (totDiarias - totDesconto) + totProtecoes + totServicos + totAdministrativo + totRetorno + totHoraExtra;

            // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  glbResumoTotalGeral " + sTiksExecucaoFormulário);

            Globals.glbResumoTotalGeral = string.Format(new CultureInfo("pt-BR"), "{0:C}", totGeral);

            // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  lblValorTotalVar " + sTiksExecucaoFormulário);

            lblValorTotalVar.Text = string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDouble(totGeral));

            tmrAguardar.Stop();
            tmrAguardar.Enabled = false;
            pnlReservaWait.Visible = false;

        }

        private void MostrarOcultarVoucher()
        {
            // EventLog.WriteEntry("Totem", "MostrarOcultarVoucher -  if (!string.IsNullOrEmpty(Globals.glbVoucherIdentifier)) " + sTiksExecucaoFormulário);

            if (!string.IsNullOrEmpty(Globals.glbVoucherIdentifier))
            {
                lblVoucher.Text = "Voucher:";
                // EventLog.WriteEntry("Totem", "AtualizaTexto -  Vai Setar  lblVoucherVar " + sTiksExecucaoFormulário);
                lblVoucherVar.Text = Globals.glbVoucherIdentifier.Trim();
                lblVoucher.Visible = true;
                lblVoucherVar.Visible = true;
                lblVoucherVar.Refresh();
                lblVoucher.Refresh();
            }
            else
            {
                lblVoucher.Text = "";
                lblVoucherVar.Text = "";
                lblVoucher.Visible = false;
                lblVoucherVar.Visible = false;
                lblVoucherVar.Refresh();
                lblVoucher.Refresh();
            }
        }

        private void Shutdown_Enable()
        {
            if (Globals.glbDebugMode == 0)
                pictShutdown.Visible = false;
            else
                pictShutdown.Visible = true;
        }

        private void pictShutdown_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(1);
        }


        private void pictReservaAlterar_Click(object sender, EventArgs e)
        {
            timerDisabled();
            Globals.glbMensagem = 0401;
            
            frmMensagem frmMsg = new frmMensagem();
            frmMsg.Show();
            this.Close();
            this.Dispose();
        }

        private int obtemReservaXML_RA(string xmlEncodedList)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlEncodedList);

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESDOCCLI = xmlDoc.GetElementsByTagName('RESDOCCLI'); " + sTiksExecucaoFormulário);

            XmlNodeList tabRESDOCCLI = xmlDoc.GetElementsByTagName("RESDOCCLI");
            if (tabRESDOCCLI.Count > 0)
            {
                for (int x = 0; x < tabRESDOCCLI.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESDOCCLI.Item(x).InnerXml))
                    {
                        // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  Vai setar glbRA_clifat " + sTiksExecucaoFormulário);

                        Globals.glbRA_clifat = tabRESDOCCLI.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESLOC = xmlDoc.GetElementsByTagName('RESLOC'); " + sTiksExecucaoFormulário);

            XmlNodeList tabRESLOC = xmlDoc.GetElementsByTagName("RESLOC");
            if (tabRESLOC.Count > 0)
            {
                for (int x = 0; x < tabRESLOC.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESLOC.Item(x).InnerXml))
                    {
                        Globals.glbRA_clidoc = tabRESLOC.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESLOCNOM = xmlDoc.GetElementsByTagName('RESLOCNOM');  " + sTiksExecucaoFormulário);

            XmlNodeList tabRESLOCNOM = xmlDoc.GetElementsByTagName("RESLOCNOM");
            if (tabRESLOCNOM.Count > 0)
            {
                for (int x = 0; x < tabRESLOCNOM.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESLOCNOM.Item(x).InnerXml))
                    {
                        if (!string.IsNullOrEmpty(Globals.glbCLI_CLINOM_SAUDACOES))
                        {
                            Globals.glbRA_clinom = Globals.glbCLI_CLINOM_SAUDACOES.Trim();
                        }
                        else
                            Globals.glbRA_clinom = tabRESLOCNOM.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESLOJOPE = xmlDoc.GetElementsByTagName('RESLOJOPE');  " + sTiksExecucaoFormulário);

            XmlNodeList tabRESLOJOPE = xmlDoc.GetElementsByTagName("RESLOJOPE");
            if (tabRESLOJOPE.Count > 0)
            {
                for (int x = 0; x < tabRESLOJOPE.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESLOJOPE.Item(x).InnerXml))
                    {
                        Globals.glbRA_r_aresloj = tabRESLOJOPE.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESSIGRET = xmlDoc.GetElementsByTagName('RESSIGRET');  " + sTiksExecucaoFormulário);

            XmlNodeList tabRESSIGRET = xmlDoc.GetElementsByTagName("RESSIGRET");
            if (tabRESSIGRET.Count > 0)
            {
                for (int x = 0; x < tabRESSIGRET.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESSIGRET.Item(x).InnerXml))
                    {
                        Globals.glbRA_r_alojret = tabRESSIGRET.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESDATRET = xmlDoc.GetElementsByTagName('RESDATRET');  " + sTiksExecucaoFormulário);

            XmlNodeList tabRESDATRET = xmlDoc.GetElementsByTagName("RESDATRET");
            if (tabRESDATRET.Count > 0)
            {
                for (int x = 0; x < tabRESDATRET.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESDATRET.Item(x).InnerXml))
                    {
                        Globals.glbRA_r_adatret = tabRESDATRET.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESHORRET = xmlDoc.GetElementsByTagName('RESHORRET');  " + sTiksExecucaoFormulário);

            XmlNodeList tabRESHORRET = xmlDoc.GetElementsByTagName("RESHORRET");
            if (tabRESHORRET.Count > 0)
            {
                for (int x = 0; x < tabRESHORRET.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESHORRET.Item(x).InnerXml))
                    {
                        Globals.glbRA_r_ahorret = tabRESHORRET.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESDATDEV = xmlDoc.GetElementsByTagName('RESDATDEV'); " + sTiksExecucaoFormulário);

            XmlNodeList tabRESDATDEV = xmlDoc.GetElementsByTagName("RESDATDEV");
            if (tabRESDATDEV.Count > 0)
            {
                for (int x = 0; x < tabRESDATDEV.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESDATDEV.Item(x).InnerXml))
                    {
                        Globals.glbRA_r_adatdev = tabRESDATDEV.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESHORDEV = xmlDoc.GetElementsByTagName('RESHORDEV'); " + sTiksExecucaoFormulário);
            XmlNodeList tabRESHORDEV = xmlDoc.GetElementsByTagName("RESHORDEV");
            if (tabRESHORDEV.Count > 0)
            {
                for (int x = 0; x < tabRESHORDEV.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESHORDEV.Item(x).InnerXml))
                    {
                        Globals.glbRA_r_ahordev = tabRESHORDEV.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESGRUCOD = xmlDoc.GetElementsByTagName('RESGRUCOD'); " + sTiksExecucaoFormulário);
            XmlNodeList tabRESGRUCOD = xmlDoc.GetElementsByTagName("RESGRUCOD");
            if (tabRESGRUCOD.Count > 0)
            {
                for (int x = 0; x < tabRESGRUCOD.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESGRUCOD.Item(x).InnerXml))
                    {
                        Globals.glbRA_grucod = tabRESGRUCOD.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESSIGDEV = xmlDoc.GetElementsByTagName('RESSIGDEV'); " + sTiksExecucaoFormulário);
            XmlNodeList tabRESSIGDEV = xmlDoc.GetElementsByTagName("RESSIGDEV");
            if (tabRESSIGDEV.Count > 0)
            {
                for (int x = 0; x < tabRESSIGDEV.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESSIGDEV.Item(x).InnerXml))
                    {
                        Globals.glbRA_r_alojdev = tabRESSIGDEV.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabTPTCONCOD = xmlDoc.GetElementsByTagName('TPTCONCOD'); " + sTiksExecucaoFormulário);

            XmlNodeList tabTPTCONCOD = xmlDoc.GetElementsByTagName("TPTCONCOD");
            if (tabTPTCONCOD.Count > 0)
            {
                for (int x = 0; x < tabTPTCONCOD.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabTPTCONCOD.Item(x).InnerXml))
                    {
                        Globals.glbRA_tptconcod = tabTPTCONCOD.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabTARDES = xmlDoc.GetElementsByTagName('TARDES'); " + sTiksExecucaoFormulário);
            XmlNodeList tabTARDES = xmlDoc.GetElementsByTagName("TARDES");
            if (tabTARDES.Count > 0)
            {
                for (int x = 0; x < tabTARDES.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabTARDES.Item(x).InnerXml))
                    {
                        Globals.glbRA_tardes = tabTARDES.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabTRFCOD = xmlDoc.GetElementsByTagName('TRFCOD'); " + sTiksExecucaoFormulário);
            XmlNodeList tabTRFCOD = xmlDoc.GetElementsByTagName("TRFCOD");
            if (tabTRFCOD.Count > 0)
            {
                for (int x = 0; x < tabTRFCOD.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabTRFCOD.Item(x).InnerXml))
                    {
                        Globals.glbRA_trfcod = tabTRFCOD.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabACOCOD = xmlDoc.GetElementsByTagName('ACOCOD'); " + sTiksExecucaoFormulário);

            XmlNodeList tabACOCOD = xmlDoc.GetElementsByTagName("ACOCOD");
            if (tabACOCOD.Count > 0)
            {
                for (int x = 0; x < tabACOCOD.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabACOCOD.Item(x).InnerXml))
                    {
                        Globals.glbRA_acocod = tabACOCOD.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabACODIG = xmlDoc.GetElementsByTagName('ACODIG'); " + sTiksExecucaoFormulário);
            XmlNodeList tabACODIG = xmlDoc.GetElementsByTagName("ACODIG");
            if (tabACODIG.Count > 0)
            {
                for (int x = 0; x < tabACODIG.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabACODIG.Item(x).InnerXml))
                    {
                        Globals.glbRA_acodig = tabACODIG.Item(x).InnerXml.Trim();
                    }
                }
            }


            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESCLISEG = xmlDoc.GetElementsByTagName('RESCLISEG'); " + sTiksExecucaoFormulário);

            XmlNodeList tabRESCLISEG = xmlDoc.GetElementsByTagName("RESCLISEG");
            if (tabRESCLISEG.Count > 0)
            {
                for (int x = 0; x < tabRESCLISEG.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESCLISEG.Item(x).InnerXml))
                    {
                        Globals.glbRA_R_aCliSeg = tabRESCLISEG.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESCANVEN = xmlDoc.GetElementsByTagName('RESCANVEN'); " + sTiksExecucaoFormulário);

            XmlNodeList tabRESCANVEN = xmlDoc.GetElementsByTagName("RESCANVEN");
            if (tabRESCANVEN.Count > 0)
            {
                for (int x = 0; x < tabRESCANVEN.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESCANVEN.Item(x).InnerXml))
                    {
                        Globals.glbRA_R_aCanVen = tabRESCANVEN.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESTRFNEW = xmlDoc.GetElementsByTagName('RESTRFNEW'); " + sTiksExecucaoFormulário);

            XmlNodeList tabRESTRFNEW = xmlDoc.GetElementsByTagName("RESTRFNEW");
            if (tabRESTRFNEW.Count > 0)
            {
                for (int x = 0; x < tabRESTRFNEW.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESTRFNEW.Item(x).InnerXml))
                    {
                        Globals.glbRA_restrfnew = tabRESTRFNEW.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESTIPPRT = xmlDoc.GetElementsByTagName('RESTIPPRT'); " + sTiksExecucaoFormulário);
            XmlNodeList tabRESTIPPRT = xmlDoc.GetElementsByTagName("RESTIPPRT");
            if (tabRESTIPPRT.Count > 0)
            {
                for (int x = 0; x < tabRESTIPPRT.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESTIPPRT.Item(x).InnerXml))
                    {
                        Globals.glbRA_r_aprot = tabRESTIPPRT.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESPROT = xmlDoc.GetElementsByTagName('RESPROT'); " + sTiksExecucaoFormulário);

            XmlNodeList tabRESPROT = xmlDoc.GetElementsByTagName("RESPROT");
            if (tabRESPROT.Count > 0)
            {
                for (int x = 0; x < tabRESPROT.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESPROT.Item(x).InnerXml))
                    {
                        Globals.glbRA_r_aprotpot = tabRESPROT.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESPRO = xmlDoc.GetElementsByTagName('RESPRO'); " + sTiksExecucaoFormulário);

            XmlNodeList tabRESPRO = xmlDoc.GetElementsByTagName("RESPRO");
            if (tabRESPRO.Count > 0)
            {
                for (int x = 0; x < tabRESPRO.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESPRO.Item(x).InnerXml))
                    {
                        Globals.glbRA_r_aprtpa = tabRESPRO.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESDSC = xmlDoc.GetElementsByTagName('RESDSC'); " + sTiksExecucaoFormulário);

            XmlNodeList tabRESDSC = xmlDoc.GetElementsByTagName("RESDSC");
            if (tabRESDSC.Count > 0)
            {
                for (int x = 0; x < tabRESDSC.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESDSC.Item(x).InnerXml))
                    {
                        Globals.glbRA_r_adsc = tabRESDSC.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabCLITARCOD = xmlDoc.GetElementsByTagName('CLITARCOD'); " + sTiksExecucaoFormulário);

            XmlNodeList tabCLITARCOD = xmlDoc.GetElementsByTagName("CLITARCOD");
            if (tabCLITARCOD.Count > 0)
            {
                for (int x = 0; x < tabCLITARCOD.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabCLITARCOD.Item(x).InnerXml))
                    {
                        Globals.glbRA_clitarcod = tabCLITARCOD.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabCLITARSEQ = xmlDoc.GetElementsByTagName('CLITARSEQ'); " + sTiksExecucaoFormulário);

            XmlNodeList tabCLITARSEQ = xmlDoc.GetElementsByTagName("CLITARSEQ");
            if (tabCLITARSEQ.Count > 0)
            {
                for (int x = 0; x < tabCLITARSEQ.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabCLITARSEQ.Item(x).InnerXml))
                    {
                        Globals.glbRA_clitarseq = tabCLITARSEQ.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESCODSML = xmlDoc.GetElementsByTagName('RESCODSML'); " + sTiksExecucaoFormulário);

            XmlNodeList tabRESCODSML = xmlDoc.GetElementsByTagName("RESCODSML");
            if (tabRESCODSML.Count > 0)
            {
                for (int x = 0; x < tabRESCODSML.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESCODSML.Item(x).InnerXml))
                    {
                        Globals.glbRA_rescodsml = tabRESCODSML.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESTIPSML = xmlDoc.GetElementsByTagName('RESTIPSML');" + sTiksExecucaoFormulário);

            XmlNodeList tabRESTIPSML = xmlDoc.GetElementsByTagName("RESTIPSML");
            if (tabRESTIPSML.Count > 0)
            {
                for (int x = 0; x < tabRESTIPSML.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESTIPSML.Item(x).InnerXml))
                    {
                        Globals.glbRA_restipsml = tabRESTIPSML.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESTVCH = xmlDoc.GetElementsByTagName('RESTVCH'); " + sTiksExecucaoFormulário);

            XmlNodeList tabRESTVCH = xmlDoc.GetElementsByTagName("RESTVCH");
            if (tabRESTVCH.Count > 0)
            {
                for (int x = 0; x < tabRESTVCH.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESTVCH.Item(x).InnerXml))
                    {
                        Globals.glbRA_ResTVch = tabRESTVCH.Item(x).InnerXml.Trim();
                    }
                }
            }


            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESEXPCLI = xmlDoc.GetElementsByTagName('RESEXPCLI');  " + sTiksExecucaoFormulário);

            XmlNodeList tabRESEXPCLI = xmlDoc.GetElementsByTagName("RESEXPCLI");
            if (tabRESEXPCLI.Count > 0)
            {
                for (int x = 0; x < tabRESEXPCLI.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESEXPCLI.Item(x).InnerXml))
                    {
                        Globals.glbRA_ResExpCli = tabRESEXPCLI.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESPAXCPF = xmlDoc.GetElementsByTagName('RESPAXCPF');  " + sTiksExecucaoFormulário);

            XmlNodeList tabRESPAXCPF = xmlDoc.GetElementsByTagName("RESPAXCPF");
            if (tabRESPAXCPF.Count > 0)
            {
                for (int x = 0; x < tabRESPAXCPF.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESPAXCPF.Item(x).InnerXml))
                    {
                        Globals.glbRA_respaxcpf = tabRESPAXCPF.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESGRUPRO = xmlDoc.GetElementsByTagName('RESGRUPRO');  " + sTiksExecucaoFormulário);

            XmlNodeList tabRESGRUPRO = xmlDoc.GetElementsByTagName("RESGRUPRO");
            if (tabRESGRUPRO.Count > 0)
            {
                for (int x = 0; x < tabRESGRUPRO.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESGRUPRO.Item(x).InnerXml))
                    {
                        Globals.glbRA_resgrupro = tabRESGRUPRO.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESMOTADI = xmlDoc.GetElementsByTagName('RESMOTADI');  " + sTiksExecucaoFormulário);

            XmlNodeList tabRESMOTADI = xmlDoc.GetElementsByTagName("RESMOTADI");
            if (tabRESMOTADI.Count > 0)
            {
                for (int x = 0; x < tabRESMOTADI.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESMOTADI.Item(x).InnerXml))
                    {
                        Globals.glbRA_resmotadi = tabRESMOTADI.Item(x).InnerXml.Trim();
                    }
                }
            }


            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESCODTAM = xmlDoc.GetElementsByTagName('RESCODTAM');  " + sTiksExecucaoFormulário);

            XmlNodeList tabRESCODTAM = xmlDoc.GetElementsByTagName("RESCODTAM");
            if (tabRESCODTAM.Count > 0)
            {
                for (int x = 0; x < tabRESCODTAM.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESCODTAM.Item(x).InnerXml))
                    {
                        Globals.glbRA_rescodtam = tabRESCODTAM.Item(x).InnerXml.Trim();
                    }
                }
            }


            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESCUPCOD = xmlDoc.GetElementsByTagName('RESCUPCOD');  " + sTiksExecucaoFormulário);

            XmlNodeList tabRESCUPCOD = xmlDoc.GetElementsByTagName("RESCUPCOD");
            if (tabRESCUPCOD.Count > 0)
            {
                for (int x = 0; x < tabRESCUPCOD.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESCUPCOD.Item(x).InnerXml))
                    {
                        Globals.glbRA_rescupcod = tabRESCUPCOD.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESDIAAUT = xmlDoc.GetElementsByTagName('RESDIAAUT');  " + sTiksExecucaoFormulário);

            XmlNodeList tabRESDIAAUT = xmlDoc.GetElementsByTagName("RESDIAAUT");
            if (tabRESDIAAUT.Count > 0)
            {
                for (int x = 0; x < tabRESDIAAUT.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESDIAAUT.Item(x).InnerXml))
                    {
                        Globals.glbRA_resdiaaut = tabRESDIAAUT.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabRESPNR = xmlDoc.GetElementsByTagName('RESPNR');  " + sTiksExecucaoFormulário);

            XmlNodeList tabRESPNR = xmlDoc.GetElementsByTagName("RESPNR");
            if (tabRESPNR.Count > 0)
            {
                for (int x = 0; x < tabRESPNR.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabRESPNR.Item(x).InnerXml))
                    {
                        Globals.glbRA_respnr = tabRESPNR.Item(x).InnerXml.Trim();
                    }
                }
            }

            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  XmlNodeList tabCATSEGDSC = xmlDoc.GetElementsByTagName('CATSEGDSC');  " + sTiksExecucaoFormulário);

            XmlNodeList tabCATSEGDSC = xmlDoc.GetElementsByTagName("CATSEGDSC");
            if (tabCATSEGDSC.Count > 0)
            {
                for (int x = 0; x < tabCATSEGDSC.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabCATSEGDSC.Item(x).InnerXml))
                    {
                        Globals.glbRA_r_atipo = tabCATSEGDSC.Item(x).InnerXml.Trim();
                    }
                }
            }


            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  if (Globals.glbRA_r_atipo.Trim().ToUpper().Contains('NCIA'))  " + sTiksExecucaoFormulário);

            if (Globals.glbRA_r_atipo.Trim().ToUpper().Contains("NCIA"))
            {
                Globals.glbRA_r_atipo = "A";
            }
            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  if (Globals.glbRA_r_atipo.Trim().ToUpper().Contains('FISICA'))  " + sTiksExecucaoFormulário);

            if (Globals.glbRA_r_atipo.Trim().ToUpper().Contains("FISICA"))
            {
                Globals.glbRA_r_atipo = "F";
            }
            // EventLog.WriteEntry("Totem", "obtemReservaXML_RA -  if (Globals.glbRA_r_atipo.Trim().ToUpper().Contains('A'))  " + sTiksExecucaoFormulário);

            if ((Globals.glbRA_r_atipo != "F") && (Globals.glbRA_r_atipo != "A"))
            {
                Globals.glbRA_r_atipo = "J";
            }
            return 1;
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            tmrReturn.Stop();
            tmrReturn.Enabled = false;
            // btnConfirmar.Enabled = false;

            try
            {
                int nAberturaRA = SetAberturaRA();
                Globals.glbRA_Contrato = nAberturaRA.ToString().Trim();

                if (!string.IsNullOrEmpty(Globals.glbRA_respaxcpf.Trim()))
                {
                    Globals.glbRESDOCCLI = Globals.glbRA_respaxcpf;
                }

                if (!string.IsNullOrEmpty(Globals.glbVoucherIdentifier))
                {
                    if (Globals.glbVOUPRE.Trim().ToUpper() == "S")
                    {
                        timerDisabled();
                        frmScreen11 frm11 = new frmScreen11();
                        frm11.Show();
                        this.Close();
                        this.Dispose();
                    }
                    else
                    {
                        timerDisabled();
                        frmScreen09 frm09 = new frmScreen09();
                        frm09.Show();
                        this.Close();
                        this.Dispose();
                    }
                }
                else
                {
                    timerDisabled();
                    frmScreen09 frm09 = new frmScreen09();
                    frm09.Show();
                    this.Close();
                    this.Dispose();
                }
            }
            catch (Exception ex)
            {
                timerDisabled();

                Globals.glbScreen = 1;
                Globals.glbMensagem = 0203;
                frmMensagem frmMsg = new frmMensagem();
                frmMsg.Show();
                this.Close();
                this.Dispose();
                return;
            }
        }

        private void resetVoucher()
        {
            Globals.glbVOU_RESNUM = "";
            Globals.glbVOU_RESLOJOPE = "";
            Globals.glbVOU_RESNVCH = "";
            Globals.glbVOU_RESVDIA = "";
            Globals.glbVOU_RESVDIAE = "";
            Globals.glbVOU_RESVHORE = "";
            Globals.glbVOU_RESVKM = "";
            Globals.glbVOU_RESVTAXS = "";
            Globals.glbVOU_RESVPROT = "";
            Globals.glbVOU_RESVMOT = "";
            Globals.glbVOU_RESVAPRI = "";
            Globals.glbVOU_RESVCMB = "";
            Globals.glbVOU_RESVAVA = "";
            Globals.glbVOU_RESVPRTO = "";
            Globals.glbVOU_RESVADC = "";
            Globals.glbVOU_RESVMOTA = "";
            Globals.glbVOU_RESVTAXR = "";
            Globals.glbVOU_RESVDSP = "";
            Globals.glbVOU_RESMUL = "";
            Globals.glbVOU_RESNVCLI = "";
        }

        private int SetAberturaRA()
        {
            string numeroContratoRA = "0";
            try
            {
                int pR_ACONFEC = 4;
                Globals.glbR_ACONFEC = pR_ACONFEC;

                string pR_AHORRET = Globals.glbRA_r_ahorret.Substring(0, 5);
                string pR_AHORDEV = Globals.glbRA_r_ahordev.Substring(11, 5);

                DateTime dtAtual = DateTime.Now;
                string strDataAtual = dtAtual.ToString("yyyy-MM-dd");
                string strHoraAtual = dtAtual.ToString("HH:mm:ss");
                string strDataCompleta = strDataAtual + "T00:00:00";
                string strDataHora = strDataAtual + "T" + strHoraAtual;

                if (string.IsNullOrEmpty(Globals.glbKM))
                {
                    Globals.glbKM = "0";
                }
                else
                {
                    Globals.glbKM = Globals.glbKM.Replace(".", "");
                    Globals.glbKM = Globals.glbKM.Replace(",", "");
                    Globals.glbKM = Globals.glbKM.Replace("km", "");
                    Globals.glbKM = Globals.glbKM.Replace("Km", "");
                    Globals.glbKM = Globals.glbKM.Replace("KM", "");
                    Globals.glbKM = Globals.glbKM.Replace(" ", "");
                }
                if (string.IsNullOrEmpty(Globals.glbVEICOM)) Globals.glbVEICOM = "0";
                if (string.IsNullOrEmpty(Globals.glbProtecoesValorPar1)) Globals.glbProtecoesValorPar1 = "0";

                CentralUnidasClassClient clientWcf = new CentralUnidasClassClient();

                if (Globals.glbRA_r_atipo.Trim() == "F") Globals.glbRA_respaxcpf = "";

                numeroContratoRA = clientWcf.SetAberturaRA(
                    Globals.glbRA_r_alojret,
                    Globals.glbRA_tptconcod,
                    Globals.glbRA_tardes,
                    Convert.ToInt32(Globals.glbRA_clitarcod),
                    Convert.ToInt32(Globals.glbRA_clitarseq),
                    Globals.glbRA_r_aprot,
                    Convert.ToDecimal(Globals.glbRA_r_adsc.Replace(".", ",")),
                    Convert.ToDecimal(Globals.glbProtecoesValorPar1.Replace(".", ",")),
                    pR_ACONFEC,
                    Globals.glbRA_r_alojdev,
                    Globals.glbRA_r_adatret.Substring(0, 10),
                    Globals.glbRA_r_adatdev,
                    pR_AHORRET,
                    pR_AHORDEV,
                    Globals.glbRA_clidoc,
                    Globals.glbRA_clifat,
                    Globals.glbRA_r_aprotpot,
                    Globals.glbRA_grucod,
                    Convert.ToInt32(Globals.glbRA_acocod),
                    Convert.ToInt32(Globals.glbRA_acodig),
                    Convert.ToInt32(Globals.glbRA_trfcod),
                    Globals.glbRA_r_atipo,
                    "APP-TOTEM",
                    Globals.glbPRECPF,
                    Convert.ToInt32(Globals.glbReservaNumeroPesquisa),
                    "N",
                    "N",
                    Globals.glbRA_r_aprtpa,
                    Globals.glbRA_r_aresloj,
                    Globals.glbRA_restrfnew,
                    Convert.ToInt32(Globals.glbRA_R_aCliSeg),
                    Convert.ToInt32(Globals.glbRA_R_aCanVen),
                    Globals.glbRA_rescodsml,
                    Globals.glbRA_restipsml,
                    Globals.glbRA_ResExpCli,
                    Globals.glbRA_grucod,
                    Convert.ToInt32(Globals.glbRA_rescodtam),
                    Globals.glbRA_clinom,
                    Globals.glbRA_rescupcod,
                    Convert.ToInt32(Globals.glbKM),
                    Convert.ToInt32(Globals.glbKM),
                    Convert.ToInt32(Globals.glbVEICOM),
                    Convert.ToInt32(Globals.glbVEICOM),
                    strDataCompleta,
                    strHoraAtual,
                    strHoraAtual,
                    Globals.glbVEIPLA,
                    Globals.glbRA_clidoc,
                    Globals.glbRA_r_adatdev,
                    0);

                // ContBS

                if (Convert.ToInt32(numeroContratoRA.Trim()) > 0)
                {

                    Globals.glbCC_R_ANUM = numeroContratoRA;

                    string mR_ANUM = numeroContratoRA;
                    string mR_ALOJRET = "";
                    string mCATSRVCOD = "";
                    string mCATDAT = "";
                    string mCATSRVVAL = "";
                    string mULTDATSRV = "";
                    string mULTHORSRV = "";
                    string mCATSRVCPF = "";
                    string mCATSRVGRU = "";
                    string mCATQTDSRV = "";
                    string mCATFRQVAL = "";
                    string mCATQTDFRQ = "";

                    int nRow = Globals.glbSERV_CATSRVCOD.Length;

                    for (int x = 0; x < nRow; x++)
                    {
                        //string strCATSRVDSC = Globals.glbSERV_CATSRVDSC[x].Trim();
                        string strCATSRVDSC = Globals.glbSERV_CATSRVTIP[x].Trim();

                        // Se descrição de Serviço Não possuir caracteres "PROTE" - NÃO FOR PROTEÇÃO
                        if (!strCATSRVDSC.ToUpper().Contains("PROTE"))
                        {
                            mR_ALOJRET = Globals.glbSERV_RESLOJOPE[x].Trim();
                            mCATSRVCOD = Globals.glbSERV_CATSRVCOD[x].Trim();
                            mCATDAT = Globals.glbSERV_RESSRVDAT[x].Trim();
                            mCATSRVVAL = Globals.glbSERV_RESSRVVAL[x].Trim();
                            mULTDATSRV = DateTime.Now.ToString("yyyy-MM-dd");
                            mULTHORSRV = DateTime.Now.ToString("HH:mm");
                            mCATSRVCPF = Globals.glbSERV_RESSRVCPF[x].Trim();
                            mCATSRVGRU = Globals.glbSERV_RESSRVGRU[x].Trim();
                            mCATQTDSRV = Globals.glbSERV_RESQTDSRV[x].Trim();
                            mCATFRQVAL = "0";
                            mCATQTDFRQ = "0";

                            if (string.IsNullOrEmpty(mCATSRVVAL)) mCATSRVVAL = "0";

                            string strServicosRA = clientWcf.SetServicosRA(mR_ANUM,
                                Globals.glbRA_r_alojret,
                                mCATSRVCOD,
                                mCATDAT,
                                mCATSRVVAL,
                                mULTDATSRV,
                                mULTHORSRV,
                                mCATSRVCPF,
                                mCATSRVGRU,
                                mCATQTDSRV,
                                mCATFRQVAL,
                                mCATQTDFRQ);
                        }
                    }

                    // executa proteções
                    mCATFRQVAL = "0";
                    mCATQTDFRQ = "0";
                    mCATDAT = Globals.glbRA_r_adatret + "T00:00:00";

                    if (!string.IsNullOrEmpty(Globals.glbProtecoesCodVar1))
                    {
                        Globals.glbProtecoesValorVar1 = Globals.glbProtecoesValorVar1.Replace("R$", "");
                        Globals.glbProtecoesValorVar1 = Globals.glbProtecoesValorVar1.Replace(".", "");
                        Globals.glbProtecoesValorVar1 = Globals.glbProtecoesValorVar1.Replace(",", ".");
                        Globals.glbProtecoesValorVar1 = Globals.glbProtecoesValorVar1.Replace(" ", "");

                        string strServicosRA = clientWcf.SetServicosRA(mR_ANUM,
                                Globals.glbRA_r_alojret,
                                Globals.glbProtecoesCodVar1.Trim(),
                                mCATDAT,
                                Globals.glbProtecoesValorVar1,
                                DateTime.Now.ToString("yyyy-MM-dd"),
                                DateTime.Now.ToString("HH:mm"),
                                mCATSRVCPF,
                                mCATSRVGRU,
                                mCATQTDSRV,
                                mCATFRQVAL,
                                mCATQTDFRQ);
                    }

                    if (!string.IsNullOrEmpty(Globals.glbProtecoesCodVar2))
                    {
                        Globals.glbProtecoesValorVar2 = Globals.glbProtecoesValorVar2.Replace("R$", "");
                        Globals.glbProtecoesValorVar2 = Globals.glbProtecoesValorVar2.Replace(".", "");
                        Globals.glbProtecoesValorVar2 = Globals.glbProtecoesValorVar2.Replace(",", ".");
                        Globals.glbProtecoesValorVar2 = Globals.glbProtecoesValorVar2.Replace(" ", "");

                        string strServicosRA = clientWcf.SetServicosRA(mR_ANUM,
                                Globals.glbRA_r_alojret,
                                Globals.glbProtecoesCodVar2.Trim(),
                                mCATDAT,
                                Globals.glbProtecoesValorVar2,
                                DateTime.Now.ToString("yyyy-MM-dd"),
                                DateTime.Now.ToString("HH:mm"),
                                mCATSRVCPF,
                                mCATSRVGRU,
                                mCATQTDSRV,
                                mCATFRQVAL,
                                mCATQTDFRQ);
                    }

                    if (!string.IsNullOrEmpty(Globals.glbProtecoesCodVar3))
                    {
                        Globals.glbProtecoesValorVar3 = Globals.glbProtecoesValorVar3.Replace("R$", "");
                        Globals.glbProtecoesValorVar3 = Globals.glbProtecoesValorVar3.Replace(".", "");
                        Globals.glbProtecoesValorVar3 = Globals.glbProtecoesValorVar3.Replace(",", ".");
                        Globals.glbProtecoesValorVar3 = Globals.glbProtecoesValorVar3.Replace(" ", "");

                        string strServicosRA = clientWcf.SetServicosRA(mR_ANUM,
                                Globals.glbRA_r_alojret,
                                Globals.glbProtecoesCodVar3.Trim(),
                                mCATDAT,
                                Globals.glbProtecoesValorVar3,
                                DateTime.Now.ToString("yyyy-MM-dd"),
                                DateTime.Now.ToString("HH:mm"),
                                mCATSRVCPF,
                                mCATSRVGRU,
                                mCATQTDSRV,
                                mCATFRQVAL,
                                mCATQTDFRQ);
                    }

                    if (!string.IsNullOrEmpty(Globals.glbProtecoesCodVar4))
                    {
                        Globals.glbProtecoesValorVar4 = Globals.glbProtecoesValorVar4.Replace("R$", "");
                        Globals.glbProtecoesValorVar4 = Globals.glbProtecoesValorVar4.Replace(".", "");
                        Globals.glbProtecoesValorVar4 = Globals.glbProtecoesValorVar4.Replace(",", ".");
                        Globals.glbProtecoesValorVar4 = Globals.glbProtecoesValorVar4.Replace(" ", "");

                        string strServicosRA = clientWcf.SetServicosRA(mR_ANUM,
                                Globals.glbRA_r_alojret,
                                Globals.glbProtecoesCodVar4.Trim(),
                                mCATDAT,
                                Globals.glbProtecoesValorVar4,
                                DateTime.Now.ToString("yyyy-MM-dd"),
                                DateTime.Now.ToString("HH:mm"),
                                mCATSRVCPF,
                                mCATSRVGRU,
                                mCATQTDSRV,
                                mCATFRQVAL,
                                mCATQTDFRQ);
                    }

                    if (!string.IsNullOrEmpty(Globals.glbProtecoesCodVar5))
                    {
                        Globals.glbProtecoesValorVar5 = Globals.glbProtecoesValorVar5.Replace("R$", "");
                        Globals.glbProtecoesValorVar5 = Globals.glbProtecoesValorVar5.Replace(".", "");
                        Globals.glbProtecoesValorVar5 = Globals.glbProtecoesValorVar5.Replace(",", ".");
                        Globals.glbProtecoesValorVar5 = Globals.glbProtecoesValorVar5.Replace(" ", "");

                        string strServicosRA = clientWcf.SetServicosRA(mR_ANUM,
                                Globals.glbRA_r_alojret,
                                Globals.glbProtecoesCodVar5.Trim(),
                                mCATDAT,
                                Globals.glbProtecoesValorVar5,
                                DateTime.Now.ToString("yyyy-MM-dd"),
                                DateTime.Now.ToString("HH:mm"),
                                mCATSRVCPF,
                                mCATSRVGRU,
                                mCATQTDSRV,
                                mCATFRQVAL,
                                mCATQTDFRQ);
                    }

                    if (!string.IsNullOrEmpty(Globals.glbProtecoesCodVar6))
                    {
                        Globals.glbProtecoesValorVar6 = Globals.glbProtecoesValorVar6.Replace("R$", "");
                        Globals.glbProtecoesValorVar6 = Globals.glbProtecoesValorVar6.Replace(".", "");
                        Globals.glbProtecoesValorVar6 = Globals.glbProtecoesValorVar6.Replace(",", ".");
                        Globals.glbProtecoesValorVar6 = Globals.glbProtecoesValorVar6.Replace(" ", "");

                        string strServicosRA = clientWcf.SetServicosRA(mR_ANUM,
                                Globals.glbRA_r_alojret,
                                Globals.glbProtecoesCodVar6.Trim(),
                                mCATDAT,
                                Globals.glbProtecoesValorVar6,
                                DateTime.Now.ToString("yyyy-MM-dd"),
                                DateTime.Now.ToString("HH:mm"),
                                mCATSRVCPF,
                                mCATSRVGRU,
                                mCATQTDSRV,
                                mCATFRQVAL,
                                mCATQTDFRQ);
                    }

                    // obtem serviço e retorno
                    string strValorAdmnistrativo = "0";
                    string strValorRetorno = "0";
                    if (Globals.glbFeeDescription != null)
                    {
                        int nTaxas = Globals.glbFeeDescription.Count();
                        for (int x = 0; x < nTaxas; x++)
                        {
                            if (Globals.glbFeeDescription[x].Trim().ToUpper().Contains("RETORNO"))
                            {
                                strValorRetorno = Globals.glbFeeAmount[x].Trim();
                            }

                            if (Globals.glbFeeDescription[x].Trim().ToUpper().Contains("SERVI"))
                            {
                                strValorAdmnistrativo = Globals.glbFeeAmount[x].Trim();
                            }

                            if (Globals.glbFeeDescription[x].Trim().ToUpper().Contains("ADMIN"))
                            {
                                strValorAdmnistrativo = Globals.glbFeeAmount[x].Trim();
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(Globals.glbServicosCodVar1))
                    {
                        string strAdministrativo = strValorAdmnistrativo.Trim().Replace("R$", "");
                        strAdministrativo = strAdministrativo.Replace(",", ".");
                        strAdministrativo = strAdministrativo.Replace(" ", "");

                        string strServicosRA = clientWcf.SetServicosRA(mR_ANUM,
                                Globals.glbRA_r_alojret,
                                Globals.glbServicosCodVar1.Trim(),
                                DateTime.Now.ToString("yyyy-MM-dd"),
                                strAdministrativo,
                                DateTime.Now.ToString("yyyy-MM-dd"),
                                DateTime.Now.ToString("HH:mm"),
                                mCATSRVCPF,
                                Globals.glbRA_grucod,
                                mCATQTDSRV,
                                mCATFRQVAL,
                                mCATQTDFRQ);
                    }

                    if (!string.IsNullOrEmpty(Globals.glbRetornoCodVar1))
                    {
                        string strRetorno = strValorRetorno.Trim().Replace("R$", "");
                        strRetorno = strRetorno.Replace(".", "");
                        strRetorno = strRetorno.Replace(",", ".");
                        strRetorno = strRetorno.Replace(" ", "");

                        if (string.IsNullOrEmpty(strRetorno)) strRetorno = "0";

                        string strServicosRA = clientWcf.SetServicosRA(mR_ANUM,
                                Globals.glbRA_r_alojret,
                                Globals.glbRetornoCodVar1.Trim(),
                                DateTime.Now.ToString("yyyy-MM-dd"),
                                strRetorno,
                                DateTime.Now.ToString("yyyy-MM-dd"),
                                DateTime.Now.ToString("HH:mm"),
                                mCATSRVCPF,
                                Globals.glbRA_grucod,
                                mCATQTDSRV,
                                mCATFRQVAL,
                                mCATQTDFRQ);
                    }
                }
                // ContBB

                // Atualiza Voucher
                if (!string.IsNullOrEmpty(Globals.glbVoucherIdentifier))
                {
                    resetVoucher();

                    int nModeloVoucher = 0;
                    if (Globals.glbRA_ResTVch.Trim().ToUpper() == "F")
                    {
                        if (string.IsNullOrEmpty(Globals.glbRA_resdiaaut))
                        {
                            if (Convert.ToInt32(Globals.glbRA_resdiaaut) > 0)
                            {
                                nModeloVoucher = 1;
                            }
                            else
                                nModeloVoucher = 2;
                        }
                        else
                            nModeloVoucher = 2;
                    }

                    if (Globals.glbRA_ResTVch.Trim().ToUpper() == "A")
                    {
                        nModeloVoucher = 3;
                    }

                    if (Globals.glbRA_ResTVch.Trim().ToUpper() == "O")
                    {
                        nModeloVoucher = 4;
                    }

                    if (nModeloVoucher > 0)
                    {
                        string xmlRetornoWCF = clientWcf.GetVoucherReserva(Convert.ToInt32(Globals.glbReservaNumeroPesquisa));
                        if (string.IsNullOrEmpty(xmlRetornoWCF) || (xmlRetornoWCF.Trim() == "<UNIDASWCF />"))
                        {
                            nModeloVoucher = 0;
                        }
                        else
                        {
                            xmlRetornoWCF = xmlRetornoWCF.Replace("\r\n", "");
                            DataSet dtSet = new DataSet();
                            System.IO.StringReader xmlSR = new System.IO.StringReader(xmlRetornoWCF);
                            dtSet.ReadXml(xmlSR);
                            foreach (DataRow dbRow in dtSet.Tables["Table"].Rows)
                            {
                                Globals.glbVOU_RESNUM = dbRow["RESNUM"].ToString().Trim();
                                Globals.glbVOU_RESLOJOPE = dbRow["RESLOJOPE"].ToString().Trim();
                                Globals.glbVOU_RESNVCH = dbRow["RESNVCH"].ToString().Trim();
                                Globals.glbVOU_RESVDIA = dbRow["RESVDIA"].ToString().Trim();
                                Globals.glbVOU_RESVDIAE = dbRow["RESVDIAE"].ToString().Trim();
                                Globals.glbVOU_RESVHORE = dbRow["RESVHORE"].ToString().Trim();
                                Globals.glbVOU_RESVKM = dbRow["RESVKM"].ToString().Trim();
                                Globals.glbVOU_RESVTAXS = dbRow["RESVTAXS"].ToString().Trim();
                                Globals.glbVOU_RESVPROT = dbRow["RESVPROT"].ToString().Trim();
                                Globals.glbVOU_RESVMOT = dbRow["RESVMOT"].ToString().Trim();
                                Globals.glbVOU_RESVAPRI = dbRow["RESVAPRI"].ToString().Trim();
                                Globals.glbVOU_RESVCMB = dbRow["RESVCMB"].ToString().Trim();
                                Globals.glbVOU_RESVAVA = dbRow["RESVAVA"].ToString().Trim();
                                Globals.glbVOU_RESVPRTO = dbRow["RESVPRTO"].ToString().Trim();
                                Globals.glbVOU_RESVADC = dbRow["RESVADC"].ToString().Trim();
                                Globals.glbVOU_RESVMOTA = dbRow["RESVMOTA"].ToString().Trim();
                                Globals.glbVOU_RESVTAXR = dbRow["RESVTAXR"].ToString().Trim();
                                Globals.glbVOU_RESVDSP = dbRow["RESVDSP"].ToString().Trim();
                                Globals.glbVOU_RESMUL = dbRow["RESMUL"].ToString().Trim();
                                Globals.glbVOU_RESNVCLI = dbRow["RESNVCLI"].ToString().Trim();
                                break;
                            }
                        }

                    }

                    if (nModeloVoucher == 1)
                    {
                        string strVoucher_F = clientWcf.SetVoucherReserva_F(
                            Globals.glbCC_R_ANUM,
                            Globals.glbRA_r_alojret,
                            Globals.glbVoucherIdentifier,
                            Globals.glbRA_ResTVch,
                            Globals.glbVOU_RESNVCH,
                            Globals.glbVOU_RESNVCLI,
                            "N",
                            Globals.glbVOU_RESVDIA,
                            Globals.glbRA_resdiaaut,
                            "N",
                            "S",
                            Globals.glbVOU_RESVKM,
                            Globals.glbVOU_RESVADC,
                            Globals.glbVOU_RESVAVA,
                            Globals.glbVOU_RESVPRTO,
                            Globals.glbVOU_RESVCMB,
                            "S",
                            Globals.glbVOU_RESVDSP,
                            Globals.glbVOU_RESVMOT,
                            Globals.glbVOU_RESVMOTA,
                            Globals.glbVOU_RESVTAXR,
                            Globals.glbVOU_RESVTAXS,
                            Globals.glbRA_ResTpvCh);
                    }

                    if (nModeloVoucher == 2)
                    {
                        string strVoucher_F1 = clientWcf.SetVoucherReserva_F1(
                            Globals.glbCC_R_ANUM,
                            Globals.glbRA_r_alojret,
                            Globals.glbVoucherIdentifier,
                            Globals.glbRA_ResTVch,
                            Globals.glbVOU_RESNVCH,
                            Globals.glbVOU_RESNVCLI,
                            "N",
                            Globals.glbVOU_RESVDIA,
                            Globals.glbRA_resdiaaut,
                            Globals.glbVOU_RESVHORE,
                            Globals.glbVOU_RESVKM,
                            Globals.glbVOU_RESVADC,
                            Globals.glbVOU_RESVAVA,
                            Globals.glbVOU_RESVPRTO,
                            Globals.glbVOU_RESVCMB,
                            "S",
                            Globals.glbVOU_RESVDSP,
                            Globals.glbVOU_RESVMOT,
                            Globals.glbVOU_RESVMOTA,
                            Globals.glbVOU_RESVTAXR,
                            Globals.glbVOU_RESVTAXS,
                            Globals.glbRA_ResTpvCh);
                    }

                    if (nModeloVoucher == 3)
                    {
                        string strVoucher_A = clientWcf.SetVoucherReserva_A(
                            Globals.glbCC_R_ANUM,
                            Globals.glbRA_r_alojret,
                            Globals.glbVoucherIdentifier,
                            Globals.glbRA_ResTVch,
                            Globals.glbVOU_RESNVCH,
                            Globals.glbVOU_RESNVCLI,
                            Globals.glbRA_ResTpvCh);
                    }

                    if (nModeloVoucher == 4)
                    {
                        string strVoucher_O = clientWcf.SetVoucherReserva_O(
                            Globals.glbCC_R_ANUM,
                            Globals.glbRA_r_alojret,
                            Globals.glbVoucherIdentifier,
                            Globals.glbRA_ResTVch,
                            Globals.glbVOU_RESNVCH,
                            Globals.glbVOU_RESNVCLI,
                            Globals.glbRA_ResTpvCh);
                    }

                }

                string xmlRetornoWCF_CC = "";
                xmlRetornoWCF_CC = clientWcf.SetVeiculoContratoRA(Globals.glbVEIPLA, Convert.ToInt32(Globals.glbCC_R_ANUM.Trim()));

                xmlRetornoWCF_CC = clientWcf.SetAtualizaFlag(Convert.ToInt32(Globals.glbCC_R_ANUM.Trim()));

                clientWcf.Close();
            }
            catch (Exception ex)
            {
                return 0;
            }
            return Convert.ToInt32(numeroContratoRA.Trim());
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            tmrReturn.Stop();
            tmrReturn.Enabled = false;
            
            timerDisabled();

            Globals.glbMensagem = 0401;
            frmMensagem frmMsg = new frmMensagem();
            frmMsg.Show();
            this.Close();
            this.Dispose();
        }

        private void tmrReturn_Tick(object sender, EventArgs e)
        {
            timerDisabled();

            tmrReturn.Enabled = false;
            frmScreen01 frm1 = new frmScreen01();
            frm1.Show();
            this.Close();
            this.Dispose();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            tmrReturn.Enabled = false;

            // AQUI - Alterar depois
            timerDisabled();


            frmScreen06 frm06 = new frmScreen06();
            frm06.Show();
            this.Close();
            this.Dispose();
        }

        private void lblSaudacoes_Click(object sender, EventArgs e)
        {

        }

        private void lblConfira_Click(object sender, EventArgs e)
        {

        }

        private void lblDataDevolucao_Click(object sender, EventArgs e)
        {

        }

        private void lblDataDevolucaoVar_Click(object sender, EventArgs e)
        {

        }

        private void label23_Click(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void label28_Click(object sender, EventArgs e)
        {

        }

        private void tmrAguardar_Tick(object sender, EventArgs e)
        {
            lblPorfavor.Text = "Por favor, aguarde ...";
            Application.DoEvents();
        }

        private string RemoverAcentos(string texto)
        {
            if (string.IsNullOrEmpty(texto))
                return String.Empty;
            else
            {
                byte[] bytes = System.Text.Encoding.GetEncoding("iso-8859-8").GetBytes(texto);
                return System.Text.Encoding.UTF8.GetString(bytes);
            }
        }

        private void butIdioma_Click(object sender, EventArgs e)
        {
            frmIdioma frmIdi = new frmIdioma();
            frmIdi.ShowDialog();
            AtualizarInterfaceParaIdiomaCorrente();
        }

        private void AtualizarInterfaceParaIdiomaCorrente()
        {

            MontarResourceSetParaIdiomaCorrente();

            butIdioma.Text = ResxSetForm.GetString("butIdioma.Text");
            lblValorTotal.Text = ResxSetForm.GetString("lblValorTotal.Text");
            lblTaxaRetorno.Text = ResxSetForm.GetString("lblTaxaRetorno.Text.");
            lblTaxaAdministrativa.Text = ResxSetForm.GetString("lblTaxaAdministrativa.Text");
            if (!string.IsNullOrEmpty(Globals.glbPRENOM))
            {
                lblSaudacoes.Text = ResxSetForm.GetString("lblSaudacoes.Text") + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Globals.glbPRENOM.ToLower()) + ResxSetForm.GetString("ComplementoSaudacoes");
            }
            else
            {
                lblSaudacoes.Text = ResxSetForm.GetString("lblSaudacoes.Text") + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Globals.glbCLI_CLINOM_SAUDACOES.ToLower()) + ResxSetForm.GetString("ComplementoSaudacoes");
            }
            lblResumoReserva.Text = ResxSetForm.GetString("lblResumoReserva.Text") + "  " + Globals.glbReservaNumeroPesquisa;
            lblReservaNumero.Text = ResxSetForm.GetString("lblReservaNumero.Text");
            lblProtecoes.Text = ResxSetForm.GetString("lblProtecoes.Text");
            lblPorfavor.Text = ResxSetForm.GetString("lblPorfavor.Text");
            lblLocalRetirada.Text = ResxSetForm.GetString("lblLocalRetirada.Text");
            lblLocalDevolucao.Text = ResxSetForm.GetString("lblLocalDevolucao.Text");
            lblHoraExtra.Text = ResxSetForm.GetString("lblHoraExtra.Text");
            lblGrupoVeiculo.Text = ResxSetForm.GetString("lblGrupoVeiculo.Text");
            lblDiaria.Text = ResxSetForm.GetString("lblDiaria.Text");
            lblDesconto.Text = ResxSetForm.GetString("lblDesconto.Text");
            lblDataRetirada.Text = ResxSetForm.GetString("lblDataRetirada.Text");
            lblDataDevolucao.Text = ResxSetForm.GetString("lblDataDevolucao.Text");
            lblConfira.Text = ResxSetForm.GetString("lblConfira.Text");
            lblAcessorios.Text = ResxSetForm.GetString("lblAcessorios.Text");
            butVoltar.Text = ResxSetForm.GetString("butVoltar.Text");
            butIdioma.Text = ResxSetForm.GetString("butIdioma.Text");
            butConfirmar.Text = ResxSetForm.GetString("butConfirmar.Text");
            lblValorTotalSuperrior.Text = ResxSetForm.GetString("lblValorTotalSuperrior.Text");
            lblValorUnitarioSuperior.Text = ResxSetForm.GetString("lblValorUnitarioSuperior.Text");
            lblQuantidadeSuperior.Text = ResxSetForm.GetString("lblQuantidadeSuperior.Text");

            if (Globals.glbLanguage == 1) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_Brasil_botao;
            if (Globals.glbLanguage == 2) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_Espanha_botao;
            if (Globals.glbLanguage == 3) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_UK_botao;

            butIdioma.Refresh();
        }

        private void MontarResourceSetParaIdiomaCorrente()
        {
            string srtCaminhoExeTotem = System.IO.Path.GetDirectoryName(Application.ExecutablePath.ToString());
            // Português
            if (Globals.glbLanguage == 1)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen08.pt.resx");
            }
            //Espanhol
            else if (Globals.glbLanguage == 2)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen08.es.resx");
            }
            // Inglês
            else if (Globals.glbLanguage == 3)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen08.en.resx");
            }
        }

        private void butVoltar_Click(object sender, EventArgs e)
        {
            tmrReturn.Enabled = false;

            // AQUI - Alterar depois
            timerDisabled();

            frmScreen06 frm06 = new frmScreen06();
            frm06.Show();
            this.Close();
            this.Dispose();
        }

        private void butConfirmar_Click(object sender, EventArgs e)
        {
            tmrReturn.Stop();
            tmrReturn.Enabled = false;
            butConfirmar.Enabled = false;

            try
            {
                int nAberturaRA = SetAberturaRA();
                Globals.glbRA_Contrato = nAberturaRA.ToString().Trim();

                LogTotem.InsertLog(new LogOperacao()
                {
                    Application_Name = Globals.glbApplicationName,
                    Application_Version = Globals.glbApplicationVersion,
                    Local = Globals.glbLojaTotem,
                    Operation_Ticks = Globals.glbOperationTicks,
                    Operation_Name = "Resumo da Reserva - Geração RA",
                    Form = "FrmScreen08",
                    RESNUM = String.IsNullOrEmpty(Globals.glbReservaNumeroPesquisa) ? 0 : Convert.ToInt32(Globals.glbReservaNumeroPesquisa),
                    RANUM = String.IsNullOrEmpty(Globals.glbRA_Contrato) ? 0 : Convert.ToInt32(Globals.glbRA_Contrato)
                });

                if (!string.IsNullOrEmpty(Globals.glbRA_respaxcpf.Trim()))
                {
                    Globals.glbRESDOCCLI = Globals.glbRA_respaxcpf;
                }

                if (!string.IsNullOrEmpty(Globals.glbVoucherIdentifier))
                {
                    if (Globals.glbVOUPRE.Trim().ToUpper() == "S")
                    {
                        timerDisabled();

                        frmScreen11 frm11 = new frmScreen11();
                        frm11.Show();
                        this.Close();
                        this.Dispose();
                    }
                    else
                    {
                        timerDisabled();

                        frmScreen09 frm09 = new frmScreen09();
                        frm09.Show();
                        this.Close();
                        this.Dispose();
                    }
                }
                else
                {
                    timerDisabled();

                    frmScreen09 frm09 = new frmScreen09();
                    frm09.Show();
                    this.Close();
                    this.Dispose();
                }
            }
            catch (Exception ex)
            {
                timerDisabled();

                Globals.glbScreen = 1;
                Globals.glbMensagem = 0203;
                frmMensagem frmMsg = new frmMensagem();
                frmMsg.Show();
                this.Close();
                this.Dispose();
                return;
            }
        }

        private void lblResumoReserva_Click(object sender, EventArgs e)
        {

        }

        private void timerDisabled()
        {
            tmrReturn.Enabled = false;
            tmrAguardar.Enabled = false;
        }


    }
}