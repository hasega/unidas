﻿namespace centralUnidas
{
    partial class frmScreen03
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmScreen03));
            this.FingerprintImage = new System.Windows.Forms.PictureBox();
            this.pictShutdown = new System.Windows.Forms.PictureBox();
            this.pictUnidasLogo = new System.Windows.Forms.PictureBox();
            this.tmrReturn = new System.Windows.Forms.Timer(this.components);
            this.tmrBiometria = new System.Windows.Forms.Timer(this.components);
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.butIdioma = new System.Windows.Forms.Button();
            this.lblBiometria = new System.Windows.Forms.Label();
            this.butVoltar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.FingerprintImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictShutdown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // FingerprintImage
            // 
            this.FingerprintImage.BackColor = System.Drawing.Color.Black;
            this.FingerprintImage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.FingerprintImage.Location = new System.Drawing.Point(12, 256);
            this.FingerprintImage.Name = "FingerprintImage";
            this.FingerprintImage.Size = new System.Drawing.Size(149, 217);
            this.FingerprintImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FingerprintImage.TabIndex = 49;
            this.FingerprintImage.TabStop = false;
            this.FingerprintImage.Visible = false;
            // 
            // pictShutdown
            // 
            this.pictShutdown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictShutdown.BackColor = System.Drawing.Color.Transparent;
            this.pictShutdown.Image = global::centralUnidas.Properties.Resources.shutdown_button;
            this.pictShutdown.Location = new System.Drawing.Point(12, 146);
            this.pictShutdown.Name = "pictShutdown";
            this.pictShutdown.Size = new System.Drawing.Size(50, 55);
            this.pictShutdown.TabIndex = 81;
            this.pictShutdown.TabStop = false;
            this.pictShutdown.Visible = false;
            this.pictShutdown.Click += new System.EventHandler(this.pictShutdown_Click);
            // 
            // pictUnidasLogo
            // 
            this.pictUnidasLogo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictUnidasLogo.BackColor = System.Drawing.Color.Transparent;
            this.pictUnidasLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictUnidasLogo.Image = global::centralUnidas.Properties.Resources.totem_header;
            this.pictUnidasLogo.Location = new System.Drawing.Point(0, 0);
            this.pictUnidasLogo.Name = "pictUnidasLogo";
            this.pictUnidasLogo.Size = new System.Drawing.Size(1366, 140);
            this.pictUnidasLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictUnidasLogo.TabIndex = 87;
            this.pictUnidasLogo.TabStop = false;
            // 
            // tmrReturn
            // 
            this.tmrReturn.Enabled = true;
            this.tmrReturn.Interval = 60000;
            this.tmrReturn.Tick += new System.EventHandler(this.tmrReturn_Tick);
            // 
            // tmrBiometria
            // 
            this.tmrBiometria.Interval = 10;
            this.tmrBiometria.Tick += new System.EventHandler(this.tmrBiometria_Tick);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox2.Image = global::centralUnidas.Properties.Resources.Totem_Traco;
            this.pictureBox2.Location = new System.Drawing.Point(399, 527);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(683, 1);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 140;
            this.pictureBox2.TabStop = false;
            // 
            // butIdioma
            // 
            this.butIdioma.FlatAppearance.BorderSize = 0;
            this.butIdioma.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.butIdioma.ForeColor = System.Drawing.Color.Gray;
            this.butIdioma.Image = global::centralUnidas.Properties.Resources.Totem_Flag_Brasil_botao;
            this.butIdioma.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butIdioma.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.butIdioma.Location = new System.Drawing.Point(1148, 0);
            this.butIdioma.Name = "butIdioma";
            this.butIdioma.Size = new System.Drawing.Size(148, 61);
            this.butIdioma.TabIndex = 164;
            this.butIdioma.Text = "PORTUGUÊS";
            this.butIdioma.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butIdioma.UseCompatibleTextRendering = true;
            this.butIdioma.UseMnemonic = false;
            this.butIdioma.UseVisualStyleBackColor = true;
            this.butIdioma.Click += new System.EventHandler(this.butIdioma_Click);
            // 
            // lblBiometria
            // 
            this.lblBiometria.AutoSize = true;
            this.lblBiometria.Font = new System.Drawing.Font("Arial", 35F, System.Drawing.FontStyle.Bold);
            this.lblBiometria.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblBiometria.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblBiometria.Location = new System.Drawing.Point(292, 426);
            this.lblBiometria.Name = "lblBiometria";
            this.lblBiometria.Size = new System.Drawing.Size(960, 110);
            this.lblBiometria.TabIndex = 166;
            this.lblBiometria.Text = "Posicione o dedo indicador da sua\r\nmão esquerda no dispositivo ao seu lado.";
            this.lblBiometria.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // butVoltar
            // 
            this.butVoltar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.butVoltar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.butVoltar.Font = new System.Drawing.Font("Arial", 20F, System.Drawing.FontStyle.Bold);
            this.butVoltar.ForeColor = System.Drawing.Color.White;
            this.butVoltar.Location = new System.Drawing.Point(598, 641);
            this.butVoltar.Name = "butVoltar";
            this.butVoltar.Size = new System.Drawing.Size(251, 72);
            this.butVoltar.TabIndex = 167;
            this.butVoltar.Text = "Voltar";
            this.butVoltar.UseVisualStyleBackColor = false;
            this.butVoltar.Click += new System.EventHandler(this.butVoltar_Click);
            // 
            // frmScreen03
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1366, 768);
            this.ControlBox = false;
            this.Controls.Add(this.butVoltar);
            this.Controls.Add(this.lblBiometria);
            this.Controls.Add(this.butIdioma);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictUnidasLogo);
            this.Controls.Add(this.pictShutdown);
            this.Controls.Add(this.FingerprintImage);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmScreen03";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CAU - Unidas";
            this.TopMost = false;
            this.WindowState = System.Windows.Forms.FormWindowState.Normal;
            this.Load += new System.EventHandler(this.frmScreen03_Load);
            ((System.ComponentModel.ISupportInitialize)(this.FingerprintImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictShutdown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox FingerprintImage;
        private System.Windows.Forms.PictureBox pictShutdown;
        private System.Windows.Forms.PictureBox pictUnidasLogo;
        private System.Windows.Forms.Timer tmrReturn;
        private System.Windows.Forms.Timer tmrBiometria;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button butIdioma;
        private System.Windows.Forms.Label lblBiometria;
        private System.Windows.Forms.Button butVoltar;
    }
}