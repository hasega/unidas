﻿// Aplicação: Unidas Totem
// Data     : 01/10/2015
// Versão   : 1.0
// Empresa  : Unidas Rent a Car
// Equipe   : Unidas Desenvolvimento
// Objetivo : Tela de abertura
// Histórico: 
//      01/10/2015: Inicio de desenvolvimento para acesso geral
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace centralUnidas
{
    public partial class frmSplashScreen : Form
    {
        int nCount = 20;
        public frmSplashScreen()
        {
            InitializeComponent();
        }

        private void frmSplashScreen_Load(object sender, EventArgs e)
        {
            this.TransparencyKey = BackColor;
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;
            lblVersion.Text = String.Format("Versao: {0}", version);
        }

        private void tmrFadeOut_Tick(object sender, EventArgs e)
        {
            if (this.Opacity == 1)
            {
                tmrFadeOut.Enabled = false;
                tmrSplash.Enabled = true;
                return;
            }
            this.Opacity += 0.01;
        }

        private void tmrSplash_Tick(object sender, EventArgs e)
        {
            if (nCount == 0)
            {
                tmrSplash.Enabled = false;
                tmrFadeIn.Enabled = true;
                return;
            }
            nCount -= 1;
        }

        private void tmrFadeIn_Tick(object sender, EventArgs e)
        {
            if (this.Opacity == 0)
            {
                tmrFadeIn.Enabled = false;

                frmScreen01 frm = new frmScreen01();
                frm.Show();
                this.Hide();
                return;
            }
            this.Opacity -= 0.01;
        }


    }
}
