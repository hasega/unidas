﻿// Aplicação: Unidas Totem
// Data     : 01/10/2015
// Versão   : 1.0
// Empresa  : Unidas Rent a Car
// Equipe   : Unidas Desenvolvimento
// Objetivo : Informa codigo de segurança 
// Histórico: 
//      01/10/2015: Inicio de desenvolvimento para acesso geral
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;
using centralUnidas.centralUnidasWCF;
using System.Resources;

namespace centralUnidas
{
    public partial class frmScreen10 : Form
    {

        private int gConfirma = 0;
        private int gTeclado = 1;
        private int gPrint = 0;
        public ResXResourceSet ResxSetForm; 

        PrintDocument pdoc = null;
        public frmScreen10()
        {
            InitializeComponent();
        }

        private void frmScreen10_Load(object sender, EventArgs e)
        {

            LogTotem.InsertLog(new LogOperacao()
            {
                Application_Name = Globals.glbApplicationName,
                Application_Version = Globals.glbApplicationVersion,
                Local = Globals.glbLojaTotem,
                Operation_Ticks = Globals.glbOperationTicks,
                Operation_Name = "Informa Código de Segurança do Cartão",
                Form = "FrmScreen10",
                RESNUM = String.IsNullOrEmpty(Globals.glbReservaNumeroPesquisa) ? 0 : Convert.ToInt32(Globals.glbReservaNumeroPesquisa),
                RANUM = String.IsNullOrEmpty(Globals.glbRA_Contrato) ? 0 : Convert.ToInt32(Globals.glbRA_Contrato)

            });

            Globals.glbScreen = 6;
            int nScreenWidth = this.Width;
            int nScreenWidthSplit = nScreenWidth / 2;

            this.Refresh();

            Globals.glbDebugMode = Convert.ToInt16(ConfigurationManager.AppSettings.Get("appDebugMode"));
            Shutdown_Enable();

            AtualizarInterfaceParaIdiomaCorrente();

            butContinuar.Enabled = true;

        }

        private void Shutdown_Enable()
        {
            if (Globals.glbDebugMode == 0)
                pictShutdown.Visible = false;
            else
                pictShutdown.Visible = true;
        }

        private void pictShutdown_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(1);
        }

        private void pictBack_Click(object sender, EventArgs e)
        {
            timerDisabled();

            frmScreen09 frm9 = new frmScreen09();
            frm9.Show();
            this.Close();
            this.Dispose();
        }

        private void pictReservaAlterar_Click(object sender, EventArgs e)
        {
            timerDisabled();

            Globals.glbMensagem = 0401;
            frmMensagem frmMsg = new frmMensagem();
            frmMsg.Show();
            this.Close();
            this.Dispose();
        }

        private void pictReservaConfirmar_Click(object sender, EventArgs e)
        {
            timerDisabled();

            frmScreen11 frm11 = new frmScreen11();
            frm11.Show();
            this.Close();
            this.Dispose();
        }

        private void pictTec_1_Click(object sender, EventArgs e)
        {
            int nMaxLength = 3;
            if (Globals.glbBandeiraCartao.Trim().Contains("AMX"))
            {
                nMaxLength = 4;
            }

            if (txtCodigoSeguranca.Text.Length < nMaxLength)
            {
                txtCodigoSeguranca.Text = txtCodigoSeguranca.Text + "1";
                txtCodigoSeguranca.SelectionStart = txtCodigoSeguranca.Text.Length + 1;
            }
        }

        private void pictTec_2_Click(object sender, EventArgs e)
        {
            int nMaxLength = 3;
            if (Globals.glbBandeiraCartao.Trim().Contains("AMX"))
            {
                nMaxLength = 4;
            }

            if (txtCodigoSeguranca.Text.Length < nMaxLength)
            {
                txtCodigoSeguranca.Text = txtCodigoSeguranca.Text + "2";
                txtCodigoSeguranca.SelectionStart = txtCodigoSeguranca.Text.Length + 1;
            }
        }

        private void pictTec_3_Click(object sender, EventArgs e)
        {
            int nMaxLength = 3;
            if (Globals.glbBandeiraCartao.Trim().Contains("AMX"))
            {
                nMaxLength = 4;
            }

            if (txtCodigoSeguranca.Text.Length < nMaxLength)
            {
                txtCodigoSeguranca.Text = txtCodigoSeguranca.Text + "3";
                txtCodigoSeguranca.SelectionStart = txtCodigoSeguranca.Text.Length + 1;
            }
        }

        private void pictTec_4_Click(object sender, EventArgs e)
        {
            int nMaxLength = 3;
            if (Globals.glbBandeiraCartao.Trim().Contains("AMX"))
            {
                nMaxLength = 4;
            }

            if (txtCodigoSeguranca.Text.Length < nMaxLength)
            {
                txtCodigoSeguranca.Text = txtCodigoSeguranca.Text + "4";
                txtCodigoSeguranca.SelectionStart = txtCodigoSeguranca.Text.Length + 1;
            }
        }

        private void pictTec_5_Click(object sender, EventArgs e)
        {
            int nMaxLength = 3;
            if (Globals.glbBandeiraCartao.Trim().Contains("AMX"))
            {
                nMaxLength = 4;
            }

            if (txtCodigoSeguranca.Text.Length < nMaxLength)
            {
                txtCodigoSeguranca.Text = txtCodigoSeguranca.Text + "5";
                txtCodigoSeguranca.SelectionStart = txtCodigoSeguranca.Text.Length + 1;
            }
        }

        private void pictTec_6_Click(object sender, EventArgs e)
        {
            int nMaxLength = 3;
            if (Globals.glbBandeiraCartao.Trim().Contains("AMX"))
            {
                nMaxLength = 4;
            }

            if (txtCodigoSeguranca.Text.Length < nMaxLength)
            {
                txtCodigoSeguranca.Text = txtCodigoSeguranca.Text + "6";
                txtCodigoSeguranca.SelectionStart = txtCodigoSeguranca.Text.Length + 1;
            }
        }

        private void pictTec_7_Click(object sender, EventArgs e)
        {
            int nMaxLength = 3;
            if (Globals.glbBandeiraCartao.Trim().Contains("AMX"))
            {
                nMaxLength = 4;
            }

            if (txtCodigoSeguranca.Text.Length < nMaxLength)
            {
                txtCodigoSeguranca.Text = txtCodigoSeguranca.Text + "7";
                txtCodigoSeguranca.SelectionStart = txtCodigoSeguranca.Text.Length + 1;
            }
        }

        private void pictTec_8_Click(object sender, EventArgs e)
        {
            int nMaxLength = 3;
            if (Globals.glbBandeiraCartao.Trim().Contains("AMX"))
            {
                nMaxLength = 4;
            }

            if (txtCodigoSeguranca.Text.Length < nMaxLength)
            {
                txtCodigoSeguranca.Text = txtCodigoSeguranca.Text + "8";
                txtCodigoSeguranca.SelectionStart = txtCodigoSeguranca.Text.Length + 1;
            }
        }

        private void pictTec_9_Click(object sender, EventArgs e)
        {
            int nMaxLength = 3;
            if (Globals.glbBandeiraCartao.Trim().Contains("AMX"))
            {
                nMaxLength = 4;
            }

            if (txtCodigoSeguranca.Text.Length < nMaxLength)
            {
                txtCodigoSeguranca.Text = txtCodigoSeguranca.Text + "9";
                txtCodigoSeguranca.SelectionStart = txtCodigoSeguranca.Text.Length + 1;
            }
        }

        private void pictTec_0_Click(object sender, EventArgs e)
        {
            int nMaxLength = 3;
            if (Globals.glbBandeiraCartao.Trim().Contains("AMX"))
            {
                nMaxLength = 4;
            }

            if (txtCodigoSeguranca.Text.Length < nMaxLength)
            {
                txtCodigoSeguranca.Text = txtCodigoSeguranca.Text + "0";
                txtCodigoSeguranca.SelectionStart = txtCodigoSeguranca.Text.Length + 1;
            }
        }

        private void pictTec_BACK2_Click(object sender, EventArgs e)
        {

            int nTamanho = txtCodigoSeguranca.Text.Length;
            if (nTamanho > 0)
            {
                txtCodigoSeguranca.Text = txtCodigoSeguranca.Text.Substring(0, nTamanho - 1);
            }
            txtCodigoSeguranca.SelectionStart = txtCodigoSeguranca.Text.Length + 1;
        }

        private void pictTec_TECLADO_Click(object sender, EventArgs e)
        {
            // tmrTeclado.Enabled = true;
        }

        private void tmrTeclado_Tick(object sender, EventArgs e)
        {
            tmrTeclado.Enabled = false;
            if (gTeclado == 1)
            {
                Teclado_Hide();
                gTeclado = 2;
            }
            else
            {
                Teclado_Show();
                gTeclado = 1;
            }
        }

        private void Teclado_Hide()
        {
            int nTarget = pictTecladoFundo.Top + pictTecladoFundo.Height + 100;
            int x = pictTecladoFundo.Top;
            // Primeira fileira
            int tec1 = pictTec_1.Top - x;
            int tec2 = pictTec_2.Top - x;
            int tec3 = pictTec_3.Top - x;

            // Segunda fileira
            int tec4 = pictTec_4.Top - x;
            int tec5 = pictTec_5.Top - x;
            int tec6 = pictTec_6.Top - x;

            // Terceira fileira
            int tec7 = pictTec_7.Top - x;
            int tec8 = pictTec_8.Top - x;
            int tec9 = pictTec_9.Top - x;

            // Quarta fileira
            int tecTECLADO = pictTec_TECLADO.Top - x;
            int tec0 = pictTec_0.Top - x;
            int tecBACK2 = pictTec_BACK2.Top - x;

            while (x < nTarget)
            {

                pictTecladoFundo.Top = x;

                // PRIMEIRA FILEIRA
                pictTec_1.Top = tec1 + x;
                pictTec_2.Top = tec2 + x;
                pictTec_3.Top = tec3 + x;

                // SEGUNDA FILEIRA
                pictTec_4.Top = tec4 + x;
                pictTec_5.Top = tec5 + x;
                pictTec_6.Top = tec6 + x;

                // TERCEIRA FILEIRA
                pictTec_7.Top = tec7 + x;
                pictTec_8.Top = tec8 + x;
                pictTec_9.Top = tec9 + x;

                // QUARTA FILEIRA
                pictTec_TECLADO.Top = tecTECLADO + x;
                pictTec_0.Top = tec0 + x;
                pictTec_BACK2.Top = tecBACK2 + x;

                this.Refresh();
                x = x + 100;
            }
        }

        private void Teclado_Show()
        {
            int nTarget = this.Height - pictTecladoFundo.Height;
            int x = pictTecladoFundo.Top;
            // Primeira fileira
            int tec1 = pictTec_1.Top - x;
            int tec2 = pictTec_2.Top - x;
            int tec3 = pictTec_3.Top - x;

            // Segunda fileira
            int tec4 = pictTec_4.Top - x;
            int tec5 = pictTec_5.Top - x;
            int tec6 = pictTec_6.Top - x;

            // Terceira fileira
            int tec7 = pictTec_7.Top - x;
            int tec8 = pictTec_8.Top - x;
            int tec9 = pictTec_9.Top - x;

            // Quarta fileira
            int tecTECLADO = pictTec_TECLADO.Top - x;
            int tec0 = pictTec_0.Top - x;
            int tecBACK2 = pictTec_BACK2.Top - x;

            while (x > nTarget)
            {

                pictTecladoFundo.Top = x;

                // PRIMEIRA FILEIRA
                pictTec_1.Top = tec1 + x;
                pictTec_2.Top = tec2 + x;
                pictTec_3.Top = tec3 + x;

                // SEGUNDA FILEIRA
                pictTec_4.Top = tec4 + x;
                pictTec_5.Top = tec5 + x;
                pictTec_6.Top = tec6 + x;

                // TERCEIRA FILEIRA
                pictTec_7.Top = tec7 + x;
                pictTec_8.Top = tec8 + x;
                pictTec_9.Top = tec9 + x;

                // QUARTA FILEIRA
                pictTec_TECLADO.Top = tecTECLADO + x;
                pictTec_0.Top = tec0 + x;
                pictTec_BACK2.Top = tecBACK2 + x;

                this.Refresh();
                x = x - 100;
            }
        }

        //private void pictureBox5_Click(object sender, EventArgs e)
        //{
        //    pictureBox5.Enabled = false;

        //    // Permite apenas um click para evitar abertura de varias telas
        //    if (gConfirma == 0)
        //    {
        //        tmrReturn.Stop();
        //        tmrReturn.Enabled = false;
        //        bool bValida = true;

        //        if (txtCodigoSeguranca.Text.Trim().Length != 3) bValida = false;

        //        if (bValida)
        //        {
        //            Globals.glbCC_ValidadeInformado = txtCodigoSeguranca.Text.Trim();
        //            frmScreen11 frm11 = new frmScreen11();
        //            frm11.Show();
        //            this.Close();
        //            this.Dispose();
        //        }
        //        else
        //        {
        //            Globals.glbScreen = 10;
        //            Globals.glbMensagem = 1007;
        //            frmMensagem frmMsg = new frmMensagem();
        //            frmMsg.Show();
        //            this.Close();
        //            this.Dispose();
        //        }
        //    }
        //    gConfirma = 1;
        //}

        private void tmrReturn_Tick(object sender, EventArgs e)
        {
            tmrReturn.Enabled = false;

            CentralUnidasClassClient clientWcf = new CentralUnidasClassClient();
            string xmlRetornoWCF = clientWcf.getCupomFiscal(Convert.ToInt32(Globals.glbReservaNumeroPesquisa));
            if (string.IsNullOrEmpty(xmlRetornoWCF) || (xmlRetornoWCF.Trim() == "<UNIDASWCF />"))
            {
                Globals.glbCupomFiscal1 = "";
                Globals.glbCupomFiscal2 = "";
                Globals.glbCC_CodigoPreAutorizacao = "";
                Globals.glbCC_CodigoAutorizacaoEmissor = "";
                Globals.glbBandeiraCartao = "";
            }
            else
            {
                xmlRetornoWCF = xmlRetornoWCF.Replace("\r\n", "");
                DataSet dtSet = new DataSet();
                System.IO.StringReader xmlSR = new System.IO.StringReader(xmlRetornoWCF);
                dtSet.ReadXml(xmlSR);
                foreach (DataRow dbRow in dtSet.Tables["Table"].Rows)
                {

                    Globals.glbCupomFiscal1 = dbRow["CUPOM_CLIENTE"].ToString().Trim();
                    Globals.glbCupomFiscal2 = dbRow["CUPOM_LOJA"].ToString().Trim();
                    Globals.glbCC_CodigoPreAutorizacao = dbRow["CODIGO_PREAUTORIZACAO"].ToString().Trim();
                    Globals.glbCC_CodigoAutorizacaoEmissor = dbRow["CODIGO_EMISSOR"].ToString().Trim();

                    string strCupomString = "";
                    strCupomString = Globals.glbCupomFiscal1.Trim();
                    if (strCupomString.ToUpper().Contains("REDE")) Globals.glbBandeiraCartao = "RED";
                    if (strCupomString.ToUpper().Contains("AMERICAN")) Globals.glbBandeiraCartao = "AMX";
                    if (strCupomString.ToUpper().Contains("CIELO")) Globals.glbBandeiraCartao = "CIE";
                    if (strCupomString.ToUpper().Contains("DINERS")) Globals.glbBandeiraCartao = "DNR";
                    if (strCupomString.ToUpper().Contains("ELO")) Globals.glbBandeiraCartao = "ELO";
                    if (strCupomString.ToUpper().Contains("MASTERCARD")) Globals.glbBandeiraCartao = "MAS";
                    if (strCupomString.ToUpper().Contains("VISA")) Globals.glbBandeiraCartao = "VIS";

                    break;
                }
            }

            int nRetorno = 0;
            nRetorno = printRA();

            timerDisabled();
            frmScreen01 frm1 = new frmScreen01();
            frm1.Show();
            this.Close();
            this.Dispose();
        }

        private void txtCodigoSeguranca_TextChanged(object sender, EventArgs e)
        {
            tmrReturn.Stop();
            tmrReturn.Start();
        }

        private void pictVoltar_Click(object sender, EventArgs e)
        {
            tmrReturn.Stop();
            tmrReturn.Enabled = false;

            CentralUnidasClassClient clientWcf = new CentralUnidasClassClient();
            string xmlRetornoWCF = clientWcf.getCupomFiscal(Convert.ToInt32(Globals.glbReservaNumeroPesquisa));
            if (string.IsNullOrEmpty(xmlRetornoWCF) || (xmlRetornoWCF.Trim() == "<UNIDASWCF />"))
            {
                Globals.glbCupomFiscal1 = "";
                Globals.glbCupomFiscal2 = "";
                Globals.glbCC_CodigoPreAutorizacao = "";
                Globals.glbCC_CodigoAutorizacaoEmissor = "";
                Globals.glbBandeiraCartao = "";
            }
            else
            {
                xmlRetornoWCF = xmlRetornoWCF.Replace("\r\n", "");
                DataSet dtSet = new DataSet();
                System.IO.StringReader xmlSR = new System.IO.StringReader(xmlRetornoWCF);
                dtSet.ReadXml(xmlSR);
                foreach (DataRow dbRow in dtSet.Tables["Table"].Rows)
                {

                    Globals.glbCupomFiscal1 = dbRow["CUPOM_CLIENTE"].ToString().Trim();
                    Globals.glbCupomFiscal2 = dbRow["CUPOM_LOJA"].ToString().Trim();
                    Globals.glbCC_CodigoPreAutorizacao = dbRow["CODIGO_PREAUTORIZACAO"].ToString().Trim();
                    Globals.glbCC_CodigoAutorizacaoEmissor = dbRow["CODIGO_EMISSOR"].ToString().Trim();

                    string strCupomString = "";
                    strCupomString = Globals.glbCupomFiscal1.Trim();
                    if (strCupomString.ToUpper().Contains("REDE")) Globals.glbBandeiraCartao = "RED";
                    if (strCupomString.ToUpper().Contains("AMERICAN")) Globals.glbBandeiraCartao = "AMX";
                    if (strCupomString.ToUpper().Contains("CIELO")) Globals.glbBandeiraCartao = "CIE";
                    if (strCupomString.ToUpper().Contains("DINERS")) Globals.glbBandeiraCartao = "DNR";
                    if (strCupomString.ToUpper().Contains("ELO")) Globals.glbBandeiraCartao = "ELO";
                    if (strCupomString.ToUpper().Contains("MASTERCARD")) Globals.glbBandeiraCartao = "MAS";
                    if (strCupomString.ToUpper().Contains("VISA")) Globals.glbBandeiraCartao = "VIS";

                    break;
                }
            }

            int nRetorno = 0;
            nRetorno = printRA();

            if (gPrint > 0)
            {
                timerDisabled();

                Globals.glbScreen = 15;
                Globals.glbMensagem = 1015;
                frmMensagem frmMsg = new frmMensagem();
                frmMsg.Show();
                this.Close();
                this.Dispose();
            }
            else
            {
                timerDisabled();

                frmScreen01 frm02 = new frmScreen01();
                frm02.Show();
                this.Close();
                this.Dispose(); 
            }
        }

        private int printRA()
        {
            try
            {
                pdoc = new PrintDocument();
                PrinterSettings ps = new PrinterSettings();

                pdoc.PrintPage += new PrintPageEventHandler(pdoc_PrintPage);
                PaperSize pkSize = new PaperSize("RA", 300, 1100);
                Margins pkMargin = new Margins(10, 10, 10, 10);

                pdoc.OriginAtMargins = true;
                pdoc.DefaultPageSettings.Margins.Left = 20;
                pdoc.DefaultPageSettings.Margins.Right = 20;
                pdoc.DefaultPageSettings.Margins.Top = 20;
                pdoc.DefaultPageSettings.Margins.Bottom = 20;
                pdoc.DefaultPageSettings.PaperSize = pkSize;
                pdoc.PrinterSettings.DefaultPageSettings.PaperSize = new PaperSize("RA", 300, 1100);
                pdoc.PrinterSettings.DefaultPageSettings.Margins.Left = 20;
                pdoc.PrinterSettings.DefaultPageSettings.Margins.Right = 20;
                pdoc.PrinterSettings.DefaultPageSettings.Margins.Top = 20;
                pdoc.PrinterSettings.DefaultPageSettings.Margins.Bottom = 20;
                pdoc.PrinterSettings.DefaultPageSettings.PrinterSettings.DefaultPageSettings.PaperSize = new PaperSize("RA", 300, 1100);
                pdoc.DocumentName = "RA";
                pdoc.Print();
            }
            catch (Exception ex)
            {

            }
            return gPrint;
        }

        void pdoc_PrintPage(object sender, PrintPageEventArgs e)
        {
            try
            {
                Graphics graphics = e.Graphics;
                Font font = new Font("Courier New", 9, FontStyle.Bold);
                float fontHeight = font.GetHeight();

                int startX = 3;
                int startY = 5;
                int Offset = 40;

                if (!string.IsNullOrEmpty(Globals.glbCupomFiscal1))
                {
                    gPrint = 1;

                    graphics.DrawString("--------------------------------------------", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    Offset = Offset + 17;
                    graphics.DrawString("               CUPOM FISCAL                  ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    Offset = Offset + 17;
                    Offset = Offset + 17;
                    graphics.DrawString(Globals.glbCupomFiscal1, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    for (int x = 0; x < 17; x++)
                    {
                        Offset = Offset + 17;
                    }
                }

                if (!string.IsNullOrEmpty(Globals.glbCupomFiscal2))
                {

                    gPrint = 1;

                    graphics.DrawString("--------------------------------------------", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    Offset = Offset + 17;
                    graphics.DrawString("               CUPOM FISCAL                  ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    Offset = Offset + 17;
                    Offset = Offset + 17;
                    graphics.DrawString(Globals.glbCupomFiscal2, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    for (int x = 0; x < 17; x++)
                    {
                        Offset = Offset + 17;
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void butVoltar_Click(object sender, EventArgs e)
        {
            tmrReturn.Stop();
            tmrReturn.Enabled = false;

            CentralUnidasClassClient clientWcf = new CentralUnidasClassClient();
            string xmlRetornoWCF = clientWcf.getCupomFiscal(Convert.ToInt32(Globals.glbReservaNumeroPesquisa));
            if (string.IsNullOrEmpty(xmlRetornoWCF) || (xmlRetornoWCF.Trim() == "<UNIDASWCF />"))
            {
                Globals.glbCupomFiscal1 = "";
                Globals.glbCupomFiscal2 = "";
                Globals.glbCC_CodigoPreAutorizacao = "";
                Globals.glbCC_CodigoAutorizacaoEmissor = "";
                Globals.glbBandeiraCartao = "";
            }
            else
            {
                xmlRetornoWCF = xmlRetornoWCF.Replace("\r\n", "");
                DataSet dtSet = new DataSet();
                System.IO.StringReader xmlSR = new System.IO.StringReader(xmlRetornoWCF);
                dtSet.ReadXml(xmlSR);
                foreach (DataRow dbRow in dtSet.Tables["Table"].Rows)
                {

                    Globals.glbCupomFiscal1 = dbRow["CUPOM_CLIENTE"].ToString().Trim();
                    Globals.glbCupomFiscal2 = dbRow["CUPOM_LOJA"].ToString().Trim();
                    Globals.glbCC_CodigoPreAutorizacao = dbRow["CODIGO_PREAUTORIZACAO"].ToString().Trim();
                    Globals.glbCC_CodigoAutorizacaoEmissor = dbRow["CODIGO_EMISSOR"].ToString().Trim();

                    string strCupomString = "";
                    strCupomString = Globals.glbCupomFiscal1.Trim();
                    if (strCupomString.ToUpper().Contains("REDE")) Globals.glbBandeiraCartao = "RED";
                    if (strCupomString.ToUpper().Contains("AMERICAN")) Globals.glbBandeiraCartao = "AMX";
                    if (strCupomString.ToUpper().Contains("CIELO")) Globals.glbBandeiraCartao = "CIE";
                    if (strCupomString.ToUpper().Contains("DINERS")) Globals.glbBandeiraCartao = "DNR";
                    if (strCupomString.ToUpper().Contains("ELO")) Globals.glbBandeiraCartao = "ELO";
                    if (strCupomString.ToUpper().Contains("MASTERCARD")) Globals.glbBandeiraCartao = "MAS";
                    if (strCupomString.ToUpper().Contains("VISA")) Globals.glbBandeiraCartao = "VIS";

                    break;
                }
            }

            int nRetorno = 0;
            nRetorno = printRA();

            if (gPrint > 0)
            {
                timerDisabled();

                Globals.glbScreen = 15;
                Globals.glbMensagem = 1015;
                frmMensagem frmMsg = new frmMensagem();
                frmMsg.Show();
                this.Close();
                this.Dispose();
            }
            else
            {
                timerDisabled();

                frmScreen01 frm02 = new frmScreen01();
                frm02.Show();
                this.Close();
                this.Dispose();
            }
        }

        private void butContinuar_Click(object sender, EventArgs e)
        {
            butContinuar.Enabled = false;

            // Permite apenas um click para evitar abertura de varias telas
            if (gConfirma == 0)
            {
                tmrReturn.Stop();
                tmrReturn.Enabled = false;
                bool bValida = true;

                if (txtCodigoSeguranca.Text.Trim().Length != 3) bValida = false;

                if (bValida)
                {
                    timerDisabled();

                    Globals.glbCC_ValidadeInformado = txtCodigoSeguranca.Text.Trim();
                    frmScreen11 frm11 = new frmScreen11();
                    frm11.Show();
                    this.Close();
                    this.Dispose();
                }
                else
                {
                    timerDisabled();

                    Globals.glbScreen = 10;
                    Globals.glbMensagem = 1007;
                    frmMensagem frmMsg = new frmMensagem();
                    frmMsg.Show();
                    this.Close();
                    this.Dispose();
                }
            }
            gConfirma = 1;
        }

        private void AtualizarInterfaceParaIdiomaCorrente()
        {

            MontarResourceSetParaIdiomaCorrente();

            butIdioma.Text = ResxSetForm.GetString("butIdioma.Text");
            butVoltar.Text = ResxSetForm.GetString("butVoltar.Text");
            lblCodigoSegurancaoMenor.Text = ResxSetForm.GetString("lblCodigoSegurancaoMenor.Text");
            lblCodigoSegurancaMaior.Text = ResxSetForm.GetString("lblCodigoSegurancaMaior.Text");
            lblCodigo.Text = ResxSetForm.GetString("lblCodigo.Text");
          
            if (Globals.glbLanguage == 1) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_Brasil_botao;
            if (Globals.glbLanguage == 2) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_Espanha_botao;
            if (Globals.glbLanguage == 3) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_UK_botao;

            butIdioma.Refresh();
        }

        private void MontarResourceSetParaIdiomaCorrente()
        {
            string srtCaminhoExeTotem = System.IO.Path.GetDirectoryName(Application.ExecutablePath.ToString());
            // Português
            if (Globals.glbLanguage == 1)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen10.pt.resx");
            }
            //Espanhol
            else if (Globals.glbLanguage == 2)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen10.es.resx");
            }
            // Inglês
            else if (Globals.glbLanguage == 3)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen10.en.resx");
            }
        }

        private void butIdioma_Click(object sender, EventArgs e)
        {
            frmIdioma frmIdi = new frmIdioma();
            frmIdi.ShowDialog();
            AtualizarInterfaceParaIdiomaCorrente();
        }

        private void timerDisabled()
        {
            tmrReturn.Enabled = false;
            tmrTeclado.Enabled = false;
        }
    }
}
