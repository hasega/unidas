﻿namespace centralUnidas
{
    partial class frmMensagem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMensagem));
            this.pictShutdown = new System.Windows.Forms.PictureBox();
            this.richMensagem = new System.Windows.Forms.RichTextBox();
            this.tmrReturn = new System.Windows.Forms.Timer(this.components);
            this.pictUnidasLogo = new System.Windows.Forms.PictureBox();
            this.pictLinha = new System.Windows.Forms.PictureBox();
            this.butIdioma = new System.Windows.Forms.Button();
            this.butVoltar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictShutdown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictLinha)).BeginInit();
            this.SuspendLayout();
            // 
            // pictShutdown
            // 
            this.pictShutdown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictShutdown.BackColor = System.Drawing.Color.Transparent;
            this.pictShutdown.Image = global::centralUnidas.Properties.Resources.shutdown_button;
            this.pictShutdown.Location = new System.Drawing.Point(12, 146);
            this.pictShutdown.Name = "pictShutdown";
            this.pictShutdown.Size = new System.Drawing.Size(50, 55);
            this.pictShutdown.TabIndex = 81;
            this.pictShutdown.TabStop = false;
            this.pictShutdown.Visible = false;
            this.pictShutdown.Click += new System.EventHandler(this.pictShutdown_Click);
            // 
            // richMensagem
            // 
            this.richMensagem.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richMensagem.BackColor = System.Drawing.Color.White;
            this.richMensagem.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.richMensagem.Font = new System.Drawing.Font("Arial", 32.25F, System.Drawing.FontStyle.Bold);
            this.richMensagem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.richMensagem.Location = new System.Drawing.Point(231, 246);
            this.richMensagem.Name = "richMensagem";
            this.richMensagem.ReadOnly = true;
            this.richMensagem.Size = new System.Drawing.Size(1095, 298);
            this.richMensagem.TabIndex = 86;
            this.richMensagem.Text = "";
            this.richMensagem.TextChanged += new System.EventHandler(this.richMensagem_TextChanged);
            // 
            // tmrReturn
            // 
            this.tmrReturn.Interval = 10000;
            this.tmrReturn.Tick += new System.EventHandler(this.tmrReturn_Tick);
            // 
            // pictUnidasLogo
            // 
            this.pictUnidasLogo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictUnidasLogo.BackColor = System.Drawing.Color.Transparent;
            this.pictUnidasLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictUnidasLogo.Image = global::centralUnidas.Properties.Resources.totem_header;
            this.pictUnidasLogo.Location = new System.Drawing.Point(0, 0);
            this.pictUnidasLogo.Name = "pictUnidasLogo";
            this.pictUnidasLogo.Size = new System.Drawing.Size(1456, 140);
            this.pictUnidasLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictUnidasLogo.TabIndex = 93;
            this.pictUnidasLogo.TabStop = false;
            this.pictUnidasLogo.Click += new System.EventHandler(this.pictUnidasLogo_Click);
            // 
            // pictLinha
            // 
            this.pictLinha.Image = global::centralUnidas.Properties.Resources.totem_biometria_linha0;
            this.pictLinha.Location = new System.Drawing.Point(231, 563);
            this.pictLinha.Name = "pictLinha";
            this.pictLinha.Size = new System.Drawing.Size(1005, 9);
            this.pictLinha.TabIndex = 96;
            this.pictLinha.TabStop = false;
            // 
            // butIdioma
            // 
            this.butIdioma.FlatAppearance.BorderSize = 0;
            this.butIdioma.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.butIdioma.ForeColor = System.Drawing.Color.Gray;
            this.butIdioma.Image = global::centralUnidas.Properties.Resources.Totem_Flag_Brasil_botao;
            this.butIdioma.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butIdioma.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.butIdioma.Location = new System.Drawing.Point(1148, 0);
            this.butIdioma.Name = "butIdioma";
            this.butIdioma.Size = new System.Drawing.Size(148, 61);
            this.butIdioma.TabIndex = 165;
            this.butIdioma.Text = "PORTUGUÊS";
            this.butIdioma.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butIdioma.UseCompatibleTextRendering = true;
            this.butIdioma.UseMnemonic = false;
            this.butIdioma.UseVisualStyleBackColor = true;
            // 
            // butVoltar
            // 
            this.butVoltar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.butVoltar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.butVoltar.Font = new System.Drawing.Font("Arial", 20F, System.Drawing.FontStyle.Bold);
            this.butVoltar.ForeColor = System.Drawing.Color.White;
            this.butVoltar.Location = new System.Drawing.Point(595, 645);
            this.butVoltar.Name = "butVoltar";
            this.butVoltar.Size = new System.Drawing.Size(341, 72);
            this.butVoltar.TabIndex = 168;
            this.butVoltar.Text = "Voltar";
            this.butVoltar.UseVisualStyleBackColor = false;
            this.butVoltar.Click += new System.EventHandler(this.butVoltar_Click);
            // 
            // frmMensagem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1366, 768);
            this.ControlBox = false;
            this.Controls.Add(this.butVoltar);
            this.Controls.Add(this.butIdioma);
            this.Controls.Add(this.pictLinha);
            this.Controls.Add(this.pictUnidasLogo);
            this.Controls.Add(this.richMensagem);
            this.Controls.Add(this.pictShutdown);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMensagem";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CAU - Unidas";
            this.TopMost = false;
            this.WindowState = System.Windows.Forms.FormWindowState.Normal;
            this.Load += new System.EventHandler(this.frmMensagem_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictShutdown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictLinha)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictShutdown;
        private System.Windows.Forms.RichTextBox richMensagem;
        private System.Windows.Forms.Timer tmrReturn;
        private System.Windows.Forms.PictureBox pictUnidasLogo;
        private System.Windows.Forms.PictureBox pictLinha;
        private System.Windows.Forms.Button butIdioma;
        private System.Windows.Forms.Button butVoltar;
    }
}