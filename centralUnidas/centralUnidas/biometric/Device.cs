﻿// Aplicação: Unidas Totem
// Data     : 01/10/2015
// Versão   : 1.0
// Empresa  : Unidas Rent a Car
// Equipe   : Unidas Desenvolvimento
// Histórico: 
//      01/10/2015: Inicio de desenvolvimento para acesso geral
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace centralUnidas
{
    struct _FTRSCAN_FAKE_REPLICA_PARAMETERS
    {
        bool bCalculated;
        int nCalculatedSum1;
        int nCalculatedSumFuzzy;
        int nCalculatedSumEmpty;
        int nCalculatedSum2;
        double dblCalculatedTremor;
        double dblCalculatedValue;
    }

    struct _FTRSCAN_FRAME_PARAMETERS
    {
        int nContrastOnDose2;
        int nContrastOnDose4;
        int nDose;
        int nBrightnessOnDose1;
        int nBrightnessOnDose2;
        int nBrightnessOnDose3;
        int nBrightnessOnDose4;
        _FTRSCAN_FAKE_REPLICA_PARAMETERS FakeReplicaParams;
        _FTRSCAN_FAKE_REPLICA_PARAMETERS Reserved;
    }

    struct _FTRSCAN_IMAGE_SIZE
    {
        public int nWidth;
        public int nHeight;
        public int nImageSize;
    }

    struct _LPFTRIMGPARMS
    {
        public ulong nWidth;
        public ulong nHeight;
        public ulong nDPI;
        public ulong nRAW_size;
        public ulong nBMP_size;
        public ulong nWSQ_size;
        public float nBitrate;
    }

    public class Device : IScanner, IDisposable
    {
        [DllImport("C:\\UnidasTotem\\Library\\ftrScanAPI.dll")]
        static extern bool ftrScanIsFingerPresent(IntPtr ftrHandle, out _FTRSCAN_FRAME_PARAMETERS pFrameParameters);
        [DllImport("C:\\UnidasTotem\\Library\\ftrScanAPI.dll")]
        static extern IntPtr ftrScanOpenDevice();
        [DllImport("C:\\UnidasTotem\\Library\\ftrScanAPI.dll")]
        static extern void ftrScanCloseDevice(IntPtr ftrHandle);
        [DllImport("C:\\UnidasTotem\\Library\\ftrScanAPI.dll")]
        static extern bool ftrScanSetDiodesStatus(IntPtr ftrHandle, byte byGreenDiodeStatus, byte byRedDiodeStatus);
        [DllImport("C:\\UnidasTotem\\Library\\ftrScanAPI.dll")]
        static extern bool ftrScanGetDiodesStatus(IntPtr ftrHandle, out bool pbIsGreenDiodeOn, out bool pbIsRedDiodeOn);
        [DllImport("C:\\UnidasTotem\\Library\\ftrScanAPI.dll")]
        static extern bool ftrScanGetImageSize(IntPtr ftrHandle, out _FTRSCAN_IMAGE_SIZE pImageSize);
        [DllImport("C:\\UnidasTotem\\Library\\ftrScanAPI.dll")]
        static extern bool ftrScanGetImage(IntPtr ftrHandle, int nDose, byte[] pBuffer);

        [DllImport("C:\\UnidasTotem\\Library\\ftrScanAPI.dll")]
        static extern bool ftrScanGetImage2(IntPtr ftrHandle, int nDose, byte[] pBuffer);

        [DllImport("C:\\UnidasTotem\\Library\\ftrScanAPI.dll")]
        static extern bool ftrScanGetDarkImage(IntPtr ftrHandle, byte[] pBuffer);

        [DllImport("C:\\UnidasTotem\\Library\\ftrWSQ.dll")]
        static extern bool ftrWSQ_FromRAWImage(IntPtr ftrHandle, byte[] pBuffer, _LPFTRIMGPARMS pParam, out byte[] pBufferWSQ);
        
        IntPtr device;

        _FTRSCAN_IMAGE_SIZE ImageSize;

        public Bitmap imagemCapture;
        public int imagemHash;

        AutoResetEvent stopThread = new AutoResetEvent(false), threadStopped = new AutoResetEvent(false);

        Thread loop;

        public Device()
        {
            loop = new Thread(ThreadFunction);
            loop.Name = "Futronic device loop";
            loop.Start();
        }

        public bool Init()
        {
            if (!Connected)
            {
                try
                {
                    device = ftrScanOpenDevice();
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return Connected;
        }

        public bool Connected
        {
            get { return (device != IntPtr.Zero); }
            set { }
        }

        public void Dispose()
        {
            stopThread.Set();
            if (Connected)
            {
                ftrScanCloseDevice(device);
                device = IntPtr.Zero;
            }
            threadStopped.WaitOne();
        }

        public static byte[] ImageToByte2(Image img)
        {
            byte[] byteArray = new byte[0];
            using (MemoryStream stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Bmp);
                stream.Close();

                byteArray = stream.ToArray();
            }
            return byteArray;
        }

        public Bitmap ExportBitMap()
        {
            if (!Connected)
                return null;

            var t = new _FTRSCAN_IMAGE_SIZE();
            ftrScanGetImageSize(device, out t);
            byte[] arr = new byte[t.nImageSize];
            ftrScanGetImage2(device, 6, arr);

            var b = new Bitmap(t.nWidth, t.nHeight);
            for (int x = 0; x < t.nWidth; x++)
                for (int y = 0; y < t.nHeight; y++)
                {
                    int a = 255 - arr[y * t.nWidth + x]; // fundo branco
                    // int a = arr[y * t.nWidth + x]; // fundo preto
                    b.SetPixel(x, y, Color.FromArgb(a, a, a));
                }
            return b;
        }

        public byte[] ExportWSQ(byte[] img)
        {
            if (!Connected)
                return null;

            var t = new _FTRSCAN_IMAGE_SIZE();
            ftrScanGetImageSize(device, out t);
            byte[] bufferWSQ = new byte[t.nHeight * t.nWidth];
            var pParam = new _LPFTRIMGPARMS();
            pParam.nBitrate = 0.75F;
            pParam.nHeight = (ulong)t.nHeight;
            pParam.nWidth = (ulong)t.nWidth;
            // pParam.nDPI = (ulong)-1;
            pParam.nRAW_size = (ulong)(t.nHeight * t.nWidth);
            pParam.nWSQ_size = (ulong)(t.nHeight * t.nWidth);
                
            bool bRet = ftrWSQ_FromRAWImage(device, img, pParam, out bufferWSQ);
            return bufferWSQ;
        }

        public void GetDiodesStatus(out bool green, out bool red)
        {
            ftrScanGetDiodesStatus(device, out green, out red);
        }

        public void SetDiodesStatus(bool green, bool red)
        {
            ftrScanSetDiodesStatus(device, (byte)(green ? 255 : 0), (byte)(red ? 255 : 0));
        }

        public bool IsFinger()
        {
            var t = new _FTRSCAN_FRAME_PARAMETERS();
            return ftrScanIsFingerPresent(device, out t);
        }

        public event ImageReceivedHandler ImageReceived;

        private void ThreadFunction()
        {
            while (!stopThread.WaitOne(200))
            {
                if (!Connected) Init();
                if (Connected && IsFinger())
                {
                    System.Drawing.ImageConverter ic = new System.Drawing.ImageConverter();
                    byte[] btImage1 = new byte[1];
                    btImage1 = (byte[])ic.ConvertTo(ExportBitMap(), btImage1.GetType());
                    SHA256Managed shaM = new SHA256Managed();
                    byte[] hash1 = shaM.ComputeHash(btImage1);

                    imagemHash = BitConverter.ToInt32(hash1, 0);
                    imagemCapture = ExportBitMap();

                    if (imagemCapture != null)
                    {
                        this.Dispose();
                        break;
                    }
                }
            }
            threadStopped.Set();
        }
    }
}
