﻿namespace centralUnidas
{
    partial class frmScreen07
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmScreen07));
            this.pictShutdown = new System.Windows.Forms.PictureBox();
            this.imgList = new System.Windows.Forms.ImageList(this.components);
            this.lblModelo01 = new System.Windows.Forms.Label();
            this.pictUnidasLogo = new System.Windows.Forms.PictureBox();
            this.tmrReturn = new System.Windows.Forms.Timer(this.components);
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblQuerMelhorarProtecao = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblCod1a = new System.Windows.Forms.Label();
            this.lblValor1a = new System.Windows.Forms.Label();
            this.lblValor1 = new System.Windows.Forms.Label();
            this.lblProtecaoParcial = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblProtegeVeiculoLocado = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblProtegeDanosTerceiros = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblProtegeOcupantesCasoAcidentes = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblValorMinimoFranquia = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lblCod2a = new System.Windows.Forms.Label();
            this.lblValor2a = new System.Windows.Forms.Label();
            this.lblValor2 = new System.Windows.Forms.Label();
            this.lblProtecaoEspecial = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.lblCod4a = new System.Windows.Forms.Label();
            this.lblValor4a = new System.Windows.Forms.Label();
            this.lblValor4 = new System.Windows.Forms.Label();
            this.lblProtecaoTerceiros = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.lblCod5a = new System.Windows.Forms.Label();
            this.lblValor5a = new System.Windows.Forms.Label();
            this.lblValor5 = new System.Windows.Forms.Label();
            this.lblProtecaoOcupantesETerceiros = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.lblCod6a = new System.Windows.Forms.Label();
            this.lblValor6a = new System.Windows.Forms.Label();
            this.lblValor6 = new System.Windows.Forms.Label();
            this.lblProtecaoSuper = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.lblCod7a = new System.Windows.Forms.Label();
            this.lblValor7a = new System.Windows.Forms.Label();
            this.lblValor7 = new System.Windows.Forms.Label();
            this.lblProtecaoSuperZero = new System.Windows.Forms.Label();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.pict11 = new System.Windows.Forms.PictureBox();
            this.pict12 = new System.Windows.Forms.PictureBox();
            this.pict13 = new System.Windows.Forms.PictureBox();
            this.panel12 = new System.Windows.Forms.Panel();
            this.lblFranquia1 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.lblFranquia2 = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.lblFranquia4 = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.lblFranquia5 = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.lblFranquia6 = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.lblFranquia7 = new System.Windows.Forms.Label();
            this.pict23 = new System.Windows.Forms.PictureBox();
            this.pict22 = new System.Windows.Forms.PictureBox();
            this.pict21 = new System.Windows.Forms.PictureBox();
            this.pict43 = new System.Windows.Forms.PictureBox();
            this.pict42 = new System.Windows.Forms.PictureBox();
            this.pict41 = new System.Windows.Forms.PictureBox();
            this.pict53 = new System.Windows.Forms.PictureBox();
            this.pict52 = new System.Windows.Forms.PictureBox();
            this.pict51 = new System.Windows.Forms.PictureBox();
            this.pict63 = new System.Windows.Forms.PictureBox();
            this.pict62 = new System.Windows.Forms.PictureBox();
            this.pict61 = new System.Windows.Forms.PictureBox();
            this.pict73 = new System.Windows.Forms.PictureBox();
            this.pict72 = new System.Windows.Forms.PictureBox();
            this.pict71 = new System.Windows.Forms.PictureBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.lblValorLancamentoPreAutorizacao = new System.Windows.Forms.Label();
            this.lblPre1 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.lblPar2a = new System.Windows.Forms.Label();
            this.lblPre2a = new System.Windows.Forms.Label();
            this.lblPre2 = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.lblPar1a = new System.Windows.Forms.Label();
            this.lblPre1a = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.lblPar4a = new System.Windows.Forms.Label();
            this.lblPre4a = new System.Windows.Forms.Label();
            this.lblPre4 = new System.Windows.Forms.Label();
            this.panel21 = new System.Windows.Forms.Panel();
            this.lblPar5a = new System.Windows.Forms.Label();
            this.lblPre5a = new System.Windows.Forms.Label();
            this.lblPre5 = new System.Windows.Forms.Label();
            this.panel22 = new System.Windows.Forms.Panel();
            this.lblPar6a = new System.Windows.Forms.Label();
            this.lblPre6a = new System.Windows.Forms.Label();
            this.lblPre6 = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.lblPar7a = new System.Windows.Forms.Label();
            this.lblPre7a = new System.Windows.Forms.Label();
            this.lblPre7 = new System.Windows.Forms.Label();
            this.panel24 = new System.Windows.Forms.Panel();
            this.lblPar3a = new System.Windows.Forms.Label();
            this.lblPre3a = new System.Windows.Forms.Label();
            this.lblPre3 = new System.Windows.Forms.Label();
            this.pict33 = new System.Windows.Forms.PictureBox();
            this.pict32 = new System.Windows.Forms.PictureBox();
            this.pict31 = new System.Windows.Forms.PictureBox();
            this.panel25 = new System.Windows.Forms.Panel();
            this.lblFranquia3 = new System.Windows.Forms.Label();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.panel26 = new System.Windows.Forms.Panel();
            this.lblCod3a = new System.Windows.Forms.Label();
            this.lblValor3a = new System.Windows.Forms.Label();
            this.lblValor3 = new System.Windows.Forms.Label();
            this.lblProtecaoOcupantes = new System.Windows.Forms.Label();
            this.butVoltar = new System.Windows.Forms.Button();
            this.butConfirmar = new System.Windows.Forms.Button();
            this.butIdioma = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictShutdown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pict11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict13)).BeginInit();
            this.panel12.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pict23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict71)).BeginInit();
            this.panel7.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pict33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict31)).BeginInit();
            this.panel25.SuspendLayout();
            this.panel26.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictShutdown
            // 
            this.pictShutdown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictShutdown.BackColor = System.Drawing.Color.Transparent;
            this.pictShutdown.Image = global::centralUnidas.Properties.Resources.shutdown_button;
            this.pictShutdown.Location = new System.Drawing.Point(7, 165);
            this.pictShutdown.Name = "pictShutdown";
            this.pictShutdown.Size = new System.Drawing.Size(50, 55);
            this.pictShutdown.TabIndex = 112;
            this.pictShutdown.TabStop = false;
            this.pictShutdown.Visible = false;
            this.pictShutdown.Click += new System.EventHandler(this.pictShutdown_Click);
            // 
            // imgList
            // 
            this.imgList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgList.ImageStream")));
            this.imgList.TransparentColor = System.Drawing.Color.Transparent;
            this.imgList.Images.SetKeyName(0, "ar.gif");
            this.imgList.Images.SetKeyName(1, "check1.png");
            this.imgList.Images.SetKeyName(2, "Img_Abs.png");
            this.imgList.Images.SetKeyName(3, "Img_Airbag.png");
            this.imgList.Images.SetKeyName(4, "malas.gif");
            this.imgList.Images.SetKeyName(5, "passageiros.gif");
            this.imgList.Images.SetKeyName(6, "volante.gif");
            this.imgList.Images.SetKeyName(7, "uncheck1.png");
            // 
            // lblModelo01
            // 
            this.lblModelo01.AutoSize = true;
            this.lblModelo01.BackColor = System.Drawing.Color.Transparent;
            this.lblModelo01.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lblModelo01.ForeColor = System.Drawing.Color.SteelBlue;
            this.lblModelo01.Location = new System.Drawing.Point(19, 146);
            this.lblModelo01.Name = "lblModelo01";
            this.lblModelo01.Size = new System.Drawing.Size(0, 37);
            this.lblModelo01.TabIndex = 117;
            // 
            // pictUnidasLogo
            // 
            this.pictUnidasLogo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictUnidasLogo.BackColor = System.Drawing.Color.Transparent;
            this.pictUnidasLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictUnidasLogo.Image = global::centralUnidas.Properties.Resources.totem_header;
            this.pictUnidasLogo.Location = new System.Drawing.Point(0, 0);
            this.pictUnidasLogo.Name = "pictUnidasLogo";
            this.pictUnidasLogo.Size = new System.Drawing.Size(1370, 140);
            this.pictUnidasLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictUnidasLogo.TabIndex = 119;
            this.pictUnidasLogo.TabStop = false;
            // 
            // tmrReturn
            // 
            this.tmrReturn.Interval = 60000;
            this.tmrReturn.Tick += new System.EventHandler(this.tmrReturn_Tick);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox2.Image = global::centralUnidas.Properties.Resources.Totem_Traco;
            this.pictureBox2.Location = new System.Drawing.Point(342, 192);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(683, 1);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 143;
            this.pictureBox2.TabStop = false;
            // 
            // lblQuerMelhorarProtecao
            // 
            this.lblQuerMelhorarProtecao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQuerMelhorarProtecao.BackColor = System.Drawing.Color.White;
            this.lblQuerMelhorarProtecao.Font = new System.Drawing.Font("Arial", 26F, System.Drawing.FontStyle.Bold);
            this.lblQuerMelhorarProtecao.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblQuerMelhorarProtecao.Location = new System.Drawing.Point(0, 138);
            this.lblQuerMelhorarProtecao.Name = "lblQuerMelhorarProtecao";
            this.lblQuerMelhorarProtecao.Size = new System.Drawing.Size(1370, 54);
            this.lblQuerMelhorarProtecao.TabIndex = 160;
            this.lblQuerMelhorarProtecao.Text = "Quer melhorar sua proteção ?";
            this.lblQuerMelhorarProtecao.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Arial", 15F);
            this.checkBox1.Location = new System.Drawing.Point(70, 294);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 162;
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightGray;
            this.panel1.Controls.Add(this.lblCod1a);
            this.panel1.Controls.Add(this.lblValor1a);
            this.panel1.Controls.Add(this.lblValor1);
            this.panel1.Controls.Add(this.lblProtecaoParcial);
            this.panel1.Location = new System.Drawing.Point(104, 270);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(343, 54);
            this.panel1.TabIndex = 165;
            this.panel1.Click += new System.EventHandler(this.panel1_Click);
            // 
            // lblCod1a
            // 
            this.lblCod1a.AutoSize = true;
            this.lblCod1a.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblCod1a.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblCod1a.Location = new System.Drawing.Point(184, 35);
            this.lblCod1a.Name = "lblCod1a";
            this.lblCod1a.Size = new System.Drawing.Size(16, 16);
            this.lblCod1a.TabIndex = 5;
            this.lblCod1a.Text = "0";
            this.lblCod1a.Visible = false;
            // 
            // lblValor1a
            // 
            this.lblValor1a.AutoSize = true;
            this.lblValor1a.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblValor1a.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblValor1a.Location = new System.Drawing.Point(233, 35);
            this.lblValor1a.Name = "lblValor1a";
            this.lblValor1a.Size = new System.Drawing.Size(16, 16);
            this.lblValor1a.TabIndex = 4;
            this.lblValor1a.Text = "0";
            this.lblValor1a.Visible = false;
            // 
            // lblValor1
            // 
            this.lblValor1.AutoSize = true;
            this.lblValor1.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblValor1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblValor1.Location = new System.Drawing.Point(233, 19);
            this.lblValor1.Name = "lblValor1";
            this.lblValor1.Size = new System.Drawing.Size(33, 16);
            this.lblValor1.TabIndex = 3;
            this.lblValor1.Text = "| R$";
            // 
            // lblProtecaoParcial
            // 
            this.lblProtecaoParcial.AutoSize = true;
            this.lblProtecaoParcial.Enabled = false;
            this.lblProtecaoParcial.Font = new System.Drawing.Font("Arial", 15.75F);
            this.lblProtecaoParcial.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblProtecaoParcial.Location = new System.Drawing.Point(11, 14);
            this.lblProtecaoParcial.Name = "lblProtecaoParcial";
            this.lblProtecaoParcial.Size = new System.Drawing.Size(167, 24);
            this.lblProtecaoParcial.TabIndex = 0;
            this.lblProtecaoParcial.Text = "Proteção Parcial";
            this.lblProtecaoParcial.Click += new System.EventHandler(this.label1_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel2.Controls.Add(this.lblProtegeVeiculoLocado);
            this.panel2.Location = new System.Drawing.Point(448, 198);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(144, 70);
            this.panel2.TabIndex = 166;
            // 
            // lblProtegeVeiculoLocado
            // 
            this.lblProtegeVeiculoLocado.AutoSize = true;
            this.lblProtegeVeiculoLocado.Font = new System.Drawing.Font("Arial", 12F);
            this.lblProtegeVeiculoLocado.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblProtegeVeiculoLocado.Location = new System.Drawing.Point(39, 5);
            this.lblProtegeVeiculoLocado.Name = "lblProtegeVeiculoLocado";
            this.lblProtegeVeiculoLocado.Size = new System.Drawing.Size(64, 54);
            this.lblProtegeVeiculoLocado.TabIndex = 0;
            this.lblProtegeVeiculoLocado.Text = "Protege\r\nveículo\r\nlocado?";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel3.Controls.Add(this.lblProtegeDanosTerceiros);
            this.panel3.Location = new System.Drawing.Point(595, 198);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(144, 70);
            this.panel3.TabIndex = 167;
            // 
            // lblProtegeDanosTerceiros
            // 
            this.lblProtegeDanosTerceiros.AutoSize = true;
            this.lblProtegeDanosTerceiros.Font = new System.Drawing.Font("Arial", 12F);
            this.lblProtegeDanosTerceiros.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblProtegeDanosTerceiros.Location = new System.Drawing.Point(40, 5);
            this.lblProtegeDanosTerceiros.Name = "lblProtegeDanosTerceiros";
            this.lblProtegeDanosTerceiros.Size = new System.Drawing.Size(78, 54);
            this.lblProtegeDanosTerceiros.TabIndex = 0;
            this.lblProtegeDanosTerceiros.Text = "Protege\r\ndanos a\r\nterceiros?";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel4.Controls.Add(this.lblProtegeOcupantesCasoAcidentes);
            this.panel4.Location = new System.Drawing.Point(742, 198);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(150, 70);
            this.panel4.TabIndex = 168;
            // 
            // lblProtegeOcupantesCasoAcidentes
            // 
            this.lblProtegeOcupantesCasoAcidentes.AutoSize = true;
            this.lblProtegeOcupantesCasoAcidentes.Font = new System.Drawing.Font("Arial", 12F);
            this.lblProtegeOcupantesCasoAcidentes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblProtegeOcupantesCasoAcidentes.Location = new System.Drawing.Point(4, 4);
            this.lblProtegeOcupantesCasoAcidentes.Name = "lblProtegeOcupantesCasoAcidentes";
            this.lblProtegeOcupantesCasoAcidentes.Size = new System.Drawing.Size(145, 54);
            this.lblProtegeOcupantesCasoAcidentes.TabIndex = 0;
            this.lblProtegeOcupantesCasoAcidentes.Text = "Protege ocupantes\r\ndo veículo em caso \r\nde acidentes?";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel5.Controls.Add(this.lblValorMinimoFranquia);
            this.panel5.Location = new System.Drawing.Point(896, 198);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(144, 70);
            this.panel5.TabIndex = 168;
            // 
            // lblValorMinimoFranquia
            // 
            this.lblValorMinimoFranquia.AutoSize = true;
            this.lblValorMinimoFranquia.Font = new System.Drawing.Font("Arial", 12F);
            this.lblValorMinimoFranquia.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblValorMinimoFranquia.Location = new System.Drawing.Point(35, 5);
            this.lblValorMinimoFranquia.Name = "lblValorMinimoFranquia";
            this.lblValorMinimoFranquia.Size = new System.Drawing.Size(74, 54);
            this.lblValorMinimoFranquia.TabIndex = 0;
            this.lblValorMinimoFranquia.Text = "Valor da\r\nFranquia \r\nMínima";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.LightGray;
            this.panel6.Controls.Add(this.lblCod2a);
            this.panel6.Controls.Add(this.lblValor2a);
            this.panel6.Controls.Add(this.lblValor2);
            this.panel6.Controls.Add(this.lblProtecaoEspecial);
            this.panel6.Location = new System.Drawing.Point(104, 328);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(343, 54);
            this.panel6.TabIndex = 169;
            this.panel6.Click += new System.EventHandler(this.panel6_Click);
            // 
            // lblCod2a
            // 
            this.lblCod2a.AutoSize = true;
            this.lblCod2a.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblCod2a.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblCod2a.Location = new System.Drawing.Point(184, 36);
            this.lblCod2a.Name = "lblCod2a";
            this.lblCod2a.Size = new System.Drawing.Size(16, 16);
            this.lblCod2a.TabIndex = 6;
            this.lblCod2a.Text = "0";
            this.lblCod2a.Visible = false;
            // 
            // lblValor2a
            // 
            this.lblValor2a.AutoSize = true;
            this.lblValor2a.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblValor2a.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblValor2a.Location = new System.Drawing.Point(233, 36);
            this.lblValor2a.Name = "lblValor2a";
            this.lblValor2a.Size = new System.Drawing.Size(16, 16);
            this.lblValor2a.TabIndex = 5;
            this.lblValor2a.Text = "0";
            this.lblValor2a.Visible = false;
            // 
            // lblValor2
            // 
            this.lblValor2.AutoSize = true;
            this.lblValor2.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblValor2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblValor2.Location = new System.Drawing.Point(233, 20);
            this.lblValor2.Name = "lblValor2";
            this.lblValor2.Size = new System.Drawing.Size(33, 16);
            this.lblValor2.TabIndex = 4;
            this.lblValor2.Text = "| R$";
            // 
            // lblProtecaoEspecial
            // 
            this.lblProtecaoEspecial.AutoSize = true;
            this.lblProtecaoEspecial.Enabled = false;
            this.lblProtecaoEspecial.Font = new System.Drawing.Font("Arial", 15.75F);
            this.lblProtecaoEspecial.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblProtecaoEspecial.Location = new System.Drawing.Point(11, 14);
            this.lblProtecaoEspecial.Name = "lblProtecaoEspecial";
            this.lblProtecaoEspecial.Size = new System.Drawing.Size(182, 24);
            this.lblProtecaoEspecial.TabIndex = 0;
            this.lblProtecaoEspecial.Text = "Proteção Especial";
            this.lblProtecaoEspecial.Click += new System.EventHandler(this.label15_Click);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.LightGray;
            this.panel8.Controls.Add(this.lblCod4a);
            this.panel8.Controls.Add(this.lblValor4a);
            this.panel8.Controls.Add(this.lblValor4);
            this.panel8.Controls.Add(this.lblProtecaoTerceiros);
            this.panel8.Location = new System.Drawing.Point(104, 444);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(343, 54);
            this.panel8.TabIndex = 166;
            this.panel8.Click += new System.EventHandler(this.panel8_Click);
            this.panel8.Paint += new System.Windows.Forms.PaintEventHandler(this.panel8_Paint);
            // 
            // lblCod4a
            // 
            this.lblCod4a.AutoSize = true;
            this.lblCod4a.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblCod4a.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblCod4a.Location = new System.Drawing.Point(184, 36);
            this.lblCod4a.Name = "lblCod4a";
            this.lblCod4a.Size = new System.Drawing.Size(16, 16);
            this.lblCod4a.TabIndex = 9;
            this.lblCod4a.Text = "0";
            this.lblCod4a.Visible = false;
            // 
            // lblValor4a
            // 
            this.lblValor4a.AutoSize = true;
            this.lblValor4a.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblValor4a.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblValor4a.Location = new System.Drawing.Point(233, 36);
            this.lblValor4a.Name = "lblValor4a";
            this.lblValor4a.Size = new System.Drawing.Size(16, 16);
            this.lblValor4a.TabIndex = 8;
            this.lblValor4a.Text = "0";
            this.lblValor4a.Visible = false;
            // 
            // lblValor4
            // 
            this.lblValor4.AutoSize = true;
            this.lblValor4.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblValor4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblValor4.Location = new System.Drawing.Point(233, 20);
            this.lblValor4.Name = "lblValor4";
            this.lblValor4.Size = new System.Drawing.Size(33, 16);
            this.lblValor4.TabIndex = 7;
            this.lblValor4.Text = "| R$";
            // 
            // lblProtecaoTerceiros
            // 
            this.lblProtecaoTerceiros.AutoSize = true;
            this.lblProtecaoTerceiros.Enabled = false;
            this.lblProtecaoTerceiros.Font = new System.Drawing.Font("Arial", 15.75F);
            this.lblProtecaoTerceiros.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblProtecaoTerceiros.Location = new System.Drawing.Point(11, 14);
            this.lblProtecaoTerceiros.Name = "lblProtecaoTerceiros";
            this.lblProtecaoTerceiros.Size = new System.Drawing.Size(203, 24);
            this.lblProtecaoTerceiros.TabIndex = 0;
            this.lblProtecaoTerceiros.Text = "Proteção a terceiros";
            this.lblProtecaoTerceiros.Click += new System.EventHandler(this.label17_Click);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.LightGray;
            this.panel9.Controls.Add(this.lblCod5a);
            this.panel9.Controls.Add(this.lblValor5a);
            this.panel9.Controls.Add(this.lblValor5);
            this.panel9.Controls.Add(this.lblProtecaoOcupantesETerceiros);
            this.panel9.Location = new System.Drawing.Point(104, 502);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(343, 54);
            this.panel9.TabIndex = 166;
            this.panel9.Click += new System.EventHandler(this.panel9_Click);
            this.panel9.Paint += new System.Windows.Forms.PaintEventHandler(this.panel9_Paint);
            // 
            // lblCod5a
            // 
            this.lblCod5a.AutoSize = true;
            this.lblCod5a.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblCod5a.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblCod5a.Location = new System.Drawing.Point(184, 36);
            this.lblCod5a.Name = "lblCod5a";
            this.lblCod5a.Size = new System.Drawing.Size(16, 16);
            this.lblCod5a.TabIndex = 10;
            this.lblCod5a.Text = "0";
            this.lblCod5a.Visible = false;
            // 
            // lblValor5a
            // 
            this.lblValor5a.AutoSize = true;
            this.lblValor5a.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblValor5a.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblValor5a.Location = new System.Drawing.Point(233, 36);
            this.lblValor5a.Name = "lblValor5a";
            this.lblValor5a.Size = new System.Drawing.Size(16, 16);
            this.lblValor5a.TabIndex = 8;
            this.lblValor5a.Text = "0";
            this.lblValor5a.Visible = false;
            // 
            // lblValor5
            // 
            this.lblValor5.AutoSize = true;
            this.lblValor5.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblValor5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblValor5.Location = new System.Drawing.Point(233, 20);
            this.lblValor5.Name = "lblValor5";
            this.lblValor5.Size = new System.Drawing.Size(33, 16);
            this.lblValor5.TabIndex = 7;
            this.lblValor5.Text = "| R$";
            // 
            // lblProtecaoOcupantesETerceiros
            // 
            this.lblProtecaoOcupantesETerceiros.AutoSize = true;
            this.lblProtecaoOcupantesETerceiros.Enabled = false;
            this.lblProtecaoOcupantesETerceiros.Font = new System.Drawing.Font("Arial", 15.75F);
            this.lblProtecaoOcupantesETerceiros.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblProtecaoOcupantesETerceiros.Location = new System.Drawing.Point(11, 14);
            this.lblProtecaoOcupantesETerceiros.Name = "lblProtecaoOcupantesETerceiros";
            this.lblProtecaoOcupantesETerceiros.Size = new System.Drawing.Size(218, 24);
            this.lblProtecaoOcupantesETerceiros.TabIndex = 0;
            this.lblProtecaoOcupantesETerceiros.Text = "Ocupantes e terceiros";
            this.lblProtecaoOcupantesETerceiros.Click += new System.EventHandler(this.label18_Click);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.LightGray;
            this.panel10.Controls.Add(this.lblCod6a);
            this.panel10.Controls.Add(this.lblValor6a);
            this.panel10.Controls.Add(this.lblValor6);
            this.panel10.Controls.Add(this.lblProtecaoSuper);
            this.panel10.Location = new System.Drawing.Point(104, 560);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(343, 54);
            this.panel10.TabIndex = 166;
            this.panel10.Click += new System.EventHandler(this.panel10_Click);
            // 
            // lblCod6a
            // 
            this.lblCod6a.AutoSize = true;
            this.lblCod6a.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblCod6a.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblCod6a.Location = new System.Drawing.Point(184, 33);
            this.lblCod6a.Name = "lblCod6a";
            this.lblCod6a.Size = new System.Drawing.Size(16, 16);
            this.lblCod6a.TabIndex = 11;
            this.lblCod6a.Text = "0";
            this.lblCod6a.Visible = false;
            // 
            // lblValor6a
            // 
            this.lblValor6a.AutoSize = true;
            this.lblValor6a.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblValor6a.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblValor6a.Location = new System.Drawing.Point(233, 33);
            this.lblValor6a.Name = "lblValor6a";
            this.lblValor6a.Size = new System.Drawing.Size(16, 16);
            this.lblValor6a.TabIndex = 9;
            this.lblValor6a.Text = "0";
            this.lblValor6a.Visible = false;
            // 
            // lblValor6
            // 
            this.lblValor6.AutoSize = true;
            this.lblValor6.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblValor6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblValor6.Location = new System.Drawing.Point(233, 20);
            this.lblValor6.Name = "lblValor6";
            this.lblValor6.Size = new System.Drawing.Size(33, 16);
            this.lblValor6.TabIndex = 8;
            this.lblValor6.Text = "| R$";
            // 
            // lblProtecaoSuper
            // 
            this.lblProtecaoSuper.AutoSize = true;
            this.lblProtecaoSuper.Enabled = false;
            this.lblProtecaoSuper.Font = new System.Drawing.Font("Arial", 15.75F);
            this.lblProtecaoSuper.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblProtecaoSuper.Location = new System.Drawing.Point(11, 14);
            this.lblProtecaoSuper.Name = "lblProtecaoSuper";
            this.lblProtecaoSuper.Size = new System.Drawing.Size(157, 24);
            this.lblProtecaoSuper.TabIndex = 0;
            this.lblProtecaoSuper.Text = "Proteção Super";
            this.lblProtecaoSuper.Click += new System.EventHandler(this.label19_Click);
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.LightGray;
            this.panel11.Controls.Add(this.lblCod7a);
            this.panel11.Controls.Add(this.lblValor7a);
            this.panel11.Controls.Add(this.lblValor7);
            this.panel11.Controls.Add(this.lblProtecaoSuperZero);
            this.panel11.Location = new System.Drawing.Point(104, 618);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(343, 54);
            this.panel11.TabIndex = 166;
            this.panel11.Click += new System.EventHandler(this.panel11_Click);
            // 
            // lblCod7a
            // 
            this.lblCod7a.AutoSize = true;
            this.lblCod7a.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblCod7a.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblCod7a.Location = new System.Drawing.Point(184, 36);
            this.lblCod7a.Name = "lblCod7a";
            this.lblCod7a.Size = new System.Drawing.Size(16, 16);
            this.lblCod7a.TabIndex = 12;
            this.lblCod7a.Text = "0";
            this.lblCod7a.Visible = false;
            // 
            // lblValor7a
            // 
            this.lblValor7a.AutoSize = true;
            this.lblValor7a.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblValor7a.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblValor7a.Location = new System.Drawing.Point(233, 36);
            this.lblValor7a.Name = "lblValor7a";
            this.lblValor7a.Size = new System.Drawing.Size(16, 16);
            this.lblValor7a.TabIndex = 9;
            this.lblValor7a.Text = "0";
            this.lblValor7a.Visible = false;
            // 
            // lblValor7
            // 
            this.lblValor7.AutoSize = true;
            this.lblValor7.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblValor7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblValor7.Location = new System.Drawing.Point(233, 20);
            this.lblValor7.Name = "lblValor7";
            this.lblValor7.Size = new System.Drawing.Size(33, 16);
            this.lblValor7.TabIndex = 8;
            this.lblValor7.Text = "| R$";
            // 
            // lblProtecaoSuperZero
            // 
            this.lblProtecaoSuperZero.AutoSize = true;
            this.lblProtecaoSuperZero.Enabled = false;
            this.lblProtecaoSuperZero.Font = new System.Drawing.Font("Arial", 15.75F);
            this.lblProtecaoSuperZero.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblProtecaoSuperZero.Location = new System.Drawing.Point(11, 14);
            this.lblProtecaoSuperZero.Name = "lblProtecaoSuperZero";
            this.lblProtecaoSuperZero.Size = new System.Drawing.Size(207, 24);
            this.lblProtecaoSuperZero.TabIndex = 0;
            this.lblProtecaoSuperZero.Text = "Proteção Super Zero";
            this.lblProtecaoSuperZero.Click += new System.EventHandler(this.label20_Click);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Font = new System.Drawing.Font("Arial", 15F);
            this.checkBox2.Location = new System.Drawing.Point(70, 347);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(15, 14);
            this.checkBox2.TabIndex = 170;
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Font = new System.Drawing.Font("Arial", 15F);
            this.checkBox4.Location = new System.Drawing.Point(70, 463);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(15, 14);
            this.checkBox4.TabIndex = 172;
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.checkBox4_CheckedChanged);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Font = new System.Drawing.Font("Arial", 15F);
            this.checkBox5.Location = new System.Drawing.Point(70, 521);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(15, 14);
            this.checkBox5.TabIndex = 173;
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.checkBox5_CheckedChanged);
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Font = new System.Drawing.Font("Arial", 15F);
            this.checkBox6.Location = new System.Drawing.Point(70, 579);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(15, 14);
            this.checkBox6.TabIndex = 174;
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.CheckedChanged += new System.EventHandler(this.checkBox6_CheckedChanged);
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Font = new System.Drawing.Font("Arial", 15F);
            this.checkBox7.Location = new System.Drawing.Point(70, 637);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(15, 14);
            this.checkBox7.TabIndex = 175;
            this.checkBox7.UseVisualStyleBackColor = true;
            this.checkBox7.CheckedChanged += new System.EventHandler(this.checkBox7_CheckedChanged);
            // 
            // pict11
            // 
            this.pict11.BackgroundImage = global::centralUnidas.Properties.Resources.Totem_NOK;
            this.pict11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pict11.ImageLocation = "";
            this.pict11.Location = new System.Drawing.Point(448, 271);
            this.pict11.Name = "pict11";
            this.pict11.Size = new System.Drawing.Size(144, 54);
            this.pict11.TabIndex = 176;
            this.pict11.TabStop = false;
            // 
            // pict12
            // 
            this.pict12.BackgroundImage = global::centralUnidas.Properties.Resources.Totem_NOK;
            this.pict12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pict12.Location = new System.Drawing.Point(595, 271);
            this.pict12.Name = "pict12";
            this.pict12.Size = new System.Drawing.Size(144, 54);
            this.pict12.TabIndex = 183;
            this.pict12.TabStop = false;
            // 
            // pict13
            // 
            this.pict13.BackgroundImage = global::centralUnidas.Properties.Resources.Totem_NOK;
            this.pict13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pict13.Location = new System.Drawing.Point(742, 271);
            this.pict13.Name = "pict13";
            this.pict13.Size = new System.Drawing.Size(150, 54);
            this.pict13.TabIndex = 190;
            this.pict13.TabStop = false;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.lblFranquia1);
            this.panel12.Location = new System.Drawing.Point(896, 271);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(144, 54);
            this.panel12.TabIndex = 197;
            // 
            // lblFranquia1
            // 
            this.lblFranquia1.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblFranquia1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblFranquia1.Location = new System.Drawing.Point(3, 20);
            this.lblFranquia1.Name = "lblFranquia1";
            this.lblFranquia1.Size = new System.Drawing.Size(130, 16);
            this.lblFranquia1.TabIndex = 2;
            this.lblFranquia1.Text = "R$";
            this.lblFranquia1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.lblFranquia2);
            this.panel13.Location = new System.Drawing.Point(896, 328);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(144, 54);
            this.panel13.TabIndex = 198;
            // 
            // lblFranquia2
            // 
            this.lblFranquia2.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblFranquia2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblFranquia2.Location = new System.Drawing.Point(3, 20);
            this.lblFranquia2.Name = "lblFranquia2";
            this.lblFranquia2.Size = new System.Drawing.Size(130, 16);
            this.lblFranquia2.TabIndex = 3;
            this.lblFranquia2.Text = "R$";
            this.lblFranquia2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.lblFranquia4);
            this.panel15.Location = new System.Drawing.Point(896, 444);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(144, 54);
            this.panel15.TabIndex = 198;
            // 
            // lblFranquia4
            // 
            this.lblFranquia4.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblFranquia4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblFranquia4.Location = new System.Drawing.Point(3, 20);
            this.lblFranquia4.Name = "lblFranquia4";
            this.lblFranquia4.Size = new System.Drawing.Size(130, 16);
            this.lblFranquia4.TabIndex = 5;
            this.lblFranquia4.Text = "R$";
            this.lblFranquia4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.lblFranquia5);
            this.panel16.Location = new System.Drawing.Point(896, 502);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(144, 54);
            this.panel16.TabIndex = 198;
            // 
            // lblFranquia5
            // 
            this.lblFranquia5.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblFranquia5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblFranquia5.Location = new System.Drawing.Point(3, 17);
            this.lblFranquia5.Name = "lblFranquia5";
            this.lblFranquia5.Size = new System.Drawing.Size(130, 16);
            this.lblFranquia5.TabIndex = 6;
            this.lblFranquia5.Text = "R$";
            this.lblFranquia5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.lblFranquia6);
            this.panel17.Location = new System.Drawing.Point(896, 560);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(144, 54);
            this.panel17.TabIndex = 198;
            // 
            // lblFranquia6
            // 
            this.lblFranquia6.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblFranquia6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblFranquia6.Location = new System.Drawing.Point(3, 20);
            this.lblFranquia6.Name = "lblFranquia6";
            this.lblFranquia6.Size = new System.Drawing.Size(130, 16);
            this.lblFranquia6.TabIndex = 7;
            this.lblFranquia6.Text = "R$";
            this.lblFranquia6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.lblFranquia7);
            this.panel18.Location = new System.Drawing.Point(896, 618);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(144, 54);
            this.panel18.TabIndex = 198;
            // 
            // lblFranquia7
            // 
            this.lblFranquia7.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblFranquia7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblFranquia7.Location = new System.Drawing.Point(3, 20);
            this.lblFranquia7.Name = "lblFranquia7";
            this.lblFranquia7.Size = new System.Drawing.Size(130, 16);
            this.lblFranquia7.TabIndex = 8;
            this.lblFranquia7.Text = "R$";
            this.lblFranquia7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pict23
            // 
            this.pict23.BackgroundImage = global::centralUnidas.Properties.Resources.Totem_NOK;
            this.pict23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pict23.Location = new System.Drawing.Point(742, 328);
            this.pict23.Name = "pict23";
            this.pict23.Size = new System.Drawing.Size(149, 54);
            this.pict23.TabIndex = 201;
            this.pict23.TabStop = false;
            // 
            // pict22
            // 
            this.pict22.BackgroundImage = global::centralUnidas.Properties.Resources.Totem_NOK;
            this.pict22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pict22.Location = new System.Drawing.Point(595, 328);
            this.pict22.Name = "pict22";
            this.pict22.Size = new System.Drawing.Size(144, 54);
            this.pict22.TabIndex = 200;
            this.pict22.TabStop = false;
            // 
            // pict21
            // 
            this.pict21.BackgroundImage = global::centralUnidas.Properties.Resources.Totem_NOK;
            this.pict21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pict21.ImageLocation = "";
            this.pict21.Location = new System.Drawing.Point(448, 328);
            this.pict21.Name = "pict21";
            this.pict21.Size = new System.Drawing.Size(144, 54);
            this.pict21.TabIndex = 199;
            this.pict21.TabStop = false;
            // 
            // pict43
            // 
            this.pict43.BackgroundImage = global::centralUnidas.Properties.Resources.Totem_NOK;
            this.pict43.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pict43.Location = new System.Drawing.Point(742, 444);
            this.pict43.Name = "pict43";
            this.pict43.Size = new System.Drawing.Size(150, 54);
            this.pict43.TabIndex = 207;
            this.pict43.TabStop = false;
            // 
            // pict42
            // 
            this.pict42.BackgroundImage = global::centralUnidas.Properties.Resources.Totem_NOK;
            this.pict42.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pict42.Location = new System.Drawing.Point(595, 444);
            this.pict42.Name = "pict42";
            this.pict42.Size = new System.Drawing.Size(144, 54);
            this.pict42.TabIndex = 206;
            this.pict42.TabStop = false;
            // 
            // pict41
            // 
            this.pict41.BackgroundImage = global::centralUnidas.Properties.Resources.Totem_NOK;
            this.pict41.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pict41.ImageLocation = "";
            this.pict41.Location = new System.Drawing.Point(448, 444);
            this.pict41.Name = "pict41";
            this.pict41.Size = new System.Drawing.Size(144, 54);
            this.pict41.TabIndex = 205;
            this.pict41.TabStop = false;
            // 
            // pict53
            // 
            this.pict53.BackgroundImage = global::centralUnidas.Properties.Resources.Totem_NOK;
            this.pict53.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pict53.Location = new System.Drawing.Point(742, 502);
            this.pict53.Name = "pict53";
            this.pict53.Size = new System.Drawing.Size(149, 54);
            this.pict53.TabIndex = 210;
            this.pict53.TabStop = false;
            // 
            // pict52
            // 
            this.pict52.BackgroundImage = global::centralUnidas.Properties.Resources.Totem_NOK;
            this.pict52.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pict52.Location = new System.Drawing.Point(595, 502);
            this.pict52.Name = "pict52";
            this.pict52.Size = new System.Drawing.Size(144, 54);
            this.pict52.TabIndex = 209;
            this.pict52.TabStop = false;
            // 
            // pict51
            // 
            this.pict51.BackgroundImage = global::centralUnidas.Properties.Resources.Totem_NOK;
            this.pict51.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pict51.ImageLocation = "";
            this.pict51.Location = new System.Drawing.Point(448, 502);
            this.pict51.Name = "pict51";
            this.pict51.Size = new System.Drawing.Size(144, 54);
            this.pict51.TabIndex = 208;
            this.pict51.TabStop = false;
            // 
            // pict63
            // 
            this.pict63.BackgroundImage = global::centralUnidas.Properties.Resources.Totem_NOK;
            this.pict63.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pict63.Location = new System.Drawing.Point(742, 560);
            this.pict63.Name = "pict63";
            this.pict63.Size = new System.Drawing.Size(150, 54);
            this.pict63.TabIndex = 213;
            this.pict63.TabStop = false;
            // 
            // pict62
            // 
            this.pict62.BackgroundImage = global::centralUnidas.Properties.Resources.Totem_NOK;
            this.pict62.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pict62.Location = new System.Drawing.Point(595, 560);
            this.pict62.Name = "pict62";
            this.pict62.Size = new System.Drawing.Size(144, 54);
            this.pict62.TabIndex = 212;
            this.pict62.TabStop = false;
            // 
            // pict61
            // 
            this.pict61.BackgroundImage = global::centralUnidas.Properties.Resources.Totem_NOK;
            this.pict61.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pict61.ImageLocation = "";
            this.pict61.Location = new System.Drawing.Point(448, 560);
            this.pict61.Name = "pict61";
            this.pict61.Size = new System.Drawing.Size(144, 54);
            this.pict61.TabIndex = 211;
            this.pict61.TabStop = false;
            // 
            // pict73
            // 
            this.pict73.BackgroundImage = global::centralUnidas.Properties.Resources.Totem_NOK;
            this.pict73.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pict73.Location = new System.Drawing.Point(742, 618);
            this.pict73.Name = "pict73";
            this.pict73.Size = new System.Drawing.Size(149, 54);
            this.pict73.TabIndex = 216;
            this.pict73.TabStop = false;
            // 
            // pict72
            // 
            this.pict72.BackgroundImage = global::centralUnidas.Properties.Resources.Totem_NOK;
            this.pict72.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pict72.Location = new System.Drawing.Point(595, 618);
            this.pict72.Name = "pict72";
            this.pict72.Size = new System.Drawing.Size(144, 54);
            this.pict72.TabIndex = 215;
            this.pict72.TabStop = false;
            // 
            // pict71
            // 
            this.pict71.BackgroundImage = global::centralUnidas.Properties.Resources.Totem_NOK;
            this.pict71.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pict71.ImageLocation = "";
            this.pict71.Location = new System.Drawing.Point(448, 618);
            this.pict71.Name = "pict71";
            this.pict71.Size = new System.Drawing.Size(144, 54);
            this.pict71.TabIndex = 214;
            this.pict71.TabStop = false;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel7.Controls.Add(this.lblValorLancamentoPreAutorizacao);
            this.panel7.Location = new System.Drawing.Point(1046, 198);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(144, 70);
            this.panel7.TabIndex = 218;
            // 
            // lblValorLancamentoPreAutorizacao
            // 
            this.lblValorLancamentoPreAutorizacao.AutoSize = true;
            this.lblValorLancamentoPreAutorizacao.Font = new System.Drawing.Font("Arial", 12F);
            this.lblValorLancamentoPreAutorizacao.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblValorLancamentoPreAutorizacao.Location = new System.Drawing.Point(12, 5);
            this.lblValorLancamentoPreAutorizacao.Name = "lblValorLancamentoPreAutorizacao";
            this.lblValorLancamentoPreAutorizacao.Size = new System.Drawing.Size(119, 54);
            this.lblValorLancamentoPreAutorizacao.TabIndex = 0;
            this.lblValorLancamentoPreAutorizacao.Text = "Valor para\r\nLançamento na \r\npré-autorização";
            // 
            // lblPre1
            // 
            this.lblPre1.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblPre1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblPre1.Location = new System.Drawing.Point(3, 16);
            this.lblPre1.Name = "lblPre1";
            this.lblPre1.Size = new System.Drawing.Size(130, 16);
            this.lblPre1.TabIndex = 2;
            this.lblPre1.Text = "R$";
            this.lblPre1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.lblPar2a);
            this.panel14.Controls.Add(this.lblPre2a);
            this.panel14.Controls.Add(this.lblPre2);
            this.panel14.Location = new System.Drawing.Point(1046, 328);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(144, 54);
            this.panel14.TabIndex = 220;
            // 
            // lblPar2a
            // 
            this.lblPar2a.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblPar2a.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblPar2a.Location = new System.Drawing.Point(4, 32);
            this.lblPar2a.Name = "lblPar2a";
            this.lblPar2a.Size = new System.Drawing.Size(24, 18);
            this.lblPar2a.TabIndex = 5;
            this.lblPar2a.Text = "0";
            this.lblPar2a.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblPar2a.Visible = false;
            // 
            // lblPre2a
            // 
            this.lblPre2a.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblPre2a.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblPre2a.Location = new System.Drawing.Point(109, 36);
            this.lblPre2a.Name = "lblPre2a";
            this.lblPre2a.Size = new System.Drawing.Size(24, 16);
            this.lblPre2a.TabIndex = 4;
            this.lblPre2a.Text = "0";
            this.lblPre2a.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblPre2a.Visible = false;
            // 
            // lblPre2
            // 
            this.lblPre2.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblPre2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblPre2.Location = new System.Drawing.Point(3, 16);
            this.lblPre2.Name = "lblPre2";
            this.lblPre2.Size = new System.Drawing.Size(130, 16);
            this.lblPre2.TabIndex = 3;
            this.lblPre2.Text = "R$";
            this.lblPre2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.lblPar1a);
            this.panel19.Controls.Add(this.lblPre1a);
            this.panel19.Controls.Add(this.lblPre1);
            this.panel19.Location = new System.Drawing.Point(1046, 271);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(144, 54);
            this.panel19.TabIndex = 219;
            // 
            // lblPar1a
            // 
            this.lblPar1a.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblPar1a.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblPar1a.Location = new System.Drawing.Point(4, 32);
            this.lblPar1a.Name = "lblPar1a";
            this.lblPar1a.Size = new System.Drawing.Size(24, 18);
            this.lblPar1a.TabIndex = 4;
            this.lblPar1a.Text = "0";
            this.lblPar1a.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblPar1a.Visible = false;
            // 
            // lblPre1a
            // 
            this.lblPre1a.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblPre1a.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblPre1a.Location = new System.Drawing.Point(109, 35);
            this.lblPre1a.Name = "lblPre1a";
            this.lblPre1a.Size = new System.Drawing.Size(24, 18);
            this.lblPre1a.TabIndex = 3;
            this.lblPre1a.Text = "0";
            this.lblPre1a.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblPre1a.Visible = false;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.lblPar4a);
            this.panel20.Controls.Add(this.lblPre4a);
            this.panel20.Controls.Add(this.lblPre4);
            this.panel20.Location = new System.Drawing.Point(1046, 444);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(144, 54);
            this.panel20.TabIndex = 221;
            // 
            // lblPar4a
            // 
            this.lblPar4a.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblPar4a.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblPar4a.Location = new System.Drawing.Point(4, 34);
            this.lblPar4a.Name = "lblPar4a";
            this.lblPar4a.Size = new System.Drawing.Size(24, 18);
            this.lblPar4a.TabIndex = 7;
            this.lblPar4a.Text = "0";
            this.lblPar4a.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblPar4a.Visible = false;
            // 
            // lblPre4a
            // 
            this.lblPre4a.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblPre4a.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblPre4a.Location = new System.Drawing.Point(109, 36);
            this.lblPre4a.Name = "lblPre4a";
            this.lblPre4a.Size = new System.Drawing.Size(24, 16);
            this.lblPre4a.TabIndex = 6;
            this.lblPre4a.Text = "0";
            this.lblPre4a.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblPre4a.Visible = false;
            // 
            // lblPre4
            // 
            this.lblPre4.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblPre4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblPre4.Location = new System.Drawing.Point(3, 20);
            this.lblPre4.Name = "lblPre4";
            this.lblPre4.Size = new System.Drawing.Size(130, 16);
            this.lblPre4.TabIndex = 5;
            this.lblPre4.Text = "R$";
            this.lblPre4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.lblPar5a);
            this.panel21.Controls.Add(this.lblPre5a);
            this.panel21.Controls.Add(this.lblPre5);
            this.panel21.Location = new System.Drawing.Point(1046, 502);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(144, 54);
            this.panel21.TabIndex = 222;
            // 
            // lblPar5a
            // 
            this.lblPar5a.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblPar5a.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblPar5a.Location = new System.Drawing.Point(4, 33);
            this.lblPar5a.Name = "lblPar5a";
            this.lblPar5a.Size = new System.Drawing.Size(24, 18);
            this.lblPar5a.TabIndex = 8;
            this.lblPar5a.Text = "0";
            this.lblPar5a.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblPar5a.Visible = false;
            // 
            // lblPre5a
            // 
            this.lblPre5a.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblPre5a.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblPre5a.Location = new System.Drawing.Point(109, 33);
            this.lblPre5a.Name = "lblPre5a";
            this.lblPre5a.Size = new System.Drawing.Size(24, 19);
            this.lblPre5a.TabIndex = 7;
            this.lblPre5a.Text = "0";
            this.lblPre5a.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblPre5a.Visible = false;
            // 
            // lblPre5
            // 
            this.lblPre5.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblPre5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblPre5.Location = new System.Drawing.Point(3, 17);
            this.lblPre5.Name = "lblPre5";
            this.lblPre5.Size = new System.Drawing.Size(130, 16);
            this.lblPre5.TabIndex = 6;
            this.lblPre5.Text = "R$";
            this.lblPre5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.lblPar6a);
            this.panel22.Controls.Add(this.lblPre6a);
            this.panel22.Controls.Add(this.lblPre6);
            this.panel22.Location = new System.Drawing.Point(1046, 560);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(144, 54);
            this.panel22.TabIndex = 223;
            // 
            // lblPar6a
            // 
            this.lblPar6a.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblPar6a.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblPar6a.Location = new System.Drawing.Point(4, 31);
            this.lblPar6a.Name = "lblPar6a";
            this.lblPar6a.Size = new System.Drawing.Size(24, 18);
            this.lblPar6a.TabIndex = 9;
            this.lblPar6a.Text = "0";
            this.lblPar6a.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblPar6a.Visible = false;
            // 
            // lblPre6a
            // 
            this.lblPre6a.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblPre6a.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblPre6a.Location = new System.Drawing.Point(109, 36);
            this.lblPre6a.Name = "lblPre6a";
            this.lblPre6a.Size = new System.Drawing.Size(24, 18);
            this.lblPre6a.TabIndex = 8;
            this.lblPre6a.Text = "0";
            this.lblPre6a.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblPre6a.Visible = false;
            // 
            // lblPre6
            // 
            this.lblPre6.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblPre6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblPre6.Location = new System.Drawing.Point(3, 20);
            this.lblPre6.Name = "lblPre6";
            this.lblPre6.Size = new System.Drawing.Size(130, 16);
            this.lblPre6.TabIndex = 7;
            this.lblPre6.Text = "R$";
            this.lblPre6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.lblPar7a);
            this.panel23.Controls.Add(this.lblPre7a);
            this.panel23.Controls.Add(this.lblPre7);
            this.panel23.Location = new System.Drawing.Point(1046, 618);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(144, 54);
            this.panel23.TabIndex = 224;
            // 
            // lblPar7a
            // 
            this.lblPar7a.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblPar7a.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblPar7a.Location = new System.Drawing.Point(4, 34);
            this.lblPar7a.Name = "lblPar7a";
            this.lblPar7a.Size = new System.Drawing.Size(24, 18);
            this.lblPar7a.TabIndex = 10;
            this.lblPar7a.Text = "0";
            this.lblPar7a.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblPar7a.Visible = false;
            // 
            // lblPre7a
            // 
            this.lblPre7a.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblPre7a.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblPre7a.Location = new System.Drawing.Point(109, 36);
            this.lblPre7a.Name = "lblPre7a";
            this.lblPre7a.Size = new System.Drawing.Size(25, 16);
            this.lblPre7a.TabIndex = 9;
            this.lblPre7a.Text = "0";
            this.lblPre7a.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblPre7a.Visible = false;
            // 
            // lblPre7
            // 
            this.lblPre7.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblPre7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblPre7.Location = new System.Drawing.Point(3, 20);
            this.lblPre7.Name = "lblPre7";
            this.lblPre7.Size = new System.Drawing.Size(130, 16);
            this.lblPre7.TabIndex = 8;
            this.lblPre7.Text = "R$";
            this.lblPre7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.lblPar3a);
            this.panel24.Controls.Add(this.lblPre3a);
            this.panel24.Controls.Add(this.lblPre3);
            this.panel24.Location = new System.Drawing.Point(1047, 386);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(144, 54);
            this.panel24.TabIndex = 231;
            // 
            // lblPar3a
            // 
            this.lblPar3a.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblPar3a.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblPar3a.Location = new System.Drawing.Point(3, 32);
            this.lblPar3a.Name = "lblPar3a";
            this.lblPar3a.Size = new System.Drawing.Size(24, 18);
            this.lblPar3a.TabIndex = 6;
            this.lblPar3a.Text = "0";
            this.lblPar3a.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblPar3a.Visible = false;
            // 
            // lblPre3a
            // 
            this.lblPre3a.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblPre3a.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblPre3a.Location = new System.Drawing.Point(108, 36);
            this.lblPre3a.Name = "lblPre3a";
            this.lblPre3a.Size = new System.Drawing.Size(25, 16);
            this.lblPre3a.TabIndex = 4;
            this.lblPre3a.Text = "0";
            this.lblPre3a.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblPre3a.Visible = false;
            // 
            // lblPre3
            // 
            this.lblPre3.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblPre3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblPre3.Location = new System.Drawing.Point(3, 16);
            this.lblPre3.Name = "lblPre3";
            this.lblPre3.Size = new System.Drawing.Size(130, 16);
            this.lblPre3.TabIndex = 3;
            this.lblPre3.Text = "R$";
            this.lblPre3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pict33
            // 
            this.pict33.BackgroundImage = global::centralUnidas.Properties.Resources.Totem_NOK;
            this.pict33.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pict33.Location = new System.Drawing.Point(743, 386);
            this.pict33.Name = "pict33";
            this.pict33.Size = new System.Drawing.Size(148, 54);
            this.pict33.TabIndex = 230;
            this.pict33.TabStop = false;
            // 
            // pict32
            // 
            this.pict32.BackgroundImage = global::centralUnidas.Properties.Resources.Totem_NOK;
            this.pict32.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pict32.Location = new System.Drawing.Point(596, 386);
            this.pict32.Name = "pict32";
            this.pict32.Size = new System.Drawing.Size(144, 54);
            this.pict32.TabIndex = 229;
            this.pict32.TabStop = false;
            // 
            // pict31
            // 
            this.pict31.BackgroundImage = global::centralUnidas.Properties.Resources.Totem_NOK;
            this.pict31.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pict31.ImageLocation = "";
            this.pict31.Location = new System.Drawing.Point(449, 386);
            this.pict31.Name = "pict31";
            this.pict31.Size = new System.Drawing.Size(144, 54);
            this.pict31.TabIndex = 228;
            this.pict31.TabStop = false;
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.lblFranquia3);
            this.panel25.Location = new System.Drawing.Point(897, 386);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(144, 54);
            this.panel25.TabIndex = 227;
            // 
            // lblFranquia3
            // 
            this.lblFranquia3.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblFranquia3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblFranquia3.Location = new System.Drawing.Point(3, 20);
            this.lblFranquia3.Name = "lblFranquia3";
            this.lblFranquia3.Size = new System.Drawing.Size(130, 16);
            this.lblFranquia3.TabIndex = 3;
            this.lblFranquia3.Text = "R$";
            this.lblFranquia3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Font = new System.Drawing.Font("Arial", 15F);
            this.checkBox3.Location = new System.Drawing.Point(71, 405);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(15, 14);
            this.checkBox3.TabIndex = 226;
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged_1);
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.LightGray;
            this.panel26.Controls.Add(this.lblCod3a);
            this.panel26.Controls.Add(this.lblValor3a);
            this.panel26.Controls.Add(this.lblValor3);
            this.panel26.Controls.Add(this.lblProtecaoOcupantes);
            this.panel26.Location = new System.Drawing.Point(104, 386);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(343, 54);
            this.panel26.TabIndex = 225;
            this.panel26.Click += new System.EventHandler(this.panel26_Click);
            this.panel26.Paint += new System.Windows.Forms.PaintEventHandler(this.panel26_Paint);
            // 
            // lblCod3a
            // 
            this.lblCod3a.AutoSize = true;
            this.lblCod3a.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblCod3a.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblCod3a.Location = new System.Drawing.Point(184, 36);
            this.lblCod3a.Name = "lblCod3a";
            this.lblCod3a.Size = new System.Drawing.Size(16, 16);
            this.lblCod3a.TabIndex = 6;
            this.lblCod3a.Text = "0";
            this.lblCod3a.Visible = false;
            // 
            // lblValor3a
            // 
            this.lblValor3a.AutoSize = true;
            this.lblValor3a.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblValor3a.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblValor3a.Location = new System.Drawing.Point(233, 36);
            this.lblValor3a.Name = "lblValor3a";
            this.lblValor3a.Size = new System.Drawing.Size(16, 16);
            this.lblValor3a.TabIndex = 5;
            this.lblValor3a.Text = "0";
            this.lblValor3a.Visible = false;
            // 
            // lblValor3
            // 
            this.lblValor3.AutoSize = true;
            this.lblValor3.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblValor3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblValor3.Location = new System.Drawing.Point(233, 20);
            this.lblValor3.Name = "lblValor3";
            this.lblValor3.Size = new System.Drawing.Size(33, 16);
            this.lblValor3.TabIndex = 4;
            this.lblValor3.Text = "| R$";
            // 
            // lblProtecaoOcupantes
            // 
            this.lblProtecaoOcupantes.AutoSize = true;
            this.lblProtecaoOcupantes.Enabled = false;
            this.lblProtecaoOcupantes.Font = new System.Drawing.Font("Arial", 15.75F);
            this.lblProtecaoOcupantes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblProtecaoOcupantes.Location = new System.Drawing.Point(11, 14);
            this.lblProtecaoOcupantes.Name = "lblProtecaoOcupantes";
            this.lblProtecaoOcupantes.Size = new System.Drawing.Size(217, 24);
            this.lblProtecaoOcupantes.TabIndex = 0;
            this.lblProtecaoOcupantes.Text = "Proteção a ocupantes";
            this.lblProtecaoOcupantes.Click += new System.EventHandler(this.label29_Click);
            // 
            // butVoltar
            // 
            this.butVoltar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.butVoltar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.butVoltar.Font = new System.Drawing.Font("Arial", 22F, System.Drawing.FontStyle.Bold);
            this.butVoltar.ForeColor = System.Drawing.Color.White;
            this.butVoltar.Location = new System.Drawing.Point(431, 680);
            this.butVoltar.Name = "butVoltar";
            this.butVoltar.Size = new System.Drawing.Size(267, 79);
            this.butVoltar.TabIndex = 232;
            this.butVoltar.Text = "Voltar";
            this.butVoltar.UseVisualStyleBackColor = false;
            this.butVoltar.Click += new System.EventHandler(this.butVoltar_Click);
            // 
            // butConfirmar
            // 
            this.butConfirmar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.butConfirmar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.butConfirmar.Font = new System.Drawing.Font("Arial", 22F, System.Drawing.FontStyle.Bold);
            this.butConfirmar.ForeColor = System.Drawing.Color.White;
            this.butConfirmar.Location = new System.Drawing.Point(765, 680);
            this.butConfirmar.Name = "butConfirmar";
            this.butConfirmar.Size = new System.Drawing.Size(267, 79);
            this.butConfirmar.TabIndex = 233;
            this.butConfirmar.Text = "Confirmar";
            this.butConfirmar.UseVisualStyleBackColor = false;
            this.butConfirmar.Click += new System.EventHandler(this.butConfirmar_Click);
            // 
            // butIdioma
            // 
            this.butIdioma.FlatAppearance.BorderSize = 0;
            this.butIdioma.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.butIdioma.ForeColor = System.Drawing.Color.Gray;
            this.butIdioma.Image = global::centralUnidas.Properties.Resources.Totem_Flag_Brasil_botao;
            this.butIdioma.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butIdioma.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.butIdioma.Location = new System.Drawing.Point(1148, 0);
            this.butIdioma.Name = "butIdioma";
            this.butIdioma.Size = new System.Drawing.Size(148, 61);
            this.butIdioma.TabIndex = 234;
            this.butIdioma.Text = "PORTUGUÊS";
            this.butIdioma.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butIdioma.UseCompatibleTextRendering = true;
            this.butIdioma.UseMnemonic = false;
            this.butIdioma.UseVisualStyleBackColor = true;
            this.butIdioma.Click += new System.EventHandler(this.butIdioma_Click);
            // 
            // frmScreen07
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1366, 768);
            this.ControlBox = false;
            this.Controls.Add(this.butIdioma);
            this.Controls.Add(this.butConfirmar);
            this.Controls.Add(this.butVoltar);
            this.Controls.Add(this.panel24);
            this.Controls.Add(this.pict33);
            this.Controls.Add(this.pict32);
            this.Controls.Add(this.pict31);
            this.Controls.Add(this.panel25);
            this.Controls.Add(this.checkBox3);
            this.Controls.Add(this.panel26);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel14);
            this.Controls.Add(this.panel19);
            this.Controls.Add(this.panel20);
            this.Controls.Add(this.panel21);
            this.Controls.Add(this.panel22);
            this.Controls.Add(this.panel23);
            this.Controls.Add(this.pict73);
            this.Controls.Add(this.pict72);
            this.Controls.Add(this.pict71);
            this.Controls.Add(this.pict63);
            this.Controls.Add(this.pict62);
            this.Controls.Add(this.pict61);
            this.Controls.Add(this.pict53);
            this.Controls.Add(this.pict52);
            this.Controls.Add(this.pict51);
            this.Controls.Add(this.pict43);
            this.Controls.Add(this.pict42);
            this.Controls.Add(this.pict41);
            this.Controls.Add(this.pict23);
            this.Controls.Add(this.pict22);
            this.Controls.Add(this.pict21);
            this.Controls.Add(this.panel18);
            this.Controls.Add(this.panel17);
            this.Controls.Add(this.panel16);
            this.Controls.Add(this.panel15);
            this.Controls.Add(this.panel13);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.pict13);
            this.Controls.Add(this.pict12);
            this.Controls.Add(this.pict11);
            this.Controls.Add(this.checkBox7);
            this.Controls.Add(this.checkBox6);
            this.Controls.Add(this.checkBox5);
            this.Controls.Add(this.checkBox4);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.pictShutdown);
            this.Controls.Add(this.lblQuerMelhorarProtecao);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictUnidasLogo);
            this.Controls.Add(this.lblModelo01);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmScreen07";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CAU - Unidas";
            this.TopMost = false;
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.Load += new System.EventHandler(this.frmScreen07_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictShutdown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pict11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict13)).EndInit();
            this.panel12.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pict23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict71)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel22.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pict33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict31)).EndInit();
            this.panel25.ResumeLayout(false);
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictShutdown;
        private System.Windows.Forms.ImageList imgList;
        private System.Windows.Forms.Label lblModelo01;
        private System.Windows.Forms.PictureBox pictUnidasLogo;
        private System.Windows.Forms.Timer tmrReturn;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblQuerMelhorarProtecao;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblProtecaoParcial;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblProtegeVeiculoLocado;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblProtegeDanosTerceiros;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lblProtegeOcupantesCasoAcidentes;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lblValorMinimoFranquia;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label lblProtecaoEspecial;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label lblProtecaoTerceiros;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label lblProtecaoOcupantesETerceiros;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label lblProtecaoSuper;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label lblProtecaoSuperZero;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.PictureBox pict11;
        private System.Windows.Forms.PictureBox pict12;
        private System.Windows.Forms.PictureBox pict13;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.PictureBox pict23;
        private System.Windows.Forms.PictureBox pict22;
        private System.Windows.Forms.PictureBox pict21;
        private System.Windows.Forms.PictureBox pict43;
        private System.Windows.Forms.PictureBox pict42;
        private System.Windows.Forms.PictureBox pict41;
        private System.Windows.Forms.PictureBox pict53;
        private System.Windows.Forms.PictureBox pict52;
        private System.Windows.Forms.PictureBox pict51;
        private System.Windows.Forms.PictureBox pict63;
        private System.Windows.Forms.PictureBox pict62;
        private System.Windows.Forms.PictureBox pict61;
        private System.Windows.Forms.PictureBox pict73;
        private System.Windows.Forms.PictureBox pict72;
        private System.Windows.Forms.PictureBox pict71;
        private System.Windows.Forms.Label lblFranquia1;
        private System.Windows.Forms.Label lblFranquia2;
        private System.Windows.Forms.Label lblFranquia4;
        private System.Windows.Forms.Label lblFranquia5;
        private System.Windows.Forms.Label lblFranquia6;
        private System.Windows.Forms.Label lblFranquia7;
        private System.Windows.Forms.Label lblValor1;
        private System.Windows.Forms.Label lblValor2;
        private System.Windows.Forms.Label lblValor4;
        private System.Windows.Forms.Label lblValor5;
        private System.Windows.Forms.Label lblValor6;
        private System.Windows.Forms.Label lblValor7;
        private System.Windows.Forms.Label lblValor1a;
        private System.Windows.Forms.Label lblValor2a;
        private System.Windows.Forms.Label lblValor4a;
        private System.Windows.Forms.Label lblValor5a;
        private System.Windows.Forms.Label lblValor6a;
        private System.Windows.Forms.Label lblValor7a;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label lblValorLancamentoPreAutorizacao;
        private System.Windows.Forms.Label lblPre1;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label lblPre2;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Label lblPre4;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Label lblPre5;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Label lblPre6;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Label lblPre7;
        private System.Windows.Forms.Label lblCod1a;
        private System.Windows.Forms.Label lblCod2a;
        private System.Windows.Forms.Label lblCod4a;
        private System.Windows.Forms.Label lblCod5a;
        private System.Windows.Forms.Label lblCod6a;
        private System.Windows.Forms.Label lblCod7a;
        private System.Windows.Forms.Label lblPre2a;
        private System.Windows.Forms.Label lblPre1a;
        private System.Windows.Forms.Label lblPre4a;
        private System.Windows.Forms.Label lblPre5a;
        private System.Windows.Forms.Label lblPre6a;
        private System.Windows.Forms.Label lblPre7a;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Label lblPre3a;
        private System.Windows.Forms.Label lblPre3;
        private System.Windows.Forms.PictureBox pict33;
        private System.Windows.Forms.PictureBox pict32;
        private System.Windows.Forms.PictureBox pict31;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Label lblFranquia3;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Label lblCod3a;
        private System.Windows.Forms.Label lblValor3a;
        private System.Windows.Forms.Label lblValor3;
        private System.Windows.Forms.Label lblProtecaoOcupantes;
        private System.Windows.Forms.Label lblPar2a;
        private System.Windows.Forms.Label lblPar1a;
        private System.Windows.Forms.Label lblPar4a;
        private System.Windows.Forms.Label lblPar5a;
        private System.Windows.Forms.Label lblPar6a;
        private System.Windows.Forms.Label lblPar7a;
        private System.Windows.Forms.Label lblPar3a;
        private System.Windows.Forms.Button butVoltar;
        private System.Windows.Forms.Button butConfirmar;
        private System.Windows.Forms.Button butIdioma;
    }
}