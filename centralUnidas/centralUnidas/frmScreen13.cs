﻿// Aplicação: Unidas Totem
// Data     : 01/10/2015
// Versão   : 1.0
// Empresa  : Unidas Rent a Car
// Equipe   : Unidas Desenvolvimento
// Objetivo : Informar numero do cartao
// Histórico: 
//      01/10/2015: Inicio de desenvolvimento para acesso geral
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;
using centralUnidas.centralUnidasWCF;
using System.Resources;

namespace centralUnidas
{
    public partial class frmScreen13 : Form
    {
        private int gConfirma = 0;
        private int gTeclado = 1;
        private int gPrint = 0;
        public ResXResourceSet ResxSetForm;

        PrintDocument pdoc = null;
        public frmScreen13()
        {
            InitializeComponent();
        }

        private void frmScreen13_Load(object sender, EventArgs e)
        {

            LogTotem.InsertLog(new LogOperacao()
            {
                Application_Name = Globals.glbApplicationName,
                Application_Version = Globals.glbApplicationVersion,
                Local = Globals.glbLojaTotem,
                Operation_Ticks = Globals.glbOperationTicks,
                Operation_Name = "Informa Número do Cartão",
                Form = "FrmScreen13",
                RESNUM = String.IsNullOrEmpty(Globals.glbReservaNumeroPesquisa) ? 0 : Convert.ToInt32(Globals.glbReservaNumeroPesquisa),
                RANUM = String.IsNullOrEmpty(Globals.glbRA_Contrato) ? 0 : Convert.ToInt32(Globals.glbRA_Contrato)

            });
            Globals.glbScreen = 7;
            int nScreenWidth = this.Width;
            int nScreenWidthSplit = nScreenWidth / 2;

            AtualizarInterfaceParaIdiomaCorrente();

            this.Refresh();

            Globals.glbDebugMode = Convert.ToInt16(ConfigurationManager.AppSettings.Get("appDebugMode"));
            Shutdown_Enable();
        }

        private bool verificaCartao(string strNumeroCartao)
        {
            int nTamCartao = strNumeroCartao.Trim().Length;
            if (nTamCartao != 16) return false;

            string[] strCartaoDigito = new string[16];
            bool bCalculo = true;
            int nSoma1 = 0;
            int nSoma2 = 0;
            int nUltimoDigito = 0;
            for (int x = 0; x < 16; x++)
            {
                strCartaoDigito[x] = strNumeroCartao.Substring(x, 1);
                if (bCalculo)
                {
                    bCalculo = false;
                    int nDigito = Convert.ToInt32(strCartaoDigito[x].Trim());
                    int nDigitoConta = 0;
                    if (nDigito * 2 > 9)
                    {
                        nDigitoConta = (nDigito * 2) - 9;
                    }
                    else
                    {
                        nDigitoConta = (nDigito * 2);
                    }
                    nSoma1 = nSoma1 + nDigitoConta;
                }
                else
                {
                    bCalculo = true;
                    int nDigitoConta = Convert.ToInt32(strCartaoDigito[x].Trim());
                    nSoma2 = nSoma2 + nDigitoConta;
                }
                nUltimoDigito = Convert.ToInt32(strCartaoDigito[x].Trim());
            }
            int nResultadoGeral = nSoma1 + nSoma2;
            bool bCartaoValido = false;
            int nMod = 0;
            nMod = nResultadoGeral % 10;
            if (nMod > 0)
                bCartaoValido = false;
            else
                bCartaoValido = true;
            return bCartaoValido;
        }

        private void Shutdown_Enable()
        {
            if (Globals.glbDebugMode == 0)
                pictShutdown.Visible = false;
            else
                pictShutdown.Visible = true;
        }

        private void pictShutdown_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(1);
        }

        private void imprimeCupom()
        {
            tmrReturn.Stop();
            tmrReturn.Enabled = false;

            CentralUnidasClassClient clientWcf = new CentralUnidasClassClient();
            string xmlRetornoWCF = clientWcf.getCupomFiscal(Convert.ToInt32(Globals.glbReservaNumeroPesquisa));
            if (string.IsNullOrEmpty(xmlRetornoWCF) || (xmlRetornoWCF.Trim() == "<UNIDASWCF />"))
            {
                Globals.glbCupomFiscal1 = "";
                Globals.glbCupomFiscal2 = "";
                Globals.glbCC_CodigoPreAutorizacao = "";
                Globals.glbCC_CodigoAutorizacaoEmissor = "";
                Globals.glbBandeiraCartao = "";
            }
            else
            {
                xmlRetornoWCF = xmlRetornoWCF.Replace("\r\n", "");
                DataSet dtSet = new DataSet();
                System.IO.StringReader xmlSR = new System.IO.StringReader(xmlRetornoWCF);
                dtSet.ReadXml(xmlSR);
                foreach (DataRow dbRow in dtSet.Tables["Table"].Rows)
                {

                    Globals.glbCupomFiscal1 = dbRow["CUPOM_CLIENTE"].ToString().Trim();
                    Globals.glbCupomFiscal2 = dbRow["CUPOM_LOJA"].ToString().Trim();
                    Globals.glbCC_CodigoPreAutorizacao = dbRow["CODIGO_PREAUTORIZACAO"].ToString().Trim();
                    Globals.glbCC_CodigoAutorizacaoEmissor = dbRow["CODIGO_EMISSOR"].ToString().Trim();

                    string strCupomString = "";
                    strCupomString = Globals.glbCupomFiscal1.Trim();
                    if (strCupomString.ToUpper().Contains("REDE")) Globals.glbBandeiraCartao = "RED";
                    if (strCupomString.ToUpper().Contains("AMERICAN")) Globals.glbBandeiraCartao = "AMX";
                    if (strCupomString.ToUpper().Contains("CIELO")) Globals.glbBandeiraCartao = "CIE";
                    if (strCupomString.ToUpper().Contains("DINERS")) Globals.glbBandeiraCartao = "DNR";
                    if (strCupomString.ToUpper().Contains("ELO")) Globals.glbBandeiraCartao = "ELO";
                    if (strCupomString.ToUpper().Contains("MASTERCARD")) Globals.glbBandeiraCartao = "MAS";
                    if (strCupomString.ToUpper().Contains("VISA")) Globals.glbBandeiraCartao = "VIS";

                    break;
                }
            }
            int nRetorno = printRA();
        }

        private void pictCartaoConfirmar_Click(object sender, EventArgs e)
        {
            if (gConfirma == 0)
            {
                tmrReturn.Stop();
                tmrReturn.Enabled = false;
                bool bValida = true;

                if (txtCartao1.Text.Trim().Length != 4) bValida = false;
                if (txtCartao2.Text.Trim().Length != 4) bValida = false;
                if (txtCartao3.Text.Trim().Length != 4) bValida = false;
                if (txtCartao4.Text.Trim().Length != 4) bValida = false;

                if (bValida)
                {
                    string strCartaoCompleto = "";
                    strCartaoCompleto = txtCartao1.Text.Trim() + txtCartao2.Text.Trim() + txtCartao3.Text.Trim() + txtCartao4.Text.Trim();
                    bValida = verificaCartao(strCartaoCompleto);
                    if (bValida)
                    {
                        timerDisabled();

                        Globals.glbCC_CartaoInformado = txtCartao1.Text.Trim() + txtCartao2.Text.Trim() + txtCartao3.Text.Trim() + txtCartao4.Text.Trim();
                        frmScreen14 frm14 = new frmScreen14();
                        frm14.Show();
                        this.Close();
                        this.Dispose();
                    }
                    else
                    {
                        //imprimeCupom();
                        timerDisabled();

                        Globals.glbScreen = 17;
                        Globals.glbMensagem = 1017;
                        frmMensagem frmMsg = new frmMensagem();
                        frmMsg.Show();
                        this.Close();
                        this.Dispose();
                    }
                }
                else
                {
                    //imprimeCupom();
                    timerDisabled();

                    Globals.glbScreen = 13;
                    Globals.glbMensagem = 1006;
                    frmMensagem frmMsg = new frmMensagem();
                    frmMsg.Show();
                    this.Close();
                    this.Dispose();
                }
            }
            gConfirma = 1;
        }

        private void pictTec_1_Click(object sender, EventArgs e)
        {
            if (txtCartao1.Text.Length < 4)
            {
                txtCartao1.Focus();
                txtCartao1.Text = txtCartao1.Text + "1";
                txtCartao1.SelectionStart = txtCartao1.Text.Length + 1;
            }
            else
            {
                if (txtCartao2.Text.Length < 4)
                {
                    txtCartao2.Focus();
                    txtCartao2.Text = txtCartao2.Text + "1";
                    txtCartao2.SelectionStart = txtCartao2.Text.Length + 1;
                }
                else
                {
                    if (txtCartao3.Text.Length < 4)
                    {
                        txtCartao3.Focus();
                        txtCartao3.Text = txtCartao3.Text + "1";
                        txtCartao3.SelectionStart = txtCartao3.Text.Length + 1;
                    }
                    else
                    {
                        if (txtCartao4.Text.Length < 4)
                        {
                            txtCartao4.Focus();
                            txtCartao4.Text = txtCartao4.Text + "1";
                            txtCartao4.SelectionStart = txtCartao4.Text.Length + 1;
                        }
                    }
                }
            }
        }

        private void pictTec_2_Click(object sender, EventArgs e)
        {
            if (txtCartao1.Text.Length < 4)
            {
                txtCartao1.Focus();
                txtCartao1.Text = txtCartao1.Text + "2";
                txtCartao1.SelectionStart = txtCartao1.Text.Length + 1;
            }
            else
            {
                if (txtCartao2.Text.Length < 4)
                {
                    txtCartao2.Focus();
                    txtCartao2.Text = txtCartao2.Text + "2";
                    txtCartao2.SelectionStart = txtCartao2.Text.Length + 1;
                }
                else
                {
                    if (txtCartao3.Text.Length < 4)
                    {
                        txtCartao3.Focus();
                        txtCartao3.Text = txtCartao3.Text + "2";
                        txtCartao3.SelectionStart = txtCartao3.Text.Length + 1;
                    }
                    else
                    {
                        if (txtCartao4.Text.Length < 4)
                        {
                            txtCartao4.Focus();
                            txtCartao4.Text = txtCartao4.Text + "2";
                            txtCartao4.SelectionStart = txtCartao4.Text.Length + 1;
                        }
                    }
                }
            }
        }

        private void pictTec_3_Click(object sender, EventArgs e)
        {
            if (txtCartao1.Text.Length < 4)
            {
                txtCartao1.Focus();
                txtCartao1.Text = txtCartao1.Text + "3";
                txtCartao1.SelectionStart = txtCartao1.Text.Length + 1;
            }
            else
            {
                if (txtCartao2.Text.Length < 4)
                {
                    txtCartao2.Focus();
                    txtCartao2.Text = txtCartao2.Text + "3";
                    txtCartao2.SelectionStart = txtCartao2.Text.Length + 1;
                }
                else
                {
                    if (txtCartao3.Text.Length < 4)
                    {
                        txtCartao3.Focus();
                        txtCartao3.Text = txtCartao3.Text + "3";
                        txtCartao3.SelectionStart = txtCartao3.Text.Length + 1;
                    }
                    else
                    {
                        if (txtCartao4.Text.Length < 4)
                        {
                            txtCartao4.Focus();
                            txtCartao4.Text = txtCartao4.Text + "3";
                            txtCartao4.SelectionStart = txtCartao4.Text.Length + 1;
                        }
                    }
                }
            }
        }

        private void pictTec_4_Click(object sender, EventArgs e)
        {
            if (txtCartao1.Text.Length < 4)
            {
                txtCartao1.Focus();
                txtCartao1.Text = txtCartao1.Text + "4";
                txtCartao1.SelectionStart = txtCartao1.Text.Length + 1;
            }
            else
            {
                if (txtCartao2.Text.Length < 4)
                {
                    txtCartao2.Focus();
                    txtCartao2.Text = txtCartao2.Text + "4";
                    txtCartao2.SelectionStart = txtCartao2.Text.Length + 1;
                }
                else
                {
                    if (txtCartao3.Text.Length < 4)
                    {
                        txtCartao3.Focus();
                        txtCartao3.Text = txtCartao3.Text + "4";
                        txtCartao3.SelectionStart = txtCartao3.Text.Length + 1;
                    }
                    else
                    {
                        if (txtCartao4.Text.Length < 4)
                        {
                            txtCartao4.Focus();
                            txtCartao4.Text = txtCartao4.Text + "4";
                            txtCartao4.SelectionStart = txtCartao4.Text.Length + 1;
                        }
                    }
                }
            }
        }

        private void pictTec_5_Click(object sender, EventArgs e)
        {
            if (txtCartao1.Text.Length < 4)
            {
                txtCartao1.Focus();
                txtCartao1.Text = txtCartao1.Text + "5";
                txtCartao1.SelectionStart = txtCartao1.Text.Length + 1;
            }
            else
            {
                if (txtCartao2.Text.Length < 4)
                {
                    txtCartao2.Focus();
                    txtCartao2.Text = txtCartao2.Text + "5";
                    txtCartao2.SelectionStart = txtCartao2.Text.Length + 1;
                }
                else
                {
                    if (txtCartao3.Text.Length < 4)
                    {
                        txtCartao3.Focus();
                        txtCartao3.Text = txtCartao3.Text + "5";
                        txtCartao3.SelectionStart = txtCartao3.Text.Length + 1;
                    }
                    else
                    {
                        if (txtCartao4.Text.Length < 4)
                        {
                            txtCartao4.Focus();
                            txtCartao4.Text = txtCartao4.Text + "5";
                            txtCartao4.SelectionStart = txtCartao4.Text.Length + 1;
                        }
                    }
                }
            }
        }

        private void pictTec_6_Click(object sender, EventArgs e)
        {
            if (txtCartao1.Text.Length < 4)
            {
                txtCartao1.Focus();
                txtCartao1.Text = txtCartao1.Text + "6";
                txtCartao1.SelectionStart = txtCartao1.Text.Length + 1;
            }
            else
            {
                if (txtCartao2.Text.Length < 4)
                {
                    txtCartao2.Focus();
                    txtCartao2.Text = txtCartao2.Text + "6";
                    txtCartao2.SelectionStart = txtCartao2.Text.Length + 1;
                }
                else
                {
                    if (txtCartao3.Text.Length < 4)
                    {
                        txtCartao3.Focus();
                        txtCartao3.Text = txtCartao3.Text + "6";
                        txtCartao3.SelectionStart = txtCartao3.Text.Length + 1;
                    }
                    else
                    {
                        if (txtCartao4.Text.Length < 4)
                        {
                            txtCartao4.Focus();
                            txtCartao4.Text = txtCartao4.Text + "6";
                            txtCartao4.SelectionStart = txtCartao4.Text.Length + 1;
                        }
                    }
                }
            }
        }

        private void pictTec_7_Click(object sender, EventArgs e)
        {
            if (txtCartao1.Text.Length < 4)
            {
                txtCartao1.Focus();
                txtCartao1.Text = txtCartao1.Text + "7";
                txtCartao1.SelectionStart = txtCartao1.Text.Length + 1;
            }
            else
            {
                if (txtCartao2.Text.Length < 4)
                {
                    txtCartao2.Focus();
                    txtCartao2.Text = txtCartao2.Text + "7";
                    txtCartao2.SelectionStart = txtCartao2.Text.Length + 1;
                }
                else
                {
                    if (txtCartao3.Text.Length < 4)
                    {
                        txtCartao3.Focus();
                        txtCartao3.Text = txtCartao3.Text + "7";
                        txtCartao3.SelectionStart = txtCartao3.Text.Length + 1;
                    }
                    else
                    {
                        if (txtCartao4.Text.Length < 4)
                        {
                            txtCartao4.Focus();
                            txtCartao4.Text = txtCartao4.Text + "7";
                            txtCartao4.SelectionStart = txtCartao4.Text.Length + 1;
                        }
                    }
                }
            }
        }

        private void pictTec_8_Click(object sender, EventArgs e)
        {
            if (txtCartao1.Text.Length < 4)
            {
                txtCartao1.Focus();
                txtCartao1.Text = txtCartao1.Text + "8";
                txtCartao1.SelectionStart = txtCartao1.Text.Length + 1;
            }
            else
            {
                if (txtCartao2.Text.Length < 4)
                {
                    txtCartao2.Focus();
                    txtCartao2.Text = txtCartao2.Text + "8";
                    txtCartao2.SelectionStart = txtCartao2.Text.Length + 1;
                }
                else
                {
                    if (txtCartao3.Text.Length < 4)
                    {
                        txtCartao3.Focus();
                        txtCartao3.Text = txtCartao3.Text + "8";
                        txtCartao3.SelectionStart = txtCartao3.Text.Length + 1;
                    }
                    else
                    {
                        if (txtCartao4.Text.Length < 4)
                        {
                            txtCartao4.Focus();
                            txtCartao4.Text = txtCartao4.Text + "8";
                            txtCartao4.SelectionStart = txtCartao4.Text.Length + 1;
                        }
                    }
                }
            }
        }

        private void pictTec_9_Click(object sender, EventArgs e)
        {
            if (txtCartao1.Text.Length < 4)
            {
                txtCartao1.Focus();
                txtCartao1.Text = txtCartao1.Text + "9";
                txtCartao1.SelectionStart = txtCartao1.Text.Length + 1;
            }
            else
            {
                if (txtCartao2.Text.Length < 4)
                {
                    txtCartao2.Focus();
                    txtCartao2.Text = txtCartao2.Text + "9";
                    txtCartao2.SelectionStart = txtCartao2.Text.Length + 1;
                }
                else
                {
                    if (txtCartao3.Text.Length < 4)
                    {
                        txtCartao3.Focus();
                        txtCartao3.Text = txtCartao3.Text + "9";
                        txtCartao3.SelectionStart = txtCartao3.Text.Length + 1;
                    }
                    else
                    {
                        if (txtCartao4.Text.Length < 4)
                        {
                            txtCartao4.Focus();
                            txtCartao4.Text = txtCartao4.Text + "9";
                            txtCartao4.SelectionStart = txtCartao4.Text.Length + 1;
                        }
                    }
                }
            }
        }

        private void pictTec_0_Click(object sender, EventArgs e)
        {
            if (txtCartao1.Text.Length < 4)
            {
                txtCartao1.Focus();
                txtCartao1.Text = txtCartao1.Text + "0";
                txtCartao1.SelectionStart = txtCartao1.Text.Length + 1;
            }
            else
            {
                if (txtCartao2.Text.Length < 4)
                {
                    txtCartao2.Focus();
                    txtCartao2.Text = txtCartao2.Text + "0";
                    txtCartao2.SelectionStart = txtCartao2.Text.Length + 1;
                }
                else
                {
                    if (txtCartao3.Text.Length < 4)
                    {
                        txtCartao3.Focus();
                        txtCartao3.Text = txtCartao3.Text + "0";
                        txtCartao3.SelectionStart = txtCartao3.Text.Length + 1;
                    }
                    else
                    {
                        if (txtCartao4.Text.Length < 4)
                        {
                            txtCartao4.Focus();
                            txtCartao4.Text = txtCartao4.Text + "0";
                            txtCartao4.SelectionStart = txtCartao4.Text.Length + 1;
                        }
                    }
                }
            }
        }

        private void pictTec_BACK2_Click(object sender, EventArgs e)
        {
            int nTamanho1 = txtCartao1.Text.Length;
            int nTamanho2 = txtCartao2.Text.Length;
            int nTamanho3 = txtCartao3.Text.Length;
            int nTamanho4 = txtCartao4.Text.Length;

            if (nTamanho4 > 0)
            {
                txtCartao4.Focus();
                txtCartao4.Text = txtCartao4.Text.Substring(0, nTamanho4 - 1);
                txtCartao4.SelectionStart = txtCartao4.Text.Length + 1;
            }
            else
            {
                if (nTamanho3 > 0)
                {
                    txtCartao3.Focus();
                    txtCartao3.Text = txtCartao3.Text.Substring(0, nTamanho3 - 1);
                    txtCartao3.SelectionStart = txtCartao3.Text.Length + 1;
                }
                else
                {
                    if (nTamanho2 > 0)
                    {
                        txtCartao2.Focus();
                        txtCartao2.Text = txtCartao2.Text.Substring(0, nTamanho2 - 1);
                        txtCartao2.SelectionStart = txtCartao2.Text.Length + 1;
                    }
                    else
                    {
                        if (nTamanho1 > 0)
                        {
                            txtCartao1.Focus();
                            txtCartao1.Text = txtCartao1.Text.Substring(0, nTamanho1 - 1);
                            txtCartao1.SelectionStart = txtCartao1.Text.Length + 1;
                        }
                    }
                }
            }

        }

        private void pictTec_TECLADO_Click(object sender, EventArgs e)
        {
            // tmrTeclado.Enabled = true;
        }

        private void tmrTeclado_Tick(object sender, EventArgs e)
        {
            tmrTeclado.Enabled = false;
            if (gTeclado == 1)
            {
                Teclado_Hide();
                gTeclado = 2;
            }
            else
            {
                Teclado_Show();
                gTeclado = 1;
            }
        }

        private void Teclado_Hide()
        {
            int nTarget = pictTecladoFundo.Top + pictTecladoFundo.Height + 100;
            int x = pictTecladoFundo.Top;
            // Primeira fileira
            int tec1 = pictTec_1.Top - x;
            int tec2 = pictTec_2.Top - x;
            int tec3 = pictTec_3.Top - x;

            // Segunda fileira
            int tec4 = pictTec_4.Top - x;
            int tec5 = pictTec_5.Top - x;
            int tec6 = pictTec_6.Top - x;

            // Terceira fileira
            int tec7 = pictTec_7.Top - x;
            int tec8 = pictTec_8.Top - x;
            int tec9 = pictTec_9.Top - x;

            // Quarta fileira
            int tecTECLADO = pictTec_TECLADO.Top - x;
            int tec0 = pictTec_0.Top - x;
            int tecBACK2 = pictTec_BACK2.Top - x;

            while (x < nTarget)
            {

                pictTecladoFundo.Top = x;

                // PRIMEIRA FILEIRA
                pictTec_1.Top = tec1 + x;
                pictTec_2.Top = tec2 + x;
                pictTec_3.Top = tec3 + x;

                // SEGUNDA FILEIRA
                pictTec_4.Top = tec4 + x;
                pictTec_5.Top = tec5 + x;
                pictTec_6.Top = tec6 + x;

                // TERCEIRA FILEIRA
                pictTec_7.Top = tec7 + x;
                pictTec_8.Top = tec8 + x;
                pictTec_9.Top = tec9 + x;

                // QUARTA FILEIRA
                pictTec_TECLADO.Top = tecTECLADO + x;
                pictTec_0.Top = tec0 + x;
                pictTec_BACK2.Top = tecBACK2 + x;

                this.Refresh();
                x = x + 100;
            }
        }

        private void Teclado_Show()
        {
            int nTarget = this.Height - pictTecladoFundo.Height;
            int x = pictTecladoFundo.Top;
            // Primeira fileira
            int tec1 = pictTec_1.Top - x;
            int tec2 = pictTec_2.Top - x;
            int tec3 = pictTec_3.Top - x;

            // Segunda fileira
            int tec4 = pictTec_4.Top - x;
            int tec5 = pictTec_5.Top - x;
            int tec6 = pictTec_6.Top - x;

            // Terceira fileira
            int tec7 = pictTec_7.Top - x;
            int tec8 = pictTec_8.Top - x;
            int tec9 = pictTec_9.Top - x;

            // Quarta fileira
            int tecTECLADO = pictTec_TECLADO.Top - x;
            int tec0 = pictTec_0.Top - x;
            int tecBACK2 = pictTec_BACK2.Top - x;

            while (x > nTarget)
            {

                pictTecladoFundo.Top = x;

                // PRIMEIRA FILEIRA
                pictTec_1.Top = tec1 + x;
                pictTec_2.Top = tec2 + x;
                pictTec_3.Top = tec3 + x;

                // SEGUNDA FILEIRA
                pictTec_4.Top = tec4 + x;
                pictTec_5.Top = tec5 + x;
                pictTec_6.Top = tec6 + x;

                // TERCEIRA FILEIRA
                pictTec_7.Top = tec7 + x;
                pictTec_8.Top = tec8 + x;
                pictTec_9.Top = tec9 + x;

                // QUARTA FILEIRA
                pictTec_TECLADO.Top = tecTECLADO + x;
                pictTec_0.Top = tec0 + x;
                pictTec_BACK2.Top = tecBACK2 + x;

                this.Refresh();
                x = x - 100;
            }
        }

        private void tmrReturn_Tick(object sender, EventArgs e)
        {
            tmrReturn.Enabled = false;
            timerDisabled();

            frmScreen01 frm1 = new frmScreen01();
            frm1.Show();
            this.Close();
            this.Dispose();
        }

        private void pictLang_Click(object sender, EventArgs e)
        {
            frmIdioma frmIdi = new frmIdioma();
            frmIdi.ShowDialog();

            if (Globals.glbLanguage == 1) pictLang.Image = centralUnidas.Properties.Resources.totem_langPT;
            if (Globals.glbLanguage == 2) pictLang.Image = centralUnidas.Properties.Resources.totem_langPT;
            if (Globals.glbLanguage == 3) pictLang.Image = centralUnidas.Properties.Resources.totem_langPT;
        }

        private void txtCartao1_TextChanged(object sender, EventArgs e)
        {
            tmrReturn.Stop();
            tmrReturn.Start();
        }

        private void txtCartao2_TextChanged(object sender, EventArgs e)
        {
            tmrReturn.Stop();
            tmrReturn.Start();
        }

        private void txtCartao3_TextChanged(object sender, EventArgs e)
        {
            tmrReturn.Stop();
            tmrReturn.Start();
        }

        private void txtCartao4_TextChanged(object sender, EventArgs e)
        {
            tmrReturn.Stop();
            tmrReturn.Start();
        }

        private void pictVoltar_Click(object sender, EventArgs e)
        {
            imprimeCupom();

            if (gPrint > 0)
            {
                timerDisabled();

                Globals.glbScreen = 15;
                Globals.glbMensagem = 1015;
                frmMensagem frmMsg = new frmMensagem();
                frmMsg.Show();
                this.Close();
                this.Dispose();
            }
            else
            {
                timerDisabled();

                frmScreen01 frm02 = new frmScreen01();
                frm02.Show();
                this.Close();
                this.Dispose();
            }
        }

        private int printRA()
        {
            try
            {
                pdoc = new PrintDocument();
                PrinterSettings ps = new PrinterSettings();

                pdoc.PrintPage += new PrintPageEventHandler(pdoc_PrintPage);
                PaperSize pkSize = new PaperSize("RA", 300, 1100);
                Margins pkMargin = new Margins(10, 10, 10, 10);

                pdoc.OriginAtMargins = true;
                pdoc.DefaultPageSettings.Margins.Left = 20;
                pdoc.DefaultPageSettings.Margins.Right = 20;
                pdoc.DefaultPageSettings.Margins.Top = 20;
                pdoc.DefaultPageSettings.Margins.Bottom = 20;
                pdoc.DefaultPageSettings.PaperSize = pkSize;
                pdoc.PrinterSettings.DefaultPageSettings.PaperSize = new PaperSize("RA", 300, 1100);
                pdoc.PrinterSettings.DefaultPageSettings.Margins.Left = 20;
                pdoc.PrinterSettings.DefaultPageSettings.Margins.Right = 20;
                pdoc.PrinterSettings.DefaultPageSettings.Margins.Top = 20;
                pdoc.PrinterSettings.DefaultPageSettings.Margins.Bottom = 20;
                pdoc.PrinterSettings.DefaultPageSettings.PrinterSettings.DefaultPageSettings.PaperSize = new PaperSize("RA", 300, 1100);
                pdoc.DocumentName = "RA";
                pdoc.Print();
            }
            catch (Exception ex)
            {

            }
            return gPrint;
        }

        void pdoc_PrintPage(object sender, PrintPageEventArgs e)
        {
            try
            {
                Graphics graphics = e.Graphics;
                Font font = new Font("Courier New", 9, FontStyle.Bold);
                float fontHeight = font.GetHeight();

                int startX = 3;
                int startY = 5;
                int Offset = 40;

                if (!string.IsNullOrEmpty(Globals.glbCupomFiscal1))
                {
                    gPrint = 1;

                    graphics.DrawString("--------------------------------------------", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    Offset = Offset + 17;
                    graphics.DrawString("               CUPOM FISCAL                  ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    Offset = Offset + 17;
                    Offset = Offset + 17;
                    graphics.DrawString(Globals.glbCupomFiscal1, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    for (int x = 0; x < 17; x++)
                    {
                        Offset = Offset + 17;
                    }
                }

                if (!string.IsNullOrEmpty(Globals.glbCupomFiscal2))
                {

                    gPrint = 1;

                    graphics.DrawString("--------------------------------------------", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    Offset = Offset + 17;
                    graphics.DrawString("               CUPOM FISCAL                  ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    Offset = Offset + 17;
                    Offset = Offset + 17;
                    graphics.DrawString(Globals.glbCupomFiscal2, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    for (int x = 0; x < 17; x++)
                    {
                        Offset = Offset + 17;
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void butVoltar_Click(object sender, EventArgs e)
        {
            imprimeCupom();

            if (gPrint > 0)
            {
                timerDisabled();

                Globals.glbScreen = 15;
                Globals.glbMensagem = 1015;
                frmMensagem frmMsg = new frmMensagem();
                frmMsg.Show();
                this.Close();
                this.Dispose();
            }
            else
            {
                timerDisabled();

                frmScreen01 frm02 = new frmScreen01();
                frm02.Show();
                this.Close();
                this.Dispose();
            }
        }

        private void butContinuar_Click(object sender, EventArgs e)
        {
            if (gConfirma == 0)
            {
                tmrReturn.Stop();
                tmrReturn.Enabled = false;
                bool bValida = true;

                if (txtCartao1.Text.Trim().Length != 4) bValida = false;
                if (txtCartao2.Text.Trim().Length != 4) bValida = false;
                if (txtCartao3.Text.Trim().Length != 4) bValida = false;
                if (txtCartao4.Text.Trim().Length != 4) bValida = false;

                if (bValida)
                {
                    string strCartaoCompleto = "";
                    strCartaoCompleto = txtCartao1.Text.Trim() + txtCartao2.Text.Trim() + txtCartao3.Text.Trim() + txtCartao4.Text.Trim();
                    bValida = verificaCartao(strCartaoCompleto);
                    if (bValida)
                    {
                        timerDisabled();

                        Globals.glbCC_CartaoInformado = txtCartao1.Text.Trim() + txtCartao2.Text.Trim() + txtCartao3.Text.Trim() + txtCartao4.Text.Trim();
                        frmScreen14 frm14 = new frmScreen14();
                        frm14.Show();
                        this.Close();
                        this.Dispose();
                    }
                    else
                    {
                        //imprimeCupom();
                        timerDisabled();

                        Globals.glbScreen = 17;
                        Globals.glbMensagem = 1017;
                        frmMensagem frmMsg = new frmMensagem();
                        frmMsg.Show();
                        this.Close();
                        this.Dispose();
                    }
                }
                else
                {
                    //imprimeCupom();
                    timerDisabled();

                    Globals.glbScreen = 13;
                    Globals.glbMensagem = 1006;
                    frmMensagem frmMsg = new frmMensagem();
                    frmMsg.Show();
                    this.Close();
                    this.Dispose();
                }
            }
            gConfirma = 1;
        }

        private void AtualizarInterfaceParaIdiomaCorrente()
        {
            MontarResourceSetParaIdiomaCorrente();

            butContinuar.Text = ResxSetForm.GetString("butContinuar.Text");
            butVoltar.Text = ResxSetForm.GetString("butVoltar.Text");
            lblDigiteNumeroCartao.Text = ResxSetForm.GetString("lblDigiteNumeroCartao.Text");
            lblNumeroCartao.Text = ResxSetForm.GetString("lblNumeroCartao.Text");
        }

        private void MontarResourceSetParaIdiomaCorrente()
        {
            string srtCaminhoExeTotem = System.IO.Path.GetDirectoryName(Application.ExecutablePath.ToString());

            // Português
            if (Globals.glbLanguage == 1)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen13.pt.resx");
            }
            //Espanhol
            else if (Globals.glbLanguage == 2)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen13.es.resx");
            }
            // Inglês
            else if (Globals.glbLanguage == 3)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen13.en.resx");
            }
        }

        private void timerDisabled()
        {
            tmrReturn.Enabled = false;
            tmrTeclado.Enabled = false;
        }
    }
}
