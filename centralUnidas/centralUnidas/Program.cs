﻿// Aplicação: Unidas Totem
// Data     : 01/10/2015
// Versão   : 1.0
// Empresa  : Unidas Rent a Car
// Equipe   : Unidas Desenvolvimento
// Histórico: 
//      01/10/2015: Inicio de desenvolvimento para acesso geral
//

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace centralUnidas
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 

        public static KeyboardHook kh;

        [STAThread]
        static void Main()
        {
            //using (kh = new KeyboardHook())
            //{
            Directory.SetCurrentDirectory(Path.GetDirectoryName(@"C:\UnidasTotem"));

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmSplashScreen());
            //}
        }
    }
}
