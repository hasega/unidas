﻿namespace centralUnidas
{
    partial class frmScreen08
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmScreen08));
            this.pictShutdown = new System.Windows.Forms.PictureBox();
            this.pictUnidasLogo = new System.Windows.Forms.PictureBox();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.lblValorTotalVar = new System.Windows.Forms.Label();
            this.lblValorTotal = new System.Windows.Forms.Label();
            this.lblGrupoVeiculoVar = new System.Windows.Forms.Label();
            this.lblLocalDevolucaoVar = new System.Windows.Forms.Label();
            this.lblLocalDevolucao = new System.Windows.Forms.Label();
            this.pictGrupoVeiculo = new System.Windows.Forms.PictureBox();
            this.lblDataDevolucaoVar = new System.Windows.Forms.Label();
            this.lblGrupoVeiculo = new System.Windows.Forms.Label();
            this.lblDataDevolucao = new System.Windows.Forms.Label();
            this.lblConfira = new System.Windows.Forms.Label();
            this.lblCategoria = new System.Windows.Forms.Label();
            this.lblDataRetiradaVar = new System.Windows.Forms.Label();
            this.lblReservaNumeroVar = new System.Windows.Forms.Label();
            this.lblDataRetirada = new System.Windows.Forms.Label();
            this.lblReservaNumero = new System.Windows.Forms.Label();
            this.tmrReturn = new System.Windows.Forms.Timer(this.components);
            this.lblLocalRetirada = new System.Windows.Forms.Label();
            this.lblLocalRetiradaVar = new System.Windows.Forms.Label();
            this.lblProtecoes = new System.Windows.Forms.Label();
            this.lblAcessorios = new System.Windows.Forms.Label();
            this.lblProtecoesVar1 = new System.Windows.Forms.Label();
            this.lblAcessoriosVar1 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblResumoReserva = new System.Windows.Forms.Label();
            this.lblProtecoesVar2 = new System.Windows.Forms.Label();
            this.lblProtecoesVar3 = new System.Windows.Forms.Label();
            this.lblAcessoriosVar2 = new System.Windows.Forms.Label();
            this.lblAcessoriosVar3 = new System.Windows.Forms.Label();
            this.lblAcessoriosVar4 = new System.Windows.Forms.Label();
            this.lblVoucher = new System.Windows.Forms.Label();
            this.lblVoucherVar = new System.Windows.Forms.Label();
            this.lblDiaria = new System.Windows.Forms.Label();
            this.lblDiariaVar = new System.Windows.Forms.Label();
            this.lblDescontoVar = new System.Windows.Forms.Label();
            this.lblDesconto = new System.Windows.Forms.Label();
            this.lblAdministrativaTot = new System.Windows.Forms.Label();
            this.lblTaxaAdministrativa = new System.Windows.Forms.Label();
            this.lblQuantidadeSuperior = new System.Windows.Forms.Label();
            this.lblValorUnitarioSuperior = new System.Windows.Forms.Label();
            this.lblValorTotalSuperrior = new System.Windows.Forms.Label();
            this.lblAdministrativaUnit = new System.Windows.Forms.Label();
            this.lblDiariaUnit = new System.Windows.Forms.Label();
            this.lblAcessoriosUnit2 = new System.Windows.Forms.Label();
            this.lblAcessoriosUnit1 = new System.Windows.Forms.Label();
            this.lblProtecoesUnit2 = new System.Windows.Forms.Label();
            this.lblProtecoesUnit1 = new System.Windows.Forms.Label();
            this.lblAcessoriosUnit3 = new System.Windows.Forms.Label();
            this.lblProtecoesUnit3 = new System.Windows.Forms.Label();
            this.lblDescontoQtd = new System.Windows.Forms.Label();
            this.lblDiariaQtd = new System.Windows.Forms.Label();
            this.lblAcessoriosQtd2 = new System.Windows.Forms.Label();
            this.lblAcessoriosQtd1 = new System.Windows.Forms.Label();
            this.lblProtecoesQtd2 = new System.Windows.Forms.Label();
            this.lblProtecoesQtd1 = new System.Windows.Forms.Label();
            this.lblAcessoriosQtd3 = new System.Windows.Forms.Label();
            this.lblProtecoesQtd3 = new System.Windows.Forms.Label();
            this.lblProtecoesTot2 = new System.Windows.Forms.Label();
            this.lblProtecoesTot1 = new System.Windows.Forms.Label();
            this.lblProtecoesTot3 = new System.Windows.Forms.Label();
            this.lblAcessoriosTot2 = new System.Windows.Forms.Label();
            this.lblAcessoriosTot1 = new System.Windows.Forms.Label();
            this.lblAcessoriosTot3 = new System.Windows.Forms.Label();
            this.lblAcessoriosQtd4 = new System.Windows.Forms.Label();
            this.lblAcessoriosTot4 = new System.Windows.Forms.Label();
            this.lblAcessoriosUnit4 = new System.Windows.Forms.Label();
            this.lblRetornoUnit = new System.Windows.Forms.Label();
            this.lblRetornoTot = new System.Windows.Forms.Label();
            this.lblTaxaRetorno = new System.Windows.Forms.Label();
            this.lblRetornoQtd = new System.Windows.Forms.Label();
            this.pnlReservaWait = new System.Windows.Forms.Panel();
            this.lblPorfavor = new System.Windows.Forms.Label();
            this.pictLoad = new System.Windows.Forms.PictureBox();
            this.tmrAguardar = new System.Windows.Forms.Timer(this.components);
            this.lblHoraExtraQtd = new System.Windows.Forms.Label();
            this.lblHoraExtraUnit = new System.Windows.Forms.Label();
            this.lblHoraExtraTot = new System.Windows.Forms.Label();
            this.lblHoraExtra = new System.Windows.Forms.Label();
            this.butIdioma = new System.Windows.Forms.Button();
            this.butConfirmar = new System.Windows.Forms.Button();
            this.butVoltar = new System.Windows.Forms.Button();
            this.lblSaudacoes = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictShutdown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictGrupoVeiculo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.pnlReservaWait.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictLoad)).BeginInit();
            this.SuspendLayout();
            // 
            // pictShutdown
            // 
            this.pictShutdown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictShutdown.BackColor = System.Drawing.Color.Transparent;
            this.pictShutdown.Image = global::centralUnidas.Properties.Resources.shutdown_button;
            this.pictShutdown.Location = new System.Drawing.Point(12, 157);
            this.pictShutdown.Name = "pictShutdown";
            this.pictShutdown.Size = new System.Drawing.Size(50, 55);
            this.pictShutdown.TabIndex = 125;
            this.pictShutdown.TabStop = false;
            this.pictShutdown.Visible = false;
            this.pictShutdown.Click += new System.EventHandler(this.pictShutdown_Click);
            // 
            // pictUnidasLogo
            // 
            this.pictUnidasLogo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictUnidasLogo.BackColor = System.Drawing.Color.Transparent;
            this.pictUnidasLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictUnidasLogo.Image = global::centralUnidas.Properties.Resources.totem_header;
            this.pictUnidasLogo.Location = new System.Drawing.Point(0, 0);
            this.pictUnidasLogo.Name = "pictUnidasLogo";
            this.pictUnidasLogo.Size = new System.Drawing.Size(1484, 140);
            this.pictUnidasLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictUnidasLogo.TabIndex = 131;
            this.pictUnidasLogo.TabStop = false;
            // 
            // btnAlterar
            // 
            this.btnAlterar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAlterar.BackgroundImage = global::centralUnidas.Properties.Resources.totem_reserva_alterar_new;
            this.btnAlterar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAlterar.Location = new System.Drawing.Point(564, 699);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(332, 63);
            this.btnAlterar.TabIndex = 133;
            this.btnAlterar.UseVisualStyleBackColor = true;
            this.btnAlterar.Visible = false;
            this.btnAlterar.Click += new System.EventHandler(this.btnAlterar_Click);
            // 
            // lblValorTotalVar
            // 
            this.lblValorTotalVar.AutoSize = true;
            this.lblValorTotalVar.BackColor = System.Drawing.Color.White;
            this.lblValorTotalVar.Font = new System.Drawing.Font("Arial", 22F, System.Drawing.FontStyle.Bold);
            this.lblValorTotalVar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblValorTotalVar.Location = new System.Drawing.Point(1023, 579);
            this.lblValorTotalVar.Name = "lblValorTotalVar";
            this.lblValorTotalVar.Size = new System.Drawing.Size(0, 35);
            this.lblValorTotalVar.TabIndex = 147;
            // 
            // lblValorTotal
            // 
            this.lblValorTotal.AutoSize = true;
            this.lblValorTotal.BackColor = System.Drawing.Color.White;
            this.lblValorTotal.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold);
            this.lblValorTotal.ForeColor = System.Drawing.Color.Gray;
            this.lblValorTotal.Location = new System.Drawing.Point(842, 585);
            this.lblValorTotal.Name = "lblValorTotal";
            this.lblValorTotal.Size = new System.Drawing.Size(142, 29);
            this.lblValorTotal.TabIndex = 146;
            this.lblValorTotal.Text = "Valor Total:";
            // 
            // lblGrupoVeiculoVar
            // 
            this.lblGrupoVeiculoVar.AutoSize = true;
            this.lblGrupoVeiculoVar.BackColor = System.Drawing.Color.White;
            this.lblGrupoVeiculoVar.Font = new System.Drawing.Font("Arial", 12F);
            this.lblGrupoVeiculoVar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblGrupoVeiculoVar.Location = new System.Drawing.Point(243, 315);
            this.lblGrupoVeiculoVar.Name = "lblGrupoVeiculoVar";
            this.lblGrupoVeiculoVar.Size = new System.Drawing.Size(0, 18);
            this.lblGrupoVeiculoVar.TabIndex = 145;
            // 
            // lblLocalDevolucaoVar
            // 
            this.lblLocalDevolucaoVar.AutoSize = true;
            this.lblLocalDevolucaoVar.BackColor = System.Drawing.Color.White;
            this.lblLocalDevolucaoVar.Font = new System.Drawing.Font("Arial", 12F);
            this.lblLocalDevolucaoVar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblLocalDevolucaoVar.Location = new System.Drawing.Point(652, 290);
            this.lblLocalDevolucaoVar.Name = "lblLocalDevolucaoVar";
            this.lblLocalDevolucaoVar.Size = new System.Drawing.Size(0, 18);
            this.lblLocalDevolucaoVar.TabIndex = 144;
            // 
            // lblLocalDevolucao
            // 
            this.lblLocalDevolucao.AutoSize = true;
            this.lblLocalDevolucao.BackColor = System.Drawing.Color.White;
            this.lblLocalDevolucao.Font = new System.Drawing.Font("Arial", 12F);
            this.lblLocalDevolucao.ForeColor = System.Drawing.Color.Gray;
            this.lblLocalDevolucao.Location = new System.Drawing.Point(503, 290);
            this.lblLocalDevolucao.Name = "lblLocalDevolucao";
            this.lblLocalDevolucao.Size = new System.Drawing.Size(150, 18);
            this.lblLocalDevolucao.TabIndex = 143;
            this.lblLocalDevolucao.Text = "Local da Devolução:";
            // 
            // pictGrupoVeiculo
            // 
            this.pictGrupoVeiculo.BackColor = System.Drawing.Color.Transparent;
            this.pictGrupoVeiculo.Location = new System.Drawing.Point(846, 332);
            this.pictGrupoVeiculo.Name = "pictGrupoVeiculo";
            this.pictGrupoVeiculo.Size = new System.Drawing.Size(350, 222);
            this.pictGrupoVeiculo.TabIndex = 142;
            this.pictGrupoVeiculo.TabStop = false;
            // 
            // lblDataDevolucaoVar
            // 
            this.lblDataDevolucaoVar.AutoSize = true;
            this.lblDataDevolucaoVar.BackColor = System.Drawing.Color.White;
            this.lblDataDevolucaoVar.Font = new System.Drawing.Font("Arial", 12F);
            this.lblDataDevolucaoVar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblDataDevolucaoVar.Location = new System.Drawing.Point(243, 290);
            this.lblDataDevolucaoVar.Name = "lblDataDevolucaoVar";
            this.lblDataDevolucaoVar.Size = new System.Drawing.Size(0, 18);
            this.lblDataDevolucaoVar.TabIndex = 141;
            this.lblDataDevolucaoVar.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblDataDevolucaoVar.Click += new System.EventHandler(this.lblDataDevolucaoVar_Click);
            // 
            // lblGrupoVeiculo
            // 
            this.lblGrupoVeiculo.AutoSize = true;
            this.lblGrupoVeiculo.BackColor = System.Drawing.Color.White;
            this.lblGrupoVeiculo.Font = new System.Drawing.Font("Arial", 12F);
            this.lblGrupoVeiculo.ForeColor = System.Drawing.Color.Gray;
            this.lblGrupoVeiculo.Location = new System.Drawing.Point(97, 315);
            this.lblGrupoVeiculo.Name = "lblGrupoVeiculo";
            this.lblGrupoVeiculo.Size = new System.Drawing.Size(132, 18);
            this.lblGrupoVeiculo.TabIndex = 140;
            this.lblGrupoVeiculo.Text = "Grupo de Veiculo:";
            // 
            // lblDataDevolucao
            // 
            this.lblDataDevolucao.AutoSize = true;
            this.lblDataDevolucao.BackColor = System.Drawing.Color.White;
            this.lblDataDevolucao.Font = new System.Drawing.Font("Arial", 12F);
            this.lblDataDevolucao.ForeColor = System.Drawing.Color.Gray;
            this.lblDataDevolucao.Location = new System.Drawing.Point(96, 290);
            this.lblDataDevolucao.Name = "lblDataDevolucao";
            this.lblDataDevolucao.Size = new System.Drawing.Size(146, 18);
            this.lblDataDevolucao.TabIndex = 139;
            this.lblDataDevolucao.Text = "Data da Devolução:";
            this.lblDataDevolucao.Click += new System.EventHandler(this.lblDataDevolucao_Click);
            // 
            // lblConfira
            // 
            this.lblConfira.AutoSize = true;
            this.lblConfira.BackColor = System.Drawing.Color.White;
            this.lblConfira.Font = new System.Drawing.Font("Arial", 14F);
            this.lblConfira.ForeColor = System.Drawing.Color.Gray;
            this.lblConfira.Location = new System.Drawing.Point(95, 235);
            this.lblConfira.Name = "lblConfira";
            this.lblConfira.Size = new System.Drawing.Size(71, 22);
            this.lblConfira.TabIndex = 138;
            this.lblConfira.Text = "Confira";
            this.lblConfira.Click += new System.EventHandler(this.lblConfira_Click);
            // 
            // lblCategoria
            // 
            this.lblCategoria.AutoSize = true;
            this.lblCategoria.BackColor = System.Drawing.Color.White;
            this.lblCategoria.Font = new System.Drawing.Font("Arial", 10F);
            this.lblCategoria.ForeColor = System.Drawing.Color.Gray;
            this.lblCategoria.Location = new System.Drawing.Point(1104, 229);
            this.lblCategoria.Name = "lblCategoria";
            this.lblCategoria.Size = new System.Drawing.Size(70, 16);
            this.lblCategoria.TabIndex = 152;
            this.lblCategoria.Text = "Categoria";
            this.lblCategoria.Visible = false;
            // 
            // lblDataRetiradaVar
            // 
            this.lblDataRetiradaVar.AutoSize = true;
            this.lblDataRetiradaVar.BackColor = System.Drawing.Color.White;
            this.lblDataRetiradaVar.Font = new System.Drawing.Font("Arial", 12F);
            this.lblDataRetiradaVar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblDataRetiradaVar.Location = new System.Drawing.Point(243, 268);
            this.lblDataRetiradaVar.Name = "lblDataRetiradaVar";
            this.lblDataRetiradaVar.Size = new System.Drawing.Size(0, 18);
            this.lblDataRetiradaVar.TabIndex = 151;
            this.lblDataRetiradaVar.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblReservaNumeroVar
            // 
            this.lblReservaNumeroVar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblReservaNumeroVar.AutoSize = true;
            this.lblReservaNumeroVar.BackColor = System.Drawing.Color.White;
            this.lblReservaNumeroVar.Font = new System.Drawing.Font("Arial", 10F);
            this.lblReservaNumeroVar.ForeColor = System.Drawing.Color.Gray;
            this.lblReservaNumeroVar.Location = new System.Drawing.Point(1222, 288);
            this.lblReservaNumeroVar.Name = "lblReservaNumeroVar";
            this.lblReservaNumeroVar.Size = new System.Drawing.Size(166, 16);
            this.lblReservaNumeroVar.TabIndex = 150;
            this.lblReservaNumeroVar.Text = "Reserva numero variavel";
            this.lblReservaNumeroVar.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblReservaNumeroVar.Visible = false;
            // 
            // lblDataRetirada
            // 
            this.lblDataRetirada.AutoSize = true;
            this.lblDataRetirada.BackColor = System.Drawing.Color.White;
            this.lblDataRetirada.Font = new System.Drawing.Font("Arial", 12F);
            this.lblDataRetirada.ForeColor = System.Drawing.Color.Gray;
            this.lblDataRetirada.Location = new System.Drawing.Point(96, 268);
            this.lblDataRetirada.Name = "lblDataRetirada";
            this.lblDataRetirada.Size = new System.Drawing.Size(132, 18);
            this.lblDataRetirada.TabIndex = 149;
            this.lblDataRetirada.Text = "Data da Retirada:";
            // 
            // lblReservaNumero
            // 
            this.lblReservaNumero.AutoSize = true;
            this.lblReservaNumero.BackColor = System.Drawing.Color.White;
            this.lblReservaNumero.Font = new System.Drawing.Font("Arial", 10F);
            this.lblReservaNumero.ForeColor = System.Drawing.Color.Gray;
            this.lblReservaNumero.Location = new System.Drawing.Point(1104, 256);
            this.lblReservaNumero.Name = "lblReservaNumero";
            this.lblReservaNumero.Size = new System.Drawing.Size(113, 16);
            this.lblReservaNumero.TabIndex = 148;
            this.lblReservaNumero.Text = "Reserva numero";
            this.lblReservaNumero.Visible = false;
            // 
            // tmrReturn
            // 
            this.tmrReturn.Interval = 60000;
            this.tmrReturn.Tick += new System.EventHandler(this.tmrReturn_Tick);
            // 
            // lblLocalRetirada
            // 
            this.lblLocalRetirada.AutoSize = true;
            this.lblLocalRetirada.BackColor = System.Drawing.Color.White;
            this.lblLocalRetirada.Font = new System.Drawing.Font("Arial", 12F);
            this.lblLocalRetirada.ForeColor = System.Drawing.Color.Gray;
            this.lblLocalRetirada.Location = new System.Drawing.Point(503, 268);
            this.lblLocalRetirada.Name = "lblLocalRetirada";
            this.lblLocalRetirada.Size = new System.Drawing.Size(136, 18);
            this.lblLocalRetirada.TabIndex = 153;
            this.lblLocalRetirada.Text = "Local da Retirada:";
            // 
            // lblLocalRetiradaVar
            // 
            this.lblLocalRetiradaVar.AutoSize = true;
            this.lblLocalRetiradaVar.BackColor = System.Drawing.Color.White;
            this.lblLocalRetiradaVar.Font = new System.Drawing.Font("Arial", 12F);
            this.lblLocalRetiradaVar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblLocalRetiradaVar.Location = new System.Drawing.Point(652, 268);
            this.lblLocalRetiradaVar.Name = "lblLocalRetiradaVar";
            this.lblLocalRetiradaVar.Size = new System.Drawing.Size(0, 18);
            this.lblLocalRetiradaVar.TabIndex = 154;
            this.lblLocalRetiradaVar.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblProtecoes
            // 
            this.lblProtecoes.AutoSize = true;
            this.lblProtecoes.BackColor = System.Drawing.Color.White;
            this.lblProtecoes.Font = new System.Drawing.Font("Arial", 12F);
            this.lblProtecoes.ForeColor = System.Drawing.Color.Gray;
            this.lblProtecoes.Location = new System.Drawing.Point(95, 433);
            this.lblProtecoes.Name = "lblProtecoes";
            this.lblProtecoes.Size = new System.Drawing.Size(84, 18);
            this.lblProtecoes.TabIndex = 155;
            this.lblProtecoes.Text = "Proteções:";
            // 
            // lblAcessorios
            // 
            this.lblAcessorios.AutoSize = true;
            this.lblAcessorios.BackColor = System.Drawing.Color.White;
            this.lblAcessorios.Font = new System.Drawing.Font("Arial", 12F);
            this.lblAcessorios.ForeColor = System.Drawing.Color.Gray;
            this.lblAcessorios.Location = new System.Drawing.Point(95, 533);
            this.lblAcessorios.Name = "lblAcessorios";
            this.lblAcessorios.Size = new System.Drawing.Size(91, 18);
            this.lblAcessorios.TabIndex = 156;
            this.lblAcessorios.Text = "Acessórios:";
            // 
            // lblProtecoesVar1
            // 
            this.lblProtecoesVar1.AutoSize = true;
            this.lblProtecoesVar1.BackColor = System.Drawing.Color.White;
            this.lblProtecoesVar1.Font = new System.Drawing.Font("Arial", 12F);
            this.lblProtecoesVar1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblProtecoesVar1.Location = new System.Drawing.Point(97, 460);
            this.lblProtecoesVar1.Name = "lblProtecoesVar1";
            this.lblProtecoesVar1.Size = new System.Drawing.Size(0, 18);
            this.lblProtecoesVar1.TabIndex = 157;
            // 
            // lblAcessoriosVar1
            // 
            this.lblAcessoriosVar1.AutoSize = true;
            this.lblAcessoriosVar1.BackColor = System.Drawing.Color.White;
            this.lblAcessoriosVar1.Font = new System.Drawing.Font("Arial", 12F);
            this.lblAcessoriosVar1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblAcessoriosVar1.Location = new System.Drawing.Point(96, 558);
            this.lblAcessoriosVar1.Name = "lblAcessoriosVar1";
            this.lblAcessoriosVar1.Size = new System.Drawing.Size(0, 18);
            this.lblAcessoriosVar1.TabIndex = 158;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox2.Image = global::centralUnidas.Properties.Resources.Totem_Traco;
            this.pictureBox2.Location = new System.Drawing.Point(355, 196);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(683, 1);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 159;
            this.pictureBox2.TabStop = false;
            // 
            // lblResumoReserva
            // 
            this.lblResumoReserva.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblResumoReserva.BackColor = System.Drawing.Color.White;
            this.lblResumoReserva.Font = new System.Drawing.Font("Arial", 26F, System.Drawing.FontStyle.Bold);
            this.lblResumoReserva.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblResumoReserva.Location = new System.Drawing.Point(0, 143);
            this.lblResumoReserva.Name = "lblResumoReserva";
            this.lblResumoReserva.Size = new System.Drawing.Size(1484, 50);
            this.lblResumoReserva.TabIndex = 161;
            this.lblResumoReserva.Text = "Resumo da Reserva:";
            this.lblResumoReserva.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblResumoReserva.Click += new System.EventHandler(this.lblResumoReserva_Click);
            // 
            // lblProtecoesVar2
            // 
            this.lblProtecoesVar2.AutoSize = true;
            this.lblProtecoesVar2.BackColor = System.Drawing.Color.White;
            this.lblProtecoesVar2.Font = new System.Drawing.Font("Arial", 12F);
            this.lblProtecoesVar2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblProtecoesVar2.Location = new System.Drawing.Point(97, 486);
            this.lblProtecoesVar2.Name = "lblProtecoesVar2";
            this.lblProtecoesVar2.Size = new System.Drawing.Size(0, 18);
            this.lblProtecoesVar2.TabIndex = 162;
            // 
            // lblProtecoesVar3
            // 
            this.lblProtecoesVar3.AutoSize = true;
            this.lblProtecoesVar3.BackColor = System.Drawing.Color.White;
            this.lblProtecoesVar3.Font = new System.Drawing.Font("Arial", 12F);
            this.lblProtecoesVar3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblProtecoesVar3.Location = new System.Drawing.Point(97, 510);
            this.lblProtecoesVar3.Name = "lblProtecoesVar3";
            this.lblProtecoesVar3.Size = new System.Drawing.Size(0, 18);
            this.lblProtecoesVar3.TabIndex = 163;
            // 
            // lblAcessoriosVar2
            // 
            this.lblAcessoriosVar2.AutoSize = true;
            this.lblAcessoriosVar2.BackColor = System.Drawing.Color.White;
            this.lblAcessoriosVar2.Font = new System.Drawing.Font("Arial", 12F);
            this.lblAcessoriosVar2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblAcessoriosVar2.Location = new System.Drawing.Point(96, 582);
            this.lblAcessoriosVar2.Name = "lblAcessoriosVar2";
            this.lblAcessoriosVar2.Size = new System.Drawing.Size(0, 18);
            this.lblAcessoriosVar2.TabIndex = 164;
            // 
            // lblAcessoriosVar3
            // 
            this.lblAcessoriosVar3.AutoSize = true;
            this.lblAcessoriosVar3.BackColor = System.Drawing.Color.White;
            this.lblAcessoriosVar3.Font = new System.Drawing.Font("Arial", 12F);
            this.lblAcessoriosVar3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblAcessoriosVar3.Location = new System.Drawing.Point(96, 605);
            this.lblAcessoriosVar3.Name = "lblAcessoriosVar3";
            this.lblAcessoriosVar3.Size = new System.Drawing.Size(0, 18);
            this.lblAcessoriosVar3.TabIndex = 165;
            // 
            // lblAcessoriosVar4
            // 
            this.lblAcessoriosVar4.AutoSize = true;
            this.lblAcessoriosVar4.BackColor = System.Drawing.Color.White;
            this.lblAcessoriosVar4.Font = new System.Drawing.Font("Arial", 12F);
            this.lblAcessoriosVar4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblAcessoriosVar4.Location = new System.Drawing.Point(97, 627);
            this.lblAcessoriosVar4.Name = "lblAcessoriosVar4";
            this.lblAcessoriosVar4.Size = new System.Drawing.Size(0, 18);
            this.lblAcessoriosVar4.TabIndex = 166;
            // 
            // lblVoucher
            // 
            this.lblVoucher.AutoSize = true;
            this.lblVoucher.BackColor = System.Drawing.Color.White;
            this.lblVoucher.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold);
            this.lblVoucher.ForeColor = System.Drawing.Color.Gray;
            this.lblVoucher.Location = new System.Drawing.Point(842, 622);
            this.lblVoucher.Name = "lblVoucher";
            this.lblVoucher.Size = new System.Drawing.Size(114, 29);
            this.lblVoucher.TabIndex = 219;
            this.lblVoucher.Text = "Voucher:";
            this.lblVoucher.Visible = false;
            // 
            // lblVoucherVar
            // 
            this.lblVoucherVar.AutoSize = true;
            this.lblVoucherVar.BackColor = System.Drawing.Color.White;
            this.lblVoucherVar.Font = new System.Drawing.Font("Arial", 22F, System.Drawing.FontStyle.Bold);
            this.lblVoucherVar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblVoucherVar.Location = new System.Drawing.Point(1023, 618);
            this.lblVoucherVar.Name = "lblVoucherVar";
            this.lblVoucherVar.Size = new System.Drawing.Size(0, 35);
            this.lblVoucherVar.TabIndex = 220;
            this.lblVoucherVar.Visible = false;
            // 
            // lblDiaria
            // 
            this.lblDiaria.AutoSize = true;
            this.lblDiaria.BackColor = System.Drawing.Color.White;
            this.lblDiaria.Font = new System.Drawing.Font("Arial", 12F);
            this.lblDiaria.ForeColor = System.Drawing.Color.Gray;
            this.lblDiaria.Location = new System.Drawing.Point(95, 355);
            this.lblDiaria.Name = "lblDiaria";
            this.lblDiaria.Size = new System.Drawing.Size(63, 18);
            this.lblDiaria.TabIndex = 221;
            this.lblDiaria.Text = "Diárias:";
            // 
            // lblDiariaVar
            // 
            this.lblDiariaVar.BackColor = System.Drawing.Color.White;
            this.lblDiariaVar.Font = new System.Drawing.Font("Arial", 12F);
            this.lblDiariaVar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblDiariaVar.Location = new System.Drawing.Point(684, 355);
            this.lblDiariaVar.Name = "lblDiariaVar";
            this.lblDiariaVar.Size = new System.Drawing.Size(102, 22);
            this.lblDiariaVar.TabIndex = 222;
            this.lblDiariaVar.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblDescontoVar
            // 
            this.lblDescontoVar.BackColor = System.Drawing.Color.White;
            this.lblDescontoVar.Font = new System.Drawing.Font("Arial", 12F);
            this.lblDescontoVar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblDescontoVar.Location = new System.Drawing.Point(684, 377);
            this.lblDescontoVar.Name = "lblDescontoVar";
            this.lblDescontoVar.Size = new System.Drawing.Size(102, 22);
            this.lblDescontoVar.TabIndex = 224;
            this.lblDescontoVar.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblDesconto
            // 
            this.lblDesconto.AutoSize = true;
            this.lblDesconto.BackColor = System.Drawing.Color.White;
            this.lblDesconto.Font = new System.Drawing.Font("Arial", 12F);
            this.lblDesconto.ForeColor = System.Drawing.Color.Gray;
            this.lblDesconto.Location = new System.Drawing.Point(95, 380);
            this.lblDesconto.Name = "lblDesconto";
            this.lblDesconto.Size = new System.Drawing.Size(79, 18);
            this.lblDesconto.TabIndex = 223;
            this.lblDesconto.Text = "Desconto:";
            // 
            // lblAdministrativaTot
            // 
            this.lblAdministrativaTot.BackColor = System.Drawing.Color.White;
            this.lblAdministrativaTot.Font = new System.Drawing.Font("Arial", 12F);
            this.lblAdministrativaTot.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblAdministrativaTot.Location = new System.Drawing.Point(674, 673);
            this.lblAdministrativaTot.Name = "lblAdministrativaTot";
            this.lblAdministrativaTot.Size = new System.Drawing.Size(112, 22);
            this.lblAdministrativaTot.TabIndex = 226;
            this.lblAdministrativaTot.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblTaxaAdministrativa
            // 
            this.lblTaxaAdministrativa.AutoSize = true;
            this.lblTaxaAdministrativa.BackColor = System.Drawing.Color.White;
            this.lblTaxaAdministrativa.Font = new System.Drawing.Font("Arial", 12F);
            this.lblTaxaAdministrativa.ForeColor = System.Drawing.Color.Gray;
            this.lblTaxaAdministrativa.Location = new System.Drawing.Point(95, 673);
            this.lblTaxaAdministrativa.Name = "lblTaxaAdministrativa";
            this.lblTaxaAdministrativa.Size = new System.Drawing.Size(146, 18);
            this.lblTaxaAdministrativa.TabIndex = 225;
            this.lblTaxaAdministrativa.Text = "Taxa Administrativa:";
            // 
            // lblQuantidadeSuperior
            // 
            this.lblQuantidadeSuperior.AutoSize = true;
            this.lblQuantidadeSuperior.BackColor = System.Drawing.Color.White;
            this.lblQuantidadeSuperior.Font = new System.Drawing.Font("Arial", 12F);
            this.lblQuantidadeSuperior.ForeColor = System.Drawing.Color.Gray;
            this.lblQuantidadeSuperior.Location = new System.Drawing.Point(502, 333);
            this.lblQuantidadeSuperior.Name = "lblQuantidadeSuperior";
            this.lblQuantidadeSuperior.Size = new System.Drawing.Size(37, 18);
            this.lblQuantidadeSuperior.TabIndex = 227;
            this.lblQuantidadeSuperior.Text = "Qtd.";
            // 
            // lblValorUnitarioSuperior
            // 
            this.lblValorUnitarioSuperior.AutoSize = true;
            this.lblValorUnitarioSuperior.BackColor = System.Drawing.Color.White;
            this.lblValorUnitarioSuperior.Font = new System.Drawing.Font("Arial", 12F);
            this.lblValorUnitarioSuperior.ForeColor = System.Drawing.Color.Gray;
            this.lblValorUnitarioSuperior.Location = new System.Drawing.Point(596, 333);
            this.lblValorUnitarioSuperior.Name = "lblValorUnitarioSuperior";
            this.lblValorUnitarioSuperior.Size = new System.Drawing.Size(65, 18);
            this.lblValorUnitarioSuperior.TabIndex = 228;
            this.lblValorUnitarioSuperior.Text = "Vlr. Unit.";
            // 
            // lblValorTotalSuperrior
            // 
            this.lblValorTotalSuperrior.AutoSize = true;
            this.lblValorTotalSuperrior.BackColor = System.Drawing.Color.White;
            this.lblValorTotalSuperrior.Font = new System.Drawing.Font("Arial", 12F);
            this.lblValorTotalSuperrior.ForeColor = System.Drawing.Color.Gray;
            this.lblValorTotalSuperrior.Location = new System.Drawing.Point(715, 333);
            this.lblValorTotalSuperrior.Name = "lblValorTotalSuperrior";
            this.lblValorTotalSuperrior.Size = new System.Drawing.Size(66, 18);
            this.lblValorTotalSuperrior.TabIndex = 229;
            this.lblValorTotalSuperrior.Text = "Vlr. Total";
            // 
            // lblAdministrativaUnit
            // 
            this.lblAdministrativaUnit.BackColor = System.Drawing.Color.White;
            this.lblAdministrativaUnit.Font = new System.Drawing.Font("Arial", 12F);
            this.lblAdministrativaUnit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblAdministrativaUnit.Location = new System.Drawing.Point(582, 673);
            this.lblAdministrativaUnit.Name = "lblAdministrativaUnit";
            this.lblAdministrativaUnit.Size = new System.Drawing.Size(93, 22);
            this.lblAdministrativaUnit.TabIndex = 238;
            this.lblAdministrativaUnit.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblDiariaUnit
            // 
            this.lblDiariaUnit.BackColor = System.Drawing.Color.White;
            this.lblDiariaUnit.Font = new System.Drawing.Font("Arial", 12F);
            this.lblDiariaUnit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblDiariaUnit.Location = new System.Drawing.Point(579, 355);
            this.lblDiariaUnit.Name = "lblDiariaUnit";
            this.lblDiariaUnit.Size = new System.Drawing.Size(96, 22);
            this.lblDiariaUnit.TabIndex = 236;
            this.lblDiariaUnit.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblAcessoriosUnit2
            // 
            this.lblAcessoriosUnit2.BackColor = System.Drawing.Color.White;
            this.lblAcessoriosUnit2.Font = new System.Drawing.Font("Arial", 12F);
            this.lblAcessoriosUnit2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblAcessoriosUnit2.Location = new System.Drawing.Point(582, 582);
            this.lblAcessoriosUnit2.Name = "lblAcessoriosUnit2";
            this.lblAcessoriosUnit2.Size = new System.Drawing.Size(93, 22);
            this.lblAcessoriosUnit2.TabIndex = 235;
            this.lblAcessoriosUnit2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblAcessoriosUnit1
            // 
            this.lblAcessoriosUnit1.BackColor = System.Drawing.Color.White;
            this.lblAcessoriosUnit1.Font = new System.Drawing.Font("Arial", 12F);
            this.lblAcessoriosUnit1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblAcessoriosUnit1.Location = new System.Drawing.Point(582, 558);
            this.lblAcessoriosUnit1.Name = "lblAcessoriosUnit1";
            this.lblAcessoriosUnit1.Size = new System.Drawing.Size(93, 22);
            this.lblAcessoriosUnit1.TabIndex = 234;
            this.lblAcessoriosUnit1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblProtecoesUnit2
            // 
            this.lblProtecoesUnit2.BackColor = System.Drawing.Color.White;
            this.lblProtecoesUnit2.Font = new System.Drawing.Font("Arial", 12F);
            this.lblProtecoesUnit2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblProtecoesUnit2.Location = new System.Drawing.Point(579, 486);
            this.lblProtecoesUnit2.Name = "lblProtecoesUnit2";
            this.lblProtecoesUnit2.Size = new System.Drawing.Size(96, 22);
            this.lblProtecoesUnit2.TabIndex = 233;
            this.lblProtecoesUnit2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblProtecoesUnit1
            // 
            this.lblProtecoesUnit1.BackColor = System.Drawing.Color.White;
            this.lblProtecoesUnit1.Font = new System.Drawing.Font("Arial", 12F);
            this.lblProtecoesUnit1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblProtecoesUnit1.Location = new System.Drawing.Point(579, 460);
            this.lblProtecoesUnit1.Name = "lblProtecoesUnit1";
            this.lblProtecoesUnit1.Size = new System.Drawing.Size(96, 22);
            this.lblProtecoesUnit1.TabIndex = 232;
            this.lblProtecoesUnit1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblAcessoriosUnit3
            // 
            this.lblAcessoriosUnit3.BackColor = System.Drawing.Color.White;
            this.lblAcessoriosUnit3.Font = new System.Drawing.Font("Arial", 12F);
            this.lblAcessoriosUnit3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblAcessoriosUnit3.Location = new System.Drawing.Point(582, 605);
            this.lblAcessoriosUnit3.Name = "lblAcessoriosUnit3";
            this.lblAcessoriosUnit3.Size = new System.Drawing.Size(93, 22);
            this.lblAcessoriosUnit3.TabIndex = 231;
            this.lblAcessoriosUnit3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblAcessoriosUnit3.Click += new System.EventHandler(this.label14_Click);
            // 
            // lblProtecoesUnit3
            // 
            this.lblProtecoesUnit3.BackColor = System.Drawing.Color.White;
            this.lblProtecoesUnit3.Font = new System.Drawing.Font("Arial", 12F);
            this.lblProtecoesUnit3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblProtecoesUnit3.Location = new System.Drawing.Point(579, 510);
            this.lblProtecoesUnit3.Name = "lblProtecoesUnit3";
            this.lblProtecoesUnit3.Size = new System.Drawing.Size(96, 22);
            this.lblProtecoesUnit3.TabIndex = 230;
            this.lblProtecoesUnit3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblDescontoQtd
            // 
            this.lblDescontoQtd.AutoSize = true;
            this.lblDescontoQtd.BackColor = System.Drawing.Color.White;
            this.lblDescontoQtd.Font = new System.Drawing.Font("Arial", 12F);
            this.lblDescontoQtd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblDescontoQtd.Location = new System.Drawing.Point(507, 380);
            this.lblDescontoQtd.Name = "lblDescontoQtd";
            this.lblDescontoQtd.Size = new System.Drawing.Size(0, 18);
            this.lblDescontoQtd.TabIndex = 246;
            this.lblDescontoQtd.Visible = false;
            // 
            // lblDiariaQtd
            // 
            this.lblDiariaQtd.AutoSize = true;
            this.lblDiariaQtd.BackColor = System.Drawing.Color.White;
            this.lblDiariaQtd.Font = new System.Drawing.Font("Arial", 12F);
            this.lblDiariaQtd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblDiariaQtd.Location = new System.Drawing.Point(507, 355);
            this.lblDiariaQtd.Name = "lblDiariaQtd";
            this.lblDiariaQtd.Size = new System.Drawing.Size(0, 18);
            this.lblDiariaQtd.TabIndex = 245;
            // 
            // lblAcessoriosQtd2
            // 
            this.lblAcessoriosQtd2.AutoSize = true;
            this.lblAcessoriosQtd2.BackColor = System.Drawing.Color.White;
            this.lblAcessoriosQtd2.Font = new System.Drawing.Font("Arial", 12F);
            this.lblAcessoriosQtd2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblAcessoriosQtd2.Location = new System.Drawing.Point(507, 582);
            this.lblAcessoriosQtd2.Name = "lblAcessoriosQtd2";
            this.lblAcessoriosQtd2.Size = new System.Drawing.Size(0, 18);
            this.lblAcessoriosQtd2.TabIndex = 244;
            // 
            // lblAcessoriosQtd1
            // 
            this.lblAcessoriosQtd1.AutoSize = true;
            this.lblAcessoriosQtd1.BackColor = System.Drawing.Color.White;
            this.lblAcessoriosQtd1.Font = new System.Drawing.Font("Arial", 12F);
            this.lblAcessoriosQtd1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblAcessoriosQtd1.Location = new System.Drawing.Point(507, 558);
            this.lblAcessoriosQtd1.Name = "lblAcessoriosQtd1";
            this.lblAcessoriosQtd1.Size = new System.Drawing.Size(0, 18);
            this.lblAcessoriosQtd1.TabIndex = 243;
            // 
            // lblProtecoesQtd2
            // 
            this.lblProtecoesQtd2.AutoSize = true;
            this.lblProtecoesQtd2.BackColor = System.Drawing.Color.White;
            this.lblProtecoesQtd2.Font = new System.Drawing.Font("Arial", 12F);
            this.lblProtecoesQtd2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblProtecoesQtd2.Location = new System.Drawing.Point(507, 486);
            this.lblProtecoesQtd2.Name = "lblProtecoesQtd2";
            this.lblProtecoesQtd2.Size = new System.Drawing.Size(0, 18);
            this.lblProtecoesQtd2.TabIndex = 242;
            // 
            // lblProtecoesQtd1
            // 
            this.lblProtecoesQtd1.AutoSize = true;
            this.lblProtecoesQtd1.BackColor = System.Drawing.Color.White;
            this.lblProtecoesQtd1.Font = new System.Drawing.Font("Arial", 12F);
            this.lblProtecoesQtd1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblProtecoesQtd1.Location = new System.Drawing.Point(507, 460);
            this.lblProtecoesQtd1.Name = "lblProtecoesQtd1";
            this.lblProtecoesQtd1.Size = new System.Drawing.Size(0, 18);
            this.lblProtecoesQtd1.TabIndex = 241;
            // 
            // lblAcessoriosQtd3
            // 
            this.lblAcessoriosQtd3.AutoSize = true;
            this.lblAcessoriosQtd3.BackColor = System.Drawing.Color.White;
            this.lblAcessoriosQtd3.Font = new System.Drawing.Font("Arial", 12F);
            this.lblAcessoriosQtd3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblAcessoriosQtd3.Location = new System.Drawing.Point(507, 605);
            this.lblAcessoriosQtd3.Name = "lblAcessoriosQtd3";
            this.lblAcessoriosQtd3.Size = new System.Drawing.Size(0, 18);
            this.lblAcessoriosQtd3.TabIndex = 240;
            this.lblAcessoriosQtd3.Click += new System.EventHandler(this.label23_Click);
            // 
            // lblProtecoesQtd3
            // 
            this.lblProtecoesQtd3.AutoSize = true;
            this.lblProtecoesQtd3.BackColor = System.Drawing.Color.White;
            this.lblProtecoesQtd3.Font = new System.Drawing.Font("Arial", 12F);
            this.lblProtecoesQtd3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblProtecoesQtd3.Location = new System.Drawing.Point(507, 510);
            this.lblProtecoesQtd3.Name = "lblProtecoesQtd3";
            this.lblProtecoesQtd3.Size = new System.Drawing.Size(0, 18);
            this.lblProtecoesQtd3.TabIndex = 239;
            // 
            // lblProtecoesTot2
            // 
            this.lblProtecoesTot2.BackColor = System.Drawing.Color.White;
            this.lblProtecoesTot2.Font = new System.Drawing.Font("Arial", 12F);
            this.lblProtecoesTot2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblProtecoesTot2.Location = new System.Drawing.Point(684, 486);
            this.lblProtecoesTot2.Name = "lblProtecoesTot2";
            this.lblProtecoesTot2.Size = new System.Drawing.Size(102, 22);
            this.lblProtecoesTot2.TabIndex = 249;
            this.lblProtecoesTot2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblProtecoesTot1
            // 
            this.lblProtecoesTot1.BackColor = System.Drawing.Color.White;
            this.lblProtecoesTot1.Font = new System.Drawing.Font("Arial", 12F);
            this.lblProtecoesTot1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblProtecoesTot1.Location = new System.Drawing.Point(684, 460);
            this.lblProtecoesTot1.Name = "lblProtecoesTot1";
            this.lblProtecoesTot1.Size = new System.Drawing.Size(102, 22);
            this.lblProtecoesTot1.TabIndex = 248;
            this.lblProtecoesTot1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblProtecoesTot3
            // 
            this.lblProtecoesTot3.BackColor = System.Drawing.Color.White;
            this.lblProtecoesTot3.Font = new System.Drawing.Font("Arial", 12F);
            this.lblProtecoesTot3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblProtecoesTot3.Location = new System.Drawing.Point(684, 510);
            this.lblProtecoesTot3.Name = "lblProtecoesTot3";
            this.lblProtecoesTot3.Size = new System.Drawing.Size(102, 22);
            this.lblProtecoesTot3.TabIndex = 247;
            this.lblProtecoesTot3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblAcessoriosTot2
            // 
            this.lblAcessoriosTot2.BackColor = System.Drawing.Color.White;
            this.lblAcessoriosTot2.Font = new System.Drawing.Font("Arial", 12F);
            this.lblAcessoriosTot2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblAcessoriosTot2.Location = new System.Drawing.Point(687, 582);
            this.lblAcessoriosTot2.Name = "lblAcessoriosTot2";
            this.lblAcessoriosTot2.Size = new System.Drawing.Size(99, 22);
            this.lblAcessoriosTot2.TabIndex = 252;
            this.lblAcessoriosTot2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblAcessoriosTot1
            // 
            this.lblAcessoriosTot1.BackColor = System.Drawing.Color.White;
            this.lblAcessoriosTot1.Font = new System.Drawing.Font("Arial", 12F);
            this.lblAcessoriosTot1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblAcessoriosTot1.Location = new System.Drawing.Point(687, 558);
            this.lblAcessoriosTot1.Name = "lblAcessoriosTot1";
            this.lblAcessoriosTot1.Size = new System.Drawing.Size(99, 22);
            this.lblAcessoriosTot1.TabIndex = 251;
            this.lblAcessoriosTot1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblAcessoriosTot3
            // 
            this.lblAcessoriosTot3.BackColor = System.Drawing.Color.White;
            this.lblAcessoriosTot3.Font = new System.Drawing.Font("Arial", 12F);
            this.lblAcessoriosTot3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblAcessoriosTot3.Location = new System.Drawing.Point(687, 605);
            this.lblAcessoriosTot3.Name = "lblAcessoriosTot3";
            this.lblAcessoriosTot3.Size = new System.Drawing.Size(99, 22);
            this.lblAcessoriosTot3.TabIndex = 250;
            this.lblAcessoriosTot3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblAcessoriosTot3.Click += new System.EventHandler(this.label28_Click);
            // 
            // lblAcessoriosQtd4
            // 
            this.lblAcessoriosQtd4.AutoSize = true;
            this.lblAcessoriosQtd4.BackColor = System.Drawing.Color.White;
            this.lblAcessoriosQtd4.Font = new System.Drawing.Font("Arial", 12F);
            this.lblAcessoriosQtd4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblAcessoriosQtd4.Location = new System.Drawing.Point(507, 627);
            this.lblAcessoriosQtd4.Name = "lblAcessoriosQtd4";
            this.lblAcessoriosQtd4.Size = new System.Drawing.Size(0, 18);
            this.lblAcessoriosQtd4.TabIndex = 253;
            // 
            // lblAcessoriosTot4
            // 
            this.lblAcessoriosTot4.BackColor = System.Drawing.Color.White;
            this.lblAcessoriosTot4.Font = new System.Drawing.Font("Arial", 12F);
            this.lblAcessoriosTot4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblAcessoriosTot4.Location = new System.Drawing.Point(687, 627);
            this.lblAcessoriosTot4.Name = "lblAcessoriosTot4";
            this.lblAcessoriosTot4.Size = new System.Drawing.Size(99, 22);
            this.lblAcessoriosTot4.TabIndex = 254;
            this.lblAcessoriosTot4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblAcessoriosUnit4
            // 
            this.lblAcessoriosUnit4.BackColor = System.Drawing.Color.White;
            this.lblAcessoriosUnit4.Font = new System.Drawing.Font("Arial", 12F);
            this.lblAcessoriosUnit4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblAcessoriosUnit4.Location = new System.Drawing.Point(582, 627);
            this.lblAcessoriosUnit4.Name = "lblAcessoriosUnit4";
            this.lblAcessoriosUnit4.Size = new System.Drawing.Size(93, 22);
            this.lblAcessoriosUnit4.TabIndex = 255;
            this.lblAcessoriosUnit4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblRetornoUnit
            // 
            this.lblRetornoUnit.BackColor = System.Drawing.Color.White;
            this.lblRetornoUnit.Font = new System.Drawing.Font("Arial", 12F);
            this.lblRetornoUnit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblRetornoUnit.Location = new System.Drawing.Point(582, 650);
            this.lblRetornoUnit.Name = "lblRetornoUnit";
            this.lblRetornoUnit.Size = new System.Drawing.Size(93, 22);
            this.lblRetornoUnit.TabIndex = 258;
            this.lblRetornoUnit.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblRetornoTot
            // 
            this.lblRetornoTot.BackColor = System.Drawing.Color.White;
            this.lblRetornoTot.Font = new System.Drawing.Font("Arial", 12F);
            this.lblRetornoTot.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblRetornoTot.Location = new System.Drawing.Point(674, 650);
            this.lblRetornoTot.Name = "lblRetornoTot";
            this.lblRetornoTot.Size = new System.Drawing.Size(112, 22);
            this.lblRetornoTot.TabIndex = 257;
            this.lblRetornoTot.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblTaxaRetorno
            // 
            this.lblTaxaRetorno.AutoSize = true;
            this.lblTaxaRetorno.BackColor = System.Drawing.Color.White;
            this.lblTaxaRetorno.Font = new System.Drawing.Font("Arial", 12F);
            this.lblTaxaRetorno.ForeColor = System.Drawing.Color.Gray;
            this.lblTaxaRetorno.Location = new System.Drawing.Point(95, 650);
            this.lblTaxaRetorno.Name = "lblTaxaRetorno";
            this.lblTaxaRetorno.Size = new System.Drawing.Size(125, 18);
            this.lblTaxaRetorno.TabIndex = 256;
            this.lblTaxaRetorno.Text = "Taxa de Retorno:";
            // 
            // lblRetornoQtd
            // 
            this.lblRetornoQtd.AutoSize = true;
            this.lblRetornoQtd.BackColor = System.Drawing.Color.White;
            this.lblRetornoQtd.Font = new System.Drawing.Font("Arial", 12F);
            this.lblRetornoQtd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblRetornoQtd.Location = new System.Drawing.Point(508, 650);
            this.lblRetornoQtd.Name = "lblRetornoQtd";
            this.lblRetornoQtd.Size = new System.Drawing.Size(0, 18);
            this.lblRetornoQtd.TabIndex = 259;
            // 
            // pnlReservaWait
            // 
            this.pnlReservaWait.Controls.Add(this.lblPorfavor);
            this.pnlReservaWait.Controls.Add(this.pictLoad);
            this.pnlReservaWait.Location = new System.Drawing.Point(7, 203);
            this.pnlReservaWait.Name = "pnlReservaWait";
            this.pnlReservaWait.Size = new System.Drawing.Size(1353, 570);
            this.pnlReservaWait.TabIndex = 262;
            // 
            // lblPorfavor
            // 
            this.lblPorfavor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPorfavor.BackColor = System.Drawing.Color.White;
            this.lblPorfavor.Font = new System.Drawing.Font("Arial", 26F, System.Drawing.FontStyle.Bold);
            this.lblPorfavor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblPorfavor.Location = new System.Drawing.Point(3, 226);
            this.lblPorfavor.Name = "lblPorfavor";
            this.lblPorfavor.Size = new System.Drawing.Size(1347, 50);
            this.lblPorfavor.TabIndex = 162;
            this.lblPorfavor.Text = "Por favor, aguarde ...";
            this.lblPorfavor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictLoad
            // 
            this.pictLoad.Image = global::centralUnidas.Properties.Resources.Loading;
            this.pictLoad.Location = new System.Drawing.Point(859, 226);
            this.pictLoad.Name = "pictLoad";
            this.pictLoad.Size = new System.Drawing.Size(55, 50);
            this.pictLoad.TabIndex = 163;
            this.pictLoad.TabStop = false;
            this.pictLoad.Visible = false;
            // 
            // tmrAguardar
            // 
            this.tmrAguardar.Tick += new System.EventHandler(this.tmrAguardar_Tick);
            // 
            // lblHoraExtraQtd
            // 
            this.lblHoraExtraQtd.AutoSize = true;
            this.lblHoraExtraQtd.BackColor = System.Drawing.Color.White;
            this.lblHoraExtraQtd.Font = new System.Drawing.Font("Arial", 12F);
            this.lblHoraExtraQtd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblHoraExtraQtd.Location = new System.Drawing.Point(507, 405);
            this.lblHoraExtraQtd.Name = "lblHoraExtraQtd";
            this.lblHoraExtraQtd.Size = new System.Drawing.Size(0, 18);
            this.lblHoraExtraQtd.TabIndex = 264;
            // 
            // lblHoraExtraUnit
            // 
            this.lblHoraExtraUnit.BackColor = System.Drawing.Color.White;
            this.lblHoraExtraUnit.Font = new System.Drawing.Font("Arial", 12F);
            this.lblHoraExtraUnit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblHoraExtraUnit.Location = new System.Drawing.Point(581, 405);
            this.lblHoraExtraUnit.Name = "lblHoraExtraUnit";
            this.lblHoraExtraUnit.Size = new System.Drawing.Size(93, 22);
            this.lblHoraExtraUnit.TabIndex = 263;
            this.lblHoraExtraUnit.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblHoraExtraTot
            // 
            this.lblHoraExtraTot.BackColor = System.Drawing.Color.White;
            this.lblHoraExtraTot.Font = new System.Drawing.Font("Arial", 12F);
            this.lblHoraExtraTot.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblHoraExtraTot.Location = new System.Drawing.Point(673, 405);
            this.lblHoraExtraTot.Name = "lblHoraExtraTot";
            this.lblHoraExtraTot.Size = new System.Drawing.Size(112, 22);
            this.lblHoraExtraTot.TabIndex = 262;
            this.lblHoraExtraTot.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblHoraExtra
            // 
            this.lblHoraExtra.AutoSize = true;
            this.lblHoraExtra.BackColor = System.Drawing.Color.White;
            this.lblHoraExtra.Font = new System.Drawing.Font("Arial", 12F);
            this.lblHoraExtra.ForeColor = System.Drawing.Color.Gray;
            this.lblHoraExtra.Location = new System.Drawing.Point(95, 405);
            this.lblHoraExtra.Name = "lblHoraExtra";
            this.lblHoraExtra.Size = new System.Drawing.Size(82, 18);
            this.lblHoraExtra.TabIndex = 261;
            this.lblHoraExtra.Text = "Hora Extra";
            // 
            // butIdioma
            // 
            this.butIdioma.FlatAppearance.BorderSize = 0;
            this.butIdioma.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.butIdioma.ForeColor = System.Drawing.Color.Gray;
            this.butIdioma.Image = global::centralUnidas.Properties.Resources.Totem_Flag_Brasil_botao;
            this.butIdioma.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butIdioma.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.butIdioma.Location = new System.Drawing.Point(1148, 0);
            this.butIdioma.Name = "butIdioma";
            this.butIdioma.Size = new System.Drawing.Size(148, 61);
            this.butIdioma.TabIndex = 265;
            this.butIdioma.Text = "PORTUGUÊS";
            this.butIdioma.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butIdioma.UseCompatibleTextRendering = true;
            this.butIdioma.UseMnemonic = false;
            this.butIdioma.UseVisualStyleBackColor = true;
            this.butIdioma.Click += new System.EventHandler(this.butIdioma_Click);
            // 
            // butConfirmar
            // 
            this.butConfirmar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.butConfirmar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.butConfirmar.Font = new System.Drawing.Font("Arial", 22F, System.Drawing.FontStyle.Bold);
            this.butConfirmar.ForeColor = System.Drawing.Color.White;
            this.butConfirmar.Location = new System.Drawing.Point(718, 697);
            this.butConfirmar.Name = "butConfirmar";
            this.butConfirmar.Size = new System.Drawing.Size(250, 63);
            this.butConfirmar.TabIndex = 268;
            this.butConfirmar.Text = "Confirmar";
            this.butConfirmar.UseVisualStyleBackColor = false;
            this.butConfirmar.Click += new System.EventHandler(this.butConfirmar_Click);
            // 
            // butVoltar
            // 
            this.butVoltar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.butVoltar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.butVoltar.Font = new System.Drawing.Font("Arial", 22F, System.Drawing.FontStyle.Bold);
            this.butVoltar.ForeColor = System.Drawing.Color.White;
            this.butVoltar.Location = new System.Drawing.Point(382, 699);
            this.butVoltar.Name = "butVoltar";
            this.butVoltar.Size = new System.Drawing.Size(250, 63);
            this.butVoltar.TabIndex = 267;
            this.butVoltar.Text = "Voltar";
            this.butVoltar.UseVisualStyleBackColor = false;
            this.butVoltar.Click += new System.EventHandler(this.butVoltar_Click);
            // 
            // lblSaudacoes
            // 
            this.lblSaudacoes.AutoSize = true;
            this.lblSaudacoes.BackColor = System.Drawing.Color.White;
            this.lblSaudacoes.Font = new System.Drawing.Font("Arial", 14F);
            this.lblSaudacoes.ForeColor = System.Drawing.Color.Gray;
            this.lblSaudacoes.Location = new System.Drawing.Point(96, 209);
            this.lblSaudacoes.Name = "lblSaudacoes";
            this.lblSaudacoes.Size = new System.Drawing.Size(106, 22);
            this.lblSaudacoes.TabIndex = 269;
            this.lblSaudacoes.Text = "Saudações";
            // 
            // frmScreen08
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1362, 764);
            this.ControlBox = false;
            this.Controls.Add(this.pnlReservaWait);
            this.Controls.Add(this.lblSaudacoes);
            this.Controls.Add(this.butConfirmar);
            this.Controls.Add(this.butVoltar);
            this.Controls.Add(this.butIdioma);
            this.Controls.Add(this.lblHoraExtraQtd);
            this.Controls.Add(this.lblHoraExtraUnit);
            this.Controls.Add(this.lblHoraExtraTot);
            this.Controls.Add(this.lblRetornoQtd);
            this.Controls.Add(this.lblRetornoUnit);
            this.Controls.Add(this.lblRetornoTot);
            this.Controls.Add(this.lblTaxaRetorno);
            this.Controls.Add(this.lblAcessoriosUnit4);
            this.Controls.Add(this.lblAcessoriosTot4);
            this.Controls.Add(this.lblAcessoriosQtd4);
            this.Controls.Add(this.lblAcessoriosTot2);
            this.Controls.Add(this.lblAcessoriosTot1);
            this.Controls.Add(this.lblAcessoriosTot3);
            this.Controls.Add(this.lblProtecoesTot2);
            this.Controls.Add(this.lblProtecoesTot1);
            this.Controls.Add(this.lblProtecoesTot3);
            this.Controls.Add(this.lblDescontoQtd);
            this.Controls.Add(this.lblDiariaQtd);
            this.Controls.Add(this.lblAcessoriosQtd2);
            this.Controls.Add(this.lblAcessoriosQtd1);
            this.Controls.Add(this.lblProtecoesQtd2);
            this.Controls.Add(this.lblProtecoesQtd1);
            this.Controls.Add(this.lblAcessoriosQtd3);
            this.Controls.Add(this.lblProtecoesQtd3);
            this.Controls.Add(this.lblAdministrativaUnit);
            this.Controls.Add(this.lblDiariaUnit);
            this.Controls.Add(this.lblAcessoriosUnit2);
            this.Controls.Add(this.lblAcessoriosUnit1);
            this.Controls.Add(this.lblProtecoesUnit2);
            this.Controls.Add(this.lblProtecoesUnit1);
            this.Controls.Add(this.lblAcessoriosUnit3);
            this.Controls.Add(this.lblProtecoesUnit3);
            this.Controls.Add(this.lblValorTotalSuperrior);
            this.Controls.Add(this.lblValorUnitarioSuperior);
            this.Controls.Add(this.lblQuantidadeSuperior);
            this.Controls.Add(this.lblAdministrativaTot);
            this.Controls.Add(this.lblTaxaAdministrativa);
            this.Controls.Add(this.lblDescontoVar);
            this.Controls.Add(this.lblDesconto);
            this.Controls.Add(this.lblDiariaVar);
            this.Controls.Add(this.lblDiaria);
            this.Controls.Add(this.lblVoucherVar);
            this.Controls.Add(this.lblVoucher);
            this.Controls.Add(this.lblAcessoriosVar4);
            this.Controls.Add(this.lblAcessoriosVar3);
            this.Controls.Add(this.lblAcessoriosVar2);
            this.Controls.Add(this.lblProtecoesVar3);
            this.Controls.Add(this.lblProtecoesVar2);
            this.Controls.Add(this.pictShutdown);
            this.Controls.Add(this.lblResumoReserva);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.lblAcessoriosVar1);
            this.Controls.Add(this.lblProtecoesVar1);
            this.Controls.Add(this.lblAcessorios);
            this.Controls.Add(this.lblProtecoes);
            this.Controls.Add(this.lblLocalRetiradaVar);
            this.Controls.Add(this.lblLocalRetirada);
            this.Controls.Add(this.lblCategoria);
            this.Controls.Add(this.lblDataRetiradaVar);
            this.Controls.Add(this.lblReservaNumeroVar);
            this.Controls.Add(this.lblDataRetirada);
            this.Controls.Add(this.lblReservaNumero);
            this.Controls.Add(this.lblValorTotalVar);
            this.Controls.Add(this.lblValorTotal);
            this.Controls.Add(this.lblGrupoVeiculoVar);
            this.Controls.Add(this.lblLocalDevolucaoVar);
            this.Controls.Add(this.lblLocalDevolucao);
            this.Controls.Add(this.pictGrupoVeiculo);
            this.Controls.Add(this.lblDataDevolucaoVar);
            this.Controls.Add(this.lblGrupoVeiculo);
            this.Controls.Add(this.lblDataDevolucao);
            this.Controls.Add(this.lblConfira);
            this.Controls.Add(this.pictUnidasLogo);
            this.Controls.Add(this.lblHoraExtra);
            this.Controls.Add(this.btnAlterar);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmScreen08";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CAU - Unidas";
            this.TopMost = false;
            this.WindowState = System.Windows.Forms.FormWindowState.Normal;
            this.Load += new System.EventHandler(this.frmScreen08_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictShutdown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictGrupoVeiculo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.pnlReservaWait.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictLoad)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictShutdown;
        private System.Windows.Forms.PictureBox pictUnidasLogo;
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.Label lblValorTotalVar;
        private System.Windows.Forms.Label lblValorTotal;
        private System.Windows.Forms.Label lblGrupoVeiculoVar;
        private System.Windows.Forms.Label lblLocalDevolucaoVar;
        private System.Windows.Forms.Label lblLocalDevolucao;
        private System.Windows.Forms.PictureBox pictGrupoVeiculo;
        private System.Windows.Forms.Label lblDataDevolucaoVar;
        private System.Windows.Forms.Label lblGrupoVeiculo;
        private System.Windows.Forms.Label lblDataDevolucao;
        private System.Windows.Forms.Label lblConfira;
        private System.Windows.Forms.Label lblCategoria;
        private System.Windows.Forms.Label lblDataRetiradaVar;
        private System.Windows.Forms.Label lblReservaNumeroVar;
        private System.Windows.Forms.Label lblDataRetirada;
        private System.Windows.Forms.Label lblReservaNumero;
        private System.Windows.Forms.Timer tmrReturn;
        private System.Windows.Forms.Label lblLocalRetirada;
        private System.Windows.Forms.Label lblLocalRetiradaVar;
        private System.Windows.Forms.Label lblProtecoes;
        private System.Windows.Forms.Label lblAcessorios;
        private System.Windows.Forms.Label lblProtecoesVar1;
        private System.Windows.Forms.Label lblAcessoriosVar1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblResumoReserva;
        private System.Windows.Forms.Label lblProtecoesVar2;
        private System.Windows.Forms.Label lblProtecoesVar3;
        private System.Windows.Forms.Label lblAcessoriosVar2;
        private System.Windows.Forms.Label lblAcessoriosVar3;
        private System.Windows.Forms.Label lblAcessoriosVar4;
        private System.Windows.Forms.Label lblVoucher;
        private System.Windows.Forms.Label lblVoucherVar;
        private System.Windows.Forms.Label lblDiaria;
        private System.Windows.Forms.Label lblDiariaVar;
        private System.Windows.Forms.Label lblDescontoVar;
        private System.Windows.Forms.Label lblDesconto;
        private System.Windows.Forms.Label lblAdministrativaTot;
        private System.Windows.Forms.Label lblTaxaAdministrativa;
        private System.Windows.Forms.Label lblQuantidadeSuperior;
        private System.Windows.Forms.Label lblValorUnitarioSuperior;
        private System.Windows.Forms.Label lblValorTotalSuperrior;
        private System.Windows.Forms.Label lblAdministrativaUnit;
        private System.Windows.Forms.Label lblDiariaUnit;
        private System.Windows.Forms.Label lblAcessoriosUnit2;
        private System.Windows.Forms.Label lblAcessoriosUnit1;
        private System.Windows.Forms.Label lblProtecoesUnit2;
        private System.Windows.Forms.Label lblProtecoesUnit1;
        private System.Windows.Forms.Label lblAcessoriosUnit3;
        private System.Windows.Forms.Label lblProtecoesUnit3;
        private System.Windows.Forms.Label lblDescontoQtd;
        private System.Windows.Forms.Label lblDiariaQtd;
        private System.Windows.Forms.Label lblAcessoriosQtd2;
        private System.Windows.Forms.Label lblAcessoriosQtd1;
        private System.Windows.Forms.Label lblProtecoesQtd2;
        private System.Windows.Forms.Label lblProtecoesQtd1;
        private System.Windows.Forms.Label lblAcessoriosQtd3;
        private System.Windows.Forms.Label lblProtecoesQtd3;
        private System.Windows.Forms.Label lblProtecoesTot2;
        private System.Windows.Forms.Label lblProtecoesTot1;
        private System.Windows.Forms.Label lblProtecoesTot3;
        private System.Windows.Forms.Label lblAcessoriosTot2;
        private System.Windows.Forms.Label lblAcessoriosTot1;
        private System.Windows.Forms.Label lblAcessoriosTot3;
        private System.Windows.Forms.Label lblAcessoriosQtd4;
        private System.Windows.Forms.Label lblAcessoriosTot4;
        private System.Windows.Forms.Label lblAcessoriosUnit4;
        private System.Windows.Forms.Label lblRetornoUnit;
        private System.Windows.Forms.Label lblRetornoTot;
        private System.Windows.Forms.Label lblTaxaRetorno;
        private System.Windows.Forms.Label lblRetornoQtd;
        private System.Windows.Forms.Panel pnlReservaWait;
        private System.Windows.Forms.Label lblPorfavor;
        private System.Windows.Forms.Timer tmrAguardar;
        private System.Windows.Forms.PictureBox pictLoad;
        private System.Windows.Forms.Label lblHoraExtraQtd;
        private System.Windows.Forms.Label lblHoraExtraUnit;
        private System.Windows.Forms.Label lblHoraExtraTot;
        private System.Windows.Forms.Label lblHoraExtra;
        private System.Windows.Forms.Button butIdioma;
        private System.Windows.Forms.Button butConfirmar;
        private System.Windows.Forms.Button butVoltar;
        private System.Windows.Forms.Label lblSaudacoes;
    }
}