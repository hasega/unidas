﻿// Aplicação: Unidas Totem
// Data     : 01/10/2015
// Versão   : 1.0
// Empresa  : Unidas Rent a Car
// Equipe   : Unidas Desenvolvimento
// Objetivo : Obtem digital do cliente
// Histórico: 
//      01/10/2015: Inicio de desenvolvimento para acesso geral
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using centralUnidas.centralUnidasWCF;
using System.IO;
using Veridis.Biometric;
using System.Xml;
using System.Resources;

namespace centralUnidas
{
    public partial class frmScreen03 : Form
    {
        Device deviceLocal = new Device();

        public ResXResourceSet ResxSetForm;

        animation animate;
        Graphics g;
        Graphics scG;
        System.Windows.Forms.Timer t;

        Bitmap btm;

        private void OnFrameChanged(object o, EventArgs e)
        {
            this.Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
        }

        public frmScreen03()
        {
            InitializeComponent();
        }

        private void frmScreen03_Load(object sender, EventArgs e)
        {

            LogTotem.InsertLog(new LogOperacao()
            {
                Application_Name = Globals.glbApplicationName,
                Application_Version = Globals.glbApplicationVersion,
                Local = Globals.glbLojaTotem,
                Operation_Ticks = Globals.glbOperationTicks,
                Operation_Name = "Validação Biometria",
                Form = "FrmScreen03",
                RESNUM = String.IsNullOrEmpty(Globals.glbReservaNumeroPesquisa) ? 0 : Convert.ToInt32(Globals.glbReservaNumeroPesquisa),
                RANUM = String.IsNullOrEmpty(Globals.glbRA_Contrato) ? 0 : Convert.ToInt32(Globals.glbRA_Contrato)
            });

            System.Threading.Thread.Sleep(1000);

            AtualizarInterfaceParaIdiomaCorrente();

            Globals.glbScreen = 3;
            int nScreenWidth = this.Width;
            int nScreenWidthSplit = nScreenWidth / 2;

            Globals.glbDebugMode = Convert.ToInt16(ConfigurationManager.AppSettings.Get("appDebugMode"));
            Shutdown_Enable();

            this.Refresh();

            g = this.CreateGraphics();

            btm = new Bitmap(this.Width, this.Height);
            scG = Graphics.FromImage(btm);

            t = new System.Windows.Forms.Timer();

            t.Interval = 1000;
            t.Tick += new EventHandler(t_Tick);

            animate = new animation(new Bitmap[] { centralUnidas.Properties.Resources.totem_biometria_img1, centralUnidas.Properties.Resources.totem_biometria_img3, centralUnidas.Properties.Resources.totem_biometria_img2 });
            t.Enabled = true;

            scG.Clear(Color.White);
            scG.DrawImage(animate.GiveNextImage(), new Point(640, 190));
            g.DrawImage(btm, Point.Empty);

            // Licença valida na Veridis - Devera ser alterada posteriormente para uma licença valida da Unidas
            VeridisLicense.InstallLicense("0000-0105-C68B-DF86-1924", "");

            try
            {
                BiometricCapture.StartSDK(null);
            }
            catch (Exception ex)
            {
            }
            tmrBiometria.Start();
        }

        private void Shutdown_Enable()
        {
            if (Globals.glbDebugMode == 0)
                pictShutdown.Visible = false;
            else
                pictShutdown.Visible = true;
        }

        private void pictShutdown_Click(object sender, EventArgs e)
        {
            t.Stop();
            System.Environment.Exit(1);
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            deviceLocal.Dispose();
            t.Stop();
            t.Enabled = false;

            timerDisabled();

            frmScreen01 frm2 = new frmScreen01();
            frm2.Show();
            this.Close();
            this.Dispose();
        }

        private void t_Tick(object sender, EventArgs e)
        {
            scG.Clear(Color.White);
            scG.DrawImage(animate.GiveNextImage(), new Point(640, 190));
            g.DrawImage(btm, Point.Empty);
            System.Threading.Thread.Sleep(800);
            Application.DoEvents();
        }

        private void tmrReturn_Tick(object sender, EventArgs e)
        {
            t.Stop();
            t.Enabled = false;
            tmrBiometria.Stop();
            tmrBiometria.Enabled = false;

            tmrReturn.Enabled = false;
            timerDisabled();

            frmScreen01 frm1 = new frmScreen01();
            frm1.Show();
            this.Close();
            this.Dispose();
        }

        public int CapturaBiometria()
        {
            int nRetornoBio = 0;

            while (deviceLocal.imagemCapture == null)
            {
                Application.DoEvents();
            }
            FingerprintImage.Image = deviceLocal.imagemCapture;

            CentralUnidasClassClient clientWcf = new CentralUnidasClassClient();

            string strNumeroReserva = Globals.glbConfID.Trim();

            byte[] imagemBio = imageToByteArray(deviceLocal.imagemCapture);

            string strCPF = "";
            if (Globals.glbRA_r_atipo.Trim() == "F")
            {
                strCPF = Globals.glbRESDOCCLI.Trim();
            }
            else
            {
                strCPF = Globals.glbRA_respaxcpf.Trim();
            }

            // AQUI 10.02.2016 - DEBUG PARA PERMITIR CAPTURAR BIOMETRIA E PROSSEGUIR COM O FLUXO
            // strCPF = "02145772871";
            strCPF = "00735340722";

            byte[] imgRetorno = clientWcf.GetBiometria(strCPF.Trim());
            clientWcf.Close();

            try
            {
                if (imgRetorno != null)
                {
                    BiometricSample bioSample = new BiometricSample(deviceLocal.imagemCapture, 500);
                    BiometricTemplate bioTemplateOriginal = new BiometricTemplate(bioSample);
                    BiometricTemplate bioTemplateGravado = new BiometricTemplate(imgRetorno);
                    ComparisonResult ret = bioTemplateGravado.CompareTo(bioTemplateOriginal);
                    if (ret.Match)
                    {
                        nRetornoBio = 1;
                    }
                    else
                        nRetornoBio = 2;
                }
                else
                    nRetornoBio = 2;
            }
            catch
            {
                nRetornoBio = 2;
            }
            return nRetornoBio;
        }

        public byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
            return ms.ToArray();
        }

        public Bitmap byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            Bitmap returnBitMap = (Bitmap)returnImage;
            return returnBitMap;
        }

        private void resetCLI()
        {
            Globals.glbCLI_CLIDOC = "";
            Globals.glbCLI_CLINOM = "";
            Globals.glbCLI_CLINOM_SAUDACOES = "";
            Globals.glbCLI_CPFENDRES = "";
            Globals.glbCLI_CPFCIDRES = "";
            Globals.glbCLI_CPFESTRES = "";
            Globals.glbCLI_CPFCEPRES = "";
            Globals.glbCLI_CPFTELRES = "";
            Globals.glbCLI_CPFCPF = "";
            Globals.glbCLI_CPFRG = "";
            Globals.glbCLI_CPFCARHAB = "";
            Globals.glbCLI_CPFESTHAB = "";
            Globals.glbCLI_CPFEXPHAB = "";
            Globals.glbCLI_CLIPRMHAB = "";
            Globals.glbCLI_TCLCOD = "";
            Globals.glbCLI_PRFTIP = "";
            Globals.glbCLI_PRFCOD = "";
            Globals.glbCLI_CPFPAS = "";
            Globals.glbCLI_CPFSEX = "";
            Globals.glbCLI_CPFDATNAS = "";
            Globals.glbCLI_CLICARDSC = "";
            Globals.glbCLI_PRASIG = "";
            Globals.glbCLI_CLICATEGO = "";
            Globals.glbCLI_CLIEMAIL = "";
        }

        private int obtemReservaXML_CliPre(string xmlEncodedList)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlEncodedList);
            XmlNodeList tabPRECPF = xmlDoc.GetElementsByTagName("PRECPF");
            if (tabPRECPF.Count > 0)
            {
                for (int x = 0; x < tabPRECPF.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabPRECPF.Item(x).InnerXml))
                    {
                        Globals.glbPRECPF = tabPRECPF.Item(x).InnerXml.Trim();
                    }
                }
            }
            XmlNodeList tabPRENOM = xmlDoc.GetElementsByTagName("PRENOM");
            if (tabPRENOM.Count > 0)
            {
                for (int x = 0; x < tabPRENOM.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabPRENOM.Item(x).InnerXml))
                    {
                        Globals.glbPRENOM = tabPRENOM.Item(x).InnerXml.Trim();
                    }
                }
            }
            return 1;
        }

        private int obtemReservaXML_Cli(string xmlEncodedList)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlEncodedList);

            XmlNodeList tabCLIDOC = xmlDoc.GetElementsByTagName("CLIDOC");
            if (tabCLIDOC.Count > 0)
            {
                for (int x = 0; x < tabCLIDOC.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabCLIDOC.Item(x).InnerXml))
                    {
                        Globals.glbCLI_CLIDOC = tabCLIDOC.Item(x).InnerXml.Trim();
                    }
                }
            }

            XmlNodeList tabCLINOM = xmlDoc.GetElementsByTagName("CLINOM");
            if (tabCLINOM.Count > 0)
            {
                for (int x = 0; x < tabCLINOM.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabCLINOM.Item(x).InnerXml))
                    {
                        Globals.glbCLI_CLINOM = tabCLINOM.Item(x).InnerXml.Trim();
                        if (string.IsNullOrEmpty(Globals.glbCLI_CLINOM_SAUDACOES))
                            Globals.glbCLI_CLINOM_SAUDACOES = tabCLINOM.Item(x).InnerXml.Trim();
                    }
                }
            }

            XmlNodeList tabCPFENDRES = xmlDoc.GetElementsByTagName("CPFENDRES");
            if (tabCPFENDRES.Count > 0)
            {
                for (int x = 0; x < tabCPFENDRES.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabCPFENDRES.Item(x).InnerXml))
                    {
                        Globals.glbCLI_CPFENDRES = tabCPFENDRES.Item(x).InnerXml.Trim();
                    }
                }
            }

            XmlNodeList tabCPFCIDRES = xmlDoc.GetElementsByTagName("CPFCIDRES");
            if (tabCPFCIDRES.Count > 0)
            {
                for (int x = 0; x < tabCPFCIDRES.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabCPFCIDRES.Item(x).InnerXml))
                    {
                        Globals.glbCLI_CPFCIDRES = tabCPFCIDRES.Item(x).InnerXml.Trim();
                    }
                }
            }

            XmlNodeList tabCPFESTRES = xmlDoc.GetElementsByTagName("CPFESTRES");
            if (tabCPFESTRES.Count > 0)
            {
                for (int x = 0; x < tabCPFESTRES.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabCPFESTRES.Item(x).InnerXml))
                    {
                        Globals.glbCLI_CPFESTRES = tabCPFESTRES.Item(x).InnerXml.Trim();
                    }
                }
            }

            XmlNodeList tabCPFCEPRES = xmlDoc.GetElementsByTagName("CPFCEPRES");
            if (tabCPFCEPRES.Count > 0)
            {
                for (int x = 0; x < tabCPFCEPRES.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabCPFCEPRES.Item(x).InnerXml))
                    {
                        Globals.glbCLI_CPFCEPRES = tabCPFCEPRES.Item(x).InnerXml.Trim();
                    }
                }
            }

            XmlNodeList tabCPFTELRES = xmlDoc.GetElementsByTagName("CPFTELRES");
            if (tabCPFTELRES.Count > 0)
            {
                for (int x = 0; x < tabCPFTELRES.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabCPFTELRES.Item(x).InnerXml))
                    {
                        Globals.glbCLI_CPFTELRES = tabCPFTELRES.Item(x).InnerXml.Trim();
                    }
                }
            }

            XmlNodeList tabCPFCPF = xmlDoc.GetElementsByTagName("CPFCPF");
            if (tabCPFCPF.Count > 0)
            {
                for (int x = 0; x < tabCPFCPF.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabCPFCPF.Item(x).InnerXml))
                    {
                        Globals.glbCLI_CPFCPF = tabCPFCPF.Item(x).InnerXml.Trim();
                    }
                }
            }

            XmlNodeList tabCPFRG = xmlDoc.GetElementsByTagName("CPFRG");
            if (tabCPFRG.Count > 0)
            {
                for (int x = 0; x < tabCPFRG.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabCPFRG.Item(x).InnerXml))
                    {
                        Globals.glbCLI_CPFRG = tabCPFRG.Item(x).InnerXml.Trim();
                    }
                }
            }

            XmlNodeList tabCPFCARHAB = xmlDoc.GetElementsByTagName("CPFCARHAB");
            if (tabCPFCARHAB.Count > 0)
            {
                for (int x = 0; x < tabCPFCARHAB.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabCPFCARHAB.Item(x).InnerXml))
                    {
                        Globals.glbCLI_CPFCARHAB = tabCPFCARHAB.Item(x).InnerXml.Trim();
                    }
                }
            }

            XmlNodeList tabCPFESTHAB = xmlDoc.GetElementsByTagName("CPFESTHAB");
            if (tabCPFESTHAB.Count > 0)
            {
                for (int x = 0; x < tabCPFESTHAB.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabCPFESTHAB.Item(x).InnerXml))
                    {
                        Globals.glbCLI_CPFESTHAB = tabCPFESTHAB.Item(x).InnerXml.Trim();
                    }
                }
            }

            XmlNodeList tabCPFEXPHAB = xmlDoc.GetElementsByTagName("CPFEXPHAB");
            if (tabCPFEXPHAB.Count > 0)
            {
                for (int x = 0; x < tabCPFEXPHAB.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabCPFEXPHAB.Item(x).InnerXml))
                    {
                        Globals.glbCLI_CPFEXPHAB = tabCPFEXPHAB.Item(x).InnerXml.Trim();
                    }
                }
            }

            XmlNodeList tabCLIPRMHAB = xmlDoc.GetElementsByTagName("CLIPRMHAB");
            if (tabCLIPRMHAB.Count > 0)
            {
                for (int x = 0; x < tabCLIPRMHAB.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabCLIPRMHAB.Item(x).InnerXml))
                    {
                        Globals.glbCLI_CLIPRMHAB = tabCLIPRMHAB.Item(x).InnerXml.Trim();
                    }
                }
            }

            #region Regra de Clitab
            XmlNodeList tabTCLCOD = xmlDoc.GetElementsByTagName("TCLCOD");
            if (tabTCLCOD.Count > 0)
            {
                for (int x = 0; x < tabTCLCOD.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabTCLCOD.Item(x).InnerXml))
                    {
                        Globals.glbCLI_TCLCOD = tabTCLCOD.Item(x).InnerXml.Trim();
                    }
                }
            }

            XmlNodeList tabPRFCOD = xmlDoc.GetElementsByTagName("PRFCOD");
            if (tabPRFCOD.Count > 0)
            {
                for (int x = 0; x < tabPRFCOD.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabPRFCOD.Item(x).InnerXml))
                    {
                        Globals.glbCLI_PRFCOD = tabPRFCOD.Item(x).InnerXml.Trim();
                    }
                }
            }
            #endregion

            XmlNodeList tabPRFTIP = xmlDoc.GetElementsByTagName("PRFTIP");
            if (tabPRFTIP.Count > 0)
            {
                for (int x = 0; x < tabPRFTIP.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabPRFTIP.Item(x).InnerXml))
                    {
                        Globals.glbCLI_PRFTIP = tabPRFTIP.Item(x).InnerXml.Trim();
                    }
                }
            }

            XmlNodeList tabCRECOD = xmlDoc.GetElementsByTagName("CRECOD");
            if (tabCRECOD.Count > 0)
            {
                for (int x = 0; x < tabCRECOD.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabCRECOD.Item(x).InnerXml))
                    {
                        Globals.glbCLI_CRECOD = tabCRECOD.Item(x).InnerXml.Trim();
                    }
                }
            }

            XmlNodeList tabCPFPAS = xmlDoc.GetElementsByTagName("CPFPAS");
            if (tabCPFPAS.Count > 0)
            {
                for (int x = 0; x < tabCPFPAS.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabCPFPAS.Item(x).InnerXml))
                    {
                        Globals.glbCLI_CPFPAS = tabCPFPAS.Item(x).InnerXml.Trim();
                    }
                }
            }

            XmlNodeList tabCPFSEX = xmlDoc.GetElementsByTagName("CPFSEX");
            if (tabCPFSEX.Count > 0)
            {
                for (int x = 0; x < tabCPFSEX.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabCPFSEX.Item(x).InnerXml))
                    {
                        Globals.glbCLI_CPFSEX = tabCPFSEX.Item(x).InnerXml.Trim();
                    }
                }
            }

            XmlNodeList tabCPFDATNAS = xmlDoc.GetElementsByTagName("CPFDATNAS");
            if (tabCPFDATNAS.Count > 0)
            {
                for (int x = 0; x < tabCPFDATNAS.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabCPFDATNAS.Item(x).InnerXml))
                    {
                        Globals.glbCLI_CPFDATNAS = tabCPFDATNAS.Item(x).InnerXml.Trim();
                    }
                }
            }

            XmlNodeList tabCLICARDSC = xmlDoc.GetElementsByTagName("CLICARDSC");
            if (tabCLICARDSC.Count > 0)
            {
                for (int x = 0; x < tabCLICARDSC.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabCLICARDSC.Item(x).InnerXml))
                    {
                        Globals.glbCLI_CLICARDSC = tabCLICARDSC.Item(x).InnerXml.Trim();
                    }
                }
            }

            XmlNodeList tabPRASIG = xmlDoc.GetElementsByTagName("PRASIG");
            if (tabPRASIG.Count > 0)
            {
                for (int x = 0; x < tabPRASIG.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabPRASIG.Item(x).InnerXml))
                    {
                        Globals.glbCLI_PRASIG = tabPRASIG.Item(x).InnerXml.Trim();
                    }
                }
            }

            XmlNodeList tabCLICATEGO = xmlDoc.GetElementsByTagName("CLICATEGO");
            if (tabCLICATEGO.Count > 0)
            {
                for (int x = 0; x < tabCLICATEGO.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabCLICATEGO.Item(x).InnerXml))
                    {
                        Globals.glbCLI_CLICATEGO = tabCLICATEGO.Item(x).InnerXml.Trim();
                    }
                }
            }

            XmlNodeList tabCLIEMAIL = xmlDoc.GetElementsByTagName("CLIEMAIL");
            if (tabCLIEMAIL.Count > 0)
            {
                for (int x = 0; x < tabCLIEMAIL.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabCLIEMAIL.Item(x).InnerXml))
                    {
                        Globals.glbCLI_CLIEMAIL = tabCLIEMAIL.Item(x).InnerXml.Trim();
                    }
                }
            }

            XmlNodeList tabCLIR_ANUM = xmlDoc.GetElementsByTagName("R_ANUM");
            if (tabCLIR_ANUM.Count > 0)
            {
                for (int x = 0; x < tabCLIR_ANUM.Count; x++)
                {
                    if (!string.IsNullOrEmpty(tabCLIR_ANUM.Item(x).InnerXml))
                    {
                        Globals.glbCLI_R_ANUM = tabCLIR_ANUM.Item(x).InnerXml.ToString().Trim();
                    }
                }
            }

            return 1;
        }

        private void tmrBiometria_Tick(object sender, EventArgs e)
        {
            tmrBiometria.Stop();
            tmrBiometria.Enabled = false;
            int retornaBio = 0;
            while (true)
            {
                if (Globals.glbVezes > 2)
                {
                    Globals.glbVezes = 0;
                    retornaBio = 2;
                    break;
                }

                retornaBio = CapturaBiometria();

                // retornaBio = 1; // realizando biometria

                if (retornaBio == 1)
                {
                    Globals.glbVezes = 0;
                    break;
                }
                else
                {
                    Globals.glbVezes = Globals.glbVezes + 1;
                    t.Stop();
                    t.Enabled = false;
                    if (Globals.glbVezes > 2)
                    {
                        Globals.glbVezes = 0;
                        retornaBio = 2;
                        break;
                    }
                    tmrBiometria.Stop();
                    tmrBiometria.Enabled = false;
                    
                    timerDisabled();

                    Globals.glbMensagem = 0301;
                    Globals.glbScreen = 3;
                    frmMensagem frmMsg = new frmMensagem();
                    frmMsg.Show();
                    this.Dispose();
                    return;
                }
            }

            if (retornaBio == 2)
            {
                t.Stop();
                t.Enabled = false;

                tmrBiometria.Stop();
                tmrBiometria.Enabled = false;

                timerDisabled();

                Globals.glbMensagem = 0302;
                Globals.glbScreen = 1;
                frmMensagem frmMsg = new frmMensagem();
                frmMsg.Show();
                this.Close();
                this.Dispose();
                return;
            }
            if (retornaBio == 1)
            {
                t.Stop();
                t.Enabled = false;

                tmrBiometria.Stop();
                tmrBiometria.Enabled = false;
                resetCLI();

                // Obtem informacao do cliente PF
                try
                {
                    string xmlRetornoWCF = "";
                    CentralUnidasClassClient clientWcf = new CentralUnidasClassClient();
                    if (Globals.glbRA_r_atipo.Trim() == "F")
                    {
                        xmlRetornoWCF = clientWcf.GetClienteDoc(Globals.glbRESDOCCLI.Trim());
                    }
                    else
                    {
                        xmlRetornoWCF = clientWcf.GetClienteDoc(Globals.glbRA_respaxcpf.Trim());
                    }
                    clientWcf.Close();
                    int nCli = obtemReservaXML_Cli(xmlRetornoWCF.Trim());
                }
                catch (Exception ex)
                {
                    timerDisabled();

                    Globals.glbScreen = 1;
                    Globals.glbMensagem = 1001;
                    frmMensagem frmMsg = new frmMensagem();
                    frmMsg.Show();
                    this.Close();
                    this.Dispose();
                    return;
                }

                if (!string.IsNullOrEmpty(Globals.glbCLI_R_ANUM))
                {
                    // AQUI 10.02.2016 DEBUG: ALTERADO PARA EXIBIR O RESULTADO
                    if (Convert.ToInt32(Globals.glbCLI_R_ANUM) > 0)
                    {
                        timerDisabled();

                        Globals.glbScreen = 1;
                        Globals.glbMensagem = 1014;
                        frmMensagem frmMsg = new frmMensagem();
                        frmMsg.Show();
                        this.Close();
                        this.Dispose();
                        return;
                    }
                }

                // Obtem informacao do cliente PJ
                try
                {
                    string xmlRetornoWCF = "";
                    CentralUnidasClassClient clientWcf = new CentralUnidasClassClient();
                    xmlRetornoWCF = clientWcf.GetClienteDoc(Globals.glbRESDOCCLI.Trim());
                    clientWcf.Close();
                    int nCli = obtemReservaXML_Cli(xmlRetornoWCF.Trim());
                }
                catch (Exception ex)
                {
                    timerDisabled();

                    Globals.glbScreen = 1;
                    Globals.glbMensagem = 1001;
                    frmMensagem frmMsg = new frmMensagem();
                    frmMsg.Show();
                    this.Close();
                    this.Dispose();
                    return;
                }

                if (string.IsNullOrEmpty(Globals.glbCLI_TCLCOD))
                {
                    timerDisabled();

                    Globals.glbScreen = 1;
                    Globals.glbMensagem = 1004;
                    frmMensagem frmMsg = new frmMensagem();
                    frmMsg.Show();
                    this.Close();
                    this.Dispose();
                    return;
                }



                // Tranto Agencia 3 Partes
                if (Globals.glbRA_r_atipo.Trim() == "A"
                    //&& Convert.ToInt32(Globals.glbCLI_PRFCOD) != 336 
                    && !String.IsNullOrEmpty(Globals.glbRA_clidoc)
                    && !String.IsNullOrEmpty(Globals.glbRA_clinom))
                {

                    if (!string.IsNullOrEmpty(Globals.glbRA_respaxcpf.Trim()))
                    {
                        try
                        {
                            string xmlRetornoCliPreWCF = "";
                            CentralUnidasClassClient clientPreWcf = new CentralUnidasClassClient();
                            xmlRetornoCliPreWCF = clientPreWcf.GetPrepostoTotemData(Globals.glbRA_clidoc.Trim(), Globals.glbRA_respaxcpf.Trim());
                            clientPreWcf.Close();
                            int nCliPre = obtemReservaXML_CliPre(xmlRetornoCliPreWCF.Trim());
                        }
                        catch (Exception ex)
                        {
                            timerDisabled();

                            Globals.glbScreen = 1;
                            Globals.glbMensagem = 1001;
                            frmMensagem frmMsg = new frmMensagem();
                            frmMsg.Show();
                            this.Close();
                            this.Dispose();
                            return;
                        }
                        if (Globals.glbRA_respaxcpf.Trim() != Globals.glbPRECPF.Trim())
                        {
                            timerDisabled();

                            Globals.glbScreen = 1;
                            Globals.glbMensagem = 1022;
                            frmMensagem frmMsg = new frmMensagem();
                            frmMsg.Show();
                            this.Close();
                            this.Dispose();
                            return;
                        }
                    }
                    else
                    {
                        if (Globals.glbRA_respaxcpf.Trim() != Globals.glbPRECPF.Trim())
                        {
                            timerDisabled();

                            Globals.glbScreen = 1;
                            Globals.glbMensagem = 1022;
                            frmMensagem frmMsg = new frmMensagem();
                            frmMsg.Show();
                            this.Close();
                            this.Dispose();
                            return;
                        }
                    }
                    // SJO
                    //if (!string.IsNullOrEmpty(Globals.glbRESDOCCLI))
                    //{
                    //    Globals.glbRA_clidoc = Globals.loc
                    //}
                    //if (!string.IsNullOrEmpty(Globals.glbRESNOM))
                    //{
                    //    Globals.glbRA_clinom = Globals.glbRESNOM.Trim();
                    //}
                }

                #region Tratamento PJ Normal
                // Trata Preposto PJ Normal
                if (Globals.glbRA_r_atipo.Trim() == "J"
                && Convert.ToInt32(Globals.glbCLI_PRFCOD) != 336)
                {

                    if (!string.IsNullOrEmpty(Globals.glbRA_respaxcpf.Trim()))
                    {
                        try
                        {
                            string xmlRetornoCliPreWCF = "";
                            CentralUnidasClassClient clientPreWcf = new CentralUnidasClassClient();
                            xmlRetornoCliPreWCF = clientPreWcf.GetPrepostoTotemData(Globals.glbRA_clifat.Trim(), Globals.glbRA_respaxcpf.Trim());
                            clientPreWcf.Close();
                            int nCliPre = obtemReservaXML_CliPre(xmlRetornoCliPreWCF.Trim());
                        }
                        catch (Exception ex)
                        {

                            timerDisabled();

                            Globals.glbScreen = 1;
                            Globals.glbMensagem = 1001;
                            frmMensagem frmMsg = new frmMensagem();
                            frmMsg.Show();
                            this.Close();
                            this.Dispose();
                            return;
                        }
                        if (Globals.glbRA_respaxcpf.Trim() != Globals.glbPRECPF.Trim())
                        {
                            timerDisabled();

                            Globals.glbScreen = 1;
                            Globals.glbMensagem = 1022;
                            frmMensagem frmMsg = new frmMensagem();
                            frmMsg.Show();
                            this.Close();
                            this.Dispose();
                            return;
                        }
                    }
                    else
                    {
                        if (Globals.glbRA_respaxcpf.Trim() != Globals.glbPRECPF.Trim())
                        {
                            timerDisabled();

                            Globals.glbScreen = 1;
                            Globals.glbMensagem = 1022;
                            frmMensagem frmMsg = new frmMensagem();
                            frmMsg.Show();
                            this.Close();
                            this.Dispose();
                            return;
                        }
                    }

                    if (!string.IsNullOrEmpty(Globals.glbRESDOCCLI))
                    {
                        Globals.glbRA_clidoc = Globals.glbRESDOCCLI.Trim();
                    }
                    if (!string.IsNullOrEmpty(Globals.glbRESNOM))
                    {
                        Globals.glbRA_clinom = Globals.glbRESNOM.Trim();
                    }
                }

                #endregion Tratamento PJ Normal

                #region Tratamento PJ Seguradora
                // Trata Preposto PJ Normal
                if (Globals.glbRA_r_atipo.Trim() == "J" && Convert.ToInt32(Globals.glbCLI_PRFCOD) == 336)
                {
                    if (!string.IsNullOrEmpty(Globals.glbRA_respaxcpf.Trim()))
                    {
                        if (!string.IsNullOrEmpty(Globals.glbRA_respaxcpf))
                        {
                            Globals.glbRA_clidoc = Globals.glbRA_respaxcpf.Trim();
                        }
                        if (!string.IsNullOrEmpty(Globals.glbRA_respax))
                        {
                            if (!string.IsNullOrEmpty(Globals.glbCLI_CLINOM_SAUDACOES))
                                Globals.glbRA_clinom = Globals.glbCLI_CLINOM_SAUDACOES.Trim();
                            else
                                Globals.glbRA_clinom = Globals.glbRA_respax.Trim();
                        }
                    }
                }

                #endregion Tratamento PJ Normal

                //#region Busca informações da tabela CLIPRE. Apenas para pessoa juridica ou agência
                //if (Globals.glbRA_r_atipo.Trim() == "A" || Globals.glbRA_r_atipo.Trim() == "J")
                //{
                //    if (!string.IsNullOrEmpty(Globals.glbRA_respaxcpf.Trim()))
                //    {
                //        try
                //        {
                //            string xmlRetornoCliPreWCF = "";
                //            CentralUnidasClassClient clientPreWcf = new CentralUnidasClassClient();
                //            xmlRetornoCliPreWCF = clientPreWcf.GetPrepostoTotemData(Globals.glbRA_clidoc.Trim(), Globals.glbRA_respaxcpf.Trim());
                //            clientPreWcf.Close();
                //            int nCliPre = obtemReservaXML_CliPre(xmlRetornoCliPreWCF.Trim());
                //        }
                //        catch (Exception ex)
                //        {
                //            Globals.glbScreen = 1;
                //            Globals.glbMensagem = 1001;
                //            frmMensagem frmMsg = new frmMensagem();
                //            frmMsg.Show();
                //            this.Close();
                //            this.Dispose();
                //            return;
                //        }
                //        if (Globals.glbRA_respaxcpf.Trim() != Globals.glbPRECPF.Trim())
                //        {
                //            Globals.glbScreen = 1;
                //            Globals.glbMensagem = 1022;
                //            frmMensagem frmMsg = new frmMensagem();
                //            frmMsg.Show();
                //            this.Close();
                //            this.Dispose();
                //            return;
                //        }
                //    }
                //    else
                //    {
                //        if (Globals.glbRA_respaxcpf.Trim() != Globals.glbPRECPF.Trim())
                //        {
                //            Globals.glbScreen = 1;
                //            Globals.glbMensagem = 1022;
                //            frmMensagem frmMsg = new frmMensagem();
                //            frmMsg.Show();
                //            this.Close();
                //            this.Dispose();
                //            return;
                //        }
                //    }
                //}
                //#endregion

                // SJO
                //if (Convert.ToInt32(Globals.glbCLI_TCLCOD) == 2)
                //{
                //    if (Convert.ToInt32(Globals.glbCLI_PRFCOD) != 336)
                //    {


                //        //Globals.glbPRECPF = Globals.glbRA_respaxcpf.Trim();

                //        if (!string.IsNullOrEmpty(Globals.glbRA_respaxcpf.Trim()))
                //        {
                //            try
                //            {
                //                string xmlRetornoCliPreWCF = "";
                //                CentralUnidasClassClient clientPreWcf = new CentralUnidasClassClient();
                //                xmlRetornoCliPreWCF = clientPreWcf.GetPrepostoTotemData(Globals.glbRA_clidoc.Trim(), Globals.glbRA_respaxcpf.Trim());
                //                clientPreWcf.Close();
                //                int nCliPre = obtemReservaXML_CliPre(xmlRetornoCliPreWCF.Trim());

                //            }
                //            catch (Exception ex)
                //            {
                //                Globals.glbScreen = 1;
                //                Globals.glbMensagem = 1001;
                //                frmMensagem frmMsg = new frmMensagem();
                //                frmMsg.Show();
                //                this.Close();
                //                this.Dispose();
                //                return;
                //            }
                //            if (Globals.glbRA_respaxcpf.Trim() != Globals.glbPRECPF.Trim())
                //            {
                //                Globals.glbScreen = 1;
                //                Globals.glbMensagem = 1022;
                //                frmMensagem frmMsg = new frmMensagem();
                //                frmMsg.Show();
                //                this.Close();
                //                this.Dispose();
                //                return;
                //            }
                //        }
                //        else
                //        {

                //            Globals.glbScreen = 1;
                //            Globals.glbMensagem = 1022;
                //            frmMensagem frmMsg = new frmMensagem();
                //            frmMsg.Show();
                //            this.Close();
                //            this.Dispose();
                //            return;
                //        }
                //    }
                //    //PJ  (SEGURADORA)
                //    else if (Convert.ToInt32(Globals.glbCLI_PRFCOD) == 336)
                //    {
                //        if (!string.IsNullOrEmpty(Globals.glbRA_respaxcpf))
                //        {
                //            Globals.glbRA_clidoc = Globals.glbRA_respaxcpf.Trim();
                //        }
                //        if (!string.IsNullOrEmpty(Globals.glbRA_respax))
                //        {
                //            Globals.glbRA_clinom = Globals.glbRA_respax.Trim();
                //        }
                //    }
                //}

                // Agência
                if (Convert.ToInt32(Globals.glbCLI_TCLCOD) == 3 && String.IsNullOrEmpty(Globals.glbRA_clidoc) && String.IsNullOrEmpty(Globals.glbRA_clinom))
                {
                    if (!string.IsNullOrEmpty(Globals.glbRA_respaxcpf))
                    {
                        Globals.glbRA_clidoc = Globals.glbRA_respaxcpf.Trim();
                    }
                    if (!string.IsNullOrEmpty(Globals.glbRA_respax))
                    {
                        Globals.glbRA_clinom = Globals.glbRA_respax.Trim();
                    }
                }

                // PF e PJ
                if (Convert.ToInt32(Globals.glbCLI_TCLCOD) == 1 || Convert.ToInt32(Globals.glbCLI_TCLCOD) == 6)
                {
                    if (string.IsNullOrEmpty(Globals.glbCLI_CPFENDRES))
                    {
                        timerDisabled();

                        Globals.glbScreen = 1;
                        Globals.glbMensagem = 1004;
                        frmMensagem frmMsg = new frmMensagem();
                        frmMsg.Show();
                        this.Close();
                        this.Dispose();
                        return;
                    }
                    if (string.IsNullOrEmpty(Globals.glbCLI_CPFCIDRES))
                    {
                        timerDisabled();

                        Globals.glbScreen = 1;
                        Globals.glbMensagem = 1004;
                        frmMensagem frmMsg = new frmMensagem();
                        frmMsg.Show();
                        this.Close();
                        this.Dispose();
                        return;
                    }
                    if (string.IsNullOrEmpty(Globals.glbCLI_CPFCEPRES))
                    {
                        timerDisabled();

                        Globals.glbScreen = 1;
                        Globals.glbMensagem = 1004;
                        frmMensagem frmMsg = new frmMensagem();
                        frmMsg.Show();
                        this.Close();
                        this.Dispose();
                        return;
                    }
                    if (string.IsNullOrEmpty(Globals.glbCLI_CPFESTRES))
                    {
                        timerDisabled();

                        Globals.glbScreen = 1;
                        Globals.glbMensagem = 1004;
                        frmMensagem frmMsg = new frmMensagem();
                        frmMsg.Show();
                        this.Close();
                        this.Dispose();
                        return;
                    }
                    if ((Convert.ToInt32(Globals.glbCLI_CRECOD) > 2) && (Convert.ToInt32(Globals.glbCLI_CRECOD) < 22))
                    {
                        timerDisabled();

                        Globals.glbScreen = 1;
                        Globals.glbMensagem = 1001;
                        frmMensagem frmMsg = new frmMensagem();
                        frmMsg.Show();
                        this.Close();
                        this.Dispose();
                        return;
                    }

                    DateTime dtHabilitacao = Convert.ToDateTime(Globals.glbCLI_CPFEXPHAB);
                    DateTime dtReturnDateTime = Convert.ToDateTime(Globals.glbReturnDateTime);
                    if ((dtHabilitacao.AddDays(30)) < dtReturnDateTime)
                    {
                        timerDisabled();

                        Globals.glbScreen = 1;
                        Globals.glbMensagem = 1005;
                        frmMensagem frmMsg = new frmMensagem();
                        frmMsg.Show();
                        this.Close();
                        this.Dispose();
                        return;
                    }

                    DateTime zeroTime = new DateTime(1, 1, 1);
                    DateTime dtPrimeiraHabilitacao = Convert.ToDateTime(Globals.glbCLI_CLIPRMHAB);
                    DateTime dtAtual = DateTime.Now;

                    TimeSpan span = dtAtual - dtPrimeiraHabilitacao;

                    int nYears = (zeroTime + span).Year - 1;
                    if (nYears < 2)
                    {
                        timerDisabled();

                        Globals.glbScreen = 1;
                        Globals.glbMensagem = 1013;
                        frmMensagem frmMsg = new frmMensagem();
                        frmMsg.Show();
                        this.Close();
                        this.Dispose();
                        return;
                    }


                    // PJ e PF 
                    if (!string.IsNullOrEmpty(Globals.glbRESDOCCLI))
                    {
                        Globals.glbRA_clidoc = Globals.glbRESDOCCLI.Trim();
                    }
                    if (!string.IsNullOrEmpty(Globals.glbRESNOM))
                    {
                        Globals.glbRA_clinom = Globals.glbRESNOM.Trim();
                    }

                    //if ((dtPrimeiraHabilitacao.AddDays(30)) < dtReturnDateTime)
                    //{
                    //    Globals.glbScreen = 1;
                    //    Globals.glbMensagem = 1005;
                    //    frmMensagem frmMsg = new frmMensagem();
                    //    frmMsg.Show();
                    //    this.Dispose();
                    //    return;
                    //}
                }
                timerDisabled();

                frmScreen05 frm5 = new frmScreen05();
                frm5.Show();
                this.Close();
                this.Dispose();
                return;
            }
        }

        private void AtualizarInterfaceParaIdiomaCorrente()
        {

            MontarResourceSetParaIdiomaCorrente();


            butIdioma.Text = ResxSetForm.GetString("butIdioma.Text");
            butVoltar.Text = ResxSetForm.GetString("butVoltar.Text");
            lblBiometria.Text = ResxSetForm.GetString("lblBiometria.Text");

            this.Icon = ((System.Drawing.Icon)(ResxSetForm.GetObject("$this.Icon")));

            if (Globals.glbLanguage == 1) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_Brasil_botao;
            if (Globals.glbLanguage == 2) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_Espanha_botao;
            if (Globals.glbLanguage == 3) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_UK_botao;

            butIdioma.Refresh();
        }

        private void MontarResourceSetParaIdiomaCorrente()
        {
            string srtCaminhoExeTotem = System.IO.Path.GetDirectoryName(Application.ExecutablePath.ToString());
            // Português
            if (Globals.glbLanguage == 1)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen03.pt.resx");
            }
            //Espanhol
            else if (Globals.glbLanguage == 2)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen03.es.resx");
            }
            // Inglês
            else if (Globals.glbLanguage == 3)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen03.en.resx");
            }
        }

        private void butIdioma_Click(object sender, EventArgs e)
        {
            frmIdioma frmIdi = new frmIdioma();
            frmIdi.ShowDialog();
            AtualizarInterfaceParaIdiomaCorrente();
        }

        private void butVoltar_Click(object sender, EventArgs e)
        {
            deviceLocal.Dispose();
            t.Stop();
            t.Enabled = false;

            timerDisabled();

            frmScreen01 frm2 = new frmScreen01();
            frm2.Show();
            this.Close();
            this.Dispose();
        }

        private void timerDisabled()
        {
            tmrReturn.Enabled = false;
            tmrBiometria.Enabled = false;
        }

    }
}
