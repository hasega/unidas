﻿namespace centralUnidas
{
    partial class frmScreen02
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmScreen02));
            this.pictUnidasLogo = new System.Windows.Forms.PictureBox();
            this.txtReserva1 = new System.Windows.Forms.TextBox();
            this.pictShutdown = new System.Windows.Forms.PictureBox();
            this.tmrReserva = new System.Windows.Forms.Timer(this.components);
            this.pictLoad = new System.Windows.Forms.PictureBox();
            this.btnContinuar = new System.Windows.Forms.Button();
            this.pictTecladoFundo = new System.Windows.Forms.PictureBox();
            this.pictTec_1 = new System.Windows.Forms.PictureBox();
            this.pictTec_2 = new System.Windows.Forms.PictureBox();
            this.pictTec_3 = new System.Windows.Forms.PictureBox();
            this.pictTec_4 = new System.Windows.Forms.PictureBox();
            this.pictTec_5 = new System.Windows.Forms.PictureBox();
            this.pictTec_6 = new System.Windows.Forms.PictureBox();
            this.pictTec_9 = new System.Windows.Forms.PictureBox();
            this.pictTec_8 = new System.Windows.Forms.PictureBox();
            this.pictTec_7 = new System.Windows.Forms.PictureBox();
            this.pictTec_0 = new System.Windows.Forms.PictureBox();
            this.pictTec_TECLADO = new System.Windows.Forms.PictureBox();
            this.pictTec_BACK2 = new System.Windows.Forms.PictureBox();
            this.tmrTeclado = new System.Windows.Forms.Timer(this.components);
            this.tmrTecladoShow = new System.Windows.Forms.Timer(this.components);
            this.pictTecladoShow = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tmrReturn = new System.Windows.Forms.Timer(this.components);
            this.lblInformeDadasAbaixo = new System.Windows.Forms.Label();
            this.lblNumeroReserva = new System.Windows.Forms.Label();
            this.lblVersao = new System.Windows.Forms.Label();
            this.butIdioma = new System.Windows.Forms.Button();
            this.lblLoja = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictShutdown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictLoad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTecladoFundo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_TECLADO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_BACK2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTecladoShow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // pictUnidasLogo
            // 
            this.pictUnidasLogo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictUnidasLogo.BackColor = System.Drawing.Color.Transparent;
            this.pictUnidasLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictUnidasLogo.Image = global::centralUnidas.Properties.Resources.totem_header;
            this.pictUnidasLogo.Location = new System.Drawing.Point(0, 0);
            this.pictUnidasLogo.Name = "pictUnidasLogo";
            this.pictUnidasLogo.Size = new System.Drawing.Size(1374, 140);
            this.pictUnidasLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictUnidasLogo.TabIndex = 6;
            this.pictUnidasLogo.TabStop = false;
            // 
            // txtReserva1
            // 
            this.txtReserva1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReserva1.BackColor = System.Drawing.Color.White;
            this.txtReserva1.Font = new System.Drawing.Font("Arial", 28F);
            this.txtReserva1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(109)))), ((int)(((byte)(175)))));
            this.txtReserva1.Location = new System.Drawing.Point(598, 262);
            this.txtReserva1.MaxLength = 1;
            this.txtReserva1.Name = "txtReserva1";
            this.txtReserva1.Size = new System.Drawing.Size(276, 50);
            this.txtReserva1.TabIndex = 57;
            this.txtReserva1.WordWrap = false;
            this.txtReserva1.TextChanged += new System.EventHandler(this.txtReserva1_TextChanged);
            this.txtReserva1.Enter += new System.EventHandler(this.txtReserva1_Enter);
            // 
            // pictShutdown
            // 
            this.pictShutdown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictShutdown.BackColor = System.Drawing.Color.Transparent;
            this.pictShutdown.Image = global::centralUnidas.Properties.Resources.shutdown_button;
            this.pictShutdown.Location = new System.Drawing.Point(12, 156);
            this.pictShutdown.Name = "pictShutdown";
            this.pictShutdown.Size = new System.Drawing.Size(50, 55);
            this.pictShutdown.TabIndex = 79;
            this.pictShutdown.TabStop = false;
            this.pictShutdown.Visible = false;
            this.pictShutdown.Click += new System.EventHandler(this.pictShutdown_Click);
            // 
            // tmrReserva
            // 
            this.tmrReserva.Interval = 1000;
            this.tmrReserva.Tick += new System.EventHandler(this.tmrReserva_Tick);
            // 
            // pictLoad
            // 
            this.pictLoad.Image = global::centralUnidas.Properties.Resources.Loading;
            this.pictLoad.Location = new System.Drawing.Point(997, 262);
            this.pictLoad.Name = "pictLoad";
            this.pictLoad.Size = new System.Drawing.Size(55, 50);
            this.pictLoad.TabIndex = 0;
            this.pictLoad.TabStop = false;
            this.pictLoad.Visible = false;
            // 
            // btnContinuar
            // 
            this.btnContinuar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnContinuar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.btnContinuar.Font = new System.Drawing.Font("Arial", 16.5F, System.Drawing.FontStyle.Bold);
            this.btnContinuar.ForeColor = System.Drawing.Color.White;
            this.btnContinuar.Location = new System.Drawing.Point(634, 331);
            this.btnContinuar.Name = "btnContinuar";
            this.btnContinuar.Size = new System.Drawing.Size(211, 64);
            this.btnContinuar.TabIndex = 80;
            this.btnContinuar.Text = "Continuar";
            this.btnContinuar.UseVisualStyleBackColor = false;
            this.btnContinuar.Click += new System.EventHandler(this.btnContinuar_Click);
            // 
            // pictTecladoFundo
            // 
            this.pictTecladoFundo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictTecladoFundo.BackColor = System.Drawing.Color.Transparent;
            this.pictTecladoFundo.BackgroundImage = global::centralUnidas.Properties.Resources.totem_teclado_fundo;
            this.pictTecladoFundo.Location = new System.Drawing.Point(0, 433);
            this.pictTecladoFundo.Name = "pictTecladoFundo";
            this.pictTecladoFundo.Size = new System.Drawing.Size(1374, 335);
            this.pictTecladoFundo.TabIndex = 83;
            this.pictTecladoFundo.TabStop = false;
            // 
            // pictTec_1
            // 
            this.pictTec_1.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_1.Image = global::centralUnidas.Properties.Resources.totem_1;
            this.pictTec_1.Location = new System.Drawing.Point(602, 448);
            this.pictTec_1.Name = "pictTec_1";
            this.pictTec_1.Size = new System.Drawing.Size(76, 73);
            this.pictTec_1.TabIndex = 95;
            this.pictTec_1.TabStop = false;
            this.pictTec_1.Click += new System.EventHandler(this.pictTec_1_Click);
            // 
            // pictTec_2
            // 
            this.pictTec_2.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_2.Image = global::centralUnidas.Properties.Resources.totem_2;
            this.pictTec_2.Location = new System.Drawing.Point(684, 448);
            this.pictTec_2.Name = "pictTec_2";
            this.pictTec_2.Size = new System.Drawing.Size(76, 73);
            this.pictTec_2.TabIndex = 96;
            this.pictTec_2.TabStop = false;
            this.pictTec_2.Click += new System.EventHandler(this.pictTec_2_Click);
            // 
            // pictTec_3
            // 
            this.pictTec_3.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_3.Image = global::centralUnidas.Properties.Resources.totem_3;
            this.pictTec_3.Location = new System.Drawing.Point(766, 448);
            this.pictTec_3.Name = "pictTec_3";
            this.pictTec_3.Size = new System.Drawing.Size(76, 73);
            this.pictTec_3.TabIndex = 97;
            this.pictTec_3.TabStop = false;
            this.pictTec_3.Click += new System.EventHandler(this.pictTec_3_Click);
            // 
            // pictTec_4
            // 
            this.pictTec_4.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_4.Image = global::centralUnidas.Properties.Resources.totem_4;
            this.pictTec_4.Location = new System.Drawing.Point(602, 527);
            this.pictTec_4.Name = "pictTec_4";
            this.pictTec_4.Size = new System.Drawing.Size(76, 73);
            this.pictTec_4.TabIndex = 107;
            this.pictTec_4.TabStop = false;
            this.pictTec_4.Click += new System.EventHandler(this.pictTec_4_Click);
            // 
            // pictTec_5
            // 
            this.pictTec_5.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_5.Image = global::centralUnidas.Properties.Resources.totem_5;
            this.pictTec_5.Location = new System.Drawing.Point(684, 527);
            this.pictTec_5.Name = "pictTec_5";
            this.pictTec_5.Size = new System.Drawing.Size(76, 73);
            this.pictTec_5.TabIndex = 108;
            this.pictTec_5.TabStop = false;
            this.pictTec_5.Click += new System.EventHandler(this.pictTec_5_Click);
            // 
            // pictTec_6
            // 
            this.pictTec_6.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_6.Image = global::centralUnidas.Properties.Resources.totem_6;
            this.pictTec_6.Location = new System.Drawing.Point(766, 527);
            this.pictTec_6.Name = "pictTec_6";
            this.pictTec_6.Size = new System.Drawing.Size(76, 73);
            this.pictTec_6.TabIndex = 109;
            this.pictTec_6.TabStop = false;
            this.pictTec_6.Click += new System.EventHandler(this.pictTec_6_Click);
            // 
            // pictTec_9
            // 
            this.pictTec_9.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_9.Image = global::centralUnidas.Properties.Resources.totem_9;
            this.pictTec_9.Location = new System.Drawing.Point(766, 606);
            this.pictTec_9.Name = "pictTec_9";
            this.pictTec_9.Size = new System.Drawing.Size(76, 73);
            this.pictTec_9.TabIndex = 121;
            this.pictTec_9.TabStop = false;
            this.pictTec_9.Click += new System.EventHandler(this.pictTec_9_Click);
            // 
            // pictTec_8
            // 
            this.pictTec_8.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_8.Image = global::centralUnidas.Properties.Resources.totem_8;
            this.pictTec_8.Location = new System.Drawing.Point(684, 606);
            this.pictTec_8.Name = "pictTec_8";
            this.pictTec_8.Size = new System.Drawing.Size(76, 73);
            this.pictTec_8.TabIndex = 120;
            this.pictTec_8.TabStop = false;
            this.pictTec_8.Click += new System.EventHandler(this.pictTec_8_Click);
            // 
            // pictTec_7
            // 
            this.pictTec_7.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_7.Image = global::centralUnidas.Properties.Resources.totem_7;
            this.pictTec_7.Location = new System.Drawing.Point(602, 606);
            this.pictTec_7.Name = "pictTec_7";
            this.pictTec_7.Size = new System.Drawing.Size(76, 73);
            this.pictTec_7.TabIndex = 119;
            this.pictTec_7.TabStop = false;
            this.pictTec_7.Click += new System.EventHandler(this.pictTec_7_Click);
            // 
            // pictTec_0
            // 
            this.pictTec_0.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_0.Image = global::centralUnidas.Properties.Resources.totem_0;
            this.pictTec_0.Location = new System.Drawing.Point(684, 685);
            this.pictTec_0.Name = "pictTec_0";
            this.pictTec_0.Size = new System.Drawing.Size(76, 73);
            this.pictTec_0.TabIndex = 122;
            this.pictTec_0.TabStop = false;
            this.pictTec_0.Click += new System.EventHandler(this.pictTec_0_Click);
            // 
            // pictTec_TECLADO
            // 
            this.pictTec_TECLADO.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_TECLADO.Image = global::centralUnidas.Properties.Resources.totem_teclado;
            this.pictTec_TECLADO.Location = new System.Drawing.Point(602, 685);
            this.pictTec_TECLADO.Name = "pictTec_TECLADO";
            this.pictTec_TECLADO.Size = new System.Drawing.Size(76, 73);
            this.pictTec_TECLADO.TabIndex = 125;
            this.pictTec_TECLADO.TabStop = false;
            this.pictTec_TECLADO.Click += new System.EventHandler(this.pictTec_TECLADO_Click);
            // 
            // pictTec_BACK2
            // 
            this.pictTec_BACK2.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_BACK2.Image = global::centralUnidas.Properties.Resources.totem_Backspace;
            this.pictTec_BACK2.Location = new System.Drawing.Point(766, 685);
            this.pictTec_BACK2.Name = "pictTec_BACK2";
            this.pictTec_BACK2.Size = new System.Drawing.Size(76, 73);
            this.pictTec_BACK2.TabIndex = 126;
            this.pictTec_BACK2.TabStop = false;
            this.pictTec_BACK2.Click += new System.EventHandler(this.pictTec_BACK2_Click);
            // 
            // tmrTeclado
            // 
            this.tmrTeclado.Tick += new System.EventHandler(this.tmrTeclado_Tick);
            // 
            // tmrTecladoShow
            // 
            this.tmrTecladoShow.Tick += new System.EventHandler(this.tmrTecladoShow_Tick);
            // 
            // pictTecladoShow
            // 
            this.pictTecladoShow.BackColor = System.Drawing.Color.Transparent;
            this.pictTecladoShow.Image = global::centralUnidas.Properties.Resources.totem_teclado_up;
            this.pictTecladoShow.Location = new System.Drawing.Point(905, 262);
            this.pictTecladoShow.Name = "pictTecladoShow";
            this.pictTecladoShow.Size = new System.Drawing.Size(76, 73);
            this.pictTecladoShow.TabIndex = 129;
            this.pictTecladoShow.TabStop = false;
            this.pictTecladoShow.Visible = false;
            this.pictTecladoShow.Click += new System.EventHandler(this.pictTecladoShow_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox2.Image = global::centralUnidas.Properties.Resources.Totem_Traco;
            this.pictureBox2.Location = new System.Drawing.Point(341, 210);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(683, 1);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 139;
            this.pictureBox2.TabStop = false;
            // 
            // tmrReturn
            // 
            this.tmrReturn.Enabled = true;
            this.tmrReturn.Interval = 60000;
            this.tmrReturn.Tick += new System.EventHandler(this.tmrReturn_Tick);
            // 
            // lblInformeDadasAbaixo
            // 
            this.lblInformeDadasAbaixo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblInformeDadasAbaixo.BackColor = System.Drawing.Color.White;
            this.lblInformeDadasAbaixo.Font = new System.Drawing.Font("Arial", 26F, System.Drawing.FontStyle.Bold);
            this.lblInformeDadasAbaixo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblInformeDadasAbaixo.Location = new System.Drawing.Point(0, 153);
            this.lblInformeDadasAbaixo.Name = "lblInformeDadasAbaixo";
            this.lblInformeDadasAbaixo.Size = new System.Drawing.Size(1374, 54);
            this.lblInformeDadasAbaixo.TabIndex = 158;
            this.lblInformeDadasAbaixo.Text = "Informe os dados abaixo:";
            this.lblInformeDadasAbaixo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNumeroReserva
            // 
            this.lblNumeroReserva.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNumeroReserva.BackColor = System.Drawing.Color.White;
            this.lblNumeroReserva.Font = new System.Drawing.Font("Arial", 22F);
            this.lblNumeroReserva.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(126)))), ((int)(((byte)(126)))));
            this.lblNumeroReserva.Location = new System.Drawing.Point(343, 261);
            this.lblNumeroReserva.Name = "lblNumeroReserva";
            this.lblNumeroReserva.Size = new System.Drawing.Size(248, 54);
            this.lblNumeroReserva.TabIndex = 159;
            this.lblNumeroReserva.Text = "Nro. da Reserva:";
            this.lblNumeroReserva.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblVersao
            // 
            this.lblVersao.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblVersao.AutoSize = true;
            this.lblVersao.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(210)))), ((int)(((byte)(213)))));
            this.lblVersao.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lblVersao.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblVersao.Location = new System.Drawing.Point(12, 743);
            this.lblVersao.Name = "lblVersao";
            this.lblVersao.Size = new System.Drawing.Size(53, 15);
            this.lblVersao.TabIndex = 160;
            this.lblVersao.Text = "Versao:";
            // 
            // butIdioma
            // 
            this.butIdioma.FlatAppearance.BorderSize = 0;
            this.butIdioma.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.butIdioma.ForeColor = System.Drawing.Color.Gray;
            this.butIdioma.Image = global::centralUnidas.Properties.Resources.Totem_Flag_Brasil_botao;
            this.butIdioma.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butIdioma.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.butIdioma.Location = new System.Drawing.Point(1148, 0);
            this.butIdioma.Name = "butIdioma";
            this.butIdioma.Size = new System.Drawing.Size(148, 61);
            this.butIdioma.TabIndex = 163;
            this.butIdioma.Text = "PORTUGUÊS";
            this.butIdioma.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butIdioma.UseCompatibleTextRendering = true;
            this.butIdioma.UseMnemonic = false;
            this.butIdioma.UseVisualStyleBackColor = true;
            this.butIdioma.Click += new System.EventHandler(this.butIdioma_Click);
            // 
            // lblLoja
            // 
            this.lblLoja.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLoja.AutoSize = true;
            this.lblLoja.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(210)))), ((int)(((byte)(213)))));
            this.lblLoja.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lblLoja.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblLoja.Location = new System.Drawing.Point(12, 723);
            this.lblLoja.Name = "lblLoja";
            this.lblLoja.Size = new System.Drawing.Size(35, 15);
            this.lblLoja.TabIndex = 164;
            this.lblLoja.Text = "Loja:";
            // 
            // frmScreen02
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1366, 768);
            this.ControlBox = false;
            this.Controls.Add(this.lblLoja);
            this.Controls.Add(this.butIdioma);
            this.Controls.Add(this.lblVersao);
            this.Controls.Add(this.lblNumeroReserva);
            this.Controls.Add(this.pictShutdown);
            this.Controls.Add(this.lblInformeDadasAbaixo);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictTecladoShow);
            this.Controls.Add(this.pictTec_BACK2);
            this.Controls.Add(this.pictTec_TECLADO);
            this.Controls.Add(this.pictTec_0);
            this.Controls.Add(this.pictTec_9);
            this.Controls.Add(this.pictTec_8);
            this.Controls.Add(this.pictTec_7);
            this.Controls.Add(this.pictTec_6);
            this.Controls.Add(this.pictTec_5);
            this.Controls.Add(this.pictTec_4);
            this.Controls.Add(this.pictTec_3);
            this.Controls.Add(this.pictTec_2);
            this.Controls.Add(this.pictTec_1);
            this.Controls.Add(this.pictTecladoFundo);
            this.Controls.Add(this.btnContinuar);
            this.Controls.Add(this.pictLoad);
            this.Controls.Add(this.txtReserva1);
            this.Controls.Add(this.pictUnidasLogo);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmScreen02";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CAU - Unidas";
            this.TopMost = false;
            this.WindowState = System.Windows.Forms.FormWindowState.Normal;
            this.Load += new System.EventHandler(this.frmScreen02_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictShutdown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictLoad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTecladoFundo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_TECLADO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_BACK2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTecladoShow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictUnidasLogo;
        private System.Windows.Forms.TextBox txtReserva1;
        private System.Windows.Forms.PictureBox pictShutdown;
        private System.Windows.Forms.Timer tmrReserva;
        private System.Windows.Forms.PictureBox pictLoad;
        private System.Windows.Forms.Button btnContinuar;
        private System.Windows.Forms.PictureBox pictTecladoFundo;
        private System.Windows.Forms.PictureBox pictTec_1;
        private System.Windows.Forms.PictureBox pictTec_2;
        private System.Windows.Forms.PictureBox pictTec_3;
        private System.Windows.Forms.PictureBox pictTec_4;
        private System.Windows.Forms.PictureBox pictTec_5;
        private System.Windows.Forms.PictureBox pictTec_6;
        private System.Windows.Forms.PictureBox pictTec_9;
        private System.Windows.Forms.PictureBox pictTec_8;
        private System.Windows.Forms.PictureBox pictTec_7;
        private System.Windows.Forms.PictureBox pictTec_0;
        private System.Windows.Forms.PictureBox pictTec_TECLADO;
        private System.Windows.Forms.PictureBox pictTec_BACK2;
        private System.Windows.Forms.Timer tmrTeclado;
        private System.Windows.Forms.Timer tmrTecladoShow;
        private System.Windows.Forms.PictureBox pictTecladoShow;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Timer tmrReturn;
        private System.Windows.Forms.Label lblInformeDadasAbaixo;
        private System.Windows.Forms.Label lblNumeroReserva;
        private System.Windows.Forms.Label lblVersao;
        private System.Windows.Forms.Button butIdioma;
        private System.Windows.Forms.Label lblLoja;
    }
}