﻿namespace centralUnidas
{
    partial class frmScreen05
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmScreen05));
            this.pictShutdown = new System.Windows.Forms.PictureBox();
            this.pictLang = new System.Windows.Forms.PictureBox();
            this.pictUnidasLogo = new System.Windows.Forms.PictureBox();
            this.lblSelecioneGrupo1 = new System.Windows.Forms.Label();
            this.lblSelecioneGrupo2 = new System.Windows.Forms.Label();
            this.pnlVeiculo1 = new System.Windows.Forms.Panel();
            this.pictVeiculo1 = new System.Windows.Forms.PictureBox();
            this.lblDescricaoPNL1 = new System.Windows.Forms.Label();
            this.lblModeloPNL1 = new System.Windows.Forms.Label();
            this.pnlVeiculo2 = new System.Windows.Forms.Panel();
            this.pictVeiculo2 = new System.Windows.Forms.PictureBox();
            this.lblDescricaoPNL2 = new System.Windows.Forms.Label();
            this.lblModeloPNL2 = new System.Windows.Forms.Label();
            this.pnlVeiculo3 = new System.Windows.Forms.Panel();
            this.pictVeiculo3 = new System.Windows.Forms.PictureBox();
            this.lblDescricaoPNL3 = new System.Windows.Forms.Label();
            this.lblModeloPNL3 = new System.Windows.Forms.Label();
            this.pnlVeiculo4 = new System.Windows.Forms.Panel();
            this.pictVeiculo4 = new System.Windows.Forms.PictureBox();
            this.lblDescricaoPNL4 = new System.Windows.Forms.Label();
            this.lblModeloPNL4 = new System.Windows.Forms.Label();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.tmrReturn = new System.Windows.Forms.Timer(this.components);
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.butIdioma = new System.Windows.Forms.Button();
            this.butVoltar = new System.Windows.Forms.Button();
            this.butConfirmar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictShutdown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictLang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).BeginInit();
            this.pnlVeiculo1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictVeiculo1)).BeginInit();
            this.pnlVeiculo2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictVeiculo2)).BeginInit();
            this.pnlVeiculo3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictVeiculo3)).BeginInit();
            this.pnlVeiculo4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictVeiculo4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // pictShutdown
            // 
            this.pictShutdown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictShutdown.BackColor = System.Drawing.Color.Transparent;
            this.pictShutdown.Image = global::centralUnidas.Properties.Resources.shutdown_button;
            this.pictShutdown.Location = new System.Drawing.Point(12, 146);
            this.pictShutdown.Name = "pictShutdown";
            this.pictShutdown.Size = new System.Drawing.Size(50, 55);
            this.pictShutdown.TabIndex = 100;
            this.pictShutdown.TabStop = false;
            this.pictShutdown.Visible = false;
            this.pictShutdown.Click += new System.EventHandler(this.pictShutdown_Click);
            // 
            // pictLang
            // 
            this.pictLang.Image = ((System.Drawing.Image)(resources.GetObject("pictLang.Image")));
            this.pictLang.Location = new System.Drawing.Point(895, 50);
            this.pictLang.Name = "pictLang";
            this.pictLang.Size = new System.Drawing.Size(148, 61);
            this.pictLang.TabIndex = 109;
            this.pictLang.TabStop = false;
            this.pictLang.Visible = false;
            this.pictLang.Click += new System.EventHandler(this.pictLang_Click);
            // 
            // pictUnidasLogo
            // 
            this.pictUnidasLogo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictUnidasLogo.BackColor = System.Drawing.Color.Transparent;
            this.pictUnidasLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictUnidasLogo.Image = global::centralUnidas.Properties.Resources.totem_header;
            this.pictUnidasLogo.Location = new System.Drawing.Point(0, 0);
            this.pictUnidasLogo.Name = "pictUnidasLogo";
            this.pictUnidasLogo.Size = new System.Drawing.Size(1366, 140);
            this.pictUnidasLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictUnidasLogo.TabIndex = 108;
            this.pictUnidasLogo.TabStop = false;
            // 
            // lblSelecioneGrupo1
            // 
            this.lblSelecioneGrupo1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSelecioneGrupo1.AutoSize = true;
            this.lblSelecioneGrupo1.BackColor = System.Drawing.Color.White;
            this.lblSelecioneGrupo1.Font = new System.Drawing.Font("Arial", 26.25F, System.Drawing.FontStyle.Bold);
            this.lblSelecioneGrupo1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblSelecioneGrupo1.Location = new System.Drawing.Point(424, 160);
            this.lblSelecioneGrupo1.Name = "lblSelecioneGrupo1";
            this.lblSelecioneGrupo1.Size = new System.Drawing.Size(661, 41);
            this.lblSelecioneGrupo1.TabIndex = 114;
            this.lblSelecioneGrupo1.Text = "Selecione um dos veículos disponíveis ";
            this.lblSelecioneGrupo1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSelecioneGrupo2
            // 
            this.lblSelecioneGrupo2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSelecioneGrupo2.AutoSize = true;
            this.lblSelecioneGrupo2.BackColor = System.Drawing.Color.White;
            this.lblSelecioneGrupo2.Font = new System.Drawing.Font("Arial", 26.25F, System.Drawing.FontStyle.Bold);
            this.lblSelecioneGrupo2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblSelecioneGrupo2.Location = new System.Drawing.Point(424, 212);
            this.lblSelecioneGrupo2.Name = "lblSelecioneGrupo2";
            this.lblSelecioneGrupo2.Size = new System.Drawing.Size(235, 41);
            this.lblSelecioneGrupo2.TabIndex = 115;
            this.lblSelecioneGrupo2.Text = "para o Grupo";
            this.lblSelecioneGrupo2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlVeiculo1
            // 
            this.pnlVeiculo1.BackColor = System.Drawing.Color.Transparent;
            this.pnlVeiculo1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlVeiculo1.Controls.Add(this.pictVeiculo1);
            this.pnlVeiculo1.Controls.Add(this.lblDescricaoPNL1);
            this.pnlVeiculo1.Controls.Add(this.lblModeloPNL1);
            this.pnlVeiculo1.Location = new System.Drawing.Point(27, 305);
            this.pnlVeiculo1.Name = "pnlVeiculo1";
            this.pnlVeiculo1.Size = new System.Drawing.Size(316, 300);
            this.pnlVeiculo1.TabIndex = 117;
            this.pnlVeiculo1.Visible = false;
            this.pnlVeiculo1.Click += new System.EventHandler(this.pnlVeiculo1_Click);
            // 
            // pictVeiculo1
            // 
            this.pictVeiculo1.Location = new System.Drawing.Point(6, 9);
            this.pictVeiculo1.Name = "pictVeiculo1";
            this.pictVeiculo1.Size = new System.Drawing.Size(300, 200);
            this.pictVeiculo1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictVeiculo1.TabIndex = 2;
            this.pictVeiculo1.TabStop = false;
            this.pictVeiculo1.Click += new System.EventHandler(this.pictVeiculo1_Click);
            // 
            // lblDescricaoPNL1
            // 
            this.lblDescricaoPNL1.AutoSize = true;
            this.lblDescricaoPNL1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lblDescricaoPNL1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblDescricaoPNL1.Location = new System.Drawing.Point(6, 268);
            this.lblDescricaoPNL1.Name = "lblDescricaoPNL1";
            this.lblDescricaoPNL1.Size = new System.Drawing.Size(77, 19);
            this.lblDescricaoPNL1.TabIndex = 1;
            this.lblDescricaoPNL1.Text = "Decrição";
            // 
            // lblModeloPNL1
            // 
            this.lblModeloPNL1.AutoSize = true;
            this.lblModeloPNL1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lblModeloPNL1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblModeloPNL1.Location = new System.Drawing.Point(6, 237);
            this.lblModeloPNL1.Name = "lblModeloPNL1";
            this.lblModeloPNL1.Size = new System.Drawing.Size(65, 19);
            this.lblModeloPNL1.TabIndex = 0;
            this.lblModeloPNL1.Text = "Modelo";
            // 
            // pnlVeiculo2
            // 
            this.pnlVeiculo2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlVeiculo2.Controls.Add(this.pictVeiculo2);
            this.pnlVeiculo2.Controls.Add(this.lblDescricaoPNL2);
            this.pnlVeiculo2.Controls.Add(this.lblModeloPNL2);
            this.pnlVeiculo2.Location = new System.Drawing.Point(351, 305);
            this.pnlVeiculo2.Name = "pnlVeiculo2";
            this.pnlVeiculo2.Size = new System.Drawing.Size(316, 300);
            this.pnlVeiculo2.TabIndex = 118;
            this.pnlVeiculo2.Visible = false;
            this.pnlVeiculo2.Click += new System.EventHandler(this.pnlVeiculo2_Click);
            // 
            // pictVeiculo2
            // 
            this.pictVeiculo2.Location = new System.Drawing.Point(7, 9);
            this.pictVeiculo2.Name = "pictVeiculo2";
            this.pictVeiculo2.Size = new System.Drawing.Size(300, 200);
            this.pictVeiculo2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictVeiculo2.TabIndex = 4;
            this.pictVeiculo2.TabStop = false;
            this.pictVeiculo2.Click += new System.EventHandler(this.pictVeiculo2_Click);
            // 
            // lblDescricaoPNL2
            // 
            this.lblDescricaoPNL2.AutoSize = true;
            this.lblDescricaoPNL2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lblDescricaoPNL2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblDescricaoPNL2.Location = new System.Drawing.Point(3, 268);
            this.lblDescricaoPNL2.Name = "lblDescricaoPNL2";
            this.lblDescricaoPNL2.Size = new System.Drawing.Size(77, 19);
            this.lblDescricaoPNL2.TabIndex = 3;
            this.lblDescricaoPNL2.Text = "Decrição";
            // 
            // lblModeloPNL2
            // 
            this.lblModeloPNL2.AutoSize = true;
            this.lblModeloPNL2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lblModeloPNL2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblModeloPNL2.Location = new System.Drawing.Point(3, 237);
            this.lblModeloPNL2.Name = "lblModeloPNL2";
            this.lblModeloPNL2.Size = new System.Drawing.Size(65, 19);
            this.lblModeloPNL2.TabIndex = 2;
            this.lblModeloPNL2.Text = "Modelo";
            // 
            // pnlVeiculo3
            // 
            this.pnlVeiculo3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlVeiculo3.Controls.Add(this.pictVeiculo3);
            this.pnlVeiculo3.Controls.Add(this.lblDescricaoPNL3);
            this.pnlVeiculo3.Controls.Add(this.lblModeloPNL3);
            this.pnlVeiculo3.Location = new System.Drawing.Point(675, 305);
            this.pnlVeiculo3.Name = "pnlVeiculo3";
            this.pnlVeiculo3.Size = new System.Drawing.Size(316, 300);
            this.pnlVeiculo3.TabIndex = 118;
            this.pnlVeiculo3.Visible = false;
            this.pnlVeiculo3.Click += new System.EventHandler(this.pnlVeiculo3_Click);
            // 
            // pictVeiculo3
            // 
            this.pictVeiculo3.Location = new System.Drawing.Point(7, 9);
            this.pictVeiculo3.Name = "pictVeiculo3";
            this.pictVeiculo3.Size = new System.Drawing.Size(300, 200);
            this.pictVeiculo3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictVeiculo3.TabIndex = 4;
            this.pictVeiculo3.TabStop = false;
            this.pictVeiculo3.Click += new System.EventHandler(this.pictVeiculo3_Click);
            // 
            // lblDescricaoPNL3
            // 
            this.lblDescricaoPNL3.AutoSize = true;
            this.lblDescricaoPNL3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lblDescricaoPNL3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblDescricaoPNL3.Location = new System.Drawing.Point(3, 268);
            this.lblDescricaoPNL3.Name = "lblDescricaoPNL3";
            this.lblDescricaoPNL3.Size = new System.Drawing.Size(77, 19);
            this.lblDescricaoPNL3.TabIndex = 3;
            this.lblDescricaoPNL3.Text = "Decrição";
            // 
            // lblModeloPNL3
            // 
            this.lblModeloPNL3.AutoSize = true;
            this.lblModeloPNL3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lblModeloPNL3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblModeloPNL3.Location = new System.Drawing.Point(3, 237);
            this.lblModeloPNL3.Name = "lblModeloPNL3";
            this.lblModeloPNL3.Size = new System.Drawing.Size(65, 19);
            this.lblModeloPNL3.TabIndex = 2;
            this.lblModeloPNL3.Text = "Modelo";
            // 
            // pnlVeiculo4
            // 
            this.pnlVeiculo4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlVeiculo4.Controls.Add(this.pictVeiculo4);
            this.pnlVeiculo4.Controls.Add(this.lblDescricaoPNL4);
            this.pnlVeiculo4.Controls.Add(this.lblModeloPNL4);
            this.pnlVeiculo4.Location = new System.Drawing.Point(999, 305);
            this.pnlVeiculo4.Name = "pnlVeiculo4";
            this.pnlVeiculo4.Size = new System.Drawing.Size(316, 300);
            this.pnlVeiculo4.TabIndex = 118;
            this.pnlVeiculo4.Visible = false;
            this.pnlVeiculo4.Click += new System.EventHandler(this.pnlVeiculo4_Click);
            // 
            // pictVeiculo4
            // 
            this.pictVeiculo4.Location = new System.Drawing.Point(7, 9);
            this.pictVeiculo4.Name = "pictVeiculo4";
            this.pictVeiculo4.Size = new System.Drawing.Size(300, 200);
            this.pictVeiculo4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictVeiculo4.TabIndex = 4;
            this.pictVeiculo4.TabStop = false;
            this.pictVeiculo4.Click += new System.EventHandler(this.pictVeiculo4_Click);
            // 
            // lblDescricaoPNL4
            // 
            this.lblDescricaoPNL4.AutoSize = true;
            this.lblDescricaoPNL4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lblDescricaoPNL4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblDescricaoPNL4.Location = new System.Drawing.Point(3, 268);
            this.lblDescricaoPNL4.Name = "lblDescricaoPNL4";
            this.lblDescricaoPNL4.Size = new System.Drawing.Size(77, 19);
            this.lblDescricaoPNL4.TabIndex = 3;
            this.lblDescricaoPNL4.Text = "Decrição";
            // 
            // lblModeloPNL4
            // 
            this.lblModeloPNL4.AutoSize = true;
            this.lblModeloPNL4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lblModeloPNL4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblModeloPNL4.Location = new System.Drawing.Point(3, 237);
            this.lblModeloPNL4.Name = "lblModeloPNL4";
            this.lblModeloPNL4.Size = new System.Drawing.Size(65, 19);
            this.lblModeloPNL4.TabIndex = 2;
            this.lblModeloPNL4.Text = "Modelo";
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConfirmar.Image = global::centralUnidas.Properties.Resources.totem_reserva_confirmar;
            this.btnConfirmar.Location = new System.Drawing.Point(1044, 650);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(263, 79);
            this.btnConfirmar.TabIndex = 112;
            this.btnConfirmar.UseVisualStyleBackColor = true;
            this.btnConfirmar.Visible = false;
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // btnAlterar
            // 
            this.btnAlterar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAlterar.BackgroundImage = global::centralUnidas.Properties.Resources.totem_voltar;
            this.btnAlterar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAlterar.Location = new System.Drawing.Point(123, 650);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(263, 79);
            this.btnAlterar.TabIndex = 111;
            this.btnAlterar.UseVisualStyleBackColor = true;
            this.btnAlterar.Visible = false;
            this.btnAlterar.Click += new System.EventHandler(this.btnAlterar_Click);
            // 
            // tmrReturn
            // 
            this.tmrReturn.Enabled = true;
            this.tmrReturn.Interval = 40000;
            this.tmrReturn.Tick += new System.EventHandler(this.tmrReturn_Tick);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox2.Image = global::centralUnidas.Properties.Resources.Totem_Traco;
            this.pictureBox2.Location = new System.Drawing.Point(416, 261);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(683, 1);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 141;
            this.pictureBox2.TabStop = false;
            // 
            // butIdioma
            // 
            this.butIdioma.FlatAppearance.BorderSize = 0;
            this.butIdioma.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.butIdioma.ForeColor = System.Drawing.Color.Gray;
            this.butIdioma.Image = global::centralUnidas.Properties.Resources.Totem_Flag_Brasil_botao;
            this.butIdioma.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butIdioma.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.butIdioma.Location = new System.Drawing.Point(1148, 0);
            this.butIdioma.Name = "butIdioma";
            this.butIdioma.Size = new System.Drawing.Size(148, 61);
            this.butIdioma.TabIndex = 165;
            this.butIdioma.Text = "PORTUGUÊS";
            this.butIdioma.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butIdioma.UseCompatibleTextRendering = true;
            this.butIdioma.UseMnemonic = false;
            this.butIdioma.UseVisualStyleBackColor = true;
            this.butIdioma.Click += new System.EventHandler(this.butIdioma_Click);
            // 
            // butVoltar
            // 
            this.butVoltar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.butVoltar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.butVoltar.Font = new System.Drawing.Font("Arial", 22F, System.Drawing.FontStyle.Bold);
            this.butVoltar.ForeColor = System.Drawing.Color.White;
            this.butVoltar.Location = new System.Drawing.Point(431, 650);
            this.butVoltar.Name = "butVoltar";
            this.butVoltar.Size = new System.Drawing.Size(263, 79);
            this.butVoltar.TabIndex = 168;
            this.butVoltar.Text = "Voltar";
            this.butVoltar.UseVisualStyleBackColor = false;
            this.butVoltar.Click += new System.EventHandler(this.butVoltar_Click);
            // 
            // butConfirmar
            // 
            this.butConfirmar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.butConfirmar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.butConfirmar.Font = new System.Drawing.Font("Arial", 22F, System.Drawing.FontStyle.Bold);
            this.butConfirmar.ForeColor = System.Drawing.Color.White;
            this.butConfirmar.Location = new System.Drawing.Point(775, 650);
            this.butConfirmar.Name = "butConfirmar";
            this.butConfirmar.Size = new System.Drawing.Size(263, 79);
            this.butConfirmar.TabIndex = 169;
            this.butConfirmar.Text = "Confirmar";
            this.butConfirmar.UseVisualStyleBackColor = false;
            this.butConfirmar.Click += new System.EventHandler(this.butConfirmar_Click);
            // 
            // frmScreen05
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1366, 741);
            this.ControlBox = false;
            this.Controls.Add(this.butConfirmar);
            this.Controls.Add(this.butVoltar);
            this.Controls.Add(this.butIdioma);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.btnConfirmar);
            this.Controls.Add(this.pnlVeiculo4);
            this.Controls.Add(this.btnAlterar);
            this.Controls.Add(this.pnlVeiculo3);
            this.Controls.Add(this.pnlVeiculo2);
            this.Controls.Add(this.pnlVeiculo1);
            this.Controls.Add(this.lblSelecioneGrupo2);
            this.Controls.Add(this.lblSelecioneGrupo1);
            this.Controls.Add(this.pictLang);
            this.Controls.Add(this.pictUnidasLogo);
            this.Controls.Add(this.pictShutdown);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmScreen05";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CAU - Unidas";
            this.TopMost = false;
            this.WindowState = System.Windows.Forms.FormWindowState.Normal;
            this.Load += new System.EventHandler(this.frmScreen05_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictShutdown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictLang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).EndInit();
            this.pnlVeiculo1.ResumeLayout(false);
            this.pnlVeiculo1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictVeiculo1)).EndInit();
            this.pnlVeiculo2.ResumeLayout(false);
            this.pnlVeiculo2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictVeiculo2)).EndInit();
            this.pnlVeiculo3.ResumeLayout(false);
            this.pnlVeiculo3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictVeiculo3)).EndInit();
            this.pnlVeiculo4.ResumeLayout(false);
            this.pnlVeiculo4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictVeiculo4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictShutdown;
        private System.Windows.Forms.PictureBox pictLang;
        private System.Windows.Forms.PictureBox pictUnidasLogo;
        private System.Windows.Forms.Label lblSelecioneGrupo1;
        private System.Windows.Forms.Label lblSelecioneGrupo2;
        private System.Windows.Forms.Panel pnlVeiculo1;
        private System.Windows.Forms.Panel pnlVeiculo2;
        private System.Windows.Forms.Panel pnlVeiculo3;
        private System.Windows.Forms.Panel pnlVeiculo4;
        private System.Windows.Forms.Label lblModeloPNL1;
        private System.Windows.Forms.Label lblDescricaoPNL1;
        private System.Windows.Forms.Label lblDescricaoPNL2;
        private System.Windows.Forms.Label lblModeloPNL2;
        private System.Windows.Forms.Label lblDescricaoPNL3;
        private System.Windows.Forms.Label lblModeloPNL3;
        private System.Windows.Forms.Label lblDescricaoPNL4;
        private System.Windows.Forms.Label lblModeloPNL4;
        private System.Windows.Forms.PictureBox pictVeiculo1;
        private System.Windows.Forms.PictureBox pictVeiculo2;
        private System.Windows.Forms.PictureBox pictVeiculo3;
        private System.Windows.Forms.PictureBox pictVeiculo4;
        private System.Windows.Forms.Button btnConfirmar;
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.Timer tmrReturn;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button butIdioma;
        private System.Windows.Forms.Button butVoltar;
        private System.Windows.Forms.Button butConfirmar;
    }
}