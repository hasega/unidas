﻿// Aplicação: Unidas Totem
// Data     : 01/10/2015
// Versão   : 1.0
// Empresa  : Unidas Rent a Car
// Equipe   : Unidas Desenvolvimento
// Objetivo : Escolha da Proteção
// Histórico: 
//      01/10/2015: Inicio de desenvolvimento para acesso geral
//

using centralUnidas.centralUnidasWCF;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using centralUnidas.wbsUnidasSrv;
using System.Resources;

namespace centralUnidas
{
    public partial class frmScreen07 : Form
    {
        public ResXResourceSet ResxSetForm;

        public frmScreen07()
        {
            InitializeComponent();
        }

        private void frmScreen07_Load(object sender, EventArgs e)
        {

            LogTotem.InsertLog(new LogOperacao()
            {
                Application_Name = Globals.glbApplicationName,
                Application_Version = Globals.glbApplicationVersion,
                Local = Globals.glbLojaTotem,
                Operation_Ticks = Globals.glbOperationTicks,
                Operation_Name = "Seleção de Proteções",
                Form = "FrmScreen07",
                RESNUM = String.IsNullOrEmpty(Globals.glbReservaNumeroPesquisa) ? 0 : Convert.ToInt32(Globals.glbReservaNumeroPesquisa),
                RANUM = String.IsNullOrEmpty(Globals.glbRA_Contrato) ? 0 : Convert.ToInt32(Globals.glbRA_Contrato)
            });

            CultureInfo culture = new CultureInfo("pt-BR");
            int intLanguage = Globals.glbLanguage;

            if (!string.IsNullOrEmpty(Globals.glbRTBTOTRES_ANT)) Globals.glbRTBTOTRES = Globals.glbRTBTOTRES_ANT.Trim();

            int iRetServico = ConsultaProtecao();
            Application.DoEvents();

            // Desabilitado tela de proteções
            butConfirmar.PerformClick();
        }

        private void Shutdown_Enable()
        {
            if (Globals.glbDebugMode == 0)
                pictShutdown.Visible = false;
            else
                pictShutdown.Visible = true;
        }

        private int ConsultaProtecao()
        {
            int iRetServico = 0;
            string xmlRetornoWCF = "";
            try
            {

                CentralUnidasClassClient clientWcf = new CentralUnidasClassClient();

                string strNumeroReserva = Globals.glbConfID.Trim();
                string strALOJRET = Globals.glbPickUpLocation.Trim();
                string strTipo = "PROTE";

                xmlRetornoWCF = clientWcf.GetProtecoesTotemData(Globals.glbResGruCod, Convert.ToInt32(Globals.glbReservaNumeroPesquisa));
                if (string.IsNullOrEmpty(xmlRetornoWCF) || (xmlRetornoWCF.Trim() == "<UNIDASWCF />"))
                {
                    iRetServico = 99;
                }
                else
                {
                    xmlRetornoWCF = xmlRetornoWCF.Replace("\r\n", "");
                    DataSet dtSet = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(xmlRetornoWCF);
                    dtSet.ReadXml(xmlSR);

                    int nProtec = 0;
                    int nAcess = 0;

                    checkBox1.Checked = false;
                    checkBox2.Checked = false;
                    checkBox3.Checked = false;
                    checkBox4.Checked = false;
                    checkBox5.Checked = false;
                    checkBox6.Checked = false;
                    checkBox7.Checked = false;

                    if (!string.IsNullOrEmpty(Globals.glbProtecoesCodVar1))
                    {

                        if (this.RemoverAcentos(Globals.glbProtecoesVar1.ToUpper().Trim()).Contains("PARCIAL")) checkBox1.Checked = true; // parcial
                        if (this.RemoverAcentos(Globals.glbProtecoesVar1.ToUpper().Trim()).Contains("ESPECIAL")) checkBox2.Checked = true; // especial
                        if (this.RemoverAcentos(Globals.glbProtecoesVar1.ToUpper().Trim()) == "PROTECAO A OCUPANTES") checkBox3.Checked = true; // ocupantes
                        if (this.RemoverAcentos(Globals.glbProtecoesVar1.ToUpper().Trim()) == "PROTECAO A TERCEIROS") checkBox4.Checked = true; // terceiros
                        if (this.RemoverAcentos(Globals.glbProtecoesVar1.ToUpper().Trim()).Contains("OCUPANTES E TERCEIROS")) checkBox5.Checked = true; // ocupantes terceiros
                        if (this.RemoverAcentos(Globals.glbProtecoesVar1.ToUpper().Trim()).Contains("SUPER")) checkBox6.Checked = true; // super
                        if (this.RemoverAcentos(Globals.glbProtecoesVar1.ToUpper().Trim()).Contains("ZERO"))
                        {
                            if (checkBox6.Checked == true) checkBox6.Checked = false;
                            checkBox7.Checked = true; // zero
                        }
                    }

                    if (!string.IsNullOrEmpty(Globals.glbProtecoesCodVar2))
                    {
                        if (this.RemoverAcentos(Globals.glbProtecoesVar2.ToUpper().Trim()).Contains("PARCIAL")) checkBox1.Checked = true; // parcial
                        if (this.RemoverAcentos(Globals.glbProtecoesVar2.ToUpper().Trim()).Contains("ESPECIAL")) checkBox2.Checked = true; // especial
                        if (this.RemoverAcentos(Globals.glbProtecoesVar2.ToUpper().Trim()) == "PROTECAO A OCUPANTES") checkBox3.Checked = true; // ocupantes
                        if (this.RemoverAcentos(Globals.glbProtecoesVar2.ToUpper().Trim()) == "PROTECAO A TERCEIROS") checkBox4.Checked = true; // terceiros
                        if (this.RemoverAcentos(Globals.glbProtecoesVar2.ToUpper().Trim()).Contains("OCUPANTES E TERCEIROS")) checkBox5.Checked = true; // ocupantes terceiros
                        if (this.RemoverAcentos(Globals.glbProtecoesVar2.ToUpper().Trim()).Contains("SUPER")) checkBox6.Checked = true; // super
                        if (this.RemoverAcentos(Globals.glbProtecoesVar2.ToUpper().Trim()).Contains("ZERO"))
                        {
                            if (checkBox6.Checked == true) checkBox6.Checked = false;
                            checkBox7.Checked = true; // zero
                        }
                    }

                    if (!string.IsNullOrEmpty(Globals.glbProtecoesCodVar3))
                    {
                        if (this.RemoverAcentos(Globals.glbProtecoesVar3.ToUpper().Trim()).Contains("PARCIAL")) checkBox1.Checked = true; // parcial
                        if (this.RemoverAcentos(Globals.glbProtecoesVar3.ToUpper().Trim()).Contains("ESPECIAL")) checkBox2.Checked = true; // especial
                        if (this.RemoverAcentos(Globals.glbProtecoesVar3.ToUpper().Trim()) == "PROTECAO A OCUPANTES") checkBox3.Checked = true; // ocupantes
                        if (this.RemoverAcentos(Globals.glbProtecoesVar3.ToUpper().Trim()) == "PROTECAO A TERCEIROS") checkBox4.Checked = true; // terceiros
                        if (this.RemoverAcentos(Globals.glbProtecoesVar3.ToUpper().Trim()).Contains("OCUPANTES E TERCEIROS")) checkBox5.Checked = true; // ocupantes terceiros
                        if (this.RemoverAcentos(Globals.glbProtecoesVar3.ToUpper().Trim()).Contains("SUPER")) checkBox6.Checked = true; // super
                        if (this.RemoverAcentos(Globals.glbProtecoesVar3.ToUpper().Trim()).Contains("ZERO"))
                        {
                            if (checkBox6.Checked == true) checkBox6.Checked = false;
                            checkBox7.Checked = true; // zero
                        }
                    }

                    if (checkBox2.Checked)
                    {
                        protecaoParcial_Hide();
                        protecaoEspecial_MoveUp(1);
                        protecaoTerceiros_MoveUp(1);
                        protecaoOcupantes_MoveUp(1);
                        protecaoOcupantesTerceiros_MoveUp(1);
                        protecaoSuper_MoveUp(1);
                        protecaoZero_MoveUp(1);
                    }

                    if (checkBox6.Checked)
                    {
                        protecaoParcial_Hide();
                        protecaoEspecial_Hide();
                        protecaoTerceiros_Hide();
                        protecaoOcupantes_Hide();
                        protecaoOcupantesTerceiros_Hide();
                        protecaoSuper_MoveUp(4);
                        protecaoZero_MoveUp(4);
                    }

                    if (checkBox7.Checked)
                    {
                        protecaoParcial_Hide();
                        protecaoEspecial_Hide();
                        protecaoTerceiros_Hide();
                        protecaoOcupantes_Hide();
                        protecaoOcupantesTerceiros_Hide();
                        protecaoSuper_Hide();
                        protecaoZero_MoveUp(5);
                    }

                    foreach (DataRow dbRow in dtSet.Tables["Table"].Rows)
                    {
                        string strCATSRVCOD = dbRow["CATSRVCOD"].ToString().Trim();
                        string strCATSRVDSC = dbRow["CATSRVDSC"].ToString().Trim();
                        string strCATVLDIA = dbRow["CATSRVVLR"].ToString().Trim();
                        string strCATPTGVEILOC = dbRow["CATPTGVEILOC"].ToString().Trim();
                        string strCATPTGTERCEIROS = dbRow["CATPTGTERCEIROS"].ToString().Trim();
                        string strCATPTGOCUPANTES = dbRow["CATPTGOCUPANTES"].ToString().Trim();
                        string strCATVLFRANQUIAMIN = dbRow["CATVLFRANQUIAMIN"].ToString().Trim();
                        string strCATVALPRE = dbRow["CATVALPRE"].ToString().Trim();
                        string strCATVALPAR = dbRow["CATVALPAR"].ToString().Trim();

                        if (this.RemoverAcentos(strCATSRVDSC.ToUpper().Trim()).Contains("PARCIAL"))
                        {
                            lblCod1a.Text = strCATSRVCOD.Trim();
                            lblPre1a.Text = strCATVALPRE.Trim().Replace(".", ",");
                            lblPar1a.Text = strCATVALPAR.Trim().Replace(".", ",");

                            lblValor1.Text = "| R$ " + strCATVLDIA + "/dia";
                            lblValor1a.Text = strCATVLDIA.Replace(".", ",");
                            if (strCATPTGVEILOC.Trim().ToLower() == "true")
                                pict11.BackgroundImage = centralUnidas.Properties.Resources.Totem_OK;
                            else
                                pict11.BackgroundImage = centralUnidas.Properties.Resources.Totem_NOK;
                            if (strCATPTGTERCEIROS.Trim().ToLower() == "true")
                                pict12.BackgroundImage = centralUnidas.Properties.Resources.Totem_OK;
                            else
                                pict12.BackgroundImage = centralUnidas.Properties.Resources.Totem_NOK;
                            if (strCATPTGOCUPANTES.Trim().ToLower() == "true")
                                pict13.BackgroundImage = centralUnidas.Properties.Resources.Totem_OK;
                            else
                                pict13.BackgroundImage = centralUnidas.Properties.Resources.Totem_NOK;
                            lblFranquia1.Text = "R$ " + strCATVALPAR;
                            lblPre1.Text = "R$ " + strCATVALPRE;
                        }

                        if (this.RemoverAcentos(strCATSRVDSC.ToUpper().Trim()).Contains("ESPECIAL"))
                        {
                            lblCod2a.Text = strCATSRVCOD.Trim();
                            lblPre2a.Text = strCATVALPRE.Trim().Replace(".", ",");
                            lblPar2a.Text = strCATVALPAR.Trim().Replace(".", ",");

                            lblValor2.Text = "| R$ " + strCATVLDIA + "/dia";
                            lblValor2a.Text = strCATVLDIA.Replace(".", ",");
                            if (strCATPTGVEILOC.Trim().ToLower() == "true")
                                pict21.BackgroundImage = centralUnidas.Properties.Resources.Totem_OK;
                            else
                                pict21.BackgroundImage = centralUnidas.Properties.Resources.Totem_NOK;
                            if (strCATPTGTERCEIROS.Trim().ToLower() == "true")
                                pict22.BackgroundImage = centralUnidas.Properties.Resources.Totem_OK;
                            else
                                pict22.BackgroundImage = centralUnidas.Properties.Resources.Totem_NOK;
                            if (strCATPTGOCUPANTES.Trim().ToLower() == "true")
                                pict23.BackgroundImage = centralUnidas.Properties.Resources.Totem_OK;
                            else
                                pict23.BackgroundImage = centralUnidas.Properties.Resources.Totem_NOK;
                            lblFranquia2.Text = "R$ " + strCATVALPAR;
                            lblPre2.Text = "R$ " + strCATVALPRE;
                        }

                        if (this.RemoverAcentos(strCATSRVDSC.ToUpper().Trim()).Contains("PROTECAO A TERCEIROS"))
                        {
                            lblCod4a.Text = strCATSRVCOD.Trim();
                            lblPre4a.Text = strCATVALPRE.Trim().Replace(".", ",");
                            lblPar4a.Text = strCATVALPAR.Trim().Replace(".", ",");

                            lblValor4.Text = "| R$ " + strCATVLDIA + "/dia";
                            lblValor4a.Text = strCATVLDIA.Replace(".", ",");
                            if (strCATPTGVEILOC.Trim().ToLower() == "true")
                                pict41.BackgroundImage = centralUnidas.Properties.Resources.Totem_OK;
                            else
                                pict41.BackgroundImage = centralUnidas.Properties.Resources.Totem_NOK;
                            if (strCATPTGTERCEIROS.Trim().ToLower() == "true")
                                pict42.BackgroundImage = centralUnidas.Properties.Resources.Totem_OK;
                            else
                                pict42.BackgroundImage = centralUnidas.Properties.Resources.Totem_NOK;
                            if (strCATPTGOCUPANTES.Trim().ToLower() == "true")
                                pict43.BackgroundImage = centralUnidas.Properties.Resources.Totem_OK;
                            else
                                pict43.BackgroundImage = centralUnidas.Properties.Resources.Totem_NOK;
                            lblFranquia4.Text = "R$ " + strCATVALPAR;
                            lblPre4.Text = "R$ " + strCATVALPRE;
                        }

                        if (this.RemoverAcentos(strCATSRVDSC.ToUpper().Trim()).Contains("PROTECAO A OCUPANTES"))
                        {
                            lblCod3a.Text = strCATSRVCOD.Trim();
                            lblPre3a.Text = strCATVALPRE.Trim().Replace(".", ",");
                            lblPar3a.Text = strCATVALPAR.Trim().Replace(".", ",");

                            lblValor3.Text = "| R$ " + strCATVLDIA + "/dia";
                            lblValor3a.Text = strCATVLDIA.Replace(".", ",");
                            if (strCATPTGVEILOC.Trim().ToLower() == "true")
                                pict31.BackgroundImage = centralUnidas.Properties.Resources.Totem_OK;
                            else
                                pict31.BackgroundImage = centralUnidas.Properties.Resources.Totem_NOK;
                            if (strCATPTGTERCEIROS.Trim().ToLower() == "true")
                                pict32.BackgroundImage = centralUnidas.Properties.Resources.Totem_OK;
                            else
                                pict32.BackgroundImage = centralUnidas.Properties.Resources.Totem_NOK;
                            if (strCATPTGOCUPANTES.Trim().ToLower() == "true")
                                pict33.BackgroundImage = centralUnidas.Properties.Resources.Totem_OK;
                            else
                                pict33.BackgroundImage = centralUnidas.Properties.Resources.Totem_NOK;
                            lblFranquia3.Text = "R$ " + strCATVALPAR;
                            lblPre3.Text = "R$ " + strCATVALPRE;
                        }

                        if (this.RemoverAcentos(strCATSRVDSC.ToUpper().Trim()).Contains("PROTECAO OCUPANTES E TERCEIROS"))
                        {
                            lblCod5a.Text = strCATSRVCOD.Trim();
                            lblPre5a.Text = strCATVALPRE.Trim().Replace(".", ",");
                            lblPar5a.Text = strCATVALPAR.Trim().Replace(".", ",");

                            lblValor5.Text = "| R$ " + strCATVLDIA + "/dia";
                            lblValor5a.Text = strCATVLDIA.Replace(".", ",");
                            if (strCATPTGVEILOC.Trim().ToLower() == "true")
                                pict51.BackgroundImage = centralUnidas.Properties.Resources.Totem_OK;
                            else
                                pict51.BackgroundImage = centralUnidas.Properties.Resources.Totem_NOK;
                            if (strCATPTGTERCEIROS.Trim().ToLower() == "true")
                                pict52.BackgroundImage = centralUnidas.Properties.Resources.Totem_OK;
                            else
                                pict52.BackgroundImage = centralUnidas.Properties.Resources.Totem_NOK;
                            if (strCATPTGOCUPANTES.Trim().ToLower() == "true")
                                pict53.BackgroundImage = centralUnidas.Properties.Resources.Totem_OK;
                            else
                                pict53.BackgroundImage = centralUnidas.Properties.Resources.Totem_NOK;
                            lblFranquia5.Text = "R$ " + strCATVALPAR;
                            lblPre5.Text = "R$ " + strCATVALPRE;
                        }

                        //if (this.RemoverAcentos(strCATSRVDSC.ToUpper().Trim()).Contains("PROTECAO SUPER"))
                        if (this.RemoverAcentos(strCATSRVDSC.ToUpper().Trim()).Contains("PROTECAO SUPER")   
                            &&
                            !this.RemoverAcentos(strCATSRVDSC.ToUpper().Trim()).Contains("ZERO")
                            )
                        {
                            lblCod6a.Text = strCATSRVCOD.Trim();
                            lblPre6a.Text = strCATVALPRE.Trim().Replace(".", ",");
                            lblPar6a.Text = strCATVALPAR.Trim().Replace(".", ",");

                            lblValor6.Text = "| R$ " + strCATVLDIA + "/dia";
                            lblValor6a.Text = strCATVLDIA.Replace(".", ",");
                            if (strCATPTGVEILOC.Trim().ToLower() == "true")
                                pict61.BackgroundImage = centralUnidas.Properties.Resources.Totem_OK;
                            else
                                pict61.BackgroundImage = centralUnidas.Properties.Resources.Totem_NOK;
                            if (strCATPTGTERCEIROS.Trim().ToLower() == "true")
                                pict62.BackgroundImage = centralUnidas.Properties.Resources.Totem_OK;
                            else
                                pict62.BackgroundImage = centralUnidas.Properties.Resources.Totem_NOK;
                            if (strCATPTGOCUPANTES.Trim().ToLower() == "true")
                                pict63.BackgroundImage = centralUnidas.Properties.Resources.Totem_OK;
                            else
                                pict63.BackgroundImage = centralUnidas.Properties.Resources.Totem_NOK;
                            lblFranquia6.Text = "R$ " + strCATVALPAR;
                            lblPre6.Text = "R$ " + strCATVALPRE;
                        }

                        if (this.RemoverAcentos(strCATSRVDSC.ToUpper().Trim()).Contains("PROTECAO SUPER ZERO"))
                        {
                            lblCod7a.Text = strCATSRVCOD.Trim();
                            lblPre7a.Text = strCATVALPRE.Trim().Replace(".", ",");
                            lblPar7a.Text = strCATVALPAR.Trim().Replace(".", ",");

                            lblValor7.Text = "| R$ " + strCATVLDIA + "/dia";
                            lblValor7a.Text = strCATVLDIA.Replace(".", ",");
                            if (strCATPTGVEILOC.Trim().ToLower() == "true")
                                pict71.BackgroundImage = centralUnidas.Properties.Resources.Totem_OK;
                            else
                                pict71.BackgroundImage = centralUnidas.Properties.Resources.Totem_NOK;
                            if (strCATPTGTERCEIROS.Trim().ToLower() == "true")
                                pict72.BackgroundImage = centralUnidas.Properties.Resources.Totem_OK;
                            else
                                pict72.BackgroundImage = centralUnidas.Properties.Resources.Totem_NOK;
                            if (strCATPTGOCUPANTES.Trim().ToLower() == "true")
                                pict73.BackgroundImage = centralUnidas.Properties.Resources.Totem_OK;
                            else
                                pict73.BackgroundImage = centralUnidas.Properties.Resources.Totem_NOK;
                            lblFranquia7.Text = "R$ " + strCATVALPAR;
                            lblPre7.Text = "R$ " + strCATVALPRE;
                        }
                    }
                }
                clientWcf.Close();
            }
            catch (Exception ex)
            {
                iRetServico = 2;
            }

            return iRetServico;
        }

        private void pictShutdown_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(1);
        }

        private void lstProtecao_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {

            bool bValid = true;

            //if (checkBox1.Checked == false &&
            //    checkBox2.Checked == false &&
            //    checkBox6.Checked == false &&
            //    checkBox7.Checked == false)
            //{
            //    bValid = false;
            //    Globals.glbMensagem = 1018;
            //    Globals.glbScreen = 1;
            //    frmMensagem frmMsg1018 = new frmMensagem();
            //    frmMsg1018.Show();
            //    this.Close();
            //    this.Dispose();
            //}

            if (bValid)
            {
                string strCATSRVCOD = "";
                string strCATSRVDSC = "";
                string strCATSRVVAL = "";

                Globals.glbProtecoesCodVar1 = "";
                Globals.glbProtecoesVar1 = "";
                Globals.glbProtecoesValorVar1 = "";
                Globals.glbProtecoesValorPre1 = "";
                Globals.glbProtecoesQtdUnit1 = "";

                Globals.glbProtecoesCodVar2 = "";
                Globals.glbProtecoesVar2 = "";
                Globals.glbProtecoesValorVar2 = "";
                Globals.glbProtecoesValorPre2 = "";
                Globals.glbProtecoesQtdUnit2 = "";

                Globals.glbProtecoesCodVar3 = "";
                Globals.glbProtecoesVar3 = "";
                Globals.glbProtecoesValorVar3 = "";
                Globals.glbProtecoesValorPre3 = "";
                Globals.glbProtecoesQtdUnit3 = "";

                int nTotalDiarias = 0;

                if (!string.IsNullOrEmpty(Globals.glbRTBDIARIA))
                {
                    nTotalDiarias = Convert.ToInt32(Globals.glbRTBDIARIA);
                }

                if (checkBox1.Checked == true)
                {
                    Globals.glbProtecoesCodVar1 = lblCod1a.Text;
                    decimal nValorTotalPre = Convert.ToDecimal(lblPre1a.Text);
                    Globals.glbProtecoesValorPre1 = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalPre);

                    decimal nValorTotalPar = Convert.ToDecimal(lblPar1a.Text);
                    Globals.glbProtecoesValorPar1 = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalPar);

                    Globals.glbProtecoesVar1 = "PROTECAO PARCIAL ";
                    decimal nValorTotalProtecao1 = Convert.ToDecimal(lblValor1a.Text) * nTotalDiarias;
                    decimal nValorTotalReserva = Convert.ToDecimal(Globals.glbRTBTOTRES);

                    Globals.glbProtecoesValorVar1 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao1);
                    Globals.glbRTBTOTRES = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalReserva);
                }

                if (checkBox2.Checked == true)
                {
                    decimal nValorTotalPre = Convert.ToDecimal(lblPre2a.Text);
                    Globals.glbProtecoesValorPre1 = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalPre);

                    decimal nValorTotalPar = Convert.ToDecimal(lblPar2a.Text);
                    Globals.glbProtecoesValorPar1 = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalPar);

                    decimal nValorTotalProtecao2 = Convert.ToDecimal(lblValor2a.Text) * nTotalDiarias;
                    decimal nValorTotalReserva = Convert.ToDecimal(Globals.glbRTBTOTRES);

                    if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar1))
                    {
                        Globals.glbProtecoesCodVar1 = lblCod2a.Text;
                        Globals.glbProtecoesVar1 = "PROTECAO ESPECIAL ";
                        Globals.glbProtecoesValorVar1 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao2);
                    }
                    else if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar2))
                    {
                        Globals.glbProtecoesCodVar2 = lblCod2a.Text;
                        Globals.glbProtecoesVar2 = "PROTECAO ESPECIAL ";
                        Globals.glbProtecoesValorVar2 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao2);
                    }
                    Globals.glbRTBTOTRES = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalReserva);
                }

                if (checkBox3.Checked == true)
                {
                    decimal nValorTotalProtecao3 = Convert.ToDecimal(lblValor3a.Text) * nTotalDiarias;
                    decimal nValorTotalReserva = Convert.ToDecimal(Globals.glbRTBTOTRES);

                    if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar1))
                    {
                        Globals.glbProtecoesCodVar1 = lblCod3a.Text;
                        Globals.glbProtecoesVar1 = "PROTECAO A OCUPANTES ";
                        Globals.glbProtecoesValorVar1 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao3);
                    }
                    else if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar2))
                    {
                        Globals.glbProtecoesCodVar2 = lblCod3a.Text;
                        Globals.glbProtecoesVar2 = "PROTECAO A OCUPANTES ";
                        Globals.glbProtecoesValorVar2 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao3);
                    }
                    else if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar3))
                    {
                        Globals.glbProtecoesCodVar3 = lblCod3a.Text;
                        Globals.glbProtecoesVar3 = "PROTECAO A OCUPANTES ";
                        Globals.glbProtecoesValorVar3 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao3);
                    }
                    Globals.glbRTBTOTRES = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalReserva);
                }

                if (checkBox4.Checked == true)
                {
                    decimal nValorTotalProtecao3 = Convert.ToDecimal(lblValor4a.Text) * nTotalDiarias;
                    decimal nValorTotalReserva = Convert.ToDecimal(Globals.glbRTBTOTRES);

                    if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar1))
                    {
                        Globals.glbProtecoesCodVar1 = lblCod4a.Text;
                        Globals.glbProtecoesVar1 = "PROTECAO A TERCEIROS ";
                        Globals.glbProtecoesValorVar1 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao3);
                    }
                    else if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar2))
                    {
                        Globals.glbProtecoesCodVar2 = lblCod4a.Text;
                        Globals.glbProtecoesVar2 = "PROTECAO A TERCEIROS ";
                        Globals.glbProtecoesValorVar2 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao3);
                    }
                    else if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar3))
                    {
                        Globals.glbProtecoesCodVar3 = lblCod4a.Text;
                        Globals.glbProtecoesVar3 = "PROTECAO A TERCEIROS ";
                        Globals.glbProtecoesValorVar3 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao3);
                    }
                    Globals.glbRTBTOTRES = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalReserva);
                }

                if (checkBox5.Checked == true)
                {
                    decimal nValorTotalProtecao1 = Convert.ToDecimal(lblValor5a.Text) * nTotalDiarias;
                    decimal nValorTotalReserva = Convert.ToDecimal(Globals.glbRTBTOTRES);

                    if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar1))
                    {
                        Globals.glbProtecoesCodVar1 = lblCod5a.Text;
                        Globals.glbProtecoesVar1 = "PROTECAO A OCUPANTES E TERCEIROS";
                        Globals.glbProtecoesValorVar1 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao1);
                    }
                    else if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar2))
                    {
                        Globals.glbProtecoesCodVar2 = lblCod5a.Text;
                        Globals.glbProtecoesVar2 = "PROTECAO A OCUPANTES E TERCEIROS";
                        Globals.glbProtecoesValorVar2 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao1);
                    }
                    else if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar3))
                    {
                        Globals.glbProtecoesCodVar3 = lblCod5a.Text;
                        Globals.glbProtecoesVar3 = "PROTECAO A OCUPANTES E TERCEIROS";
                        Globals.glbProtecoesValorVar3 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao1);
                    }
                    Globals.glbRTBTOTRES = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalReserva);
                }

                if (checkBox6.Checked == true)
                {
                    decimal nValorTotalPre = Convert.ToDecimal(lblPre6a.Text);
                    Globals.glbProtecoesValorPre1 = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalPre);

                    decimal nValorTotalPar = Convert.ToDecimal(lblPar6a.Text);
                    Globals.glbProtecoesValorPar1 = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalPar);

                    decimal nValorTotalProtecao1 = Convert.ToDecimal(lblValor6a.Text) * nTotalDiarias;
                    decimal nValorTotalReserva = Convert.ToDecimal(Globals.glbRTBTOTRES);

                    if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar1))
                    {
                        Globals.glbProtecoesCodVar1 = lblCod6a.Text;
                        Globals.glbProtecoesVar1 = "PROTECAO SUPER ";
                        Globals.glbProtecoesValorVar1 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao1);
                    }
                    else if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar2))
                    {
                        Globals.glbProtecoesCodVar2 = lblCod6a.Text;
                        Globals.glbProtecoesVar2 = "PROTECAO SUPER ";
                        Globals.glbProtecoesValorVar2 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao1);
                    }
                    else if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar3))
                    {
                        Globals.glbProtecoesCodVar3 = lblCod6a.Text;
                        Globals.glbProtecoesVar3 = "PROTECAO SUPER ";
                        Globals.glbProtecoesValorVar3 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao1);
                    }
                    Globals.glbRTBTOTRES = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalReserva);
                }

                if (checkBox7.Checked == true)
                {
                    decimal nValorTotalPre = Convert.ToDecimal(lblPre7a.Text);
                    Globals.glbProtecoesValorPre1 = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalPre);

                    decimal nValorTotalPar = Convert.ToDecimal(lblPar7a.Text);
                    Globals.glbProtecoesValorPar1 = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalPar);

                    decimal nValorTotalProtecao1 = Convert.ToDecimal(lblValor7a.Text) * nTotalDiarias;
                    decimal nValorTotalReserva = Convert.ToDecimal(Globals.glbRTBTOTRES);

                    if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar1))
                    {
                        Globals.glbProtecoesCodVar1 = lblCod7a.Text;
                        Globals.glbProtecoesVar1 = "PROTECAO SUPER ZERO ";
                        Globals.glbProtecoesValorVar1 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao1);
                    }
                    else if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar2))
                    {
                        Globals.glbProtecoesCodVar2 = lblCod7a.Text;
                        Globals.glbProtecoesVar2 = "PROTECAO SUPER ZERO ";
                        Globals.glbProtecoesValorVar2 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao1);
                    }
                    else if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar3))
                    {
                        Globals.glbProtecoesCodVar3 = lblCod7a.Text;
                        Globals.glbProtecoesVar3 = "PROTECAO SUPER ZERO ";
                        Globals.glbProtecoesValorVar3 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao1);
                    }
                    Globals.glbRTBTOTRES = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalReserva);
                }

                Globals.glbCATSRVCOD = strCATSRVCOD.Trim();
                Globals.glbCATSRVDSC = strCATSRVDSC.Trim();
                Globals.glbCATSRVVAL = strCATSRVVAL.Trim();

                timerDisabled();

                frmScreen08 frm08 = new frmScreen08();
                frm08.Show();

                protecaoParcial_Show();
                protecaoEspecial_Show();
                protecaoTerceiros_Show();
                protecaoOcupantes_Show();
                protecaoOcupantesTerceiros_Show();
                protecaoSuper_Show();

                this.Close();
                this.Dispose();
            }
        }

        private void tmrReturn_Tick(object sender, EventArgs e)
        {
            tmrReturn.Enabled = false;
            timerDisabled();
            frmScreen01 frm1 = new frmScreen01();
            frm1.Show();
            this.Dispose();
        }

        // protecao parcial
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                checkBox2.Checked = false;
                checkBox6.Checked = false;
                checkBox7.Checked = false;
            }
        }
        // Protecao especial
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            bool bValid = true;
            if (bValid)
            {
                if (checkBox2.Checked == true)
                {
                    checkBox1.Checked = false;
                    checkBox6.Checked = false;
                    checkBox7.Checked = false;
                }
            }
        }
        // sem protecao - inativo
        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {

        }
        // protecao terceiros
        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox4.Checked == true)
            {
                if (checkBox1.Checked == false && checkBox2.Checked == false)
                {
                    checkBox2.Checked = true;
                }
            }
            checkBox5.Checked = false;
        }
        // protecao ocupantes e terceiros
        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            bool bValid = true;
            if (checkBox5.Checked == true)
            {
                checkBox3.Checked = false;
                checkBox4.Checked = false;
                bValid = false;
            }

            if (checkBox5.Checked == true)
            {
                if (checkBox1.Checked == false && checkBox2.Checked == false)
                {
                    checkBox2.Checked = true;
                }
            }
        }
        // protecao super
        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox6.Checked == true)
            {
                checkBox1.Checked = false;
                checkBox2.Checked = false;
                checkBox4.Checked = false;
                checkBox5.Checked = false;
                checkBox7.Checked = false;
            }
        }
        // protecao zero
        private void checkBox7_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox7.Checked == true)
            {
                checkBox1.Checked = false;
                checkBox2.Checked = false;
                checkBox4.Checked = false;
                checkBox5.Checked = false;
                checkBox6.Checked = false;
            }
        }

        private void panel1_Click(object sender, EventArgs e)
        {
            checkBox1.Checked = true;
        }

        private void panel6_Click(object sender, EventArgs e)
        {
            checkBox2.Checked = true;
        }

        private void panel8_Click(object sender, EventArgs e)
        {
            checkBox4.Checked = true;
        }

        private void panel9_Click(object sender, EventArgs e)
        {
            checkBox5.Checked = true;
        }

        private void panel10_Click(object sender, EventArgs e)
        {
            checkBox1.Checked = false;
            checkBox2.Checked = false;
            checkBox4.Checked = false;
            checkBox5.Checked = false;
            checkBox6.Checked = true;
            checkBox7.Checked = false;
        }

        private void panel11_Click(object sender, EventArgs e)
        {
            checkBox1.Checked = false;
            checkBox2.Checked = false;
            checkBox4.Checked = false;
            checkBox5.Checked = false;
            checkBox6.Checked = false;
            checkBox7.Checked = true;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            checkBox1.Checked = true;
        }

        private void label15_Click(object sender, EventArgs e)
        {
            checkBox2.Checked = true;
        }

        private void label17_Click(object sender, EventArgs e)
        {
            checkBox4.Checked = true;
        }

        private void label18_Click(object sender, EventArgs e)
        {
            checkBox5.Checked = true;
        }

        private void label19_Click(object sender, EventArgs e)
        {
            checkBox1.Checked = false;
            checkBox2.Checked = false;
            checkBox4.Checked = false;
            checkBox5.Checked = false;
            checkBox6.Checked = true;
            checkBox7.Checked = false;
        }

        private void label20_Click(object sender, EventArgs e)
        {
            checkBox1.Checked = false;
            checkBox2.Checked = false;
            checkBox4.Checked = false;
            checkBox5.Checked = false;
            checkBox6.Checked = false;
            checkBox7.Checked = true;
        }

        // Parcial
        private void protecaoParcial_Hide()
        {
            checkBox1.Hide();
            panel1.Hide();
            pict11.Hide();
            pict12.Hide();
            pict13.Hide();
            panel12.Hide();
            panel19.Hide();
        }

        private void protecaoParcial_Show()
        {
            checkBox1.Show();
            panel1.Show();
            pict11.Show();
            pict12.Show();
            pict13.Show();
            panel12.Show();
            panel19.Show();
        }

        // Especial
        private void protecaoEspecial_Hide()
        {
            checkBox2.Hide();
            panel6.Hide();
            pict21.Hide();
            pict22.Hide();
            pict23.Hide();
            panel13.Hide();
            panel14.Hide();
        }

        private void protecaoEspecial_Show()
        {
            checkBox2.Show();
            panel6.Show();
            pict21.Show();
            pict22.Show();
            pict23.Show();
            panel13.Show();
            panel14.Show();
        }

        private void protecaoEspecial_MoveUp(int nFator)
        {
            checkBox2.Location = new Point(checkBox2.Location.X, checkBox2.Location.Y - (54 * nFator));
            checkBox2.Refresh();

            panel6.Location = new Point(panel6.Location.X, panel6.Location.Y - (54 * nFator));
            panel6.Refresh();

            pict21.Location = new Point(pict21.Location.X, pict21.Location.Y - (54 * nFator));
            pict21.Refresh();

            pict22.Location = new Point(pict22.Location.X, pict22.Location.Y - (54 * nFator));
            pict22.Refresh();

            pict23.Location = new Point(pict23.Location.X, pict23.Location.Y - (54 * nFator));
            pict23.Refresh();

            panel13.Location = new Point(panel13.Location.X, panel13.Location.Y - (54 * nFator));
            panel13.Refresh();

            panel14.Location = new Point(panel14.Location.X, panel14.Location.Y - (54 * nFator));
            panel14.Refresh();
        }

        // OCUPANTES
        private void protecaoOcupantes_Hide()
        {
            checkBox3.Hide();
            panel26.Hide();
            pict31.Hide();
            pict32.Hide();
            pict33.Hide();
            panel25.Hide();
            panel24.Hide();
        }

        private void protecaoOcupantes_Show()
        {
            checkBox3.Show();
            panel26.Show();
            pict31.Show();
            pict32.Show();
            pict33.Show();
            panel25.Show();
            panel24.Show();
        }

        private void protecaoOcupantes_MoveUp(int nFator)
        {
            checkBox3.Location = new Point(checkBox3.Location.X, checkBox3.Location.Y - (54 * nFator));
            checkBox3.Refresh();

            panel26.Location = new Point(panel26.Location.X, panel26.Location.Y - (54 * nFator));
            panel26.Refresh();

            pict31.Location = new Point(pict31.Location.X, pict31.Location.Y - (54 * nFator));
            pict31.Refresh();

            pict32.Location = new Point(pict32.Location.X, pict32.Location.Y - (54 * nFator));
            pict32.Refresh();

            pict33.Location = new Point(pict33.Location.X, pict33.Location.Y - (54 * nFator));
            pict33.Refresh();

            panel25.Location = new Point(panel25.Location.X, panel25.Location.Y - (54 * nFator));
            panel25.Refresh();

            panel24.Location = new Point(panel24.Location.X, panel24.Location.Y - (54 * nFator));
            panel24.Refresh();
        }


        // Terceiros
        private void protecaoTerceiros_Hide()
        {
            checkBox4.Hide();
            panel8.Hide();
            pict41.Hide();
            pict42.Hide();
            pict43.Hide();
            panel15.Hide();
            panel20.Hide();
        }

        private void protecaoTerceiros_Show()
        {
            checkBox4.Show();
            panel8.Show();
            pict41.Show();
            pict42.Show();
            pict43.Show();
            panel15.Show();
            panel20.Show();
        }
        private void protecaoTerceiros_MoveUp(int nFator)
        {
            checkBox4.Location = new Point(checkBox4.Location.X, checkBox4.Location.Y - (54 * nFator));
            checkBox4.Refresh();

            panel8.Location = new Point(panel8.Location.X, panel8.Location.Y - (54 * nFator));
            panel8.Refresh();

            pict41.Location = new Point(pict41.Location.X, pict41.Location.Y - (54 * nFator));
            pict41.Refresh();

            pict42.Location = new Point(pict42.Location.X, pict42.Location.Y - (54 * nFator));
            pict42.Refresh();

            pict43.Location = new Point(pict43.Location.X, pict43.Location.Y - (54 * nFator));
            pict43.Refresh();

            panel15.Location = new Point(panel15.Location.X, panel15.Location.Y - (54 * nFator));
            panel15.Refresh();

            panel20.Location = new Point(panel20.Location.X, panel20.Location.Y - (54 * nFator));
            panel20.Refresh();
        }

        // Ocupantes
        private void protecaoOcupantesTerceiros_Hide()
        {
            checkBox5.Hide();
            panel9.Hide();
            pict51.Hide();
            pict52.Hide();
            pict53.Hide();
            panel16.Hide();
            panel21.Hide();
        }

        private void protecaoOcupantesTerceiros_Show()
        {
            checkBox5.Show();
            panel9.Show();
            pict51.Show();
            pict52.Show();
            pict53.Show();
            panel16.Show();
            panel21.Show();
        }

        private void protecaoOcupantesTerceiros_MoveUp(int nFator)
        {
            checkBox5.Location = new Point(checkBox5.Location.X, checkBox5.Location.Y - (54 * nFator));
            checkBox5.Refresh();

            panel9.Location = new Point(panel9.Location.X, panel9.Location.Y - (54 * nFator));
            panel9.Refresh();

            pict51.Location = new Point(pict51.Location.X, pict51.Location.Y - (54 * nFator));
            pict51.Refresh();

            pict52.Location = new Point(pict52.Location.X, pict52.Location.Y - (54 * nFator));
            pict52.Refresh();

            pict53.Location = new Point(pict53.Location.X, pict53.Location.Y - (54 * nFator));
            pict53.Refresh();

            panel16.Location = new Point(panel16.Location.X, panel16.Location.Y - (54 * nFator));
            panel16.Refresh();

            panel21.Location = new Point(panel21.Location.X, panel21.Location.Y - (54 * nFator));
            panel21.Refresh();
        }

        // Super
        private void protecaoSuper_Hide()
        {
            checkBox6.Hide();
            panel10.Hide();
            pict61.Hide();
            pict62.Hide();
            pict63.Hide();
            panel17.Hide();
            panel22.Hide();
        }

        private void protecaoSuper_Show()
        {
            checkBox6.Show();
            panel10.Show();
            pict61.Show();
            pict62.Show();
            pict63.Show();
            panel17.Show();
            panel22.Show();
        }

        private void protecaoSuper_MoveUp(int nFator)
        {
            checkBox6.Location = new Point(checkBox6.Location.X, checkBox6.Location.Y - (54 * nFator));
            checkBox6.Refresh();

            panel10.Location = new Point(panel10.Location.X, panel10.Location.Y - (54 * nFator));
            panel10.Refresh();

            pict61.Location = new Point(pict61.Location.X, pict61.Location.Y - (54 * nFator));
            pict61.Refresh();

            pict62.Location = new Point(pict62.Location.X, pict62.Location.Y - (54 * nFator));
            pict62.Refresh();

            pict63.Location = new Point(pict63.Location.X, pict63.Location.Y - (54 * nFator));
            pict63.Refresh();

            panel17.Location = new Point(panel17.Location.X, panel17.Location.Y - (54 * nFator));
            panel17.Refresh();

            panel22.Location = new Point(panel22.Location.X, panel22.Location.Y - (54 * nFator));
            panel22.Refresh();
        }

        // Zero
        private void protecaoZero_Hide()
        {
            checkBox7.Hide();
            panel11.Hide();
            pict71.Hide();
            pict72.Hide();
            pict73.Hide();
            panel18.Hide();
            panel23.Hide();
        }

        private void protecaoZero_Show()
        {
            checkBox7.Show();
            panel11.Show();
            pict71.Show();
            pict72.Show();
            pict73.Show();
            panel18.Show();
            panel23.Show();
        }

        private void protecaoZero_MoveUp(int nFator)
        {
            checkBox7.Location = new Point(checkBox7.Location.X, checkBox7.Location.Y - (54 * nFator));
            checkBox7.Refresh();

            panel11.Location = new Point(panel11.Location.X, panel11.Location.Y - (54 * nFator));
            panel11.Refresh();

            pict71.Location = new Point(pict71.Location.X, pict71.Location.Y - (54 * nFator));
            pict71.Refresh();

            pict72.Location = new Point(pict72.Location.X, pict72.Location.Y - (54 * nFator));
            pict72.Refresh();

            pict73.Location = new Point(pict73.Location.X, pict73.Location.Y - (54 * nFator));
            pict73.Refresh();

            panel18.Location = new Point(panel18.Location.X, panel18.Location.Y - (54 * nFator));
            panel18.Refresh();

            panel23.Location = new Point(panel23.Location.X, panel23.Location.Y - (54 * nFator));
            panel23.Refresh();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {

            tmrReturn.Enabled = false;
            timerDisabled();
            frmScreen06 frm06 = new frmScreen06();
            frm06.Show();
            this.Close();
            this.Dispose();
        }

        private void label26_Click(object sender, EventArgs e)
        {

        }

        private void label29_Click(object sender, EventArgs e)
        {
            checkBox3.Checked = true;
        }

        private void panel26_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel26_Click(object sender, EventArgs e)
        {
            checkBox3.Checked = true;
        }

        private void panel9_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel8_Paint(object sender, PaintEventArgs e)
        {

        }

        private void checkBox3_CheckedChanged_1(object sender, EventArgs e)
        {
            if (checkBox3.Checked == true)
            {
                if (checkBox1.Checked == false && checkBox2.Checked == false)
                {
                    checkBox2.Checked = true;
                }
            }
            checkBox5.Checked = false;
        }

        private string RemoverAcentos(string texto)
        {
            if (string.IsNullOrEmpty(texto))
                return String.Empty;
            else
            {
                byte[] bytes = System.Text.Encoding.GetEncoding("iso-8859-8").GetBytes(texto);
                return System.Text.Encoding.UTF8.GetString(bytes);
            }
        }

        private void butVoltar_Click(object sender, EventArgs e)
        {
            tmrReturn.Enabled = false;
            timerDisabled();
            frmScreen06 frm06 = new frmScreen06();
            frm06.Show();
            this.Close();
            this.Dispose();
        }

        private void butConfirmar_Click(object sender, EventArgs e)
        {
            bool bValid = true;

            //if (checkBox1.Checked == false &&
            //    checkBox2.Checked == false &&
            //    checkBox6.Checked == false &&
            //    checkBox7.Checked == false)
            //{
            //    bValid = false;
            //    Globals.glbMensagem = 1018;
            //    Globals.glbScreen = 1;
            //    frmMensagem frmMsg1018 = new frmMensagem();
            //    frmMsg1018.Show();
            //    this.Close();
            //    this.Dispose();
            //}

            if (bValid)
            {
                string strCATSRVCOD = "";
                string strCATSRVDSC = "";
                string strCATSRVVAL = "";

                Globals.glbProtecoesCodVar1 = "";
                Globals.glbProtecoesVar1 = "";
                Globals.glbProtecoesValorVar1 = "";
                Globals.glbProtecoesValorPre1 = "";
                Globals.glbProtecoesQtdUnit1 = "";

                Globals.glbProtecoesCodVar2 = "";
                Globals.glbProtecoesVar2 = "";
                Globals.glbProtecoesValorVar2 = "";
                Globals.glbProtecoesValorPre2 = "";
                Globals.glbProtecoesQtdUnit2 = "";

                Globals.glbProtecoesCodVar3 = "";
                Globals.glbProtecoesVar3 = "";
                Globals.glbProtecoesValorVar3 = "";
                Globals.glbProtecoesValorPre3 = "";
                Globals.glbProtecoesQtdUnit3 = "";

                int nTotalDiarias = 0;

                if (!string.IsNullOrEmpty(Globals.glbRTBDIARIA))
                {
                    nTotalDiarias = Convert.ToInt32(Globals.glbRTBDIARIA);
                }

                if (checkBox1.Checked == true)
                {
                    Globals.glbProtecoesCodVar1 = lblCod1a.Text;
                    decimal nValorTotalPre = Convert.ToDecimal(lblPre1a.Text);
                    Globals.glbProtecoesValorPre1 = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalPre);

                    decimal nValorTotalPar = Convert.ToDecimal(lblPar1a.Text);
                    Globals.glbProtecoesValorPar1 = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalPar);

                    Globals.glbProtecoesVar1 = "PROTECAO PARCIAL ";
                    decimal nValorTotalProtecao1 = Convert.ToDecimal(lblValor1a.Text) * nTotalDiarias;
                    decimal nValorTotalReserva = Convert.ToDecimal(Globals.glbRTBTOTRES);

                    Globals.glbProtecoesValorVar1 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao1);
                    Globals.glbRTBTOTRES = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalReserva);
                }

                if (checkBox2.Checked == true)
                {
                    decimal nValorTotalPre = Convert.ToDecimal(lblPre2a.Text);
                    Globals.glbProtecoesValorPre1 = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalPre);

                    decimal nValorTotalPar = Convert.ToDecimal(lblPar2a.Text);
                    Globals.glbProtecoesValorPar1 = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalPar);

                    decimal nValorTotalProtecao2 = Convert.ToDecimal(lblValor2a.Text) * nTotalDiarias;
                    decimal nValorTotalReserva = Convert.ToDecimal(Globals.glbRTBTOTRES);

                    if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar1))
                    {
                        Globals.glbProtecoesCodVar1 = lblCod2a.Text;
                        Globals.glbProtecoesVar1 = "PROTECAO ESPECIAL ";
                        Globals.glbProtecoesValorVar1 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao2);
                    }
                    else if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar2))
                    {
                        Globals.glbProtecoesCodVar2 = lblCod2a.Text;
                        Globals.glbProtecoesVar2 = "PROTECAO ESPECIAL ";
                        Globals.glbProtecoesValorVar2 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao2);
                    }
                    Globals.glbRTBTOTRES = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalReserva);
                }

                if (checkBox3.Checked == true)
                {
                    decimal nValorTotalProtecao3 = Convert.ToDecimal(lblValor3a.Text) * nTotalDiarias;
                    decimal nValorTotalReserva = Convert.ToDecimal(Globals.glbRTBTOTRES);

                    if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar1))
                    {
                        Globals.glbProtecoesCodVar1 = lblCod3a.Text;
                        Globals.glbProtecoesVar1 = "PROTECAO A OCUPANTES ";
                        Globals.glbProtecoesValorVar1 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao3);
                    }
                    else if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar2))
                    {
                        Globals.glbProtecoesCodVar2 = lblCod3a.Text;
                        Globals.glbProtecoesVar2 = "PROTECAO A OCUPANTES ";
                        Globals.glbProtecoesValorVar2 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao3);
                    }
                    else if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar3))
                    {
                        Globals.glbProtecoesCodVar3 = lblCod3a.Text;
                        Globals.glbProtecoesVar3 = "PROTECAO A OCUPANTES ";
                        Globals.glbProtecoesValorVar3 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao3);
                    }
                    Globals.glbRTBTOTRES = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalReserva);
                }

                if (checkBox4.Checked == true)
                {
                    decimal nValorTotalProtecao3 = Convert.ToDecimal(lblValor4a.Text) * nTotalDiarias;
                    decimal nValorTotalReserva = Convert.ToDecimal(Globals.glbRTBTOTRES);

                    if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar1))
                    {
                        Globals.glbProtecoesCodVar1 = lblCod4a.Text;
                        Globals.glbProtecoesVar1 = "PROTECAO A TERCEIROS ";
                        Globals.glbProtecoesValorVar1 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao3);
                    }
                    else if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar2))
                    {
                        Globals.glbProtecoesCodVar2 = lblCod4a.Text;
                        Globals.glbProtecoesVar2 = "PROTECAO A TERCEIROS ";
                        Globals.glbProtecoesValorVar2 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao3);
                    }
                    else if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar3))
                    {
                        Globals.glbProtecoesCodVar3 = lblCod4a.Text;
                        Globals.glbProtecoesVar3 = "PROTECAO A TERCEIROS ";
                        Globals.glbProtecoesValorVar3 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao3);
                    }
                    Globals.glbRTBTOTRES = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalReserva);
                }

                if (checkBox5.Checked == true)
                {
                    decimal nValorTotalProtecao1 = Convert.ToDecimal(lblValor5a.Text) * nTotalDiarias;
                    decimal nValorTotalReserva = Convert.ToDecimal(Globals.glbRTBTOTRES);

                    if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar1))
                    {
                        Globals.glbProtecoesCodVar1 = lblCod5a.Text;
                        Globals.glbProtecoesVar1 = "PROTECAO A OCUPANTES E TERCEIROS";
                        Globals.glbProtecoesValorVar1 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao1);
                    }
                    else if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar2))
                    {
                        Globals.glbProtecoesCodVar2 = lblCod5a.Text;
                        Globals.glbProtecoesVar2 = "PROTECAO A OCUPANTES E TERCEIROS";
                        Globals.glbProtecoesValorVar2 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao1);
                    }
                    else if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar3))
                    {
                        Globals.glbProtecoesCodVar3 = lblCod5a.Text;
                        Globals.glbProtecoesVar3 = "PROTECAO A OCUPANTES E TERCEIROS";
                        Globals.glbProtecoesValorVar3 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao1);
                    }
                    Globals.glbRTBTOTRES = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalReserva);
                }

                if (checkBox6.Checked == true)
                {
                    decimal nValorTotalPre = Convert.ToDecimal(lblPre6a.Text);
                    Globals.glbProtecoesValorPre1 = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalPre);

                    decimal nValorTotalPar = Convert.ToDecimal(lblPar6a.Text);
                    Globals.glbProtecoesValorPar1 = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalPar);

                    decimal nValorTotalProtecao1 = Convert.ToDecimal(lblValor6a.Text) * nTotalDiarias;
                    decimal nValorTotalReserva = Convert.ToDecimal(Globals.glbRTBTOTRES);

                    if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar1))
                    {
                        Globals.glbProtecoesCodVar1 = lblCod6a.Text;
                        Globals.glbProtecoesVar1 = "PROTECAO SUPER ";
                        Globals.glbProtecoesValorVar1 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao1);
                    }
                    else if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar2))
                    {
                        Globals.glbProtecoesCodVar2 = lblCod6a.Text;
                        Globals.glbProtecoesVar2 = "PROTECAO SUPER ";
                        Globals.glbProtecoesValorVar2 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao1);
                    }
                    else if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar3))
                    {
                        Globals.glbProtecoesCodVar3 = lblCod6a.Text;
                        Globals.glbProtecoesVar3 = "PROTECAO SUPER ";
                        Globals.glbProtecoesValorVar3 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao1);
                    }
                    Globals.glbRTBTOTRES = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalReserva);
                }

                if (checkBox7.Checked == true)
                {
                    decimal nValorTotalPre = Convert.ToDecimal(lblPre7a.Text);
                    Globals.glbProtecoesValorPre1 = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalPre);

                    decimal nValorTotalPar = Convert.ToDecimal(lblPar7a.Text);
                    Globals.glbProtecoesValorPar1 = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalPar);

                    decimal nValorTotalProtecao1 = Convert.ToDecimal(lblValor7a.Text) * nTotalDiarias;
                    decimal nValorTotalReserva = Convert.ToDecimal(Globals.glbRTBTOTRES);

                    if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar1))
                    {
                        Globals.glbProtecoesCodVar1 = lblCod7a.Text;
                        Globals.glbProtecoesVar1 = "PROTECAO SUPER ZERO ";
                        Globals.glbProtecoesValorVar1 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao1);
                    }
                    else if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar2))
                    {
                        Globals.glbProtecoesCodVar2 = lblCod7a.Text;
                        Globals.glbProtecoesVar2 = "PROTECAO SUPER ZERO ";
                        Globals.glbProtecoesValorVar2 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao1);
                    }
                    else if (string.IsNullOrEmpty(Globals.glbProtecoesCodVar3))
                    {
                        Globals.glbProtecoesCodVar3 = lblCod7a.Text;
                        Globals.glbProtecoesVar3 = "PROTECAO SUPER ZERO ";
                        Globals.glbProtecoesValorVar3 = string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalProtecao1);
                    }
                    Globals.glbRTBTOTRES = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorTotalReserva);
                }

                Globals.glbCATSRVCOD = strCATSRVCOD.Trim();
                Globals.glbCATSRVDSC = strCATSRVDSC.Trim();
                Globals.glbCATSRVVAL = strCATSRVVAL.Trim();

                timerDisabled();

                frmScreen08 frm08 = new frmScreen08();
                frm08.Show();

                protecaoParcial_Show();
                protecaoEspecial_Show();
                protecaoTerceiros_Show();
                protecaoOcupantes_Show();
                protecaoOcupantesTerceiros_Show();
                protecaoSuper_Show();

                this.Close();
                this.Dispose();
            }
        }

        private void butIdioma_Click(object sender, EventArgs e)
        {
            frmIdioma frmIdi = new frmIdioma();
            frmIdi.ShowDialog();
            AtualizarInterfaceParaIdiomaCorrente();
        }

        private void AtualizarInterfaceParaIdiomaCorrente()
        {

            MontarResourceSetParaIdiomaCorrente();

            butIdioma.Text = ResxSetForm.GetString("butIdioma.Text");
            butVoltar.Text = ResxSetForm.GetString("butVoltar.Text");
            butConfirmar.Text = ResxSetForm.GetString("butConfirmar.Text");
            lblProtecaoEspecial.Text = ResxSetForm.GetString("lblProtecaoEspecial.Text");
            lblProtecaoOcupantes.Text = ResxSetForm.GetString("lblProtecaoOcupantes.Text");
            lblProtecaoOcupantesETerceiros.Text = ResxSetForm.GetString(" lblProtecaoOcupantesETerceiros.Text");
            lblProtecaoParcial.Text = ResxSetForm.GetString("lblProtecaoParcial.Text");
            lblProtecaoSuper.Text = ResxSetForm.GetString(" lblProtecaoSuper.Text");
            lblProtecaoSuperZero.Text = ResxSetForm.GetString("lblProtecaoSuperZero.Text");
            lblProtecaoTerceiros.Text = ResxSetForm.GetString("lblProtecaoTerceiros.Text");
            lblProtegeDanosTerceiros.Text = ResxSetForm.GetString("lblProtegeDanosTerceiros.Text");
            lblProtegeOcupantesCasoAcidentes.Text = ResxSetForm.GetString("lblProtegeOcupantesCasoAcidentes.Text");
            lblProtegeVeiculoLocado.Text = ResxSetForm.GetString("lblProtegeVeiculoLocado.Text");
            lblQuerMelhorarProtecao.Text = ResxSetForm.GetString("lblQuerMelhorarProtecao.Text");
            lblValorLancamentoPreAutorizacao.Text = ResxSetForm.GetString("lblValorLancamentoPreAutorizacao.Text");
            lblValorMinimoFranquia.Text = ResxSetForm.GetString("lblValorMinimoFranquia.Text");

            if (Globals.glbLanguage == 1) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_Brasil_botao;
            if (Globals.glbLanguage == 2) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_Espanha_botao;
            if (Globals.glbLanguage == 3) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_UK_botao;

            butIdioma.Refresh();
        }

        private void MontarResourceSetParaIdiomaCorrente()
        {
            string srtCaminhoExeTotem = System.IO.Path.GetDirectoryName(Application.ExecutablePath.ToString());
            // Português
            if (Globals.glbLanguage == 1)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen07.pt.resx");
            }
            //Espanhol
            else if (Globals.glbLanguage == 2)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen07.es.resx");
            }
            // Inglês
            else if (Globals.glbLanguage == 3)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen07.en.resx");
            }
        }

        private void timerDisabled()
        {
            tmrReturn.Enabled = false;
        }


    }
}
