﻿// Aplicação: Unidas Totem
// Data     : 01/10/2015
// Versão   : 1.0
// Empresa  : Unidas Rent a Car
// Equipe   : Unidas Desenvolvimento
// Objetivo : Informe sobre rodizio
// Histórico: 
//      01/10/2015: Inicio de desenvolvimento para acesso geral
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace centralUnidas
{
    public partial class frmScreen06A : Form
    {
        public ResXResourceSet ResxSetForm; 

        public frmScreen06A()
        {
            InitializeComponent();
        }

        private void frmScreen06A_Load(object sender, EventArgs e)
        {
            AtualizarInterfaceParaIdiomaCorrente();

            LogTotem.InsertLog(new LogOperacao()
            {
                Application_Name = Globals.glbApplicationName,
                Application_Version = Globals.glbApplicationVersion,
                Local = Globals.glbLojaTotem,
                Operation_Ticks = Globals.glbOperationTicks,
                Operation_Name = "Mensagem de Rodízio",
                Form = "FrmScreen06A",
                RESNUM = Convert.ToInt32(Globals.glbReservaNumeroPesquisa)
            });
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Globals.glbRA_r_atipo.Trim()))
            {
                if (Globals.glbRA_r_atipo.Trim().ToUpper() == "F")
                {
                    frmScreen07 frm07 = new frmScreen07();
                    frm07.Show();
                    this.Dispose();
                }
                else
                {
                    frmScreen08 frm08 = new frmScreen08();
                    frm08.Show();
                    this.Dispose();
                }
            }
            else
            {
                frmScreen07 frm07 = new frmScreen07();
                frm07.Show();
                this.Dispose();
            }

        }

        private void butContinuar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Globals.glbRA_r_atipo.Trim()))
            {
                if (Globals.glbRA_r_atipo.Trim().ToUpper() == "F")
                {
                    frmScreen07 frm07 = new frmScreen07();
                    frm07.Show();
                    this.Dispose();
                }
                else
                {
                    frmScreen08 frm08 = new frmScreen08();
                    frm08.Show();
                    this.Dispose();
                }
            }
            else
            {
                frmScreen07 frm07 = new frmScreen07();
                frm07.Show();
                this.Dispose();
            }
        }

        private void AtualizarInterfaceParaIdiomaCorrente()
        {

            MontarResourceSetParaIdiomaCorrente();

            //butContinuar.Text = ResxSetForm.GetString("butContinuar.Text");

            //string sCaminho = System.IO.Path.GetDirectoryName(Application.ExecutablePath.ToString()) + "\\" + DeterminaTextoRodizioNoIdiomaAtual();

            string scaminho = System.IO.Path.GetDirectoryName(Application.ExecutablePath.ToString()) + "\\" + DeterminaTextoRodizioNoIdiomaAtual();
            //string scaminho = System.IO.Path.GetDirectoryName(Application.ExecutablePath.ToString()) + "\\Rodizio_PT.rtf";

            //string strTexto = File.ReadAllText(scaminho);

            rtboxAvisoRodizio.LoadFile(scaminho, RichTextBoxStreamType.RichText);
            //rtboxAvisoRodizio.Text = strTexto;
            //richTextBox1.LoadFile(scaminho, RichTextBoxStreamType.PlainText);
           
        }

        private void MontarResourceSetParaIdiomaCorrente()
        {
            string srtCaminhoExeTotem = System.IO.Path.GetDirectoryName(Application.ExecutablePath.ToString());
            // Português
            if (Globals.glbLanguage == 1)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen06A.pt.resx");
            }
            //Espanhol
            else if (Globals.glbLanguage == 2)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen06A.es.resx");
            }
            // Inglês
            else if (Globals.glbLanguage == 3)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen06A.en.resx");
            }
        }

        private string DeterminaTextoRodizioNoIdiomaAtual()
        {
            try
            {
                if (Globals.glbLanguage == 1)
                    return "AvisoRodizio_PT.rtf";
                else if (Globals.glbLanguage == 2)
                    return "AvisoRodizio_ES.rtf";
                else if (Globals.glbLanguage == 3)
                    return "AvisoRodizio_EN.rtf";
                else
                    return "AvisoRodizio_PT.rtf";
            }
            catch (Exception ex)
            {
                
                throw ex;
            }

            return "";
        }
    }
}
