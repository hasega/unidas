﻿// Aplicação: Unidas Totem
// Data     : 01/10/2015
// Versão   : 1.0
// Empresa  : Unidas Rent a Car
// Equipe   : Unidas Desenvolvimento
// Histórico: 
//      01/10/2015: Inicio de desenvolvimento para acesso geral
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace centralUnidas
{
    public partial class frmIdioma : Form
    {
        public frmIdioma()
        {
            InitializeComponent();
        }

        private void pictBrasil_Click(object sender, EventArgs e)
        {
            pnlBrasil.BackColor = Color.Blue;
            pnlUK.BackColor = Color.Transparent;
            pnlEspanha.BackColor = Color.Transparent;
            Globals.glbLanguage = 1;
            tmrIdioma.Enabled = true;
        }

        private void pictUK_Click(object sender, EventArgs e)
        {
            pnlBrasil.BackColor = Color.Transparent;
            pnlUK.BackColor = Color.Blue;
            pnlEspanha.BackColor = Color.Transparent;
            Globals.glbLanguage = 3;
            tmrIdioma.Enabled = true;
        }

        private void pictEspanha_Click(object sender, EventArgs e)
        {
            pnlBrasil.BackColor = Color.Transparent;
            pnlUK.BackColor = Color.Transparent;
            pnlEspanha.BackColor = Color.Blue;
            Globals.glbLanguage = 2;
            tmrIdioma.Enabled = true;
        }

        private void frmIdioma_Load(object sender, EventArgs e)
        {

        }

        private void pnlBrasil_Click(object sender, EventArgs e)
        {
            pnlBrasil.BackColor = Color.Blue;
            pnlUK.BackColor = Color.Transparent;
            pnlEspanha.BackColor = Color.Transparent;
            Globals.glbLanguage = 1;
            tmrIdioma.Enabled = true;
        }

        private void pnlUK_Click(object sender, EventArgs e)
        {
            pnlBrasil.BackColor = Color.Transparent;
            pnlUK.BackColor = Color.Blue;
            pnlEspanha.BackColor = Color.Transparent;
            Globals.glbLanguage = 3;
            tmrIdioma.Enabled = true;
        }

        private void pnlEspanha_Click(object sender, EventArgs e)
        {
            pnlBrasil.BackColor = Color.Transparent;
            pnlUK.BackColor = Color.Transparent;
            pnlEspanha.BackColor = Color.Blue;
            Globals.glbLanguage = 2;
            tmrIdioma.Enabled = true;
        }

        private void tmrIdioma_Tick(object sender, EventArgs e)
        {
            tmrIdioma.Enabled = false;
            this.Close();
            this.Hide();
            this.Dispose();
        }

        private void tmrReturn_Tick(object sender, EventArgs e)
        {
            tmrReturn.Enabled = false;
            frmScreen01 frm1 = new frmScreen01();
            frm1.Show();
            this.Dispose();
        }

        private void pnlBrasil_Paint(object sender, PaintEventArgs e)
        {

        }

        private void funcaoSetLanguage()
        {
            // funcao criada para teste por Marcos Romero 1
        }

        private void funcaoSetLanguageBranch()
        {
            // funcao criada para teste por Marcos Romero
        }
    }
}
