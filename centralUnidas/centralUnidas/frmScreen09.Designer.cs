﻿namespace centralUnidas
{
    partial class frmScreen09
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmScreen09));
            this.pictShutdown = new System.Windows.Forms.PictureBox();
            this.pictLang = new System.Windows.Forms.PictureBox();
            this.pictUnidasLogo = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.tmrReturn = new System.Windows.Forms.Timer(this.components);
            this.lblSelecioneGrupo1 = new System.Windows.Forms.Label();
            this.lblTextoEsteValorSeraPreAutorizado = new System.Windows.Forms.Label();
            this.lblTextoAposADevolucaoDoVeiculo = new System.Windows.Forms.Label();
            this.lblPreAutorizacaoLocacao = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tmrRotina = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictShutdown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictLang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictShutdown
            // 
            this.pictShutdown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictShutdown.BackColor = System.Drawing.Color.Transparent;
            this.pictShutdown.Image = global::centralUnidas.Properties.Resources.shutdown_button;
            this.pictShutdown.Location = new System.Drawing.Point(12, 181);
            this.pictShutdown.Name = "pictShutdown";
            this.pictShutdown.Size = new System.Drawing.Size(50, 55);
            this.pictShutdown.TabIndex = 138;
            this.pictShutdown.TabStop = false;
            this.pictShutdown.Visible = false;
            this.pictShutdown.Click += new System.EventHandler(this.pictShutdown_Click);
            // 
            // pictLang
            // 
            this.pictLang.Image = ((System.Drawing.Image)(resources.GetObject("pictLang.Image")));
            this.pictLang.Location = new System.Drawing.Point(1148, 0);
            this.pictLang.Name = "pictLang";
            this.pictLang.Size = new System.Drawing.Size(148, 61);
            this.pictLang.TabIndex = 140;
            this.pictLang.TabStop = false;
            this.pictLang.Visible = false;
            this.pictLang.Click += new System.EventHandler(this.pictLang_Click);
            // 
            // pictUnidasLogo
            // 
            this.pictUnidasLogo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictUnidasLogo.BackColor = System.Drawing.Color.Transparent;
            this.pictUnidasLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictUnidasLogo.Image = global::centralUnidas.Properties.Resources.totem_header;
            this.pictUnidasLogo.Location = new System.Drawing.Point(0, 0);
            this.pictUnidasLogo.Name = "pictUnidasLogo";
            this.pictUnidasLogo.Size = new System.Drawing.Size(1374, 140);
            this.pictUnidasLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictUnidasLogo.TabIndex = 139;
            this.pictUnidasLogo.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox8.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox8.Image = global::centralUnidas.Properties.Resources.Totem_Saiba_Mais;
            this.pictureBox8.Location = new System.Drawing.Point(7, 234);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(459, 30);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox8.TabIndex = 149;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.Visible = false;
            // 
            // tmrReturn
            // 
            this.tmrReturn.Enabled = true;
            this.tmrReturn.Interval = 60000;
            this.tmrReturn.Tick += new System.EventHandler(this.tmrReturn_Tick);
            // 
            // lblSelecioneGrupo1
            // 
            this.lblSelecioneGrupo1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSelecioneGrupo1.BackColor = System.Drawing.Color.White;
            this.lblSelecioneGrupo1.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold);
            this.lblSelecioneGrupo1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblSelecioneGrupo1.Location = new System.Drawing.Point(0, 234);
            this.lblSelecioneGrupo1.Name = "lblSelecioneGrupo1";
            this.lblSelecioneGrupo1.Size = new System.Drawing.Size(1374, 29);
            this.lblSelecioneGrupo1.TabIndex = 154;
            this.lblSelecioneGrupo1.Text = "Valor a ser cobrado: R$";
            this.lblSelecioneGrupo1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblSelecioneGrupo1.Click += new System.EventHandler(this.lblSelecioneGrupo1_Click);
            // 
            // lblTextoEsteValorSeraPreAutorizado
            // 
            this.lblTextoEsteValorSeraPreAutorizado.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTextoEsteValorSeraPreAutorizado.AutoSize = true;
            this.lblTextoEsteValorSeraPreAutorizado.BackColor = System.Drawing.Color.White;
            this.lblTextoEsteValorSeraPreAutorizado.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lblTextoEsteValorSeraPreAutorizado.ForeColor = System.Drawing.Color.Gray;
            this.lblTextoEsteValorSeraPreAutorizado.Location = new System.Drawing.Point(422, 714);
            this.lblTextoEsteValorSeraPreAutorizado.Name = "lblTextoEsteValorSeraPreAutorizado";
            this.lblTextoEsteValorSeraPreAutorizado.Size = new System.Drawing.Size(567, 19);
            this.lblTextoEsteValorSeraPreAutorizado.TabIndex = 155;
            this.lblTextoEsteValorSeraPreAutorizado.Text = "* Este valor será pré-autorizado em seu cartão como garantia da locação. ";
            this.lblTextoEsteValorSeraPreAutorizado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTextoAposADevolucaoDoVeiculo
            // 
            this.lblTextoAposADevolucaoDoVeiculo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTextoAposADevolucaoDoVeiculo.AutoSize = true;
            this.lblTextoAposADevolucaoDoVeiculo.BackColor = System.Drawing.Color.White;
            this.lblTextoAposADevolucaoDoVeiculo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lblTextoAposADevolucaoDoVeiculo.ForeColor = System.Drawing.Color.Gray;
            this.lblTextoAposADevolucaoDoVeiculo.Location = new System.Drawing.Point(300, 740);
            this.lblTextoAposADevolucaoDoVeiculo.Name = "lblTextoAposADevolucaoDoVeiculo";
            this.lblTextoAposADevolucaoDoVeiculo.Size = new System.Drawing.Size(841, 19);
            this.lblTextoAposADevolucaoDoVeiculo.TabIndex = 156;
            this.lblTextoAposADevolucaoDoVeiculo.Text = "Após a devolução do veículo, caso não haja avarias, o valor será liberado novamen" +
    "te em seu limite de crédito.";
            this.lblTextoAposADevolucaoDoVeiculo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPreAutorizacaoLocacao
            // 
            this.lblPreAutorizacaoLocacao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPreAutorizacaoLocacao.BackColor = System.Drawing.Color.White;
            this.lblPreAutorizacaoLocacao.Font = new System.Drawing.Font("Arial", 26F, System.Drawing.FontStyle.Bold);
            this.lblPreAutorizacaoLocacao.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblPreAutorizacaoLocacao.Location = new System.Drawing.Point(0, 150);
            this.lblPreAutorizacaoLocacao.Name = "lblPreAutorizacaoLocacao";
            this.lblPreAutorizacaoLocacao.Size = new System.Drawing.Size(1374, 54);
            this.lblPreAutorizacaoLocacao.TabIndex = 157;
            this.lblPreAutorizacaoLocacao.Text = "Pré-autorização de locação";
            this.lblPreAutorizacaoLocacao.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Image = global::centralUnidas.Properties.Resources.Totem_Traco;
            this.pictureBox1.Location = new System.Drawing.Point(355, 207);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(683, 1);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 160;
            this.pictureBox1.TabStop = false;
            // 
            // tmrRotina
            // 
            this.tmrRotina.Interval = 500;
            this.tmrRotina.Tick += new System.EventHandler(this.tmrRotina_Tick);
            // 
            // frmScreen09
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1354, 756);
            this.ControlBox = false;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictShutdown);
            this.Controls.Add(this.lblPreAutorizacaoLocacao);
            this.Controls.Add(this.lblTextoAposADevolucaoDoVeiculo);
            this.Controls.Add(this.lblTextoEsteValorSeraPreAutorizado);
            this.Controls.Add(this.lblSelecioneGrupo1);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictLang);
            this.Controls.Add(this.pictUnidasLogo);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmScreen09";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CAU - Unidas";
            this.TopMost = false;
            this.WindowState = System.Windows.Forms.FormWindowState.Normal;
            this.Load += new System.EventHandler(this.frmScreen09_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictShutdown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictLang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictShutdown;
        private System.Windows.Forms.PictureBox pictLang;
        private System.Windows.Forms.PictureBox pictUnidasLogo;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Timer tmrReturn;
        private System.Windows.Forms.Label lblSelecioneGrupo1;
        private System.Windows.Forms.Label lblTextoEsteValorSeraPreAutorizado;
        private System.Windows.Forms.Label lblTextoAposADevolucaoDoVeiculo;
        private System.Windows.Forms.Label lblPreAutorizacaoLocacao;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer tmrRotina;
    }
}