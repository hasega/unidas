﻿namespace centralUnidas
{
    partial class frmScreen11
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmScreen11));
            this.pictShutdown = new System.Windows.Forms.PictureBox();
            this.pictLang = new System.Windows.Forms.PictureBox();
            this.pictUnidasLogo = new System.Windows.Forms.PictureBox();
            this.lblTransacaoEfetuadaSucesso = new System.Windows.Forms.Label();
            this.lblObrigadoEscolherUnidas = new System.Windows.Forms.Label();
            this.lblRetireComprovanteLocacao = new System.Windows.Forms.Label();
            this.lblBalcaoRetirarChaves = new System.Windows.Forms.Label();
            this.tmrReturn = new System.Windows.Forms.Timer(this.components);
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.butConcluir = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictShutdown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictLang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // pictShutdown
            // 
            this.pictShutdown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictShutdown.BackColor = System.Drawing.Color.Transparent;
            this.pictShutdown.Image = global::centralUnidas.Properties.Resources.shutdown_button;
            this.pictShutdown.Location = new System.Drawing.Point(13, 150);
            this.pictShutdown.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictShutdown.Name = "pictShutdown";
            this.pictShutdown.Size = new System.Drawing.Size(56, 62);
            this.pictShutdown.TabIndex = 151;
            this.pictShutdown.TabStop = false;
            this.pictShutdown.Visible = false;
            this.pictShutdown.Click += new System.EventHandler(this.pictShutdown_Click);
            // 
            // pictLang
            // 
            this.pictLang.Image = ((System.Drawing.Image)(resources.GetObject("pictLang.Image")));
            this.pictLang.Location = new System.Drawing.Point(1148, 0);
            this.pictLang.Name = "pictLang";
            this.pictLang.Size = new System.Drawing.Size(148, 61);
            this.pictLang.TabIndex = 153;
            this.pictLang.TabStop = false;
            this.pictLang.Visible = false;
            this.pictLang.Click += new System.EventHandler(this.pictLang_Click);
            // 
            // pictUnidasLogo
            // 
            this.pictUnidasLogo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictUnidasLogo.BackColor = System.Drawing.Color.Transparent;
            this.pictUnidasLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictUnidasLogo.Image = global::centralUnidas.Properties.Resources.totem_header;
            this.pictUnidasLogo.Location = new System.Drawing.Point(0, 0);
            this.pictUnidasLogo.Name = "pictUnidasLogo";
            this.pictUnidasLogo.Size = new System.Drawing.Size(1370, 140);
            this.pictUnidasLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictUnidasLogo.TabIndex = 152;
            this.pictUnidasLogo.TabStop = false;
            // 
            // lblTransacaoEfetuadaSucesso
            // 
            this.lblTransacaoEfetuadaSucesso.AutoSize = true;
            this.lblTransacaoEfetuadaSucesso.Font = new System.Drawing.Font("Arial", 32.25F, System.Drawing.FontStyle.Bold);
            this.lblTransacaoEfetuadaSucesso.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblTransacaoEfetuadaSucesso.Location = new System.Drawing.Point(357, 237);
            this.lblTransacaoEfetuadaSucesso.Name = "lblTransacaoEfetuadaSucesso";
            this.lblTransacaoEfetuadaSucesso.Size = new System.Drawing.Size(720, 51);
            this.lblTransacaoEfetuadaSucesso.TabIndex = 157;
            this.lblTransacaoEfetuadaSucesso.Text = "Transação efetuada com sucesso.";
            // 
            // lblObrigadoEscolherUnidas
            // 
            this.lblObrigadoEscolherUnidas.AutoSize = true;
            this.lblObrigadoEscolherUnidas.Font = new System.Drawing.Font("Arial", 32.25F);
            this.lblObrigadoEscolherUnidas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblObrigadoEscolherUnidas.Location = new System.Drawing.Point(394, 347);
            this.lblObrigadoEscolherUnidas.Name = "lblObrigadoEscolherUnidas";
            this.lblObrigadoEscolherUnidas.Size = new System.Drawing.Size(632, 49);
            this.lblObrigadoEscolherUnidas.TabIndex = 158;
            this.lblObrigadoEscolherUnidas.Text = "Obrigado por escolher a Unidas";
            // 
            // lblRetireComprovanteLocacao
            // 
            this.lblRetireComprovanteLocacao.AutoSize = true;
            this.lblRetireComprovanteLocacao.Font = new System.Drawing.Font("Arial", 32.25F);
            this.lblRetireComprovanteLocacao.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblRetireComprovanteLocacao.Location = new System.Drawing.Point(289, 413);
            this.lblRetireComprovanteLocacao.Name = "lblRetireComprovanteLocacao";
            this.lblRetireComprovanteLocacao.Size = new System.Drawing.Size(869, 49);
            this.lblRetireComprovanteLocacao.TabIndex = 159;
            this.lblRetireComprovanteLocacao.Text = "Retire o comprovante da sua locação e leve";
            // 
            // lblBalcaoRetirarChaves
            // 
            this.lblBalcaoRetirarChaves.AutoSize = true;
            this.lblBalcaoRetirarChaves.Font = new System.Drawing.Font("Arial", 32.25F);
            this.lblBalcaoRetirarChaves.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblBalcaoRetirarChaves.Location = new System.Drawing.Point(331, 478);
            this.lblBalcaoRetirarChaves.Name = "lblBalcaoRetirarChaves";
            this.lblBalcaoRetirarChaves.Size = new System.Drawing.Size(805, 49);
            this.lblBalcaoRetirarChaves.TabIndex = 160;
            this.lblBalcaoRetirarChaves.Text = "até o balcão para retirar as suas chaves.";
            // 
            // tmrReturn
            // 
            this.tmrReturn.Interval = 30000;
            this.tmrReturn.Tick += new System.EventHandler(this.tmrReturn_Tick);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox2.Image = global::centralUnidas.Properties.Resources.Totem_Traco;
            this.pictureBox2.Location = new System.Drawing.Point(382, 560);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(683, 1);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 176;
            this.pictureBox2.TabStop = false;
            // 
            // butConcluir
            // 
            this.butConcluir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.butConcluir.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.butConcluir.Font = new System.Drawing.Font("Arial", 22F, System.Drawing.FontStyle.Bold);
            this.butConcluir.ForeColor = System.Drawing.Color.White;
            this.butConcluir.Location = new System.Drawing.Point(584, 617);
            this.butConcluir.Name = "butConcluir";
            this.butConcluir.Size = new System.Drawing.Size(247, 71);
            this.butConcluir.TabIndex = 177;
            this.butConcluir.Text = "Concluir";
            this.butConcluir.UseVisualStyleBackColor = false;
            this.butConcluir.Click += new System.EventHandler(this.butConcluir_Click);
            // 
            // frmScreen11
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1354, 733);
            this.ControlBox = false;
            this.Controls.Add(this.butConcluir);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.lblBalcaoRetirarChaves);
            this.Controls.Add(this.lblRetireComprovanteLocacao);
            this.Controls.Add(this.lblObrigadoEscolherUnidas);
            this.Controls.Add(this.lblTransacaoEfetuadaSucesso);
            this.Controls.Add(this.pictLang);
            this.Controls.Add(this.pictUnidasLogo);
            this.Controls.Add(this.pictShutdown);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Arial", 18F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmScreen11";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CAU - Unidas";
            this.WindowState = System.Windows.Forms.FormWindowState.Normal;
            this.Load += new System.EventHandler(this.frmScreen11_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictShutdown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictLang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictShutdown;
        private System.Windows.Forms.PictureBox pictLang;
        private System.Windows.Forms.PictureBox pictUnidasLogo;
        private System.Windows.Forms.Label lblTransacaoEfetuadaSucesso;
        private System.Windows.Forms.Label lblObrigadoEscolherUnidas;
        private System.Windows.Forms.Label lblRetireComprovanteLocacao;
        private System.Windows.Forms.Label lblBalcaoRetirarChaves;
        private System.Windows.Forms.Timer tmrReturn;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button butConcluir;
    }
}