﻿// Aplicação: Unidas Totem
// Data     : 01/10/2015
// Versão   : 1.0
// Empresa  : Unidas Rent a Car
// Equipe   : Unidas Desenvolvimento
// Objetivo : Chama rotina Sitef para pre-pagamento
// Histórico: 
//      01/10/2015: Inicio de desenvolvimento para acesso geral
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using centralUnidas.centralUnidasWCF;
using Newtonsoft.Json.Linq;
using System.Xml;
using System.Text.RegularExpressions;
using System.Resources;

namespace centralUnidas
{
    public partial class frmScreen09 : Form
    {
        public ResXResourceSet ResxSetForm; 

        public frmScreen09()
        {
            InitializeComponent();
        }

        private void frmScreen09_Load(object sender, EventArgs e)
        {

            LogTotem.InsertLog(new LogOperacao()
            {
                Application_Name = Globals.glbApplicationName,
                Application_Version = Globals.glbApplicationVersion,
                Local = Globals.glbLojaTotem,
                Operation_Ticks = Globals.glbOperationTicks,
                Operation_Name = "Pré-Autorização",
                Form = "FrmScreen09",
                RESNUM = String.IsNullOrEmpty(Globals.glbReservaNumeroPesquisa) ? 0 : Convert.ToInt32(Globals.glbReservaNumeroPesquisa),
                RANUM = String.IsNullOrEmpty(Globals.glbRA_Contrato) ? 0 : Convert.ToInt32(Globals.glbRA_Contrato)

            });

            ResetCC();
            ResetSegur();

            Globals.glbInteractSitef = 0;

            Globals.glbScreen = 6;
            int nScreenWidth = this.Width;
            int nScreenWidthSplit = nScreenWidth / 2;

            CultureInfo culture = new CultureInfo("pt-BR");
            int intLanguage = Globals.glbLanguage;
            this.Refresh();
            
            //lblSelecioneGrupo1.Text = "Valor a ser cobrado: " + string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDouble(Globals.glbProtecoesValorPre1)) + " *";

            //lblSelecioneGrupo1.Refresh();

            AtualizarInterfaceParaIdiomaCorrente();

            Globals.glbDebugMode = Convert.ToInt16(ConfigurationManager.AppSettings.Get("appDebugMode"));
            Shutdown_Enable();

            tmrRotina.Enabled = true;
        }

        private void Shutdown_Enable()
        {
            if (Globals.glbDebugMode == 0)
                pictShutdown.Visible = false;
            else
                pictShutdown.Visible = true;
        }

        private void pictShutdown_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(1);
        }

        private void pictReservaAlterar_Click(object sender, EventArgs e)
        {
            timerDisabled();

            Globals.glbMensagem = 0401;
            frmMensagem frmMsg = new frmMensagem();
            frmMsg.Show();
            this.Close();
            this.Dispose();
        }

        private void pictReservaConfirmar_Click(object sender, EventArgs e)
        {
            string strPlaca = "";
            if (string.IsNullOrEmpty(strPlaca))
            {
                timerDisabled();

                Globals.glbMensagem = 0601;
                frmMensagem frmMsg0601 = new frmMensagem();
                frmMsg0601.Show();
                this.Close();
                this.Dispose();
                return;
            }
            else
            {
                timerDisabled();

                Globals.glbVEIPLA = strPlaca.Trim();
                frmScreen11 frm11 = new frmScreen11();
                frm11.Show();
                this.Close();
                this.Dispose();
            }
        }

        private void tmrReturn_Tick(object sender, EventArgs e)
        {
            tmrReturn.Enabled = false;
            timerDisabled();


            Globals.glbMensagem = 1001;
            frmMensagem frmMsg0201 = new frmMensagem();
            frmMsg0201.Show();
            this.Close();
            this.Dispose();
        }

        private void pictLang_Click(object sender, EventArgs e)
        {
            frmIdioma frmIdi = new frmIdioma();
            frmIdi.ShowDialog();

            if (Globals.glbLanguage == 1) pictLang.Image = centralUnidas.Properties.Resources.totem_langPT;
            if (Globals.glbLanguage == 2) pictLang.Image = centralUnidas.Properties.Resources.totem_langPT;
            if (Globals.glbLanguage == 3) pictLang.Image = centralUnidas.Properties.Resources.totem_langPT;
        }

        private void logExitSitef(string strResultado, string strLoja, string strTerminal, string strCupom, string nTransacaoConfirma, string strReservaProduto)
        {
            string strDataLog = DateTime.Now.ToString("yyyyMMdd");
            string strDataLogAtual = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string strFileName = @"C:\CliSiTef\UnidasTotemLog" + strDataLog.Trim() + ".txt";
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(strFileName, true))
            {
                file.WriteLine(strDataLogAtual + ":");
                file.WriteLine("==== INICIO =====");
                file.WriteLine(" ");
                file.WriteLine("** Loja: " + strLoja);
                file.WriteLine("** Terminal: " + strTerminal);
                file.WriteLine("** Cupom: " + strCupom);
                file.WriteLine("** Reserva: " + strReservaProduto);
                file.WriteLine("** nTransacaoConfirma: " + nTransacaoConfirma);
                file.WriteLine(" ");
                file.WriteLine("---> Fluxo Sitef (Saida): ");
                file.WriteLine(" ");
                file.WriteLine(strResultado);
                file.WriteLine("==== FIM ========");
                file.WriteLine(" ");
                file.WriteLine(" ");
                file.Flush();
                file.Close();
            }
        }

        private void ResetSegur()
        {
            Globals.glbSEG_R_ANUM = "";
            Globals.glbSEG_R_ALOJRET = "";
            Globals.glbSEG_PAGITENUM = "";
            Globals.glbSEG_HASH = "";
            Globals.glbSEG_TP_INFO = "";
            Globals.glbSEG_TP_OPER = "";
            Globals.glbSEG_HASH_OUT = "";
            Globals.glbSEG_HASH_NEW = "";
            Globals.glbSEG_RT_OPER = "";
            Globals.glbSEG_Cpf = "";
            Globals.glbSEG_CrtNum = "";
            Globals.glbSEG_MessageText = "";
        }

        private void ResetCC()
        {
            Globals.glbScreenCard = 0;
            Globals.glbCC_R_ANUM = "";
            Globals.glbCC_R_ALOJRET = "";
            Globals.glbCC_CCAITENUM = "";
            Globals.glbCC_CupomFiscal = "";
            Globals.glbCC_ValidadeCupomFiscal = "";
            Globals.glbCC_CartaoInformado = "";
            Globals.glbCC_ValidadeInformado = "";
            Globals.glbCC_SegurancaInformado = "";

        }
        
        private void richSitef_TextChanged(object sender, EventArgs e)
        {
            if (tmrReturn.Enabled)
            {
                tmrReturn.Enabled = false;
            }
            tmrReturn.Enabled = true;
        }

        private void pictTec_TECLADO_Click(object sender, EventArgs e)
        {
            Globals.glbInteractSitef = 1;
        }

        private void pictTec_BACK2_Click(object sender, EventArgs e)
        {
        }

        private void tmrRotina_Tick(object sender, EventArgs e)
        {
            tmrRotina.Enabled = false;

            timerDisabled();

            string strArgumentos = "";
            strArgumentos = Globals.glbProtecoesValorPre1.Trim() + " ";
            strArgumentos += Globals.glbLOJCODSITE.Trim() + " ";
            strArgumentos += Globals.glbPortaPinPad.Trim() + " ";
            strArgumentos += Globals.glbReservaNumeroPesquisa.Trim() + " ";
            strArgumentos += Globals.glbRESDOCCLI.Trim();

            // AQUI RETORNAR DEPOIS - ROTINA SITEF disabled 18/02/2016

            System.Diagnostics.Process pProcess = new System.Diagnostics.Process();
            pProcess.StartInfo.FileName = @"centralSitef.exe";
            pProcess.StartInfo.Arguments = strArgumentos; //argument
            pProcess.StartInfo.UseShellExecute = false;
            pProcess.StartInfo.RedirectStandardInput = true;
            pProcess.StartInfo.RedirectStandardOutput = true;
            pProcess.StartInfo.RedirectStandardError = true;
            pProcess.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal;
            pProcess.StartInfo.CreateNoWindow = false; //diplay a windows
            pProcess.Start();

            System.Threading.Thread.Sleep(500);

            string output = pProcess.StandardOutput.ReadToEnd(); //The output result
            string error = pProcess.StandardError.ReadToEnd(); //The error result
            pProcess.WaitForExit();

            if (pProcess.ExitCode != 0)
            {
                int nExitCode = pProcess.ExitCode;
                switch (nExitCode)
                {
                    case 10001:
                        timerDisabled();
                        Globals.glbScreen = 1;
                        Globals.glbMensagem = 1001;
                        frmMensagem frm10001 = new frmMensagem();
                        frm10001.Show();
                        this.Close();
                        this.Dispose();
                        break;
                    case 10002:
                        timerDisabled();
                        Globals.glbScreen = 1;
                        Globals.glbMensagem = 1001;
                        frmMensagem frm10002 = new frmMensagem();
                        frm10002.Show();
                        this.Close();
                        this.Dispose();
                        break;
                    case 10003:
                        timerDisabled();
                        Globals.glbScreen = 1;
                        Globals.glbMensagem = 1001;
                        frmMensagem frm10003 = new frmMensagem();
                        frm10003.Show();
                        this.Close();
                        this.Dispose();
                        break;
                    case 10004:
                        timerDisabled();
                        Globals.glbScreen = 1;
                        Globals.glbMensagem = 1001;
                        frmMensagem frm10004 = new frmMensagem();
                        frm10004.Show();
                        this.Close();
                        this.Dispose();
                        break;
                    case 10007:
                        timerDisabled();
                        Globals.glbScreen = 1;
                        Globals.glbMensagem = 1001;
                        frmMensagem frm10007 = new frmMensagem();
                        frm10007.Show();
                        this.Close();
                        this.Dispose();
                        break;
                    case 10008:
                        timerDisabled();
                        Globals.glbScreen = 1;
                        Globals.glbMensagem = 1001;
                        frmMensagem frm10008 = new frmMensagem();
                        frm10008.Show();
                        this.Close();
                        this.Dispose();
                        break;
                    case 10010:
                        timerDisabled();
                        Globals.glbScreen = 1;
                        Globals.glbMensagem = 1001;
                        frmMensagem frm10010 = new frmMensagem();
                        frm10010.Show();
                        this.Close();
                        this.Dispose();
                        break;
                    case 10013:
                        timerDisabled();
                        Globals.glbScreen = 1;
                        Globals.glbMensagem = 1001;
                        frmMensagem frm10013 = new frmMensagem();
                        frm10013.Show();
                        this.Close();
                        this.Dispose();
                        break;
                    case 10014:
                        timerDisabled();
                        Globals.glbScreen = 1;
                        Globals.glbMensagem = 1001;
                        frmMensagem frm10014 = new frmMensagem();
                        frm10014.Show();
                        this.Close();
                        this.Dispose();
                        break;
                    case 10015:
                        timerDisabled();
                        Globals.glbScreen = 1;
                        Globals.glbMensagem = 1001;
                        frmMensagem frm10015 = new frmMensagem();
                        frm10015.Show();
                        this.Close();
                        this.Dispose();
                        break;
                    case 20000:
                        timerDisabled();
                        Globals.glbScreen = 1;
                        Globals.glbMensagem = 1001;
                        frmMensagem frmMsg0201 = new frmMensagem();
                        frmMsg0201.Show();
                        this.Close();
                        this.Dispose();
                        break;
                    case 20001:
                        timerDisabled();
                        Globals.glbScreen = 1;
                        Globals.glbMensagem = 1001;
                        frmMensagem frm20001 = new frmMensagem();
                        frm20001.Show();
                        this.Close();
                        this.Dispose();
                        break;
                    case 20002:
                        timerDisabled();
                        Globals.glbScreen = 1;
                        Globals.glbMensagem = 1001;
                        frmMensagem frm20002 = new frmMensagem();
                        frm20002.Show();
                        this.Close();
                        this.Dispose();
                        break;
                    case 20003:
                        timerDisabled();
                        Globals.glbScreen = 1;
                        Globals.glbMensagem = 1001;
                        frmMensagem frm20003 = new frmMensagem();
                        frm20003.Show();
                        this.Close();
                        this.Dispose();
                        break;
                    case 20004:
                        timerDisabled();
                        Globals.glbScreen = 1;
                        Globals.glbMensagem = 1001;
                        frmMensagem frm20004 = new frmMensagem();
                        frm20004.Show();
                        this.Close();
                        this.Dispose();
                        break;
                    case 30001:
                        timerDisabled();
                        frmScreen11 frm30001 = new frmScreen11();
                        frm30001.Show();
                        this.Close();
                        this.Dispose();
                        break;
                    case 30002:
                        timerDisabled();
                        frmScreen13 frm30002 = new frmScreen13();
                        frm30002.Show();
                        this.Close();
                        this.Dispose();
                        break;
                    default:
                        timerDisabled();
                        frmScreen01 frmDefault = new frmScreen01();
                        frmDefault.Show();
                        this.Close();
                        this.Dispose();
                        break;
                }
            }
            else
            {
                timerDisabled();
                Globals.glbScreen = 2;
                Globals.glbMensagem = 1001;
                frmMensagem frmMsg = new frmMensagem();
                frmMsg.Show();
                this.Close();
                this.Dispose();
                return;
            }
        }

        private void AtualizarInterfaceParaIdiomaCorrente()
        {

            MontarResourceSetParaIdiomaCorrente();

            lblTextoEsteValorSeraPreAutorizado.Text = ResxSetForm.GetString("lblTextoEsteValorSeraPreAutorizado.Text");
            lblTextoAposADevolucaoDoVeiculo.Text = ResxSetForm.GetString("lblTextoAposADevolucaoDoVeiculo.Text");
            lblSelecioneGrupo1.Text = ResxSetForm.GetString("lblSelecioneGrupo1.Text") + string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDouble(Globals.glbProtecoesValorPre1)) + " *"; ;
            lblPreAutorizacaoLocacao.Text = ResxSetForm.GetString("lblPreAutorizacaoLocacao.Text");

            //butIdioma.Text = ResxSetForm.GetString("butIdioma.Text");
            //butVoltar.Text = ResxSetForm.GetString("butVoltar.Text");
            //butConfirmar.Text = ResxSetForm.GetString("butConfirmar.Text");

            ////this.Icon = ((System.Drawing.Icon)(ResxSetForm.GetObject("$this.Icon")));
            //lstPlacas.Columns["colCORDES"].Text = ResxSetForm.GetString("colCORDES.Text");
            //lstPlacas.Columns["colVEIKMSATU"].Text = ResxSetForm.GetString("colVEIKMSATU.Text");
            //lstPlacas.Columns["colVEIPLA"].Text = ResxSetForm.GetString("colVEIPLA.Text");

            //if (Globals.glbLanguage == 1) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_Brasil_botao;
            //if (Globals.glbLanguage == 2) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_Espanha_botao;
            //if (Globals.glbLanguage == 3) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_UK_botao;

            //butIdioma.Refresh();
        }

        private void MontarResourceSetParaIdiomaCorrente()
        {
            string srtCaminhoExeTotem = System.IO.Path.GetDirectoryName(Application.ExecutablePath.ToString());
            // Português
            if (Globals.glbLanguage == 1)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen09.pt.resx");
            }
            //Espanhol
            else if (Globals.glbLanguage == 2)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen09.es.resx");
            }
            // Inglês
            else if (Globals.glbLanguage == 3)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen09.en.resx");
            }
        }

        private void lblSelecioneGrupo1_Click(object sender, EventArgs e)
        {

        }

        private void timerDisabled()
        {
            tmrReturn.Enabled = false;
            tmrRotina.Enabled = false;
        }
    }
}
