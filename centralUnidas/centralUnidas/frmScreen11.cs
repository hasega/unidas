﻿// Aplicação: Unidas Totem
// Data     : 01/10/2015
// Versão   : 1.0
// Empresa  : Unidas Rent a Car
// Equipe   : Unidas Desenvolvimento
// Objetivo : Imprime RA e Fecha RA
// Histórico: 
//      01/10/2015: Inicio de desenvolvimento para acesso geral
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Management;
using System.Runtime.InteropServices;
using centralUnidas.centralUnidasWCF;
using System.Xml;
using System.Resources;

namespace centralUnidas
{
    public partial class frmScreen11 : Form
    {
        public ResXResourceSet ResxSetForm;

        string strNome = string.Empty;

        [DllImport("winspool.drv", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int OpenPrinter(string pPrinterName, out IntPtr phPrinter, ref PRINTER_DEFAULTS pDefault);

        [DllImport("winspool.drv", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern bool GetPrinter(IntPtr hPrinter, Int32 dwLevel, IntPtr pPrinter, Int32 dwBuf, out Int32 dwNeeded);

        [DllImport("winspool.drv", SetLastError = true)]
        public static extern int ClosePrinter(IntPtr hPrinter);

        [StructLayout(LayoutKind.Sequential)]
        public struct PRINTER_DEFAULTS
        {
            public IntPtr pDatatype;
            public IntPtr pDevMode;
            public int DesiredAccess;
        }

        static Random _r = new Random();

        private PRINTER_DEFAULTS PrinterValues = new PRINTER_DEFAULTS();

        private const int PRINTER_ACCESS_ADMINISTER = 0x4;
        private const int PRINTER_ACCESS_USE = 0x8;
        private const int STANDARD_RIGHTS_REQUIRED = 0xF0000;
        private const int PRINTER_ALL_ACCESS =
            (STANDARD_RIGHTS_REQUIRED | PRINTER_ACCESS_ADMINISTER
            | PRINTER_ACCESS_USE);

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public struct PRINTER_INFO_2
        {
            [MarshalAs(UnmanagedType.LPTStr)]
            public string pServerName;

            [MarshalAs(UnmanagedType.LPTStr)]
            public string pPrinterName;

            [MarshalAs(UnmanagedType.LPTStr)]
            public string pShareName;

            [MarshalAs(UnmanagedType.LPTStr)]
            public string pPortName;

            [MarshalAs(UnmanagedType.LPTStr)]
            public string pDriverName;

            [MarshalAs(UnmanagedType.LPTStr)]
            public string pComment;

            [MarshalAs(UnmanagedType.LPTStr)]
            public string pLocation;

            public IntPtr pDevMode;

            [MarshalAs(UnmanagedType.LPTStr)]
            public string pSepFile;

            [MarshalAs(UnmanagedType.LPTStr)]
            public string pPrintProcessor;

            [MarshalAs(UnmanagedType.LPTStr)]
            public string pDatatype;

            [MarshalAs(UnmanagedType.LPTStr)]
            public string pParameters;

            public IntPtr pSecurityDescriptor;
            public uint Attributes;
            public uint Priority;
            public uint DefaultPriority;
            public uint StartTime;
            public uint UntilTime;
            public uint Status;
            public uint cJobs;
            public uint AveragePPM;
        }

        private int m_currentPageIndex;
        private IList<Stream> m_streams;
        PrintDocument pdoc = null;

        private void resetVoucher()
        {
            Globals.glbVOU_RESNUM = "";
            Globals.glbVOU_RESLOJOPE = "";
            Globals.glbVOU_RESNVCH = "";
            Globals.glbVOU_RESVDIA = "";
            Globals.glbVOU_RESVDIAE = "";
            Globals.glbVOU_RESVHORE = "";
            Globals.glbVOU_RESVKM = "";
            Globals.glbVOU_RESVTAXS = "";
            Globals.glbVOU_RESVPROT = "";
            Globals.glbVOU_RESVMOT = "";
            Globals.glbVOU_RESVAPRI = "";
            Globals.glbVOU_RESVCMB = "";
            Globals.glbVOU_RESVAVA = "";
            Globals.glbVOU_RESVPRTO = "";
            Globals.glbVOU_RESVADC = "";
            Globals.glbVOU_RESVMOTA = "";
            Globals.glbVOU_RESVTAXR = "";
            Globals.glbVOU_RESVDSP = "";
            Globals.glbVOU_RESMUL = "";
            Globals.glbVOU_RESNVCLI = "";
        }

        public frmScreen11()
        {
            InitializeComponent();
        }

        private void PrintPage(object sender, PrintPageEventArgs ev)
        {
            Metafile pageImage = new Metafile(m_streams[m_currentPageIndex]);

            // Adjust rectangular area with printer margins.
            Rectangle adjustedRect = new Rectangle(
                ev.PageBounds.Left - (int)ev.PageSettings.HardMarginX,
                ev.PageBounds.Top - (int)ev.PageSettings.HardMarginY,
                ev.PageBounds.Width,
                ev.PageBounds.Height);

            // Draw a white background for the report
            ev.Graphics.FillRectangle(Brushes.White, adjustedRect);

            // Draw the report content
            ev.Graphics.DrawImage(pageImage, adjustedRect);

            // Prepare for the next page. Make sure we haven't hit the end.
            m_currentPageIndex++;
            ev.HasMorePages = (m_currentPageIndex < m_streams.Count);
        }

        private void Print()
        {
            try
            {
                if (m_streams == null || m_streams.Count == 0)
                    throw new Exception("Error: no stream to print.");
                PrintDocument printDoc = new PrintDocument();
                if (!printDoc.PrinterSettings.IsValid)
                {
                    throw new Exception("Error: cannot find the default printer.");
                }
                else
                {
                    printDoc.PrintPage += new PrintPageEventHandler(PrintPage);
                    m_currentPageIndex = 0;
                    printDoc.Print();
                    printDoc.Dispose();
                }
            }
            catch
            {
                tmrReturn.Enabled = true;
            }
        }

        public PRINTER_INFO_2? GetPrinterInfo(String printerName)
        {
            IntPtr pHandle;
            PRINTER_DEFAULTS defaults = new PRINTER_DEFAULTS();
            PRINTER_INFO_2? Info2 = null;

            defaults.DesiredAccess = PRINTER_ALL_ACCESS;

            OpenPrinter(printerName, out pHandle, ref defaults);

            Int32 cbNeeded = 0;

            bool bRet = GetPrinter(pHandle, 2, IntPtr.Zero, 0, out cbNeeded);

            if (cbNeeded > 0)
            {
                IntPtr pAddr = Marshal.AllocHGlobal((int)cbNeeded);

                bRet = GetPrinter(pHandle, 2, pAddr, cbNeeded, out cbNeeded);

                if (bRet)
                {
                    Info2 = (PRINTER_INFO_2)Marshal.PtrToStructure(pAddr, typeof(PRINTER_INFO_2));
                }

                Marshal.FreeHGlobal(pAddr);
            }

            ClosePrinter(pHandle);

            return Info2;
        }

        private bool checkPrinter()
        {

            return true;

            string strPrinter = ConfigurationManager.AppSettings.Get("appPrinter").ToString().Trim();

            PRINTER_INFO_2 printStruct = (PRINTER_INFO_2)GetPrinterInfo(strPrinter);

            uint nStatusPrinter = printStruct.Status;
            bool bCheckOffline = true;
            if (nStatusPrinter == 2) bCheckOffline = false;         // PRINTER_STATUS_ERROR
            if (nStatusPrinter == 262144) bCheckOffline = false;    // PRINTER_STATUS_NO_TONER
            if (nStatusPrinter == 128) bCheckOffline = false;       // PRINTER_STATUS_OFFLINE
            if (nStatusPrinter == 8) bCheckOffline = false;         // PRINTER_STATUS_PAPER_JAM
            if (nStatusPrinter == 10) bCheckOffline = false;        // PRINTER_STATUS_PAPER_OUT
            if (nStatusPrinter == 64) bCheckOffline = false;        // PRINTER_STATUS_PAPER_PROBLEM
            // if (nStatusPrinter == 131072) bCheckOffline = false;    // PRINTER_STATUS_TONER_LOW
            return bCheckOffline;
        }

        private void printRA()
        {
            try
            {
                pdoc = new PrintDocument();
                PrinterSettings ps = new PrinterSettings();

                bool bOffline = checkPrinter();
                if (bOffline)
                {
                    pdoc.PrintPage += new PrintPageEventHandler(pdoc_PrintPage);
                    //PaperSize pkSize = new PaperSize();

                    PaperSize pkSize;
                    if (!string.IsNullOrEmpty(Globals.glbCupomFiscal1))
                    {
                        pkSize = new PaperSize("RA", 300, 3300);
                    }
                    else
                    {
                        pkSize = new PaperSize("RA", 300, 2500);
                    }
                    pdoc.OriginAtMargins = true;
                    pdoc.DefaultPageSettings.Margins.Left = 5;
                    pdoc.DefaultPageSettings.Margins.Right = 0;
                    pdoc.DefaultPageSettings.Margins.Top = 10;
                    pdoc.DefaultPageSettings.Margins.Bottom = 10;
                    pdoc.DefaultPageSettings.PaperSize = pkSize;

                    if (!string.IsNullOrEmpty(Globals.glbCupomFiscal1))
                    {
                        pdoc.PrinterSettings.DefaultPageSettings.PaperSize = new PaperSize("RA", 300, 3300);
                    }
                    else
                    {
                        pdoc.PrinterSettings.DefaultPageSettings.PaperSize = new PaperSize("RA", 300, 2500);
                    }
                    pdoc.PrinterSettings.DefaultPageSettings.Margins.Left = 5;
                    pdoc.PrinterSettings.DefaultPageSettings.Margins.Right = 0;
                    pdoc.PrinterSettings.DefaultPageSettings.Margins.Top = 10;
                    pdoc.PrinterSettings.DefaultPageSettings.Margins.Bottom = 10;

                    if (!string.IsNullOrEmpty(Globals.glbCupomFiscal1))
                    {
                        pdoc.PrinterSettings.DefaultPageSettings.PrinterSettings.DefaultPageSettings.PaperSize = new PaperSize("RA", 300, 3300);
                    }
                    else
                    {
                        pdoc.PrinterSettings.DefaultPageSettings.PrinterSettings.DefaultPageSettings.PaperSize = new PaperSize("RA", 300, 2500);
                    }

                    pdoc.DocumentName = "RA";
                    pdoc.Print();
                }
                else
                {
                    timerDisabled();
                    Globals.glbScreen = 1;
                    Globals.glbMensagem = 1003;
                    frmMensagem frmMsg0201 = new frmMensagem();
                    frmMsg0201.Show();
                    this.Close();
                    this.Dispose();
                }
            }
            catch (Exception ex)
            {
                timerDisabled();
                Globals.glbMensagem = 1001;
                frmMensagem frmMsg = new frmMensagem();
                frmMsg.Show();
                this.Close();
                this.Dispose();
            }
        }

        void pdoc_PrintPage(object sender, PrintPageEventArgs e)
        {
            try
            {
                string dtEmissao = DateTime.Now.ToString("dd/MM/yyyy");

                string strPickup = Globals.glbPickUpDateTime.Replace("T", " ").Replace("-", "/");
                strPickup = strPickup.Substring(8, 2) + "/" + strPickup.Substring(5, 2) + "/" + strPickup.Substring(0, 4) + " " + strPickup.Substring(11, 5);

                string strReturn = Globals.glbReturnDateTime.Replace("T", " ").Replace("-", "/");
                strReturn = strReturn.Substring(8, 2) + "/" + strReturn.Substring(5, 2) + "/" + strReturn.Substring(0, 4) + " " + strReturn.Substring(11, 5);

                Graphics graphics = e.Graphics;
                Font font = new Font("Courier New", 9, FontStyle.Bold);
                float fontHeight = font.GetHeight();

                int startX = 3;
                int startY = 5;
                int Offset = 40;

                if (!string.IsNullOrEmpty(Globals.glbCupomFiscal1))
                {
                    graphics.DrawString("--------------------------------------------", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    Offset = Offset + 17;
                    graphics.DrawString("               CUPOM FISCAL                  ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    Offset = Offset + 17;
                    Offset = Offset + 17;
                    graphics.DrawString(Globals.glbCupomFiscal1, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    for (int x = 0; x < 17; x++)
                    {
                        Offset = Offset + 17;
                    }
                }

                graphics.DrawString("--------------------------------------------", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                Offset = Offset + 17;
                graphics.DrawString("            Unidas S/A", new Font("Courier New", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("         Cupom nao Fiscal", new Font("Courier New", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                Offset = Offset + 17;
                graphics.DrawString("Via Cliente", new Font("Courier New", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                Offset = Offset + 17;
                graphics.DrawString("Loja: " + Globals.glbPickUpLocation + "   Contrato:" + Globals.glbRA_Contrato, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("Veiculo: " + Globals.glbVEIPLA, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("Emissao: " + dtEmissao, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                Offset = Offset + 17;

                graphics.DrawString("Locadora: UNIDAS LOCADORA DE VEICULOS LTDA.", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("Assistencia 24 horas - 08007720994", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                Offset = Offset + 17;

                if (Globals.glbRA_r_atipo.Trim() == "F")
                {
                    // graphics.DrawString("LOCATARIO: " + Globals.glbRA_clinom, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    graphics.DrawString("LOCATARIO: " + Globals.glbCLI_CLINOM_SAUDACOES, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    graphics.DrawString("CPF: " + Globals.glbRA_clidoc, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                }
                else
                {
                    //if (Globals.glbRA_r_atipo.Trim() == "A")
                    //{
                    //    graphics.DrawString("LOCATARIO: " + Globals.glbRA_clinom, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    //    Offset = Offset + 17;
                    //    graphics.DrawString("CNPJ: " + Globals.glbRA_clidoc, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    //}
                    //else
                    //{
                    //    graphics.DrawString("LOCATARIO: " + Globals.glbCLI_CLINOM_SAUDACOES, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    //    Offset = Offset + 17;
                    //    graphics.DrawString("CPF: " + Globals.glbRA_clidoc, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    //}
                    if (Globals.glbRA_r_atipo.Trim() == "A")
                    {
                        graphics.DrawString("LOCATARIO: " + Globals.glbRA_clinom, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                        Offset = Offset + 17;
                        graphics.DrawString("CNPJ: " + Globals.glbRA_clidoc, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    }
                    else if (Globals.glbRA_r_atipo.Trim() == "J" && Convert.ToInt32(Globals.glbCLI_PRFCOD) != 336)
                    {
                        graphics.DrawString("LOCATARIO: " + Globals.glbCompanyShortName, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                        Offset = Offset + 17;
                        graphics.DrawString("CNPJ: " + Globals.glbRA_clidoc, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    }
                    else
                    {
                        graphics.DrawString("LOCATARIO: " + Globals.glbCLI_CLINOM_SAUDACOES, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                        Offset = Offset + 17;
                        graphics.DrawString("CPF: " + Globals.glbRA_clidoc, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    }
                    Offset = Offset + 17;
                    if (!string.IsNullOrEmpty(Globals.glbPRECPF))
                    {
                        graphics.DrawString("PREPOSTO: " + Globals.glbPRENOM, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                        Offset = Offset + 17;
                        graphics.DrawString("CPF: " + Globals.glbPRECPF, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                        Offset = Offset + 17;

                    }
                    graphics.DrawString("FATURAR P/: " + Globals.glbCompanyShortName, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    graphics.DrawString("CNPJ: " + Globals.glbRA_clifat, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                }
                if (!string.IsNullOrEmpty(Globals.glbVoucherIdentifier))
                {
                    graphics.DrawString("Voucher: " + Globals.glbVoucherIdentifier + "  Reserva: " + Globals.glbReservaNumeroPesquisa, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                }
                else
                {
                    graphics.DrawString("Reserva: " + Globals.glbReservaNumeroPesquisa, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                }
                Offset = Offset + 17;

                Globals.glbKM = Globals.glbKM.Replace("Km", "");

                graphics.DrawString("Veiculo: " + Globals.glbVEIPLA + " Grupo: " + Globals.glbResGruCod, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("Modelo: " + Globals.glbMODDES + " - " + Globals.glbTPMDES, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("Km:" + Globals.glbKM, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("Retirada: " + Globals.glbPickUpLocation + " - " + strPickup, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("Devolucao: " + Globals.glbReturnLocation + " - " + strReturn, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);

                Offset = Offset + 17;
                Offset = Offset + 17;
                graphics.DrawString("PROTECOES: ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString(Globals.glbProtecoesVar1 + ": " + Globals.glbProtecoesValorVar1, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                if (!string.IsNullOrEmpty(Globals.glbProtecoesVar2))
                {
                    graphics.DrawString(Globals.glbProtecoesVar2 + ": " + Globals.glbProtecoesValorVar2, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                }
                if (!string.IsNullOrEmpty(Globals.glbProtecoesVar3))
                {
                    graphics.DrawString(Globals.glbProtecoesVar3 + ": " + Globals.glbProtecoesValorVar3, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                }
                Offset = Offset + 17;
                graphics.DrawString("ACESSORIOS: ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;

                string strAuxiliar = "";
                decimal nValorAcessorios = 0;
                int nAcessorio = 0;
                if (Globals.glbPricedEquipDescription != null) nAcessorio = Globals.glbPricedEquipDescription.Count();

                if (nAcessorio > 0)
                {
                    graphics.DrawString(Globals.glbPricedEquipDescription[0].Trim() + ": " + Globals.glbPricedEquipChargeAmount[0].Trim(), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;

                    strAuxiliar = Globals.glbPricedEquipChargeAmount[0].Trim().Replace(".", ",");
                    strAuxiliar = strAuxiliar.Trim().Replace("R$", "");
                    strAuxiliar = strAuxiliar.Trim().Replace(" ", "");
                    nValorAcessorios = Convert.ToDecimal(strAuxiliar);
                }
                else
                {
                    graphics.DrawString("NAO CONTRATADO", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                }
                if (nAcessorio > 1)
                {
                    graphics.DrawString(Globals.glbPricedEquipDescription[1].Trim() + ": " + Globals.glbPricedEquipChargeAmount[1].Trim(), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    strAuxiliar = Globals.glbPricedEquipChargeAmount[1].Trim().Replace(".", ",");
                    strAuxiliar = strAuxiliar.Trim().Replace("R$", "");
                    strAuxiliar = strAuxiliar.Trim().Replace(" ", "");
                    nValorAcessorios = nValorAcessorios + Convert.ToDecimal(strAuxiliar);
                }
                if (nAcessorio > 2)
                {
                    graphics.DrawString(Globals.glbPricedEquipDescription[2].Trim() + ": " + Globals.glbPricedEquipChargeAmount[2].Trim(), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    strAuxiliar = Globals.glbPricedEquipChargeAmount[2].Trim().Replace(".", ",");
                    strAuxiliar = strAuxiliar.Trim().Replace("R$", "");
                    strAuxiliar = strAuxiliar.Trim().Replace(" ", "");
                    nValorAcessorios = nValorAcessorios + Convert.ToDecimal(strAuxiliar);
                }
                if (nAcessorio > 3)
                {
                    graphics.DrawString(Globals.glbPricedEquipDescription[3].Trim() + ": " + Globals.glbPricedEquipChargeAmount[3].Trim(), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    strAuxiliar = Globals.glbPricedEquipChargeAmount[3].Trim().Replace(".", ",");
                    strAuxiliar = strAuxiliar.Trim().Replace("R$", "");
                    strAuxiliar = strAuxiliar.Trim().Replace(" ", "");
                    nValorAcessorios = nValorAcessorios + Convert.ToDecimal(strAuxiliar);
                }
                if (nAcessorio > 4)
                {
                    graphics.DrawString(Globals.glbPricedEquipDescription[4].Trim() + ": " + Globals.glbPricedEquipChargeAmount[4].Trim(), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    strAuxiliar = Globals.glbPricedEquipChargeAmount[4].Trim().Replace(".", ",");
                    strAuxiliar = strAuxiliar.Trim().Replace("R$", "");
                    strAuxiliar = strAuxiliar.Trim().Replace(" ", "");
                    nValorAcessorios = nValorAcessorios + Convert.ToDecimal(strAuxiliar);
                }
                if (nAcessorio > 5)
                {
                    graphics.DrawString(Globals.glbPricedEquipDescription[5].Trim() + ": " + Globals.glbPricedEquipChargeAmount[5].Trim(), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    strAuxiliar = Globals.glbPricedEquipChargeAmount[5].Trim().Replace(".", ",");
                    strAuxiliar = strAuxiliar.Trim().Replace("R$", "");
                    strAuxiliar = strAuxiliar.Trim().Replace(" ", "");
                    nValorAcessorios = nValorAcessorios + Convert.ToDecimal(strAuxiliar);
                }
                if (nAcessorio > 6)
                {
                    graphics.DrawString(Globals.glbPricedEquipDescription[6].Trim() + ": " + Globals.glbPricedEquipChargeAmount[6].Trim(), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    strAuxiliar = Globals.glbPricedEquipChargeAmount[6].Trim().Replace(".", ",");
                    strAuxiliar = strAuxiliar.Trim().Replace("R$", "");
                    strAuxiliar = strAuxiliar.Trim().Replace(" ", "");
                    nValorAcessorios = nValorAcessorios + Convert.ToDecimal(strAuxiliar);
                }
                if (nAcessorio > 7)
                {
                    graphics.DrawString(Globals.glbPricedEquipDescription[7].Trim() + ": " + Globals.glbPricedEquipChargeAmount[7].Trim(), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    strAuxiliar = Globals.glbPricedEquipChargeAmount[7].Trim().Replace(".", ",");
                    strAuxiliar = strAuxiliar.Trim().Replace("R$", "");
                    strAuxiliar = strAuxiliar.Trim().Replace(" ", "");
                    nValorAcessorios = nValorAcessorios + Convert.ToDecimal(strAuxiliar);
                }
                Offset = Offset + 17;
                String underLine = "------------------------------------------";
                graphics.DrawString(underLine, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;

                graphics.DrawString("TOTAIS", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;

                decimal nValorTotal = 0;
                if (!string.IsNullOrEmpty(Globals.glbResumoDiariasVar.Trim()))
                {
                    strAuxiliar = Globals.glbResumoDiariasVar.Trim().Replace(".", "");
                    strAuxiliar = strAuxiliar.Trim().Replace("R$", "");
                    strAuxiliar = strAuxiliar.Trim().Replace(" ", "");
                    nValorTotal = Convert.ToDecimal(strAuxiliar);
                }

                decimal nValorProtecoes = 0;
                if (!string.IsNullOrEmpty(Globals.glbProtecoesValorVar1.Trim()))
                {
                    strAuxiliar = Globals.glbProtecoesValorVar1.Trim().Replace(".", "");
                    strAuxiliar = strAuxiliar.Trim().Replace("R$", "");
                    strAuxiliar = strAuxiliar.Trim().Replace(" ", "");
                    nValorProtecoes = Convert.ToDecimal(strAuxiliar);
                }

                if (!string.IsNullOrEmpty(Globals.glbProtecoesValorVar2.Trim()))
                {
                    strAuxiliar = Globals.glbProtecoesValorVar2.Trim().Replace(".", "");
                    strAuxiliar = strAuxiliar.Trim().Replace("R$", "");
                    strAuxiliar = strAuxiliar.Trim().Replace(" ", "");
                    nValorProtecoes = nValorProtecoes + Convert.ToDecimal(strAuxiliar);
                }

                if (!string.IsNullOrEmpty(Globals.glbProtecoesValorVar3.Trim()))
                {
                    strAuxiliar = Globals.glbProtecoesValorVar3.Trim().Replace(".", "");
                    strAuxiliar = strAuxiliar.Trim().Replace("R$", "");
                    strAuxiliar = strAuxiliar.Trim().Replace(" ", "");
                    nValorProtecoes = nValorProtecoes + Convert.ToDecimal(strAuxiliar);
                }

                decimal nValorTotalReserva = 0;
                if (!string.IsNullOrEmpty(Globals.glbResumoTotalGeral.Trim()))
                {
                    strAuxiliar = Globals.glbResumoTotalGeral.Trim().Replace(".", "");
                    strAuxiliar = strAuxiliar.Trim().Replace("R$", "");
                    strAuxiliar = strAuxiliar.Trim().Replace(" ", "");
                    nValorTotalReserva = Convert.ToDecimal(strAuxiliar);
                }

                decimal nValorDesconto = 0;
                if (!string.IsNullOrEmpty(Globals.glbResumoDescontoVar.Trim()))
                {
                    strAuxiliar = Globals.glbResumoDescontoVar.Trim().Replace(".", "");
                    strAuxiliar = strAuxiliar.Trim().Replace("R$", "");
                    strAuxiliar = strAuxiliar.Trim().Replace(" ", "");
                    nValorDesconto = Convert.ToDecimal(strAuxiliar);
                }

                decimal nValorAdmnistrativo = 0;
                decimal nValorRetorno = 0;
                decimal nValorHoraExtra = 0;

                if (!string.IsNullOrEmpty(Globals.glbResumoHoraExtraQtd))
                {
                    if (Convert.ToInt32(Globals.glbResumoHoraExtraQtd.Trim()) > 0)
                    {
                        strAuxiliar = Globals.glbResumoHoraExtraVar.Trim().Replace("R$", "");
                        strAuxiliar = strAuxiliar.Trim().Replace(".", "");
                        strAuxiliar = strAuxiliar.Trim().Replace(" ", "");
                        nValorHoraExtra = Convert.ToDecimal(strAuxiliar);
                    }
                }

                // TAXAS
                int nTaxas = 0;
                if (Globals.glbFeeDescription != null)
                {
                    nTaxas = Globals.glbFeeDescription.Count();
                    for (int x = 0; x < nTaxas; x++)
                    {
                        if (Globals.glbFeeDescription[x].Trim().ToUpper().Contains("RETORNO"))
                        {
                            strAuxiliar = Globals.glbFeeAmount[x].Trim().Replace("R$", "");
                            strAuxiliar = strAuxiliar.Trim().Replace(".", "");
                            strAuxiliar = strAuxiliar.Trim().Replace(" ", "");
                            nValorRetorno = Convert.ToDecimal(strAuxiliar);
                        }

                        if (Globals.glbFeeDescription[x].Trim().ToUpper().Contains("SERVI"))
                        {
                            strAuxiliar = Globals.glbFeeAmount[x].Trim().Replace("R$", "");
                            strAuxiliar = strAuxiliar.Trim().Replace(".", "");
                            strAuxiliar = strAuxiliar.Trim().Replace(" ", "");
                            nValorAdmnistrativo = Convert.ToDecimal(strAuxiliar);
                        }

                        if (Globals.glbFeeDescription[x].Trim().ToUpper().Contains("ADMIN"))
                        {
                            strAuxiliar = Globals.glbFeeAmount[x].Trim().Replace("R$", "");
                            strAuxiliar = strAuxiliar.Trim().Replace(".", "");
                            strAuxiliar = strAuxiliar.Trim().Replace(" ", "");
                            nValorAdmnistrativo = Convert.ToDecimal(strAuxiliar);
                        }
                    }
                }

                graphics.DrawString("Diaria/Pacote:" + (Globals.glbResumoDiariasUnit.Trim() + " " + string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotal)).PadLeft(26), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                if (nValorDesconto > 0)
                {
                    graphics.DrawString("Desconto:" + ("- " + string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorDesconto)).PadLeft(31), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                }
                if (nValorHoraExtra > 0)
                {
                    graphics.DrawString("Hora Extra:" + (string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorHoraExtra)).PadLeft(29), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                }
                graphics.DrawString("Proteções:" + string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorProtecoes).PadLeft(30), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("Acessórios:" + string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorAcessorios).PadLeft(29), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("Tx Administrativa:" + string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorAdmnistrativo).PadLeft(22), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                if (nValorRetorno > 0)
                {
                    graphics.DrawString("Tx Retorno:" + string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorRetorno).PadLeft(29), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                }
                Offset = Offset + 17;
                graphics.DrawString("T O T A L:" + string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalReserva).PadLeft(30), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                Offset = Offset + 17;
                if (Globals.glbLojMunCod.ToUpper().Trim().Contains("SAO"))
                {
                    graphics.DrawString("Restricao para circulacao no municipio de", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    graphics.DrawString("Sao Paulo - SP (Capital)", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    Offset = Offset + 17;
                    graphics.DrawString("Final de Placa           Dia da Semana", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    graphics.DrawString("    1 e 2                   2a Feira", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    graphics.DrawString("    3 e 4                   3a Feira", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    graphics.DrawString("    5 e 6                   4a Feira", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    graphics.DrawString("    7 e 8                   5a Feira", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    graphics.DrawString("    9 e 0                   6a Feira", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    Offset = Offset + 17;
                }
                graphics.DrawString("Caso o veiculo nao seja devolvido ao final   ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("da locacao, o contrato podera ser renovado   ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("automaticamente, por prazo indeterminado,    ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("ate a efetiva devolucao do veiculo,de acor-  ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("do com a tarifa nacional vigente, na data    ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("da renovacao. Para locacoes realizadas por   ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("um periodo de 22 a 30 dias, havera um limi-  ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("te de 4500 quilometros rodados no periodo,   ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("sendo cobrados a parte cada quilometro que   ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("exceder esse limite.                         ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                Offset = Offset + 17;
                graphics.DrawString("Ao assinar este documento declaro que rece-  ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("bi o veiculo em perfeitas condicoes de con-  ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("servacao e funcionamento e estou aderindo e  ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("vinculando-me as clausulas e condicoes do    ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("Contrato de Locacao de Veiculo e Outras      ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("Avencas da Unidas Rent a Car registrado sob  ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("Nr 1397035 junto ao 5 Oficial de Registros   ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("de Titulos e Documentos e Civil de Pessoa    ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("Juridica de Sao Paulo/SP, em especial quan-  ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("to ao item 7.7 do referido Contrato, que     ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("encontra-se exposto e foi entregue a mim     ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("na loja de contratacao da locacao.           ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                Offset = Offset + 17;
                graphics.DrawString("              Unidas Rent a Car              ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                Offset = Offset + 17;
                Offset = Offset + 17;
                graphics.DrawString("      -------------------------------        ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("           Assinatura do Cliente             ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                Offset = Offset + 17;
                Offset = Offset + 17;
                graphics.DrawString("-------------------------------------------- ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("-------------------------------------------- ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);

                if (!string.IsNullOrEmpty(Globals.glbCupomFiscal2))
                {
                    Offset = Offset + 17;
                    Offset = Offset + 17;
                    graphics.DrawString("               CUPOM FISCAL                  ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    Offset = Offset + 17;
                    Offset = Offset + 17;
                    graphics.DrawString(Globals.glbCupomFiscal2, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    for (int x = 0; x < 17; x++)
                    {
                        Offset = Offset + 17;
                    }
                }
                graphics.DrawString("--------------------------------------------", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                Offset = Offset + 17;
                graphics.DrawString("            Unidas S/A", new Font("Courier New", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("         Cupom nao Fiscal", new Font("Courier New", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                Offset = Offset + 17;
                graphics.DrawString("Via Loja", new Font("Courier New", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                Offset = Offset + 17;
                graphics.DrawString("Loja: " + Globals.glbPickUpLocation + "   Contrato:" + Globals.glbRA_Contrato, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("Veiculo: " + Globals.glbVEIPLA, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("Emissao: " + dtEmissao, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                Offset = Offset + 17;

                graphics.DrawString("Locadora: UNIDAS LOCADORA DE VEICULOS LTDA.", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("Assistencia 24 horas - 08007720994", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                Offset = Offset + 17;

                if (Globals.glbRA_r_atipo.Trim() == "F")
                {
                    // graphics.DrawString("LOCATARIO: " + Globals.glbRA_clinom, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    graphics.DrawString("LOCATARIO: " + Globals.glbCLI_CLINOM_SAUDACOES, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    graphics.DrawString("CPF: " + Globals.glbRA_clidoc, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                }
                else
                {
                    //if (Globals.glbRA_r_atipo.Trim() == "A")
                    //{
                    //    graphics.DrawString("LOCATARIO: " + Globals.glbRA_clinom, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    //    Offset = Offset + 17;
                    //    graphics.DrawString("CNPJ: " + Globals.glbRA_clidoc, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    //}
                    //else
                    //{
                    //    graphics.DrawString("LOCATARIO: " + Globals.glbCLI_CLINOM_SAUDACOES, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    //    Offset = Offset + 17;
                    //    graphics.DrawString("CPF: " + Globals.glbRA_clidoc, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    //}
                    if (Globals.glbRA_r_atipo.Trim() == "A")
                    {
                        graphics.DrawString("LOCATARIO: " + Globals.glbRA_clinom, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                        Offset = Offset + 17;
                        graphics.DrawString("CNPJ: " + Globals.glbRA_clidoc, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    }
                    else if (Globals.glbRA_r_atipo.Trim() == "J" && Convert.ToInt32(Globals.glbCLI_PRFCOD) != 336)
                    {
                        graphics.DrawString("LOCATARIO: " + Globals.glbCompanyShortName, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                        Offset = Offset + 17;
                        graphics.DrawString("CNPJ: " + Globals.glbRA_clidoc, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    }
                    else
                    {
                        graphics.DrawString("LOCATARIO: " + Globals.glbCLI_CLINOM_SAUDACOES, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                        Offset = Offset + 17;
                        graphics.DrawString("CPF: " + Globals.glbRA_clidoc, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    }
                    Offset = Offset + 17;
                    if (!string.IsNullOrEmpty(Globals.glbPRECPF))
                    {
                        graphics.DrawString("PREPOSTO: " + Globals.glbPRENOM, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                        Offset = Offset + 17;
                        graphics.DrawString("CPF: " + Globals.glbPRECPF, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                        Offset = Offset + 17;

                    }
                    graphics.DrawString("FATURAR P/: " + Globals.glbCompanyShortName, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                    graphics.DrawString("CNPJ: " + Globals.glbRA_clifat, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                }
                if (!string.IsNullOrEmpty(Globals.glbVoucherIdentifier))
                {
                    graphics.DrawString("Voucher: " + Globals.glbVoucherIdentifier + "  Reserva: " + Globals.glbReservaNumeroPesquisa, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                }
                else
                {
                    graphics.DrawString("Reserva: " + Globals.glbReservaNumeroPesquisa, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                }
                Offset = Offset + 17;
                Offset = Offset + 17;

                Globals.glbKM = Globals.glbKM.Replace("Km", "");

                graphics.DrawString("Veiculo: " + Globals.glbVEIPLA + " Grupo: " + Globals.glbResGruCod, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("Modelo: " + Globals.glbMODDES + " - " + Globals.glbTPMDES, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("Km:" + Globals.glbKM, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("Retirada: " + Globals.glbPickUpLocation + " - " + strPickup, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("Devolucao: " + Globals.glbReturnLocation + " - " + strReturn, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);

                Offset = Offset + 17;
                Offset = Offset + 17;
                graphics.DrawString("PROTECOES: ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString(Globals.glbProtecoesVar1 + ": " + Globals.glbProtecoesValorVar1, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                if (!string.IsNullOrEmpty(Globals.glbProtecoesVar2))
                {
                    graphics.DrawString(Globals.glbProtecoesVar2 + ": " + Globals.glbProtecoesValorVar2, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                }
                if (!string.IsNullOrEmpty(Globals.glbProtecoesVar3))
                {
                    graphics.DrawString(Globals.glbProtecoesVar3 + ": " + Globals.glbProtecoesValorVar3, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                }
                Offset = Offset + 17;
                graphics.DrawString("ACESSORIOS: ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;

                if (Globals.glbPricedEquipDescription != null)
                    nAcessorio = Globals.glbPricedEquipDescription.Count();
                else
                    nAcessorio = 0;
                if (nAcessorio > 0)
                {
                    graphics.DrawString(Globals.glbPricedEquipDescription[0].Trim() + ": " + Globals.glbPricedEquipChargeAmount[0].Trim(), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                }
                else
                {
                    graphics.DrawString("NAO CONTRATADO", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                }
                if (nAcessorio > 1)
                {
                    graphics.DrawString(Globals.glbPricedEquipDescription[1].Trim() + ": " + Globals.glbPricedEquipChargeAmount[1].Trim(), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                }
                if (nAcessorio > 2)
                {
                    graphics.DrawString(Globals.glbPricedEquipDescription[2].Trim() + ": " + Globals.glbPricedEquipChargeAmount[2].Trim(), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                }
                if (nAcessorio > 3)
                {
                    graphics.DrawString(Globals.glbPricedEquipDescription[3].Trim() + ": " + Globals.glbPricedEquipChargeAmount[3].Trim(), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                }
                if (nAcessorio > 4)
                {
                    graphics.DrawString(Globals.glbPricedEquipDescription[4].Trim() + ": " + Globals.glbPricedEquipChargeAmount[4].Trim(), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                }
                if (nAcessorio > 5)
                {
                    graphics.DrawString(Globals.glbPricedEquipDescription[5].Trim() + ": " + Globals.glbPricedEquipChargeAmount[5].Trim(), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                }
                if (nAcessorio > 6)
                {
                    graphics.DrawString(Globals.glbPricedEquipDescription[6].Trim() + ": " + Globals.glbPricedEquipChargeAmount[6].Trim(), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                }
                if (nAcessorio > 7)
                {
                    graphics.DrawString(Globals.glbPricedEquipDescription[6].Trim() + ": " + Globals.glbPricedEquipChargeAmount[7].Trim(), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                }
                Offset = Offset + 17;
                underLine = "------------------------------------------";
                graphics.DrawString(underLine, new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;

                graphics.DrawString("TOTAIS", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("Diaria/Pacote:" + (Globals.glbResumoDiariasUnit.Trim() + " " + string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotal)).PadLeft(26), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                if (nValorDesconto > 0)
                {
                    graphics.DrawString("Desconto:" + ("- " + string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorDesconto)).PadLeft(31), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                }
                if (nValorHoraExtra > 0)
                {
                    graphics.DrawString("Hora Extra:" + (string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorHoraExtra)).PadLeft(29), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                }
                graphics.DrawString("Proteções:" + string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorProtecoes).PadLeft(30), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("Acessórios:" + string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorAcessorios).PadLeft(29), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("Tx Administrativa:" + string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorAdmnistrativo).PadLeft(22), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                if (nValorRetorno > 0)
                {
                    graphics.DrawString("Tx Retorno:" + string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorRetorno).PadLeft(29), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 17;
                }
                Offset = Offset + 17;
                graphics.DrawString("T O T A L:" + string.Format(new CultureInfo("pt-BR"), "{0:C}", nValorTotalReserva).PadLeft(30), new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                Offset = Offset + 17;
                Offset = Offset + 17;

                graphics.DrawString("              Unidas Rent a Car              ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                Offset = Offset + 17;
                Offset = Offset + 17;
                graphics.DrawString("      -------------------------------        ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                graphics.DrawString("           Assinatura do Cliente             ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 17;
                Offset = Offset + 17;
                Offset = Offset + 17;

                Offset = Offset + 17;
                Offset = Offset + 17;
                graphics.DrawString("-------------------------------------------- ", new Font("Courier New", 7, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset);

            }
            catch (Exception ex)
            {
                timerDisabled();
                Globals.glbMensagem = 1001;
                frmMensagem frmMsg = new frmMensagem();
                frmMsg.Show();
                this.Close();
                this.Dispose();
            }

        }

        private string CreateString(string Campo)
        {
            string OutputString = "";
            string rndstring;

            for (int i = 0; i < Campo.Length; i++)
            {
                rndstring = GetRandom(1);
                OutputString += rndstring + GetRandom(Convert.ToInt32(rndstring)) + Campo[i];
            }
            return OutputString;
        }
        private int obtemContratoXMLviaWCF(string xmlEncodedList)
        {
            try
            {
                Globals.glbCC_R_ANUM_RA = "";
                Globals.glbCRTEXPCRP = "";
                Globals.glbCCACODSEGC = "";
                Globals.glbCCANUMCRP = "";
                Globals.glbCCANUM = "";

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xmlEncodedList);

                string strR_ANUM = "";
                string strR_ALOJRET = "";
                string strCCAITENUM = "";
                string strCRTEXPCRP = "";
                string strCCACODSEGC = "";
                string strCCANUMCRP = "";
                string strCCANUM = "";

                XmlNodeList xml1 = xmlDoc.GetElementsByTagName("R_ANUM");
                if (xml1.Count > 0)
                {
                    for (int x = 0; x < xml1.Count; x++)
                    {
                        if (!string.IsNullOrEmpty(xml1.Item(x).InnerXml))
                        {
                            strR_ANUM = xml1.Item(x).InnerXml;
                            Globals.glbCC_R_ANUM_RA = strR_ANUM.Trim();
                        }
                    }
                }

                XmlNodeList xml2 = xmlDoc.GetElementsByTagName("CRTEXPCRP");
                if (xml2.Count > 0)
                {
                    for (int x = 0; x < xml2.Count; x++)
                    {
                        if (!string.IsNullOrEmpty(xml2.Item(x).InnerXml))
                        {
                            strCRTEXPCRP = xml2.Item(x).InnerXml;
                            Globals.glbCRTEXPCRP = strCRTEXPCRP.Trim();
                        }
                    }
                }

                XmlNodeList xml3 = xmlDoc.GetElementsByTagName("CCACODSEGC");
                if (xml3.Count > 0)
                {
                    for (int x = 0; x < xml3.Count; x++)
                    {
                        if (!string.IsNullOrEmpty(xml3.Item(x).InnerXml))
                        {
                            strCCACODSEGC = xml3.Item(x).InnerXml;
                            Globals.glbCCACODSEGC = strCCACODSEGC.Trim();
                        }
                    }
                }

                XmlNodeList xml4 = xmlDoc.GetElementsByTagName("CCANUMCRP");
                if (xml4.Count > 0)
                {
                    for (int x = 0; x < xml4.Count; x++)
                    {
                        if (!string.IsNullOrEmpty(xml4.Item(x).InnerXml))
                        {
                            strCCANUMCRP = xml4.Item(x).InnerXml;
                            Globals.glbCCANUMCRP = strCCANUMCRP.Trim();
                        }
                    }
                }

                XmlNodeList xml5 = xmlDoc.GetElementsByTagName("CCANUM");
                if (xml5.Count > 0)
                {
                    for (int x = 0; x < xml5.Count; x++)
                    {
                        if (!string.IsNullOrEmpty(xml5.Item(x).InnerXml))
                        {
                            strCCANUM = xml5.Item(x).InnerXml;
                            Globals.glbCCANUM = strCCANUM.Trim();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return 1;
        }

        private string GetRandom(int charnum)
        {
            string OutputRandom = "";
            for (int i = 1; i <= charnum; i++)
            {
                int n = _r.Next(1, 9);
                OutputRandom = OutputRandom + n.ToString();
            }
            return OutputRandom;
        }

        public string UnScramble(string HASH)
        {

            //string DT_VAL; // Data de validade 
            //string CR_CSE; // Código Segurança
            //string CR_NBR; // Número do cartão de credito

            int RAND;
            string TEMP = "";
            int POS;
            string NBR;
            string RESULT = "";
            int ITEM = 1;

            //	INVERTE ENTRADA
            char[] charArray = HASH.ToCharArray();
            Array.Reverse(charArray);
            // HASH = charArray.ToString();
            HASH = new string(charArray);


            while (HASH.Length > 0)
            {
                RAND = Convert.ToInt32(HASH.Substring(0, 1));
                TEMP = HASH.Substring(0, ITEM + Convert.ToInt32(HASH.Substring(0, 1)) + ITEM);
                POS = TEMP.Length;
                NBR = TEMP.Substring(TEMP.Length - 1, 1);
                RESULT = RESULT + NBR.ToString();
                HASH = HASH.Substring(POS, HASH.Length - POS);
            }
            return RESULT;
        }

        private string Scramble(string CR_NBR, string CR_CSE, string DT_VAL)
        {
            string ST_RETURN;
            string LN_NBR;

            ST_RETURN = "";

            LN_NBR = GetRandom(1);
            ST_RETURN = LN_NBR.ToString() + GetRandom(Convert.ToInt32(LN_NBR)).ToString();
            ST_RETURN = ST_RETURN + CR_NBR.Length.ToString().Substring(0, 1);

            LN_NBR = GetRandom(1);
            ST_RETURN = ST_RETURN + LN_NBR.ToString() + GetRandom(Convert.ToInt32(LN_NBR)).ToString();
            ST_RETURN = ST_RETURN + CR_NBR.Length.ToString().Substring(1, 1);

            ST_RETURN = ST_RETURN + CreateString(CR_NBR);

            LN_NBR = GetRandom(1);
            ST_RETURN = ST_RETURN + LN_NBR.ToString() + GetRandom(Convert.ToInt32(LN_NBR)).ToString();
            ST_RETURN = ST_RETURN + CR_CSE.Length.ToString() + CreateString(CR_CSE);

            LN_NBR = GetRandom(1);
            ST_RETURN = ST_RETURN + LN_NBR.ToString() + GetRandom(Convert.ToInt32(LN_NBR)).ToString();
            ST_RETURN = ST_RETURN + DT_VAL.Length.ToString() + CreateString(DT_VAL);

            char[] charArray = ST_RETURN.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        private int SetAberturaRA_Final()
        {
            string numeroContratoRA = "0";
            try
            {
                // Abertura de RA - 4 (cadastro)
                int pR_ACONFEC = 4;
                Globals.glbR_ACONFEC = pR_ACONFEC;

                string pR_AHORRET = Globals.glbRA_r_ahorret.Substring(0, 5);
                string pR_AHORDEV = Globals.glbRA_r_ahordev.Substring(11, 5);

                DateTime dtAtual = DateTime.Now;
                string strDataAtual = dtAtual.ToString("yyyy-MM-dd");
                string strHoraAtual = dtAtual.ToString("HH:mm:ss");
                string strDataCompleta = strDataAtual + "T00:00:00";
                string strDataHora = strDataAtual + "T" + strHoraAtual;

                CentralUnidasClassClient clientWcf = new CentralUnidasClassClient();

                numeroContratoRA = Globals.glbRA_Contrato.Trim();
                if (Convert.ToInt32(numeroContratoRA.Trim()) > 0)
                {

                    Globals.glbCC_R_ANUM = numeroContratoRA;

                    // Atualiza cartão
                    string mULTDATC6 = DateTime.Now.ToString("yyyy-MM-dd");
                    string mULTHORC6 = DateTime.Now.ToString("HH:mm:ss");

                    string xmlRetornoWCF_SEG = clientWcf.SetSegurancaRA(Globals.glbCC_R_ANUM.Trim(),
                        Globals.glbRA_r_alojret.Trim(),
                        "1",
                        Convert.ToDecimal(Globals.glbProtecoesValorPre1),
                        mULTDATC6,
                        mULTHORC6,
                        Globals.glbRESDOCCLI.Trim(),
                        Globals.glbBandeiraCartao,
                        Globals.glbCC_CodigoPreAutorizacao.Trim(),
                        Globals.glbCC_CodigoAutorizacaoEmissor.Trim());

                    string mSeguranca = "";
                    string mValidade = "";
                    Globals.glbSEG_HASH_NEW = "";
                    string xmlRetornoWCF_CC = "";

                    if (!string.IsNullOrEmpty(Globals.glbCC_CartaoInformado.Trim()))
                    {

                        if (!string.IsNullOrEmpty(Globals.glbCC_SegurancaInformado.Trim()))
                        {
                            mSeguranca = Globals.glbCC_ValidadeInformado.Trim();
                        }
                        if (!string.IsNullOrEmpty(Globals.glbCC_ValidadeInformado.Trim()))
                        {
                            mValidade = Globals.glbCC_SegurancaInformado.Trim().Substring(4, 2) + Globals.glbCC_SegurancaInformado.Trim().Substring(2, 2);
                        }
                        string strCCScramble = Scramble(Globals.glbCC_CartaoInformado.Trim(), mSeguranca.Trim(), mValidade.Trim());
                        Globals.glbSEG_HASH_NEW = strCCScramble;
                        if (!string.IsNullOrEmpty(Globals.glbSEG_HASH_NEW))
                        {
                            // Verifica Rotina Cartao
                            string pR_ANUM = Globals.glbCC_R_ANUM.Trim();
                            string pR_ALOJRET = Globals.glbRA_r_alojret.Trim();
                            int pPAGITENUM = 1;
                            string pHASH = Globals.glbSEG_HASH_NEW.Trim();
                            string pTP_INFO = "A";
                            string pTP_OPER = "I";
                            string pHASH_OUT = "";
                            string pRT_OPER = "";
                            string pCpf = Globals.glbRESDOCCLI.Trim();
                            xmlRetornoWCF_CC = clientWcf.SetConsultaTotem(pR_ANUM, pR_ALOJRET, pPAGITENUM, pHASH, pTP_INFO, pTP_OPER, pHASH_OUT, pRT_OPER, pCpf);
                        }
                    }
                    else
                    {
                        Globals.glbCC_R_ANUM_RA = "";
                        string xmlRetornoWCF = clientWcf.GetContratoTotem(Globals.glbRESDOCCLI.Trim().Substring(0, 11));
                        if (!string.IsNullOrEmpty(xmlRetornoWCF))
                        {
                            int iRetContrato = obtemContratoXMLviaWCF(xmlRetornoWCF);
                            xmlRetornoWCF_CC = clientWcf.SetSegurancaTotem(Globals.glbCC_R_ANUM.Trim(), Globals.glbCCANUM.Trim(), Globals.glbCRTEXPCRP.Trim(), Globals.glbCCACODSEGC.Trim(), Globals.glbCCANUMCRP.Trim());
                        }
                    }
                    xmlRetornoWCF_CC = clientWcf.SetAtualizaConfec(Convert.ToInt32(Globals.glbCC_R_ANUM.Trim()), 0);
                    xmlRetornoWCF_CC = clientWcf.SetReservaContratoRA(Convert.ToInt32(Globals.glbReservaNumeroPesquisa), Convert.ToInt32(Globals.glbCC_R_ANUM.Trim()));
                    xmlRetornoWCF_CC = clientWcf.SetAtualizaFlag(Convert.ToInt32(Globals.glbCC_R_ANUM.Trim()));
                }
                clientWcf.Close();
            }
            catch (Exception ex)
            {
                return 0;
            }
            return Convert.ToInt32(numeroContratoRA.Trim());
        }

        private void frmScreen11_Load(object sender, EventArgs e)
        {


            LogTotem.InsertLog(new LogOperacao()
            {
                Application_Name = Globals.glbApplicationName,
                Application_Version = Globals.glbApplicationVersion,
                Local = Globals.glbLojaTotem,
                Operation_Ticks = Globals.glbOperationTicks,
                Operation_Name = "Transação Efetuada com Sucesso",
                Form = "FrmScreen11",
                RESNUM = String.IsNullOrEmpty(Globals.glbReservaNumeroPesquisa) ? 0 : Convert.ToInt32(Globals.glbReservaNumeroPesquisa),
                RANUM = String.IsNullOrEmpty(Globals.glbRA_Contrato) ? 0 : Convert.ToInt32(Globals.glbRA_Contrato)

            });

            try
            {

                tmrReturn.Enabled = false;

                Globals.glbDebugMode = Convert.ToInt16(ConfigurationManager.AppSettings.Get("appDebugMode"));
                Shutdown_Enable();

                Globals.glbPickUpDateTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");

                string strMensagem = string.Empty;

                if (!string.IsNullOrEmpty(Globals.glbCLI_CLINOM_SAUDACOES.Trim()))
                {
                    strNome = "";
                    for (int x = 0; x < Globals.glbCLI_CLINOM_SAUDACOES.Trim().Length; x++)
                    {
                        if (Globals.glbCLI_CLINOM_SAUDACOES.Substring(x, 1) == " ") break;
                        strNome += Globals.glbCLI_CLINOM_SAUDACOES.Substring(x, 1);
                    }
                }

                if (!string.IsNullOrEmpty(Globals.glbPRENOM.Trim()))
                {
                    strNome = "";
                    for (int x = 0; x < Globals.glbPRENOM.Trim().Length; x++)
                    {
                        if (Globals.glbPRENOM.Substring(x, 1) == " ") break;
                        strNome += Globals.glbPRENOM.Substring(x, 1);
                    }
                }

                int nScreenWidth = this.Width;
                int nScreenHeight = this.Height;
                int nScreenWidthSplit = nScreenWidth / 2;
                int nScreenHeightSplit = nScreenHeight / 2;

                AtualizarInterfaceParaIdiomaCorrente();


                //lblObrigadoEscolherUnidas.Text = lblObrigadoEscolherUnidas.Text + ", " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(strNome.ToLower()) + "!";
                lblObrigadoEscolherUnidas.Location = new Point((nScreenWidthSplit - (lblObrigadoEscolherUnidas.Width / 2)), lblObrigadoEscolherUnidas.Location.Y);

                //lblTransacaoEfetuadaSucesso.Refresh();
                //lblObrigadoEscolherUnidas.Refresh();
                //lblRetireComprovanteLocacao.Refresh();
                //lblBalcaoRetirarChaves.Refresh();

                this.Refresh();

                string xmlRetornoWCF = "";
                CentralUnidasClassClient clientWcf = new CentralUnidasClassClient();

                if (!string.IsNullOrEmpty(Globals.glbRA_Contrato.Trim()))
                {
                    xmlRetornoWCF = clientWcf.getCupomFiscal(Convert.ToInt32(Globals.glbReservaNumeroPesquisa));
                    if (string.IsNullOrEmpty(xmlRetornoWCF) || (xmlRetornoWCF.Trim() == "<UNIDASWCF />"))
                    {
                        Globals.glbCupomFiscal1 = "";
                        Globals.glbCupomFiscal2 = "";
                        Globals.glbCC_CodigoPreAutorizacao = "";
                        Globals.glbCC_CodigoAutorizacaoEmissor = "";
                        Globals.glbBandeiraCartao = "";
                    }
                    else
                    {
                        xmlRetornoWCF = xmlRetornoWCF.Replace("\r\n", "");
                        DataSet dtSet = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(xmlRetornoWCF);
                        dtSet.ReadXml(xmlSR);
                        foreach (DataRow dbRow in dtSet.Tables["Table"].Rows)
                        {

                            Globals.glbCupomFiscal1 = dbRow["CUPOM_CLIENTE"].ToString().Trim();
                            Globals.glbCupomFiscal2 = dbRow["CUPOM_LOJA"].ToString().Trim();
                            Globals.glbCC_CodigoPreAutorizacao = dbRow["CODIGO_PREAUTORIZACAO"].ToString().Trim();
                            Globals.glbCC_CodigoAutorizacaoEmissor = dbRow["CODIGO_EMISSOR"].ToString().Trim();

                            string strCupomString = "";
                            strCupomString = Globals.glbCupomFiscal1.Trim();
                            if (strCupomString.ToUpper().Contains("REDE")) Globals.glbBandeiraCartao = "RED";
                            if (strCupomString.ToUpper().Contains("AMERICAN")) Globals.glbBandeiraCartao = "AMX";
                            if (strCupomString.ToUpper().Contains("CIELO")) Globals.glbBandeiraCartao = "CIE";
                            if (strCupomString.ToUpper().Contains("DINERS")) Globals.glbBandeiraCartao = "DNR";
                            if (strCupomString.ToUpper().Contains("ELO")) Globals.glbBandeiraCartao = "ELO";
                            if (strCupomString.ToUpper().Contains("MASTERCARD")) Globals.glbBandeiraCartao = "MAS";
                            if (strCupomString.ToUpper().Contains("VISA")) Globals.glbBandeiraCartao = "VIS";

                            break;
                        }
                    }
                }


                // AQUI 22.01.2016
                int nAberturaRA = SetAberturaRA_Final();
                Globals.glbRA_Contrato = nAberturaRA.ToString().Trim();
                Globals.glbCC_R_ANUM = nAberturaRA.ToString().Trim();

                Globals.glbResumoHoraExtraQtd = "";
                Globals.glbResumoHoraExtraVar = "";

                if (!string.IsNullOrEmpty(Globals.glbCC_R_ANUM.Trim()))
                {
                    Globals.glbRA_Contrato = Globals.glbCC_R_ANUM.Trim();

                    xmlRetornoWCF = clientWcf.SetAtualizaFlag(Convert.ToInt32(Globals.glbCC_R_ANUM.Trim()));
                    xmlRetornoWCF = clientWcf.SetAtualizaConfec(Convert.ToInt32(Globals.glbCC_R_ANUM.Trim()), 0);

                    int nAcessorios = 0;
                    int nProtecoes = 0;
                    int nTaxas = 0;

                    xmlRetornoWCF = clientWcf.GetContratoRAAberto(Convert.ToInt32(Globals.glbReservaNumeroPesquisa), Convert.ToInt32(Globals.glbCC_R_ANUM));
                    if (!string.IsNullOrEmpty(xmlRetornoWCF) || (xmlRetornoWCF.Trim() == "<UNIDASWCF />"))
                    {
                        xmlRetornoWCF = xmlRetornoWCF.Replace("\r\n", "");
                        DataSet dtSet = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(xmlRetornoWCF);
                        dtSet.ReadXml(xmlSR);

                        foreach (DataRow dbRow in dtSet.Tables["Table"].Rows)
                        {
                            string strSRVDSC = dbRow["SRVDSC"].ToString().Trim();
                            if (strSRVDSC.ToUpper().Contains("ADICIONAIS"))
                            {
                                nAcessorios = nAcessorios + 1;
                            }
                            if (strSRVDSC.ToUpper().Contains("TAXA"))
                            {
                                nTaxas = nTaxas + 1;
                            }
                            if (strSRVDSC.ToUpper().Contains("PROTE"))
                            {
                                nProtecoes = nProtecoes + 1;
                            }
                        }

                        if (nTaxas > 0)
                        {
                            Globals.glbFeeDescription = new string[nTaxas];
                            Globals.glbFeeAmount = new string[nTaxas];
                        }

                        if (nAcessorios > 0)
                        {
                            Globals.glbPricedEquipDescription = new string[nAcessorios];
                            Globals.glbPricedEquipChargeAmount = new string[nAcessorios];
                        }

                        nAcessorios = 0;
                        nTaxas = 0;
                        int nProtecao = 0;

                        foreach (DataRow dbRow in dtSet.Tables["Table"].Rows)
                        {
                            string strSRVDSC = dbRow["SRVDSC"].ToString().Trim();
                            if (strSRVDSC.ToUpper().Contains("ADICIONAIS"))
                            {
                                Globals.glbPricedEquipDescription[nAcessorios] = dbRow["CATSRVDSC"].ToString().Trim();
                                Globals.glbPricedEquipChargeAmount[nAcessorios] = string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDouble(dbRow["CATSRVVAL"].ToString().Trim().Replace(".", ",")));
                                nAcessorios++;
                            }
                            if (strSRVDSC.ToUpper().Contains("TAXA"))
                            {
                                Globals.glbFeeDescription[nTaxas] = dbRow["CATSRVDSC"].ToString().Trim();
                                string strServico = dbRow["CATSRVDSC"].ToString().Trim();
                                if (strServico.ToUpper().Contains("SERV"))
                                {
                                    Globals.glbFeeAmount[nTaxas] = string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDouble(dbRow["R_ATAXSER"].ToString().Trim().Replace(".", ",")));
                                }
                                if (strServico.ToUpper().Contains("RETO"))
                                {
                                    Globals.glbFeeAmount[nTaxas] = string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDouble(dbRow["R_ATAXRET"].ToString().Trim().Replace(".", ",")));
                                }
                                nTaxas++;
                            }
                            if (strSRVDSC.ToUpper().Contains("PROTE"))
                            {
                                if (nProtecao == 0)
                                {
                                    Globals.glbProtecoesVar1 = dbRow["CATSRVDSC"].ToString().Trim();
                                    Globals.glbProtecoesValorVar1 = string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDouble(dbRow["CATSRVVAL"].ToString().Trim().Replace(".", ",")));
                                }
                                if (nProtecao == 1)
                                {
                                    Globals.glbProtecoesVar2 = dbRow["CATSRVDSC"].ToString().Trim();
                                    Globals.glbProtecoesValorVar2 = string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDouble(dbRow["CATSRVVAL"].ToString().Trim().Replace(".", ",")));
                                }
                                if (nProtecao == 2)
                                {
                                    Globals.glbProtecoesVar3 = dbRow["CATSRVDSC"].ToString().Trim();
                                    Globals.glbProtecoesValorVar3 = string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDouble(dbRow["CATSRVVAL"].ToString().Trim().Replace(".", ",")));
                                }
                                nProtecao++;
                            }

                            Globals.glbResumoHoraExtraQtd = dbRow["R_AQTDHORE"].ToString().Trim();
                            Globals.glbResumoHoraExtraVar = string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDouble(dbRow["R_ATOTHORE"].ToString().Trim().Replace(".", ",")));
                            Globals.glbResumoDiariasUnit = dbRow["R_AQTDDIA"].ToString().Trim();
                            Globals.glbResumoDiariasVar = string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDouble(dbRow["R_ATOTDIA"].ToString().Trim().Replace(".", ",")));
                            Globals.glbResumoDescontoVar = string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDouble(dbRow["R_ATOTDSC"].ToString().Trim().Replace(".", ",")));
                            Globals.glbResumoTotalGeral = string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDouble(dbRow["R_ASALDO2"].ToString().Trim().Replace(".", ",")));
                            Globals.glbGivenName = dbRow["CLINOM"].ToString().Trim();
                        }
                    }
                }

                // Imprime Contrato RA
                printRA();
                tmrReturn.Enabled = true;
                Application.DoEvents();
            }
            catch (Exception ex)
            {
                timerDisabled();
                Globals.glbScreen = 1;
                Globals.glbMensagem = 1001;
                frmMensagem frmMsg = new frmMensagem();
                frmMsg.Show();
                this.Close();
                this.Dispose();
            }
        }
        private void Shutdown_Enable()
        {
            if (Globals.glbDebugMode == 0)
                pictShutdown.Visible = false;
            else
                pictShutdown.Visible = true;
        }

        private void pictReservaConfirmar_Click_1(object sender, EventArgs e)
        {
            timerDisabled();
            frmScreen01 frm02 = new frmScreen01();
            frm02.Show();
            this.Close();
            this.Dispose();
        }

        private void pictConcluir_Click(object sender, EventArgs e)
        {
            //tmrReturn.Stop();
            //tmrReturn.Enabled = false;
            timerDisabled();
            frmScreen01 frm01 = new frmScreen01();
            frm01.Show();
            this.Close();
            this.Dispose();
        }

        private void pictShutdown_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(1);
        }

        private void pictLang_Click(object sender, EventArgs e)
        {
            frmIdioma frmIdi = new frmIdioma();
            frmIdi.ShowDialog();

            if (Globals.glbLanguage == 1) pictLang.Image = centralUnidas.Properties.Resources.totem_langPT;
            if (Globals.glbLanguage == 2) pictLang.Image = centralUnidas.Properties.Resources.totem_langPT;
            if (Globals.glbLanguage == 3) pictLang.Image = centralUnidas.Properties.Resources.totem_langPT;
        }

        private void tmrReturn_Tick(object sender, EventArgs e)
        {
            tmrReturn.Enabled = false;

            tmrReturn.Enabled = false;

            CentralUnidasClassClient clientWcf = new CentralUnidasClassClient();
            string xmlRetornoWCF = clientWcf.getCupomFiscal(Convert.ToInt32(Globals.glbReservaNumeroPesquisa));
            if (string.IsNullOrEmpty(xmlRetornoWCF) || (xmlRetornoWCF.Trim() == "<UNIDASWCF />"))
            {
                Globals.glbCupomFiscal1 = "";
                Globals.glbCupomFiscal2 = "";
                Globals.glbCC_CodigoPreAutorizacao = "";
                Globals.glbCC_CodigoAutorizacaoEmissor = "";
                Globals.glbBandeiraCartao = "";
            }
            else
            {
                xmlRetornoWCF = xmlRetornoWCF.Replace("\r\n", "");
                DataSet dtSet = new DataSet();
                System.IO.StringReader xmlSR = new System.IO.StringReader(xmlRetornoWCF);
                dtSet.ReadXml(xmlSR);
                foreach (DataRow dbRow in dtSet.Tables["Table"].Rows)
                {

                    Globals.glbCupomFiscal1 = dbRow["CUPOM_CLIENTE"].ToString().Trim();
                    Globals.glbCupomFiscal2 = dbRow["CUPOM_LOJA"].ToString().Trim();
                    Globals.glbCC_CodigoPreAutorizacao = dbRow["CODIGO_PREAUTORIZACAO"].ToString().Trim();
                    Globals.glbCC_CodigoAutorizacaoEmissor = dbRow["CODIGO_EMISSOR"].ToString().Trim();

                    string strCupomString = "";
                    strCupomString = Globals.glbCupomFiscal1.Trim();
                    if (strCupomString.ToUpper().Contains("REDE")) Globals.glbBandeiraCartao = "RED";
                    if (strCupomString.ToUpper().Contains("AMERICAN")) Globals.glbBandeiraCartao = "AMX";
                    if (strCupomString.ToUpper().Contains("CIELO")) Globals.glbBandeiraCartao = "CIE";
                    if (strCupomString.ToUpper().Contains("DINERS")) Globals.glbBandeiraCartao = "DNR";
                    if (strCupomString.ToUpper().Contains("ELO")) Globals.glbBandeiraCartao = "ELO";
                    if (strCupomString.ToUpper().Contains("MASTERCARD")) Globals.glbBandeiraCartao = "MAS";
                    if (strCupomString.ToUpper().Contains("VISA")) Globals.glbBandeiraCartao = "VIS";

                    break;
                }
            }
            timerDisabled();
            frmScreen01 frm01 = new frmScreen01();
            frm01.Show();
            this.Close();
            this.Dispose();
        }

        private void butConcluir_Click(object sender, EventArgs e)
        {
            timerDisabled();
            frmScreen01 frm01 = new frmScreen01();
            frm01.Show();
            this.Close();
            this.Dispose();
        }

        private void AtualizarInterfaceParaIdiomaCorrente()
        {
            MontarResourceSetParaIdiomaCorrente();

            butConcluir.Text = ResxSetForm.GetString("butConcluir.Text");
            lblBalcaoRetirarChaves.Text = ResxSetForm.GetString("lblBalcaoRetirarChaves.Text");
            lblObrigadoEscolherUnidas.Text = ResxSetForm.GetString("lblObrigadoEscolherUnidas.Text") + ", " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(strNome.ToLower()) + "!"; ;
            lblRetireComprovanteLocacao.Text = ResxSetForm.GetString("lblRetireComprovanteLocacao.Text");
            lblTransacaoEfetuadaSucesso.Text = ResxSetForm.GetString("lblTransacaoEfetuadaSucesso.Text");
        }

        private void MontarResourceSetParaIdiomaCorrente()
        {
            string srtCaminhoExeTotem = System.IO.Path.GetDirectoryName(Application.ExecutablePath.ToString());

            // Português
            if (Globals.glbLanguage == 1)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen11.pt.resx");
            }
            //Espanhol
            else if (Globals.glbLanguage == 2)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen11.es.resx");
            }
            // Inglês
            else if (Globals.glbLanguage == 3)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen11.en.resx");
            }
        }

        private void timerDisabled()
        {
            tmrReturn.Enabled = false;
        }
    }
}

