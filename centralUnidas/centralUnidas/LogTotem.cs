﻿using centralUnidas.centralUnidasWCF;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace centralUnidas
{
    public static class LogTotem
    {
        public static void InsertLog(LogOperacao pOperacao)
        {
            try
            {
                using (centralUnidasWCF.CentralUnidasClassClient UnicasWCF = new CentralUnidasClassClient())
                {
                    string s = UnicasWCF.InsertLog(
                        pOperacao.Application_Name
                        , pOperacao.Application_Version
                        , pOperacao.Local
                        , pOperacao.Operation_Ticks
                        , pOperacao.Operation_Name
                        , pOperacao.Form
                        , pOperacao.RESNUM
                        , pOperacao.RANUM
                        , pOperacao.Status
                        , pOperacao.Message
                        , pOperacao.Output_XML
                        , pOperacao.HostName
                        , pOperacao.DbUsername
                        , pOperacao.LoginName
                        , pOperacao.Client_Net_Address
                        , pOperacao.Client_Mac_Address
                        , pOperacao.Client_Connection_Id
                        );
                }
            }
            catch (Exception ex)
            {                
                
            }
             
        }

        public static string RetornaHostName(string strHost)
        {

            string strHabilitaResolveDns = ConfigurationManager.AppSettings["habilitaResolveDNS"].ToString().Trim();
            if (strHabilitaResolveDns.Trim().ToLower() == "true")
            {
                try
                {
                    IPHostEntry hostEntry = Dns.GetHostEntry(strHost);
                    strHost = strHost + "_" + hostEntry.HostName.ToString().Trim();
                }
                catch
                {
                    return strHost;
                }
            }
            return strHost;
        }
    }
}
