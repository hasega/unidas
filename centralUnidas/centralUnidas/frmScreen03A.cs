﻿// Aplicação: Unidas Totem
// Data     : 01/10/2015
// Versão   : 1.0
// Empresa  : Unidas Rent a Car
// Equipe   : Unidas Desenvolvimento
// Histórico: 
//      01/10/2015: Inicio de desenvolvimento para acesso geral
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace centralUnidas
{
    public partial class frmScreen03A : Form
    {
        animation animate;
        Graphics g;
        Graphics scG;
        System.Windows.Forms.Timer t;
        public ResXResourceSet ResxSetForm; 

        Bitmap btm;

        private void OnFrameChanged(object o, EventArgs e)
        {
            this.Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
        }

        public frmScreen03A()
        {
            InitializeComponent();
        }

        private void frmScreen03A_Load(object sender, EventArgs e)
        {

            LogTotem.InsertLog(new LogOperacao()
            {
                Application_Name = Globals.glbApplicationName,
                Application_Version = Globals.glbApplicationVersion,
                Local = Globals.glbLojaTotem,
                Operation_Ticks = Globals.glbOperationTicks,
                Operation_Name = "Validação Biometria - Tente Novamente",
                Form = "FrmScreen03A",
                RESNUM = Convert.ToInt32(Globals.glbReservaNumeroPesquisa)
            });

            AtualizarInterfaceParaIdiomaCorrente();

            g = this.CreateGraphics();
            btm = new Bitmap(this.Width, this.Height);
            scG = Graphics.FromImage(btm);

            t = new System.Windows.Forms.Timer();

            t.Interval = 100;
            t.Tick += new EventHandler(t_Tick);

            animate = new animation(new Bitmap[] { centralUnidas.Properties.Resources.totem_biometria_img1, centralUnidas.Properties.Resources.totem_biometria_img3, centralUnidas.Properties.Resources.totem_biometria_img2 });

            t.Start();

         


        }
        private void t_Tick(object sender, EventArgs e)
        {
            scG.Clear(Color.White);
            scG.DrawImage(animate.GiveNextImage(), new Point(640, 190));
            g.DrawImage(btm, Point.Empty);
            System.Threading.Thread.Sleep(800);
            Application.DoEvents();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            t.Enabled = false;
            timerDisabled();
            frmScreen03 frm3 = new frmScreen03();
            frm3.Show();
            this.Dispose();
        }

        private void tmrReturn_Tick(object sender, EventArgs e)
        {
            t.Stop();
            t.Enabled = false;
            tmrReturn.Enabled = false;
            timerDisabled();
            frmScreen03 frm3 = new frmScreen03();
            frm3.Show();
            this.Dispose();
        }

        private void pictLang_Click(object sender, EventArgs e)
        {
            frmIdioma frmIdi = new frmIdioma();
            frmIdi.ShowDialog();

            if (Globals.glbLanguage == 1) pictLang.Image = centralUnidas.Properties.Resources.totem_langPT;
            if (Globals.glbLanguage == 2) pictLang.Image = centralUnidas.Properties.Resources.totem_langPT;
            if (Globals.glbLanguage == 3) pictLang.Image = centralUnidas.Properties.Resources.totem_langPT;

        }

        private void butVoltar_Click(object sender, EventArgs e)
        {
            t.Enabled = false;
            timerDisabled();
            frmScreen03 frm3 = new frmScreen03();
            frm3.Show();
            this.Dispose();
        }



        private void AtualizarInterfaceParaIdiomaCorrente()
        {

            MontarResourceSetParaIdiomaCorrente();

            butIdioma.Text = ResxSetForm.GetString("butIdioma.Text");
            butVoltar.Text = ResxSetForm.GetString("butVoltar.Text");
            lblTenteNovamente.Text = ResxSetForm.GetString("lblTenteNovamente.Text");
            this.Icon = ((System.Drawing.Icon)(ResxSetForm.GetObject("$this.Icon")));

            if (Globals.glbLanguage == 1) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_Brasil_botao;
            if (Globals.glbLanguage == 2) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_Espanha_botao;
            if (Globals.glbLanguage == 3) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_UK_botao;

            butIdioma.Refresh();
        }

        private void MontarResourceSetParaIdiomaCorrente()
        {
            string srtCaminhoExeTotem = System.IO.Path.GetDirectoryName(Application.ExecutablePath.ToString());
            // Português
            if (Globals.glbLanguage == 1)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen03A.pt.resx");
            }
            //Espanhol
            else if (Globals.glbLanguage == 2)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen03A.es.resx");
            }
            // Inglês            
            else if (Globals.glbLanguage == 3)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen03A.en.resx");

            }
        }

        private void butIdioma_Click(object sender, EventArgs e)
        {
            frmIdioma frmIdi = new frmIdioma();
            frmIdi.ShowDialog();
            AtualizarInterfaceParaIdiomaCorrente();
        }

        private void timerDisabled()
        {
            tmrReturn.Enabled = false;
        }
     
    }
}
