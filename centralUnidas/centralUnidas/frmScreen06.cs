﻿// Aplicação: Unidas Totem
// Data     : 01/10/2015
// Versão   : 1.0
// Empresa  : Unidas Rent a Car
// Equipe   : Unidas Desenvolvimento
// Objetivo : Escolha da Placa
// Histórico: 
//      01/10/2015: Inicio de desenvolvimento para acesso geral
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using centralUnidas.centralUnidasWCF;
using System.Xml;
using System.Runtime.InteropServices;
using System.Windows;
using System.Resources;

namespace centralUnidas
{
    public partial class frmScreen06 : Form
    {
        [DllImport("user32")]
        static public extern bool ShowScrollBar(System.IntPtr hWnd, int wBar, bool bShow);

        [DllImport("user32.dll")]
        static public extern bool EnableScrollBar(System.IntPtr hWnd, uint wSBflags, uint wArrows);

        [DllImport("user32.dll")]
        static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, UIntPtr wParam, IntPtr lParam);

        private const uint SB_HORZ = 0;
        const Int32 LVM_FIRST = 0x1000;

        const Int32 LVM_SCROLL = LVM_FIRST + 20;

        const int SBS_HORZ = 0;

        private const uint SB_VERT = 1;
        private const uint ESB_DISABLE_BOTH = 0x3;
        private const uint ESB_ENABLE_BOTH = 0x0;
        private const int WM_VSCROLL = 0x115;

        public ResXResourceSet ResxSetForm; 

        private void HideHorizontalScrollBar()
        {
            ShowScrollBar(this.lstPlacas.Handle, (int)SB_VERT, false);
        }

        private void UpdateListview()
        {
            int curItem = this.lstPlacas.TopItem.Index;
            if (curItem > 0)
            {
                this.lstPlacas.TopItem = this.lstPlacas.Items[curItem - 1];
            }

        }

        public frmScreen06()
        {
            InitializeComponent();
        }

        private void frmScreen06_Load(object sender, EventArgs e)
        {

            LogTotem.InsertLog(new LogOperacao()
            {
                Application_Name = Globals.glbApplicationName,
                Application_Version = Globals.glbApplicationVersion,
                Local = Globals.glbLojaTotem,
                Operation_Ticks = Globals.glbOperationTicks,
                Operation_Name = "Seleção Placa do Carro",
                Form = "FrmScreen06",
                RESNUM = String.IsNullOrEmpty(Globals.glbReservaNumeroPesquisa) ? 0 : Convert.ToInt32(Globals.glbReservaNumeroPesquisa),
                RANUM = String.IsNullOrEmpty(Globals.glbRA_Contrato) ? 0 : Convert.ToInt32(Globals.glbRA_Contrato)
            });

            Globals.glbScreen = 6;
            int nScreenWidth = this.Width;
            int nScreenWidthSplit = nScreenWidth / 2;

            EnableScrollBar(this.lstPlacas.Handle, (int)SB_VERT, ESB_DISABLE_BOTH);

            CultureInfo culture = new CultureInfo("pt-BR");
            int intLanguage = Globals.glbLanguage;
            switch (intLanguage)
            {
                case 1:
                    // Portugues
                    culture = new CultureInfo("pt-BR");

                    lblModelo01.Text = "Modelo " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Globals.glbGRUDES.Trim().ToLower()) + " escolhido";
                    lblModelo01.Refresh();

                    break;
                case 2:
                    // Espanhol
                    culture = new CultureInfo("Es-es");

                    lblModelo01.Text = "Usted ha elegido el modelo " +
                        CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Globals.glbGRUCOD.Trim().ToLower()) +
                        " - " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Globals.glbGRUDES.Trim().ToLower());
                    lblModelo01.Refresh();

                    break;
                case 3:
                    // Ingles 
                    culture = new CultureInfo("en-US");

                    lblModelo01.Text = "You have chosen the model " +
                        CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Globals.glbGRUCOD.Trim().ToLower()) +
                        " - " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Globals.glbGRUDES.Trim().ToLower());
                    lblModelo01.Refresh();

                    break;
                default:
                    // Portugues
                    culture = new CultureInfo("pt-BR");

                    lblModelo01.Text = "Modelo " +
                        CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Globals.glbGRUCOD.Trim().ToLower()) +
                        " - " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Globals.glbGRUDES.Trim().ToLower()) + " escolhido";
                    lblModelo01.Refresh();

                    break;
            }

            //lblModelo01.Text = "Modelo " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Globals.glbMODDES.Trim().ToLower()) + " - "
            //    + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Globals.glbTPMDES.Trim().ToLower()) + " escolhido:";
            //lblModelo01.Refresh();

            pictVeiculo1.Image = centralUnidas.Properties.Resources.GrupoIndisponivel;

            string strTipo = Globals.glbMODDES.Trim().ToUpper();

            pictVeiculo1.Image = centralUnidas.Properties.Resources.GrupoIndisponivel;

            if (strTipo.Contains("A4")) pictVeiculo1.Image = centralUnidas.Properties.Resources.AudiA4;
            if (strTipo.Contains("C3")) pictVeiculo1.Image = centralUnidas.Properties.Resources.C3Picasso;
            if (strTipo.Contains("CLASSIC")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Classic34;
            if (strTipo.Contains("UNO")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Unobranco4portas;
            if (strTipo.Contains("PALIO")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Palio2portasmerged;
            if (strTipo.Contains("ATTRACTIVE")) pictVeiculo1.Image = centralUnidas.Properties.Resources.FiatPalioAttractive;
            if (strTipo.Contains("ADVENTURE")) pictVeiculo1.Image = centralUnidas.Properties.Resources.FiatStradaAdventure2014;
            if (strTipo.Contains("STRADA")) pictVeiculo1.Image = centralUnidas.Properties.Resources.FiatStradaAdventure2014;
            if (strTipo.Contains("PUNTO")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Puntomerged;
            if (strTipo.Contains("WORKING")) pictVeiculo1.Image = centralUnidas.Properties.Resources.FiatStradaWorking2014DIR;
            if (strTipo.Contains("FIESTA")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Fiesta3_4SedanFly;
            if (strTipo.Contains("FOCUS")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Ford_Focus_Sedan2014;
            if (strTipo.Contains("SEDAN 2.0 16V Glx")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Focus3_4sedanGlx;
            if (strTipo.Contains("FUSION")) pictVeiculo1.Image = centralUnidas.Properties.Resources.FordFusion2014;
            if (strTipo.Contains("KA")) pictVeiculo1.Image = centralUnidas.Properties.Resources.FORDKA3_42014;
            if (strTipo.Contains("RANGER")) pictVeiculo1.Image = centralUnidas.Properties.Resources.FORDRANGER;
            if (strTipo.Contains("ONIX")) pictVeiculo1.Image = centralUnidas.Properties.Resources.GMBRAOnix_3_4;
            if (strTipo.Contains("PRISMA")) pictVeiculo1.Image = centralUnidas.Properties.Resources.GMBRAPrisma3_4;
            if (strTipo.Contains("CRUZE")) pictVeiculo1.Image = centralUnidas.Properties.Resources.GMCruzeSedan2014;
            if (strTipo.Contains("GOL")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Gol_G6_VW;
            if (strTipo.Contains("G6")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Gol_G6_VW;
            if (strTipo.Contains("G5")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Gol_G5_VW;
            if (strTipo.Contains("LINEA")) pictVeiculo1.Image = centralUnidas.Properties.Resources.LineaLX1_8FlexMerged;
            if (strTipo.Contains("MARCH")) pictVeiculo1.Image = centralUnidas.Properties.Resources.March_SL_Prata;
            if (strTipo.Contains("TRITON")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Mitsubishil200_triton;
            if (strTipo.Contains("FRONTIER")) pictVeiculo1.Image = centralUnidas.Properties.Resources.NissanFrontier;
            if (strTipo.Contains("LIVINA")) pictVeiculo1.Image = centralUnidas.Properties.Resources.NissanLivina1;
            if (strTipo.Contains("GRAND LIVINA")) pictVeiculo1.Image = centralUnidas.Properties.Resources.NissanGrandLivina;
            if (strTipo.Contains("SENTRA")) pictVeiculo1.Image = centralUnidas.Properties.Resources.NissanSentra;
            if (strTipo.Contains("408")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Peugeot408;
            if (strTipo.Contains("POLO")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Polo3_4hatch;
            if (strTipo.Contains("KANGOO")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Renault_KangooVU_ComPorta;
            if (strTipo.Contains("LOGAN")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Renault_Logan_2014;
            if (strTipo.Contains("SANDERO")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Renault_Sandero_2015;
            if (strTipo.Contains("FLUENCE")) pictVeiculo1.Image = centralUnidas.Properties.Resources.RenaultFluence;
            if (strTipo.Contains("S10")) pictVeiculo1.Image = centralUnidas.Properties.Resources.S103_4;
            if (strTipo.Contains("SPIN")) pictVeiculo1.Image = centralUnidas.Properties.Resources.SpinGM;
            if (strTipo.Contains("CAMRY")) pictVeiculo1.Image = centralUnidas.Properties.Resources.ToyotaCAMRY2015;
            if (strTipo.Contains("VECTRA")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Vectamerged;
            if (strTipo.Contains("VERSA")) pictVeiculo1.Image = centralUnidas.Properties.Resources.Versa_Prata_SL;
            if (strTipo.Contains("AMAROK")) pictVeiculo1.Image = centralUnidas.Properties.Resources.VWAmarokTrendline;
            if (strTipo.Contains("FOX")) pictVeiculo1.Image = centralUnidas.Properties.Resources.VWFoxPrime_4P;
            if (strTipo.Contains("JETTA")) pictVeiculo1.Image = centralUnidas.Properties.Resources.VWJetta;
            if (strTipo.Contains("KOMBI")) pictVeiculo1.Image = centralUnidas.Properties.Resources.VWKombi;
            if (strTipo.Contains("SAVEIRO")) pictVeiculo1.Image = centralUnidas.Properties.Resources.VW_Saveiro;
            if (strTipo.Contains("UP")) pictVeiculo1.Image = centralUnidas.Properties.Resources.VWUP;
            if (strTipo.Contains("VOYAGE")) pictVeiculo1.Image = centralUnidas.Properties.Resources.VWVOYAGE_PRATA;
            if (strTipo.Contains("COBALT")) pictVeiculo1.Image = centralUnidas.Properties.Resources.GM_Cobalt_LTZ;

            pictVeiculo1.Refresh();

            AtualizarInterfaceParaIdiomaCorrente();

            this.Refresh();

            Globals.glbDebugMode = Convert.ToInt16(ConfigurationManager.AppSettings.Get("appDebugMode"));
            Shutdown_Enable();

            int iRetServico = ConsultaPlaca();
            Application.DoEvents();
            switch (iRetServico)
            {
                // sucesso na obtenção das informações segue o fluxo
                case 0:
                    break;
                // Erro tipo 0201
                case 1:
                    timerDisabled();
                    Globals.glbMensagem = 0201;
                    frmMensagem frmMsg0201 = new frmMensagem();
                    frmMsg0201.Show();
                    this.Close();
                    this.Dispose();
                    break;
                // Erro tipo 0202
                case 2:
                    timerDisabled();
                    Globals.glbMensagem = 0202;
                    frmMensagem frmMsg0202 = new frmMensagem();
                    frmMsg0202.Show();
                    this.Close();
                    this.Dispose();
                    break;
                // Erro tipo 0203
                case 3:
                    timerDisabled();
                    Globals.glbMensagem = 0203;
                    frmMensagem frmMsg0203 = new frmMensagem();
                    frmMsg0203.Show();
                    this.Close();
                    this.Dispose();
                    break;
                case 4:
                    timerDisabled();
                    Globals.glbScreen = 1;
                    Globals.glbMensagem = 1010;
                    frmMensagem frmMsg1010 = new frmMensagem();
                    frmMsg1010.Show();
                    this.Close();
                    this.Dispose();
                    break;
                // Erro tipo 0202
                default:
                    timerDisabled();
                    Globals.glbMensagem = 0202;
                    frmMensagem frmMsg0202a = new frmMensagem();
                    frmMsg0202a.Show();
                    this.Close();
                    this.Dispose();
                    break;
            }
        }

        private void Shutdown_Enable()
        {
            if (Globals.glbDebugMode == 0)
                pictShutdown.Visible = false;
            else
                pictShutdown.Visible = true;
        }

        private int ConsultaPlaca()
        {
            int iRetServico = 0;
            string xmlRetornoWCF = "";
            try
            {
                CentralUnidasClassClient clientWcf = new CentralUnidasClassClient();

                string strGRUCOD = Globals.glbResGruCod.Trim();
                string strVEISIGLOC = Globals.glbPickUpLocation.Trim();
                string strMODDES = Globals.glbMODDES.Trim();
                string strTPMDES = Globals.glbTPMDES.Trim();

                xmlRetornoWCF = clientWcf.GetPlacasData(strGRUCOD, strVEISIGLOC, strMODDES, strTPMDES);
                if (string.IsNullOrEmpty(xmlRetornoWCF) || (xmlRetornoWCF.Trim() == "<UNIDASWCF />"))
                {
                    iRetServico = 4;
                }
                else
                {
                    xmlRetornoWCF = xmlRetornoWCF.Replace("\r\n", "");
                    DataSet dtSet = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(xmlRetornoWCF);
                    dtSet.ReadXml(xmlSR);
                    foreach (DataRow dbRow in dtSet.Tables["Table"].Rows)
                    {
                        ListViewItem lvi = new ListViewItem();
                        lvi.ImageIndex = 7;
                        lvi.SubItems.Add(dbRow["VEIPLA"].ToString().Trim());
                        lvi.SubItems.Add(dbRow["CORDES"].ToString().Trim());
                        lvi.SubItems.Add(String.Format("{0:###,### Km}", Convert.ToInt32(dbRow["VEIKMSATU"].ToString().Trim())));
                        lvi.SubItems.Add(dbRow["VEICOM"].ToString().Trim());
                        lstPlacas.Items.Add(lvi);
                    }
                    lstPlacas.Refresh();
                }
                clientWcf.Close();
            }
            catch (Exception ex)
            {
                iRetServico = 2;
            }

            return iRetServico;
        }

        private void pictShutdown_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(1);
        }

        private void pictBack_Click(object sender, EventArgs e)
        {
            timerDisabled();
            frmScreen05 frm5 = new frmScreen05();
            frm5.Show();
            this.Close();
            this.Dispose();
        }

        private void lstPlacas_SelectedIndexChanged(object sender, EventArgs e)
        {
            tmrReturn.Stop();
            tmrReturn.Start();

            for (int i = 0; i < lstPlacas.Items.Count; i++)
            {
                if (lstPlacas.Items[i].Selected)
                {
                    lstPlacas.Items[i].ImageIndex = 1;
                }
                else
                {
                    lstPlacas.Items[i].ImageIndex = 7;
                }
            }
        }

        private void tmrReturn_Tick(object sender, EventArgs e)
        {
            tmrReturn.Enabled = false;
            timerDisabled();
            frmScreen01 frm1 = new frmScreen01();
            frm1.Show();
            this.Close();
            this.Dispose();
        }

        private void pictDireita_Click(object sender, EventArgs e)
        {
            SendMessage(this.lstPlacas.Handle, (uint)WM_VSCROLL, (System.UIntPtr)ScrollEventType.SmallIncrement, (System.IntPtr)0);
        }

        private void pictEsquerda_Click(object sender, EventArgs e)
        {
            SendMessage(this.lstPlacas.Handle, (uint)WM_VSCROLL, (System.UIntPtr)ScrollEventType.SmallDecrement, (System.IntPtr)0);
        }

        private void butVoltar_Click(object sender, EventArgs e)
        {
            tmrReturn.Stop();
            tmrReturn.Enabled = false;
            frmScreen05 frm05 = new frmScreen05();
            frm05.ShowDialog();
            this.Close();
            this.Dispose();
        }

        private void butConfirmar_Click(object sender, EventArgs e)
        {
            tmrReturn.Stop();
            tmrReturn.Enabled = false;

            ListView.SelectedListViewItemCollection placas = this.lstPlacas.SelectedItems;
            string strPlaca = "";
            string strKM = "";
            string strCor = "";
            string strCombustivel = "";
            foreach (ListViewItem item in placas)
            {
                strPlaca = (item.SubItems[1].Text);
                strCor = (item.SubItems[2].Text);
                strKM = (item.SubItems[3].Text);
                strCombustivel = (item.SubItems[4].Text);
            }

            if (string.IsNullOrEmpty(strPlaca))
            {
                timerDisabled();
                Globals.glbScreen = 6;
                Globals.glbMensagem = 0601;
                frmMensagem frmMsg0601 = new frmMensagem();
                frmMsg0601.Show();
                this.Close();
                this.Dispose();
                return;
            }
            else
            {
                Globals.glbVEIPLA = strPlaca.Trim();
                Globals.glbCOR = strCor;
                Globals.glbKM = strKM;
                Globals.glbVEICOM = strCombustivel;

                bool bAviso = false;
                if (Globals.glbLojMunCod.Trim().ToUpper().Contains("SAO"))
                {
                    bAviso = true;
                }

                if (bAviso)
                {
                    timerDisabled();
                    frmScreen06A frm06A = new frmScreen06A();
                    frm06A.Show();
                    this.Close();
                    this.Dispose();
                }
                else
                {
                    if (Globals.glbRA_r_atipo.Trim().ToUpper() == "F")
                    {
                        timerDisabled();
                        frmScreen07 frm07 = new frmScreen07();
                        frm07.Show();
                        this.Close();
                        this.Dispose();
                    }
                    else
                    {
                        timerDisabled();
                        frmScreen08 frm08 = new frmScreen08();
                        frm08.Show();
                        this.Close();
                        this.Dispose();
                    }
                }
            }
        }

        private void AtualizarInterfaceParaIdiomaCorrente()
        {

            MontarResourceSetParaIdiomaCorrente();

            butIdioma.Text = ResxSetForm.GetString("butIdioma.Text");
            butVoltar.Text = ResxSetForm.GetString("butVoltar.Text");
            butConfirmar.Text = ResxSetForm.GetString("butConfirmar.Text");
            lblSelecioneAbaixoOpcao.Text = ResxSetForm.GetString("lblSelecioneAbaixoOpcao.Text");
            lblUseAsSetas.Text = ResxSetForm.GetString("lblUseAsSetas.Text");
            lblUseAsSetas.Text = ResxSetForm.GetString("lblUseAsSetas.Text");
            lblModelo01.Text = ResxSetForm.GetString("lblModelo01.Text") + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Globals.glbGRUCOD.Trim().ToLower()) +
                                        " - " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Globals.glbGRUDES.Trim().ToLower());

            //this.Icon = ((System.Drawing.Icon)(ResxSetForm.GetObject("$this.Icon")));
            lstPlacas.Columns[1].Text = ResxSetForm.GetString("colVEIPLA.Text");
            lstPlacas.Columns[2].Text = ResxSetForm.GetString("colCORDES.Text");
            lstPlacas.Columns[3].Text = ResxSetForm.GetString("colVEIKMSATU.Text");

            if (Globals.glbLanguage == 1) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_Brasil_botao;
            if (Globals.glbLanguage == 2) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_Espanha_botao;
            if (Globals.glbLanguage == 3) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_UK_botao;

            butIdioma.Refresh();
        }

        private void MontarResourceSetParaIdiomaCorrente()
        {
            string srtCaminhoExeTotem = System.IO.Path.GetDirectoryName(Application.ExecutablePath.ToString());
            // Português
            if (Globals.glbLanguage == 1)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen06.pt.resx");
            }
            //Espanhol
            else if (Globals.glbLanguage == 2)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen06.es.resx");
            }
            // Inglês
            else if (Globals.glbLanguage == 3)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen06.en.resx");
            }
        }

        private void butIdioma_Click(object sender, EventArgs e)
        {
            frmIdioma frmIdi = new frmIdioma();
            frmIdi.ShowDialog();
            AtualizarInterfaceParaIdiomaCorrente();
        }

        private void timerDisabled()
        {
            tmrReturn.Enabled = false;
        }
    }
}
