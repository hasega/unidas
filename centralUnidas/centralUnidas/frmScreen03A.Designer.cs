﻿namespace centralUnidas
{
    partial class frmScreen03A
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmScreen03A));
            this.btnVoltar = new System.Windows.Forms.Button();
            this.pictLinha = new System.Windows.Forms.PictureBox();
            this.pictLang = new System.Windows.Forms.PictureBox();
            this.pictUnidasLogo = new System.Windows.Forms.PictureBox();
            this.tmrReturn = new System.Windows.Forms.Timer(this.components);
            this.butIdioma = new System.Windows.Forms.Button();
            this.butVoltar = new System.Windows.Forms.Button();
            this.lblTenteNovamente = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictLinha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictLang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // btnVoltar
            // 
            this.btnVoltar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVoltar.Image = global::centralUnidas.Properties.Resources.totem_voltar1;
            this.btnVoltar.Location = new System.Drawing.Point(720, 647);
            this.btnVoltar.Name = "btnVoltar";
            this.btnVoltar.Size = new System.Drawing.Size(252, 72);
            this.btnVoltar.TabIndex = 90;
            this.btnVoltar.UseVisualStyleBackColor = true;
            this.btnVoltar.Click += new System.EventHandler(this.btnVoltar_Click);
            // 
            // pictLinha
            // 
            this.pictLinha.Image = global::centralUnidas.Properties.Resources.Totem_Traco;
            this.pictLinha.Location = new System.Drawing.Point(230, 432);
            this.pictLinha.Name = "pictLinha";
            this.pictLinha.Size = new System.Drawing.Size(1005, 9);
            this.pictLinha.TabIndex = 91;
            this.pictLinha.TabStop = false;
            // 
            // pictLang
            // 
            this.pictLang.Image = ((System.Drawing.Image)(resources.GetObject("pictLang.Image")));
            this.pictLang.Location = new System.Drawing.Point(350, 34);
            this.pictLang.Name = "pictLang";
            this.pictLang.Size = new System.Drawing.Size(148, 61);
            this.pictLang.TabIndex = 94;
            this.pictLang.TabStop = false;
            this.pictLang.Click += new System.EventHandler(this.pictLang_Click);
            // 
            // pictUnidasLogo
            // 
            this.pictUnidasLogo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictUnidasLogo.BackColor = System.Drawing.Color.Transparent;
            this.pictUnidasLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictUnidasLogo.Image = global::centralUnidas.Properties.Resources.totem_header;
            this.pictUnidasLogo.Location = new System.Drawing.Point(0, 0);
            this.pictUnidasLogo.Name = "pictUnidasLogo";
            this.pictUnidasLogo.Size = new System.Drawing.Size(1366, 140);
            this.pictUnidasLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictUnidasLogo.TabIndex = 93;
            this.pictUnidasLogo.TabStop = false;
            // 
            // tmrReturn
            // 
            this.tmrReturn.Enabled = true;
            this.tmrReturn.Interval = 1500;
            this.tmrReturn.Tick += new System.EventHandler(this.tmrReturn_Tick);
            // 
            // butIdioma
            // 
            this.butIdioma.FlatAppearance.BorderSize = 0;
            this.butIdioma.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.butIdioma.ForeColor = System.Drawing.Color.Gray;
            this.butIdioma.Image = global::centralUnidas.Properties.Resources.Totem_Flag_Brasil_botao;
            this.butIdioma.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butIdioma.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.butIdioma.Location = new System.Drawing.Point(1148, 0);
            this.butIdioma.Name = "butIdioma";
            this.butIdioma.Size = new System.Drawing.Size(148, 61);
            this.butIdioma.TabIndex = 164;
            this.butIdioma.Text = "PORTUGUÊSs";
            this.butIdioma.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butIdioma.UseCompatibleTextRendering = true;
            this.butIdioma.UseMnemonic = false;
            this.butIdioma.UseVisualStyleBackColor = true;
            this.butIdioma.Click += new System.EventHandler(this.butIdioma_Click);
            // 
            // butVoltar
            // 
            this.butVoltar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.butVoltar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.butVoltar.Font = new System.Drawing.Font("Arial", 20F, System.Drawing.FontStyle.Bold);
            this.butVoltar.ForeColor = System.Drawing.Color.White;
            this.butVoltar.Location = new System.Drawing.Point(350, 604);
            this.butVoltar.Name = "butVoltar";
            this.butVoltar.Size = new System.Drawing.Size(251, 72);
            this.butVoltar.TabIndex = 168;
            this.butVoltar.Text = "Voltar";
            this.butVoltar.UseVisualStyleBackColor = false;
            this.butVoltar.Click += new System.EventHandler(this.butVoltar_Click);
            // 
            // lblTenteNovamente
            // 
            this.lblTenteNovamente.AutoSize = true;
            this.lblTenteNovamente.Font = new System.Drawing.Font("Arial", 35F, System.Drawing.FontStyle.Bold);
            this.lblTenteNovamente.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblTenteNovamente.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblTenteNovamente.Location = new System.Drawing.Point(569, 320);
            this.lblTenteNovamente.Name = "lblTenteNovamente";
            this.lblTenteNovamente.Size = new System.Drawing.Size(411, 110);
            this.lblTenteNovamente.TabIndex = 169;
            this.lblTenteNovamente.Text = "Por favor, \r\ntente novamente.";
            this.lblTenteNovamente.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmScreen03A
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1366, 768);
            this.ControlBox = false;
            this.Controls.Add(this.lblTenteNovamente);
            this.Controls.Add(this.butVoltar);
            this.Controls.Add(this.butIdioma);
            this.Controls.Add(this.pictLang);
            this.Controls.Add(this.pictUnidasLogo);
            this.Controls.Add(this.pictLinha);
            this.Controls.Add(this.btnVoltar);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmScreen03A";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CAU - Unidas";
            this.TopMost = false;
            this.WindowState = System.Windows.Forms.FormWindowState.Normal;
            this.Load += new System.EventHandler(this.frmScreen03A_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictLinha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictLang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnVoltar;
        private System.Windows.Forms.PictureBox pictLinha;
        private System.Windows.Forms.PictureBox pictLang;
        private System.Windows.Forms.PictureBox pictUnidasLogo;
        private System.Windows.Forms.Timer tmrReturn;
        private System.Windows.Forms.Button butIdioma;
        private System.Windows.Forms.Button butVoltar;
        private System.Windows.Forms.Label lblTenteNovamente;
    }
}