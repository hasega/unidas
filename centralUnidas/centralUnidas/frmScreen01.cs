﻿// Aplicação: Unidas Totem
// Data     : 01/10/2015
// Versão   : 1.0
// Empresa  : Unidas Rent a Car
// Equipe   : Unidas Desenvolvimento
// Histórico: 
//      01/10/2015: Inicio de desenvolvimento para acesso geral
//

using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using centralUnidas.centralUnidasWCF;
using System.Diagnostics;

namespace centralUnidas
{
    public partial class frmScreen01 : Form
    {

        [DllImport(@"C:/SitefDLL/CliSiTef32I.dll", EntryPoint = "ConfiguraIntSiTefInterativoEx", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int ConfiguraIntSiTefInterativoEx(byte[] IPSiTef, byte[] IdLoja, byte[] IdTerminal, short Reservado, byte[] ParamAdic);

        [DllImport(@"C:/SitefDLL/CliSiTef32I.dll", EntryPoint = "FinalizaTransacaoSiTefInterativo", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int FinalizaTransacaoSiTefInterativoA(int Confirma, string COOOperacao, string DataFiscalOperacao, string HoraOperacao);

        [DllImport(@"C:/SitefDLL/CliSiTef32I.dll", EntryPoint = "EscreveMensagemPermanentePinPad", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int EscreveMensagemPermanentePinPad(byte[] strPermanente);

        [DllImport(@"C:/SitefDLL/CliSiTef32I.dll", EntryPoint = "VerificaPresencaPinPad", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int VerificaPresencaPinPad();

        [DllImport(@"C:/SitefDLL/CliSiTef32I.dll", EntryPoint = "ObtemQuantidadeTransacoesPendentes", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int ObtemQuantidadeTransacoesPendentes(byte[] DataFiscal, byte[] CupomFiscal);

        public frmScreen01()
        {
            InitializeComponent();
        }

        private void resetGlobals()
        {
            Globals.glbVezes = 0;
            Globals.glbLanguage = 1;
            Globals.glbScreen = 0;
            Globals.glbMensagem = 1;
            Globals.glbMensagemSitef = "";
            Globals.glbPopupSitef = 0;
            Globals.glbTimeoutSitef = 0;
            Globals.glbInteractSitef = 0;
            Globals.glbScreenCard = 0;
            Globals.glbCC_R_ANUM = "";
            Globals.glbCC_R_ALOJRET = "";
            Globals.glbCC_CCAITENUM = "";
            Globals.glbCC_CupomFiscal = "";
            Globals.glbCC_ValidadeCupomFiscal = "";
            Globals.glbCC_CodigoPreAutorizacao = "";
            Globals.glbCC_CodigoAutorizacaoEmissor = "";
            Globals.glbCC_CartaoInformado = "";
            Globals.glbCC_ValidadeInformado = "";
            Globals.glbCC_SegurancaInformado = "";
            Globals.glbSEG_R_ANUM = "";
            Globals.glbSEG_R_ALOJRET = "";
            Globals.glbSEG_PAGITENUM = "";
            Globals.glbSEG_HASH = "";
            Globals.glbSEG_TP_INFO = "";
            Globals.glbSEG_TP_OPER = "";
            Globals.glbSEG_HASH_OUT = "";
            Globals.glbSEG_RT_OPER = "";
            Globals.glbSEG_Cpf = "";
            Globals.glbSEG_CrtNum = "";
            Globals.glbSEG_MessageText = "";
            Globals.glbSEG_HASH_NEW = "";
            Globals.glbReservaNumeroPesquisa = "";
            Globals.glbReservaNumero = "";
            Globals.glbGivenName = "";
            Globals.glbGender = "";
            Globals.glbEmail = "";
            Globals.glbVehReservation = "";
            Globals.glbVoucherIdentifier = "";
            Globals.glbVOUPRE = "";
            Globals.glbVoucherValueType = "";
            Globals.glbConfID = "";
            Globals.glbCompanyShortName = "";
            Globals.glbCompanyNameCode = "";
            Globals.glbResGruCod = "";
            Globals.glbPickUpDateTime = "";
            Globals.glbReturnDateTime = "";
            Globals.glbDistUnitName = "";
            Globals.glbPickUpLocation = "";
            Globals.glbReturnLocation = "";
            Globals.glbPassengerQuantity = "";
            Globals.glbBaggageQuantity = "";
            Globals.glbVehicleCode = "";
            Globals.glbVehicleCodeContext = "";
            Globals.glbVehicleMakeModel = "";
            Globals.glbPictureURL = "";
            Globals.glbVehicleChargeCurrencyCode = "";
            Globals.glbVehicleChargeAmount = "";
            Globals.glbVehicleChargeTaxInclusive = "";
            Globals.glbVehicleChargeGuaranteedInd = "";
            Globals.glbVehicleChargePurpose = "";
            Globals.glbCOR = "";
            Globals.glbKM = "";
            Globals.glbVEICOM = "";
            Globals.glbAcordo = "";
            Globals.glbAcordoSenha = "";
            Globals.glbACOCOD = "";
            Globals.glbACOSEN = "";
            Globals.glbACOWBS = "";
            Globals.glbACWTAG = "";
            Globals.glbSTASIG = "";
            Globals.glbMOTDES = "";
            Globals.glbPRASIG = "";
            Globals.glbLOJNOM = "";
            Globals.glbLOJDES = "";
            Globals.glbLOJNOMDEV = "";
            Globals.glbLOJDESDEV = "";
            Globals.glbLOJMAPBLQ = "";
            Globals.glbLOJMAPPER = "";
            Globals.glbGRUCOD = "";
            Globals.glbGRUSIPP = "";
            Globals.glbGRUDES = "";
            Globals.glbGRUURL = "";
            Globals.glbFRTOPE = "";
            Globals.glbTOTMAPA = "";
            Globals.glbTOTOCUP = "";
            Globals.glbDISPPERC = "";
            Globals.glbDISPPERC_DSC = "";
            Globals.glbDISPPERC_AGV = "";
            Globals.glbDISPONIB1 = "";
            Globals.glbDISPONIB2 = "";
            Globals.glbSTATUS = "";
            Globals.glbAGRAVO = "";
            Globals.glbMODDES = "";
            Globals.glbTPMDES = "";
            Globals.glbCOMDES = "";
            Globals.glbVEIPLA = "";
            Globals.glbCATSRVCOD = "";
            Globals.glbCATSRVDSC = "";
            Globals.glbCATSRVVAL = "";
            Globals.glbRESOPE = "";
            Globals.glbLOJTEL = "";
            Globals.glbLOJCODSITE = "";
            Globals.glbSTIP = "";
            Globals.glbSTPORTA = "";
            Globals.glbRESDOCCLI = "";
            Globals.glbRESNOM = "";
            Globals.glbCPFCARHAB = "";
            Globals.glbCPFEXPHAB = "";
            Globals.glbR_ANUMRES = "";
            Globals.glbResumoDescontoPercentage = "";
            Globals.glbResumoDescontoTotal = "";
            Globals.glbResumoTotalGeral = "";
            Globals.glbProtecoesVar1 = "";
            Globals.glbProtecoesVar2 = "";
            Globals.glbProtecoesVar3 = "";
            Globals.glbProtecoesVar4 = "";
            Globals.glbProtecoesVar5 = "";
            Globals.glbProtecoesVar6 = "";
            Globals.glbProtecoesCodVar = "";
            Globals.glbProtecoesCodVar1 = "";
            Globals.glbProtecoesCodVar2 = "";
            Globals.glbProtecoesCodVar3 = "";
            Globals.glbProtecoesCodVar4 = "";
            Globals.glbProtecoesCodVar5 = "";
            Globals.glbProtecoesCodVar6 = "";
            Globals.glbProtecoesValorVar1 = "";
            Globals.glbProtecoesValorVar2 = "";
            Globals.glbProtecoesValorVar3 = "";
            Globals.glbProtecoesValorVar4 = "";
            Globals.glbProtecoesValorVar5 = "";
            Globals.glbProtecoesValorVar6 = "";
            Globals.glbProtecoesValorPre1 = "";
            Globals.glbProtecoesValorPre2 = "";
            Globals.glbProtecoesValorPre3 = "";
            Globals.glbProtecoesValorPre4 = "";
            Globals.glbProtecoesValorPre5 = "";
            Globals.glbProtecoesValorPre6 = "";
            Globals.glbProtecoesValorPar1 = "";
            Globals.glbProtecoesQtdUnit1 = "";
            Globals.glbProtecoesQtdUnit2 = "";
            Globals.glbProtecoesQtdUnit3 = "";
            Globals.glbProtecoesQtdUnit4 = "";
            Globals.glbProtecoesQtdUnit5 = "";
            Globals.glbProtecoesQtdUnit6 = "";
            Globals.glbBandeiraCartao = "";
            Globals.glbAcessoriosVar1 = "";
            Globals.glbAcessoriosVar2 = "";
            Globals.glbAcessoriosVar3 = "";
            Globals.glbAcessoriosVar4 = "";
            Globals.glbAcessoriosVar5 = "";
            Globals.glbAcessoriosVar6 = "";
            Globals.glbAcessoriosVar7 = "";
            Globals.glbAcessoriosVar8 = "";
            Globals.glbAcessoriosVar9 = "";
            Globals.glbAcessoriosCodVar1 = "";
            Globals.glbAcessoriosCodVar2 = "";
            Globals.glbAcessoriosCodVar3 = "";
            Globals.glbAcessoriosCodVar4 = "";
            Globals.glbAcessoriosCodVar5 = "";
            Globals.glbAcessoriosCodVar6 = "";
            Globals.glbAcessoriosCodVar7 = "";
            Globals.glbAcessoriosCodVar8 = "";
            Globals.glbAcessoriosCodVar9 = "";
            Globals.glbAcessoriosValorVar1 = "";
            Globals.glbAcessoriosValorVar2 = "";
            Globals.glbAcessoriosValorVar3 = "";
            Globals.glbAcessoriosValorVar4 = "";
            Globals.glbAcessoriosValorVar5 = "";
            Globals.glbAcessoriosValorVar6 = "";
            Globals.glbAcessoriosValorVar7 = "";
            Globals.glbAcessoriosValorVar8 = "";
            Globals.glbAcessoriosValorVar9 = "";
            Globals.glbServicosCodVar1 = "";
            Globals.glbServicosVar1 = "";
            Globals.glbRetornoCodVar1 = "";
            Globals.glbRetornoVar1 = "";
            Globals.glbRTBDIARIA = "";
            Globals.glbRTBDIAEXT = "";
            Globals.glbRTBHOREXT = "";
            Globals.glbRTBTOTDIA = "";
            Globals.glbRTBTOTDIE = "";
            Globals.glbRTBTOTHRE = "";
            Globals.glbRTBTOTDSC = "";
            Globals.glbRTBSUBTOT = "";
            Globals.glbRTBTOTRES = "";
            Globals.glbRTBTOTRES_ANT = "";
            Globals.glbRTBVALDIA = "";
            Globals.glbRTBVALFRQ = "";
            Globals.glbRTBPERTXS = "";
            Globals.glbRTBVALTXS = "";
            Globals.glbRTBPERTAG = "";
            Globals.glbRTBVALTAG = "";
            Globals.glbResumoHoraExtraQtd = "";
            Globals.glbResumoHoraExtraVar = "";
            Globals.glbResumoDiariasQtd = "";
            Globals.glbResumoDiariasUnit = "";
            Globals.glbResumoDiariasVar = "";
            Globals.glbResumoDescontoQtd = "";
            Globals.glbResumoDescontoVar = "";
            Globals.glbResumoProtecoesVar1 = "";
            Globals.glbResumoProtecoesVar2 = "";
            Globals.glbResumoProtecoesVar3 = "";
            Globals.glbResumoProtecoesQtd1 = "";
            Globals.glbResumoProtecoesQtd2 = "";
            Globals.glbResumoProtecoesQtd3 = "";
            Globals.glbResumoProtecoesUnit1 = "";
            Globals.glbResumoProtecoesUnit2 = "";
            Globals.glbResumoProtecoesUnit3 = "";
            Globals.glbResumoProtecoesTot1 = "";
            Globals.glbResumoProtecoesTot2 = "";
            Globals.glbResumoProtecoesTot3 = "";
            Globals.glbResumoAcessoriosVar1 = "";
            Globals.glbResumoAcessoriosVar2 = "";
            Globals.glbResumoAcessoriosVar3 = "";
            Globals.glbResumoAcessoriosQtd1 = "";
            Globals.glbResumoAcessoriosQtd2 = "";
            Globals.glbResumoAcessoriosQtd3 = "";
            Globals.glbResumoAcessoriosUnit1 = "";
            Globals.glbResumoAcessoriosUnit2 = "";
            Globals.glbResumoAcessoriosUnit3 = "";
            Globals.glbResumoAcessoriosTot1 = "";
            Globals.glbResumoAcessoriosTot2 = "";
            Globals.glbResumoAcessoriosTot3 = "";
            Globals.glbResumoAdministrativaUnit = "";
            Globals.glbResumoAdministrativaTot = "";
            Globals.glbCartao = "";
            Globals.glbDebugMode = 0;
            Globals.glbCupomFiscal1 = "";
            Globals.glbCupomFiscal2 = "";
            Globals.glbRA_Contrato = "";
            Globals.glbPRECPF = "";
            Globals.glbPRENOM = "";
            Globals.glbRA_clifat = "";
            Globals.glbRA_clidoc = "";
            Globals.glbRA_clinom = "";
            Globals.glbRA_clinomfat = "";
            Globals.glbRA_prfcod = "";
            Globals.glbRA_tclcod = "";
            Globals.glbRA_r_aresloj = "";
            Globals.glbRA_r_alojret = "";
            Globals.glbRA_r_adatret = "";
            Globals.glbRA_r_ahorret = "";
            Globals.glbRA_r_adatdev = "";
            Globals.glbRA_r_ahordev = "";
            Globals.glbRA_grucod = "";
            Globals.glbRA_r_alojdev = "";
            Globals.glbRA_tptconcod = "";
            Globals.glbRA_tardes = "";
            Globals.glbRA_trfcod = "";
            Globals.glbRA_acocod = "";
            Globals.glbRA_acodig = "";
            Globals.glbRA_R_aCliSeg = "";
            Globals.glbRA_R_aCanVen = "";
            Globals.glbRA_restrfnew = "";
            Globals.glbRA_r_aprot = "";
            Globals.glbRA_r_aprotpot = "";
            Globals.glbRA_r_aprtpa = "";
            Globals.glbRA_r_adsc = "";
            Globals.glbRA_clitarcod = "";
            Globals.glbRA_clitarseq = "";
            Globals.glbRA_rescodsml = "";
            Globals.glbRA_restipsml = "";
            Globals.glbRA_ResTVch = "";
            Globals.glbRA_ResExpCli = "";
            Globals.glbRA_respaxcpf = "";
            Globals.glbRA_respax = "";
            Globals.glbRA_resgrupro = "";
            Globals.glbRA_resmotadi = "";
            Globals.glbRA_rescodtam = "";
            Globals.glbRA_rescupcod = "";
            Globals.glbRA_resdiaaut = "";
            Globals.glbRA_ResTpvCh = "";
            Globals.glbRA_respnr = "";
            Globals.glbRA_r_atipo = "";
            Globals.glbRESPESCPF = "";
            Globals.glbCLI_CLIDOC = "";
            Globals.glbCLIDOC_EXIST = "";
            Globals.glbCLI_CLINOM = "";
            Globals.glbCLI_CLINOM_SAUDACOES = "";
            Globals.glbCLI_CPFENDRES = "";
            Globals.glbCLI_CPFCIDRES = "";
            Globals.glbCLI_CPFESTRES = "";
            Globals.glbCLI_CPFCEPRES = "";
            Globals.glbCLI_CPFTELRES = "";
            Globals.glbCLI_CPFCPF = "";
            Globals.glbCLI_CPFRG = "";
            Globals.glbCLI_CPFCARHAB = "";
            Globals.glbCLI_CPFESTHAB = "";
            Globals.glbCLI_CPFEXPHAB = "";
            Globals.glbCLI_CLIPRMHAB = "";
            Globals.glbCLI_TCLCOD = "";
            Globals.glbCLI_PRFTIP = "";
            Globals.glbCLI_PRFCOD = "";
            Globals.glbCLI_CPFPAS = "";
            Globals.glbCLI_CPFSEX = "";
            Globals.glbCLI_CPFDATNAS = "";
            Globals.glbCLI_CLICARDSC = "";
            Globals.glbCLI_PRASIG = "";
            Globals.glbCLI_CLICATEGO = "";
            Globals.glbCLI_CLIEMAIL = "";
            Globals.glbCLI_CRECOD = "";
            Globals.glbCLI_R_ANUM = "";
            Globals.glbCLI_R_ATIPO = "";
            Globals.glbVOU_RESNUM = "";
            Globals.glbVOU_RESLOJOPE = "";
            Globals.glbVOU_RESNVCH = "";
            Globals.glbVOU_RESVDIA = "";
            Globals.glbVOU_RESVDIAE = "";
            Globals.glbVOU_RESVHORE = "";
            Globals.glbVOU_RESVKM = "";
            Globals.glbVOU_RESVTAXS = "";
            Globals.glbVOU_RESVPROT = "";
            Globals.glbVOU_RESVMOT = "";
            Globals.glbVOU_RESVAPRI = "";
            Globals.glbVOU_RESVCMB = "";
            Globals.glbVOU_RESVAVA = "";
            Globals.glbVOU_RESVPRTO = "";
            Globals.glbVOU_RESVADC = "";
            Globals.glbVOU_RESVMOTA = "";
            Globals.glbVOU_RESVTAXR = "";
            Globals.glbVOU_RESVDSP = "";
            Globals.glbVOU_RESMUL = "";
            Globals.glbVOU_RESNVCLI = "";
            Globals.glbLojMunCod = "";
            Globals.glbDATA_CAP = "";
        }

        private void frmScreen01_Load(object sender, EventArgs e)
        {

            resetGlobals();

            Globals.glbScreen = 1;
            Globals.glbDebugMode = Convert.ToInt16(ConfigurationManager.AppSettings.Get("appDebugMode"));
            Shutdown_Enable();

            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;
            lblVersion.Text = String.Format("Versao: {0}", version);
            Globals.glbApplicationVersion = version;

            int nPendentes = 0;
            short result = 0;

            string appPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string configFile = System.IO.Path.Combine(appPath, "CentralUnidas.exe.config");
            ExeConfigurationFileMap configFileMap = new ExeConfigurationFileMap();
            configFileMap.ExeConfigFilename = configFile;
            System.Configuration.Configuration config = ConfigurationManager.OpenMappedExeConfiguration(configFileMap, ConfigurationUserLevel.None);

            string strCupom = config.AppSettings.Settings["totemCupom"].Value;
            string strCupomData = config.AppSettings.Settings["totemCupomData"].Value;
            string strCupomHora = config.AppSettings.Settings["totemCupomHora"].Value;

            string strLoja = ConfigurationManager.AppSettings["totemLoja"].ToString();
            string strTerminal = ConfigurationManager.AppSettings["totemTerminal"].ToString();

            // INICIO OBTEM LOJA
            string strHostName = System.Environment.MachineName.Trim();
            string lojaUnidas = "";
            string lojaSitefUnidas = "";
            Globals.glbLojaTotem = "";

            try
            {

                CentralUnidasClassClient clientWcf = new CentralUnidasClassClient();

                string xmlRetornoWCF = "";
                xmlRetornoWCF = clientWcf.GetHostnameLojas(strHostName);

                if (clientWcf != null) clientWcf.Close();
                if (!string.IsNullOrEmpty(xmlRetornoWCF) && (xmlRetornoWCF.Trim() != "<UNIDASWCF />"))
                {
                    xmlRetornoWCF = xmlRetornoWCF.Replace("\r\n", "");
                    DataSet dtSet = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(xmlRetornoWCF);
                    dtSet.ReadXml(xmlSR);

                    int nRegistros = dtSet.Tables["Table"].Rows.Count;
                    foreach (DataRow dbRow in dtSet.Tables["Table"].Rows)
                    {
                        lojaUnidas = dbRow["PRASIG"].ToString().Trim();
                        Globals.glbPortaPinPad = dbRow["PORTPINPAD"].ToString().Trim();
                        Globals.glbLojMunCod = dbRow["MUNCOD"].ToString().Trim();
                        lojaSitefUnidas = dbRow["LOJCODSITE"].ToString().Trim();
                        Globals.glbLojaTotem = lojaUnidas.Trim();
                        lblLoja.Text = "Loja: " + Globals.glbLojaTotem.Trim();
                    }

                    LogTotem.InsertLog(new LogOperacao()
                    {
                        Application_Name = Globals.glbApplicationName,
                        Application_Version = Globals.glbApplicationVersion,
                        Local = Globals.glbLojaTotem,
                        Operation_Ticks =  DateTime.Now.Ticks,
                        Operation_Name = "Inicialização Aplicativo Totem",
                        Form = "FrmScreen01"
                       
                        
                    } );
                }
            }
            catch (Exception ex)
            {
             
                Globals.glbMensagem = 1019;
                frmMensagem frmMsg = new frmMensagem();
                frmMsg.Show();
                this.Close();
                this.Dispose();
                System.Threading.Thread.Sleep(6000);
            }
            /*
             
             Inicio: Rotina para exclusão de transações pendentes no Sitef
             * 
             */

            try
            {
                string strDataFiscal = strCupomData + strCupomHora;
                //192.168.51.81
                byte[] _ipSitef = Encoding.ASCII.GetBytes("localhost:4096\0");
                //lojaSitefUnidas
                byte[] _lojaSitefUnidas = Encoding.ASCII.GetBytes("0000" + "\0");
                byte[] _strTerminal = Encoding.ASCII.GetBytes(strTerminal + "\0");

                // Obtem numero da porta automaticamente. Caso não encontre assume conteudo da tabela.
                int nPorta = retornaPortaPinPad(_ipSitef, _lojaSitefUnidas, _strTerminal);
                if (nPorta > 0)
                {
                    Globals.glbPortaPinPad = nPorta.ToString("D2");
                }
                else
                {
                    nPorta = Convert.ToInt32(Globals.glbPortaPinPad.Trim());
                    
                }

                string strPortaDinamica = string.Format("[MultiplosCupons=1;PortaPinPad={0:D2};]", nPorta);
                byte[] _strPortaDinamica = Encoding.ASCII.GetBytes(strPortaDinamica + "\0");

                int retorno = ConfiguraIntSiTefInterativoEx(_ipSitef, _lojaSitefUnidas, _strTerminal, result, _strPortaDinamica);

                if (retorno == 0)
                {
                    string strMensagemPinPad = ". UNIDAS TOTEM .|";
                    byte[] _strPermanente = Encoding.ASCII.GetBytes(strMensagemPinPad + "\0");

                    int nPerm = EscreveMensagemPermanentePinPad(_strPermanente);

                    byte[] _strCupomData = Encoding.ASCII.GetBytes(strCupomData + "\0");
                    byte[] _strCupom = Encoding.ASCII.GetBytes(strCupom + "\0");

                    nPendentes = ObtemQuantidadeTransacoesPendentes(_strCupomData, _strCupom);
                    if (nPendentes > 0)
                    {
                        int nTransacaoConfirma = 0;
                        int nFinal = FinalizaTransacaoSiTefInterativoA(nTransacaoConfirma, strCupom, strCupomData, strCupomHora);
                        Globals.glbScreen = 1;
                        Globals.glbMensagem = 1002;
                        frmMensagem frmMsg0201 = new frmMensagem();
                        frmMsg0201.Show();
                        this.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                Globals.glbMensagem = 1020;
                frmMensagem frmMsg = new frmMensagem();
                frmMsg.Show();
                this.Close();
                this.Dispose();
                System.Threading.Thread.Sleep(6000);
            }

            /*
             
            Fim: Rotina para exclusão de transações pendentes no Sitef
            * 
            */

        }

        private void Shutdown_Enable()
        {
            if (Globals.glbDebugMode == 0)
                pictShutdown.Visible = false;
            else
                pictShutdown.Visible = true;
        }

        private void pictShutdown_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(1);
        }

        private void frmScreen01_Click(object sender, EventArgs e)
        {
            Globals.glbLanguage = 1;
            frmScreen02 frm = new frmScreen02();
            frm.Show();
            this.Close();
            this.Dispose();
        }

        private int retornaPortaPinPad(byte[] _ipSitef, byte[] _lojaSitefUnidas, byte[] _strTerminal)
        {
            short result = 0;
            string strNumeroPorta;
            string strNumeroPortaReturn = "";
            try
            {
                string[] theSerialPortNames = System.IO.Ports.SerialPort.GetPortNames();
                foreach (string item in theSerialPortNames)
                {
                    strNumeroPorta = item.Substring(3);
                    if (strNumeroPorta.Length == 1)
                        strNumeroPorta = "0" + strNumeroPorta;

                    string strPortaDinamica = string.Format("[MultiplosCupons=1;PortaPinPad={0:D2};]", Convert.ToInt32(strNumeroPorta));
                    byte[] _strPortaDinamica = Encoding.ASCII.GetBytes(strPortaDinamica + "\0");

                    int retorno = ConfiguraIntSiTefInterativoEx(_ipSitef, _lojaSitefUnidas, _strTerminal, result, _strPortaDinamica);
                    if (retorno == 0)
                    {
                        if (VerificaPresencaPinPad() == 1)
                        {
                            strNumeroPortaReturn = item.Replace("COM", "");
                            break;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            int numPortaReturn = 0;
            bool canConvert = int.TryParse(strNumeroPortaReturn, out numPortaReturn);
            if (canConvert)
                return numPortaReturn;
            else
                return 0;
        }
       
    }
}
