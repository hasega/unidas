﻿namespace centralUnidas
{
    partial class frmScreen14
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmScreen14));
            this.pictLang = new System.Windows.Forms.PictureBox();
            this.pictUnidasLogo = new System.Windows.Forms.PictureBox();
            this.lblValidadeCartao = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblInformeValidade = new System.Windows.Forms.Label();
            this.comboMes = new System.Windows.Forms.ComboBox();
            this.lblMes = new System.Windows.Forms.Label();
            this.lblAno = new System.Windows.Forms.Label();
            this.comboAno = new System.Windows.Forms.ComboBox();
            this.tmrReturn = new System.Windows.Forms.Timer(this.components);
            this.pictShutdown = new System.Windows.Forms.PictureBox();
            this.butContinuar = new System.Windows.Forms.Button();
            this.butVoltar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictLang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictShutdown)).BeginInit();
            this.SuspendLayout();
            // 
            // pictLang
            // 
            this.pictLang.Image = ((System.Drawing.Image)(resources.GetObject("pictLang.Image")));
            this.pictLang.Location = new System.Drawing.Point(1148, 0);
            this.pictLang.Name = "pictLang";
            this.pictLang.Size = new System.Drawing.Size(148, 61);
            this.pictLang.TabIndex = 156;
            this.pictLang.TabStop = false;
            this.pictLang.Visible = false;
            // 
            // pictUnidasLogo
            // 
            this.pictUnidasLogo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictUnidasLogo.BackColor = System.Drawing.Color.Transparent;
            this.pictUnidasLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictUnidasLogo.Image = global::centralUnidas.Properties.Resources.totem_header;
            this.pictUnidasLogo.Location = new System.Drawing.Point(0, 0);
            this.pictUnidasLogo.Name = "pictUnidasLogo";
            this.pictUnidasLogo.Size = new System.Drawing.Size(1370, 140);
            this.pictUnidasLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictUnidasLogo.TabIndex = 155;
            this.pictUnidasLogo.TabStop = false;
            // 
            // lblValidadeCartao
            // 
            this.lblValidadeCartao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblValidadeCartao.BackColor = System.Drawing.Color.White;
            this.lblValidadeCartao.Font = new System.Drawing.Font("Arial", 26F, System.Drawing.FontStyle.Bold);
            this.lblValidadeCartao.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblValidadeCartao.Location = new System.Drawing.Point(0, 152);
            this.lblValidadeCartao.Name = "lblValidadeCartao";
            this.lblValidadeCartao.Size = new System.Drawing.Size(1370, 54);
            this.lblValidadeCartao.TabIndex = 177;
            this.lblValidadeCartao.Text = "Validade do Cartão";
            this.lblValidadeCartao.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblValidadeCartao.Click += new System.EventHandler(this.label3_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox2.Image = global::centralUnidas.Properties.Resources.Totem_Traco;
            this.pictureBox2.Location = new System.Drawing.Point(364, 212);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(683, 1);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 178;
            this.pictureBox2.TabStop = false;
            // 
            // lblInformeValidade
            // 
            this.lblInformeValidade.AutoSize = true;
            this.lblInformeValidade.BackColor = System.Drawing.Color.White;
            this.lblInformeValidade.Font = new System.Drawing.Font("Arial", 17F);
            this.lblInformeValidade.ForeColor = System.Drawing.Color.Gray;
            this.lblInformeValidade.Location = new System.Drawing.Point(530, 279);
            this.lblInformeValidade.Name = "lblInformeValidade";
            this.lblInformeValidade.Size = new System.Drawing.Size(305, 26);
            this.lblInformeValidade.TabIndex = 179;
            this.lblInformeValidade.Text = "Informe a validade do cartão:";
            // 
            // comboMes
            // 
            this.comboMes.Font = new System.Drawing.Font("Arial", 26F);
            this.comboMes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(109)))), ((int)(((byte)(175)))));
            this.comboMes.FormattingEnabled = true;
            this.comboMes.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.comboMes.Location = new System.Drawing.Point(535, 370);
            this.comboMes.Name = "comboMes";
            this.comboMes.Size = new System.Drawing.Size(73, 48);
            this.comboMes.TabIndex = 180;
            this.comboMes.SelectedIndexChanged += new System.EventHandler(this.comboMes_SelectedIndexChanged);
            this.comboMes.Click += new System.EventHandler(this.comboMes_Click);
            // 
            // lblMes
            // 
            this.lblMes.AutoSize = true;
            this.lblMes.BackColor = System.Drawing.Color.White;
            this.lblMes.Font = new System.Drawing.Font("Arial", 15F);
            this.lblMes.ForeColor = System.Drawing.Color.Gray;
            this.lblMes.Location = new System.Drawing.Point(533, 344);
            this.lblMes.Name = "lblMes";
            this.lblMes.Size = new System.Drawing.Size(54, 23);
            this.lblMes.TabIndex = 181;
            this.lblMes.Text = "Mês:";
            // 
            // lblAno
            // 
            this.lblAno.AutoSize = true;
            this.lblAno.BackColor = System.Drawing.Color.White;
            this.lblAno.Font = new System.Drawing.Font("Arial", 15F);
            this.lblAno.ForeColor = System.Drawing.Color.Gray;
            this.lblAno.Location = new System.Drawing.Point(614, 344);
            this.lblAno.Name = "lblAno";
            this.lblAno.Size = new System.Drawing.Size(50, 23);
            this.lblAno.TabIndex = 183;
            this.lblAno.Text = "Ano:";
            // 
            // comboAno
            // 
            this.comboAno.Font = new System.Drawing.Font("Arial", 26F);
            this.comboAno.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(109)))), ((int)(((byte)(175)))));
            this.comboAno.FormattingEnabled = true;
            this.comboAno.Items.AddRange(new object[] {
            "2015",
            "2016",
            "2017",
            "2018",
            "2019",
            "2020",
            "2021",
            "2022",
            "2023",
            "2024",
            "2025",
            "2026",
            "2027",
            "2028",
            "2029",
            "2030"});
            this.comboAno.Location = new System.Drawing.Point(614, 370);
            this.comboAno.Name = "comboAno";
            this.comboAno.Size = new System.Drawing.Size(122, 48);
            this.comboAno.TabIndex = 182;
            this.comboAno.Click += new System.EventHandler(this.comboAno_Click);
            // 
            // tmrReturn
            // 
            this.tmrReturn.Enabled = true;
            this.tmrReturn.Interval = 90000;
            this.tmrReturn.Tick += new System.EventHandler(this.tmrReturn_Tick);
            // 
            // pictShutdown
            // 
            this.pictShutdown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictShutdown.BackColor = System.Drawing.Color.Transparent;
            this.pictShutdown.Image = global::centralUnidas.Properties.Resources.shutdown_button;
            this.pictShutdown.Location = new System.Drawing.Point(12, 173);
            this.pictShutdown.Name = "pictShutdown";
            this.pictShutdown.Size = new System.Drawing.Size(50, 55);
            this.pictShutdown.TabIndex = 199;
            this.pictShutdown.TabStop = false;
            this.pictShutdown.Visible = false;
            this.pictShutdown.Click += new System.EventHandler(this.pictShutdown_Click);
            // 
            // butContinuar
            // 
            this.butContinuar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.butContinuar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.butContinuar.Font = new System.Drawing.Font("Arial", 13F, System.Drawing.FontStyle.Bold);
            this.butContinuar.ForeColor = System.Drawing.Color.White;
            this.butContinuar.Location = new System.Drawing.Point(756, 378);
            this.butContinuar.Name = "butContinuar";
            this.butContinuar.Size = new System.Drawing.Size(143, 45);
            this.butContinuar.TabIndex = 273;
            this.butContinuar.Text = "Continuar";
            this.butContinuar.UseVisualStyleBackColor = false;
            this.butContinuar.Click += new System.EventHandler(this.butContinuar_Click);
            // 
            // butVoltar
            // 
            this.butVoltar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.butVoltar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.butVoltar.Font = new System.Drawing.Font("Arial", 13F, System.Drawing.FontStyle.Bold);
            this.butVoltar.ForeColor = System.Drawing.Color.White;
            this.butVoltar.Location = new System.Drawing.Point(970, 378);
            this.butVoltar.Name = "butVoltar";
            this.butVoltar.Size = new System.Drawing.Size(147, 45);
            this.butVoltar.TabIndex = 272;
            this.butVoltar.Text = "Voltar";
            this.butVoltar.UseVisualStyleBackColor = false;
            this.butVoltar.Click += new System.EventHandler(this.butVoltar_Click);
            // 
            // frmScreen14
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1366, 768);
            this.ControlBox = false;
            this.Controls.Add(this.butContinuar);
            this.Controls.Add(this.butVoltar);
            this.Controls.Add(this.pictShutdown);
            this.Controls.Add(this.lblAno);
            this.Controls.Add(this.comboAno);
            this.Controls.Add(this.lblMes);
            this.Controls.Add(this.comboMes);
            this.Controls.Add(this.lblInformeValidade);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.lblValidadeCartao);
            this.Controls.Add(this.pictLang);
            this.Controls.Add(this.pictUnidasLogo);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmScreen14";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CAU - Unidas";
            this.TopMost = false;
            this.WindowState = System.Windows.Forms.FormWindowState.Normal;
            this.Load += new System.EventHandler(this.frmScreen14_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictLang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictShutdown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictLang;
        private System.Windows.Forms.PictureBox pictUnidasLogo;
        private System.Windows.Forms.Label lblValidadeCartao;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblInformeValidade;
        private System.Windows.Forms.ComboBox comboMes;
        private System.Windows.Forms.Label lblMes;
        private System.Windows.Forms.Label lblAno;
        private System.Windows.Forms.ComboBox comboAno;
        private System.Windows.Forms.Timer tmrReturn;
        private System.Windows.Forms.PictureBox pictShutdown;
        private System.Windows.Forms.Button butContinuar;
        private System.Windows.Forms.Button butVoltar;
    }
}