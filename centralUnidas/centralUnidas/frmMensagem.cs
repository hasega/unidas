﻿// Aplicação: Unidas Totem
// Data     : 01/10/2015
// Versão   : 1.0
// Empresa  : Unidas Rent a Car
// Equipe   : Unidas Desenvolvimento
// Histórico: 
//      01/10/2015: Inicio de desenvolvimento para acesso geral
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace centralUnidas
{
    public partial class frmMensagem : Form
    {
        public ResXResourceSet ResxSetFormMensagens;
        public ResXResourceSet ResxSetForm;
        public frmMensagem()
        {
            InitializeComponent();
            
        }

        private void frmMensagem_Load(object sender, EventArgs e)
        {
            int nScreenWidth = this.Width;
            int nScreenHeight = this.Height;
            int nScreenWidthSplit = nScreenWidth / 2;
            int nScreenHeightSplit = nScreenHeight / 2;
            
            richMensagem.Width = nScreenWidth - 200;
            richMensagem.Location = new Point((nScreenWidthSplit - (richMensagem.Size.Width / 2)), (nScreenHeightSplit - (richMensagem.Size.Height / 2)));

            this.Refresh();

            Globals.glbDebugMode = Convert.ToInt16(ConfigurationManager.AppSettings.Get("appDebugMode"));
            Shutdown_Enable();
            
            string srtCaminhoExeTotem = System.IO.Path.GetDirectoryName(Application.ExecutablePath.ToString());
            ResxSetFormMensagens = new ResXResourceSet(srtCaminhoExeTotem + "\\MensagensCentralUnidas.resx");

            AtualizarInterfaceParaIdiomaCorrente();

            callMensagem();

            tmrReturn.Enabled = true;

        }

        private void callMensagem()
        {

            /*
             Sistema ainda aguarda definição dos textos para idiomas.
             * Data: 10/2015
             * Daniela Araujo (Unidas) disse que após a definição das telas do sistema vai providenciar a tradução para aplicação
             
             * 
             * Esta previsto na versão 2, a utilização de tabela em banco de dados que versiona as mensagens por idioma/tela
             * 
             */

            int intLanguage = Globals.glbLanguage;
            int intMensagem = Globals.glbMensagem;
            switch (intMensagem)
            {
                case 0201:
                    switch (intLanguage)
                    {
                            // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0101BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0101BR");
                            break;
                            // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0101ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0101ES");

                            break;
                            // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0101EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0101EN");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0101BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0101BR");

                            break;
                    }
                    break;
                case 0202:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0201BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0201BR");

                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0201ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0201ES");

                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0201EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0201EN");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0201BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0201BR");

                            break;
                    }
                    break;
                case 0203:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0301BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0301BR");
                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0301ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0301ES");

                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0301EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0301EN");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0301BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0301BR");

                            break;
                    }
                    break;

                case 0204:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0401BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0401BR");

                            break;
                        // Espanhol
                        case 2:
                            ///richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0401ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0401ES");

                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0401EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0401EN");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0401BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0401BR");

                            break;
                    }
                    break;
                case 0205:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0501BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0501BR");

                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0501BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0501ES");

                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0501BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0501EN");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0501BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0501BR");

                            break;
                    }
                    break;
                case 0206:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0601BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0601BR");

                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0601BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0601ES");

                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0601BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0601EN");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0601BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0601BR");

                            break;
                    }
                    break;
                case 0207:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0701BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0701BR");

                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0701BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0701ES");

                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0701BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0701EN");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0701BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0701BR");

                            break;
                    }
                    break;
                case 0301:
                    tmrReturn.Stop();
                    tmrReturn.Interval = 2000;
                    tmrReturn.Start();
                    switch (intLanguage)
                    {
                            // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen03Msg0101BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen03Msg0101BR");

                            break;
                            // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen03Msg0101ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen03Msg0101ES");

                            break;
                            // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen03Msg0101EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen03Msg0101EN");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen03Msg0101BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen03Msg0101BR");

                            break;
                    }
                    break;
                case 0302:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen03Msg0201BR") + Environment.NewLine + Environment.NewLine;
                            //richMensagem.Text += ConfigurationManager.AppSettings.Get("screen03Msg0202BR") + Environment.NewLine;
                            //richMensagem.Text += ConfigurationManager.AppSettings.Get("screen03Msg0203BR");

                            richMensagem.Text = ResxSetFormMensagens.GetString("screen03Msg0201BR") + Environment.NewLine + Environment.NewLine;
                            richMensagem.Text += ResxSetFormMensagens.GetString("screen03Msg0202BR") + Environment.NewLine;
                            richMensagem.Text += ResxSetFormMensagens.GetString("screen03Msg0203BR");
                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen03Msg0201ES") + Environment.NewLine + Environment.NewLine;
                            //richMensagem.Text += ConfigurationManager.AppSettings.Get("screen03Msg0202ES") + Environment.NewLine;
                            //richMensagem.Text += ConfigurationManager.AppSettings.Get("screen03Msg0203ES");

                            richMensagem.Text = ResxSetFormMensagens.GetString("screen03Msg0201ES") + Environment.NewLine + Environment.NewLine;
                            richMensagem.Text += ResxSetFormMensagens.GetString("screen03Msg0202ES") + Environment.NewLine;
                            richMensagem.Text += ResxSetFormMensagens.GetString("screen03Msg0203ES");
                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen03Msg0201EN") + Environment.NewLine + Environment.NewLine;
                            //richMensagem.Text += ConfigurationManager.AppSettings.Get("screen03Msg0202EN") + Environment.NewLine;
                            //richMensagem.Text += ConfigurationManager.AppSettings.Get("screen03Msg0203EN");

                            richMensagem.Text = ResxSetFormMensagens.GetString("screen03Msg0201EN") + Environment.NewLine + Environment.NewLine;
                            richMensagem.Text += ResxSetFormMensagens.GetString("screen03Msg0202EN") + Environment.NewLine;
                            richMensagem.Text += ResxSetFormMensagens.GetString("screen03Msg0203EN");
                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen03Msg0201BR") + Environment.NewLine + Environment.NewLine;
                            //richMensagem.Text += ConfigurationManager.AppSettings.Get("screen03Msg0202BR") + Environment.NewLine;
                            //richMensagem.Text += ConfigurationManager.AppSettings.Get("screen03Msg0203BR");

                            richMensagem.Text = ResxSetFormMensagens.GetString("screen03Msg0201BR") + Environment.NewLine + Environment.NewLine;
                            richMensagem.Text += ResxSetFormMensagens.GetString("screen03Msg0202BR") + Environment.NewLine;
                            richMensagem.Text += ResxSetFormMensagens.GetString("screen03Msg0203BR");

                            break;
                    }
                    break;
                case 0401:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen04Msg0101BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen04Msg0101BR");
                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen04Msg0101ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen04Msg0101ES");

                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen04Msg0101EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen04Msg0101EN");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen04Msg0101BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen04Msg0101BR");

                            break;
                    }
                    break;
                case 0601:
                    switch (intLanguage)
                    {
                            // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen06Msg0101BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen06Msg0101BR");

                            break;
                            // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen06Msg0101ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen06Msg0101ES");

                            break;
                            // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen06Msg0101EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen06Msg0101EN");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen06Msg0101BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen06Msg0101BR");

                            break;
                    }
                    break;
                case 0701:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen07Msg0701BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen07Msg0701BR");

                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen07Msg0701BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen07Msg0701ES");

                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen07Msg0701BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen07Msg0701EN");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen07Msg0701BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen07Msg0701BR");

                            break;
                    }
                    break;
                case 1001:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1001BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1001BR");

                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1001ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1001ES");

                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1001EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1001EN");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1001BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1001BR");

                            break;
                    }
                    break;
                case 1002:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1002BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1002BR");

                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1002ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1002ES");

                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1002EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1002EN");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1002BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1002BR");

                            break;
                    }
                    break;
                case 1003:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1003BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1003BR");

                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1003ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1003ES");

                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1003EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1003EN");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1003BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1003BR");

                            break;
                    }
                    break;
                case 1004:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1004BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1004BR");

                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1004ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1004ES");

                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1004ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1004ES");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1004BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1004BR");

                            break;
                    }
                    break;
                case 1005:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1005BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1005BR");

                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1005ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1005ES");

                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1005EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1005EN");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1005BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1005BR");

                            break;
                    }
                    break;
                case 1006:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1006BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1006BR");

                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1006ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1006ES");

                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1006EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1006EN");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1006BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1006BR");

                            break;
                    }
                    break;
                case 1007:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1007BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1007BR");

                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1007ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1007ES");

                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1007EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1007EN");

                            break;
                        default:
                            // Porgues
                            // richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1007BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1007BR");

                            break;
                    }
                    break;
                case 1008:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1008BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1008BR");

                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1008ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1008ES");

                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1008EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1008EN");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1008BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1008BR");

                            break;
                    }
                    break;
                case 1009:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1009BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1009BR");

                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1009ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1009ES");

                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1009EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1009EN");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1009BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1009BR");

                            break;
                    }
                    break;
                case 1010:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1010BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1010BR");

                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1010ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1010ES");

                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1010EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1010EN");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1010BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1010BR");

                            break;
                    }
                    break;
                case 1011:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1011BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1011BR");

                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1011ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1011ES");

                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1011EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1011EN");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1011BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1011BR");

                            break;
                    }
                    break;
                case 1012:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1012BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1012BR");

                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1012ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1012ES");

                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1012EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1012EN");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1012BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1012BR");

                            break;
                    }
                    break;
                case 1013:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1013BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1013BR");

                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1013ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1013ES");

                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1013EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1013EN");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1013BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1013BR");

                            break;
                    }
                    break;
                case 1014:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1014BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1014BR");

                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1014ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1014ES");

                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1014EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1014EN");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1014BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1014BR");

                            break;
                    }
                    break;
                case 1015:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1015BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1015BR");

                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1015ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1015ES");

                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1015EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1015EN");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1015BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1015BR");

                            break;
                    }
                    break;
                case 1016:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1016BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1016BR");

                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1016ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1016ES");

                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1016EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1016EN");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1016BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1016BR");

                            break;
                    }
                    break;
                case 1017:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1017BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1017BR");

                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1017ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1017ES");
                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1017EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1017EN");
                            break;

                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1017BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1017BR");

                            break;
                    }
                    break;
                case 1018:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1018BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1018BR");

                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1018ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1018ES");

                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1018EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1018EN");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1018BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1018BR");
                            break;
                    }
                    break;
                case 1019:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1019BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1019BR");

                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1019ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1019ES");

                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1019EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1019EN");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1019BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1019BR");

                            break;
                    }
                    break;
                case 1020:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1020BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1020BR");

                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1020ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1020ES");

                            break;
                        // Ingles
                        case 3:
                            // richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1020EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1020EN");

                            break;
                        default:
                            // Porgues
                            // richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1020BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1020BR");

                            break;
                    }
                    break;
                case 1021:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1021BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1021BR");

                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1021ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1021ES");

                            break;
                        // Ingles
                        case 3:
                            // richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1021EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1021EN");

                            break;
                        default:
                            // Porgues
                            // richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1021BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1021BR");

                            break;
                    }
                    break;
                case 1022:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1022BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1022BR");

                            break;
                        // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1022ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1022ES");

                            break;
                        // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1022EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1022EN");

                            break;
                        default:
                            // Porgues
                            // richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1022BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen10Msg1022BR");

                            break;
                    }
                    break;
                case 10000:
                    richMensagem.Text = Globals.glbMensagemSitef;
                    break;
                default:
                    switch (intLanguage)
                    {
                            // Portugues
                        case 1:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0101BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0101BR");

                            break;
                            // Espanhol
                        case 2:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0101ES");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0101ES");

                            break;
                            // Ingles
                        case 3:
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0101EN");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0101EN");

                            break;
                        default:
                            // Porgues
                            //richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0101BR");
                            richMensagem.Text = ResxSetFormMensagens.GetString("screen02Msg0101BR");

                            break;
                    }
                    break;
            }
            richMensagem.WordWrap = true;
            richMensagem.SelectionAlignment = HorizontalAlignment.Center;
            richMensagem.Refresh();
            Globals.glbMensagem = 0;
            Globals.glbMensagemSitef = "";
            ResxSetFormMensagens.Close();
            ResxSetFormMensagens = null;

            LogTotem.InsertLog(new LogOperacao()
            {
                Application_Name = Globals.glbApplicationName,
                Application_Version = Globals.glbApplicationVersion,
                Local = Globals.glbLojaTotem,
                Operation_Ticks = Globals.glbOperationTicks,
                Operation_Name = "Totem Mostrou Mensagem",
                Form = "FrmMensagem",
                Message = richMensagem.Text,
            //    RESNUM = Convert.ToInt32(Globals.glbReservaNumeroPesquisa),
                HostName = Globals.glbMachineName
            });
        }
                
        private void Shutdown_Enable()
        {
            if (Globals.glbDebugMode == 0)
                pictShutdown.Visible = false;
            else
                pictShutdown.Visible = true;
        }
        private void pictShutdown_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(1);
        }

        private void richMensagem_TextChanged(object sender, EventArgs e)
        {

        }
        
        private void tmrReturn_Tick(object sender, EventArgs e)
        {
            tmrReturn.Enabled = false;

            switch (Globals.glbScreen)
            {
                case 1:
                    Globals.glbScreen = 0;
                    frmScreen01 frm1 = new frmScreen01();
                    frm1.Show();
                    this.Dispose();
                    break;
                case 2:
                    Globals.glbScreen = 0;
                    frmScreen01 frm2 = new frmScreen01();
                    frm2.Show();
                    this.Dispose();
                    break;
                case 3:
                    Globals.glbScreen = 0;
                    frmScreen03 frm3 = new frmScreen03();
                    frm3.Show();
                    this.Dispose();
                    break;
                case 4:
                    Globals.glbScreen = 0;
                    frmScreen01 frm2a = new frmScreen01();
                    frm2a.Show();
                    this.Dispose();
                    break;
                case 6:
                    Globals.glbScreen = 0;
                    frmScreen06 frm06 = new frmScreen06();
                    frm06.Show();
                    this.Dispose();
                    break;
                case 7:
                    Globals.glbScreen = 0;
                    frmScreen07 frm7a = new frmScreen07();
                    frm7a.Show();
                    this.Dispose();
                    break;
                case 8:
                    Globals.glbScreen = 0;
                    frmScreen03 frm3a = new frmScreen03();
                    frm3a.Show();
                    this.Dispose();
                    break;
                case 10:
                    Globals.glbScreen = 0;
                    frmScreen10 frm10 = new frmScreen10();
                    frm10.Show();
                    this.Dispose();
                    break;
                case 13:
                    Globals.glbScreen = 0;
                    frmScreen13 frm13 = new frmScreen13();
                    frm13.Show();
                    this.Dispose();
                    break;
                case 14:
                    Globals.glbScreen = 0;
                    frmScreen14 frm14 = new frmScreen14();
                    frm14.Show();
                    this.Dispose();
                    break;
                case 15:
                    Globals.glbScreen = 0;
                    frmScreen01 frm15 = new frmScreen01();
                    frm15.Show();
                    this.Dispose();
                    break;
                case 16:
                    Globals.glbScreen = 0;
                    frmScreen05 frm16 = new frmScreen05();
                    frm16.Show();
                    this.Dispose();
                    break;
                case 17:
                    Globals.glbScreen = 0;
                    frmScreen13 frm17 = new frmScreen13();
                    frm17.Show();
                    this.Dispose();
                    break;
                default:
                    Globals.glbScreen = 0;
                    frmScreen01 frmDefault = new frmScreen01();
                    frmDefault.Show();
                    this.Dispose();
                    break;
            }
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            switch (Globals.glbScreen)
            {
                case 1:
                    Globals.glbScreen = 0;
                    frmScreen01 frm1 = new frmScreen01();
                    frm1.Show();
                    this.Dispose();
                    break;
                case 2:
                    Globals.glbScreen = 0;
                    frmScreen01 frm2 = new frmScreen01();
                    frm2.Show();
                    this.Dispose();
                    break;
                case 3:
                    Globals.glbScreen = 0;
                    frmScreen03 frm3 = new frmScreen03();
                    frm3.Show();
                    this.Dispose();
                    break;
                case 6:
                    Globals.glbScreen = 0;
                    frmScreen06 frm06 = new frmScreen06();
                    frm06.Show();
                    this.Dispose();
                    break;
                case 4:
                    Globals.glbScreen = 0;
                    frmScreen01 frm2a = new frmScreen01();
                    frm2a.Show();
                    this.Dispose();
                    break;
                case 7:
                    Globals.glbScreen = 0;
                    frmScreen07 frm7a = new frmScreen07();
                    frm7a.Show();
                    this.Dispose();
                    break;
                case 8:
                    Globals.glbScreen = 0;
                    frmScreen03 frm3a = new frmScreen03();
                    frm3a.Show();
                    this.Dispose();
                    break;
                case 10:
                    Globals.glbScreen = 0;
                    frmScreen10 frm10 = new frmScreen10();
                    frm10.Show();
                    this.Dispose();
                    break;
                case 13:
                    Globals.glbScreen = 0;
                    frmScreen13 frm13 = new frmScreen13();
                    frm13.Show();
                    this.Dispose();
                    break;
                case 14:
                    Globals.glbScreen = 0;
                    frmScreen14 frm14 = new frmScreen14();
                    frm14.Show();
                    this.Dispose();
                    break;
                case 15:
                    Globals.glbScreen = 0;
                    frmScreen01 frm15 = new frmScreen01();
                    frm15.Show();
                    this.Dispose();
                    break;
                case 16:
                    Globals.glbScreen = 0;
                    frmScreen05 frm16 = new frmScreen05();
                    frm16.Show();
                    this.Dispose();
                    break;
                case 17:
                    frmScreen13 frm17 = new frmScreen13();
                    frm17.Show();
                    this.Dispose();
                    break;
                default:
                    Globals.glbScreen = 0;
                    frmScreen01 frmDefault = new frmScreen01();
                    frmDefault.Show();
                    this.Dispose();
                    break;
            }
        }

        private void butVoltar_Click(object sender, EventArgs e)
        {
            switch (Globals.glbScreen)
            {
                case 1:
                    Globals.glbScreen = 0;
                    frmScreen01 frm1 = new frmScreen01();
                    frm1.Show();
                    this.Dispose();
                    break;
                case 2:
                    Globals.glbScreen = 0;
                    frmScreen01 frm2 = new frmScreen01();
                    frm2.Show();
                    this.Dispose();
                    break;
                case 3:
                    Globals.glbScreen = 0;
                    frmScreen03 frm3 = new frmScreen03();
                    frm3.Show();
                    this.Dispose();
                    break;
                case 6:
                    Globals.glbScreen = 0;
                    frmScreen06 frm06 = new frmScreen06();
                    frm06.Show();
                    this.Dispose();
                    break;
                case 4:
                    Globals.glbScreen = 0;
                    frmScreen01 frm2a = new frmScreen01();
                    frm2a.Show();
                    this.Dispose();
                    break;
                case 7:
                    Globals.glbScreen = 0;
                    frmScreen07 frm7a = new frmScreen07();
                    frm7a.Show();
                    this.Dispose();
                    break;
                case 8:
                    Globals.glbScreen = 0;
                    frmScreen03 frm3a = new frmScreen03();
                    frm3a.Show();
                    this.Dispose();
                    break;
                case 10:
                    Globals.glbScreen = 0;
                    frmScreen10 frm10 = new frmScreen10();
                    frm10.Show();
                    this.Dispose();
                    break;
                case 13:
                    Globals.glbScreen = 0;
                    frmScreen13 frm13 = new frmScreen13();
                    frm13.Show();
                    this.Dispose();
                    break;
                case 14:
                    Globals.glbScreen = 0;
                    frmScreen14 frm14 = new frmScreen14();
                    frm14.Show();
                    this.Dispose();
                    break;
                case 15:
                    Globals.glbScreen = 0;
                    frmScreen01 frm15 = new frmScreen01();
                    frm15.Show();
                    this.Dispose();
                    break;
                case 16:
                    Globals.glbScreen = 0;
                    frmScreen05 frm16 = new frmScreen05();
                    frm16.Show();
                    this.Dispose();
                    break;
                case 17:
                    frmScreen13 frm17 = new frmScreen13();
                    frm17.Show();
                    this.Dispose();
                    break;
                default:
                    Globals.glbScreen = 0;
                    frmScreen01 frmDefault = new frmScreen01();
                    frmDefault.Show();
                    this.Dispose();
                    break;
            }
        }

        private void AtualizarInterfaceParaIdiomaCorrente()
        {

            MontarResourceSetParaIdiomaCorrente();


            butIdioma.Text = ResxSetForm.GetString("butIdioma.Text");
            butVoltar.Text = ResxSetForm.GetString("butVoltar.Text");

            this.Icon = ((System.Drawing.Icon)(ResxSetFormMensagens.GetObject("$this.Icon")));

            if (Globals.glbLanguage == 1) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_Brasil_botao;
            if (Globals.glbLanguage == 2) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_Espanha_botao;
            if (Globals.glbLanguage == 3) butIdioma.Image = centralUnidas.Properties.Resources.Totem_Flag_UK_botao;

            butIdioma.Refresh();

            ResxSetForm.Close();

            ResxSetForm = null;
        }

        private void MontarResourceSetParaIdiomaCorrente()
        {
            string srtCaminhoExeTotem = System.IO.Path.GetDirectoryName(Application.ExecutablePath.ToString());
            // Português
            if (Globals.glbLanguage == 1)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen03.pt.resx");
            }
            //Espanhol
            else if (Globals.glbLanguage == 2)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen03.es.resx");
            }
            // Inglês
            else if (Globals.glbLanguage == 3)
            {
                ResxSetForm = new ResXResourceSet(srtCaminhoExeTotem + "\\frmScreen03.en.resx");
            }
        }

        private void pictUnidasLogo_Click(object sender, EventArgs e)
        {


        }
    }
}
