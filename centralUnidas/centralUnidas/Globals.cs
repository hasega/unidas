﻿// Aplicação: Unidas Totem
// Data     : 01/10/2015
// Versão   : 1.0
// Empresa  : Unidas Rent a Car
// Equipe   : Unidas Desenvolvimento
// Objetivo : Variaveis globais do sistema
// Histórico: 
//      01/10/2015: Inicio de desenvolvimento para acesso geral
//

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;

namespace centralUnidas
{

    public class Globals
    {
        // glbLanguage
        //  1 - Portugues
        //  2 - Espanhol
        //  3 - Ingles
        public static long? glbOperationTicks = null;
        public static string glbApplicationName = "Totem";
        public static string glbApplicationVersion = null;
        public static string glbMachineName = System.Environment.MachineName;
        public static int glbR_ACONFEC = 0;
        public static int glbVezes;
        public static int glbLanguage;
        public static int glbScreen;
        public static int glbMensagem;
        public static string glbMensagemSitef;
        public static int glbPopupSitef;
        public static int glbTimeoutSitef;
        public static int glbInteractSitef;
        public static string glbLojaTotem = "";

        public static int glbScreenCard;
        public static string glbCC_R_ANUM;
        public static string glbCC_R_ANUM_RA;
        public static string glbCC_R_ALOJRET;
        public static string glbCC_CCAITENUM;
        public static string glbCC_CupomFiscal;
        public static string glbCC_ValidadeCupomFiscal;
        public static string glbCC_CodigoPreAutorizacao;
        public static string glbCC_CodigoAutorizacaoEmissor;

        public static string glbCC_CartaoInformado;
        public static string glbCC_ValidadeInformado;
        public static string glbCC_SegurancaInformado;

        public static string glbSEG_R_ANUM;
        public static string glbSEG_R_ALOJRET;
        public static string glbSEG_PAGITENUM;
        public static string glbSEG_HASH;
        public static string glbSEG_TP_INFO;
        public static string glbSEG_TP_OPER;
        public static string glbSEG_HASH_OUT;
        public static string glbSEG_RT_OPER;
        public static string glbSEG_Cpf;
        public static string glbSEG_CrtNum;
        public static string glbSEG_MessageText;
        public static string glbSEG_HASH_NEW;

        public static string glbReservaNumeroPesquisa;
        public static string glbReservaNumero;
        public static string glbGivenName;
        public static string glbGender;
        public static string glbEmail;
        public static string glbVehReservation;
        public static string glbVoucherIdentifier;
        public static string glbVOUPRE;
        public static string glbVoucherValueType;
        public static string glbConfID;
        public static string glbCompanyShortName;
        public static string glbCompanyNameCode;
        public static string glbResGruCod;
        public static string glbPickUpDateTime;
        public static string glbReturnDateTime;
        public static string glbDistUnitName;
        public static string glbPickUpLocation;
        public static string glbReturnLocation;
        public static string glbPassengerQuantity;
        public static string glbBaggageQuantity;
        public static string glbVehicleCode;
        public static string glbVehicleCodeContext;
        public static string glbVehicleMakeModel;
        public static string glbPictureURL;
        public static string glbVehicleChargeCurrencyCode;
        public static string glbVehicleChargeAmount;
        public static string glbVehicleChargeTaxInclusive;
        public static string glbVehicleChargeGuaranteedInd;
        public static string glbVehicleChargePurpose;
        public static string glbCOR;
        public static string glbKM;
        public static string glbVEICOM;
        public static string glbAcordo;
        public static string glbAcordoSenha;
        public static string glbACOCOD;
        public static string glbACOSEN;
        public static string glbACOWBS;
        public static string glbACWTAG;

        public static string glbSTASIG = "";
        public static string glbMOTDES = "";
        public static string glbPRASIG = "";
        public static string glbLOJNOM = "";
        public static string glbLOJDES = "";
        public static string glbLOJNOMDEV = "";
        public static string glbLOJDESDEV = "";
        public static string glbLOJMAPBLQ = "";
        public static string glbLOJMAPPER = "";
        public static string glbGRUCOD = "";
        public static string glbGRUSIPP = "";
        public static string glbGRUDES = "";
        public static string glbGRUURL = "";
        public static string glbFRTOPE = "";
        public static string glbTOTMAPA = "";
        public static string glbTOTOCUP = "";
        public static string glbDISPPERC = "";
        public static string glbDISPPERC_DSC = "";
        public static string glbDISPPERC_AGV = "";
        public static string glbDISPONIB1 = "";
        public static string glbDISPONIB2 = "";
        public static string glbSTATUS = "";
        public static string glbAGRAVO = "";
        public static string glbMODDES = "";
        public static string glbTPMDES = "";
        public static string glbCOMDES = "";
        public static string glbVEIPLA = "";
        public static string glbCATSRVCOD = "";
        public static string glbCATSRVDSC = "";
        public static string glbCATSRVVAL = "";
        public static string glbRESOPE = "";
        public static string glbLOJTEL = "";
        public static string glbLOJCODSITE = "";
        public static string glbSTIP = "";
        public static string glbSTPORTA = "";
        public static string glbRESDOCCLI = "";
        public static string glbRESNOM = "";
        public static string glbPRECPF = "";
        public static string glbPRENOM = "";

        public static string glbCPFCARHAB = "";
        public static string glbCPFEXPHAB = "";
        public static string glbR_ANUMRES = "";
        public static string glbDATA_CAP = "";

        public static string[] glbPricedEquipDescription;
        public static string[] glbPricedEquipChargeAmount;
        public static string[] glbPricedEquipChargeUnitCharge;
        public static string[] glbPricedEquipChargeQuantity;

        public static string[] glbPricedCoverageCode;
        public static string[] glbPricedCoverageDetails;
        public static string[] glbPricedCoverageChargeAmount;
        public static string[] glbPricedCoverageChargeCalculationUnitCharge;
        public static string[] glbPricedCoverageChargeCalculationQuantity;

        public static string[] glbFeeAmount;
        public static string[] glbFeeDescription;
        public static string[] glbFeeAmountUnit;
        public static string[] glbFeeUnitCharge;
        public static string[] glbFeeAmountPercentage;

        public static string glbCRTEXPCRP = "";
        public static string glbCCACODSEGC = "";
        public static string glbCCANUMCRP = "";
        public static string glbCCANUM = "";


        public static string glbResumoDescontoPercentage = "";
        public static string glbResumoDescontoTotal = "";

        public static string glbProtecoesVar1 = "";
        public static string glbProtecoesVar2 = "";
        public static string glbProtecoesVar3 = "";
        public static string glbProtecoesVar4 = "";
        public static string glbProtecoesVar5 = "";
        public static string glbProtecoesVar6 = "";

        public static string glbProtecoesCodVar = "";

        public static string glbProtecoesCodVar1 = "";
        public static string glbProtecoesCodVar2 = "";
        public static string glbProtecoesCodVar3 = "";
        public static string glbProtecoesCodVar4 = "";
        public static string glbProtecoesCodVar5 = "";
        public static string glbProtecoesCodVar6 = "";

        public static string glbProtecoesValorVar1 = "";
        public static string glbProtecoesValorVar2 = "";
        public static string glbProtecoesValorVar3 = "";
        public static string glbProtecoesValorVar4 = "";
        public static string glbProtecoesValorVar5 = "";
        public static string glbProtecoesValorVar6 = "";

        public static string glbProtecoesValorPre1 = "";
        public static string glbProtecoesValorPre2 = "";
        public static string glbProtecoesValorPre3 = "";
        public static string glbProtecoesValorPre4 = "";
        public static string glbProtecoesValorPre5 = "";
        public static string glbProtecoesValorPre6 = "";

        public static string glbProtecoesQtdUnit1 = "";
        public static string glbProtecoesQtdUnit2 = "";
        public static string glbProtecoesQtdUnit3 = "";
        public static string glbProtecoesQtdUnit4 = "";
        public static string glbProtecoesQtdUnit5 = "";
        public static string glbProtecoesQtdUnit6 = "";

        public static string glbProtecoesValorPar1 = "";
        public static string glbBandeiraCartao = "";

        public static string glbAcessoriosVar1 = "";
        public static string glbAcessoriosVar2 = "";
        public static string glbAcessoriosVar3 = "";
        public static string glbAcessoriosVar4 = "";
        public static string glbAcessoriosVar5 = "";
        public static string glbAcessoriosVar6 = "";
        public static string glbAcessoriosVar7 = "";
        public static string glbAcessoriosVar8 = "";
        public static string glbAcessoriosVar9 = "";

        public static string glbAcessoriosCodVar1 = "";
        public static string glbAcessoriosCodVar2 = "";
        public static string glbAcessoriosCodVar3 = "";
        public static string glbAcessoriosCodVar4 = "";
        public static string glbAcessoriosCodVar5 = "";
        public static string glbAcessoriosCodVar6 = "";
        public static string glbAcessoriosCodVar7 = "";
        public static string glbAcessoriosCodVar8 = "";
        public static string glbAcessoriosCodVar9 = "";

        public static string glbAcessoriosValorVar1 = "";
        public static string glbAcessoriosValorVar2 = "";
        public static string glbAcessoriosValorVar3 = "";
        public static string glbAcessoriosValorVar4 = "";
        public static string glbAcessoriosValorVar5 = "";
        public static string glbAcessoriosValorVar6 = "";
        public static string glbAcessoriosValorVar7 = "";
        public static string glbAcessoriosValorVar8 = "";
        public static string glbAcessoriosValorVar9 = "";

        public static string glbServicosCodVar1 = "";
        public static string glbServicosVar1 = "";
        public static string glbRetornoCodVar1 = "";
        public static string glbRetornoVar1 = "";

        // Valores da reserva
        public static string glbRTBDIARIA = "";
        public static string glbRTBDIAEXT = "";
        public static string glbRTBHOREXT = "";
        public static string glbRTBTOTDIA = "";
        public static string glbRTBTOTDIE = "";
        public static string glbRTBTOTHRE = "";
        public static string glbRTBTOTDSC = "";
        public static string glbRTBSUBTOT = "";
        public static string glbRTBTOTRES = "";
        public static string glbRTBTOTRES_ANT = "";
        public static string glbRTBVALDIA = "";
        public static string glbRTBVALFRQ = "";
        public static string glbRTBPERTXS = "";
        public static string glbRTBVALTXS = "";
        public static string glbRTBPERTAG = "";
        public static string glbRTBVALTAG = "";

        // Resumo da Reserva
        public static string glbResumoDiariasQtd = "";
        public static string glbResumoDiariasUnit = "";
        public static string glbResumoDiariasVar = "";

        public static string glbResumoDescontoQtd = "";
        public static string glbResumoDescontoVar = "";

        public static string glbResumoHoraExtraQtd = "";
        public static string glbResumoHoraExtraVar = "";

        public static string glbResumoProtecoesVar1 = "";
        public static string glbResumoProtecoesVar2 = "";
        public static string glbResumoProtecoesVar3 = "";
        public static string glbResumoProtecoesQtd1 = "";
        public static string glbResumoProtecoesQtd2 = "";
        public static string glbResumoProtecoesQtd3 = "";
        public static string glbResumoProtecoesUnit1 = "";
        public static string glbResumoProtecoesUnit2 = "";
        public static string glbResumoProtecoesUnit3 = "";
        public static string glbResumoProtecoesTot1 = "";
        public static string glbResumoProtecoesTot2 = "";
        public static string glbResumoProtecoesTot3 = "";

        //

        public static string glbResumoAcessoriosVar1 = "";
        public static string glbResumoAcessoriosVar2 = "";
        public static string glbResumoAcessoriosVar3 = "";
        public static string glbResumoAcessoriosQtd1 = "";
        public static string glbResumoAcessoriosQtd2 = "";
        public static string glbResumoAcessoriosQtd3 = "";
        public static string glbResumoAcessoriosUnit1 = "";
        public static string glbResumoAcessoriosUnit2 = "";
        public static string glbResumoAcessoriosUnit3 = "";
        public static string glbResumoAcessoriosTot1 = "";
        public static string glbResumoAcessoriosTot2 = "";
        public static string glbResumoAcessoriosTot3 = "";

        //

        public static string glbResumoAdministrativaUnit = "";
        public static string glbResumoAdministrativaTot = "";
        public static string glbResumoTotalGeral = "";

        // 1 - VISA
        // 2 - Mastercard
        // 3 - American
        // 4 - Discover

        public static string glbPortaPinPad = "";
        public static string glbLojMunCod = "";
        public static string glbCartao = "";

        public static int glbDebugMode;
        public static string glbCupomFiscal1;
        public static string glbCupomFiscal2;

        // ra
        public static string glbRA_Contrato = "";
        public static string glbRA_clifat = "";
        public static string glbRA_clidoc = "";
        public static string glbRA_clinom = "";
        public static string glbRA_clinomfat = "";
        public static string glbRA_prfcod = "";
        public static string glbRA_tclcod = "";
        public static string glbRA_r_aresloj = "";
        public static string glbRA_r_alojret = "";
        public static string glbRA_r_adatret = "";
        public static string glbRA_r_ahorret = "";
        public static string glbRA_r_adatdev = "";
        public static string glbRA_r_ahordev = "";
        public static string glbRA_grucod = "";
        public static string glbRA_r_alojdev = "";
        public static string glbRA_tptconcod = "";
        public static string glbRA_tardes = "";
        public static string glbRA_trfcod = "";
        public static string glbRA_acocod = "";
        public static string glbRA_acodig = "";
        public static string glbRA_R_aCliSeg = "";
        public static string glbRA_R_aCanVen = "";
        public static string glbRA_restrfnew = "";
        public static string glbRA_r_aprot = "";
        public static string glbRA_r_aprotpot = "";
        public static string glbRA_r_aprtpa = "";
        public static string glbRA_r_adsc = "";
        public static string glbRA_clitarcod = "";
        public static string glbRA_clitarseq = "";
        public static string glbRA_rescodsml = "";
        public static string glbRA_restipsml = "";
        public static string glbRA_ResTVch = "";
        public static string glbRA_ResExpCli = "";
        public static string glbRA_respax = "";
        public static string glbRA_respaxcpf = "";
        public static string glbRA_resgrupro = "";
        public static string glbRA_resmotadi = "";
        public static string glbRA_rescodtam = "";
        public static string glbRA_rescupcod = "";
        public static string glbRA_resdiaaut = "";
        public static string glbRA_ResTpvCh = "";
        public static string glbRA_respnr = "";
        public static string glbRA_r_atipo = "";

        public static string glbRESPESCPF = "";
        public static string glbCLI_CLIDOC = "";
        public static string glbCLIDOC_EXIST = "";
        public static string glbCLI_CLINOM = "";
        public static string glbCLI_CLINOM_SAUDACOES = "";
        public static string glbCLI_CPFENDRES = "";
        public static string glbCLI_CPFCIDRES = "";
        public static string glbCLI_CPFESTRES = "";
        public static string glbCLI_CPFCEPRES = "";
        public static string glbCLI_CPFTELRES = "";
        public static string glbCLI_CPFCPF = "";
        public static string glbCLI_CPFRG = "";
        public static string glbCLI_CPFCARHAB = "";
        public static string glbCLI_CPFESTHAB = "";
        public static string glbCLI_CPFEXPHAB = "";
        public static string glbCLI_CLIPRMHAB = "";
        public static string glbCLI_TCLCOD = "";
        public static string glbCLI_PRFTIP = "";
        public static string glbCLI_PRFCOD = "";
        public static string glbCLI_CPFPAS = "";
        public static string glbCLI_CPFSEX = "";
        public static string glbCLI_CPFDATNAS = "";
        public static string glbCLI_CLICARDSC = "";
        public static string glbCLI_PRASIG = "";
        public static string glbCLI_CLICATEGO = "";
        public static string glbCLI_CLIEMAIL = "";
        public static string glbCLI_CRECOD = "";
        public static string glbCLI_R_ANUM = "";
        public static string glbCLI_R_ATIPO = "";

        public static string glbVOU_RESNUM = "";
        public static string glbVOU_RESLOJOPE = "";
        public static string glbVOU_RESNVCH = "";
        public static string glbVOU_RESVDIA = "";
        public static string glbVOU_RESVDIAE = "";
        public static string glbVOU_RESVHORE = "";
        public static string glbVOU_RESVKM = "";
        public static string glbVOU_RESVTAXS = "";
        public static string glbVOU_RESVPROT = "";
        public static string glbVOU_RESVMOT = "";
        public static string glbVOU_RESVAPRI = "";
        public static string glbVOU_RESVCMB = "";
        public static string glbVOU_RESVAVA = "";
        public static string glbVOU_RESVPRTO = "";
        public static string glbVOU_RESVADC = "";
        public static string glbVOU_RESVMOTA = "";
        public static string glbVOU_RESVTAXR = "";
        public static string glbVOU_RESVDSP = "";
        public static string glbVOU_RESMUL = "";
        public static string glbVOU_RESNVCLI = "";

        public static string[] glbSERV_RESLOJOPE;
        public static string[] glbSERV_CATSRVCOD;
        public static string[] glbSERV_CATSRVDSC;
        public static string[] glbSERV_CATSRVTIP;
        public static string[] glbSERV_CATSRVOTA;
        public static string[] glbSERV_SRVCOD;
        public static string[] glbSERV_CATSRVQFR;
        public static string[] glbSERV_RESSRVCPF;
        public static string[] glbSERV_RESSRVDAT;
        public static string[] glbSERV_RESSRVVAL;
        public static string[] glbSERV_RESSRVGRU;
        public static string[] glbSERV_RESFRQVAL;
        public static string[] glbSERV_RESQTDSRV;
        public static string[] glbSERV_RESQTDFRQ;
        public static string[] glbSERV_RESSRVTOT;
        public static string[] glbSERV_RESFRQVALD;
        public static string[] glbSERV_CATVALPAR;

        public List<string> returnAttributesXml(XmlDocument xmlDoc, string strNode, string strAttributeName)
        {
            List<string> strReturn = new List<string>();
            XmlNodeList xmlValue = xmlDoc.SelectNodes(strNode);
            if (xmlValue.Count > 0)
            {
                for (int x = 0; x < xmlValue.Count; x++)
                {
                    XmlNode node = xmlValue.Item(x).Attributes[strAttributeName];
                    if (node != null && !string.IsNullOrEmpty(node.Value))
                    {
                        strReturn.Add(node.Value);
                    }
                }
            }
            return strReturn;
        }
        public string returnAttributesOneXml(XmlDocument xmlDoc, string strNode, string strAttributeName)
        {
            string strReturn = "";
            XmlNodeList xmlValue = xmlDoc.SelectNodes(strNode);
            if (xmlValue.Count > 0)
            {
                for (int x = 0; x < xmlValue.Count; x++)
                {
                    XmlNode node = xmlValue.Item(x).Attributes[strAttributeName];
                    if (node != null && !string.IsNullOrEmpty(node.Value))
                    {
                        strReturn = node.Value;
                        break;
                    }
                }
            }
            return strReturn;
        }

        public List<string> returnInnerListXml(XmlDocument xmlDoc, string strNode)
        {
            List<string> strReturn = new List<string>();
            XmlNodeList xmlValue = xmlDoc.SelectNodes(strNode);
            if (xmlValue.Count > 0)
            {
                for (int x = 0; x < xmlValue.Count; x++)
                {
                    strReturn.Add(xmlValue.Item(x).InnerXml);
                }
            }
            return strReturn;
        }

        public string returnInnerXml(XmlDocument xmlDoc, string strNode)
        {
            string strReturn = "";
            XmlNodeList xmlValue = xmlDoc.SelectNodes(strNode);
            if (xmlValue.Count > 0)
            {
                for (int x = 0; x < xmlValue.Count; x++)
                {
                    strReturn = xmlValue.Item(x).InnerXml;
                }
            }
            return strReturn;
        }

    }
}
