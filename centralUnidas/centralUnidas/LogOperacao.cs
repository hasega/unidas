﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace centralUnidas
{
    public class LogOperacao
    {
        public string Application_Name { get; set; }
        public string Application_Version { get; set; }
        public string Local { get; set; }
        public long? Operation_Ticks { get; set; }
        public string Operation_Name { get; set; }
        public string Form { get; set; }
        public int? RESNUM { get; set; }
        public int? RANUM { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public string Output_XML { get; set; }
        public string HostName { get; set; }
        public string LoginName { get; set; }
        public string DbUsername { get; set; }
        public string Client_Net_Address { get; set; }
        public string Client_Mac_Address { get; set; }
        public string Client_Connection_Id { get; set; }

        public LogOperacao()
        {
            this.Application_Name = null;
            this.Application_Version = null;
            this.Local = null;
            this.Operation_Ticks = null;
            this.Operation_Name = null;
            this.Form = null;
            this.RESNUM = null;
            this.RANUM = null;
            this.Status = null;
            this.Message = null;
            this.Output_XML = null;
            this.HostName = null;
            this.LoginName = null;
            this.DbUsername = null;
            this.Client_Net_Address = null;
            this.Client_Mac_Address = null;
            this.Client_Connection_Id = null;            
        }

    }

}
