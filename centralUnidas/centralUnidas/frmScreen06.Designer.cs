﻿namespace centralUnidas
{
    partial class frmScreen06
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmScreen06));
            this.pictShutdown = new System.Windows.Forms.PictureBox();
            this.lstPlacas = new System.Windows.Forms.ListView();
            this.colCheck = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colVEIPLA = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colCORDES = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colVEIKMSATU = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colVEICOM = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imgList = new System.Windows.Forms.ImageList(this.components);
            this.lblModelo01 = new System.Windows.Forms.Label();
            this.lblModelo02 = new System.Windows.Forms.Label();
            this.pictUnidasLogo = new System.Windows.Forms.PictureBox();
            this.pictVeiculo1 = new System.Windows.Forms.PictureBox();
            this.lblUseAsSetas = new System.Windows.Forms.Label();
            this.pictDireita = new System.Windows.Forms.PictureBox();
            this.pictEsquerda = new System.Windows.Forms.PictureBox();
            this.tmrReturn = new System.Windows.Forms.Timer(this.components);
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblSelecioneAbaixoOpcao = new System.Windows.Forms.Label();
            this.butIdioma = new System.Windows.Forms.Button();
            this.butVoltar = new System.Windows.Forms.Button();
            this.butConfirmar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictShutdown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictVeiculo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictDireita)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictEsquerda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // pictShutdown
            // 
            this.pictShutdown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictShutdown.BackColor = System.Drawing.Color.Transparent;
            this.pictShutdown.Image = global::centralUnidas.Properties.Resources.shutdown_button;
            this.pictShutdown.Location = new System.Drawing.Point(12, 146);
            this.pictShutdown.Name = "pictShutdown";
            this.pictShutdown.Size = new System.Drawing.Size(50, 55);
            this.pictShutdown.TabIndex = 106;
            this.pictShutdown.TabStop = false;
            this.pictShutdown.Visible = false;
            this.pictShutdown.Click += new System.EventHandler(this.pictShutdown_Click);
            // 
            // lstPlacas
            // 
            this.lstPlacas.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.lstPlacas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstPlacas.BackColor = System.Drawing.Color.White;
            this.lstPlacas.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colCheck,
            this.colVEIPLA,
            this.colCORDES,
            this.colVEIKMSATU,
            this.colVEICOM});
            this.lstPlacas.Font = new System.Drawing.Font("Tahoma", 18F);
            this.lstPlacas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lstPlacas.FullRowSelect = true;
            this.lstPlacas.GridLines = true;
            this.lstPlacas.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstPlacas.LargeImageList = this.imgList;
            this.lstPlacas.Location = new System.Drawing.Point(742, 295);
            this.lstPlacas.MultiSelect = false;
            this.lstPlacas.Name = "lstPlacas";
            this.lstPlacas.Scrollable = false;
            this.lstPlacas.Size = new System.Drawing.Size(541, 327);
            this.lstPlacas.SmallImageList = this.imgList;
            this.lstPlacas.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lstPlacas.TabIndex = 107;
            this.lstPlacas.UseCompatibleStateImageBehavior = false;
            this.lstPlacas.View = System.Windows.Forms.View.Details;
            this.lstPlacas.SelectedIndexChanged += new System.EventHandler(this.lstPlacas_SelectedIndexChanged);
            // 
            // colCheck
            // 
            this.colCheck.Text = "";
            this.colCheck.Width = 50;
            // 
            // colVEIPLA
            // 
            this.colVEIPLA.Text = "Placa";
            this.colVEIPLA.Width = 120;
            // 
            // colCORDES
            // 
            this.colCORDES.Text = "Cor";
            this.colCORDES.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.colCORDES.Width = 160;
            // 
            // colVEIKMSATU
            // 
            this.colVEIKMSATU.Text = "Quilometragem";
            this.colVEIKMSATU.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.colVEIKMSATU.Width = 180;
            // 
            // colVEICOM
            // 
            this.colVEICOM.Text = "Combustivel";
            this.colVEICOM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.colVEICOM.Width = 0;
            // 
            // imgList
            // 
            this.imgList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgList.ImageStream")));
            this.imgList.TransparentColor = System.Drawing.Color.Transparent;
            this.imgList.Images.SetKeyName(0, "ar.gif");
            this.imgList.Images.SetKeyName(1, "check1.png");
            this.imgList.Images.SetKeyName(2, "Img_Abs.png");
            this.imgList.Images.SetKeyName(3, "Img_Airbag.png");
            this.imgList.Images.SetKeyName(4, "malas.gif");
            this.imgList.Images.SetKeyName(5, "passageiros.gif");
            this.imgList.Images.SetKeyName(6, "volante.gif");
            this.imgList.Images.SetKeyName(7, "uncheck1.png");
            // 
            // lblModelo01
            // 
            this.lblModelo01.AutoSize = true;
            this.lblModelo01.BackColor = System.Drawing.Color.Transparent;
            this.lblModelo01.Font = new System.Drawing.Font("Arial", 20.25F);
            this.lblModelo01.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblModelo01.Location = new System.Drawing.Point(165, 260);
            this.lblModelo01.Name = "lblModelo01";
            this.lblModelo01.Size = new System.Drawing.Size(0, 32);
            this.lblModelo01.TabIndex = 108;
            // 
            // lblModelo02
            // 
            this.lblModelo02.AutoSize = true;
            this.lblModelo02.BackColor = System.Drawing.Color.Transparent;
            this.lblModelo02.Font = new System.Drawing.Font("Arial", 20.25F);
            this.lblModelo02.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblModelo02.Location = new System.Drawing.Point(165, 308);
            this.lblModelo02.Name = "lblModelo02";
            this.lblModelo02.Size = new System.Drawing.Size(0, 32);
            this.lblModelo02.TabIndex = 109;
            // 
            // pictUnidasLogo
            // 
            this.pictUnidasLogo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictUnidasLogo.BackColor = System.Drawing.Color.Transparent;
            this.pictUnidasLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictUnidasLogo.Image = global::centralUnidas.Properties.Resources.totem_header;
            this.pictUnidasLogo.Location = new System.Drawing.Point(0, 0);
            this.pictUnidasLogo.Name = "pictUnidasLogo";
            this.pictUnidasLogo.Size = new System.Drawing.Size(1366, 140);
            this.pictUnidasLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictUnidasLogo.TabIndex = 113;
            this.pictUnidasLogo.TabStop = false;
            // 
            // pictVeiculo1
            // 
            this.pictVeiculo1.Location = new System.Drawing.Point(258, 362);
            this.pictVeiculo1.Name = "pictVeiculo1";
            this.pictVeiculo1.Size = new System.Drawing.Size(300, 200);
            this.pictVeiculo1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictVeiculo1.TabIndex = 119;
            this.pictVeiculo1.TabStop = false;
            // 
            // lblUseAsSetas
            // 
            this.lblUseAsSetas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUseAsSetas.AutoSize = true;
            this.lblUseAsSetas.BackColor = System.Drawing.Color.White;
            this.lblUseAsSetas.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold);
            this.lblUseAsSetas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblUseAsSetas.Location = new System.Drawing.Point(230, 598);
            this.lblUseAsSetas.Name = "lblUseAsSetas";
            this.lblUseAsSetas.Size = new System.Drawing.Size(346, 24);
            this.lblUseAsSetas.TabIndex = 120;
            this.lblUseAsSetas.Text = "Use as setas para trocar de carro";
            this.lblUseAsSetas.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblUseAsSetas.Visible = false;
            // 
            // pictDireita
            // 
            this.pictDireita.BackColor = System.Drawing.Color.Transparent;
            this.pictDireita.Image = global::centralUnidas.Properties.Resources.SetaAbaixo;
            this.pictDireita.Location = new System.Drawing.Point(1289, 295);
            this.pictDireita.Name = "pictDireita";
            this.pictDireita.Size = new System.Drawing.Size(48, 50);
            this.pictDireita.TabIndex = 121;
            this.pictDireita.TabStop = false;
            this.pictDireita.Click += new System.EventHandler(this.pictDireita_Click);
            // 
            // pictEsquerda
            // 
            this.pictEsquerda.BackColor = System.Drawing.Color.Transparent;
            this.pictEsquerda.Image = global::centralUnidas.Properties.Resources.SetaCima;
            this.pictEsquerda.Location = new System.Drawing.Point(1289, 572);
            this.pictEsquerda.Name = "pictEsquerda";
            this.pictEsquerda.Size = new System.Drawing.Size(48, 50);
            this.pictEsquerda.TabIndex = 122;
            this.pictEsquerda.TabStop = false;
            this.pictEsquerda.Click += new System.EventHandler(this.pictEsquerda_Click);
            // 
            // tmrReturn
            // 
            this.tmrReturn.Enabled = true;
            this.tmrReturn.Interval = 60000;
            this.tmrReturn.Tick += new System.EventHandler(this.tmrReturn_Tick);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox2.Image = global::centralUnidas.Properties.Resources.Totem_Traco;
            this.pictureBox2.Location = new System.Drawing.Point(345, 212);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(683, 1);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 142;
            this.pictureBox2.TabStop = false;
            // 
            // lblSelecioneAbaixoOpcao
            // 
            this.lblSelecioneAbaixoOpcao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSelecioneAbaixoOpcao.BackColor = System.Drawing.Color.White;
            this.lblSelecioneAbaixoOpcao.Font = new System.Drawing.Font("Arial", 26F, System.Drawing.FontStyle.Bold);
            this.lblSelecioneAbaixoOpcao.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblSelecioneAbaixoOpcao.Location = new System.Drawing.Point(0, 155);
            this.lblSelecioneAbaixoOpcao.Name = "lblSelecioneAbaixoOpcao";
            this.lblSelecioneAbaixoOpcao.Size = new System.Drawing.Size(1366, 54);
            this.lblSelecioneAbaixoOpcao.TabIndex = 159;
            this.lblSelecioneAbaixoOpcao.Text = "Selecione abaixo a opção de sua preferência: ";
            this.lblSelecioneAbaixoOpcao.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // butIdioma
            // 
            this.butIdioma.FlatAppearance.BorderSize = 0;
            this.butIdioma.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.butIdioma.ForeColor = System.Drawing.Color.Gray;
            this.butIdioma.Image = global::centralUnidas.Properties.Resources.Totem_Flag_Brasil_botao;
            this.butIdioma.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butIdioma.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.butIdioma.Location = new System.Drawing.Point(1148, 0);
            this.butIdioma.Name = "butIdioma";
            this.butIdioma.Size = new System.Drawing.Size(148, 61);
            this.butIdioma.TabIndex = 166;
            this.butIdioma.Text = "PORTUGUÊS";
            this.butIdioma.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butIdioma.UseCompatibleTextRendering = true;
            this.butIdioma.UseMnemonic = false;
            this.butIdioma.UseVisualStyleBackColor = true;
            this.butIdioma.Click += new System.EventHandler(this.butIdioma_Click);
            // 
            // butVoltar
            // 
            this.butVoltar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.butVoltar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.butVoltar.Font = new System.Drawing.Font("Arial", 22F, System.Drawing.FontStyle.Bold);
            this.butVoltar.ForeColor = System.Drawing.Color.White;
            this.butVoltar.Location = new System.Drawing.Point(431, 661);
            this.butVoltar.Name = "butVoltar";
            this.butVoltar.Size = new System.Drawing.Size(263, 79);
            this.butVoltar.TabIndex = 169;
            this.butVoltar.Text = "Voltar";
            this.butVoltar.UseVisualStyleBackColor = false;
            this.butVoltar.Click += new System.EventHandler(this.butVoltar_Click);
            // 
            // butConfirmar
            // 
            this.butConfirmar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.butConfirmar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.butConfirmar.Font = new System.Drawing.Font("Arial", 22F, System.Drawing.FontStyle.Bold);
            this.butConfirmar.ForeColor = System.Drawing.Color.White;
            this.butConfirmar.Location = new System.Drawing.Point(765, 661);
            this.butConfirmar.Name = "butConfirmar";
            this.butConfirmar.Size = new System.Drawing.Size(263, 79);
            this.butConfirmar.TabIndex = 170;
            this.butConfirmar.Text = "Confirmar";
            this.butConfirmar.UseVisualStyleBackColor = false;
            this.butConfirmar.Click += new System.EventHandler(this.butConfirmar_Click);
            // 
            // frmScreen06
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(1366, 741);
            this.ControlBox = false;
            this.Controls.Add(this.butConfirmar);
            this.Controls.Add(this.butVoltar);
            this.Controls.Add(this.butIdioma);
            this.Controls.Add(this.pictShutdown);
            this.Controls.Add(this.lblSelecioneAbaixoOpcao);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictEsquerda);
            this.Controls.Add(this.pictDireita);
            this.Controls.Add(this.lblUseAsSetas);
            this.Controls.Add(this.pictVeiculo1);
            this.Controls.Add(this.pictUnidasLogo);
            this.Controls.Add(this.lblModelo02);
            this.Controls.Add(this.lblModelo01);
            this.Controls.Add(this.lstPlacas);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmScreen06";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CAU - Unidas";
            this.TopMost = false;
            this.WindowState = System.Windows.Forms.FormWindowState.Normal;
            this.Load += new System.EventHandler(this.frmScreen06_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictShutdown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictVeiculo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictDireita)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictEsquerda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictShutdown;
        private System.Windows.Forms.ListView lstPlacas;
        private System.Windows.Forms.ColumnHeader colVEIPLA;
        private System.Windows.Forms.ColumnHeader colCORDES;
        private System.Windows.Forms.ColumnHeader colVEIKMSATU;
        private System.Windows.Forms.Label lblModelo01;
        private System.Windows.Forms.Label lblModelo02;
        private System.Windows.Forms.ColumnHeader colCheck;
        private System.Windows.Forms.ImageList imgList;
        private System.Windows.Forms.PictureBox pictUnidasLogo;
        private System.Windows.Forms.PictureBox pictVeiculo1;
        private System.Windows.Forms.Label lblUseAsSetas;
        private System.Windows.Forms.PictureBox pictDireita;
        private System.Windows.Forms.PictureBox pictEsquerda;
        private System.Windows.Forms.Timer tmrReturn;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblSelecioneAbaixoOpcao;
        private System.Windows.Forms.ColumnHeader colVEICOM;
        private System.Windows.Forms.Button butIdioma;
        private System.Windows.Forms.Button butVoltar;
        private System.Windows.Forms.Button butConfirmar;
    }
}