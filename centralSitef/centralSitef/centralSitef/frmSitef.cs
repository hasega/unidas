﻿// Aplicação: Unidas Totem
// Data     : 01/10/2015
// Versão   : 1.0.4
// Empresa  : Unidas Rent a Car
// Equipe   : Unidas Desenvolvimento
// Objetivo : Modulo separado para pagamento SITEF
// Histórico: 
//      01/10/2015: Inicio de desenvolvimento para acesso geral
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Xml;
using centralSitef.centralUnidasWCF;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace centralSitef
{
    public partial class frmSitef : Form
    {
        [DllImport(@"C:/SitefDLL/CliSiTef32I.dll", EntryPoint = "ConfiguraIntSiTefInterativoEx")]
        [return: MarshalAs(UnmanagedType.Error)]
        private static extern int ConfiguraIntSiTefInterativoEx(string IPSiTef, string IdLoja, string IdTerminal, short Reservado, string ParamAdic);

        [DllImport(@"C:/SitefDLL/CliSiTef32I.dll", EntryPoint = "VerificaPresencaPinPad")]
        [return: MarshalAs(UnmanagedType.Error)]
        private static extern int VerificaPresencaPinPad();

        [DllImport(@"C:/SitefDLL/CliSiTef32I.dll", EntryPoint = "IniciaFuncaoAASiTefInterativo")]
        [return: MarshalAs(UnmanagedType.Error)]
        private static extern int IniciaFuncaoAASiTefInterativo(int Funcao, string Valor, string CupomFiscal, string DataFiscal, string HoraFiscal, string Operador, string ParamAdic, string Produtos);

        [DllImport(@"C:/SitefDLL/CliSiTef32I.dll", EntryPoint = "ContinuaFuncaoSiTefInterativo")]
        [return: MarshalAs(UnmanagedType.Error)]
        private static extern int ContinuaFuncaoSiTefInterativo(ref int comando, ref int tipoCampo, ref int TamMin, ref int TamMax, [MarshalAs(UnmanagedType.LPArray, SizeConst = 20000)] byte[] Buffer, int TamBuffer, int Continua);

        [DllImport(@"C:/SitefDLL/CliSiTef32I.dll", EntryPoint = "FinalizaTransacaoSiTefInterativo")]
        [return: MarshalAs(UnmanagedType.Error)]
        private static extern int FinalizaTransacaoSiTefInterativo(int Confirma, string COOOperacao, string DataFiscalOperacao, string HoraOperacao);

        [DllImport(@"C:/SitefDLL/CliSiTef32I.dll", EntryPoint = "EscreveMensagemPermanentePinPad")]
        [return: MarshalAs(UnmanagedType.Error)]
        private static extern int EscreveMensagemPermanentePinPad(string strPermanente);

        static Random _r = new Random();

        public frmSitef()
        {
            InitializeComponent();
        }

        private void frmSitef_Load(object sender, EventArgs e)
        {
            DesabilitaMsgSitef();

            ResetCC();

            ResetSegur();

            Globals.glbInteractSitef = 0;

            int nScreenWidth = this.Width;
            int nScreenWidthSplit = nScreenWidth / 2;

            CultureInfo culture = new CultureInfo("pt-BR");
            lblSelecioneGrupo1.Text = "Valor a ser cobrado: " + string.Format(new CultureInfo("pt-BR"), "{0:C}", Convert.ToDouble(Globals.glbProtecoesValorPre1)) + " *";
            lblSelecioneGrupo1.Refresh();

            tmrSitef.Enabled = false;
            tmrRotina.Enabled = true;
        }

        private void tmrReturn_Tick(object sender, EventArgs e)
        {
            tmrReturn.Enabled = false;
            Application.DoEvents();
            Environment.Exit(20001); // timeout - operacao cancelada
        }

        private void logBeginSitef(string strLoja, string strTerminal, string strCupom, string strReservaProduto)
        {
            string strDataLog = DateTime.Now.ToString("yyyyMMdd");
            string strDataLogAtual = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string strFileName = @"C:\CliSiTef\UnidasTotemLog" + strDataLog.Trim() + ".txt";
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(strFileName, true))
            {
                file.WriteLine(strDataLogAtual + ":");
                file.WriteLine("==== INICIO =====");
                file.WriteLine(" ");
                file.WriteLine("** Loja: " + strLoja);
                file.WriteLine("** Terminal: " + strTerminal);
                file.WriteLine("** Cupom: " + strCupom);
                file.WriteLine("** Reserva: " + strReservaProduto);
                file.WriteLine(" ");
                file.WriteLine("---> Fluxo Sitef (Saida): ");
                file.WriteLine(" ");
                file.Flush();
                file.Close();
            }
        }

        private void logContentSitef(StringBuilder strResultado, string strLoja, string strTerminal, string strCupom, string strReservaProduto)
        {
            var strResult = strResultado.ToString();
            string strDataLog = DateTime.Now.ToString("yyyyMMdd");
            string strDataLogAtual = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string strFileName = @"C:\CliSiTef\UnidasTotemLog" + strDataLog.Trim() + ".txt";
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(strFileName, true))
            {
                file.WriteLine(strResult.ToString());
                file.Flush();
                file.Close();
            }
        }

        private void logExitSitef(string strLoja, string strTerminal, string strCupom, string nTransacaoConfirma, string strReservaProduto)
        {
            string strDataLog = DateTime.Now.ToString("yyyyMMdd");
            string strDataLogAtual = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string strFileName = @"C:\CliSiTef\UnidasTotemLog" + strDataLog.Trim() + ".txt";
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(strFileName, true))
            {
                file.WriteLine(" ");
                file.WriteLine("** nTransacaoConfirma: " + nTransacaoConfirma);
                file.WriteLine("==== FIM ========");
                file.WriteLine(" ");
                file.WriteLine(" ");
                file.Flush();
                file.Close();
            }
        }

        private void ResetSegur()
        {
            Globals.glbSEG_R_ANUM = "";
            Globals.glbSEG_R_ALOJRET = "";
            Globals.glbSEG_PAGITENUM = "";
            Globals.glbSEG_HASH = "";
            Globals.glbSEG_TP_INFO = "";
            Globals.glbSEG_TP_OPER = "";
            Globals.glbSEG_HASH_OUT = "";
            Globals.glbSEG_HASH_NEW = "";
            Globals.glbSEG_RT_OPER = "";
            Globals.glbSEG_Cpf = "";
            Globals.glbSEG_CrtNum = "";
            Globals.glbSEG_MessageText = "";
        }

        private void ResetCC()
        {
            Globals.glbScreenCard = 0;
            Globals.glbCC_R_ANUM = "";
            Globals.glbCC_R_ALOJRET = "";
            Globals.glbCC_CCAITENUM = "";
            Globals.glbCC_CupomFiscal = "";
            Globals.glbCC_ValidadeCupomFiscal = "";
            Globals.glbCC_CartaoInformado = "";
            Globals.glbCC_ValidadeInformado = "";
            Globals.glbCC_SegurancaInformado = "";

        }

        private string CreateString(string Campo)
        {
            string OutputString = "";
            string rndstring;

            for (int i = 0; i < Campo.Length; i++)
            {
                rndstring = GetRandom(1);
                OutputString += rndstring + GetRandom(Convert.ToInt32(rndstring)) + Campo[i];
            }
            return OutputString;
        }

        private string GetRandom(int charnum)
        {
            string OutputRandom = "";
            for (int i = 1; i <= charnum; i++)
            {
                int n = _r.Next(1, 9);
                OutputRandom = OutputRandom + n.ToString();
            }
            return OutputRandom;
        }

        private string Scramble(string CR_NBR, string CR_CSE, string DT_VAL)
        {
            //string DT_VAL; // Data de validade 
            //string CR_CSE; // Código Segurança
            //string CR_NBR; // Número do cartão de credito

            string ST_RETURN;
            string LN_NBR;

            ST_RETURN = "";

            // PRIMEIRO BLOCO DE TAMANHO COM RAND
            LN_NBR = GetRandom(1);
            ST_RETURN = LN_NBR.ToString() + GetRandom(Convert.ToInt32(LN_NBR)).ToString();
            ST_RETURN = ST_RETURN + CR_NBR.Length.ToString().Substring(0, 1);


            //SEGUNDO BLOCO DE TAMANHO COM RAND
            LN_NBR = GetRandom(1);
            ST_RETURN = ST_RETURN + LN_NBR.ToString() + GetRandom(Convert.ToInt32(LN_NBR)).ToString();
            ST_RETURN = ST_RETURN + CR_NBR.Length.ToString().Substring(1, 1);

            // TERCEIRO BLOCO (NUMERO CARTAO)

            ST_RETURN = ST_RETURN + CreateString(CR_NBR);


            // QUARTO BLOCO DE TAMANHO COM RAND (CODIGO DE SEGURANCA)
            LN_NBR = GetRandom(1);
            ST_RETURN = ST_RETURN + LN_NBR.ToString() + GetRandom(Convert.ToInt32(LN_NBR)).ToString();
            ST_RETURN = ST_RETURN + CR_CSE.Length.ToString() + CreateString(CR_CSE);

            // QUINTO BLOCO DE TAMANHO COM RAND (DATA VALIDADE)
            LN_NBR = GetRandom(1);
            ST_RETURN = ST_RETURN + LN_NBR.ToString() + GetRandom(Convert.ToInt32(LN_NBR)).ToString();
            ST_RETURN = ST_RETURN + DT_VAL.Length.ToString() + CreateString(DT_VAL);

            //	INVERTE RETORNO
            char[] charArray = ST_RETURN.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        public string UnScramble(string HASH)
        {

            //string DT_VAL; // Data de validade 
            //string CR_CSE; // Código Segurança
            //string CR_NBR; // Número do cartão de credito

            int RAND;
            string TEMP = "";
            int POS;
            string NBR;
            string RESULT = "";
            int ITEM = 1;
            
            //	INVERTE ENTRADA
            char[] charArray = HASH.ToCharArray();
            Array.Reverse(charArray);
            // HASH = charArray.ToString();
            HASH = new string(charArray);


            while (HASH.Length > 0)
            {
                RAND = Convert.ToInt32(HASH.Substring(0, 1));
                TEMP = HASH.Substring(0, ITEM + Convert.ToInt32(HASH.Substring(0, 1)) + ITEM);
                POS = TEMP.Length;
                NBR = TEMP.Substring(TEMP.Length - 1, 1);
                RESULT = RESULT + NBR.ToString();
                HASH = HASH.Substring(POS, HASH.Length - POS);
            }
            return RESULT;
        }

        private int RotinaSitef()
        {
            int nFinal = 0;
            try
            {
                tmrReturn.Enabled = false;

                habilitaMsgSitef();

                richSitef.Text = "Por favor, aguarde ...";
                richSitef.Refresh();

                short result = 0;
                string strResultado = "";
                string strIniciaFuncao = "";

                string strDataAtual = DateTime.Now.ToString("yyyyMMdd");
                string strHoraAtual = DateTime.Now.ToString("HHmmss");

                string strLoja = "";
                if (!string.IsNullOrEmpty(Globals.glbLOJCODSITE))
                    strLoja = Globals.glbLOJCODSITE.Trim();
                else
                    strLoja = ConfigurationManager.AppSettings["totemLoja"].ToString();

                string strTerminal = ConfigurationManager.AppSettings["totemTerminal"].ToString();

                string strCupom = "";

                Random rnd = new Random();
                int nCupom = rnd.Next(100000000);
                strCupom = nCupom.ToString("D10");
                string appPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                string configFile = System.IO.Path.Combine(appPath, "CentralUnidas.exe.config");
                ExeConfigurationFileMap configFileMap = new ExeConfigurationFileMap();
                configFileMap.ExeConfigFilename = configFile;
                System.Configuration.Configuration config = ConfigurationManager.OpenMappedExeConfiguration(configFileMap, ConfigurationUserLevel.None);

                config.AppSettings.Settings["totemCupom"].Value = strCupom;
                config.AppSettings.Settings["totemCupomData"].Value = strDataAtual;
                config.AppSettings.Settings["totemCupomHora"].Value = strHoraAtual;
                config.Save();

                // AQUI ambiente de homologação ok
                // int retorno = ConfiguraIntSiTefInterativoEx(string.Format("{0}", "10.2.0.27"), string.Format("{0}", strLoja), string.Format("{0}", strTerminal), result, string.Format("{0}", "[MultiplosCupons=1;PortaPinPad=03;]"));

                int nPorta = Convert.ToInt32(Globals.glbPortaPinPad.Trim());
                string strPortaDinamica = string.Format("[MultiplosCupons=1;PortaPinPad={0:D2};]", nPorta);

                int retorno = 0;

                byte[] _ipSitef = Encoding.ASCII.GetBytes("192.168.51.81:4096\0");
                byte[] _lojaSitefUnidas = Encoding.ASCII.GetBytes(strLoja + "\0");
                byte[] _strTerminal = Encoding.ASCII.GetBytes(strTerminal + "\0");
                byte[] _strPortaDinamica = Encoding.ASCII.GetBytes(strPortaDinamica + "\0");

                retorno = ConfiguraIntSiTefInterativoEx(string.Format("{0}", "192.168.51.81:4096"), string.Format("{0}", strLoja), string.Format("{0}", strTerminal), result, strPortaDinamica);

                int nMsg = 0;
                switch (retorno)
                {
                    case 0:
                        strResultado = "Não ocorreu erro";
                        break;
                    case 1:
                        strResultado = "Endereço IP inválido ou não resolvido";
                        break;
                    case 2:
                        strResultado = "Código da loja inválido";
                        break;
                    case 3:
                        strResultado = "Código de terminal inválido";
                        break;
                    case 6:
                        strResultado = "Erro na inicialização do Tcp/Ip";
                        break;
                    case 7:
                        strResultado = "Falta de memória";
                        break;
                    case 8:
                        strResultado = "Não encontrou a CliSiTef ou ela está com problemas";
                        break;
                    case 9:
                        strResultado = "Configuração de servidores SiTef foi excedida.";
                        break;
                    case 10:
                        strResultado = "Erro de acesso na pasta CliSiTef (possível falta de permissão para escrita)";
                        break;
                    case 11:
                        strResultado = "Dados inválidos passados pela automação.";
                        break;
                    case 12:
                        strResultado = "Modo seguro não ativo (possível falta de configuração no servidor SiTef do arquivo .cha).";
                        break;
                    case 13:
                        strResultado = "Caminho DLL inválido (o caminho completo das bibliotecas está muito grande).";
                        break;
                    default:
                        strResultado = "Não ocorreu erro";
                        break;
                }

                if (retorno != 0)
                {
                    Environment.Exit(20002); // Retorna com problema de configuração : operacao cancelada
                }

                int pinpad = VerificaPresencaPinPad();

                if (pinpad != 1)
                {
                    Environment.Exit(20003); // Retorna com problema de pinpad : operacao cancelada
                }

                string strMensagemPinPad = ". UNIDAS TOTEM .|";
                byte[] _strPermanente = Encoding.ASCII.GetBytes(strMensagemPinPad + "\0");

                int nPerm = 0;
                nPerm = EscreveMensagemPermanentePinPad(strMensagemPinPad);

                string strValorReal = ConfigurationManager.AppSettings["valorReal"].ToString();
                int intIniciaFuncao = 0;

                string strReservaProduto = "";
                decimal nValorPin = Convert.ToDecimal(Globals.glbProtecoesValorPre1);
                string strValorPin = string.Format(new CultureInfo("pt-BR"), "{0:0.00}", nValorPin);

                if (Convert.ToInt32(strValorReal) == 2)
                {
                    strReservaProduto = string.Format("[PRE{0};{1};{2};{3}]", Globals.glbReservaNumeroPesquisa, "001", "1", "1,00");
                }
                else
                {
                    strValorPin = strValorPin.Replace(".", "");
                    strReservaProduto = string.Format("[PRE{0};{1};{2};{3}]", Globals.glbReservaNumeroPesquisa, "001", "1", strValorPin);
                }

                intIniciaFuncao = IniciaFuncaoAASiTefInterativo(115, strValorPin, strCupom, strDataAtual, strHoraAtual, "UNIDAS", "", strReservaProduto);

                switch (intIniciaFuncao)
                {
                    case 0:
                        strIniciaFuncao = "Sucesso na execução da função.";
                        break;
                    case 10000:
                        strIniciaFuncao = "Deve ser chamada a rotina de continuidade do processo.";
                        break;
                    case -1:
                        strIniciaFuncao = "Módulo não inicializado. O PDV tentou chamar alguma rotina sem antes executar a função configura.";
                        break;
                    case -2:
                        strIniciaFuncao = "Operação cancelada pelo operador.";
                        break;
                    case -3:
                        strIniciaFuncao = "O parâmetro função / modalidade é inválido.";
                        break;
                    case -4:
                        strIniciaFuncao = "Falta de memória no PDV.";
                        break;
                    case -5:
                        strIniciaFuncao = "Sem comunicação com o SiTef.";
                        break;
                    case -6:
                        strIniciaFuncao = "Operação cancelada pelo usuário (no pinpad).";
                        break;
                    case -7:
                        strIniciaFuncao = "Reservado";
                        break;
                    case -8:
                        strIniciaFuncao = "A CliSiTef não possui a implementação da função necessária, provavelmente está desatualizada (a CliSiTefI é mais recente).";
                        break;
                    case -9:
                        strIniciaFuncao = "A automação chamou a rotina ContinuaFuncaoSiTefInterativo sem antes iniciar uma função iterativa.";
                        break;
                    case -10:
                        strIniciaFuncao = "Algum parâmetro obrigatório não foi passado pela automação comercial.";
                        break;
                    case -12:
                        strIniciaFuncao = "Erro na execução da rotina iterativa. Provavelmente o processo iterativo anterior não foi finalizado até o final (enquanto o retorno for igual a 10000).";
                        break;
                    case -15:
                        strIniciaFuncao = "Operação cancelada pela automação comercial.";
                        break;
                    case -20:
                        strIniciaFuncao = "Parâmetro inválido passado para a função.";
                        break;
                    case -21:
                        strIniciaFuncao = "Utilizada uma palavra proibida, por exemplo SENHA, para coletar dados em aberto no pinpad. Por exemplo na função ObtemDadoPinpadDiretoEx.";
                        break;
                    case -25:
                        strIniciaFuncao = "Erro no Correspondente bancário: Deve realizar sangria.";
                        break;
                    case -30:
                        strIniciaFuncao = "Erro de acesso ao arquivo. Certifique-se que o usuário que roda a aplicação tem direitos de leitura/escrita.";
                        break;
                    case -40:
                        strIniciaFuncao = "Transação negada pelo SiTef.";
                        break;
                    case -41:
                        strIniciaFuncao = "Dados inválidos.";
                        break;
                    case -42:
                        strIniciaFuncao = "Reservado";
                        break;
                    case -43:
                        strIniciaFuncao = "Problema na execução de alguma das rotinas no pinpad.";
                        break;
                    case -50:
                        strIniciaFuncao = "Transação não segura.";
                        break;
                    case -100:
                        strIniciaFuncao = "Erro interno do módulo.";
                        break;
                }

                Globals.glbMensagemSitef = strIniciaFuncao;
                string strMensagemSitef = "";

                if (intIniciaFuncao < 0)
                {
                    Environment.Exit(20004); // Problema para iniciar a função : Operação cancelada
                }

                int nContinua = 10000;
                int intComand = 0;
                int intTipoCampo = 0;
                int intTamMinimo = 0;
                int intTamMaximo = 0;
                int intContinua = 0;

                byte[] byteBuffer = new byte[20000];

                StringBuilder resultByteFinal = new StringBuilder();

                bool bCancelaTransacao = false;
                bool bApagaTeclado = false;
                Globals.glbInteractSitef = 0;
                Globals.glbTimeoutSitef = 0;

                logBeginSitef(strLoja, strTerminal, strCupom, strReservaProduto);

                while (nContinua == 10000)
                {
                    Application.DoEvents();

                    string resultByte = "";
                    string strMensagemVisor = "";

                    nContinua = ContinuaFuncaoSiTefInterativo(ref intComand, ref intTipoCampo, ref intTamMinimo, ref intTamMaximo, byteBuffer, byteBuffer.Length, intContinua);

                    if (Globals.glbTimeoutSitef < 0 || Globals.glbInteractSitef == 1)
                    {
                        bCancelaTransacao = true;
                        strMensagemSitef = "Operação cancelada.";
                        nContinua = -1;
                    }

                    if (nContinua < 0)
                    {
                        break;
                    }

                    if (nContinua == 10000)
                    {
                        try
                        {
                            if (byteBuffer != null)
                            {
                                if (byteBuffer[0] != 0x00)
                                {
                                    var str = System.Text.Encoding.Default.GetString(byteBuffer);
                                    string strMensagemGrava = str.ToString();

                                    strMensagemGrava = strMensagemGrava.Replace("\0", "");
                                    resultByte = strMensagemGrava.Trim();
                                    resultByteFinal.Append("Continua:" + nContinua.ToString() + " intComand:" + intComand.ToString() + " intTipoCampo:" + intTipoCampo.ToString() + " intContinua:" + intContinua.ToString() + " " + strMensagemGrava.Trim());
                                    logContentSitef(resultByteFinal, strLoja, strTerminal, strCupom, strReservaProduto);
                                    resultByteFinal.Clear();
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                        }

                        if (intComand == 3)
                        {
                            try
                            {
                                strMensagemVisor = resultByte.ToString().Trim();
                                richSitef.Text = strMensagemVisor;
                                richSitef.Refresh();

                                tmrSitef.Enabled = false;
                                tmrSitef.Interval = 60000;
                                tmrSitef.Enabled = true;
                            }
                            catch
                            {

                            }

                            System.Threading.Thread.Sleep(100);
                            string strConteudo = resultByte.ToString();
                            if (strConteudo == "SOLICITE A SENHA")
                            {
                                bApagaTeclado = true;
                            }
                        }

                        if (intComand == 13 && bApagaTeclado)
                        {
                            pictTec_TECLADO.Visible = false;
                            pictTec_TECLADO.Refresh();
                        }

                        if (intComand == 34)
                        {
                            Globals.glbInteractSitef = 0;
                        }

                        if (intComand == 22)
                        {
                            strMensagemVisor = resultByte.Trim();

                            tmrSitef.Enabled = false;
                            tmrSitef.Interval = 60000;
                            tmrSitef.Enabled = true;

                            if (strMensagemVisor.Trim().Contains("HOMOLOGA"))
                                strMensagemVisor = "";

                            richSitef.Text = strMensagemVisor.Trim();
                            richSitef.Refresh();
                            System.Threading.Thread.Sleep(2000);
                        }
                    }

                    if (intComand == 21)
                    {
                        byteBuffer = Encoding.ASCII.GetBytes("1\0");
                    }
                    else
                    {
                        if (intComand == 34)
                        {
                            string strValorAux = Globals.glbProtecoesValorPre1.Trim() + "\0";
                            byteBuffer = Encoding.ASCII.GetBytes(strValorAux);
                        }
                        else
                        {
                            System.Threading.Thread.Sleep(150);
                            byteBuffer = new byte[20000];
                            System.Threading.Thread.Sleep(150);
                        }
                    }

                    //if (resultByte.ToString().Contains("Transacao nao aceita"))
                    //{
                    //    bCancelaTransacao = true;
                    //    nContinua = -9999;
                    //    break;
                    //}

                    if (intTipoCampo == 121)
                    {
                        Globals.glbCupomFiscal1 = resultByte.ToUpper().Trim();

                        StringBuilder sCupomBuild = new StringBuilder();
                        sCupomBuild.Append(Globals.glbCupomFiscal1);

                        string strCupomString = "";
                        strCupomString = sCupomBuild.ToString();
                        if (strCupomString.ToUpper().Contains("REDE")) Globals.glbBandeiraCartao = "RED";
                        if (strCupomString.ToUpper().Contains("AMERICAN")) Globals.glbBandeiraCartao = "AMX";
                        if (strCupomString.ToUpper().Contains("CIELO")) Globals.glbBandeiraCartao = "CIE";
                        if (strCupomString.ToUpper().Contains("DINERS")) Globals.glbBandeiraCartao = "DNR";
                        if (strCupomString.ToUpper().Contains("ELO")) Globals.glbBandeiraCartao = "ELO";
                        if (strCupomString.ToUpper().Contains("MASTERCARD")) Globals.glbBandeiraCartao = "MAS";
                        if (strCupomString.ToUpper().Contains("VISA")) Globals.glbBandeiraCartao = "VIS";

                        int nLinha = 0;
                        bool bCartao = false;
                        int nLinhaGeral = sCupomBuild.ToString().Split(System.Environment.NewLine.ToCharArray()).Length;
                        if (nLinhaGeral > 0)
                        {
                            string[] strLinhaCupom = sCupomBuild.ToString().Split(System.Environment.NewLine.ToCharArray());
                            foreach (string lin in strLinhaCupom)
                            {
                                nLinha++;
                                if (lin.Trim().ToUpper().Contains("CODIGO PRE-AUTORIZACAO"))
                                {
                                    string strCodigoPreTemp = "";
                                    strCodigoPreTemp = lin.Trim();
                                    strCodigoPreTemp = strCodigoPreTemp.Replace("CODIGO PRE-AUTORIZACAO:", "");
                                    Globals.glbCC_CodigoPreAutorizacao = strCodigoPreTemp.Trim();
                                }

                                if (lin.Trim().ToUpper().Contains("AUTORIZACAO EMISSOR:"))
                                {
                                    string strAutorizacaoTemp = "";
                                    strAutorizacaoTemp = lin.Trim();
                                    strAutorizacaoTemp = strAutorizacaoTemp.Replace("AUTORIZACAO EMISSOR:", "");
                                    Globals.glbCC_CodigoAutorizacaoEmissor = strAutorizacaoTemp.Trim();
                                }

                                if (lin.Trim().ToUpper().Contains("CARTAO"))
                                {
                                    bCartao = true;
                                    string strCartaoTemp = "";
                                    strCartaoTemp = lin.Trim();
                                    strCartaoTemp = strCartaoTemp.Replace("CARTAO", "");
                                    strCartaoTemp = strCartaoTemp.Replace(":", "");
                                    strCartaoTemp = strCartaoTemp.Replace("x", "");
                                    strCartaoTemp = strCartaoTemp.Replace("X", "");
                                    strCartaoTemp = strCartaoTemp.Replace("*", "");
                                    strCartaoTemp = strCartaoTemp.Replace(".", "");
                                    strCartaoTemp = strCartaoTemp.Replace(" ", "");

                                    if (strCartaoTemp.Trim().Length > 4)
                                        strCartaoTemp = strCartaoTemp.Trim().Substring(0, 4);
                                    else
                                        strCartaoTemp = strCartaoTemp.Trim();

                                    Globals.glbCC_CupomFiscal = strCartaoTemp.Trim();
                                    Globals.glbCC_ValidadeCupomFiscal = "";
                                }
                                if (lin.Trim().ToUpper().Contains("VAL:") && bCartao)
                                {
                                    string strValidTemp = "";
                                    if (lin.Trim().Length > 24)
                                        strValidTemp = lin.Trim().Substring(lin.Trim().Length - 5, 5);
                                    if (!string.IsNullOrEmpty(strValidTemp))
                                    {
                                        strValidTemp = strValidTemp.Replace("VAL", "");
                                        strValidTemp = strValidTemp.Replace(":", "");
                                        strValidTemp = strValidTemp.Replace(" ", "");
                                        Globals.glbCC_ValidadeCupomFiscal = strValidTemp.Substring(3, 2) + strValidTemp.Substring(0, 2);
                                    }
                                    else
                                        Globals.glbCC_ValidadeCupomFiscal = "";
                                }
                            }
                        }
                        else
                        {
                            Globals.glbScreenCard = 1;
                        }
                    }

                    if (intTipoCampo == 122)
                    {
                        Globals.glbCupomFiscal2 = resultByte.ToUpper().Trim();
                    }

                    if (intComand == 22 && intTipoCampo < 0)
                    {
                        if (!resultByte.ToUpper().Trim().Contains("HOMOLOGACAO"))
                        {
                            strMensagemSitef = resultByte.ToUpper().Trim() + " " + strMensagemSitef;
                        }
                        if (nContinua == 255)
                        {
                            bCancelaTransacao = true;
                            break;
                        }
                    }
                }

                Globals.glbPopupSitef = 0;

                if (tmrReturn.Enabled == true)
                {
                    tmrReturn.Enabled = false;
                }

                int nTransacaoConfirma = 1;
                Thread.Sleep(300);
                if (nContinua < 0) nTransacaoConfirma = 0;

                if (bCancelaTransacao)
                {
                    strMensagemSitef = strMensagemSitef.Replace("Homologacao CliSiTef", "");
                    strMensagemSitef = strMensagemSitef + Environment.NewLine + "Por favor, dirija-se ao balcão para finalizar sua reserva.";
                    Globals.glbMensagemSitef = strMensagemSitef;
                    Globals.glbMensagem = 10000;
                    nTransacaoConfirma = 0;
                }

                byte[] _COOOperacao = Encoding.ASCII.GetBytes(strCupom + "\0");
                byte[] _DataFiscalOperacao = Encoding.ASCII.GetBytes(strDataAtual + "\0");
                byte[] _HoraOperacao = Encoding.ASCII.GetBytes(strHoraAtual + "\0");

                nFinal = FinalizaTransacaoSiTefInterativo(nTransacaoConfirma, strCupom, strDataAtual, strHoraAtual);
                Thread.Sleep(500);

                logExitSitef(strLoja, strTerminal, strCupom, nTransacaoConfirma.ToString(), strReservaProduto);

                if (nContinua < 0) nFinal = (int)nContinua;

                if (bCancelaTransacao)
                {
                    nFinal = 2;
                }

                if (nTransacaoConfirma == 0)
                {
                    nFinal = 2;
                }
                else
                    nFinal = 1;

                // Verifica cartao anterior
                if (nFinal == 1)
                {

                    richSitef.Text = "Por favor, aguarde ...";
                    richSitef.Refresh();
                    pictLoad.Visible = true;

                    Application.DoEvents();

                    CentralUnidasClassClient clientWcf = new CentralUnidasClassClient();

                    if ((!string.IsNullOrEmpty(Globals.glbCupomFiscal1.Trim())) && (!string.IsNullOrEmpty(Globals.glbCupomFiscal2.Trim())))
                    {
                        string strRetornoCupom = clientWcf.SetCupomFiscal(
                            Convert.ToInt32(Globals.glbReservaNumeroPesquisa),
                            0,
                            Globals.glbCupomFiscal1.Trim(),
                            Globals.glbCupomFiscal2.Trim(),
                            Globals.glbCC_CodigoPreAutorizacao,
                            Globals.glbCC_CodigoAutorizacaoEmissor);
                    }

                    string xmlRetornoWCF = clientWcf.GetContratoTotem(Globals.glbRESDOCCLI.Trim().Substring(0, 11));
                    if (string.IsNullOrEmpty(xmlRetornoWCF) || (xmlRetornoWCF.Trim() == "<UNIDASWCF />"))
                    {
                        Globals.glbScreenCard = 1;
                    }
                    else
                    {
                        int iRetContrato = obtemContratoXMLviaWCF(xmlRetornoWCF);
                        if (iRetContrato == 0)
                        {
                            Globals.glbScreenCard = 1;
                        }
                        else
                        {
                            // Verifica Rotina Cartao
                            xmlRetornoWCF = "";
                            string pR_ANUM = Globals.glbCC_R_ANUM.Trim();
                            string pR_ALOJRET = Globals.glbCC_R_ALOJRET;
                            int pPAGITENUM = 1;
                            string pHASH = ""; ;
                            string pTP_INFO = "A";
                            string pTP_OPER = "C";
                            string pHASH_OUT = "";
                            string pRT_OPER = "";
                            string pCpf = Globals.glbRESDOCCLI.Trim().Substring(0, 11);
                            xmlRetornoWCF = clientWcf.GetConsultaTotem(pR_ANUM, pR_ALOJRET, pPAGITENUM, pHASH, pTP_INFO, pTP_OPER, pHASH_OUT, pRT_OPER, pCpf);
                            if (string.IsNullOrEmpty(xmlRetornoWCF))
                            {
                                Globals.glbScreenCard = 1;
                            }
                            else
                            {
                                Globals.glbSEG_HASH_OUT = xmlRetornoWCF.Trim();
                                if (!string.IsNullOrEmpty(Globals.glbSEG_HASH_OUT))
                                {
                                    string hash_output;
                                    hash_output = UnScramble(Globals.glbSEG_HASH_OUT.Trim());
                                    int nLenCartaoSitef = Globals.glbCC_CupomFiscal.Trim().Length;
                                    if (nLenCartaoSitef > 0)
                                    {
                                        string strFinalCartaoBanco = hash_output.Trim();
                                        string strFinalCartaoBancoSitef = Globals.glbCC_CupomFiscal.Substring(nLenCartaoSitef - 4, 4);
                                        if (strFinalCartaoBanco.Trim() != strFinalCartaoBancoSitef.Trim())
                                        {
                                            Globals.glbScreenCard = 1;
                                        }
                                    }
                                    else
                                        Globals.glbScreenCard = 1;
                                }
                                else
                                {
                                    Globals.glbScreenCard = 1;
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                nFinal = 2;
            }
            string strMensagemPinPad1 = ". UNIDAS TOTEM .|";
            byte[] _strPermanente1 = Encoding.ASCII.GetBytes(strMensagemPinPad1 + "\0");
            int nPerm1 = EscreveMensagemPermanentePinPad(strMensagemPinPad1);
            return nFinal;
        }

        private int obtemContratoXMLviaWCF(string xmlEncodedList)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xmlEncodedList);

                string strR_ANUM = "";
                string strR_ALOJRET = "";
                string strCCAITENUM = "";

                XmlNodeList xml1 = xmlDoc.GetElementsByTagName("R_ANUM");
                if (xml1.Count > 0)
                {
                    for (int x = 0; x < xml1.Count; x++)
                    {
                        if (!string.IsNullOrEmpty(xml1.Item(x).InnerXml))
                        {
                            strR_ANUM = xml1.Item(x).InnerXml;
                            Globals.glbCC_R_ANUM = strR_ANUM.Trim();
                        }
                    }
                }

                XmlNodeList xml2 = xmlDoc.GetElementsByTagName("R_ALOJRET");
                if (xml2.Count > 0)
                {
                    for (int x = 0; x < xml2.Count; x++)
                    {
                        if (!string.IsNullOrEmpty(xml2.Item(x).InnerXml))
                        {
                            strR_ALOJRET = xml2.Item(x).InnerXml;
                            Globals.glbCC_R_ALOJRET = strR_ALOJRET.Trim();
                        }
                    }
                }

                XmlNodeList xml3 = xmlDoc.GetElementsByTagName("CCAITENUM");
                if (xml3.Count > 0)
                {
                    for (int x = 0; x < xml3.Count; x++)
                    {
                        if (!string.IsNullOrEmpty(xml3.Item(x).InnerXml))
                        {
                            strCCAITENUM = xml3.Item(x).InnerXml;
                            Globals.glbCC_CCAITENUM = strCCAITENUM.Trim();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return 1;
        }

        private void habilitaMsgSitef()
        {
            panelSitef.Visible = true;
            panelSitef.Refresh();

            labelTransacao.Visible = true;
            richSitef.Visible = true;
            pictTec_TECLADO.Visible = true;
            tmrSitef.Enabled = true;
        }

        private void DesabilitaMsgSitef()
        {
            label1.Visible = true;
            label2.Visible = true;

            panelSitef.Visible = false;
            panelSitef.Refresh();

            label1.Refresh();
            label2.Refresh();
        }

        private void tmrSitef_Tick(object sender, EventArgs e)
        {
            Application.DoEvents();
            Globals.glbTimeoutSitef = -1;
        }

        private void richSitef_TextChanged(object sender, EventArgs e)
        {
            if (tmrReturn.Enabled)
            {
                tmrReturn.Enabled = false;
            }
            tmrReturn.Enabled = true;
            Application.DoEvents();
        }

        private void pictTec_TECLADO_Click(object sender, EventArgs e)
        {
            Globals.glbInteractSitef = 1;
        }

        private void tmrRotina_Tick(object sender, EventArgs e)
        {
            tmrRotina.Enabled = false;

            int nSitef = RotinaSitef();
            if (nSitef == 1)
            {
                if (Globals.glbScreenCard == 1)
                {
                    Environment.Exit(30002); // pede cartao
                }
                else
                {
                    Environment.Exit(30001); // fecha RA
                }
            }
            else
            {
                if (Globals.glbMensagem == 10000)
                {
                    Globals.glbScreen = 1;
                    frmMensagem frmMsg0201 = new frmMensagem();
                    frmMsg0201.Show();
                    this.Close();
                    this.Dispose();
                }
                else
                {
                    Environment.Exit(10001); // operacao cancelada direta
                }
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void pictLoad_Click(object sender, EventArgs e)
        {

        }
    }
}
