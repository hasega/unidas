﻿namespace centralSitef
{
    partial class frmSitef
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSitef));
            this.tmrRotina = new System.Windows.Forms.Timer(this.components);
            this.pictLoad = new System.Windows.Forms.PictureBox();
            this.labelTransacao = new System.Windows.Forms.Label();
            this.richSitef = new System.Windows.Forms.RichTextBox();
            this.pictTec_TECLADO = new System.Windows.Forms.PictureBox();
            this.panelSitef = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tmrSitef = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.lblSelecioneGrupo1 = new System.Windows.Forms.Label();
            this.tmrReturn = new System.Windows.Forms.Timer(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictLang = new System.Windows.Forms.PictureBox();
            this.pictUnidasLogo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictLoad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_TECLADO)).BeginInit();
            this.panelSitef.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictLang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // tmrRotina
            // 
            this.tmrRotina.Interval = 500;
            this.tmrRotina.Tick += new System.EventHandler(this.tmrRotina_Tick);
            // 
            // pictLoad
            // 
            this.pictLoad.BackColor = System.Drawing.Color.Transparent;
            this.pictLoad.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictLoad.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.pictLoad.Image = global::centralSitef.Properties.Resources.Loading;
            this.pictLoad.Location = new System.Drawing.Point(630, 89);
            this.pictLoad.Name = "pictLoad";
            this.pictLoad.Size = new System.Drawing.Size(61, 55);
            this.pictLoad.TabIndex = 187;
            this.pictLoad.TabStop = false;
            this.pictLoad.Visible = false;
            this.pictLoad.Click += new System.EventHandler(this.pictLoad_Click);
            // 
            // labelTransacao
            // 
            this.labelTransacao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTransacao.BackColor = System.Drawing.Color.White;
            this.labelTransacao.Font = new System.Drawing.Font("Arial", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTransacao.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.labelTransacao.Location = new System.Drawing.Point(30, 34);
            this.labelTransacao.Name = "labelTransacao";
            this.labelTransacao.Size = new System.Drawing.Size(468, 40);
            this.labelTransacao.TabIndex = 200;
            this.labelTransacao.Text = "Transação disponível: Crédito";
            this.labelTransacao.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // richSitef
            // 
            this.richSitef.BackColor = System.Drawing.Color.White;
            this.richSitef.Font = new System.Drawing.Font("Arial", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richSitef.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.richSitef.Location = new System.Drawing.Point(36, 89);
            this.richSitef.Name = "richSitef";
            this.richSitef.ReadOnly = true;
            this.richSitef.Size = new System.Drawing.Size(593, 165);
            this.richSitef.TabIndex = 199;
            this.richSitef.Text = "";
            this.richSitef.TextChanged += new System.EventHandler(this.richSitef_TextChanged);
            // 
            // pictTec_TECLADO
            // 
            this.pictTec_TECLADO.BackColor = System.Drawing.Color.Transparent;
            this.pictTec_TECLADO.Image = global::centralSitef.Properties.Resources.Cancelar;
            this.pictTec_TECLADO.Location = new System.Drawing.Point(690, 89);
            this.pictTec_TECLADO.Name = "pictTec_TECLADO";
            this.pictTec_TECLADO.Size = new System.Drawing.Size(177, 55);
            this.pictTec_TECLADO.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictTec_TECLADO.TabIndex = 197;
            this.pictTec_TECLADO.TabStop = false;
            this.pictTec_TECLADO.Click += new System.EventHandler(this.pictTec_TECLADO_Click);
            // 
            // panelSitef
            // 
            this.panelSitef.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelSitef.Controls.Add(this.pictLoad);
            this.panelSitef.Controls.Add(this.labelTransacao);
            this.panelSitef.Controls.Add(this.richSitef);
            this.panelSitef.Controls.Add(this.pictTec_TECLADO);
            this.panelSitef.Location = new System.Drawing.Point(291, 275);
            this.panelSitef.Name = "panelSitef";
            this.panelSitef.Size = new System.Drawing.Size(940, 426);
            this.panelSitef.TabIndex = 196;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Location = new System.Drawing.Point(355, 212);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(683, 1);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 195;
            this.pictureBox1.TabStop = false;
            // 
            // tmrSitef
            // 
            this.tmrSitef.Interval = 60000;
            this.tmrSitef.Tick += new System.EventHandler(this.tmrSitef_Tick);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(422, 714);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(711, 24);
            this.label1.TabIndex = 192;
            this.label1.Text = "* Este valor será pré-autorizado em seu cartão como garantia da locação. ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSelecioneGrupo1
            // 
            this.lblSelecioneGrupo1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSelecioneGrupo1.BackColor = System.Drawing.Color.White;
            this.lblSelecioneGrupo1.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSelecioneGrupo1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.lblSelecioneGrupo1.Location = new System.Drawing.Point(0, 234);
            this.lblSelecioneGrupo1.Name = "lblSelecioneGrupo1";
            this.lblSelecioneGrupo1.Size = new System.Drawing.Size(1366, 29);
            this.lblSelecioneGrupo1.TabIndex = 191;
            this.lblSelecioneGrupo1.Text = "Valor a ser cobrado: R$";
            this.lblSelecioneGrupo1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tmrReturn
            // 
            this.tmrReturn.Enabled = true;
            this.tmrReturn.Interval = 120000;
            this.tmrReturn.Tick += new System.EventHandler(this.tmrReturn_Tick);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Arial", 26F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.label3.Location = new System.Drawing.Point(0, 150);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(1366, 54);
            this.label3.TabIndex = 194;
            this.label3.Text = "Pré-autorização de locação";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Gray;
            this.label2.Location = new System.Drawing.Point(300, 740);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1052, 24);
            this.label2.TabIndex = 193;
            this.label2.Text = "Após a devolução do veículo, caso não haja avarias, o valor será liberado novamen" +
    "te em seu limite de crédito.";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox8.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox8.Image = global::centralSitef.Properties.Resources.Totem_Traco;
            this.pictureBox8.Location = new System.Drawing.Point(355, 207);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(683, 1);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox8.TabIndex = 190;
            this.pictureBox8.TabStop = false;
            // 
            // pictLang
            // 
            this.pictLang.Image = ((System.Drawing.Image)(resources.GetObject("pictLang.Image")));
            this.pictLang.Location = new System.Drawing.Point(1148, 0);
            this.pictLang.Name = "pictLang";
            this.pictLang.Size = new System.Drawing.Size(148, 61);
            this.pictLang.TabIndex = 189;
            this.pictLang.TabStop = false;
            // 
            // pictUnidasLogo
            // 
            this.pictUnidasLogo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictUnidasLogo.BackColor = System.Drawing.Color.Transparent;
            this.pictUnidasLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictUnidasLogo.Image = global::centralSitef.Properties.Resources.totem_header;
            this.pictUnidasLogo.Location = new System.Drawing.Point(0, 0);
            this.pictUnidasLogo.Name = "pictUnidasLogo";
            this.pictUnidasLogo.Size = new System.Drawing.Size(1366, 140);
            this.pictUnidasLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictUnidasLogo.TabIndex = 188;
            this.pictUnidasLogo.TabStop = false;
            // 
            // frmSitef
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(15F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1362, 764);
            this.ControlBox = false;
            this.Controls.Add(this.panelSitef);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblSelecioneGrupo1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictLang);
            this.Controls.Add(this.pictUnidasLogo);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSitef";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CAU - Unidas";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmSitef_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictLoad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictTec_TECLADO)).EndInit();
            this.panelSitef.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictLang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer tmrRotina;
        private System.Windows.Forms.PictureBox pictLoad;
        private System.Windows.Forms.Label labelTransacao;
        private System.Windows.Forms.RichTextBox richSitef;
        private System.Windows.Forms.PictureBox pictTec_TECLADO;
        private System.Windows.Forms.Panel panelSitef;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer tmrSitef;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblSelecioneGrupo1;
        private System.Windows.Forms.Timer tmrReturn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictLang;
        private System.Windows.Forms.PictureBox pictUnidasLogo;
    }
}

