﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace centralSitef
{
    public partial class frmMensagem : Form
    {
        public frmMensagem()
        {
            InitializeComponent();
        }

        private void frmMensagem_Load(object sender, EventArgs e)
        {
            int nScreenWidth = this.Width;
            int nScreenHeight = this.Height;
            int nScreenWidthSplit = nScreenWidth / 2;
            int nScreenHeightSplit = nScreenHeight / 2;

            richMensagem.Width = nScreenWidth - 200;
            richMensagem.Location = new Point((nScreenWidthSplit - (richMensagem.Size.Width / 2)), (nScreenHeightSplit - (richMensagem.Size.Height / 2)));

            this.Refresh();

            Globals.glbDebugMode = Convert.ToInt16(ConfigurationManager.AppSettings.Get("appDebugMode"));

            callMensagem();

            tmrReturn.Enabled = true;
        }

        private void callMensagem()
        {
            int intLanguage = Globals.glbLanguage;
            int intMensagem = Globals.glbMensagem;
            switch (intMensagem)
            {
                case 0201:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0101BR");
                            break;
                        // Espanhol
                        case 2:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0101ES");
                            break;
                        // Ingles
                        case 3:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0101EN");
                            break;
                        default:
                            // Porgues
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0101BR");
                            break;
                    }
                    break;
                case 0202:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0201BR");
                            break;
                        // Espanhol
                        case 2:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0201ES");
                            break;
                        // Ingles
                        case 3:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0201EN");
                            break;
                        default:
                            // Porgues
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0201BR");
                            break;
                    }
                    break;
                case 0203:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0301BR");
                            break;
                        // Espanhol
                        case 2:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0301ES");
                            break;
                        // Ingles
                        case 3:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0301EN");
                            break;
                        default:
                            // Porgues
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0301BR");
                            break;
                    }
                    break;

                case 0204:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0401BR");
                            break;
                        // Espanhol
                        case 2:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0401ES");
                            break;
                        // Ingles
                        case 3:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0401EN");
                            break;
                        default:
                            // Porgues
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0401BR");
                            break;
                    }
                    break;
                case 0205:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0501BR");
                            break;
                        // Espanhol
                        case 2:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0501BR");
                            break;
                        // Ingles
                        case 3:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0501BR");
                            break;
                        default:
                            // Porgues
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0501BR");
                            break;
                    }
                    break;
                case 0206:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0601BR");
                            break;
                        // Espanhol
                        case 2:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0601BR");
                            break;
                        // Ingles
                        case 3:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0601BR");
                            break;
                        default:
                            // Porgues
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0601BR");
                            break;
                    }
                    break;
                case 0207:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0701BR");
                            break;
                        // Espanhol
                        case 2:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0701BR");
                            break;
                        // Ingles
                        case 3:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0701BR");
                            break;
                        default:
                            // Porgues
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0701BR");
                            break;
                    }
                    break;
                case 0301:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen03Msg0101BR");
                            break;
                        // Espanhol
                        case 2:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen03Msg0101ES");
                            break;
                        // Ingles
                        case 3:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen03Msg0101EN");
                            break;
                        default:
                            // Porgues
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen03Msg0101BR");
                            break;
                    }
                    break;
                case 0302:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen03Msg0201BR") + Environment.NewLine + Environment.NewLine;
                            richMensagem.Text += ConfigurationManager.AppSettings.Get("screen03Msg0202BR") + Environment.NewLine;
                            richMensagem.Text += ConfigurationManager.AppSettings.Get("screen03Msg0203BR");
                            break;
                        // Espanhol
                        case 2:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen03Msg0201ES") + Environment.NewLine + Environment.NewLine;
                            richMensagem.Text += ConfigurationManager.AppSettings.Get("screen03Msg0202ES") + Environment.NewLine;
                            richMensagem.Text += ConfigurationManager.AppSettings.Get("screen03Msg0203ES");
                            break;
                        // Ingles
                        case 3:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen03Msg0201EN") + Environment.NewLine + Environment.NewLine;
                            richMensagem.Text += ConfigurationManager.AppSettings.Get("screen03Msg0202EN") + Environment.NewLine;
                            richMensagem.Text += ConfigurationManager.AppSettings.Get("screen03Msg0203EN");
                            break;
                        default:
                            // Porgues
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen03Msg0201BR") + Environment.NewLine + Environment.NewLine;
                            richMensagem.Text += ConfigurationManager.AppSettings.Get("screen03Msg0202BR") + Environment.NewLine;
                            richMensagem.Text += ConfigurationManager.AppSettings.Get("screen03Msg0203BR");
                            break;
                    }
                    break;
                case 0401:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen04Msg0101BR");
                            break;
                        // Espanhol
                        case 2:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen04Msg0101ES");
                            break;
                        // Ingles
                        case 3:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen04Msg0101EN");
                            break;
                        default:
                            // Porgues
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen04Msg0101BR");
                            break;
                    }
                    break;
                case 0601:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen06Msg0101BR");
                            break;
                        // Espanhol
                        case 2:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen06Msg0101ES");
                            break;
                        // Ingles
                        case 3:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen06Msg0101EN");
                            break;
                        default:
                            // Porgues
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen06Msg0101BR");
                            break;
                    }
                    break;
                case 0701:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen07Msg0701BR");
                            break;
                        // Espanhol
                        case 2:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen07Msg0701BR");
                            break;
                        // Ingles
                        case 3:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen07Msg0701BR");
                            break;
                        default:
                            // Porgues
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen07Msg0701BR");
                            break;
                    }
                    break;
                case 1001:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1001BR");
                            break;
                        // Espanhol
                        case 2:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1001ES");
                            break;
                        // Ingles
                        case 3:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1001EN");
                            break;
                        default:
                            // Porgues
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1001BR");
                            break;
                    }
                    break;
                case 1002:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1002BR");
                            break;
                        // Espanhol
                        case 2:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1002ES");
                            break;
                        // Ingles
                        case 3:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1002EN");
                            break;
                        default:
                            // Porgues
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1002BR");
                            break;
                    }
                    break;
                case 1003:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1003BR");
                            break;
                        // Espanhol
                        case 2:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1003ES");
                            break;
                        // Ingles
                        case 3:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1003EN");
                            break;
                        default:
                            // Porgues
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1003BR");
                            break;
                    }
                    break;
                case 1004:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1004BR");
                            break;
                        // Espanhol
                        case 2:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1004ES");
                            break;
                        // Ingles
                        case 3:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1004EN");
                            break;
                        default:
                            // Porgues
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1004BR");
                            break;
                    }
                    break;
                case 1005:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1005BR");
                            break;
                        // Espanhol
                        case 2:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1005ES");
                            break;
                        // Ingles
                        case 3:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1005EN");
                            break;
                        default:
                            // Porgues
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1005BR");
                            break;
                    }
                    break;
                case 1006:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1006BR");
                            break;
                        // Espanhol
                        case 2:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1006ES");
                            break;
                        // Ingles
                        case 3:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1006EN");
                            break;
                        default:
                            // Porgues
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1006BR");
                            break;
                    }
                    break;
                case 1007:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1007BR");
                            break;
                        // Espanhol
                        case 2:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1007ES");
                            break;
                        // Ingles
                        case 3:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1007EN");
                            break;
                        default:
                            // Porgues
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1007BR");
                            break;
                    }
                    break;
                case 1008:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1008BR");
                            break;
                        // Espanhol
                        case 2:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1008ES");
                            break;
                        // Ingles
                        case 3:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1008EN");
                            break;
                        default:
                            // Porgues
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1008BR");
                            break;
                    }
                    break;
                case 1009:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1009BR");
                            break;
                        // Espanhol
                        case 2:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1009ES");
                            break;
                        // Ingles
                        case 3:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1009EN");
                            break;
                        default:
                            // Porgues
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1009BR");
                            break;
                    }
                    break;
                case 1010:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1010BR");
                            break;
                        // Espanhol
                        case 2:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1010ES");
                            break;
                        // Ingles
                        case 3:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1010EN");
                            break;
                        default:
                            // Porgues
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1010BR");
                            break;
                    }
                    break;
                case 1011:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1011BR");
                            break;
                        // Espanhol
                        case 2:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1011ES");
                            break;
                        // Ingles
                        case 3:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1011EN");
                            break;
                        default:
                            // Porgues
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1011BR");
                            break;
                    }
                    break;
                case 1012:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1012BR");
                            break;
                        // Espanhol
                        case 2:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1012ES");
                            break;
                        // Ingles
                        case 3:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1012EN");
                            break;
                        default:
                            // Porgues
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1012BR");
                            break;
                    }
                    break;
                case 1013:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1013BR");
                            break;
                        // Espanhol
                        case 2:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1013ES");
                            break;
                        // Ingles
                        case 3:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1013EN");
                            break;
                        default:
                            // Porgues
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1013BR");
                            break;
                    }
                    break;
                case 1014:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1014BR");
                            break;
                        // Espanhol
                        case 2:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1014ES");
                            break;
                        // Ingles
                        case 3:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1014EN");
                            break;
                        default:
                            // Porgues
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1014BR");
                            break;
                    }
                    break;
                case 1015:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1015BR");
                            break;
                        // Espanhol
                        case 2:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1015ES");
                            break;
                        // Ingles
                        case 3:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1015EN");
                            break;
                        default:
                            // Porgues
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen10Msg1015BR");
                            break;
                    }
                    break;
                case 10000:
                    richMensagem.Text = Globals.glbMensagemSitef;
                    break;
                default:
                    switch (intLanguage)
                    {
                        // Portugues
                        case 1:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0101BR");
                            break;
                        // Espanhol
                        case 2:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0101ES");
                            break;
                        // Ingles
                        case 3:
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0101EN");
                            break;
                        default:
                            // Porgues
                            richMensagem.Text = ConfigurationManager.AppSettings.Get("screen02Msg0101BR");
                            break;
                    }
                    break;
            }
            richMensagem.WordWrap = true;
            richMensagem.SelectionAlignment = HorizontalAlignment.Center;
            richMensagem.Refresh();
            Globals.glbMensagem = 0;
            Globals.glbMensagemSitef = "";
        }

        private void tmrReturn_Tick(object sender, EventArgs e)
        {
            tmrReturn.Enabled = false;

            switch (Globals.glbScreen)
            {
                case 1:
                    Environment.Exit(10001);
                    break;
                case 2:
                    Environment.Exit(10002);
                    break;
                case 3:
                    Environment.Exit(10003);
                    break;
                case 4:
                    Environment.Exit(10004);
                    break;
                case 7:
                    Environment.Exit(10007);
                    break;
                case 8:
                    Environment.Exit(10008);
                    break;
                case 10:
                    Environment.Exit(10010);
                    break;
                case 13:
                    Environment.Exit(10013);
                    break;
                case 14:
                    Environment.Exit(10014);
                    break;
                case 15:
                    Environment.Exit(10015);
                    break;
                default:
                    Environment.Exit(10001);
                    break;
            }
            Globals.glbScreen = 0;
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            switch (Globals.glbScreen)
            {
                case 1:
                    Environment.Exit(10001);
                    break;
                case 2:
                    Environment.Exit(10002);
                    break;
                case 3:
                    Environment.Exit(10003);
                    break;
                case 4:
                    Environment.Exit(10004);
                    break;
                case 7:
                    Environment.Exit(10007);
                    break;
                case 8:
                    Environment.Exit(10008);
                    break;
                case 10:
                    Environment.Exit(10010);
                    break;
                case 13:
                    Environment.Exit(10013);
                    break;
                case 14:
                    Environment.Exit(10014);
                    break;
                case 15:
                    Environment.Exit(10015);
                    break;
                default:
                    Environment.Exit(10001);
                    break;
            }
            Globals.glbScreen = 0;
        }
    }
}
