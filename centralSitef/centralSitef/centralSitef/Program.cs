﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace centralSitef
{
    
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        
        [STAThread]
        static void Main()
        {
            string[] args = Environment.GetCommandLineArgs();
            if (args.Length == 6)
            {
                Globals.glbProtecoesValorPre1 = args[1].ToString().Trim();
                Globals.glbLOJCODSITE = args[2].ToString().Trim();
                Globals.glbPortaPinPad = args[3].ToString().Trim();
                Globals.glbReservaNumeroPesquisa = args[4].ToString().Trim();
                Globals.glbRESDOCCLI = args[5].ToString().Trim();
            }

            if (args.Length != 6)
            {
                Environment.Exit(20000);
            }
            else
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new frmSitef());
            }
        }
    }
}
