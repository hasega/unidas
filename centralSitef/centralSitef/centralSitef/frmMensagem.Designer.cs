﻿namespace centralSitef
{
    partial class frmMensagem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMensagem));
            this.pictUnidasLogo = new System.Windows.Forms.PictureBox();
            this.richMensagem = new System.Windows.Forms.RichTextBox();
            this.tmrReturn = new System.Windows.Forms.Timer(this.components);
            this.pictLinha = new System.Windows.Forms.PictureBox();
            this.btnVoltar = new System.Windows.Forms.Button();
            this.pictLang = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictLinha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictLang)).BeginInit();
            this.SuspendLayout();
            // 
            // pictUnidasLogo
            // 
            this.pictUnidasLogo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictUnidasLogo.BackColor = System.Drawing.Color.Transparent;
            this.pictUnidasLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictUnidasLogo.Image = global::centralSitef.Properties.Resources.totem_header;
            this.pictUnidasLogo.Location = new System.Drawing.Point(0, 0);
            this.pictUnidasLogo.Name = "pictUnidasLogo";
            this.pictUnidasLogo.Size = new System.Drawing.Size(1366, 140);
            this.pictUnidasLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictUnidasLogo.TabIndex = 99;
            this.pictUnidasLogo.TabStop = false;
            // 
            // richMensagem
            // 
            this.richMensagem.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richMensagem.BackColor = System.Drawing.Color.White;
            this.richMensagem.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.richMensagem.Font = new System.Drawing.Font("Arial", 32.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richMensagem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(111)))), ((int)(((byte)(166)))));
            this.richMensagem.Location = new System.Drawing.Point(231, 246);
            this.richMensagem.Name = "richMensagem";
            this.richMensagem.ReadOnly = true;
            this.richMensagem.Size = new System.Drawing.Size(1005, 298);
            this.richMensagem.TabIndex = 98;
            this.richMensagem.Text = "";
            // 
            // tmrReturn
            // 
            this.tmrReturn.Interval = 10000;
            this.tmrReturn.Tick += new System.EventHandler(this.tmrReturn_Tick);
            // 
            // pictLinha
            // 
            this.pictLinha.Image = global::centralSitef.Properties.Resources.totem_biometria_linha0;
            this.pictLinha.Location = new System.Drawing.Point(231, 563);
            this.pictLinha.Name = "pictLinha";
            this.pictLinha.Size = new System.Drawing.Size(1005, 9);
            this.pictLinha.TabIndex = 102;
            this.pictLinha.TabStop = false;
            // 
            // btnVoltar
            // 
            this.btnVoltar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVoltar.Image = global::centralSitef.Properties.Resources.totem_voltar;
            this.btnVoltar.Location = new System.Drawing.Point(595, 645);
            this.btnVoltar.Name = "btnVoltar";
            this.btnVoltar.Size = new System.Drawing.Size(253, 73);
            this.btnVoltar.TabIndex = 101;
            this.btnVoltar.UseVisualStyleBackColor = true;
            this.btnVoltar.Click += new System.EventHandler(this.btnVoltar_Click);
            // 
            // pictLang
            // 
            this.pictLang.Image = ((System.Drawing.Image)(resources.GetObject("pictLang.Image")));
            this.pictLang.Location = new System.Drawing.Point(1149, 2);
            this.pictLang.Name = "pictLang";
            this.pictLang.Size = new System.Drawing.Size(148, 61);
            this.pictLang.TabIndex = 100;
            this.pictLang.TabStop = false;
            // 
            // frmMensagem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1366, 768);
            this.ControlBox = false;
            this.Controls.Add(this.pictUnidasLogo);
            this.Controls.Add(this.richMensagem);
            this.Controls.Add(this.pictLinha);
            this.Controls.Add(this.btnVoltar);
            this.Controls.Add(this.pictLang);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMensagem";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CAU - Unidas";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMensagem_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictUnidasLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictLinha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictLang)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictUnidasLogo;
        private System.Windows.Forms.RichTextBox richMensagem;
        private System.Windows.Forms.Timer tmrReturn;
        private System.Windows.Forms.PictureBox pictLinha;
        private System.Windows.Forms.Button btnVoltar;
        private System.Windows.Forms.PictureBox pictLang;
    }
}