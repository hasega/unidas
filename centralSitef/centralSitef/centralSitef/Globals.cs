﻿// Aplicação: Unidas Totem
// Data     : 01/10/2015
// Versão   : 1.0
// Empresa  : Unidas Rent a Car
// Equipe   : Unidas Desenvolvimento
// Objetivo : Variaveis globais do sistema
// Histórico: 
//      01/10/2015: Inicio de desenvolvimento para acesso geral
//

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;

namespace centralSitef
{

    public class Globals
    {
        // glbLanguage
        //  1 - Portugues
        //  2 - Espanhol
        //  3 - Ingles
        public static int glbVezes;
        public static int glbLanguage;
        public static int glbScreen;
        public static int glbMensagem;
        public static string glbMensagemSitef;
        public static int glbPopupSitef;
        public static int glbTimeoutSitef;
        public static int glbInteractSitef;

        public static int glbScreenCard;
        public static string glbCC_R_ANUM;
        public static string glbCC_R_ALOJRET;
        public static string glbCC_CCAITENUM;
        public static string glbCC_CupomFiscal;
        public static string glbCC_ValidadeCupomFiscal;
        public static string glbCC_CodigoPreAutorizacao;
        public static string glbCC_CodigoAutorizacaoEmissor;

        public static string glbCC_CartaoInformado;
        public static string glbCC_ValidadeInformado;
        public static string glbCC_SegurancaInformado;

        public static string glbSEG_R_ANUM;
        public static string glbSEG_R_ALOJRET;
        public static string glbSEG_PAGITENUM;
        public static string glbSEG_HASH;
        public static string glbSEG_TP_INFO;
        public static string glbSEG_TP_OPER;
        public static string glbSEG_HASH_OUT;
        public static string glbSEG_RT_OPER;
        public static string glbSEG_Cpf;
        public static string glbSEG_CrtNum;
        public static string glbSEG_MessageText;
        public static string glbSEG_HASH_NEW;

        public static string glbReservaNumeroPesquisa;
        public static string glbLOJCODSITE = "";
        public static string glbRESDOCCLI = "";
        
        public static string glbProtecoesValorPre1 = "";
        public static string glbBandeiraCartao = "";


        public static string glbPortaPinPad = "";
        public static string glbCartao = "";

        public static int glbDebugMode;
        public static string glbCupomFiscal1;
        public static string glbCupomFiscal2;


        public List<string> returnAttributesXml(XmlDocument xmlDoc, string strNode, string strAttributeName)
        {
            List<string> strReturn = new List<string>();
            XmlNodeList xmlValue = xmlDoc.SelectNodes(strNode);
            if (xmlValue.Count > 0)
            {
                for (int x = 0; x < xmlValue.Count; x++)
                {
                    XmlNode node = xmlValue.Item(x).Attributes[strAttributeName];
                    if (node != null && !string.IsNullOrEmpty(node.Value))
                    {
                        strReturn.Add(node.Value);
                    }
                }
            }
            return strReturn;
        }
        public string returnAttributesOneXml(XmlDocument xmlDoc, string strNode, string strAttributeName)
        {
            string strReturn = "";
            XmlNodeList xmlValue = xmlDoc.SelectNodes(strNode);
            if (xmlValue.Count > 0)
            {
                for (int x = 0; x < xmlValue.Count; x++)
                {
                    XmlNode node = xmlValue.Item(x).Attributes[strAttributeName];
                    if (node != null && !string.IsNullOrEmpty(node.Value))
                    {
                        strReturn = node.Value;
                        break;
                    }
                }
            }
            return strReturn;
        }

        public List<string> returnInnerListXml(XmlDocument xmlDoc, string strNode)
        {
            List<string> strReturn = new List<string>();
            XmlNodeList xmlValue = xmlDoc.SelectNodes(strNode);
            if (xmlValue.Count > 0)
            {
                for (int x = 0; x < xmlValue.Count; x++)
                {
                    strReturn.Add(xmlValue.Item(x).InnerXml);
                }
            }
            return strReturn;
        }

        public string returnInnerXml(XmlDocument xmlDoc, string strNode)
        {
            string strReturn = "";
            XmlNodeList xmlValue = xmlDoc.SelectNodes(strNode);
            if (xmlValue.Count > 0)
            {
                for (int x = 0; x < xmlValue.Count; x++)
                {
                    strReturn = xmlValue.Item(x).InnerXml;
                }
            }
            return strReturn;
        }

    }
}
